<?php
/**

 **/
class principal extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
    }
    public function count_results_users($user, $pass)
    {
        $this->db->where('adminUsername', $user);
        $this->db->where('adminPassword', $pass);
        $total = $this->db->count_all_results(CONST_BASE_PRINCIPAL.'admin');
        return $total;
    }

    /*
    *
    */
    public function get_all_data_users_specific($username, $pass)
    {
        $this->db->where('adminUsername', $username);
        $this->db->where('adminPassword', $pass);
        $data = $this->db->get(CONST_BASE_PRINCIPAL.'admin');
        return $data->row();
    }
    public function insert($tabla,$data){
      $this->db->insert($tabla,$data);
      return $this->db->insert_id();
    }
    public function getGeneric($campo='',$valor='',$tabla='',$campoReturn=''){
        $q = $this->db->where($campo,$valor)->get($tabla);
        if($q->num_rows()==1){
            if($campoReturn!=''){
                return $q->row()->$campoReturn;
            }
            return $q->row();
        }else{
            return '';
        }
    }
    //Obtener operaciones extras
      public function operacionesExtrasByCita($id_cita=''){
        return $this->db->where('id_cita',$id_cita)->where('autoriza_cliente',1)->where('pza_garantia',0)->where('activo',1)->get('presupuesto_multipunto')->result();
    }

}
