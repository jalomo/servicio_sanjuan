<?php
/**

 **/
class principal extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
    }
    public function count_results_users($user, $pass)
    {
        $this->db->where('adminUsername', $user);
        $this->db->where('adminPassword', $pass);
        $total = $this->db->count_all_results('principal_queretaro_ford.admin');
        return $total;
    }

    /*
    *
    */
    public function get_all_data_users_specific($username, $pass)
    {
        
        $this->db->where('adminUsername', $username);
        $this->db->where('adminPassword', $pass);
        $data = $this->db->get('principal_queretaro_ford.admin');
        return $data->row();
    }
    public function insert($tabla,$data){
      $this->db->insert($tabla,$data);
      return $this->db->insert_id();
    }

}
