<?php
defined('BASEPATH') OR exit('No direct script access allowed');
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


define('CONST_NO_CITA_NEW_UPDT',0); // A partir de que fecha se hará el proceso de la nueva actualización
define('CONST_FECHA_CITA_NEW_UPDT','2021-05-01'); //A partir de que día se van a mandar las muestras a FORD


define('CONST_URL_CONEXION','https://planificadorempresarial.com/servicios/sjr/citas_sanjuan/');
define('CONST_URL_BASE','https://planificadorempresarial.com/servicios/sjr/');
define('CONST_LOGO','https://planificadorempresarial.com/servicios/sjr/img/logoreal.jpg');
define('CONST_LOGO_EMPRESA','https://planificadorempresarial.com/servicios/sjr/img/logoempresa.jpg');

define('CONST_LAVADO','https://planificadorempresarial.com/lavado_san_juan/index.php/tablero/insertar_auto');

define('CONST_URL_TEXT_MESSAGE','http://planificadorempresarial.mx/cs/citas_ford/horarios_mazatlan/lavado/sms/mensajes_planificador/sms/index.php/sms/enviar');

define('CONST_URL_NOTIFICACION_APP','https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion');

define('CONST_ZONA_HORARIA','America/Mexico_City');

define('CONST_URL_BASE_INTERNA','https://planificadorempresarial.com/servicios/sjr/citas/');
define('CONST_URL_GERENCIA','https://planificadorempresarial.com/gerencia_sanjuan/');

define('CONST_TEL_EMPRESA','(427) 274 73 69');
define('CONST_TEL_HREF','+524272747369');
define('CONST_BASE_PRINCIPAL','principal_sjr.');
 
//Correos
define('CONST_CORREO_SALIDA','fordmylsasjr@planificadorempresarial.com');
define('CONST_PASS_CORREO','pMZ4FQB6NdyJbMwN1');
define('CONST_AGENCIA','FORD MYLSA San Juan del Río');

//updateFechaFactura (función del WS para actualizar las fechas de facturación
define('CONST_URL_FACTURA','https://planificadorempresarial.com/servicios/sjr/assets/facturas/');
define('CONST_URL_READ_XML','https://planificadorempresarial.com/lavado_san_juan/index.php/xml/leer_ford');

//OXLO
define('CONST_URL_SAVE_FILE','/Data/SJR/agencia/'); //Ruta de nuestro servidor, donde se guardan los 3 archivos para mandar a OXLO

define("SMS",'https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_mensaje');

define('CONST_FTP_OXOLO','ftp://148.244.229.38:4020/OnRampMessenger/M2137/OutBox/'); //FTP OXLO
define('CONST_USERPWD_OXLO','GOASIS:Mylsaqro2017'); //Pass FTP
//GENERAR ARCHIVOS
define('CONST_BID','M2139');
define('CONST_COSTO_MANO_OBRA',580);

//DATABASE LOCAL
define("CONST_ACTIVE_GROUP",'default'); //production
define("CONST_HOSTNAME_LOCAL",'localhost');
define("CONST_USERNAME_LOCAL",'root');
define("CONST_PASSWORD_LOCAL",'root');
define("CONST_DATABASE_LOCAL",'sjr3');

//DATABASE PRODUCTION
define("CONST_HOSTNAME_PRODUCTION",'localhost');
define("CONST_USERNAME_PRODUCTION",'root');
define("CONST_PASSWORD_PRODUCTION",'Passw0rd$$');
define("CONST_DATABASE_PRODUCTION",'servicios_sjr');

//Contraseñas
define("CONST_USER_MASTER",'ford');
define("CONST_PASS_MASTER",'fordsjr01');

define("CONST_USER_ASESOR",'diana');
define("CONST_PASS_ASESOR",'diana');

define("CONST_USER_JEFET",'jefemq');
define("CONST_PASS_JEFET",'taller');

define("CONST_USER_MASTER_ORDEN",'Admin');
define("CONST_PASS_MASTER_ORDEN",'AdminSJ123');

define("CONST_USER_ASESOR_ORDEN",'');
define("CONST_PASS_ASESOR_ORDEN",'');

define("CONST_USER_CARRYOVER",'carryover');
define("CONST_PASS_CARRYOVER",'c0que19');

define("CONST_USER_TABLERO",'ford');
define("CONST_PASS_TABLERO",'ford0822');

//Cambiar la situación
define("CONST_USER_SITUACION",'orden');
define("CONST_PASS_SITUACION",'sjr0101');

// Reemplazar facturas orden
define("CONST_USER_FACTURACION",'facturacion');
define("CONST_PASS_FACTURACION",'FacturacionSJ21');

define("CONST_FILE",'https://planificadorempresarial.com/servicios/sjr/citas_sanjuan/conexion/Conexion/loadDataText');
define("CONST_ASSOCIATED_DATA",'https://planificadorempresarial.com/servicios/sjr/citas_sanjuan/conexion/Conexion/load_associated_data');

//Redes sociales
define("CONST_FACEBOOK",'');
define("CONST_TWITTER",'');
define("CONST_INSTAGRAM",'');
define("CONST_YOUTUBE",'');

define("CONST_FACEBOOK_SOHEX",'https://www.facebook.com/SohexDms-104197548089939/');
define("CONST_TWITTER_SOHEX",'https://twitter.com/SohexDms?s=09');
define("CONST_INSTAGRAM_SOHEX",'https://www.instagram.com/sohex_dms/');
define("CONST_LINKEDIN_SOHEX",'https://www.linkedin.com/company/sohexdms');
define("CONST_WHATSAPP_SOHEX",'https://api.whatsapp.com/send?phone=+525535527547&text=¡Hola!%20Quiero%20chatear%20con%20SohexDms');

define("CONST_LINEA_PROCESO",'https://sohex.mx/cs/linea_proceso/');
define("CONST_CORREO_EXT_GARANTIA",'https://sohex.mx/cs/linea_proceso/');

//Integración con DMS (PONER EXACTAMENTE LAS DIAGONALES DE LAS URL'S)
define("CONST_BUSCAR_CLIENTE_SERIE",'https://planificadorempresarial.com/dms/san_juan_dms_api/public/api/vehiculos-clientes/buscar-serie');
define("CONST_BUSCAR_CLIENTE_NOMBRE",'https://planificadorempresarial.com/dms/san_juan_dms_api/public/api/clientes/buscar-cliente');
//Cliente nuevo
define("CONST_CLIENTE_NUEVO",'https://planificadorempresarial.com/servicios/sjr/citas_sanjuan/conexion/Conexion_DMS/generar_guardado');
//vehiculo nuevo
define("CONST_VEHICULO_NUEVO",'https://planificadorempresarial.com/servicios/sjr/citas_sanjuan/conexion/Conexion_DMS/guardar_vehiculo');

//Obtener pre pedidos
define('CONST_PREPEDIDOS','https://planificadorempresarial.com/dms/san_juan_dms_api/public/api/autos/pre-pedidos/preventa-equipo-opcional');
define('CONST_PREPEDIDOS_BY_ID','https://planificadorempresarial.com/dms/san_juan_dms_api/public/api/venta-unidades');
//Obtener el detalle del pre pedido
define('CONST_DETALLE_PREPEDIDOS','https://planificadorempresarial.com/dms/san_juan_dms_api/public/api/autos/equipo-opcional');
//Actualizar la cita de la venta 
define('CONST_ACTUALIZAR_CITA_PREPEDIDO','https://planificadorempresarial.com/dms/san_juan_dms_api/public/api/venta-unidades');
define('CONST_CATALOGO_AUTOS','https://planificadorempresarial.com/dms/san_juan_dms_api/public/api/catalogo-autos');
define('CONST_DMS_INVENTARIO','https://planificadorempresarial.com/dms/san_juan_dms_api/public/api/productos/stockActual');
//Al cerrar la orden
define('CONST_DMS_CERRAR_ORDEN','https://planificadorempresarial.com/dms/san_juan_dms_api/public/api/ventas/mpm-finalizar');

define("CONST_AGENCIA_MENSAJE",'!FordSJR!');
//Encriptar las url
define('CONST_ENCRYPT',778325.77);
define("CONST_DIRECCION",'C. Miguel Hidalgo 162, Centro, 76800 San Juan del Río, Qro');

define("CONST_FOLIOS_GARANTIAS",'https://sohexdms.com.mx/panel_garantias/index.php/garantias/api/folios_asociados_get');
define("CONST_FOLIOS_OPERACIONES",'https://sohexdms.com.mx/panel_garantias/index.php/garantias/F1863/partes_operaciones_byorden');

switch (trim($_SERVER['HTTP_HOST'], '/')) {
    case 'planificadorempresarial.com':
        $url = 'https://planificadorempresarial.com/dms/san_juan_dms_api/public/';
        break;
    default:
        // $url = 'http://localhost:8000/';
        $url = 'https://planificadorempresarial.com/dms/san_juan_dms_api/public/';
        break;
}
defined('API_URL_DEV') or define('API_URL_DEV', $url);

define("CONST_GET_COSTOS_ORDEN", 'https://planificadorempresarial.com/servicios/sjr/citas_sanjuan/conexion/Conexion/datos_refacciones');
define("CONST_CITA_ID_DMS_CLIENTES", '45006');