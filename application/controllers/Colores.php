<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class Colores extends MX_Controller {
  public function __construct()
  {
    parent::__construct();
  }
  public function index($id=0){
    if(!isset($_POST['color'])){
      echo json_encode(array('exito'=>false,'error'=>'Es necesario ingresar todos los datos'));
      exit();
    }
    $datos = array(
                  'id'=>$id,
                  'color'=>$_POST['color'],
                  'activo'=>isset($_POST['activo'])?$_POST['activo']:1,
    );
    if($id==0){
      $exito = $this->db->insert('cat_colores',$datos);
      $id = $this->db->insert_id();
    }else{
      $exito = $this->db->where('id',$id)->update('cat_colores',$datos);
    }
    $datos['id'] = $id;
    echo json_encode(array('exito'=>$exito,'data'=>$datos));
  }
  
}
