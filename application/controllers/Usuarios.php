<?php
defined('BASEPATH') or exit('No direct script access allowed');



class Usuarios extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index($id = 0)
    {

        if (!isset($_POST['nombre']) || !isset($_POST['usuario']) || !isset($_POST['password'])) {
            echo json_encode(array('exito' => false, 'error' => 'Es necesario ingresar todos los datos'));
            exit();
        }
        $datos = array(
            'adminNombre' => $_POST['nombre'],
            'adminUsername' => $_POST['usuario'],
            'adminPassword' => $_POST['password'],
            'adminEmail' => $_POST['email'],
            'adminStatus' => 1,
            'adminFecha' => date('Y-m-d H:i:s'),
            'status' => 2,
            'adminTelefono' =>  $_POST['telefono'],
            'p_citas' => 1,
        );
        if ($id == 0) {
            $q = $this->db->where('adminEmail', $_POST['email'])->get(CONST_BASE_PRINCIPAL . 'admin')->result();
            if (count($q) > 0) {
                echo json_encode(array('exito' => false, 'error' => 'El correo ya fue registrado'));
                exit();
            }
            $exito = $this->db->insert(CONST_BASE_PRINCIPAL . 'admin', $datos);
            $id = $this->db->insert_id();
        } else {
            $exito = $this->db->where('adminId', $id)->update(CONST_BASE_PRINCIPAL . 'admin', $datos);
        }
        $datos['id'] = $id;
        echo json_encode(array('exito' => $exito, 'data' => $datos)); //
    }
}
