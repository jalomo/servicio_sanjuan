<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class login extends MX_Controller {

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('principal', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'url'));
    }

	public function index(){

        $content = $this->blade->render('login',array(), FALSE);

	}

  public function mainView()
    {   
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        if(isset($username) && isset($password) && !empty($password) && !empty($username))
        {

            //$total = $this->principal->count_results_users($username, $password);
            $total = $this->db->where('adminUsername',$username)->where('adminPassword',$password)->where('p_citas',1)->get('principal_sjr.admin')->result();
            
            //if($total == 1)
            if(count($total) == 1)  
            {
                $dataUser = $this->principal->get_all_data_users_specific($username, $password);

                $array_session = array('id_usuario'=>$dataUser->adminId,'usuario'=>$dataUser->adminNombre,'permiso_parametro'=>$dataUser->p_parametros,'permiso_citas'=>$dataUser->p_citas,'usuario' => $dataUser->adminNombre);
                $this->session->set_userdata($array_session);
                if($this->session->userdata('id_usuario'))
                {
                  redirect('citas/ver_citas');
                }
                else{
                }
            }
            else{
                redirect('login');
            }
        }
        else{
            redirect('login');
        }
    }
    public function logout()
    {
        $this->session->unset_userdata('id');
        $this->session->sess_destroy();
        redirect('login');
    }

    public function saveregistro(){
        $post = $this->input->post('save');
        $post['status'] = 2;
        $res = $this->principal->insert('principal_sjr.admin', $post);
        if($res>0){
            redirect('login/');
        }


    }

     public function registroexito(){
     /*if($this->session->userdata('id'))
        {
        $menu_header = $this->load->view('companies/menu_header', '', TRUE);
        $aside = $this->load->view('companies/left_menu', '', TRUE);
        $content = $this->load->view('companies/panel', '', TRUE);
        $this->load->view('main/panel', array('menu_header'=>$menu_header,
                                                       'aside'=>$aside,
                                                       'content'=>$content,
                                                       'included_js'=>array('statics/js/libraries/form.js','statics/js/modules/notificaciones.js')));
        }
        else{
            redirect('companies');
        }*/
        $content = $this->load->view('registroexito', '', FALSE);

    }


}
