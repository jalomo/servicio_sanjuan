<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class Anios extends MX_Controller {
  public function __construct()
  {
    parent::__construct();
  }
  public function index($id=0){
    if(!isset($_POST['anio'])){
      echo json_encode(array('exito'=>false,'error'=>'Es necesario ingresar todos los datos'));
      exit();
    }
    $datos = array(
                    'id'=>$id,
                    'anio'=>$_POST['anio'],
    );
    if($id==0){
      $exito = $this->db->insert('cat_anios',$datos);
      $id = $this->db->insert_id();
    }else{
      $exito = $this->db->where('id',$id)->update('cat_anios',$datos);
    }
    $datos['id'] = $id;
    echo json_encode(array('exito'=>$exito,'data'=>$datos));//
  }
  
}
