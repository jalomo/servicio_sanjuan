<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Modelos extends MX_Controller {
  public function __construct()
  {
    parent::__construct();
  }
  public function index($id=0){
    if(!isset($_POST['modelo'])||!isset($_POST['tiempo_lavado'])||!isset($_POST['categoria_id'])){
      echo json_encode(array('exito'=>false,'error'=>'Es necesario ingresar todos los datos'));
      exit();
    }
    $datos = array(
                          'id'=>$id,
                          'modelo'=>$_POST['modelo'],
                          'tiempo_lavado'=>$_POST['tiempo_lavado'],
                          'activo'=>isset($_POST['activo'])?$_POST['activo']:1,
                          'categoria_id'=>$_POST['categoria_id'],
    );
    if($id==0){
      $exito = $this->db->insert('cat_modelo',$datos);
      $id = $this->db->insert_id();
    }else{
      $exito = $this->db->where('id',$id)->update('cat_modelo',$datos);
    }
    $datos['id'] = $id;
    echo json_encode(array('exito'=>$exito,'data'=>$datos));
  }
  
}
