<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function correo_seguimiento_cita($correo = '', $id_cita = '', $asunto = '', $reparacion = 0)
{
    $ins = &get_instance();
    $ins->load->helper(array('notificaciones', 'correo'));
    //Obtengo la pura notificación para mandarla al cuerpo del correo
    $data['notificacion'] = seguimiento_citas($id_cita);
    $data['asunto'] = $asunto;
    $data['reparacion'] = $reparacion;
    $cuerpo_correo = $ins->blade->render('notificaciones_usuario/correos/correo_seguimiento_cita', $data, TRUE);
    enviar_correo($correo, $asunto, $cuerpo_correo, array());
}
function correo_seguimiento_orden($correo = '', $id_cita = '')
{
    $ins = &get_instance();
    $ins->load->helper(array('notificaciones', 'correo'));
    $ins->load->model('notificaciones_usuario/m_notificaciones', 'mn');
    if ($ins->mn->PermisoNotificacion($id_cita, 'recepcion')) {
        //Obtengo la pura notificación para mandarla al cuerpo del correo
        $data['notificacion'] = seguimiento_orden($id_cita);
        $data['asunto'] = 'Notificación y Seguimiento en Línea de Recepción';
        $cuerpo_correo = $ins->blade->render('notificaciones_usuario/correos/correo_seguimiento_orden', $data, TRUE);
        enviar_correo($correo, $data['asunto'], $cuerpo_correo, array());
    }
}
function correo_cambio_estatus($correo = '', $id_cita = '', $id_operacion = '', $estatusCambio = '')
{
    //estatusCambio siempre es vacío a menos de que venga desde lavado
    $ins = &get_instance();
    $ins->load->helper(array('notificaciones', 'correo'));
    $ins->load->model('notificaciones_usuario/m_notificaciones', 'mn');
    if ($ins->mn->PermisoNotificacion($id_cita, 'cambio_status')) {
        //Obtengo la pura notificación para mandarla al cuerpo del correo
        $data['notificacion'] = cambio_estatus($id_cita, $id_operacion, $estatusCambio);
        $data['asunto'] = 'Notificación y Seguimiento en Línea de Reparación';
        $cuerpo_correo = $ins->blade->render('notificaciones_usuario/correos/correo_cambio_estatus', $data, TRUE);
        enviar_correo($correo, $data['asunto'], $cuerpo_correo, array());
    }
}
function correo_presupuesto($correo = '', $id_cita = '')
{
    //estatusCambio siempre es vacío a menos de que venga desde lavado
    $ins = &get_instance();
    $ins->load->helper(array('notificaciones', 'correo'));

    $ins->load->model('notificaciones_usuario/m_notificaciones', 'mn');
    if ($ins->mn->PermisoNotificacion($id_cita, 'presupuesto')) {
        //Obtengo la pura notificación para mandarla al cuerpo del correo
        $data['notificacion'] = presupuesto($id_cita);
        $data['asunto'] = 'Notificación y Seguimiento en Línea de Presupuesto';
        $cuerpo_correo = $ins->blade->render('notificaciones_usuario/correos/correo_presupuesto', $data, TRUE);
        enviar_correo($correo, $data['asunto'], $cuerpo_correo, array());
    }
}
function correo_facturacion($correo = '', $id_cita = '', $archivos = array())
{
    //estatusCambio siempre es vacío a menos de que venga desde lavado
    $ins = &get_instance();
    $ins->load->helper(array('notificaciones', 'correo'));

    $ins->load->model('notificaciones_usuario/m_notificaciones', 'mn');
    if ($ins->mn->PermisoNotificacion($id_cita, 'facturacion')) {
        //Obtengo la pura notificación para mandarla al cuerpo del correo
        $data['notificacion'] = facturacion($id_cita);
        $data['asunto'] = 'Notificación y Seguimiento en Línea de Facturación';
        $cuerpo_correo = $ins->blade->render('notificaciones_usuario/correos/correo_facturacion', $data, TRUE);
        enviar_correo($correo, $data['asunto'], $cuerpo_correo, $archivos);
    }
}

function correo_fin_reparacion($correo = '', $id_cita = '')
{
    $ins = &get_instance();
    $ins->load->helper(array('notificaciones', 'correo'));
    //Obtengo la pura notificación para mandarla al cuerpo del correo
    $data['notificacion'] = fin_reparacion($id_cita);
    $data['asunto'] = 'Notificación y Seguimiento en Línea de Fin de Reparación';
    $cuerpo_correo = $ins->blade->render('notificaciones_usuario/correos/correo_fin_reparacion', $data, TRUE);
    enviar_correo($correo, $data['asunto'], $cuerpo_correo, array());
}
