<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function seguimiento_citas($id_cita = '',$showNotification=false)
{
    $ins = &get_instance();
    $ins->load->model('citas/m_citas', 'mc');
    $ins->load->model('notificaciones_usuario/m_notificaciones', 'mn');
    $data['id_cita_encrypt'] = $id_cita;
    $id_cita = decrypt($id_cita);

    $data['id_cita'] = $id_cita;
    $data['cita'] = $ins->mn->getAllData($id_cita)[0];
    $data['asesor'] = $ins->mn->asesor_by_nombre($data['cita']->asesor);
    //Guardar el tipo de notificación que se le mandó al cliente
    //Parametros : id_cita y el tipo de notificación que se envió
    if(!$showNotification){
        guardarNotificacion($id_cita, 1);
    }
    
    return $ins->blade->render('notificaciones_usuario/v_seguimiento_citas', $data, TRUE);
}
function seguimiento_orden($id_cita = '',$showNotification=false)
{
    //$showNotification es para solo mostrar la notificación y no guardar la notificación
    $ins = &get_instance();
    $ins->load->model('citas/m_citas', 'mc');
    $ins->load->model('notificaciones_usuario/m_notificaciones', 'mn');
    //Envío de notificaciones cada que se edita la orden
    $data['id_cita_encrypt'] = $id_cita;
    $id_cita = decrypt($id_cita);
    $data['id_cita'] = $id_cita;
    $data['cita'] = $ins->mn->getAllData($id_cita)[0];
    $data['asesor'] = $ins->mn->asesor_by_nombre($data['cita']->asesor);
    $data['hora_inicio_trabajo'] = $ins->mc->getFechaTransicion('trabajando', $id_cita);
    if(!$showNotification){
        guardarNotificacion($id_cita, 2);
    }
    return $ins->blade->render('notificaciones_usuario/v_seguimiento_orden', $data, TRUE);
}
function cambio_estatus($id_cita = '', $id_operacion = '', $estatusCambio = '',$showNotification=false)
{
    
    
    //estatusCambio siempre es vacío a menos de que venga desde lavado
    $ins = &get_instance();
    $ins->load->model('citas/m_citas', 'mc');
    $ins->load->model('notificaciones_usuario/m_notificaciones', 'mn');
    //Envío de notificaciones cada que cambia un estatus
    $data['id_cita_encrypt'] = $id_cita;
    $data['id_operacion_encrypt'] = $id_operacion;
    $id_cita = decrypt($id_cita);
    $id_operacion = decrypt($id_operacion);
    if($id_operacion==0){
        $id_operacion= '';
    }
    $data['id_cita'] = $id_cita;
    $data['estatusCambio'] = $estatusCambio;
    $data['id_operacion'] = $id_operacion;
    $data['cita'] = $ins->mn->getAllData($id_cita)[0];
    $data['asesor'] = $ins->mn->asesor_by_nombre($data['cita']->asesor);
    $data['operacion'] = $ins->mn->getDataOperacion($id_operacion);
    if(!$showNotification){
        guardarNotificacion($id_cita, 3, $id_operacion, $estatusCambio);
    }
    return $ins->blade->render('notificaciones_usuario/v_cambio_estatus', $data, TRUE);
}
function presupuesto($id_cita = '',$showNotification=false)
{
    $ins = &get_instance();
    $ins->load->model('citas/m_citas', 'mc');
    $ins->load->model('notificaciones_usuario/m_notificaciones', 'mn');

    $data['id_cita_encrypt'] = $id_cita;
    $id_cita = decrypt($id_cita);
    $data['id_cita'] = $id_cita;
    $data['cita'] = $ins->mn->getAllData($id_cita)[0];
    $data['asesor'] = $ins->mn->asesor_by_nombre($data['cita']->asesor);
    if(!$showNotification){
        guardarNotificacion($id_cita, 4);
    }
    
    return $ins->blade->render('notificaciones_usuario/v_presupuesto', $data, TRUE);
}
function fin_reparacion($id_cita = '',$showNotification=false)
{
    $ins = &get_instance();
    $ins->load->model('citas/m_citas', 'mc');
    $ins->load->model('notificaciones_usuario/m_notificaciones', 'mn');

    $data['id_cita_encrypt'] = $id_cita;
    $id_cita = decrypt($id_cita);
    $data['id_cita'] = $id_cita;
    $data['cita'] = $ins->mn->getAllData($id_cita)[0];
    $data['asesor'] = $ins->mn->asesor_by_nombre($data['cita']->asesor);
    if(!$showNotification){
        guardarNotificacion($id_cita, 5);
    }
    return $ins->blade->render('notificaciones_usuario/v_fin_reparacion', $data, TRUE);
}
function facturacion($id_cita = '',$showNotification=false)
{
    $ins = &get_instance();
    $ins->load->model('citas/m_citas', 'mc');
    $ins->load->model('notificaciones_usuario/m_notificaciones', 'mn');

    $data['id_cita_encrypt'] = $id_cita;
    $id_cita = decrypt($id_cita);
    $data['id_cita'] = $id_cita;
    $data['cita'] = $ins->mn->getAllData($id_cita)[0];
    $data['asesor'] = $ins->mn->asesor_by_nombre($data['cita']->asesor);
    if(!$showNotification){
        guardarNotificacion($id_cita, 6);
    }
    return $ins->blade->render('notificaciones_usuario/v_facturacion', $data, TRUE);
}
function guardarNotificacion($id_cita = '', $id_tipo_notificacion, $id_operacion = '', $estatusCambio = '')
{
    $ins = &get_instance();
    $ins->load->model('citas/m_citas', 'mc');
    $data = [
        'id_cita' => $id_cita,
        'id_tipo_notificacion' => $id_tipo_notificacion,
        'id_operacion' => $id_operacion,
        'estatusCambio' => $estatusCambio,
        'id_usuario' => $ins->session->userdata('id_usuario'),
        'created_at' => date('Y-m-d H:i:s')
    ];
    $ins->db->insert('notificaciones_usuario_orden', $data, TRUE);
}
