<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//composer require dompdf/dompdf
	use Dompdf\Dompdf;
	use Dompdf\Options;
	function pdf_create($html, $filename='', $stream=TRUE) {
	    require_once("dompdf/dompdf_config.inc.php");

	    $dompdf = new DOMPDF();
	    $dompdf->load_html($html);
	    $dompdf->render();
	    if ($stream) {
	        $dompdf->stream($filename.".pdf");
	    } else {
	        return $dompdf->output();
	    }
	}
	function pdf_create_horizontalBK($html, $filename='', $stream=TRUE) {
	    $dompdf = new Dompdf();
	    $dompdf->loadHtml($html);
	    // (Optional) Setup the paper size and orientation
	    $dompdf->setPaper('A4', 'landscape');
	    // Render the HTML as PDF
	    $dompdf->render();
	    // Output the generated PDF to Browser
	    $dompdf->stream($filename);
	}
	function pdf_create_horizontal($html, $filename='', $stream=TRUE) {
		
	    $options = new Options();
		$options->set('isRemoteEnabled', TRUE);
		$dompdf = new Dompdf($options);
		$contxt = stream_context_create([ 
		    'ssl' => [ 
		        'verify_peer' => FALSE, 
		        'verify_peer_name' => FALSE,
		        'allow_self_signed'=> TRUE
		    ] 
		]);
		$dompdf->setHttpContext($contxt);
	    //$dompdf = new Dompdf();
	    $dompdf->loadHtml($html);
	    // (Optional) Setup the paper size and orientation
	    $dompdf->setPaper('A4', 'landscape');
	    //$dompdf->load_html(utf8_decode($html));
	    // Render the HTML as PDF
	    $dompdf->render('D');
	    $dompdf->stream($filename.".pdf");

	    //$dompdf->output('F');
	    // Output the generated PDF to Browser
	    //$dompdf->stream($filename.'.pdf', array("Attachment" => false));
	}
?>