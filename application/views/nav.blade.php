  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="">Menú</a>

    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">


        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-wrench"></i>
            <span class="nav-link-text">Citas</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseComponents">
            <li>
              <a href="{{base_url('citas/notificaciones')}}">Notificaciones</a>
            </li>
            @if($this->session->userdata('id_usuario')==1 || $this->session->userdata('id_usuario')==12 || $this->session->userdata('id_usuario')==24 || $this->session->userdata('id_usuario')==25 || $this->session->userdata('id_usuario')==26 || $this->session->userdata('id_usuario')==27 || $this->session->userdata('id_usuario')==19)
            @endif
            <li>
              <a href="{{base_url('citas/UpdateViewCita')}}">Actualizar cita</a>
            </li>
            <li>
              <a href="{{base_url('citas/index')}}">Agregar asesor</a>
            </li>
            <li>
              <a href="{{base_url('citas/lista_tecnicos')}}">Agregar técnico</a>
            </li>
            <li>
              <a href="{{base_url('citas/agendar_cita')}}">Agendar Cita</a>
            </li>
            <li>
              <a href="{{base_url('citas/ver_todas_citas')}}">Lista de citas</a>
            </li>

            <!--<li>
              <a href="{{base_url('citas/graficas')}}">Búsquedas inteligentes</a>
            </li>-->
            <li>
              <a target="_blank" href="{{base_url('citas/tablero_asesores')}}">Tablero de citas por asesor</a>
            </li>
             <li>
              <a target="_blank" href="{{base_url('citas/tablero_tecnicos')}}">Tablero de citas por técnico</a>
            </li>
            <li>
              <a target="_blank" href="{{base_url('citas/busquedas_inteligentes')}}">Búsquedas inteligentes</a>
            </li>
            <li>
              <a href="{{base_url('citas/estadisticas')}}">Estadísticas gráficas</a>
            </li>
            <li>
              <a href="{{base_url('citas/filtro_citas')}}">Filtros</a>
            </li>
            @if(PermisoModulo('citas','reagendar'))
              <li>
              <a href="{{base_url('citas/reagendar')}}">Reagendar</a>
              </li>
            @endif
            
            <li>
              <a href="{{base_url('citas/evidencia')}}">Evidencia</a>
            </li>
            <li>
              <a href="{{base_url('citas/proactivo_unidades_nuevas')}}">Proactivo unidades nuevas</a>
            </li>
            <li>
              <a href="{{base_url('citas/asc_campanias')}}">ASC Campañas</a>
            </li>
            <li>
              <a href="{{base_url('citas/unidades_en_taller')}}">Unidades en taller</a>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#orden-servicio" data-parent="#id_catalogos">
            <i class="fa fa-fw fa-wrench"></i>
            <span class="nav-link-text">Orden servicio</span>
          </a>
          <ul class="sidenav-second-level collapse" id="orden-servicio">
            <li>
              <a href="{{base_url('citas/documentacion_ordenes')}}">Documentación ordenes</a>
            </li>
            <li>
              <a href="{{base_url('citas/cita_servicio')}}">Orden servicio parte I</a>
            </li>
            <li>
              <a href="{{CONST_URL_CONEXION}}OrdenServicio_Alta/0" target="_blank">Orden servicio parte II</a>
            </li>
            <li>
              <a href="{{CONST_URL_CONEXION}}multipunto/Multipunto/index/0" target="_blank">Multipunto</a>
            </li>
            <li>
              <a href="{{base_url('orden/historial_orden_servicio')}}">Historial de ordenes</a>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link" href="{{base_url('citas/validarGO')}}">
              <i class="fa fa-fw fa-wrench"></i>
              <span class="nav-link-text1">GOASIS</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#catalogos" data-parent="#id_catalogos">
            <i class="fa fa-fw fa-wrench"></i>
            <span class="nav-link-text">Catálogos</span>
          </a>
          <ul class="sidenav-second-level collapse" id="catalogos">
            <li>
              <a href="{{base_url('citas/modelos')}}">Modelos</a>
            </li>
            <li>
              <a href="{{base_url('citas/codigos_gen')}}">Códigos genéricos</a>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link" href="https://sohex.mx/cs/linea_proceso/" target="_blank">
              <i class="fa fa-fw fa-wrench"></i>
              <span class="nav-link-text1">Línea de proceso</span>
            </a>
        </li>
        @if($this->session->userdata('id_usuario')==1 || $this->session->userdata('id_usuario')==12 || $this->session->userdata('id_usuario')==13)
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#parametros" data-parent="#parametros">
              <i class="fa fa-fw fa-wrench"></i>
              <span class="nav-link-text">Parámetros</span>
            </a>
            <ul class="sidenav-second-level collapse" id="parametros">
              <li>
                <a href="{{base_url('citas/usuarios_permisos')}}">Permisos</a>
              </li>
              <li>
                <a href="{{base_url('citas/parametros')}}">Lapso entre citas</a>
              </li>
              <li>
                <a href="{{base_url('citas/parametros_horarios_tecnicos')}}">Agregar Horarios técnicos</a>
              </li>
               <li>
                <a href="{{base_url('citas/cambiar_horarios')}}">Actualizar Horarios técnicos</a>
              </li>
            </ul>
          </li>
        @endif

         @if(PermisoModulo('contacto_proactivo'))
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#proactivo" data-parent="#proactivo">
              <i class="fa fa-fw fa-wrench"></i>
              <span class="nav-link-text">Contacto Proactivo</span>
            </a>
            <ul class="sidenav-second-level collapse" id="proactivo">
              <li>
                <a href="{{base_url('proactivo')}}">Realizar contacto</a>
              </li>
              <li>
                <a href="{{base_url('proactivo/generar_mes')}}">Generar contacto proactivo por mes</a>
              </li>
              <li>
                <a href="{{base_url('proactivo/historial_proactivo')}}">Historial</a>
              </li>
              <li>
                <a href="{{base_url('proactivo/reactivar')}}">Reactivar</a>
              </li>
            </ul>
          </li>
        @endif
        @if(PermisoModulo('importar_excel'))
          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#excel" data-parent="#excel">
              <i class="fa fa-fw fa-wrench"></i>
              <span class="nav-link-text">Importar</span>
            </a>
            <ul class="sidenav-second-level collapse" id="excel">
              <li>
                <a href="{{base_url('excel')}}">VINLIST</a>
              </li>
              <li>
                <a href="{{base_url('excel/unidades_nuevas')}}">Unidades nuevas</a>
              </li>
            </ul>
          </li>
        @endif

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link" href="{{base_url('citas/validarCambioStatus')}}" target="_blank">
              <i class="fa fa-fw fa-wrench"></i>
              <span class="nav-link-text1">Validar estatus</span>
            </a>
          
        </li>
        @if(PermisoModulo(''))
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
            <a class="nav-link" href="{{base_url('estadisticas/')}}" target="_blank">
                <i class="fa fa-fw fa-wrench"></i>
                <span class="nav-link-text1">Estadísticas</span>
              </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link" href="{{base_url('indicadores/gerencia')}}">
              <i class="fa fa-fw fa-wrench"></i>
              <span class="nav-link-text1">Gerencia</span>
            </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#permisos" data-parent="#permisos">
              <i class="fa fa-fw fa-wrench"></i>
              <span class="nav-link-text">Permisos</span>
            </a>
            <ul class="sidenav-second-level collapse" id="permisos">
              <li>
                <a href="{{base_url('permisos/permisos_accion')}}">Permisos por acción</a>
              </li>
              <li>
                <a href="{{base_url('permisos/permisos_modulo')}}">Permisos por módulos</a>
              </li>
            </ul>
          </li>
        @endif

      </ul>

      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>

    </div>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a href="{{ site_url('soporte/listado') }}" class="nav-link">
              <i style="color: white" class="fa fa-ticket"></i>

          </a>
      </li>
        <li class="nav-item">
          <a href="{{site_url('login/logout')}}" class="nav-link">
            Salir</a>
        </li>
      </ul>
  </nav>
