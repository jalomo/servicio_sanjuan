
<nav>
    <ul class="nav luna-nav">
        <li class="nav-category">
            Servicios
        </li>
        <div class="nav nav-second collapse nav-cat-2 show" id="level_one_0">
            <li id="submenu_0">
                <a href="#submenu_citas" data-toggle="collapse" aria-expanded="false" class="collapsed">
                    Citas<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="submenu_citas" class="nav nav-second nav-cat-3 collapse" style="">
                    <li>
                            <a href="{{base_url('citas/notificaciones')}}">Notificaciones</a>
                    </li>
                    @if($this->session->userdata('id_usuario')==1 || $this->session->userdata('id_usuario')==12 || $this->session->userdata('id_usuario')==4 || $this->session->userdata('id_usuario')==36)
                    <li>
                        <a href="{{base_url('citas/UpdateViewCita')}}">Actualizar cita</a>
                    </li>
                    @endif
                    <li>
                        <a href="{{base_url('citas/index')}}">Agregar asesor</a>
                    </li>
                    <li>
                        <a href="{{base_url('citas/lista_tecnicos')}}">Agregar técnico</a>
                    </li>
                    <li>
                        <a href="{{base_url('citas/agendar_cita')}}">Agendar Cita</a>
                    </li>
                    <li>
                        <a href="{{base_url('citas/unidades_instalacion')}}">Unidades nuevas para instalación</a>
                    </li>
                    <li>
                        <a href="{{base_url('citas/ver_todas_citas')}}">Lista de citas</a>
                    </li>
                    <li>
                        <a target="_blank" href="{{base_url('citas/tablero_asesores')}}">Tablero de citas por asesor</a>
                    </li>
                    <li>
                        <a target="_blank" href="{{base_url('citas/tablero_tecnicos')}}">Tablero de citas por técnico</a>
                    </li>
                    <li>
                        <a target="_blank" href="{{base_url('citas/busquedas_inteligentes')}}">Búsquedas inteligentes</a>
                    </li>
                    <li>
                        <a href="{{base_url('citas/estadisticas')}}">Estadísticas gráficas</a>
                    </li>
                    <li>
                        <a href="{{base_url('citas/filtro_citas')}}">Filtros</a>
                    </li>
                    @if(PermisoModulo('citas','reagendar'))
                    <li>
                        <a href="{{base_url('citas/reagendar')}}">Reagendar</a>
                    </li>
                    @endif
                    <li>
                        <a href="{{base_url('citas/evidencia')}}">Evidencia</a>
                    </li>
                    <li>
                        <a target="" href="#">Oasis Planta</a>
                    </li>
                    <li>
                        <a href="{{base_url('citas/proactivo_unidades_nuevas')}}">Proactivo unidades nuevas</a>
                    </li>
                    <li>
                        <a href="{{base_url('citas/asc_campanias')}}">ASC Campañas</a>
                    </li>
                    <li>
                      <a href="{{base_url('citas/proactivo_historial')}}">Proactivo Historial</a>
                    </li>
                    <li>
                        <a href="{{base_url('citas/unidades_en_taller')}}">Unidades en taller</a>
                    </li>
                </ul>
            </li>
            <li id="submenu_1">
                <a href="#submenu_orden" data-toggle="collapse" aria-expanded="false" class="collapsed">
                    Orden Servicio<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="submenu_orden" class="nav nav-second nav-cat-3 collapse" style="">
                    <li>
                        <a href="{{base_url('citas/documentacion_ordenes')}}">Documentación ordenes</a>
                    </li>
                    <li>
                        <a href="{{base_url('citas/cita_servicio')}}">Orden servicio parte I</a>
                    </li>
                    <li>
                        <a href="{{CONST_URL_CONEXION}}OrdenServicio_Verifica/{{encrypt(0)}}" target="_blank">Orden servicio parte II</a>
                    </li>
                    <li>
                        <a href="{{CONST_URL_CONEXION}}Multipunto_Edita_Asesor/0" target="_blank">Multipunto</a>
                    </li>
                    <li>
                        <a href="{{base_url('orden/historial_orden_servicio')}}">Historial de ordenes</a>
                    </li>
                </ul>
            </li>
            
            <li id="submenu_2">
                <a href="#submenu_catalogos" data-toggle="collapse" aria-expanded="false" class="collapsed">
                    Catálogos<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="submenu_catalogos" class="nav nav-second nav-cat-3 collapse" style="">
                    <li>
                        <a href="{{ base_url('citas/codigos_gen') }}">Códigos genéricos</a>
                    </li>
                    <li>
                        <a href="{{ base_url('citas/modelos') }}">Modelos</a>
                    </li>
                    <li>
                        <a href="{{ base_url('dmspaquetes/catalogos/1') }}">Artículos</a>
                    </li>
                    <li>
                        <a href="{{ base_url('dmspaquetes/catalogos/2') }}">Cilindros</a>
                    </li>
                    <li>
                        <a href="{{ base_url('dmspaquetes/catalogos/3') }}">Combustible</a>
                    </li>
                    <li>
                        <a href="{{ base_url('dmspaquetes/catalogos/4') }}">Motor</a>
                    </li>
                    <li>
                        <a href="{{ base_url('dmspaquetes/catalogos/5') }}">Tipo de paquete</a>
                    </li>
                    <li>
                        <a href="{{ base_url('dmspaquetes/catalogos/6') }}">Tipo de precio</a>
                    </li>
                    <li>
                        <a href="{{ base_url('dmspaquetes/catalogos/7') }}">Transmisiones</a>
                    </li>
                    <li>
                        <a href="{{ base_url('dmspaquetes/subarticulos') }}">Subartículos</a>
                    </li>
                    <li>
                        <a href="{{ base_url('dmspaquetes/catalogo_claves') }}">Claves</a>
                    </li>
                </ul>
            </li>
            @if($this->session->userdata('id_usuario')==1 || $this->session->userdata('id_usuario')==12 || $this->session->userdata('id_usuario')==13)
            <li id="submenu_3">
                <a href="#submenu_parametros" data-toggle="collapse" aria-expanded="false" class="collapsed">
                    Parámetros<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="submenu_parametros" class="nav nav-second nav-cat-3 collapse" style="">
                    <li>
                        <a href="{{base_url('citas/usuarios_permisos')}}">Permisos</a>
                    </li>
                    <li>
                        <a href="{{base_url('citas/parametros')}}">Lapso entre citas</a>
                    </li>
                    <li>
                        <a href="{{base_url('citas/parametros_horarios_tecnicos')}}">Agregar Horarios técnicos</a>
                    </li>
                    <li>
                        <a href="{{base_url('citas/cambiar_horarios')}}">Actualizar Horarios técnicos</a>
                    </li>
                </ul>
            </li>
            @endif
            @if(PermisoModulo('contacto_proactivo'))
            <li id="submenu_4">
                <a href="#submenu_cp" data-toggle="collapse" aria-expanded="false" class="collapsed">
                    Contacto Proactivo<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="submenu_cp" class="nav nav-second nav-cat-3 collapse" style="">
                    <li>
                        <a href="{{base_url('proactivo')}}">Realizar contacto</a>
                    </li>
                    <li>
                        <a href="{{base_url('proactivo/generar_mes')}}">Generar contacto proactivo por mes</a>
                    </li>
                    <li>
                        <a href="{{base_url('proactivo/historial_proactivo')}}">Historial</a>
                    </li>
                    <!-- <li>
                        <a href="{{base_url('proactivo/reactivar')}}">Reactivar</a>
                    </li> -->
                </ul>
            </li>
            @endif
            @if(PermisoModulo('importar_excel'))
            <li id="submenu_5">
                <a href="#submenu_importar" data-toggle="collapse" aria-expanded="false" class="collapsed">
                    Importar<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="submenu_importar" class="nav nav-second nav-cat-3 collapse" style="">
                    <li>
                        <a href="{{base_url('excel')}}">VINLIST</a>
                    </li>
                    <li>
                        <a href="{{base_url('excel/unidades_nuevas')}}">Unidades nuevas</a>
                    </li>
                </ul>
            </li>
            @endif
            @if(PermisoModulo('importar_excel'))
            <li id="submenu_6">
                <a href="#submenu_permisos" data-toggle="collapse" aria-expanded="false" class="collapsed">
                    Permisos<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="submenu_permisos" class="nav nav-second nav-cat-3 collapse" style="">
                    <li>
                        <a href="{{base_url('permisos/permisos_accion')}}">Permisos por acción</a>
                    </li>
                    <li>
                        <a href="{{base_url('permisos/permisos_modulo')}}">Permisos por módulos</a>
                    </li>
                </ul>
            </li>
            @endif
        </div>
        <li id="submenu_7">
            <a href="#submenu_paquetes" data-toggle="collapse" aria-expanded="false" class="collapsed">
                Paquetes<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
            </a>
            <ul id="submenu_paquetes" class="nav nav-second nav-cat-3 collapse" style="">
                <li>
                    <a href="{{ base_url('dmspaquetes/listado') }}">Listado de paquetes</a>
                </li>
                <li>
                    <a href="{{ base_url('dmspaquetes/agregar') }}">Agregar</a>
                </li>
            </ul>
        </li>
        <li id="" class="nav-category nav-cat-1" style="padding-left: 0px;">
            <a href="{{base_url('citas/validarGO')}}">
                <i class="fas fa-user-tie"></i> GOASIS
            </a>
        </li>
        <li id="" class="nav-category nav-cat-1" style="padding-left: 0px;">
            <a href="{{CONST_LINEA_PROCESO }}">
                <i class="fas fa-user-tie"></i> Línea de Proceso
            </a>
        </li>
        <li id="" class="nav-category nav-cat-1" style="padding-left: 0px;">
            <a href="{{base_url('citas/validarCambioStatus')}}" target="_blank">
                <i class="fas fa-user-tie"></i> Validar Estatus
            </a>
        </li>
        @if(PermisoModulo(''))
        <li id="" class="nav-category nav-cat-1" style="padding-left: 0px;">
            <a href="{{base_url('estadisticas/')}}" target="_blank">
                <i class="fas fa-user-tie"></i> Estadísticas
            </a>
        </li>
        <li id="" class="nav-category nav-cat-1" style="padding-left: 0px;">
            <a href="{{base_url('indicadores/gerencia')}}">
                <i class="fas fa-user-tie"></i> Gerencia
            </a>
        </li>
        @endif
    </ul>
    <br><br>
</nav>