<!DOCTYPE html>
<html>
<head>
    <base href="{{base_url()}}">
    @include('tema_luna/head')
    <script type="text/javascript">
        var PATH = "<?php echo site_url(); ?>";
        var PATH_BASE = "<?php echo base_url(); ?>";
        var PATH_LANGUAGE = "<?php echo base_url('js/spanish.json'); ?>";
        var CONTROLLER = "<?php echo $this->router->class; ?>";
        var FUNCTION = "<?php echo $this->router->method; ?>";
    </script>
</head>
<body>
    <!-- Wrapper-->
    <div class="wrapper">
        <!-- Header-->
        <nav class="navbar navbar-expand-md navbar-default fixed-top">
            <div class="navbar-header">
                <div id="mobile-menu">
                    <div class="left-nav-toggle">
                        <a href="#">
                            <i class="stroke-hamburgermenu"></i>
                        </a>
                    </div>
                </div>
                <a class="navbar-brand" href="<?php echo base_url()?>citas/ver_citas" style="background: #6ba0cb; padding: 0; display: flex; text-align: center; align-items: center; justify-content: center;">
                    <img src="<?php echo base_url()?>luna/logodms.png" width="90%">
                    <!--span>v.1.4</span-->
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <div class="left-nav-toggle">
                    <a href="">
                        <i class="stroke-hamburgermenu"></i>
                    </a>
                </div>
                <form class="navbar-form  mr-auto">
                </form>
                <ul class="nav navbar-nav">
                    <li class="nav-item uppercase-link">
                    <a href="{{ base_url('login/logout') }}" style="color:white;"  class="nav-link">
                            Cerrar sesión
                        </a>
                    </li>
                    <li class="nav-item profil-link">
                        <a href="<?php echo base_url()?>usuarios/perfil">
                            <span class="profile-address" style="color: #fff;">
                            {{ $this->session->userdata('nombre') }}
                            </span>
                            <img src="<?php echo base_url()?>luna/images/profile.jpg" class="rounded-circle" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- End header-->

        <!-- Navigation-->
        <aside class="navigation">
            @include('tema_luna/menu')
        </aside>
        <!-- End navigation-->
        <!-- Main content-->
        <section class="content">
            <div class="container-fluid">
                <div class="container-fluid panel-body">
                @yield('contenido')
                </div>
            </div>
        </section>
        <!-- End main content-->
    </div>
    <!-- End wrapper-->
    <!-- Vendor scripts -->
    @include('tema_luna/dependencias_footer')
    @yield('modal')
    @yield('scripts')
</body>

</html>