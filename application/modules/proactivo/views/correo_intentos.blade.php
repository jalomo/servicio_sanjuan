<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<style>
		.justify{
			text-align: justify;
		}
		table{
			margin: 0px 400px 0px;
			font-size: 20px;
		}
		@media screen and (max-width: 992px) {
		  	table{
				margin: 0px 50px 0px !important;
		    }
		}

		/*On screens that are 600px or less, set the background color to olive */
		@media screen and (max-width: 600px) {
			table{
				margin: 0px 0px 0px !important;
		    }
		}
	</style>
</head>
<body>
	<table>
		<tbody>
			<tr>
				<td><img style="display: block; margin-right: auto;" tabindex="0" src="{{CONST_LOGO_EMPRESA}}" alt="" width="400" height="120" /></td>
			</tr>
			<tr class="text-center">
				<td>
					<p>Estimad@, <strong>{{$informacion->cnombre.' '.$informacion->cap.' '.$informacion->cam}}</strong>
					</p>
					<p class="justify">
						El motivo de la presente carta es comunicarle que {{CONST_AGENCIA}} S.A. DE C.V ha tratado de localizarle para notificarle que su auto está próximo a cumplir su <strong>SERVICIO DE MANTENIMIENTO</strong> motivo por el cual se le hace una atenta y calida invitación para ponerse en contacto con nosotros en los teléfonos <strong>(442) 238-7400 EXT.411</strong> / <strong>(442) 238-7400</strong> o al e-mail: <a href="mailto:contacto@ford.com.mx"> <strong>contacto@ford.com.mx</strong> 
					</p>
					<p>
						Agradeciendo sus finas atenciones, esperamos contar con su reservación y preferencia. 
					</p>
					<div style="margin-left: 100px">
						<p>
							Reciba un cordial Saludo.	
						</p>
					</div>
					<p style="text-align: center;">
						<strong>
							"Porque eres parte de nuestra familia FORD mereces un servicio EXCELENTE"
						</strong>
						<br>
						<br>
						<br>
						<span style="font-size: 10px;">
							En caso de que al recibir este aviso ya hubiera Usted efectuado el servicio a que nos referimos, favor de hacer caso omiso.
						</span>
					</p>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>