<table id="tbl_proactivo" class="table table-hover table-striped">
	<thead>
		<tr class="tr_principal">
			<th>Acciones</th>
			<th>ID</th>
			<th>Intentos</th>
			<th>Kilometraje</th>
			<th>Placas</th>
			<th>Cliente</th>
			<th>Email</th>
			<th>Teléfono</th>
			<th>Fecha</th>
			<th>Serie</th>
			<th>Modelo</th>
			<th>Año</th>

		</tr>
	</thead>
	<tbody>
		@foreach($registros as $r => $registro)
		<tr>
			<td>

				<a href="citas/agendar_cita/0/0/0/{{$registro->id}}" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Agendar cita"><i class="fa fa-plus"></i></a>

				<a href="" data-id="{{$registro->id}}" class="fa fa-comments js_comentarios" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Comentarios" data-fecha="{{$registro->fecha_programacion}}" data-cliente="{{$registro->nombre}}"></a>

				<a href="" data-id="{{$registro->id}}" class="fa fa-info js_historial" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Historial comentarios"></a>

			</td>
			<td>{{$registro->id}}</td>
			<td>{{$registro->intentos}}</td>
			<td>{{$registro->km_actual}}</td>
			<td>{{$registro->placas}}</td>
			<td>{{$registro->nombre}}</td>
			<td>{{$registro->correo}}</td>
			<td>{{$registro->telefono}}</td>
			<td>{{$registro->fecha_programacion}}</td>
			<td>{{$registro->serie}}</td>
			<td>{{$registro->auto}}</td>
			<td>{{$registro->anio}}</td>
		</tr>
		@endforeach
	</tbody>
</table>