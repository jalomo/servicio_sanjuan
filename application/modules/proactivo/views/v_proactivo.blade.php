@layout('tema_luna/layout')
@section('contenido')
	<ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="#">Inicio</a>
	    </li>
	    <li class="breadcrumb-item active">Reparaciones menores</li>
  	</ol>
	<div class="row">
		<div class="col-sm-11">
			<h1>Lista de Reparaciones menores</h1>
		</div>
		<div class="col-sm-1 pull-right">
			<a href="{{base_url('preventivo/agregar_preventivo/0')}}" style="margin-right: 20px;" id="agregar_operador" class="btn btn-success pull-right">Agregar registro</a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div id="div_operadores">
				<table id="tbl_preventivo" class="table table striped table-hover" width="100%">
					<thead>
						<tr class="tr_principal">
							<th>Código</th>
							<th>Modelo</th>
							<th>Tipo</th>
							<th>Motor</th>
							<th>Descripción</th>
							<th>Catálogo</th>
							<th>Año</th>
							<th># Parte</th>
							<th>Cantidad</th>
							<th>Tipo MO</th>
							<th>Código Ereact</th>
							<th>Descripción</th>
							<th>Tiempo</th>
							<th>Precio</th>
							<th>Costo</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($preventivo as $c => $value)
						<tr>
							<td>{{$value->codigo}}</td>
							<td>{{$value->modelo}}</td>
							<td>{{$value->tipo}}</td>
							<td>{{$value->motor}}</td>
							<td>{{$value->descripcion1}}</td>
							<td>{{$value->catalogo}}</td>
							<td>{{$value->anio}}</td>
							<td>{{$value->parte}}</td>
							<td>{{$value->cantidad}}</td>
							<td>{{$value->tipomo}}</td>
							<td>{{$value->codigo_ereact}}</td>
							<td>{{$value->descripcion}}</td>
							<td>{{$value->tiempo}}</td>
							<td>{{$value->precio}}</td>
							<td>{{$value->costo}}</td>


							<td>
								<a href="{{base_url('preventivo/agregar_preventivo').'/'.$value->id}}" class="fa fa-edit js_editar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar"></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('scripts')

	<script>
	var site_url = "{{site_url()}}";
	inicializar_tabla("#tbl_preventivo",false);
	</script>
@endsection