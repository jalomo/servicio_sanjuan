<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_proactivo extends CI_Model{
  public function __construct(){
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
  }
  //Obtener solo los registros de hace 6 meses 
  public function getData6Month($month='',$reprogramado=0,$year=0){
    //var_dump($month,$reprogramado);die();
    $this->db->where('YEAR(fecha)',$year);
  	return $this->db->where('MONTH(fecha)',$month)->order_by('fecha','asc')->get('ordenes_old')->result();
  }
  //guarda los comentariosa
  public function saveComentario(){
    
      
       $id= $this->input->post('id_cita');
       $datos = array('comentario' => $this->input->post('comentario'),
                      'id_cita' => $id,
                      'fecha_creacion' =>date('Y-m-d H:i:s'),
                      'id_usuario'=>$this->session->userdata('id_usuario'),
                      'fecha_notificacion' =>date2sql($this->input->post('cronoFecha')).' '.$this->input->post('cronoHora'),
                      'proactivo'=>1
        );
        $this->db->insert('historial_proactivo_inicial',$datos);

        //Insertar notificación
        if($this->input->post('comentario')!=''){

          if($this->input->post('cronoFecha')!='' && $this->input->post('cronoFecha')!='cronoHora'){
            $date = date2sql($this->input->post('cronoFecha'));
            $newdate = strtotime ( '+6 month' , strtotime ( $date ) ) ;
            $date = date ( 'Y-m-d' , $newdate );

            $notific['texto']= 'Cliente: '.$this->input->post('cliente').' '.$this->input->post('comentario');
            $notific['id_user'] = $this->session->userdata('id_usuario');
            $notific['estado'] = 1;
            $notific['tipo_notificacion'] = 2;
            $notific['fecha_hora'] = $date.' '.$this->input->post('cronoHora');
          //$notific['url'] = base_url().'index.php/bodyshop/ver_proyecto/'.$this->input->post('idp');
            $notific['estadoWeb'] = 1;
            $this->db->insert(CONST_BASE_PRINCIPAL.'noti_user',$notific);
          }
          //$this->ProgramarNotificacion($this->input->post('comentario'),$date,$this->input->post('cronoHora'));
        }
        //Actualizar la variable reprogramado a 1 para saber que lo reprogramaron para otra  fecha

        $this->db->set('fecha_programacion',date2sql($this->input->post('cronoFecha')))->where('idproactivo',$id)->update('proactivo_inicial');
        //Actualizar intentos

        if($this->input->post('comentario')!=''){
          $this->UpdateIntentos($id);
          $array_historial_km = array('idproactivo'=>$id,
                                             'created_at'=>date('Y-m-d H:i:s'),
                                             'id_usuario' => $this->session->userdata('id_usuario'),
                                             'comentario'=>$this->input->post('comentario'),
                                             'fecha_contacto'=>date2sql($this->input->post('fecha')),
          );
          $this->db->insert('proactivo_inicial_historial',$array_historial_km);
        }
        if($this->input->post('no_contactar')!=NULL){
          //Actualizar automáticamente 6 meses después
          $date = date2sql($this->input->post('fecha'));
          $newdate = strtotime ( '+6 month' , strtotime ( $date ) ) ;
          $date = date ( 'Y-m-d' , $newdate );
          $dia = date("D", strtotime($date)); 
          //Si el día es sábado agregar un día, si es domingo 2

          if($dia=='Sat'){
            $newdate = strtotime ( '+2 day' , strtotime ( $date ) ) ;
            $date = date ( 'Y-m-d' , $newdate );
          }

          if($dia=='Sun'){
            $newdate = strtotime ( '+1 day' , strtotime ( $date ) ) ;
            $date = date ( 'Y-m-d' , $newdate );
          }
          $this->db->where('idproactivo',$id)->set('noContactar',1)->set('fecha_programacion',$date)->set('intentos',0)->update('proactivo_inicial');
        }

        echo 1;exit();
    }
    public function getComentariosReagendar_magic($id_cita=''){
      return $this->db->where('id_cita',$id_cita)->get('historial_proactivo_inicial')->result();

    }

    //Actualizar los intentos
    public function UpdateIntentos($id=''){
	    $q = $this->db->where('idproactivo',$id)->select('intentos')->from('proactivo_inicial')->get();
	    if($q->num_rows()==1){
	      $intentos =  $q->row()->intentos;
	      $this->db->where('idproactivo',$id)->set('intentos',$intentos+1)->update('proactivo_inicial');
	      //Enviar correo ya cuando son mas de 5 intentos
	      if(($intentos+1)>=3){
          //Guardar en el historial > 5 intentos
          $array_historial_intentos = array('idproactivo'=>$id,
                                             'created_at'=>date('Y-m-d H:i:s'),
                                             'id_usuario' => $this->session->userdata('id_usuario'),
          );
          $this->db->insert('historial_proactivo_3_intentos',$array_historial_intentos);
	      	$datos['informacion'] = $this->getDataProactivo($id);
	      	$cuerpo = $this->blade->render('correo_intentos',$datos,true);
        	enviar_correo(strtolower($datos['informacion']->correo),"¡Notificación Contacto Proactivo!",$cuerpo,array());
	      }
	    }
    }
    public function getDataProactivo($id=''){
    	return $this->db->where('id',$id)->get('ordenes_old')->row();
    }

    public function getDataByDate($date=''){
    return $this->db->where('fecha_programacion',$date)
                  ->select('pm.idproactivo as id,pm.intentos,pm.fecha_programacion,m.placas,m.kilometraje as km_actual,m.nombre,m.colonia,m.cp,m.celular as telefono,m.correo,m.serie,m.modelo as anio,m.auto')
                  ->join('ordenes_old m','pm.idproactivo = m.id')
                  ->where('intentos <',3)
                  ->get('proactivo_inicial pm')
                  ->result();
    }
    //Obtener lo que ya se ha generado para hacer el proactivo
    function getProactivoMes(){
      return $this->db->get('proactivo_mes')->result();
    }

    //Validar si ya está generado el contacto por mes
    function validaContactByMesYear($mes='',$anio=''){
      $q = $this->db->where('mes',$mes)->where('anio',$anio)->get('proactivo_mes');
      if ($q->num_rows()==1) {
        return false;
      }else{
        return true;
      }
    }
    public function getNameMes($mes=''){
    switch ($mes) {
      case '01':
        return 'Enero';
      break;
      case '02':
        return 'Febrero';
      break;
      case '03':
        return 'Marzo';
      break;
      case '04':
        return 'Abril';
      break;
      case '05':
        return 'Mayo';
      break;
      case '06':
        return 'Junio';
      break;
      case '07':
        return 'Julio';
      break;
      case '08':
        return 'Agosto';
      break;
      case '09':
        return 'Septiembre';
      break;
      case '10':
        return 'Octubre';
      break;
      case '11':
        return 'Noviembre';
      break;
      case '12':
        return 'Diciembre';
      break;
      default:
        return '';
        break;
    }
  }
  //Validar que no haya ido en el mes anterior
  public function validarSerieMes($serie=''){
    //Fecha del mes anterior
    $fecha_old = strtotime ( '-1 month' , strtotime (date('Y-m-d')) ) ;
    $month_before = date ( 'm' , $fecha_old );
    $year_before = date ( 'Y' , $fecha_old );
    $fecha_desde = $year_before.'-'.$month_before.'-01';
    //Fecha hasta
    $fecha_hasta = date('Y-m').'-31';
    $q = $this->db->where('c.vehiculo_numero_serie',$serie)->where('o.fecha_recepcion >=',$fecha_desde)->where('o.fecha_recepcion <=',$fecha_hasta)->join('ordenservicio o','o.id_cita = c.id_cita')->get('citas c');
    if($q->num_rows()>0){
      return false;
    }else{
      return true;
    }
  }
  public function ProgramarNotificacion($mensaje='',$fecha='',$hora=''){
      $celular = "4271659709"; //celular 
      $sucursal = CONST_BID; // sucursal
      if($hora!=''){
      $fecha = $fecha.' '.$hora;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/proactivo");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,
                  "celular=".$celular."&texto=".$mensaje."&sucursal=".$sucursal."&fecha=".$fecha."");
      // Receive server response ...
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $server_output = curl_exec($ch);
      curl_close ($ch);
      }
    
    }
  
  
  
}
