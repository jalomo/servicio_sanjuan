<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proactivo extends MX_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_proactivo','mp');
    $this->load->model('citas/m_catalogos','mcat');
    $this->load->helper(array('general','dompdf','correo'));
    date_default_timezone_set('America/Mexico_City');
  }
//MAGIC PARA GUARDAR COMENTARIOS
  public function comentarios_proactivo_inicial(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
       if($this->input->post()){
       $this->form_validation->set_rules('comentario', 'comentario', 'trim');
       $this->form_validation->set_rules('cronoFecha', 'fecha', 'trim|required');

       
       if ($this->form_validation->run()){
          $this->mp->saveComentario();
          exit();
          }else{
             $errors = array(
                'comentario' => form_error('comentario'),
                'cronoFecha' => form_error('cronoFecha'),
             );
            echo json_encode($errors); exit();
          }
      }
      $data['id_cita'] = $this->input->get('id_cita');
      $data['fecha'] = date2sql($this->input->get('fecha'));
      $data['cliente'] = $this->input->get('cliente');
      $data['input_comentario'] = form_textarea('comentario',"",'class="form-control" id="comentario" rows="2"');

    $this->blade->render('comentarios',$data);
  }
  public function correo(){

    $cuerpo = $this->blade->render('correo_intentos',array(),true);
    print_r($cuerpo);die();
    enviar_correo(strtolower("albertopitava@gmail.com"),"Notificación Contacto Proactivo!",$cuerpo,array());

  }
  public function correo_cp(){

    $cuerpo = $this->blade->render('correo_contacto_proactivo',array(),true);
    print_r($cuerpo);die();
    //reservacion.servicio@mylsalavilla.mx
    enviar_correo(strtolower("albertopitava@gmail.com"),"Carta previa Contacto Proactivo!",$cuerpo,array());
  }

  public function historial_proactivo(){
    $data['proactivo'] = $this->db->select('m.*,p.fecha_contacto,a.adminNombre,p.fecha_contacto,pm.intentos')->where('id_usuario',$this->session->userdata('id_usuario'))->join('ordenes_old m','p.idproactivo = m.id')->join(CONST_BASE_PRINCIPAL.'admin a','p.id_usuario=a.adminId')->join('proactivo_inicial pm','pm.idproactivo=m.id')->get('proactivo_inicial_historial p')->result();
     $this->blade->render('historial_proactivo',$data);
  }
  public function buscar_historial_proactivo(){
    $fecha_inicio = ($this->input->post('finicio'));
    $fecha_fin = ($this->input->post('ffin'));
    $this->db->where('date(created_at) >=',$fecha_inicio);
    $this->db->where('date(created_at) <=',$fecha_fin);
    $data['proactivo'] = $this->db->select('m.*,p.fecha_contacto,a.adminNombre,p.fecha_contacto,pm.intentos')->where('id_usuario',$this->session->userdata('id_usuario'))->join('ordenes_old m','p.idproactivo = m.id')->join(CONST_BASE_PRINCIPAL.'admin a','p.id_usuario=a.adminId')->join('proactivo_inicial pm','pm.idproactivo=m.id')->get('proactivo_inicial_historial p')->result();
    $this->blade->set_data($data)->render('tabla_historial');
  }
  public function generar_registros_proactivo($busqueda=0)
  {

    $month =$this->input->post('month');
    $year = $this->input->post('year');
    //Fin

    $data_registros = $this->mp->getData6Month($month,0,$year);
    $registros = array();
    foreach ($data_registros as $key => $value) {
      //Si esa serie no esta registrada en una orden del mes anterior al día actual
      if($this->mp->validarSerieMes($value->serie)){
        $registros[$value->serie] = $value;
      }
      
      
    }
    $total_semana = round((count($registros)/4));
    $total_dia = round($total_semana/5);
    $fecha_empiezo = $year.'-'.$month.'-01';
    $contador = 1;
    $datos_proactivo = array();
    foreach ($registros as $key => $value) {

      $dia = date("D", strtotime($fecha_empiezo)); 
      //Si el día es sábado agregar un día, si es domingo 2
      if($dia=='Sat'){
        $newdate = strtotime ( '+2 day' , strtotime ( $fecha_empiezo) ) ;
        $fecha_empiezo = date ( 'Y-m-d' , $newdate );
      }

      if($dia=='Sun'){
        $newdate = strtotime ( '+1 day' , strtotime ( $fecha_empiezo) ) ;
        $fecha_empiezo = date ( 'Y-m-d' , $newdate );
      }

      if($contador<=$total_dia){
        $datos = array('idproactivo' => $value->id, 
                     'intentos' => 0,
                     'fecha_programacion' => $fecha_empiezo,
        );
        $fecha_programacion = $fecha_empiezo;
      }else{
        $datos = array('idproactivo' => $value->id, 
                     'intentos' => 0,
                     'fecha_programacion' => $fecha_empiezo,
        );
        $newdate = strtotime ( '+1 day' , strtotime ( $fecha_empiezo) ) ;
        $fecha_empiezo = date ( 'Y-m-d' , $newdate );
        $contador = 1;
      }

      $contador ++;
      $this->db->insert('proactivo_inicial',$datos);

    }
    $datos_proactivo = array('mes' =>$month, 
                            'anio' =>$year, 
                            'idusuario'=>$this->session->userdata('id_usuario'),
                            'created_at'=>date('Y-m-d H:i:s')
    );
    //Insertar que ya se hizo el del mes
    $this->db->insert('proactivo_mes',$datos_proactivo);
    echo 1;exit();
    
  }

  public function index($busqueda=0){
    //Si búsqueda es 1 regresar la tabla
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }

    if($this->input->post('fecha')==''){
      $date = date('Y-m-d');
      $newdate = strtotime ( '-6 month' , strtotime ( $date ) ) ;
      $month = date ( 'm' , $newdate );
      $mes_completo = date ( 'M' , $newdate );
      $datos['fecha_actual'] = date ( 'Y-m-d' , $newdate );
    }else{
      $date = date2sql($this->input->post('fecha'));
      $date = strtotime($date);
      $month = date ( 'm' , $date );
      $mes_completo = date ( 'M' , $date );
      $datos['fecha_actual'] = date ( 'Y-m-d' , $date );
    }


    $datos['registros'] =$this->mp->getDataByDate($datos['fecha_actual']);
    //echo $this->db->last_query();die();
    //debug_var($datos['registros']);die();
    if(!$busqueda){
      $this->blade->set_data($datos)->render('lista_proactivos');
    }else{
      $this->blade->set_data($datos)->render('tabla_proactivo');
    }
  }
  public function generar_mes(){
    $date = date('Y-m-d');
    $newdate = strtotime ( '-6 month' , strtotime ( $date ) ) ;
    $data['month'] = date ( 'm' , $newdate );
    $data['year'] = date ( 'Y' , $newdate );
    $data['nameMes'] = $this->mp->getNameMes($data['month']);
    $data['validar'] = $this->mp->validaContactByMesYear($data['month'],$data['year']);
    //var_dump($month,$year);die();
    $data['registros'] = $this->mp->getProactivoMes();
    $this->blade->set_data($data)->render('proactivo_por_mes');
  }
  //MAGIC PARA GUARDAR COMENTARIOS
  public function updatePhones(){
    //print_r($_POST);die();
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
       if($this->input->post()){
       $this->form_validation->set_rules('telefono_casa', 'teléfono de casa', 'trim|numeric|exact_length[10]');
       $this->form_validation->set_rules('telefono_oficina', 'teléfono de oficina', 'trim|numeric|exact_length[10]');
       $this->form_validation->set_rules('anio', 'año', 'trim|numeric|exact_length[4]');

       
       if ($this->form_validation->run()){
          $datos = array('telefono_oficina' =>$this->input->post('telefono_oficina'),
                          'telefono_casa' =>$this->input->post('telefono_casa'),
                          'anio' =>$this->input->post('anio'),

          );
          $this->db->where('id',$this->input->post('idmagic'))->update('magic',$datos);
          echo 1;exit();
          exit();
          }else{
             $errors = array(
                'telefono_casa' => form_error('telefono_casa'),
                'telefono_oficina' => form_error('telefono_oficina'),
                'anio' => form_error('anio'),
             );
            echo json_encode($errors); exit();
          }
      }
      $info = $this->mp->getDataMagic($this->input->get('idmagic'));
      $data['idmagic'] = $this->input->get('idmagic');
     
      $data['input_telefono_casa'] = form_input('telefono_casa',set_value('telefono_casa',exist_obj($info,'telefono_casa')),'class="form-control" id="telefono_casa" ');

      $data['input_telefono_oficina'] = form_input('telefono_oficina',set_value('telefono_oficina',exist_obj($info,'telefono_oficina')),'class="form-control" id="telefono_oficina" ');

      $data['input_anio'] = form_input('anio',set_value('anio',exist_obj($info,'anio')),'class="form-control" id="anio" maxlength="4" ');

    $this->blade->render('v_phones',$data);
  }
  //Actualizar manualmente los campos de ASC de las campañas
  public function cambiarASC_Magic(){
    $this->db->where('id',$this->input->post('id'))->set($this->input->post('campo'),$this->input->post('valor'))->update('magic');
    echo 1;exit();
  }
  //Obtener por id de ordenes_old
  public function getById($id=0){
     $info= $this->db->where('id',$_POST['id'])->get('ordenes_old')->result();
    if(count($info)==1){
      echo json_encode(array('exito'=>true,'datos'=>$info[0]));
    }else{
      echo json_encode(array('exito'=>false));
    }
  }
  
}
