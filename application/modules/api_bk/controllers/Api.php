<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Api extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('m_api', 'ma');
    $this->load->model('citas/m_catalogos', 'mcat');
    $this->load->model('proceso/m_proceso', 'mp');
    $this->load->model('citas/m_citas', 'mc');
    $this->load->model('principal');
    $this->load->helper(array('correo', 'notificaciones', 'correos_usuario'));
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  public function catalogos()
  {
    /*
    1) cat_transporte
    2) cat_anios
    3) cat_marca
    4) cat_modelo
    5) preventivo_modelos
    6) drop_id_prepiking
    7) operadores (ASESORES)
    8) cat_servicios
    9) cat_colores
    */
    $data = $this->mcat->get($_POST['catalogo']);
    echo json_encode(array('exito' => true, 'data' => $data));
  }
  //Guardar la cita
  public function save()
  {
    $fecha_separada = explode('-', $_POST['fecha']);
    $fecha_anio = isset($fecha_separada[0]) ? $fecha_separada[0] : null;
    $fecha_mes = isset($fecha_separada[1]) ? $fecha_separada[1] : null;
    $fecha_dia = isset($fecha_separada[2]) ? $fecha_separada[2] : null;
    $id_horario_save = isset($_POST['horario']) ? $_POST['horario'] : null;

    //Hacer validación de que no esté ocupado
    if ($id_horario_save != null && $_POST['id'] == 0) {
      $qhorario = $this->db->select('ocupado')->from('aux')->where('id', $id_horario_save)->get();
      if ($qhorario->num_rows() == 1) {
        if ($qhorario->row()->ocupado) {
          echo json_encode(array('exito' => false, 'message' => 'El horario del asesor ya fue ocupado'));
          die();
        }
      }
    }
    //Validar horario del técnico
    if ($_POST['id'] == 0) {
      $qhorario = $this->db->where('id_tecnico', $_POST['id_tecnico'])
        ->where('hora_inicio', $_POST['horario_tecnico'])
        ->where('fecha', $_POST['fecha'])
        ->get('tecnicos_citas');
      if ($qhorario->num_rows() == 1) {
        echo json_encode(array('exito' => false, 'message' => 'El horario del técnico ya fue ocupado'));
        die();
      }
    }
    $datos = array(
      'transporte' => isset($_POST['transporte']) ? $_POST['transporte'] : null,
      'email' => strtolower($_POST['email']),
      'vehiculo_anio' => $_POST['anio'],
      'vehiculo_modelo' => $_POST['modelo'],
      'vehiculo_placas' => $_POST['placas'],
      'vehiculo_numero_serie' => $_POST['numero_serie'],
      'comentarios_servicio' => $_POST['comentarios_servicio'],
      'asesor' => $_POST['asesor'],
      'id_asesor' => isset($_POST['asesor']) ? $this->mc->getIdAsesor($_POST['asesor']) : '',
      'fecha_anio' => $fecha_anio,
      'fecha_mes' => $fecha_mes,
      'fecha_dia' => $fecha_dia,
      'datos_email' => strtolower($_POST['email']),
      'datos_nombres' => $_POST['nombre'],
      'datos_apellido_paterno' => $_POST['apellido_paterno'],
      'datos_apellido_materno' => $_POST['apellido_materno'],
      'datos_telefono' => $_POST['telefono'],
      'id_prepiking' => isset($_POST['id_prepiking']) ? $_POST['id_prepiking'] : '',
      'id_modelo_prepiking' => isset($_POST['id_modelo_prepiking']) ? $_POST['id_modelo_prepiking'] : '',
      'prepiking' => isset($_POST['id_prepiking']) ? $_POST['id_prepiking'] : '',
      'fecha_creacion' => date('Y-m-d'),
      'status' => 0,
      'servicio' => 'MANTENIMIENTO POR KILOMETRAJE',
      'id_horario' => $id_horario_save,
      'id_opcion_cita' => 1,
      'id_color' => $_POST['id_color'],
      'fecha' => $fecha_anio . '-' . $fecha_mes . '-' . $fecha_dia,
      'id_status_color' => 6,
      'fecha_hora' => $fecha_anio . '-' . $fecha_mes . '-' . $fecha_dia . ' ' . $this->mc->getHora(isset($_POST['horario']) ? $_POST['horario'] : null),
      'origen' => 8,
      'reagendada' => 0,
      'historial' => 0,
      'cancelada' => 0,
      'vehiculo_kilometraje' => isset($_POST['kilometraje']) ? $_POST['kilometraje'] : null,
      'cita_previa' => 1,
      'xehos' =>  0
    );

    if ($_POST['id'] == 0) {
      $datos['id_tecnico'] = $this->input->post('id_tecnico');
      $datos['fecha_creacion_all'] = date('Y-m-d H:i:s');
      $datos['id_usuario'] = 100;
      $datos['id_status'] = 1;
      $this->db->insert('citas', $datos);
      $id_cita = $this->db->insert_id();
      $horarios_tecnicos = array(
        'fecha' => $this->input->post('fecha'),
        'hora_inicio' => $this->input->post('horario_tecnico'),
        'hora_fin' => date('H:i:s', strtotime('+1 hour', strtotime($this->input->post('horario_tecnico')))),
        'id_cita' => $id_cita,
        'id_tecnico' => $this->input->post('id_tecnico'),
        'dia_completo' => 0,
        'fecha_fin' => $this->input->post('fecha'),
        'hora_inicio_dia' => $this->input->post('horario_tecnico')
      );
      $this->db->insert('tecnicos_citas', $horarios_tecnicos);
      $this->db->where('id', $_POST['horario'])->set('ocupado', 1)->update('aux');
      echo json_encode(array('exito' => true, 'data' => $datos, 'message' => 'Registro guardado correctamente'));
    } else {
      $this->db->where('id', $_POST['id'])->update('citas', $datos);
      echo json_encode(array('exito' => true, 'data' => $datos, 'message' => 'Registro actualizado correctamente'));
    }
  }
  //Obtener datos por placa
  public function getByPlaca()
  {
    $q = $this->db->where('vehiculo_placas', $this->input->post('placas'))->limit(1)->get('citas');
    if ($q->num_rows() == 1) {
      echo json_encode(array('exito' => true, 'data' => $q->row()));
    } else {
      echo json_encode(array('exito' => false));
    }
    exit();
  }
  //Obtener datos por serie
  public function getBySerie()
  {
    $q = $this->db->where('vehiculo_numero_serie', $this->input->post('serie'))->limit(1)->get('citas');
    if ($q->num_rows() == 1) {
      echo json_encode(array('exito' => true, 'data' => $q->row()));
    } else {
      echo json_encode(array('exito' => false));
    }
    exit();
  }
  // Obtener los técnicos
  public function getTecnicos()
  {
    $q = $this->db->where('activo', 1)->where('baja_definitiva', 0)->where('api', 1)->limit(1)->get('tecnicos');
    if ($q->num_rows() == 1) {
      echo json_encode(array('exito' => true, 'data' => $q->row()));
    }
  }
  //Obtener horarios de los técnicos 
  //Parámetros fecha y técnico
  public function getHorariosTecnicos()
  {
    $fecha = $_POST['fecha'];
    $id_tecnico = $_POST['id_tecnico'];
    $horarios = $this->db->select('hora_inicio_dia')->where('id_tecnico', $id_tecnico)->where('fecha', $fecha)->get('tecnicos_citas')->result();
    $tiempo_actual = date('H:i');
    $tiempo = '09:00';
    $horarios_ocupados = [];
    foreach ($horarios as $h => $horario) {
      $horarios_ocupados[] = $horario->hora_inicio_dia;
    }
    while ($tiempo <= '20:00') {
      $NuevoTiempo = strtotime('+1 hour', strtotime($tiempo));
      $tiempo = date('H:i:s', $NuevoTiempo);
      if (!in_array($tiempo, $horarios_ocupados)) {
        if ($fecha == date('Y-m-d')) {
          if ($tiempo >= $tiempo_actual) {
            $array_tiempo[] = $tiempo;
          }
        } else {
          $array_tiempo[] = $tiempo;
        }
      }
    }
    echo json_encode(array('exito' => true, 'data' => $array_tiempo));
    exit();
  }
  //Obtener fechas de los asesores
  //Recibe el nombre del asesor
  public function getfechasAsesores()
  {
    if ($this->input->post()) {
      $id_asesor = $this->mc->getIdAsesor($this->input->post("asesor"));
      $fechas = $this->mc->getFechasAsesor($id_asesor);
      echo json_encode($fechas);
    } else {
      echo 'Nada';
    }
  }
  /*
  Obtener los horarios de un asesor con una fecha
  Parámetros : asesor, id_horario (si se edita), fecha
  */
  public function getHorariosAsesor()
  {
    if ($this->input->post()) {
      $id_asesor = $this->mc->getIdAsesor($this->input->post("asesor"));
      $id_horario = $this->input->post("id_horario");
      $fecha = $this->input->post('fecha');
      //esta validación la puse por que si está editando debe ir por los horarios que no están ocupados y además el que ya tenía registrado
      if ($id_horario == 0) {
        $horarios = $this->mc->getHorariosAsesor($id_asesor, $fecha);
      } else {
        $horarios = $this->mc->getHorariosAsesor_edit($id_asesor, $fecha, $id_horario);
      }
      echo json_encode($horarios);
    } else {
      echo 'Nada';
    }
  }
  //Mis citas
  public function citasUsuario()
  {
    $citas = $this->db->where('datos_email', $_POST['email'])->get('citas')->result();
    echo json_encode(array('exito' => true, 'data' => $citas));
  }
  public function getAllTecnicos()
  {
    $tecnicos = $this->db->where('activo', 1)->where('baja_definitiva', 0)->get('tecnicos')->result();
    echo json_encode(array('exito' => true, 'data' => $tecnicos));
  }
  public function sms_general($celular = '', $mensaje = '')
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,CONST_URL_NOTIFICACION_APP);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt(
      $ch,
      CURLOPT_POSTFIELDS,
      "celular=" . $celular . "&mensaje=" . $mensaje . "&sucursal=" . CONST_BID . ""
    );
    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);
    //var_dump($server_output);
    curl_close($ch);
  }
  public function checkIn()
  {
    if (!$this->input->post()) {
      echo json_encode(array('exito' => false, 'message' => 'Es necesario ingresar la cita y horario'));
      exit();
    }
    $id_horario = $this->principal->getGeneric('id_cita', $_POST['id_cita'], 'citas', 'id_horario');
    $cita = $this->mc->getCitaId($_POST['id_cita']);
    if(count($cita)>0){
      $cita = $cita[0];
      $cliente = $cita->datos_nombres.' '.$cita->datos_apellido_paterno.' '.$cita->datos_apellido_materno;
      $mensaje = "Check-in efectivo Cliente $cliente placas $cita->vehiculo_placas con cita Hora $cita->fecha_hora ya esta disponible para ser recibido.";
      $tel_asesor = $this->principal->getGeneric('nombre',$cita->asesor,'operadores')->telefono;
      $this->sms_general($tel_asesor,$mensaje);
    }
    $this->db->where('id', $id_horario)->set('id_status_cita', $_POST['id_status'])->update('aux');

    //Insertar línea tiempo
    $inicio = $this->mc->getFechaCreacion($_POST['id_cita']);
    $datos_transiciones = array(
      'status_anterior' => $this->mc->getLastStatusTransiciones($_POST['id_cita']),
      'status_nuevo' => 'Llegó',
      'fecha_creacion' => date('Y-m-d H:i:s'),
      'tipo' => 1,
      'inicio' => $inicio,
      'fin' => date('Y-m-d H:i:s'),
      'usuario' => 'Cliente',
      'id_cita' => $_POST['id_cita'],
      'mostrar_linea' => 0,
      'general' => 1,
      'minutos_transcurridos' => dateDiffMinutes($inicio, date('Y-m-d H:i:s'))
    );
    $this->db->insert('transiciones_estatus', $datos_transiciones);
    echo json_encode(array('exito' => true, 'message' => 'Registro actualizado correctamente'));
  }
  public function horaCheckIn()
  {
    $q = $this->db->where('status_nuevo', 'Llegó')->where('id_cita', $_POST['id_cita'])->order_by('id', 'asc')->limit(1)->get('transiciones_estatus');

    if ($q->num_rows() == 1) {
      echo json_encode(array('exito' => true, 'fecha' => $q->row()->fecha_creacion));
      exit();
    }
    echo json_encode(array('exito' => false, 'message' => 'No se ha hecho el check-in'));
  }
  public function getHoraCita()
  {
    $q = $this->db->select('a.fecha,a.hora')
      ->join('citas c', 'c.id_horario=a.id')
      ->where('c.id_cita', $_POST['id_cita'])
      ->limit(1)
      ->get('aux a');
    if ($q->num_rows() == 1) {
      echo json_encode(array('exito' => true, 'fecha' => $q->row()->fecha . ' ' . $q->row()->hora));
      return;
    }
    echo json_encode(array('exito' => false, 'message' => 'Error en la petición'));
  }
  public function enviar_cotizacion_multipunto($id_cita = '')
  {
    $idorden = $this->mc->getIdOrdenByCita($id_cita);
    $correo_consumidor = $this->mc->correo_consumidor($id_cita);
    $correo_compania = $this->mc->correo_compania($id_cita);

    if (!$this->mc->firmoAsesor($id_cita)) {
      echo json_encode(array('exito' => false, 'message' => 'El asesor no ha firmado el presupuesto'));
      exit();
    }
    if ($correo_consumidor != $correo_compania) {
      if ($correo_consumidor != '') {
        correo_presupuesto($correo_consumidor, encrypt($id_cita));
      }
      if ($correo_compania != '') {
        correo_presupuesto($correo_compania, encrypt($id_cita));
      }
    } else {
      correo_presupuesto($correo_consumidor, encrypt($id_cita));
    }

    //Actualizar que ya se envió el sms de esa orden
    $datos_mensaje_enviado = array('tipo_mensaje' => 'email_cotizacion_multipunto', 'idorden' => $idorden, 'enviado' => 1);
    $datos_mensaje_enviado['created_at'] = date('Y-m-d H:i:s');
    $this->db->insert('mensajes_enviados_orden', $datos_mensaje_enviado);
    echo json_encode(array('exito' => true, 'message' => 'El correo se envió correctamente'));
  }
  //obtener información de una operación
  public function getDataOperacion($folio)
  {
    $datos = $this->db->select('a.id,a.descripcion, a.id_status,a.id_tecnico,a.principal,a.folio_asociado,t.inicio,t.fin,t.usuario,t.minutos_transcurridos,t.status_anterior,t.status_nuevo')
      ->join('transiciones_estatus t', 'a.id = t.id_operacion', 'left')
      //->where('folio_asociado',$_POST['folio'])
      ->where('folio_asociado', $folio)
      //->where('mostrar_linea',1)
      ->get('articulos_orden a')
      ->result();
    echo json_encode(array('exito' => true, 'data' => $datos));
  }
  //Datos de cita
  public function getDatosCita()
  {
    if (!isset($_POST['id_cita'])) {
      echo json_encode(array('exito' => false, 'message' => 'Es necesario ingresar el #cita'));
      return;
    }
    $id_cita = $_POST['id_cita'];
    $cita = $this->mc->getCitaId($id_cita);
    echo json_encode(array('exito' => true, 'data' => $cita[0]));
  }
  public function eliminar_cita()
  {
    if (!isset($_POST['id_cita'])) {
      echo json_encode(array('exito' => false, 'message' => 'Es necesario ingresar el #cita'));
      return;
    }
    $id_horario = $this->mc->getIdHorarioActual($this->input->post('id_cita'));
    if ($this->db->where('id_cita', $this->input->post('id_cita'))->delete('citas')) {
      $this->db->where('id', $id_horario)->set('ocupado', 0)->update('aux');
      $this->db->where('id_cita', $this->input->post('id_cita'))->delete('tecnicos_citas');
      $this->db->where('id_cita', $this->input->post('id_cita'))->delete('tecnicos_citas_historial');
      echo json_encode(array('exito' => true, 'message' => 'Cita eliminada correctamente'));
      exit();
    } else {
      echo json_encode(array('exito' => false, 'message' => 'Error al eliminar la cita'));
      exit();
    }
  }
  public function getEvidencia($id_cita=''){
    if ($id_cita == '') {
      echo json_encode(array('exito' => false, 'message' => 'Es necesario ingresar el #cita'));
      return;
    }
    $data['evidencia'] = $this->mp->getEvidencia($id_cita);
    $data['evidencia_multipunto'] = $this->mp->evidencia_multipunto($id_cita);
    echo json_encode(array('exito' => true, 'data' => $data));
  }
  public function get_count_orders_year($year=''){
    $query = " SELECT m.month as numero_mes,
    COUNT(fecha_recepcion) as cantidad
    FROM (SELECT 1 AS month UNION ALL SELECT 2 UNION ALL SELECT 3
      UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6
      UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9
      UNION ALL SELECT 10 UNION ALL SELECT 11 UNION ALL SELECT 12) m
    left JOIN ordenservicio  ON  m.month = MONTH(fecha_recepcion) and year(fecha_recepcion) = ".$year."
    GROUP BY m.month
    ";
    $datos = $this->db->query($query)->result();
    echo json_encode($datos);
  }
}
