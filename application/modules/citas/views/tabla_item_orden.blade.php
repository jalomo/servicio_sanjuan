<form action="" id="frm-total-orden">
    <input type="hidden" name="id_orden_save" id="id_orden_save" value="{{ $id_orden_save }}">
    <input type="hidden" name="total_temp_orden" id="total_temp_orden" value="">

    <input type="hidden" name="tipo" id="tipo" value="{{ $tipo }}">
    <table class="table">
        <thead>
            <th>Acciones</th>
            <th>Descripción</th>
            <th>Precio</th>
            <th>Cantidad</th>
            <th>Descuento</th>
            <th>Precio incluye IVA</th>
            <th>Total</th>
        </thead>
        <tbody>
            <?php $subtotal = 0; ?>
            <?php $iva = 0; ?>
            @if (count($items) > 0)
                @foreach ($items as $i => $item)
                    <?php $subtotal = $subtotal + $item->total; ?>
                    <tr>
                        <td>
                            @if ($item->operacion == '')
                                <?php (float) ($iva = $iva + (float) $item->total * 0.16); ?>
                                <a href="#" data-iditem="{{ $item->id }}" class="pe pe-7s-trash eliminar_item"
                                    aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Eliminar"></a>
                            @endif
                        </td>
                        <td>{{ $item->descripcion }}</td>
                        <td>{{ number_format($item->precio_unitario, 2) }}</td>
                        <td>{{ number_format($item->cantidad, 2) }}</td>
                        <td>{{ number_format($item->descuento, 2) }}</td>
                        <td>{{ $item->operacion != '' ? 'Si' : 'No' }}</td>
                        <td>{{ number_format($item->total, 2) }}</td>
                    </tr>
                @endforeach
                <?php (float) ($total = (float) $subtotal + (float) $iva); ?>

                @if ($tipo == 1)
                    <?php (float) ($subtotal = (float) $subtotal / 1.16); ?>
                @endif
        </tbody>
        <tfoot>
            <tr class="text-right">
                <td colspan="6">
                    <span>I.V.A. :</span>
                </td>
                <td width="15%">
                    @if ($item->operacion == '')
                        <input type="text" name="iva_orden" id="iva_orden" value="{{ number_format($iva, 2) }}"
                            class="form-control" readonly="">
                    @else
                        <input type="text" name="iva_orden" id="iva_orden"
                            value="{{ number_format($total - $subtotal, 2) }}" class="form-control" readonly="">
                    @endif
                </td>
            </tr>
            <tr class="text-right">
                <td colspan="6">
                    <span>Subtotal:</span>
                </td>
                <td width="15%">
                    <input type="text" name="subtotal_orden" id="subtotal_orden" class="form-control" readonly=""
                        value="{{ number_format($subtotal, 2) }}">
                </td>
            </tr>
            <!--<tr class="text-right">
          <td colspan="6">
            <span>Anticipo:</span><br>
          </td>
          <td width="15%">
            <input type="number" name="anticipo_orden" id="anticipo_orden" value="{{ number_format($anticipo, 2) }}" class="form-control anticipo">
          </td>
        </tr>-->
            <tr class="text-right">
                <td colspan="6">
                    <span>Total</span>
                </td>
                <td width="15%">
                    <input type="text" name="total_orden" id="total_orden" value="{{ $total }}"
                        class="form-control" readonly="">
                </td>
            </tr>
        </tfoot>
    @else
        <tr class="text-center">
            <td colspan="7">No hay registros para mostrar...</td>
        </tr>
        @endif
    </table>
</form>
<script>
    $("#total_temp_orden").val($("#total_orden").val());
    $(".anticipo").on('change', function() {
        if ($(this).val() == '' || parseFloat($(this).val()) < 0) {
            var anticipo = 0;
            $(this).val(0)
        } else {
            var anticipo = parseFloat($(this).val());
        }
        $("#total_orden").val(total - anticipo);

        var total = parseFloat($("#total_temp_orden").val());
        if (anticipo < 0 || anticipo == '') {
            anticipo = 0;
        }
        if (anticipo > total) {
            ErrorCustom("El anticipo no puede ser mayor al total");

        } else {
            $("#total_orden").val(total - anticipo);
        }


    })
    $(".anticipo").trigger('change');

</script>
