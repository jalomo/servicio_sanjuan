<form action="" method="POST" id="frm_horas_tecnicos">
  <input type="hidden" id="id_detalle" name="id_detalle" value="{{$input_id_detalle_horarios}}">
  <input type="hidden" id="input_id_tecnico" name="input_id_tecnico" value="{{$input_id_tecnico}}">
  <input type="hidden" id="tabla" name="tabla" value="{{$tabla}}">
  <input type="hidden" id="id_tecnico_seleccionado" name="id_tecnico_seleccionado" value="{{$id_tecnico_seleccionado}}">
  <input type="hidden" id="dia_completo" name="dia_completo" value="{{$dia_completo}}">
  <input type="hidden" id="id_orden_horario" name="id_orden_horario" value="{{$idorden}}">
  @if($dia_completo)
  <div id="div_completo">
    <div class="row">
      <div class="col-sm-3">
        <label for="">Técnico</label>
        {{$drop_tecnicos}}
        <span class="error_id_tecnico"></span>
      </div>
      <div class='col-sm-3'>
        <label for="">Fecha inicio</label>
        {{$input_fecha_inicio}}
        <span class="error_fecha_inicio"></span>
      </div>
      <div class='col-sm-3'>
        <label for="">Fecha Fin</label>
        {{$input_fecha_fin}}
        <span class="error_fecha_fin"></span>
      </div>
      <div class="col-sm-3">
        <label for="">Hora de inicio del día</label>
        {{$input_hora_comienzo}}
        <span class="error error_hora_comienzo"></span>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-4">
        <label>Día extra</label>
        {{$input_fecha_parcial}}
      </div>
      <div class="col-sm-4">
        <label for="">Hora inicio trabajo</label>
        {{$input_hora_inicio_extra}}
        <span class="error_hora_inicio_extra"></span>
      </div>
      <div class="col-sm-4">
        <label for="">Hora fin trabajo</label>
        {{$input_hora_fin_extra}}
        <span class="error_hora_fin_extra"></span>
        <br>
        <!--button id="guardar" class="btn btn-success pull-right">Guardar</button>-->
      </div>
    </div>
  </div> <!-- completos -->
  @else
  <div id="div_incompleto" class="row">
    <div class="col-sm-3">
      <label for="">Técnico</label>
      {{$drop_tecnicos}}
      <span class="error_id_tecnico"></span>
    </div>
    <div class='col-sm-3'>
      <label for="">Fecha</label>
      {{$input_fecha_reasignar}}
      <span class="error error_fecha_reasignar"></span>
    </div>
    <div class="col-sm-3">
      <label for="">Hora inicio trabajo</label>
      {{$input_hora_inicio}}
      <span class="error_hora_inicio"></span>
    </div>
    <div class="col-sm-3">
      <label for="">Hora fin trabajo</label>
      {{$input_hora_fin}}
      <span class="error_hora_fin"></span>
      <br>
      <!--button id="guardar" class="btn btn-success pull-right">Guardar</button>-->
    </div>
  </div> <!-- 0 -->
  @endif
</form>
<div class="row">
  <div class="col-sm-12 text-right">
    <span class="js_ver_citas" style="cursor: pointer;">Ver citas asignadas</span>
  </div>
</div>
<script type="text/javascript">
  $('.clockpicker').clockpicker();
  var dia_completo = "{{$dia_completo}}";
    var fecha_actual = "{{ date('Y-m-d') }}";
    $("#dia_completo").on('click',function(){
    if($(this).prop('checked')){
     $("#div_completo").show('slow');
     $("#div_incompleto").hide('slow');
     $("#dia_completo").val(1);
    }else{
      $("#div_completo").hide('slow');
      $("#div_incompleto").show('slow');
      $("#dia_completo").val(0);
    }
  });
    if(dia_completo==1){
      $("#div_completo").show('slow');
      $("#div_incompleto").hide('slow');
      $("#dia_completo").val(1);
    }else{
     $("#div_completo").hide('slow');
      $("#div_incompleto").show('slow');
      $("#dia_completo").val(0);
    }
    $('.date').datetimepicker({
      format: 'DD/MM/YYYY',
      icons: {
          time: "far fa-clock",
          date: "fa fa-calendar",
          up: "fa fa-arrow-up",
          down: "fa fa-arrow-down"
      },
       locale: 'es'
    });
  $('.clockpicker_inicio').clockpicker({
            afterDone: function() {
                 validar_fecha_inicio();
            },
    });
    $('.clockpicker_fin').clockpicker({
            afterDone: function() {
                validar_fin();
            },
    });
    $('.clockpicker_inicio').on('change',function(){
         validar_fecha_inicio();
    });
    $('.clockpicker_fin').on('change',validar_fin);
  function validar_fecha_inicio(){
     var dt = new Date();
      if(parseInt(dt.getMinutes())<10){
        var time = dt.getHours() + ":0" + dt.getMinutes();
      }else{
          var time = dt.getHours() + ":" + dt.getMinutes();
      }

      var hora_inicio = $("#hora_inicio_extra").val();
      var hora_fin = $("#hora_inicio_extra").val();

     
      if(fecha_actual==$("#fecha_parcial").val()){
           if(hora_inicio < time){
          $("#hora_inicio_extra").val(time);
        }
        var hora_inicio = $("#hora_inicio_extra").val();
        $("#hora_fin_extra").val(hora_inicio);
      }else{
        if(hora_fin<hora_inicio){
          $("#hora_fin_extra").val(hora_inicio)
        }
      } 
  }

</script>