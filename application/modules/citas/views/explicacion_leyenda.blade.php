<div class="row">
	<div class="col-sm-8">
		<div class="row">
			<div class="col-sm-12">
				<strong for="">1 Tiempo de entrega: </strong> indica un estatus temporal de la entrega de unidad
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<strong for="">2 Fase de servicio: </strong> representa la fase o proceso en que se encuentra la unidad dentro del servicio
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<strong for="">3 Tipo de servicio: </strong> tipo de servicio asignado a la unidad
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<strong for="">4 Número de cita: </strong> número de cita a la que está relacionada la unidad
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<strong for="">5 Estatus de Carryover: </strong> cuando el borde de la cita es de color blanco se trata de una cita normal, en cambio, cuando es de color negro, se trata de un carryover.
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="row">
			<div class="col-sm-12">
				<img id="explicacion" class="pull-right"  src="img/tabtec03.png" alt="" style="width: 120px;height: 120px;margin: 0 auto;">
			</div>
		</div>
	</div>
</div>