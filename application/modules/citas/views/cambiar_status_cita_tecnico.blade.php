@if($hizo_checkin==1)
	<input type="hidden" name="id_cita_save" id="id_cita_save" value="{{$id_cita_save}}">
	<input type="hidden" name="id_status_actual" id="id_status_actual" value="{{$id_status_actual}}">
		@if($existe_orden || $id_estatus_asesor==4)
			@if($id_estatus_asesor!=4 || $id_status_actual==1)
			<input type="hidden" name="islavado" id="islavado" value="{{$islavado}}">
			<input type="hidden" name="ubicacion_unidad" id="ubicacion_unidad" value="{{$ubicacion_unidad}}">
			<input type="hidden" name="estatus_detenido" id="estatus_detenido" value="{{$es_estatus_detenido}}">
			<!-- NUEVA VERSIÓN -->
			
			
			<div class="row">
				<div class="col-sm-12">
					<label for="">Estatus:</label>
					{{$drop_status}}
					<span class="error error_id_status"></span>
				</div>
			</div>
			<br>
			<div id="div_motivo" class="{{($es_estatus_detenido==0)?'esconder_div':''}}">
				<div class="row">
					<div class="col-sm-12">
						<label for="">Motivo detención:</label>
						<select type="text" name="motivo_detencion" id="motivo_detencion" class="form-control">
							<option selected value="">Selecciona una opción</option>
							<option value = 'R'> Falta de Refacciones </option>
							<option value = 'A'> Autorización del Cliente </option>
							<option value = 'G'> Detenido por Gerencia </option>
							<option value = 'S'> Subcontratados </option>
							<option value = 'C'> Control Calidad </option>
							<option value = 'O'> Otro</option>
						</select>
						<span class="error error_motivo_detencion"></span>
					</div>
					<br>
				</div>
			</div>
			<div id="div_ubicacion" class="row">
				<div class="col-sm-12">
					<label for="">Ubicación:</label> <br>
					@if($ubicacion_unidad=='en tránsito')
						<input class="js_ubicacion" checked="true" type="radio" name="ubicacion" value="en tránsito"> En tránsito 
					@else
						<input class="js_ubicacion" type="radio" name="ubicacion" value="en tránsito"> En tránsito 
					@endif

					@if($ubicacion_unidad=='en taller')
						<input class="js_ubicacion" checked="true" type="radio" name="ubicacion" value="en taller"> En 
						taller
					@else
						<input class="js_ubicacion" type="radio" name="ubicacion" value="en taller"> En taller
					@endif
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-12">
					<label for="">Tipo de comentario:</label>
					{{$drop_tipo_comentario}}
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-sm-12">
					<label for="">Comentarios:</label>
					{{$input_comentarios}}
				</div>
			</div>
			<a id="ver_comentarios" href="" class="pull-right ver_comentarios">Ver todos los comentarios</a>
			<a id="ver_cotizaciones" href="#" class="">Ver cotizaciones</a>
			@else
				<div class="alert alert-info" role="alert">
					El asesor determinó que el cliente no llégo
				</div>
				<div class="row">
					<div class="col-sm-12">
						<label for="">Estatus:</label>
						{{$drop_status}}
						<span class="error error_id_status"></span>
					</div>
				</div>
			@endif
		@else
		<div class="alert alert-info" role="alert">
			Para cambiar un estatus es necesario que exista la orden
		</div>
		@endif
@else
	<div class="alert alert-info" role="alert">
 		Es necesario que el asesor haga el check-in.
	</div>
@endif

<script>
	var hizo_checkin = "{{$hizo_checkin}}";
	var id_status_actual = "{{$id_status_actual}}";
	var es_estatus_detenido = "{{$es_estatus_detenido}}"
	var motivo_detencion = "{{$motivo_detencion}}"
	var terminar_dias_previos = "{{$terminar_dias_previos}}"
	var dia_actual = "{{$dia_actual}}"
	var se_trabajo = "{{$se_trabajo}}";
	//Si el estatus es detenido es por que previamente tenía un motivo (esto es para guardarse el tiempo que tardó en ese estatus de detenido)
	if(es_estatus_detenido && motivo_detencion!=''){
		$("#motivo_detencion").val(motivo_detencion);
		$("#motivo_detencion option:not(:selected)").attr("disabled", true);
	}
	if(id_status_actual==7){
		$("#div_ubicacion").removeClass('d-none');
	}else{
		$("#div_ubicacion").addClass('d-none');
	}
	if(hizo_checkin==0){
		$(".modal-footer .btn-primary").remove();
	}
	//Días previos sólo terminar en ciertos estatus
	if(!dia_actual &&  terminar_dias_previos==1){
		$("#id_status").val(3);
		$("#id_status option:not(:selected)").attr("disabled", true);
	}
	$("#id_status").on('change',function(){
		//Entrega de refacciones
		if($(this).val()==7){
			$("#div_ubicacion").removeClass('d-none');
		}else{
			$("#div_ubicacion").addClass('d-none');
		}
		ajaxJson(site_url+"/layout/getStatusDetenido",{"id_status":$(this).val()},"POST","",function(result){
			if(result==1){
				$("#div_motivo").addClass('mostrar_div');
				$("#div_motivo").removeClass('esconder_div');
				$("#motivo_detencion").val('');
				$("#motivo_detencion option").attr("disabled", false);
			}else{
				$("#div_motivo").removeClass('mostrar_div');
				$("#div_motivo").addClass('esconder_div');
			}
			$("#estatus_detenido").val(result);
		});
	})
	$(".js_ubicacion").on('click',function(){
		$("#ubicacion_unidad").val($(this).val());
	});

	$("#id_status option[value='1']").attr("disabled", true);
	//de retrasado solo a trabajando
	if(id_status_actual==4){
		$("#id_status option:not(:selected)").attr("disabled", true);
		$("#id_status option[value='2']").attr("disabled", false);
	}
	//espera de prueba solo a prueba
	if(id_status_actual==21){
		$("#id_status option:not(:selected)").attr("disabled", true);
		$("#id_status option[value='12']").attr("disabled", false);
	}
	// Detenida por autorización a Detenida por refacciones
	if(!dia_actual && id_status_actual==15){
		$("#id_status option:not(:selected)").attr("disabled", true);
		$("#id_status option[value='19']").attr("disabled", false);
		$("#id_status").prop('disabled',false)
	}
	//Si ya se trabajó bloquear retrasado
	if(se_trabajo){
		$("#id_status option[value='4']").attr("disabled", true);
	}
	
</script>