@layout('tema_luna/layout')
@section('css_vista')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
<style type="text/css">
  td a{
    margin-right: 5px !important;
    font-size: 20px !important;
  }
</style>
@section('contenido')
    <form action="{{site_url('citas/info_cliente_busqueda_shara_all')}}" id="frm" method="POST">
      <input type="hidden" name="proactivo" id="proactivo" value="{{$proactivo}}">
        <div class="row">
             <div class="col-md-3">
              <label for="">Buscar por campo</label>
              <input class="form-control" type="text" name="buscar_campo" id="buscar_campo">
            </div>
            <div class='col-md-3'>
              <label for="">Fecha inicio</label>
              <input type="date" class="form-control" name="finicio" id="finicio" value="">
                <span style="color: red" class="error error_fini"></span>
            </div>
            <div class='col-md-3'>
              <label for="">Fecha Fin</label>
                <input type="date" class="form-control" name="ffin" id="ffin" value="">
                <span style="color: red" class="error error_ffin"></span>

            </div>
      </div>
      <div class="row">
            <div class="col-md-3 col-sm-3" style="margin-top:30px;">
              <button type="button" id="buscar" name="buscar" class="btn btn-info">Buscar</button>
            </div>
            <div class="col-md-6 col-sm-6" style="margin-top:30px;">
              <!-- <a href="{{site_url('citas/modal_multipunto')}}" target="_blank"  id="indicadores" name="indicadores" class="btn btn-info">Indicadores multipunto</a>
              <a href="{{site_url('citas/modal_cotizaciones_rechazadas')}}" target="_blank"  id="rechazadas" name="rechazadas" class="btn btn-info">Presupuestos no autorizados</a> -->
              <a href="#"  id="indicadores" name="indicadores" class="btn btn-info">Indicadores multipunto</a>
              <a href="#"  id="rechazadas" name="rechazadas" class="btn btn-info">Presupuestos no autorizados</a>
            </div>
        </div>
    </form>

  <div class="row text-right">
    <div class="col-sm-12">
          <button type="button" id="inventario" style="" class="btn btn-default">Ver inventario</button>
          <button id="pdf" class="btn btn-info" style="margin-right: 20px">
            <i style="color: white" class="pe pe-7s-file"></i> Exportar PDF
          </button>
        </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered table-striped table-hover"  id="tabla" style="width:100%">
            <thead>
                <tr class="tr_principal verde">
                  @if($proactivo)
                    <th>id</th>
                    <th>nombre </th>
                    <th>ap </th>
                    <th>am </th>
                    <th>placas </th>
                    <th>tel_principal </th>
                    <th>tel_secundario </th>
                    <th>tel_adicional </th>
                    <th>correo </th>
                    <th>Acciones</th>
                  @else
                        <th>id </th>
                         <th>Serie</th>
                         <th>Fecha </th>
                         <th>Nombre completo</th>
                         <th>Contacto </th>
                         <th>Dirección </th>
                         <th>Número </th>
                         <th>Colonia </th>
                         <th>Estado </th>
                         <th>Población </th>
                         <th>Teléfono</th>
                         <th>celular </th>
                         <th>R.F.C. </th>
                         <th>Correo </th>
                         <th>Correo2 </th>
                         <th>Observaciones </th>
                         <th>Asesor </th>
                         <th>Placas </th>
                         <th>Auto </th>
                         <th>Color </th>
                         <th>Kilometraje </th>
                         <th>Cilindros</th>
                         <th>Modelo</th>
                         <th>Motor</th>
                         <th>Transmisión</th>
                         <th>Nombre</th>
                         <th>ap </th>
                         <th>am </th>
                         <th>Orden T </th>
                         <th>Acciones</th>
                  @endif

                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
  </div>
@endsection

@section('scripts')

  <script>
    var site_url = "{{site_url()}}";
        $('.date').datetimepicker({
          format: 'DD/MM/YYYY',
          icons: {
              time: "far fa-clock",
              date: "fa fa-calendar",
              up: "fa fa-arrow-up",
              down: "fa fa-arrow-down"
          },
           locale: 'es'
        });
    $(document).ready(function() {
            iniciarTabla();
            $("#buscar").on('click',function(){
                $(".error").empty();
                var tipo_busqueda = $("#tipo_busqueda").val();
                var finicio = $("#finicio").val();
                var ffin = $("#ffin").val();
                var table = $('#tabla').DataTable();
                table.destroy();
                iniciarTabla();

            });
            
        });
        function iniciarTabla(){

            var tabla = $("#tabla").dataTable({
                paging: true,
                bFilter: true,
                processing: true,
                responsive: true,
                serverSide:true,
                ajax: {
                  url: site_url+"/citas/getDatosDt",
                  type: 'POST',
                   //////////////////////////////////////////////////////////////// AQUI PUEDO AGREGAR MAS CAMPOS EN LA PETICION
                  data: function(data){
                     data.fecha_inicio = $("#finicio").val();
                     data.fecha_fin = $("#ffin").val();
                     data.tipo_busqueda = $("#tipo_busqueda").val();
                     data.buscar_campo = $("#buscar_campo").val();
                     data.proactivo = $("#proactivo").val();
                    // if(filtros.pagina){
                    //   data.start = parseInt(filtros.pagina);
                    // }
                    empieza = data.start;
                    por_pagina = data.length;
                  }
                },
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "oLanguage": {
                   "oPaginate": {
                       "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                       "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                    '<option value="5">5</option>' +
                    '<option value="10">10</option>' +
                    '<option value="20">20</option>' +
                   '<option value="30">30</option>' +
                    '<option value="40">40</option>' +
                    '<option value="50">50</option>' +
                    '<option value="-1">Todos</option>' +
                    '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "mensaje",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar"
                },
            });
        }
    $("body").on("click",'.js_comentarios',function(e){
    e.preventDefault();
    id_cita = $(this).data('id');
       var url =site_url+"/citas/comentarios_proactivo_historial/0";
       customModal(url,{"id_cita":id_cita},"GET","lg",ingresarComentario,"","Guardar","Cancelar","Ingresar comentario","modal1");
    });
    $("body").on("click",'.js_historial',function(e){
    e.preventDefault();
    id_cita = $(this).data('id');
       var url =site_url+"/citas/historial_comentarios_proactivo_historial/0";
       customModal(url,{"id_cita":id_cita},"GET","lg","","","","Cerrar","Historial de comentarios","modal1");
    });

    //Creamos el modal para el pdf de la informacion del cliente
    $("body").on("click",'.js_info',function(e){
        e.preventDefault();
        id_cita = $(this).data('id');
        var url = site_url+"/dms/info_cliente_dms/";
        customModal(url,{"id_cita":id_cita},"POST","lg","","","","Salir","Ficha Cliente","modal1");
    });

  function ingresarComentario(){
    var url =site_url+"/citas/comentarios_proactivo_historial";
    ajaxJson(url,$("#frm_comentarios").serialize(),"POST","",function(result){
      if(isNaN(result)){
        data = JSON.parse( result );
        //Se recorre el json y se coloca el error en la div correspondiente
        $.each(data, function(i, item) {
           $.each(data, function(i, item) {
                      $(".error_"+i).empty();
                      $(".error_"+i).append(item);
                      $(".error_"+i).css("color","red");
                  });
        });
      }else{
        if(result <0){
          ErrorCustom('No se pudo guardar el comentario, por favor intenta de nuevo');
        }else{
          $(".close").trigger('click');
           ExitoCustom("Comentario guardado con éxito");

        }
      }
    });
  }

  $("body").on("click",'.js_refacciones',function(e){
    e.preventDefault();
      var url =site_url+"/citas/refacciones_shara/";
      customModal(url,{},"POST","lg","","","","Salir","Refacciones","modal1");
    });
  $("body").on("click",'#indicadores',function(e){
    e.preventDefault();
       var url =site_url+"/citas/modal_multipunto";
       customModal(url,{},"GET","lg","","","","Salir","Indicadores Multipunto","modalMultipunto");
  });
  $("body").on("click",'#rechazadas',function(e){
    e.preventDefault();
       var url =site_url+"/citas/modal_cotizaciones_rechazadas";
       customModal(url,{},"GET","lg","","","","Salir","Presupuestos No autorizados","modalPresupuestos");
  });
  //Creamos el modal para el pdf de la informacion del cliente
  $("body").on("click",'#pdf',function(e){
        e.preventDefault();
        var url = site_url+"/citas/exportar_historial_pdf/";
        customModal(url,$("#frm").serialize(),"POST","lg","","","","Salir","Exportar","modalExport");
    });
  </script>
  <script src="<?php echo base_url()?>js/dms/generico.js"></script>
@endsection
