@layout('tema_luna/layout')
@section('contenido')
    <div class="page-title">
        <div class="title_left">
            <h3>Recepción de citas</h3>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">

                <div class="x_content">
                    <br />
                    <form action="" method="POST" id="frm">
                        <input type="hidden" name="id" id="id" value="{{$input_id}}">
                        <input type="hidden" name="qr" id="qr" value="{{$qr}}">
                        <input type="hidden" name="serie_old" id="serie_old" value="{{$serie_old}}">
                        <input type="hidden" name="status_actual" id="status_actual" value="{{$status_actual}}">
                        <div class="row">
                            <div class='col-sm-2'>
                              <label for="">Fecha</label>
                                <div class="form-group1">
                                    <div class='input-group date' id='datetimepicker1'>
                                        {{$input_date}}
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                 <span class="error error_date"></span>
                            </div>
                            <div class='col-sm-2'>
                              <label for="">VIN</label>
                                {{$input_vin}}
                                 <span class="error error_vin"></span>
                            </div>
                            <!--<div class='col-sm-4'>
                              <label for="">Tipo</label>
                                {{$drop_id_tipo}}
                                <span class="error error_id_tipo"></span>
                            </div>-->
                            <div class="col-sm-2">
                              <label>Artículo categoría <i style="cursor: pointer;" class="pe pe-7s-plus addCategoria" data-toggle="tooltip" data-placement="right" title="agregar categoría"></i></label>
                              {{$drop_idcategoria}}
                            </div>
                            <div class="col-sm-2">
                              <label>Artículo subcategoría <i style="cursor: pointer;" class="pe pe-7s-plus addSubCategoria" data-toggle="tooltip" data-placement="right" title="agregar subcategoría"></i></label>
                              {{$drop_idsubcategoria}}
                            </div>
                            <div class='col-sm-4'>
                              <label for="">Color</label>
                                {{$drop_id_color}}
                                <span class="error error_idcolor"></span>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class='col-sm-4'>
                              <label for="">Serie Corta</label>
                                {{$input_seriecorta}}
                                 <span class="error error_seriecorta"></span>
                            </div>
                            <div class='col-sm-2'>
                              <label for="">Año</label>
                                {{$input_year}}
                                <span class="error error_year"></span>
                            </div>
                            <div class='col-sm-3'>
                              <label for="">Estatus</label>
                                {{$drop_id_status}}
                                 <span class="error error_id_status"></span>
                            </div>
                            <!--<div class='col-sm-3'>
                              <label for="">Ubicación</label>
                                {{$drop_id_ubicacion}}
                                 <span class="error error_id_ubicacion"></span>
                            </div>-->
                            
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-4">
                                @if($qr!='')
                                <img id="imgqr" style="width: 200px;" src="{{$qr}}" alt="">
                                @endif
                            </div>
                            @if($input_id!=0)
                             <div class='col-sm-5'>
                              <label for="">Comentario</label>
                                {{$input_comentario}}
                                 <span class="error error_comentario"></span>
                            </div>
                            @endif
                        </div>
                        <div class="row">
                            <!--<div class="col-sm-2">
                                <label for=""># Económico</label>
                                {{$input_noeconomico}}
                            </div>
                            <div class="col-sm-3">
                                <label for="">Ubicación llaves</label>
                                {{$drop_id_ubicacion_llaves}}
                                <span class="error error_id_ubicacion_llaves"></span>
                            </div>-->
                            <div class="col-sm-3">
                                <label for="">Placas</label>
                                {{$input_placa}}
                                <span class="error error_placas"></span>
                            </div>
                            <div class="col-sm-3">
                                <label for="">Nombre</label>
                                {{$input_nombre}}
                                <span class="error error_nombre"></span>
                            </div>
                            <div class="col-sm-3">
                                <label for="">Apellido paterno</label>
                                {{$input_ap1}}
                                <span class="error error_ap1"></span>
                            </div>
                             <div class="col-sm-3">
                                <label for="">Apellido materno</label>
                                {{$input_ap2}}
                                <span class="error error_ap2"></span>
                            </div>
                        </div>
                    </form>
                    <br>
                    <div class="row">
                        <div class="col-sm-12 text-right">
                            {{$btn_guardar}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
  <script type="text/javascript">
        var site_url = "{{site_url()}}";
        var id = '' ; 
        var status_actual = "{{$status_actual}}";
        $(".busqueda").select2();
        $('.date').datetimepicker({
            //minDate: moment(),
            format: 'DD/MM/YYYY',
            icons: {
                time: "far fa-clock",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
             locale: 'es'
        });
        $("#guardar").on('click',guardar);
        function guardar(){
            var url =site_url+"/citas/guardarUnidad";
            ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
                result = JSON.parse( result );
                if(result.validacion){
                    //Se recorre el json y se coloca el error en la div correspondiente
                    $.each(result.errors, function(i, item) {
                            $(".error_"+i).empty();
                            $(".error_"+i).append(item);
                            $(".error_"+i).css("color","red");
                    });
                }else{
                    if(!result.exito){
                        ErrorCustom(result.message);
                    }else{
                       ExitoCustom(result.message,function(){
                         window.location.href = site_url+"/citas/guardarUnidad/"+result.id;
                       });
                        
                    }
                }
            });
        }
        $(".addCategoria").on('click',function(){
      var url =site_url+"/citas/agregar_categoria_modal/0";
      customModal(url,{},"GET","md",agregarCategoria,"","Guardar","Cancelar","Agregar categoría","modalCategoria");
  });
   $(".addSubCategoria").on('click',function(){
      var url =site_url+"/citas/agregar_subcategoria_modal/0";
      if($("#idcategoria").val()!=''){
      customModal(url,{},"GET","md",agregarSubCategoria,"","Guardar","Cancelar","Agregar subcategoría","modalCategoria");
      }else{
        ErrorCustom("Es necesario seleccionar la categoría");
      }
  });

  function agregarCategoria(){
      var url =site_url+"/citas/agregar_categoria_modal";
      ajaxJson(url,{"categoria":$("#categoria").val()},"POST","",function(result){
        result = JSON.parse( result );
        if(!result.exito){
          //Se recorre el json y se coloca el error en la div correspondiente
             $.each(result.errors, function(i, item) {
                  $(".error_"+i).empty();
                  $(".error_"+i).append(item);
                  $(".error_"+i).css("color","red");
              });
        }else{
          ExitoCustom("categoría agregada correctamente",function(){
            $("#idcategoria").append("<option value='"+result.id+"'>"+result.categoria+"</option>");
            $(".busqueda").select2('destroy')
            $("#idcategoria").val(result.id);
            $(".busqueda").select2();
            $(".close").trigger('click');
            $("#idcategoria").trigger('change');
          });
        }
      });
    }


  function agregarSubCategoria(){
      var url =site_url+"/citas/agregar_subcategoria_modal";
      ajaxJson(url,{"categoria":$("#idcategoria").val(),"subcategoria":$("#subcategoria").val(),"clave":$("#clave").val()},"POST","",function(result){
        result = JSON.parse( result );
        if(!result.exito){
          //Se recorre el json y se coloca el error en la div correspondiente
             $.each(result.errors, function(i, item) {
                  $(".error_"+i).empty();
                  $(".error_"+i).append(item);
                  $(".error_"+i).css("color","red");
              });
        }else{
          ExitoCustom("subcategoría agregada correctamente",function(){
            $("#idsubcategoria").append("<option value='"+result.id+"'>"+result.subcategoria+"</option>");
            $(".busqueda").select2('destroy')
            $("#idsubcategoria").val(result.id);
            $(".busqueda").select2();
            $(".close").trigger('click');
          });
        }
      });
    }
    $("#idcategoria").on("change",function(){
      var url ="<?php echo site_url();?>/citas/getSubcategorias";
      $(".busqueda").select2('destroy');

      $("#idsubcategoria").empty();
      $("#idsubcategoria").append("<option value=''>-- Selecciona --</option>");
      $("#idsubcategoria").attr("disabled",true);

      if($(this).val()!=0){
        ajaxJson(url,{"idcategoria":$(this).val()},"POST","",function(result){

          if(result.length !=0){
            $("#idsubcategoria").empty();
            $("#idsubcategoria").removeAttr("disabled");
            result=JSON.parse(result);
            $("#idsubcategoria").append("<option value=''>-- Selecciona --</option>");
            $.each(result, function(i, item) {
              $("#idsubcategoria").append("<option value= '" + result[i].id + "'>" + result[i].subcategoria + "</option>");
            });
          }else{
            $("#idsubcategoria").empty();

            $("#idsubcategoria").append("<option value='0'>No se encontraron datos</option>");

          }
        });
      }else{
        $("#idsubcategoria").empty();
        $("#idsubcategoria").append("<option value=''>-- Selecciona --</option>");
        $("#idsubcategoria").attr("disabled",true);

      }
      $(".busqueda").select2();
    }); 
      
</script>
@endsection