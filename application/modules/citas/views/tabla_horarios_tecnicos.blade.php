<div class="table-wrap">
<table class="table table-striped main-table" scope="col">
					<thead>
						<tr>
							
							<th style="color:#000 !important" width="20%" class="fixed-side" scope="col">Técnico</th>
							<th style="color:#000 !important">Hora L.</th>
							<?php
							 	$time = '09:00';
						     	$contador=0;
						    ?>
						    @while($contador<$valor-6)
									<td scope="col">{{$time}}</td>
					    		<?php
						            $timestamp = strtotime($time) + $tiempo_aumento*60;
						            $time = date('H:i', $timestamp);
						            $contador++;
								?>
				    		@endwhile
						</tr>
					</thead>
					<tbody>
						@foreach($tecnicos as $s => $value)
							<tr>
								<td class="nombre_tecnico fixed-side">{{$value->nombre}}</td>
								<td>{{substr($value->hora_inicio, 0,5).'-'.substr($value->hora_fin, 0,5)}}</td>
								<?php
							 	$time = '09:00';
						     	$contador=0;
							    ?>
							    @while($contador<$valor-6)
							    <?php
							    //$timestamp = strtotime($hora) + 1*60;
	    						//$hora = date('H:i', $timestamp).':00';
							    ?>
							    	@if($value->activo)
								    	<?php $tiempoplus1 = strtotime($time) + 1*60;
							            	$tiempoplus1 = date('H:i', $tiempoplus1);
						         		?>
							    		@if($tiempoplus1.':00'<=$value->hora_inicio_nolaboral || $tiempoplus1.':00' >= $value->hora_fin_nolaboral)
										    <?php $fecha_hora = $fecha.' '.$time;
												$datetimenow = date('Y-m-d H:i:s');
												if($fecha_hora>$datetimenow){
													$clase = 'asignar_cita_tablero';
												}else{
													$clase = '';
												}
												$id = str_replace(':','',$time).'_'.$value->id;

										     ?>
										    <?php $tiempo = strtotime($time) + 1*60;
									            $tiempo_comparar = date('H:i', $tiempo);
									         ?>
											<!-- validación de la hora de comida -->
									    	@if($tiempo_comparar.':00'>$value->hora_inicio_comida &&  $tiempo_comparar.':00'<$value->hora_fin_comida)
												<td data-id_tecnico= "{{$value->id}}" style="background-color: #000" class="" data-time="{{$time}}"></td>
											<!-- validación de la hora laboral -->
									    	@elseif($time.':00'>=$value->hora_inicio && $time.':00'<$value->hora_fin && $value->activo_fecha==1)
									    		<?php 
								    			$ocupado = $this->mc->HoraBetweenCita($time,$fecha,$value->id,$origen);
								    			$consulta = $this->db->last_query();
								    			
								    			$explode = explode('-', $ocupado);
												$resultado = $explode[0];
												
												$id_cita = $explode[1];
												$id_operacion = $explode[2];

								    			if(!$resultado){
								    				$ocupado_historial = $this->mc->HoraBetweenCitaHistorial($time,$fecha,$value->id,$origen);
													//$consulta_historial = $this->db->last_query();
									    			$explode_historial = explode('-', $ocupado_historial);
									    			$resultado_historial = $explode_historial[0];
									    			$id_cita_historial = $explode_historial[1];
													$id_carryover = $explode_historial[2];
													$id_operacion = $explode_historial[3];
													if($explode_historial[4]){
														$color_etiqueta = 'black';
													}else{
														$color_etiqueta = 'white';
													} //si es 1 tiene carryover
									    			
								    			}else{
								    				$color_etiqueta = 'white';
								    			}
								    		?>
								    		@if($resultado)
								    			<?php
												  $color_cita = $this->mc->getColorCita($id_cita,$id_operacion,$origen);
								    			  if(isset($color_cita)){
								    			  	$color_cita = explode('-', $color_cita);
								    			  	$color_cuadro = $color_cita[0];
								    			  	$color_circulo = $color_cita[1];
								    			  	$id_status_cita = isset($color_cita[2])?$color_cita[2]:'';
								    			  }else{
								    			  	$color_cita = '';
								    			  	$color_cuadro = '';
								    			  	$color_circulo = '';
								    			  	$id_status_cita = '';
								    			  }
								    			  //Comparar para ver si es el último registro de la cita

								    			?>
								    			@if($id_status_cita!=5)
												<!--//NUEVA VERSION -->
													<td data-tabla_operacion="tecnicos_citas" data-idcarryover="0" data-idoperacion="{{$id_operacion}}" data-cita="{{$id_cita}}" class="cita_{{$id_cita}} js_cambiar_status" style="background-color: {{$color_cuadro}}; cursor: pointer;">
													<div data-idoperacion="{{$id_operacion}}" class="arrow-down triangle triangle_{{$id_cita}}_{{$id_operacion}}" data-idcita="{{$id_cita}}" data-time="{{$time}}" data-idstatuscita="{{$id_status_cita}}"></div>
													<span class="label etiqueta mr5" style="background-color: {{$color_circulo}};border: 2px solid {{$color_etiqueta}}; cursor: pointer;">{{$id_cita}}</span></td>
								    			@else
								    			<td data-id_tecnico= "{{$value->id}}" class="{{$clase}}" id="{{$id}}" data-time="{{$time}}">
												</td>

								    			@endif
												
											@elseif($resultado_historial)
											<?php

												  $color_cita = $this->mc->getColorCita($id_cita_historial,$id_operacion,$origen);
								    			  if(isset($color_cita)){
								    			  	$color_cita = explode('-', $color_cita);
								    			  	$color_cuadro = $color_cita[0];
								    			  	$color_circulo = $color_cita[1];
								    			  	$id_status_cita = isset($color_cita[2])?$color_cita[2]:'';
								    			  }else{
								    			  	$color_cita = '';
								    			  	$color_cuadro = '';
								    			  	$color_circulo = '';
								    			  	$id_status_cita = '';
								    			  }
								    			  
								    			?>
												<!--//NUEVA VERSION -->
												<td data-tabla_operacion="tecnicos_citas_historial" data-idcarryover="{{$id_carryover}}" data-idoperacion="{{$id_operacion}}" data-cita="{{$id_cita_historial}}" class="cita_{{$id_cita_historial}} js_cambiar_status" style="background-color: {{$color_cuadro}}; cursor: pointer;">
													<div data-idoperacion="{{$id_operacion}}" class="arrow-down triangle triangle_{{$id_cita}}_{{$id_operacion}}" data-idcita="{{$id_cita}}" data-time="{{$time}}" data-idstatuscita="{{$id_status_cita}}"></div>
													<span class="label etiqueta mr5" style="background-color: {{$color_circulo}};border: 2px solid {{$color_etiqueta}}; cursor: pointer;">{{$id_cita_historial}}</span></td>
												<!--<td data-id_tecnico= "{{$value->id}}" class="{{$clase}}" data-time="{{$time}}">-->
											@else
												<td data-id_tecnico= "{{$value->id}}" class="{{$clase}}" id="{{$id}}" data-time="{{$time}}">
												</td>
								    		@endif
												
									    	@else
									    		<td class="color_hora_nolaboral"></td>
									    	@endif
									    @else <!-- validar que no tenga bloqueada la hora -->
								    		<td class="color_hora_nolaboral"></td>
								    	@endif
							    		
									@else <!-- validar que este activo -->
										<td class="color_hora_nolaboral"></td>
									@endif
									<?php
							            $timestamp = strtotime($time) + $tiempo_aumento*60;
							            $time = date('H:i', $timestamp);
							            $contador++;
									?>
					    		@endwhile
							</tr>
						@endforeach
					</tbody>
				</table>
</div>