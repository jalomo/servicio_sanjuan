<div class="row">
	<div class="col-sm-12">
		<label for="">Subcategoría</label>
		{{$input_subcategoria}}
		<span class="error error_subcategoria"></span>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<label for="">Clave</label>
		{{$input_clave}}
		<span class="error error_clave"></span>
	</div>
</div>