@layout('tema_luna/layout')
@section('css_vista')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('contenido')
    <form action="" id="frm">
        <div class="row">
          
            <div class="col-sm-3">
              <label for="">Buscar por campo</label>
              <input class="form-control" type="text" name="buscar_campo" id="buscar_campo">
            </div>
            <div class="col-sm-2" style="margin-top:30px;">
                <button type="button" id="buscar" name="buscar" class="btn btn-info pull-right">Buscar</button>
            </div>
        </div>
        
        
    </form>
    <br>
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered table-striped table-responsive table-hover"  id="tabla" class="display" style="width:100%">
		        <thead>
		           <tr class="tr_principal">
			          <th>ID</th>
	                  <th>Fecha de emisión</th>
	                  <th>Artículo</th>
	                  <th>Cliente</th>
	                  <th>Correo</th>
	                  <th>Teléfono</th>
	                  <th>Acciones</th>
                </tr>
		        </thead>
		        <tbody>
		        	
		        </tbody>
		    </table>
		</div>
	</div>
@endsection



@section('scripts')

<script>
  var site_url = "{{site_url()}}";
  var id_proactivo = '';
  var accion = '';
  var valor = '';
  var aPos = '';
  var site_url = "{{site_url()}}";
    Vals = '';
    $('.date').datetimepicker({
      format: 'DD/MM/YYYY',
      icons: {
          time: "far fa-clock",
          date: "fa fa-calendar",
          up: "fa fa-arrow-up",
          down: "fa fa-arrow-down"
      },
       locale: 'es'
    });
  $(document).ready(function() {
    iniciarTabla();
    $("#buscar").on('click',function(){
      $(".error").empty();
      var tipo_busqueda = $("#tipo_busqueda").val();
      var finicio = $("#finicio").val();
      var ffin = $("#ffin").val();

      if(tipo_busqueda!=''){
        if(finicio==''){

        }else if(ffin==''){
          $(".error_ffin").text('El campo es requerido');
        }else{
          var table = $('#tabla').DataTable();
          table.destroy();
          iniciarTabla();
        }
      }else{
        var table = $('#tabla').DataTable();
        table.destroy();
        iniciarTabla();
      }

    });
    $("div.dataTables_filter input").on('change',function(e){
    });

  }); 

        function iniciarTabla(){

            var tabla = $("#tabla").dataTable({
                paging: true,
                bFilter: false,
                processing: true,
                responsive: true,
                serverSide:true,
                ajax: {
                  url: site_url+"/citas/getDatosProactivoIntelisis",
                  type: 'POST',
                   //////////////////////////////////////////////////////////////// AQUI PUEDO AGREGAR MAS CAMPOS EN LA PETICION 
                  data: function(data){
                     data.fecha_inicio = $("#finicio").val();
                     data.fecha_fin = $("#ffin").val();
                     data.tipo_busqueda = $("#tipo_busqueda").val();
                     data.buscar_campo = $("#buscar_campo").val();
                    // if(filtros.pagina){
                    //   data.start = parseInt(filtros.pagina);
                    // }
                    empieza = data.start;
                    por_pagina = data.length;
                  }
                },
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "oLanguage": {
                   "oPaginate": {
                       "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                       "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                    '<option value="5">5</option>' +
                    '<option value="10">10</option>' +
                    '<option value="20">20</option>' +
                   '<option value="30">30</option>' +
                    '<option value="40">40</option>' +
                    '<option value="50">50</option>' +
                    '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "No hay registros para mostrar...",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar"
                },
            });
        }
    $("body").on("click",'.js_comentarios',function(e){
    e.preventDefault();
    id_proactivo = $(this).data('id');
       var url =site_url+"/citas/comentarios_Proactivo_Intelisis";
       customModal(url,{"id_proactivo":id_proactivo},"GET","md",ingresarComentario,"","Guardar","Cancelar","Ingresar comentario","modal1");
    }); 
    $("body").on("click",'.js_historial',function(e){
    e.preventDefault();
    id_proactivo = $(this).data('id');
    //alert(id_proactivo);
       var url =site_url+"/citas/historial_comentarios_proactivo_intelisis/0";
       customModal(url,{"id_proactivo":id_proactivo},"GET","md","","","","Cerrar","Historial de comentarios","modal1");
    }); 
  function ingresarComentario(){
    var url =site_url+"/citas/comentarios_Proactivo_Intelisis";
    ajaxJson(url,$("#frm_comentarios").serialize(),"POST","",function(result){
      if(isNaN(result)){
        data = JSON.parse( result );
        //Se recorre el json y se coloca el error en la div correspondiente
        $.each(data, function(i, item) {
           $.each(data, function(i, item) {
                      $(".error_"+i).empty();
                      $(".error_"+i).append(item);
                      $(".error_"+i).css("color","red");
                  });
        });
      }else{
        if(result <0){
          ErrorCustom('No se pudo guardar el comentario, por favor intenta de nuevo');
        }else{
          $(".close").trigger('click');
           ExitoCustom("Comentario guardado con éxito");
          
        }
      }
    });
  }

</script>
@endsection