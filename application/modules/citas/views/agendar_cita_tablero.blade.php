<div class="row pull-right">
    <div class="col-sm-4">
        <h4 for=""># Cita</h4>
    </div>
    <div class="col-sm-4">
        <input type="number" id="carriover" name="carriover" id="carriover" class="form-control">
        <div class="error error_carriover"></div>
    </div>
    <div class="col-sm-2">
        <button id="btn-co" class="js_carriover btn btn-success">Buscar</button>
    </div>
</div>

<br>

<br>
<h3>Datos del Vehículo</h3>
<form action="" method="POST" id="frm_cita_tablero">
    <input type="hidden" name="id" id="id" value="{{ $input_id }}">
    <input type="hidden" name="id_horario" id="id_horario" value="{{ $input_id_horario }}">
    <input type="hidden" id="realizo_servicio" name="realizo_servicio" value="{{ $input_realizo_servicio }}">
    <input type="hidden" id="origen" name="origen" value="tablero">
    <input type="hidden" name="id_tecnico_actual" id="id_tecnico_actual" value="{{ $input_id_tecnico_actual }}">

    <input type="hidden" name="iscarriover" id="iscarriover" value="">


    <div class="row">
        <!--<div class="col-sm-4 form-group">
        <label for="">Transporte</label>
        {{ $drop_transporte }}
        <div class="error error_transporte"></div>
      </div>-->
        <div class="col-sm-4 form-group">
            <label for="">Año del vehículo</label>
            {{ $drop_vehiculo_anio }}
            <div class="error error_vehiculo_anio"></div>
        </div>
        <div class="col-sm-4 form-group">
            <label for="">Placas</label>
            {{ $input_vehiculo_placas }}
            <div class="error error_vehiculo_placas"></div>
        </div>
        <div class="col-sm-4 form-group">
            <label for="">Color</label>
            {{ $drop_color }}
            <div class="error error_id_color"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 form-group">
            <label for="">Modelo</label>
            {{ $drop_vehiculo_modelo }}
            <div class="error error_vehiculo_modelo"></div>
        </div>
        <div class="col-sm-4 form-group">
            <label for="">Número de serie</label>
            {{ $input_vehiculo_numero_serie }}
            <div class="error error_numero_serie"></div>
        </div>
        <div class="col-sm-4 form-group">
            <label for="">Asesor</label>
            {{ $drop_asesor }}
            <div class="error error_asesor"></div>
        </div>
    </div>

    <div class="row" style="display: none;">
        <div class="col-sm-4 form-group">
            <label for="">Fecha</label>
            {{ $drop_fecha }}
            <div class="error error_fecha"></div>
        </div>
        <div class="col-sm-4 form-group">
            <label for="">Horario</label>
            {{ $drop_horario }}
            <div class="error error_horario"></div>
        </div>
    </div>
    <div class="row">

        <div class="col-sm-3 form-group">
            <label for="">Estatus</label>
            {{ $drop_id_status_color }}
            <div class="error error_id_status_color"></div>
        </div>
        <div class="col-sm-4">
            <br>
            <label for="">¿Cita por días completos?</label>
            {{ $input_dia_completo }}
        </div>
    </div>
    @if ($input_id == 0)
        <div id="div_completo">
            <div class="row">
                <div class="col-sm-4">
                    <label>Asignar técnico <a href="" class="js_ver_citas_dias">(Ver horarios)</a></label>
                    {{ $drop_tecnicos_dias }}
                    <span class="error error_tecnico"></span>
                </div>
            </div>
            <br>
            <div class="row">
                <div class='col-sm-3'>
                    <label for="">Fecha inicio</label>
                    {{ $input_fecha_inicio }}
                    <span class="error_fecha_inicio"></span>
                </div>
                <div class='col-sm-3'>
                    <label for="">Fecha Fin</label>
                    {{ $input_fecha_fin }}
                    <span class="error_fecha_fin"></span>
                </div>
                <div class="col-sm-3">
                    <label for="">Hora de inicio del día</label>
                    {{ $input_hora_comienzo }}
                    <span class="error_hora_hora_comienzo"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <label>Día extra</label>
                    {{ $input_fecha_parcial }}
                </div>
                <div class="col-sm-3">
                    <label for="">Hora inicio trabajo</label>
                    {{ $input_hora_inicio_extra }}
                    <span class="error_hora_inicio_extra"></span>
                </div>
                <div class="col-sm-3">
                    <label for="">Hora fin trabajo</label>
                    {{ $input_hora_fin_extra }}
                    <span class="error_hora_fin_extra"></span>
                    <br>
                    <!--button id="guardar" class="btn btn-success pull-right">Guardar</button>-->
                </div>
            </div>
        </div> <!-- completos -->
        <div id="div_incompleto" class="row">
            <div class="col-sm-4">
                <label>Asignar técnico <a href="" class="js_ver_citas">(Ver horarios)</a></label>
                {{ $drop_tecnicos }}
                <span class="error error_tecnico"></span>
            </div>
            <div class="col-sm-3">
                <label for="">Hora inicio trabajo</label>
                {{ $input_hora_inicio }}
                <span class="error_hora_inicio"></span>
            </div>
            <div class="col-sm-3">
                <label for="">Hora fin trabajo</label>
                {{ $input_hora_fin }}
                <span class="error_hora_fin"></span>
                <br>
                <!--button id="guardar" class="btn btn-success pull-right">Guardar</button>-->
            </div>
        </div> <!-- 0 -->
    @endif
    @if (PermisoAccionTablero('carryover_reservacion', true))
        <!-- <div class="row text-right">
      <div class="col-sm-12">
        <strong>Reservación sin operación</strong>  <input type="checkbox" name="carryover_reservacion" id="carryover_reservacion">
      </div>
    </div> -->
    @endif
    <div class="row">
        <div class="col-sm-6">
            <label for="">Comentarios</label>
            {{ $input_comentarios }}
            <div class="error error_comentario"></div>
        </div>
        <div class="col-sm-6">
            <h4>Lista de operaciones</h4>
            <div id="div_operaciones"></div>
        </div>
    </div>
    <div class="pull-right" id="validar_operaciones">

    </div>
    <br>
    <h3>Mis datos</h3>
    <div class="row">
        <div class="col-sm-4 form-group">
            <label for="">Correo electrónico</label>
            {{ $input_email }}
            <div class="error error_email"></div>
        </div>
        <div class="col-sm-4 form-group">
            <label for="">Nombre(s)</label>
            {{ $input_datos_nombres }}
            <div class="error error_nombre"></div>
        </div>
        <div class="col-sm-4 form-group">
            <label for="">Apellido Paterno</label>
            {{ $input_datos_apellido_paterno }}
            <div class="error error_apellido_paterno"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 form-group">
            <label for="">Apellido Materno</label>
            {{ $input_datos_apellido_materno }}
            <div class="error error_apellido_materno"></div>
        </div>
        <div class="col-sm-4 form-group">
            <label for="">Teléfono</label>
            {{ $input_datos_telefono }}
            <div class="error error_telefono"></div>
        </div>
        <!--<div class="col-sm-4 form-group">
        <label for="">Servicio</label>
        {{ $drop_servicio }}
        <div class="error error_servicio"></div>
      </div>-->
    </div>
</form>


@section('scripts')
    <script>
        //$(".busqueda").select2();
        var site_url = "{{ site_url() }}";
        //$('.clockpicker').clockpicker();
        $("#div_completo").hide();
        var fecha_actual = "<?php echo date('Y-m-d'); ?>";
        $('.date').datetimepicker({
            minDate: moment(),
            format: 'DD/MM/YYYY',
            icons: {
                time: "far fa-clock",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            locale: 'es'
        });
        $('.clockpicker_inicio').clockpicker({
            afterDone: function() {
                validar_fecha_inicio();
            },
        });
        $('.clockpicker_fin').clockpicker({
            afterDone: function() {
                validar_fin();
            },
        });
        $('.clockpicker_inicio').on('change', function() {
            validar_fecha_inicio();
        });
        $('.clockpicker_fin').on('change', validar_fin);

        function validar_fecha_inicio() {
            var dt = new Date();
            if (parseInt(dt.getMinutes()) < 10) {
                var time = dt.getHours() + ":0" + dt.getMinutes();
            } else {
                var time = dt.getHours() + ":" + dt.getMinutes();
            }

            var hora_inicio = $("#hora_inicio").val();
            var hora_fin = $("#hora_inicio").val();

            if (fecha_actual == $("#fecha").val()) {
                if (hora_inicio < time) {
                    $("#hora_inicio").val(time);
                }
                var hora_inicio = $("#hora_inicio").val();
                $("#hora_fin").val(hora_inicio);
            } else {
                if (hora_fin < hora_inicio) {
                    $("#hora_fin").val(hora_inicio)
                }
            }


        }
        $("#dia_completo").on('click', function() {
            if ($(this).prop('checked')) {
                $("#div_completo").show('slow');
                $("#div_incompleto").hide('slow');
            } else {
                $("#div_completo").hide('slow');
                $("#div_incompleto").show('slow');
            }
        });
        $("#fecha_fin").on('focusout', function() {

        })

        function validar_fin() {
            var hora_fin = $("#hora_fin").val();
            var hora_inicio = $("#hora_inicio").val();

            if (hora_fin < hora_inicio) {
                $("#hora_fin").val(hora_inicio);
            }
        }
        var input_ocupado = "{{ $input_realizo_servicio }}";
        var tecnico_actual = "{{ $input_id_tecnico_actual }}";
        $("#asesor").on("change", function() {
            var url = site_url + "/citas/getHorarios";
            var fecha_split = $("#fecha").val().split('/');

            fecha = fecha_split['2'] + '-' + fecha_split['1'] + '-' + fecha_split['0'];
            asesor = $("#asesor").val();
            if (fecha != '' && asesor != '') {
                ajaxJson(url, {
                    "asesor": asesor,
                    "fecha": fecha,
                    "id_horario": $("#id_horario").val()
                }, "POST", "", function(result) {
                    if (result.length != 0) {
                        $("#horario").empty();
                        $("#horario").removeAttr("disabled");
                        result = JSON.parse(result);
                        $("#horario").append("<option value=''>-- Selecciona --</option>");
                        $.each(result, function(i, item) {
                            $("#horario").append("<option value= '" + result[i].id + "'>" + result[
                                i].hora.substring(0, 5) + "</option>");
                        });
                        var id_cita = $("#id").val();

                    } else {
                        $("#horario").empty();
                        $("#horario").append("<option value='0'>No se encontraron datos</option>");
                    }
                });
            } else {

                $("#horario").empty();
                $("#horario").append("<option value=''>-- Selecciona --</option>");
                $("#horario").attr("disabled", true);

                $("#tecnicos").empty();
                $("#tecnicos").append("<option value=''>-- Selecciona --</option>");
                $("#tecnicos").attr("disabled", true);
                $("#hora_inicio").val('');
                $("#hora_fin").val('');

                $("#tecnicos_dias").empty();
                $("#tecnicos_dias").append("<option value=''>-- Selecciona --</option>");
                $("#tecnicos_dias").attr("disabled", true);
            }
        });
        if (tecnico_actual != 0) {
            $("#tecnicos").trigger('change');
        }
        $("#asignar_tecnico").on("click", function() {
            if ($("#tecnicos").val() != '') {
                var url = site_url + "/citas/modal_asignar_tecnico/0";
                customModal(url, {
                        "id": $("#id").val(),
                        'id_tecnico': $("#id_tecnico_actual").val()
                    }, "GET", "lg", guardarTecnico, "", "Guardar", "Cancelar", "Asignar hora a técnico",
                    "modal1");
            } else {
                ErrorCustom("Es necesario seleccionar el técnico.");
            }
        });
        $(".js_ver_citas").on('click', function(e) {
            e.preventDefault();
            var id_tecnico = $("#tecnicos").val();
            if (id_tecnico == '') {
                ErrorCustom("Es necesario asignar al técnico");
            } else {
                var url = site_url + "/citas/modal_ver_citas_asignadas/0";
                customModal(url, {
                    "id_tecnico": id_tecnico,
                    'fecha': $("#fecha").val()
                }, "POST", "md", "", "", "", "Cerrar", "Lista de citas asignadas", "modal6");
            }
        });
        $(".js_ver_citas_dias").on('click', function(e) {
            e.preventDefault();
            var id_tecnico = $("#tecnicos_dias").val();
            if (id_tecnico == '') {
                ErrorCustom("Es necesario asignar al técnico");
            } else {
                var fecha_split = $("#fecha").val().split('/');
                fecha = fecha_split['2'] + '-' + fecha_split['1'] + '-' + fecha_split['0'];
                var url = site_url + "/citas/modal_ver_citas_asignadas/0";
                customModal(url, {
                    "id_tecnico": id_tecnico,
                    'fecha': fecha
                }, "POST", "md", "", "", "", "Cerrar", "Lista de citas asignadas", "modal6");
            }
        });
        $("#carriover").on('change', function() {
            $("#btn-co").trigger('click');
        })
        //Carriover
        // $("#carriover").on('focusout',function(){
        //     //alert('entre');
        //       if($(this).val()==''){
        //         $("#btn-co").prop('disabled',true);
        //       }else{
        //         $("#btn-co").prop('disabled',false);
        //       }
        //    });

        //fin CO
        //NUEVA VERSIÓN
        $("#tecnicos option:not(:selected)").attr("disabled", true);
        $("#tecnicos_dias option:not(:selected)").attr("disabled", true);

    </script>
