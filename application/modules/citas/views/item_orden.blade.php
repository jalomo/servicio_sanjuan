<style type="text/css">
	.operaciones label{
		margin-right: 10px;
	}
	.operaciones{
		margin-right: 5px;
	}
	.tbl-operaciones th{
		text-align: center;
	}
	.tbl-operaciones th input{
		margin-bottom: 5px;
	}
	.mostrar{
		visibility: visible;
	}
	.ocultar{
		display: none;
	}

</style>
<form action="" id="order-item">
<input type="hidden" id="idorden" name="idorden" value="{{$idorden}}">
<input type="hidden" id="id_cita" name="id_cita" value="{{$id_cita}}">
<input type="hidden" id="tipo" name="tipo" value="{{$tipo}}">
<input type="hidden" id="temporal_save" name="temporal_save" value="{{$temporal_save}}">
<input type="hidden" id="idoperacion_save" name="idoperacion_save" value="{{$idoperacion_save}}">
<input type="hidden" id="id_status_op" name="id_status_op" value="{{$id_status}}">

@if(($cotizacion_extra||$idoperacion_save==0))
	<?php $disabled = 'disabled'; ?>
	@if($id_status==null || $id_status==1)
		<?php $disabled = ''; ?>
	@endif
@endif
<?php $disabled_o_e = ''; ?>
@if($idoperacion_save!=0)
	<?php $disabled_o_e = 'disabled'; ?>
@endif
@if($idorden!=0 && ($cotizacion_extra||!$idoperacion_save))
<label for="relacionar_op">¿Relacionar mano de obra con operaciones adicionales?</label> <input type="checkbox" name="relacionar_op" id="relacionar_op" {{$disabled}} {{$disabled_o_e}} {{($cotizacion_extra==1)?'checked':''}}>
@endif


<div class="row">
	<div class="col-sm-9 {{($cotizacion_extra==1)?'mostrar':'ocultar'}} " id="div_operaciones">
		<label class="text-center" for="">Operaciones extras</label> <br>
		<table class="table tbl-operaciones">
			<tr>
			@if(count($operaciones_extras)>0)
				@foreach($operaciones_extras as $o => $value)
				<th>
				<?php $check_op = ''; ?>
				@if($value->id_operacion==''||$value->id_operacion==0)
					<?php $check_op = ''; ?>
				@else
					<?php $check_op = 'checked'; ?>
				@endif
				<input class="operaciones" type="checkbox" id="opext-{{$value->id}}" name="opext[{{$value->id}}]" {{$check_op}} {{$disabled}} ><label for="opext-{{$value->id}}">{{$o+1}}){{$value->descripcion}}</label> 
				</th>
				@endforeach
			@else
				<th>No existen operaciones extras</th>
			@endif
			</tr>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-sm-2">
		<label for="">Código</label>
		{{$drop_codigo}}
		<span class="error error_codigo"></span>
	</div>
	<div class="col-sm-2">
		<label for="">Cantidad</label>
		{{$input_cantidad}}
		<span class="error error_cantidad_item"></span>
	</div>
	<div class="col-sm-4">
		<label for="">Descripción</label>
		{{$input_descripcion}}
		<span class="error error_descripcion_item"></span>
	</div>
	<div class="col-sm-2">
		<label for="">Precio</label>
		{{$input_precio}}
		<span class="error error_precio_item"></span>
	</div>
	<div class="col-sm-2">
		<label for="">Total horas</label>
		{{$input_total_horas}}
		<span class="error error_total_horas"></span>
	</div>
	
</div>
<div class="row ">
	<div class="col-sm-6"></div>
	<div class="col-sm-2">
		<label for="">Costo hora</label>
		{{$input_costo_hora}}
		<span class="error error_costo_hora"></span>
	</div>
	<div class="col-sm-2">
		<label for="">Descuento(%)</label>
		{{$input_descuento}}
		<span class="error error_descuento_item"></span>
	</div>
	<div class="col-sm-2">
		<label for="">Total</label>
		{{$input_total}}
		<span class="error error_total_item"></span>
	</div>
</div>
</form>

<script>
	$(".modal3").removeAttr('tabindex');
	$(".buscar").select2();
	$(".buscar").select2({ width: '100%' }); 
	$(".contar_item").on('change',function(){		
		var onChange = $(this).prop('name');
		$.each($(".contar_item"), function(i, item) {
			if(isNaN($(item).val()) || $(item).val()==''){
				if($(item).prop('name')=='total_horas'){
					$(item).val('1');
				}else{
					$(item).val('0');
				}
			}
        });

		var total_horas = parseFloat($("#total_horas").val());
		var precio_item = parseFloat($("#precio_item").val());

		if(onChange=='precio_item'){
			if(precio_item>0){
			  $("#total_horas").val(0)
			}
			
		}
		if(onChange=='total_horas'){
			if(total_horas>0){
				$("#precio_item").val(0)
			}
			
		}

		var total_horas = parseFloat($("#total_horas").val());
		var precio_item = parseFloat($("#precio_item").val());
        var costo_hora = parseFloat($("#costo_hora").val());
        var costo_total_horas = parseFloat(costo_hora)*parseFloat(total_horas);
        var cantidad_item = parseInt($("#cantidad_item").val());
        var descuento_item = parseFloat($("#descuento_item").val())/100;

        if(descuento_item>0){
        	var total = precio_item*cantidad_item;
        	var total = total-(total*descuento_item)
        }else{
        	var total = precio_item*cantidad_item;
        }
        total = total + costo_total_horas;

        total = (total + (total*.16)).toFixed(2);
        var total_item = $("#total_item").val(total);

	});
	$("#relacionar_op").on('click',function(){
		if($(this).prop('checked')){
			$("#div_operaciones").removeClass('ocultar');
			$("#div_operaciones").addClass('mostrar');
		}else{
			$("#div_operaciones").addClass('ocultar');
			$("#div_operaciones").removeClass('mostrar');
		}
	})
	$("#codigo").on('change',function(){
	 	var url=site_url+"/layout/getCodigoGenerico";
	 	if($(this).val()!=''){
		    ajaxJson(url,{"codigo":$(this).val()},"POST","",function(result){
		      result = JSON.parse( result );
		      if(result.exito){
		      	$("#total_horas").val(result.data.tiempo);
		      	//$("#precio_item").val(result.data.total);
		      	$("#costo_hora").val(result.data.mo);
		      	$("#descripcion_item").val(result.data.descripcion);
		      	$("#total_horas").trigger('change');
		      }
		    });
		}
	});
	//Si precio es diferente de 0 total horas va en 0 si total horas diferente de 0 precio = 0
	function validarCondicionantes(){
		var precio = parseFloat($("#precio_item").val());
		var total_horas = parseFloat($("#total_horas").val());
		console.log(precio);
		console.log(total_horas);

		if(precio>0){
			$("#total_horas").val(0);
		}else if(total_horas>0){
			$("#precio").val(0);
		}
		//$(".contar_item").trigger('change');
	}
	
</script>