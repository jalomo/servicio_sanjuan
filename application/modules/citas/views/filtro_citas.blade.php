@layout('tema_luna/layout')
<script type="text/javascript" src="https://unpkg.com/webcam-easy/dist/webcam-easy.min.js"> </script>
@section('contenido')
<style>
	/*#tabla {
    width: 70em;
    overflow-x: auto;
    white-space: nowrap;
	}*/
</style>
	<ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="#">Inicio</a>
	    </li>
	    <li class="breadcrumb-item active">Técnicos</li>
  	</ol>
	<div class="row">
		<div class="col-sm-12">
			<h1>Filtros</h1>
		</div>
	</div>
	<!-- <div class="row">
		<div class="video-wrap">
			<video id="video" playsinline autoplay></video>
			</div>
			<div class="controller">
			<button id="snap">Capture</button>
			</div>
			<canvas id="canvas" width="500" height="500"></canvas> 

	</div>	 -->	
	
	<form action="citas/exportar_citas" method="POST" id="frm">
		<input type="hidden" name="tipo" id="tipo" value="{{$tipo}}">
		<div class="row">
			<div class="col-sm-4 form-group">
				<label>Asesor</label><br>
				{{$drop_asesores}}
			</div>
			<div class="col-sm-4 form-group">
				<label>Estatus técnicos</label><br>
				{{$drop_status_tecnico}}
			</div>
		 	<div class='col-sm-4 form-group'>
	        	<label for="">Fecha</label>
	        		<input id="finicio" name="finicio" type='date' class="form-control" value="{{date('Y-m-d')}}" />
	             <span class="error_fecha"></span>
	        </div>
		</div>
		<div class="row">
			<div class="col-sm-12 text-right form-group">
				<button id="buscar" class="btn btn-success">Buscar</button>
				<button id="exportar" class="btn btn-info">Exportar a pdf</button>
				<!--<button id="excel" class="btn btn-info">Exportar a excel</button>-->
			</div>
		</div>
	</form>
		

	<br>
	<div class="row">
		<div class="col-sm-12">
			<div id="tabla">
		    	 {{$tabla}}
		   	</div>
		</div>
	</div>
	<br>
	<br>
	<br>
@endsection
@section('scripts')
<script>
	$(document).ready(function(){
		// const video = document.getElementById('video');
		// const snap = document.getElementById("snap");
		// const canvas = document.getElementById('canvas');
		// const errorMsgElement = document.querySelector('span#errorMsg');

		// const constraints = {
		// 	audio: false,
		// 	video: {
		// 		width: 800, height: 600
		// 	}
		// };

		// // Acceso a la webcam
		// async function init() {
		// 	try {
		// 		const stream = await navigator.mediaDevices.getUserMedia(constraints);
		// 		handleSuccess(stream);
		// 	} catch (e) {
		// 		errorMsgElement.innerHTML = `navigator.getUserMedia error:${e.toString()}`;
		// 	}
		// }
		// // Correcto!
		// function handleSuccess(stream) {
		// 	window.stream = stream;
		// 	video.srcObject = stream;
		// }
		// // Load init
		// init();
		// // Dibuja la imagen
		// var context = canvas.getContext('2d');
		// snap.addEventListener("click", function() {
		// 	context.drawImage(video, 0, 0, 640, 480);
		// 	var dataURL = canvas.toDataURL();
		// 	console.log(dataURL);
		// });
		$('.date').datetimepicker({
        	//minDate: moment(),
        	format: 'DD/MM/YYYY',
        	icons: {
                time: "far fa-clock",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
        	},
        	 locale: 'es'
        });
        $('.multiple').multiselect();
        $('body').on('click','.busquedalink',function(e){
			e.preventDefault();			
			url=$(this).prop('href');
			ajaxLoad(url,$("#frm").serialize(),"tabla","POST",function(){

			});
		});
		$("#buscar").on("click",function(e){
			e.preventDefault();
			url="<?php echo site_url('citas/buscar_citas') ?>";
			ajaxLoad(url,$("#frm").serialize(),"tabla","POST",function(){

			});
		});
		$('body').on('click','#exportar',function(){ 
			$("#frm").submit();
		});
		
	});
</script>
@endsection