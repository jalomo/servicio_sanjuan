@layout('tema_luna/layout')
@section('contenido')

<h1>Parámetro de tiempo</h1>
<form action="" id="frm">
<input type="hidden" name="fecha_tecnicos" id="fecha_tecnicos" value="{{$fecha_tecnicos}}">
<input type="hidden" name="fecha_asesores" id="fecha_asesores" value="{{$fecha_asesores}}">
<input type="hidden" name="valor" id="valor" value="{{$valor}}">
<div class="row">
		<div class="col-sm-3 col-sm-offset-1">
			<label for="">Cita</label>
			{{$tiempo_aumento}}
			<br>
			<button id="actualizar" class="btn btn-success">Actualizar</button>
		</div>
		<div class="col-sm-2">
			<label for="">Fecha a partir de (Técnicos)</label><br>
			{{$fecha_asesores}}
		</div>
		<div class="col-sm-2">
			<label for="">Fecha a partir de (Asesores)</label><br>
			{{$fecha_asesores}}
		</div>
	</div>
@endsection
@section('scripts')

<script>
	var site_url = "{{site_url()}}";
	var id_cita = '';
	var accion = '';
	 inicializar_tabla("#tbl_citas");
	 $("body").on("click",'#actualizar',function(e){
       e.preventDefault();
       var id = $(this).data('id')
       ConfirmCustom("¿Está seguro de cambiar el Parámetro de lapso entre horas?. Nota : esto tardará unos segundos, por favor espera", callbackEliminarCita,"", "Confirmar", "Cancelar");
      
    });
	function callbackEliminarCita(){
		var url =site_url+"/citas/generarhorariosAuxFormulario/";
		ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
			if(result ==0){
					ErrorCustom('Hubo un error al actualizar el parámetro, por favor intenta de nuevo.');
				}else{
					$("#cita").val("");
					ExitoCustom("Parámetros actualizados correctamente");

				}
		});
	}

</script>
@endsection