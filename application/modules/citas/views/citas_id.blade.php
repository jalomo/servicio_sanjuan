<div style="overflow-y: scroll;height: 400px;" class="row">
    <div class="col-sm-12">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#Cita</th>
                    <th>Técnico</th>
                    <th>Fecha</th>
                    <th>Inicio</th>
                    <th>Fin</th>
                    <th>Carryover</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tecnicos_citas as $t => $cita)
                    <tr>
                        <td>
                            <a data-fecha='{{date_eng2esp_1($cita->fecha)}}' href="#" class="js_select_cita">{{$cita->id_cita}}</a>
                        </td>
                        <td>{{$cita->tecnico}}</td>
                        <td>{{$cita->fecha}}</td>
                        <td>{{$cita->hora_inicio}}</td>
                        <td>{{$cita->hora_fin}}</td>
                        <td>No</td>
                    </tr>
                @endforeach
                @foreach($tecnicos_citas_historial as $t1 => $cita)
                    <tr>
                        <td>
                            <a data-fecha='{{date_eng2esp_1($cita->fecha)}}' href="#" class="js_select_cita">{{$cita->id_cita}}</a>
                        </td>
                        <td>{{$cita->tecnico}}</td>
                        <td>{{$cita->fecha}}</td>
                        <td>{{$cita->hora_inicio}}</td>
                        <td>{{$cita->hora_fin}}</td>
                        <td>{{($cita->carryover)?'Si':'No'}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>