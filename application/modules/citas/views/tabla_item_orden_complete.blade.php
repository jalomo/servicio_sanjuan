<form action="" id="frm-total-orden">
  <input type="hidden" name="id_orden_save" id="id_orden_save" value="{{$id_orden_save}}">
  <input type="hidden" name="total_temp_orden" id="total_temp_orden" value="">
  
<table class="table table-bordered table-striped" cellpadding="0" width="100%">
      <thead>
        <th>Acciones</th>
        <th>Descripción</th>
        <th>Precio</th>
        <th>Cantidad</th>
        <th>Descuento</th>
        <th>Total</th>
      </thead>
      <tbody>
        <?php $subtotal= 0;$total_con_iva = 0;$total_registro=0?>
        <?php $iva = 0; ?>
        @if(count($items)>0)
          @foreach($items as $i => $item)
          <!-- obtener los dos subtotales de grupo y generico -->
            <?php $clase = ''; ?>
            @if($item->no_planificado)
              <?php $clase = 'bg-gris'; ?>
            @elseif($item->id_tecnico==null)
              <?php $clase = 'bg-red'; ?>
            @else
              <?php $clase = 'bg-verde'; ?>
            @endif
            <tr class="{{$clase}}">
              <td>
                @if($item->operacion=='' && !$item->principal && $item->id_tecnico == '')
                @if(PermisoAccion('eliminar_operacion')||$id_orden_save==0)
                  <a href="#" data-iditem="{{$item->id}}" class="eliminar_item" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Eliminar" data-tipo="{{$item->tipo}}">
                    <i class="pe-7s-trash"></i>
                  </a>
                @endif
                @if($item->tipo!=5 && $item->id_tecnico == '')
                 @if(PermisoAccion('no_aplica_asignacion_operacion')||$id_orden_save==0)
                    <a href="#" class="js_no_aplica" data-idop="{{$item->id}}" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="No aplica asignación">
                    <i class="pe-7s-close"></i>
                  @endif
                @endif
                </a>
                @endif

                @if(PermisoAccion('editar_operacion')||$id_orden_save==0)
                  @if($item->tipo==5)
                    <a href="#" class="js_editar_preventivo" data-idop="{{$item->id}}" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar">
                    <i class="pe pe-7s-note"></i>
                  @elseif($item->tipo==4)
                    <a href="#" class="js_editar_correctivo" data-idop="{{$item->id}}" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar">
                    <i class="pe pe-7s-note"></i>
                  @elseif($item->tipo==3)
                    <a href="#" class="js_editar_gen" data-idop="{{$item->id}}" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar">
                    <i class="pe pe-7s-note"></i>
                  @endif
                @endif

                <!--editar tiempos operación -->
                @if(PermisoAccion('editar_hora_operacion')&&$id_orden_save!=0&&$item->id_tecnico!=null&&$item->id_status==1)
                    <a href="#" class="js_editar_horarios" data-idop="{{$item->id}}" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar horarios" data-principal="{{$item->principal}}" data-id_tecnico="{{$item->id_tecnico}}" data-idop="{{$item->id}}" data-idorden="{{$item->idorden}}">
                    <i class="pe pe-7s-clock"></i>
                @endif
                @if(PermisoAccion('')&&$id_orden_save!=0&&$item->folio_asociado==''&&!$item->no_planificado&&!$item->cancelado)
                  <a target="_blank" href="#" data-idop="{{$item->id}}" data-principal="{{$item->principal}}" data-idorden="{{$item->idorden}}" class="js_asociar_operacion" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Asociar operación F1863"><i class="pe-7s-back-2"></i></a>
                @else
                  <br>
                  <span>{{($item->folio_asociado)?'F1863:'.$item->folio_asociado:''}}</span> 
                @endif

              </td>

              <td>{{$item->descripcion}}</td>
              <td>{{number_format($item->precio_unitario,2)}}</td>
              <td>{{number_format($item->cantidad,2)}}</td>
              <td>{{number_format($item->descuento,2)}}</td>
              <td>
                 <?php (float)$total_con_iva = $total_con_iva+$item->total;?>
                @if($item->operacion=='')
                  <?php //$total_registro = (float)$item->total;
                    $total_registro = $item->total / 1.16
                  ?>
                @else
                <?php $total_registro = $item->total / 1.16 ?>
                @endif
                 <?php (float)$subtotal = $subtotal+$total_registro;?>
                 {{number_format($total_registro,2)}}
              </td>
            </tr>
          @endforeach

        <?php 
        //Obtengo el subtotal de cuando es tipo grupo, "al subtotal lo divido para tener el total sin iva"
        
        
        (float)$iva = $subtotal*.16;


        (float)$total = $subtotal+$iva;
        

        ?>
      </tbody>
      <tfoot>
        <tr class="text-right">
          <td colspan="5">
            <span>I.V.A.  :</span>
          </td>
          <td width="15%">
              <input type="text" name="iva_orden" id="iva_orden" value="{{number_format($iva,2)}}" class="form-control" readonly="">
          </td>
        </tr>
        <tr class="text-right">
          <td colspan="5">
            <span>Subtotal:</span>
          </td>
          <td width="15%">
            <input type="text" name="subtotal_orden" id="subtotal_orden" class="form-control" readonly="" value="{{number_format($subtotal,2)}}">
          </td>
        </tr>
        <tr class="text-right">
          <td colspan="5">
            <span>Anticipo:</span><br>
          </td>
          <td width="15%">
            <input type="number" name="anticipo_orden" id="anticipo_orden" value="{{number_format($anticipo,2)}}" class="form-control anticipo">
          </td>
        </tr>
        <tr class="text-right">
          <td colspan="5">
            <span>Total</span>
          </td>
          <td width="15%">
            <input type="text" name="total_orden" id="total_orden" value="{{$total}}" class="form-control" readonly="">
          </td>
        </tr>
      </tfoot>
      @else
        <tr class="text-center">
          <td colspan="6">No hay registros para mostrar...</td>
        </tr>
      @endif
    </table>
</form>
<script>
  $("#total_temp_orden").val($("#total_orden").val());
  $(".anticipo").on('change',function(){
    if($(this).val()=='' || parseFloat($(this).val())<0 ){
      var anticipo = 0;
      $(this).val(0)
    }else{
      var anticipo = parseFloat($(this).val());
    }
    $("#total_orden").val(total-anticipo);
    
    var total = parseFloat($("#total_temp_orden").val());
    if(anticipo<0 || anticipo==''){
      anticipo = 0;
    }
    if(anticipo>total){
      ErrorCustom("El anticipo no puede ser mayor al total");
      
    }else{
      $("#total_orden").val((total-anticipo).toFixed(2));
    }
  })
  $(".anticipo").trigger('change');
  

</script>