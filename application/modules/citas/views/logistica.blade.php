@layout('tema_luna/layout')
@section('contenido')

	<ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="#">Inicio</a>
	    </li>
	    <li class="breadcrumb-item active">Logística</li>
  	</ol>
  	<div class="row">
  		<div class="col-sm-8">
  			<h2>Logística</h2>
  		</div>
  		
  	</div>
  	
  	  
  	<hr>
  	<h3>Datos del Vehículo</h3>
  	<form action="" method="POST" id="frm">
  		<input type="hidden" name="id" id="id" value="{{$input_id}}">
  		
  	<div class="row">
      <div class="col-sm-2">
          <label for="">Entrega cliente</label>
          <div class="input-group clockpicker" data-autoclose="true">
              {{$input_entrega_cliente}}
              <span class="input-group-addon">
                  <span class="far fa-clock"></span>
              </span>
          </div>
          <span class="error_entrega_cliente"></span>
        </div>
  		<div class="col-sm-2 form-group">
  			<label for="">Eco</label>
  			{{$input_eco}}
  			<div class="error error_eco"></div>
  		</div>
      <div class="col-sm-2 form-group">
        <label for="">Unidad</label>
        {{$drop_id_unidad}}
        <div class="error error_id_unidad"></div>
      </div>
      <div class="col-sm-2 form-group">
        <label for="">Color</label>
        {{$drop_id_color}}
        <div class="error error_id_color"></div>
      </div>
      <div class="col-sm-2 form-group">
        <label for="">Serie</label>
        {{$input_serie}}
        <div class="error error_serie"></div>
      </div>
  	</div>

    <div class="row">
        <div class="col-sm-4">
           <label for="">Cliente</label>
          {{$input_cliente}}
          <div class="error error_cliente"></div>
        </div>
         <div class="col-sm-2">
           <label for="">Vendedor</label>
          {{$drop_id_vendedor}}
          <div class="error error_id_vendedor"></div>
        </div>
        <div class="col-sm-2">
           <label for="">Tipo de operación</label>
          {{$input_tipo_operacion}}
          <div class="error error_tipo_operacion"></div>
        </div>
        <div class="col-sm-2">
           <label for="">IVA DESG</label>
          {{$input_iva_desg}}
          <div class="error error_iva_desg"></div>
        </div>
         <div class="col-sm-2">
           <label for="">Toma</label>
          {{$input_toma}}
          <div class="error error_toma"></div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-2">
          <label for="">Hora promesa vendedor</label>
          <div class="input-group clockpicker" data-autoclose="true">
              {{$input_hora_promesa}}
              <span class="input-group-addon">
                  <span class="far fa-clock"></span>
              </span>
          </div>
          <div class="error error_hora_promesa"></div>
        </div>
         <div class="col-sm-4">
           <label for="">Ubicación</label>
          {{$input_ubicacion}}
          <div class="error error_ubicacion"></div>
        </div>
        <div class="col-sm-2">
           <label for="">Reprogramación</label><br>
          Si {{$input_reprogramacion}}
          <div class="error error_reprogramacion"></div>
        </div>
        <div class="col-sm-2">
           <label for="">Seguro</label><br>
          Si {{$input_seguro}}
          <div class="error error_seguro"></div>
        </div>
        <div class="col-sm-2">
           <label for="">Permiso</label><br>
          Si {{$input_permiso}}
          <div class="error error_permiso"></div>
        </div>
    </div>
    <br>
     <div class="row">
        <div class="col-sm-2">
           <label for="">Placas</label><br>
          Si {{$input_placas}}
          <div class="error error_placas"></div>
        </div>
        <div class="col-sm-4">
           <label for="">Accesorios</label><br>
          {{$input_accesorio}}
          <div class="error error_acessorio"></div>
        </div>
        <div class="col-sm-2">
           <label for="">Servicio</label><br>
          Si {{$input_servicio}}
          <div class="error error_servicio"></div>
        </div>
        <div class='col-sm-2'>
            <label for="">Fecha Prog</label>
            <div class="form-group1">
                <div class='input-group date' id='datetimepicker1'>
                    {{$input_fechaprog}}
                    <span class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                    </span>
                </div>
            </div>
             <span class="error_fechaprog"></span>
        </div>
        <div class="col-sm-2">
          <label for="">Hora programación</label>
          <div class="input-group clockpicker" data-autoclose="true">
              {{$input_horaprog}}
              <span class="input-group-addon">
                  <span class="far fa-clock"></span>
              </span>
          </div>
          <div class="error error_horaprog"></div>
        </div>
    </div>

  <br>
  	<div class="row pull-right">
  		<div class="col-sm-12 ">
        {{$btn_guardar}}
  		</div>
  	</div>
  	<br><br>
@endsection

@section('scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<script>
	var site_url = "{{site_url()}}";
  //$('.clockpicker').clockpicker();
  $('.date').datetimepicker({
      //minDate: moment(),
      format: 'DD/MM/YYYY',
      icons: {
          time: "far fa-clock",
          date: "fa fa-calendar",
          up: "fa fa-arrow-up",
          down: "fa fa-arrow-down"
      },
       locale: 'es'
    });
  $('.clockpicker').clockpicker();
	$(".busqueda").select2();
	$("#guardar").on('click',function(){
		 var url = site_url+'/citas/logistica/0';
        ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
                if(isNaN(result)){
                data = JSON.parse( result );
                //Se recorre el json y se coloca el error en la div correspondiente
                $.each(data, function(i, item) {
                    $(".error_"+i).empty();
                    $(".error_"+i).append(item);
                    $(".error_"+i).css("color","red");
                });
        }else{
        		if(result==1){
        			ExitoCustom("Guardado correctamente",function(){
        				window.location.href = site_url+'citas/ver_logistica';
        			});
        		}else{
                  ErrorCustom('No se pudo guardar, intenta otra vez.');
          	}
    		}
        });
	});
  $(".js_check").on("click",function(){
    if($(this).prop('checked')){
      $(this).val(1);
    }else{
       $(this).val(0);
    }
  });
</script>
@endsection