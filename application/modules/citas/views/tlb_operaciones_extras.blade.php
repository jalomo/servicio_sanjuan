<div class="row">
  <div class="col-sm-12">
    <table class="table">
        <thead>
          <tr>
            <th>Cantidad</th>
            <th>Descripción</th>
          </tr>
        </thead>
        <tbody>
          @if(count($operaciones_extras_orden)>0)

            @foreach($operaciones_extras_orden as $o => $operacion)
              @if($operacion->id_operacion==null||$operacion->id_operacion == 0)
              <?php $clase_op_ext = 'bg-red'; ?>
              @else
                <?php $clase_op_ext = 'bg-verde'; ?>
              @endif
              <tr class="{{$clase_op_ext}}">
                <td>{{$operacion->cantidad}}</td>
                <td>{{$operacion->descripcion}}</td>
              </tr>
            @endforeach
          @else
            <tr>
              <td class="text-center" colspan="2">No se tienen registradas operaciones extras</td>
            </tr>
          @endif
        </tbody>
    </table>
  </div>
</div>