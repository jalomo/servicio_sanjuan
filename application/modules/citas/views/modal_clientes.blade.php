<div class="row">
	<div class="col-sm-12">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Unidad</th>
					<th>Plcas</th>
					<th>Acción</th>
				</tr>
			</thead>
			<tbody>
				@if(count($datos_clientes)>0)
				@foreach($datos_clientes as $c =>$cliente)
					<tr>
						<td>{{$cliente->articulo_servicio}}</td>
						<td>{{$cliente->placas}}</td>
						<td>
							<a class="seleccionar_unidad" href="#" data-id="{{$cliente->id}}">Seleccionar</a>
						</td>
					</tr>
				@endforeach
				@else
					<tr class="text-center">
						<td colspan="3">No hay registros para mostrar...</td>
					</tr>
				@endif
				
			</tbody>
		</table>
	</div>
</div>