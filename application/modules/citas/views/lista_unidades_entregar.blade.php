@layout('tema_luna/layout')
<style>
	.gris{
		color: #000;
	}
	.cargreen{
		color: #1F7D31 !important;
		background-color: transparent
	}
	.desactivado{
		background-color: #7576F6;
		color: #FFF;
		font-weight: bold;
	}
	.activo{
		background-color: white;
		color: #000;
		font-weight: bold;
	}
</style>
@section('contenido')
	<h1>Lista de unidades pendientes por entregar </h1>
	<div class="row">
		<div class="col-sm-12">
			<table id="tbl_citas" class="table table-hover table-striped">
				<thead>
					<tr class="tr_principal">
						<th>ID</th>
						<th>Fecha de cita</th>
						<th>Nombre del cliente</th>
						<th>Teléfono del cliente</th>
						<th>Comentarios</th>
						<th>Técnico</th>
						<th>Placas</th>
						<th>Modelo</th>
						<th>Asesor</th>
						<th>Servicio</th>
						<th>Origen</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($citas as $c => $value)
						<tr>
							<?php $fecha_mostrar = $value->fecha_dia.'/'.$value->fecha_mes.'/'.$value->fecha_anio;
							$fecha_comparar = $value->fecha_anio.'-'.$value->fecha_mes.'-'.$value->fecha_dia; 
							 ?>
							<td>{{$value->id_cita}}</td>
							
							<td>{{$fecha_mostrar.' '.$value->hora}}
							 	
							</td>
							<td>{{$value->datos_nombres.' '.$value->datos_apellido_paterno.' '.$value->datos_apellido_materno}}</td>
							<td>{{$value->datos_telefono}}</td>
							<td>{{$value->comentarios_servicio}}</td>
							<td>{{$value->nombre}}</td>
							<td>{{$value->vehiculo_placas}}</td>
							<td><span title="{{$value->vehiculo_modelo}}">{{trim_text($value->vehiculo_modelo,1)}}</span></td>
							<td>{{$value->asesor}}</td>
							<td>{{$value->servicio}}</td>
							<td>
								@if($value->origen==0)
									Panel
								@elseif($value->origen==1)
									Aplicación
								@else
									Tablero
								@endif
							</td>
							<td>

								<?php $clase = ($value->id_status_cita==5)?"cargreen":"gris js_cambiar_status"; ?>
                      			
                      			<a style="cursor: pointer;" data-id="{{$value->id_horario}}" class="fas fa-car {{$clase}} elemento_{{$value->id_horario}}"  aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Unidad entregada" data-status="5"></a>
								
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection
@section('scripts')

<script>
	var site_url = "{{site_url()}}";
	var id_cita = '';
	var accion = '';
	var valor = '';
	var aPos = '';
	inicializar_tabla("#tbl_citas");
	$("body").on('click','.js_cambiar_status',function(e){
      	e.preventDefault();
      	var fecha_actual = "{{date('d/m/Y')}}";
		var fecha =$("#fecha").val();
			id = $(this).data('id')
  		status = $(this).data('status')
  		aPos = $(this);
		ConfirmCustom("¿Está seguro de cambiar el estatus a la cita?", callbackCambiarStatus,"", "Confirmar", "Cancelar");
      });
	function callbackCambiarStatus(){
		var url =site_url+"/citas/cambiar_status_cita/";
		ajaxJson(url,{"id":id,"status":status},"POST","",function(result){
			if(result ==0){
					ErrorCustom('No se pudo cambiar el estatus, por favor intenta de nuevo');
				}else{
					ExitoCustom("Estatus cambiado correctamente",function(){
						//buscar();
						$(".elemento_"+id).removeClass('cargreen');
						$(aPos).addClass('cargreen');
					});	
				}
		});
	}  
</script>
@endsection