@layout('tema_luna/layout')
@section('css_vista')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('contenido')
    <form action="" id="frm">
        <div class="row">
          
            <div class="col-sm-3">
              <label for="">Buscar por campo</label>
              <input class="form-control" type="text" name="buscar_campo" id="buscar_campo">
            </div>
            <div class="col-sm-2" style="margin-top:30px;">
                <button type="button" id="buscar" name="buscar" class="btn btn-info pull-right">Buscar</button>
            </div>
        </div>
        
        
    </form>
    <br>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-bordered table-striped table-hover"  id="tabla" class="display" style="width:100%">
            <thead>
                <tr class="tr_principal">
                  <th>ID</th>
                  <th>Transporte</th>
                  <th>Comentarios</th>
                  <th>Año del vehículo</th>
                  <th>Modelo</th>
                  <th>Placas</th>
                  <th># de Serie</th>
                  <th>Servicio</th>
                  <th>Operador</th>
                  <th>Técnico</th>
                  <th>Fecha de cita</th>
                  <th>Nombre</th>
                  <th>Apellido Paterno</th>
                  <th>Apellido Materno</th>
                  <th>Teléfono</th>
                  <th>Donde realizó cita</th>
                  <th>Color</th>
                  <th>Origen</th>
                  <th>Reagendada</th>
                  <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
              
            </tbody>
        </table>
    </div>
  </div>
@endsection



@section('scripts')

<script>
  var site_url = "{{site_url()}}";
  var id_cita = '';
  var accion = '';
  var valor = '';
  var aPos = '';
  var site_url = "{{site_url()}}";
    Vals = '';
    $('.date').datetimepicker({
      format: 'DD/MM/YYYY',
      icons: {
          time: "far fa-clock",
          date: "fa fa-calendar",
          up: "fa fa-arrow-up",
          down: "fa fa-arrow-down"
      },
       locale: 'es'
    });
  $(document).ready(function() {
    iniciarTabla();
    $("body").on("click",'.editar_cita',function(e){
      e.preventDefault();
      id_cita = $(this).data('id_cita');
      accion = "editar";
      var url =site_url+"/citas/login_editar_cita/0";
      customModal(url,{},"GET","md",ValidarLogin,"","Ingresar","Cancelar","Editar Cita","modal1");
    }); 
    $("body").on("click",'.js_cancelar',function(e){
      e.preventDefault();
      id_cita = $(this).data('id');
      ConfirmCustom("¿Está seguro de cancelar la cita?", callbackCancelarCita,"", "Confirmar", "Cancelar");
    }); 
    $("body").on("click",'.js_eliminar',function(e){
      e.preventDefault();
      id_cita = $(this).data('id');
      ConfirmCustom("¿Está seguro de eliminar la cita?", callbackEliminarCita,"", "Confirmar", "Cancelar");
    }); 
    $("body").on("click",'.js_confirmar',function(e){
      e.preventDefault();
      //valor es 0 cuando va rechazar y 1 cuando lo va confirmar
      aPos = $(this);
      valor = $(this).data('valor');
      if(valor==1){
        mensaje ="¿Está seguro de confirmar la cita?";
      }else{
        mensaje ="¿Está seguro de poner como no confirmada la cita?";
      }
      id_cita = $(this).data('id');
      ConfirmCustom(mensaje, confirmar_rechazar,"", "Confirmar", "Cancelar");
    });

    $("#buscar").on('click',function(){
      $(".error").empty();
      var tipo_busqueda = $("#tipo_busqueda").val();
      var finicio = $("#finicio").val();
      var ffin = $("#ffin").val();

      if(tipo_busqueda!=''){
        if(finicio==''){

        }else if(ffin==''){
          $(".error_ffin").text('El campo es requerido');
        }else{
          var table = $('#tabla').DataTable();
          table.destroy();
          iniciarTabla();
        }
      }else{
        var table = $('#tabla').DataTable();
        table.destroy();
        iniciarTabla();
      }

    });
    $("div.dataTables_filter input").on('change',function(e){
      alert('entre');
    });

  }); 
  
    function confirmar_rechazar(){
    var url =site_url+"/citas/confirmar_rechazar/";
    ajaxJson(url,{"id":id_cita,"valor":valor},"POST","",function(result){
      if(result ==0){
          ErrorCustom('Error de petición, por favor intenta de nuevo');
        }else{
          ExitoCustom("Registro actualizado correctamente.",function(){
          $(".close").trigger("click");
            if(valor==1){
              //la está confirmando
              $(aPos).data('valor',0);
              $(aPos).attr('title','Confirmar');
              $(aPos).removeClass('fa-square-o');
              $(aPos).addClass('fa-check-square-o');
              $(aPos).attr('data-original-title','Confirmar');
              

            }else{
              //la está rechazando
              $(aPos).data('valor',1);
              $(aPos).attr('title','No confirmada');
              $(aPos).removeClass('fa-check-square-o');
              $(aPos).addClass('fa-square-o');
              $(aPos).attr('data-original-title','No confirmada');

            }
            $('[data-toggle="tooltip"]').tooltip()
          }); 
          
        }
    });
  }
  function callbackEliminarCita(){
    var url =site_url+"/citas/eliminar_cita_bd/";
    ajaxJson(url,{"id":id_cita},"POST","",function(result){
      if(result ==0){
          ErrorCustom('La cita no se pudo eliminar, por favor intenta de nuevo');
        }else{
          ExitoCustom("Cita eliminada correctamente",function(){
            window.location.href = site_url+"/citas/ver_citas";
          }); 
        }
    });
  }

  function ValidarLogin(){
      var url =site_url+"/citas/login_editar_cita";
      ajaxJson(url,{"usuario":$("#usuario").val(),"password":$("#password").val()},"POST","",function(result){
        if(isNaN(result)){
          data = JSON.parse( result );
          //Se recorre el json y se coloca el error en la div correspondiente
          $.each(data, function(i, item) {
             $.each(data, function(i, item) {
                        $(".error_"+i).empty();
                        $(".error_"+i).append(item);
                        $(".error_"+i).css("color","red");
                    });
          });
        }else{
          if(result <0){
            ErrorCustom('Usuario o contraseña inválidos.');
          }else if(result==0){
            ErrorCustom('El usuario no es administrador');
          }else{
            if(accion=='editar'){
              window.location.href = site_url+'/citas/agendar_cita/'+id_cita;
            }else{
              //var perfil = "{{$this->session->userdata('tipo_perfil')}}";
              //alert(perfil);
              callbackEliminarCita();
            }
            
            
          }
        }
      });
    }
  function ValidarLogin_cancelar(){
      var url =site_url+"/citas/login_editar_cita_cancelar_eliminar";
      ajaxJson(url,{"usuario":$("#usuario").val(),"password":$("#password").val()},"POST","",function(result){
        if(isNaN(result)){
          data = JSON.parse( result );
          //Se recorre el json y se coloca el error en la div correspondiente
          $.each(data, function(i, item) {
             $.each(data, function(i, item) {
                        $(".error_"+i).empty();
                        $(".error_"+i).append(item);
                        $(".error_"+i).css("color","red");
                    });
          });
        }else{
          if(result <0){
            ErrorCustom('Usuario o contraseña inválidos.');
          }else if(result==1){
            callbackCancelarCita();
            
          }else{
            ErrorCustom('El usuario no tiene permisos para cancelar una cita');
          }
        }
      });
    }
  function ValidarLogin_eliminar(){
      var url =site_url+"/citas/login_editar_cita_cancelar_eliminar";
      ajaxJson(url,{"usuario":$("#usuario").val(),"password":$("#password").val()},"POST","",function(result){
        if(isNaN(result)){
          data = JSON.parse( result );
          //Se recorre el json y se coloca el error en la div correspondiente
          $.each(data, function(i, item) {
             $.each(data, function(i, item) {
                        $(".error_"+i).empty();
                        $(".error_"+i).append(item);
                        $(".error_"+i).css("color","red");
                    });
          });
        }else{
          if(result <0){
            ErrorCustom('Usuario o contraseña inválidos.');
          }else if(result==1){
            callbackEliminarCita();
            
          }else{
            ErrorCustom('El usuario no tiene permisos para eliminar una cita');
          }
        }
      });
    }
  function callbackCancelarCita(){
    var url =site_url+"/citas/cancelar_cita/";
    ajaxJson(url,{"id":id_cita},"POST","",function(result){
      if(result ==0){
          ErrorCustom('La cita no se pudo cancelar, por favor intenta de nuevo');
        }else{
          ExitoCustom("Cita cancelada correctamente",function(){
            window.location.href = site_url+"/citas/ver_citas";
          }); 
        }
    });
  }
        function iniciarTabla(){

            var tabla = $("#tabla").dataTable({
                paging: true,
                bFilter: false,
                processing: true,
                responsive: true,
                serverSide:true,
                ajax: {
                  url: site_url+"/citas/getDatosAllCitas",
                  type: 'POST',
                   //////////////////////////////////////////////////////////////// AQUI PUEDO AGREGAR MAS CAMPOS EN LA PETICION 
                  data: function(data){
                     data.fecha_inicio = $("#finicio").val();
                     data.fecha_fin = $("#ffin").val();
                     data.tipo_busqueda = $("#tipo_busqueda").val();
                     data.buscar_campo = $("#buscar_campo").val();
                    // if(filtros.pagina){
                    //   data.start = parseInt(filtros.pagina);
                    // }
                    empieza = data.start;
                    por_pagina = data.length;
                  }
                },
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "oLanguage": {
                   "oPaginate": {
                       "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                       "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                    '<option value="5">5</option>' +
                    '<option value="10">10</option>' +
                    '<option value="20">20</option>' +
                   '<option value="30">30</option>' +
                    '<option value="40">40</option>' +
                    '<option value="50">50</option>' +
                    '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "mensaje",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar"
                },
            });
        }

</script>
@endsection