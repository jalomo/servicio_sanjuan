<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <base href="{{base_url()}}">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Ford Plasencia</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">

   <!-- CSS CUSTOM-->
  <link href="css/custom/bootstrap-datetimepicker.css" rel="stylesheet">
  <!--<link href="css/custom/datepicker3.css" rel="stylesheet">-->
  <link href="css/custom/isloading.css" rel="stylesheet">
  <link href="css/custom/style.css" rel="stylesheet">
  <link href="css/custom/bootstrap-switch.css" rel="stylesheet">
  <link href="css/custom/clockpicker.css" rel="stylesheet">
  <style>
	    .open-bold{
	      font-family: 'OpenSans-bold' !important;
	      font-weight: bold;
	    }
		.text-center{
			text-align: center;
		}
		.logo{
			width: 200px;
			height: 60px;
		}
		.logo-round{
			width: 100px;
			height: 40px;
		}
		.color-blue{
			color: #0F2D63 !important;
		}
		.justify{
			text-align: justify !important;
		}
		.fs18{
			font-size: 18px !important;
		}
		.fs16{
			font-size: 16px !important;
		}
		.fs14{
			font-size: 14px !important;
		}
		.fs12{
			font-size: 12px !important;
		}
		.fs10{
			font-size: 10px !important;
		}
		.fs9{
			font-size: 9px !important;
		}
		.fs8{
			font-size: 8px !important;
		}
		.fs6{
			font-size: 6px !important;
		}
		.fs2{
			font-size: 2px !important;
		}
		.domicilio{
			line-height: 12px;
		}
		.orden{
			border: 1px solid #0F2D63;
    		padding: 10px;
		}
		.mt10{
			margin-top: 10px;
		}

		.borde {
		    width: 30%;
		    border: solid 1px #0F2D63;
		}
		.table3{
			width: 100%;
			padding-right: 20px;
		}
		table{
			/*border-spacing: 0px !important;*/
		}
		.w100{
			width: 100%
		}
		.w90{
			width: 90%
		}
		.w80{
			width: 80%
		}
		.w50{
			width: 50%
		}
		.w33{
			width: 33%
		}
		.w25{
			width: 25%
		}
		.w10{
			width: 10%
		}
		.descripcion{
			font-size: 10px;
		}
		.padding-textos{
			padding-left: 9px !important;
			padding-right: 9px !important;
			padding-bottom: 9px !important;
			padding-top: 9px !important;
		}
		table tr td{
			padding-left:3px;
		}
		.negritas{
			font-weight: bold;
		}
	  	.circulo {
		     width: 50px;
		     height: 50px;
		     -moz-border-radius: 50%;
		     -webkit-border-radius: 50%;
		     border-radius: 50%;
		     float: left;
		}
		table thead th{
			text-align: left;
		}
	</style>
</head>
<body class="container">
	<br>
	<input type="hidden" name="idorden" id="idorden" value="{{$idorden}}">
	<input type="hidden" name="confirmada" id="confirmada" value="{{$confirmada}}">
	<h1 class="text-center">FORD PLASENCIA MAZATLÁN</h1>
	<div class="alert alert-primary" role="alert">
	  Visualiza la cotización y da clic en "Aceptar cotización" o "Rechazar cotización"
	</div>
 	<br>
 	<table width="100%" class="table table-bordered table-striped table-striped table-responsive">
      <thead>
      	<th>Cantidad</th>
        <th>Descripción</th>
        <th># Pieza</th>
        <th>Costo</th>
        <th>Horas</th>
        <th>Total</th>
      </thead>
      <tbody>
        <?php $subtotal = 0 ?>
        <?php $iva = 0 ?>
        <?php $total = 0 ?>
        @if(count($items)>0)
          @foreach($items as $i => $item)
          <?php $subtotal = $subtotal+ $item->cantidad*($item->total*$item->total_horas)+$item->costo_hora ?>
            <tr>
              <td width="15%">{{number_format($item->cantidad,2)}}</td>
              <td width="25%">{{$item->descripcion}}</td>
              <td width="10%">{{$item->numero_pieza}}</td>
              <td width="15%">{{number_format($item->costo_hora,2)}}</td>
              <td width="15%">{{number_format($item->total_horas,2)}}</td>
              <td width="15%">{{number_format($item->total,2)}}</td>
            </tr>
          @endforeach
          <?php (float)$iva =(float)$subtotal * .16?>
        <?php (float)$total =(float)$subtotal+(float)$iva?>
      </tbody>
      <tfoot>
        <tr class="">
          <td colspan="5">
            <span>I.V.A. :</span>
          </td>
          <td width="15%">
            {{number_format($iva,2)}}
            <input type="hidden" id="iva" name="iva" value="{{$iva}}">
          </td>
        </tr>
        <tr class="">
          <td colspan="5">
            <span>Subtotal:</span>
          </td>
          <td width="15%">
             {{number_format($subtotal,2)}}
          </td>
        </tr>
        <tr class="">
          <td colspan="5">
            <span>Total</span>
          </td>
          <td width="15%">
           <span>{{number_format($total-$anticipo,2)}}</span>
           <input type="hidden" id="total" name="total" value="{{$total-$anticipo}}">
          </td>
        </tr>
      </tfoot>
      @else
        <tr class="text-center">
          <td colspan="7">No hay registros para mostrar...</td>
        </tr>
      @endif
    </table>

    @if(!$confirmada)
    <div class="row text-right">
    	<div class="col-sm-12">
    		<button id="rechazar" class="btn btn-danger">Rechazar cotización</button>
    		<button id="aceptar" class="btn btn-success">Aceptar cotización</button>

    	</div>
    </div>
    @endif
	<script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
 	<script src="js/custom/bootbox.min.js"></script>
 	<script src="js/custom/general.js"></script>
    <script src="js/custom/isloading.js"></script>
    <script>
    	var site_url = "{{site_url()}}";
    	var status = '';
    	$("#aceptar").on("click",function(){
    		status = 1;
    		ConfirmCustom("¿Está seguro de aceptar la cotización? NO podrás cambiar de decisión", callbackCambiar,"", "Confirmar", "Cancelar");
    	});
    	$("#rechazar").on("click",function(){
    		status = 2;
    		ConfirmCustom("¿Está seguro de rechazar la cotización? NO podrás cambiar de decisión", callbackCambiar,"", "Confirmar", "Cancelar");
    	});
    	function callbackCambiar(){

	      ajaxJson(site_url+"/citas/EstatusCotizacion",{"idorden":$("#idorden").val(),"status":status,"iva":$("#iva").val(),"total":$("#total").val()},"POST","async",function(result){
			if(result ==0){
					ErrorCustom('Error al actualizar, por favor intenta de nuevo');
				}else{
					ExitoCustom("Cotización actualizada correctamente",function(){
						location.reload();
					});	
				}
		});
    	}
    </script>
</body>
</html>

