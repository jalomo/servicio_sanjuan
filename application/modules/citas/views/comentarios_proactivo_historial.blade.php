<form action="" id="frm_comentarios">
	<input type="hidden" name="id_cita" id="id_cita" value="{{$id_cita}}">
	<div class="row">
		<div class="col-sm-12">
			<div class="form-group">
				<label>Titulo</label>
				<input class="form-control" name="cronoTitulo" />
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-6">
			<label for="">Fecha inicio</label>
			<input type="date" class="form-control" value="{{date2sql(date('d/m/Y'))}}" name="cronoFecha">
		</div>
		<div class="col-lg-6">
			<label for="">Hora</label>
			<input type="time" class="form-control" value="" name="cronoHora">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<label>Comentario</label>
			{{$input_comentario}}
			<span class="error error_comentario"></span>
		</div>
	</div>
</form>