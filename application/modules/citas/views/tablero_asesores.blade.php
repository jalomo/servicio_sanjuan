<!DOCTYPE html>
<html>

<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <base href="{{ base_url() }}">
    @include('tema_luna/head')
    <script type="text/javascript">
        var PATH = "<?php echo site_url(); ?>";
        var PATH_BASE = "<?php echo base_url(); ?>";
        var PATH_LANGUAGE = "<?php echo base_url('js/spanish.json'); ?>";
        var CONTROLLER = "<?php echo $this->router->class; ?>";
        var FUNCTION = "<?php echo $this->router->method; ?>";
    </script>
    <style>
        .gris {
            color: #000;
        }

        .verde {
            color: #25CA15;
            background-color: transparent
        }

        body {
            background-color: #BDBDBD;
            font-weight: bold;
        }

        .container-fluid {
            color: #000;
        }

        .desactivado {
            background-color: #7576F6;
            color: #FFF;
            font-weight: bold;
        }

        .activo {
            background-color: white;
            color: #000;
            font-weight: bold;
        }

        .content-wrapper {
            margin-left: 0px !important;
        }

        @media (min-width: 992px) {
            body {
                font-size: 12px !important;
            }
        }

        .pe {
            font-weight: bold
        }
        .scroll{
            overflow: auto;
            overflow-y: hidden;
        }

    </style>
</head>

<body>
    <!-- Wrapper-->
    <div class="wrapper">
        <section class="content" style="margin-left:10px;margin-top:10px;">
            <div class="container-fluid">
                <div class="container-fluid panel-body" style="background-color: #fff !important">
                    <div class="row">
                        <div class="col-sm-12">
                            <span style="font-size: 30px;font-weight: bold;text-align: center;">Tablero de citas por
                                asesor</span>
                            <a style="color: #fff;" href="citas/cerrar_sesion_asesor"
                                class="pull-right cerrar_sesion">Cerrar Sesión</a>
                        </div>
                    </div>
                    <br>
                    <div class="row form-group">
                        <div class='col-sm-3'>
                            <label for="">Selecciona la fecha</label>
                            <input id="fecha" name="fecha" type='date' class="form-control"
                                value="{{ $fecha }}" />
                            <span class="error_fecha"></span>
                        </div>
                        <div class="col-sm-2">
                            <br>
                            <button id="buscar" style="margin-top: 8px;" class="btn btn-success">Buscar</button>
                        </div>
                    </div>
                    <br>
                    <div id="div_tabla">
                        <div class="row">
                                <div class="col-sm-6 scroll">
                                    {{ $tabla1 }}
                                </div> <!-- col-md-6 -->

                                <div class="col-sm-6 scroll">
                                    {{ $tabla2 }}
                                </div> <!-- col-md-6 -->
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
    <!-- End main content-->
    </div>
    <!-- End wrapper-->
    <!-- Vendor scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    @include('tema_luna/dependencias_footer')
    <script type="text/javascript">
        var site_url = "{{ site_url() }}";
        var id = '';
        var status = '';
        var aPos = '';
        var fecha_cita = $("#fecha").val();
        var fecha_comparar = "{{ date('Y-m-d') }}";
        var aux1 = $(window).height(); //solo la pantalla
        var aux2 = $(document).height();
        var inicio = 100;
        var heightPage = $(document).height();
        $(function() {
            $('#datetimepicker1').datetimepicker({
                //minDate: moment(),
                format: 'DD/MM/YYYY',
                icons: {
                    time: "far fa-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                locale: 'es'
            });
            $("#buscar").on('click', buscar);
        });
        if (fecha_cita == fecha_comparar) {
            $("body").on('click', '.js_cambiar_status', function(e) {
                e.preventDefault();
                if (!$(this).hasClass('verde')) {
                    var unidad_entregada = $(this).data('unidadentregada');
                    var id_status_cita = $(this).data(
                        'idstatus'); //5 marcó que el cliente no llegó el técnico pero se puede devolver
                    status = $(this).data('status')
                    id = $(this).data('id')
                    id_cita = $(this).data('idcita')
                    aPos = $(this);
                    ajaxJson(site_url + "/citas/existeOrdebByHorario", {
                        "idhorario": $(this).data('id')
                    }, "POST", "", function(result) {
                        if (result > 0 && status != 5) {
                            ErrorCustom(
                                'No se puede hacer el cambio de estatus por que ya existe una orden'
                            );
                        } else {
                            if (id_status_cita == 1 || status == 5 || id_status_cita == 5) {
                                var fecha_actual = "{{ date('Y-m-d') }}";
                                var fecha = $("#fecha").val();
                                if (fecha_actual == fecha || id_status_cita == 9 || id_status_cita ==
                                    11) {
                                    if (unidad_entregada) {
                                        ErrorCustom("La unidad ya fue entregada");
                                    } else {
                                        ConfirmCustom("¿Está seguro de cambiar el estatus a la cita?",
                                            callbackCambiarStatus, "", "Confirmar", "Cancelar");
                                    }
                                } else {
                                    ErrorCustom(
                                        "Solamente puedes hacer check-in el día en que se generó la cita"
                                    );
                                }
                            } else {
                                ErrorCustom("Ya fue realizado el check-in anteriormente");
                            }
                        }
                    });

                }

            });
        } else {
            $("body").on('click', '.js_cambiar_status', function(e) {
                e.preventDefault();
                ErrorCustom("Sólo puedes hacer check-in el día actual de la cita.")
            });
        }

        function buscar() {
            var url = site_url + "/citas/tabla_horarios_asesores";
            ajaxLoad(url, {
                "fecha": $("#fecha").val()
            }, "div_tabla", "POST", function() {});
        }

        function callbackCambiarStatus() {
            var url = site_url + "/citas/cambiar_status_cita/";
            ajaxJson(url, {
                "id": id,
                "status": status,
                "id_cita": id_cita
            }, "POST", "async", function(result) {
                if (result == 0) {
                    ErrorCustom('No se pudo cambiar el estatus, por favor intenta de nuevo');
                } else if (result == -1) {
                    ErrorCustom("No se pudo cambiar el estatus, ya existe una cita");
                } else if (result == -2) {
                    ErrorCustom("Solamente puedes entregar una unidad si ya está cerrada la orden");
                } else if (result == -3) {
                    ErrorCustom("No se puede entregar la unidad por que no existe la firma del cliente");
                } else {
                    ExitoCustom("Estatus cambiado correctamente", function() {
                        //buscar();
                        $(".elemento_" + id).removeClass('verde');
                        $(aPos).addClass('verde');
                    });
                }
            });
        }
        // setInterval(function(){
        // 	window.scrollTo(0, inicio);
        // 	if(inicio > aux2){
        // 		inicio = 0;
        // 	}else{
        // 		inicio = inicio +800;
        // 	}

        // },10000);

        // $(window).scroll(function(){
        //     if(($(window).scrollTop() + $(window).height()) == heightPage) {
        //             buscar();
        //     }      
        // });
        // setInterval(buscar,100000);
    </script>
</body>

</html>
