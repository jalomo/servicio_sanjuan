<table width="100%" class="table table-bordered table-striped table-striped table-responsive">
      <thead>
      	<th>Acción</th>
      	<th>Cantidad</th>
        <th>Descripción</th>
        <th># Pieza</th>
        <th>Costo</th>
        <th>Horas</th>
        <th>Total</th>
      </thead>
      <tbody>
        <?php $subtotal = 0 ?>
        <?php $iva = 0 ?>
        <?php $total = 0 ?>
        @if(count($items)>0)
          @foreach($items as $i => $item)
          <?php $subtotal = $subtotal+ $item->cantidad*($item->total*$item->total_horas)+$item->costo_hora ?>
            <tr>
            	<td>
                <a href="#" data-iditem="{{$item->id}}" class="pe pe-7s-trash eliminar_item" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Eliminar" data-tipo="{{$item->tipo}}"></a>
              </td>
              <td width="15%">{{number_format($item->cantidad,2)}}</td>
              <td width="25%">{{$item->descripcion}}</td>
              <td width="10%">{{$item->numero_pieza}}</td>
              <td width="15%">{{number_format($item->costo_hora,2)}}</td>
              <td width="15%">{{number_format($item->total_horas,2)}}</td>
              <td width="15%">{{number_format($item->total,2)}}</td>
            </tr>
          @endforeach
          <?php (float)$iva =(float)$subtotal * .16?>
        <?php (float)$total =(float)$subtotal+(float)$iva?>
      </tbody>
      <tfoot>
        <tr class="">
          <td colspan="6">
            <span>I.V.A. :</span>
          </td>
          <td width="15%">
            {{number_format($iva,2)}}
            <input type="hidden" id="iva" name="iva" value="{{$iva}}">
          </td>
        </tr>
        <tr class="">
          <td colspan="6">
            <span>Subtotal:</span>
          </td>
          <td width="15%">
             {{number_format($subtotal,2)}}
          </td>
        </tr>
        <tr class="">
          <td colspan="6">
            <span>Total</span>
          </td>
          <td width="15%">
           <span>{{number_format($total-$anticipo,2)}}</span>
           <input type="hidden" id="total" name="total" value="{{$total-$anticipo}}">
          </td>
        </tr>
      </tfoot>
      @else
        <tr class="text-center">
          <td colspan="8">No hay registros para mostrar...</td>
        </tr>
      @endif
    </table>

<script>
	$("body").on('click',".eliminar_item",function(e){
		e.preventDefault();
		item = $(this).data('iditem');
		tipo = $(this).data('tipo');
		ConfirmCustom("¿Está seguro de eliminar el registro?", eliminarItem,"", "Confirmar", "Cancelar");
	});
function eliminarItem(){
   var url = site_url+'/citas/eliminarItem';
    
        ajaxJson(url,{"item":item,"tipo":tipo},"POST","async",function(result){
            if(result==1){
              ExitoCustom("Eliminado correctamente",function(){
                buscarItems();

              });
            }else{
                  ErrorCustom('No se pudo eliminar, intenta otra vez.');
            }
        });
   }
</script>