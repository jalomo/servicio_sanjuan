<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<base href="{{base_url()}}">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>{{CONST_AGENCIA}}</title>

	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


	<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

	<link href="css/sb-admin.css" rel="stylesheet">


	<link href="css/custom/bootstrap-datetimepicker.css" rel="stylesheet">

	<link href="css/custom/isloading.css" rel="stylesheet">
	<link href="css/custom/style.css" rel="stylesheet">
	<link href="css/custom/bootstrap-switch.css" rel="stylesheet">
	<link href="css/custom/clockpicker.css" rel="stylesheet">
	
	<style>
		body{
		    background: #BDBDBD;
		    background: url("img/logo_opacidad.png");
		    background-repeat: no-repeat;
		    background-position: center center;
		    background-attachment: fixed 
		}
		.progress-bar{
			font-size: 35px;
			padding-top: 15px;
		}
	</style>
</head>

<body class="sticky-footer" id="page-top">
	<div class="container-fluid">
		<style>
		#div_tabla1 {
			width: 100em;
			overflow-x: auto;
			white-space: nowrap;
		}

	}
</style>
<div class="row">
	<div class="col-sm-12">
		<span style="font-size: 30px;font-weight: bold;text-align: center;">Tablero de espera</span>
	</div>
</div>
<div class="row form-group">
        <div class='col-sm-3'>
        	<label for="">Selecciona la fecha</label>
            <div class="form-group1">
                <div class='input-group date' id='datetimepicker1'>
                    <input id="fecha" name="fecha" type='text' class="form-control" value="{{date_eng2esp_1($fecha)}}" />
                    <span class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                    </span>
                </div>
            </div>
             <span class="error_fecha"></span>
        </div>
        <div class="col-sm-2">
        	<br>
        	<button id="buscar" style="margin-top: 8px;" class="btn btn-success">Buscar</button>
        </div>

</div>
<br>
<div class="row">
	<div class="col-sm-12">
		<div id="div_tabla">
			<table class="table table-responsive table-bordered table-hover table-striped table-responsive-sm table-responsive-md table-responsive-lg">
				<thead>
					<tr>

						<th width="5%">Asesor</th>
						<th width="10%">Placas</th>
						<th width="10%"># Orden</th>
						<th  width="10%">Vehículo</th>
						<!--<th>EXPRESS</th>-->
						<!-- <th>Refacciones</th> -->
						<th width="5%">Fecha <br> Recepción</th>
						<th width="20%">Avance</th>
						<th width="10%">Fecha promesa</th>
						<th width="10%">Estatus</th>
						<th width="10%">Lavado</th>
					</tr>
				</thead>
				<tbody>
					@if(count($citas)>0)
						@foreach($citas as $c => $cita)
						<tr>
							<td width="5%">{{$cita->asesor}}</td>
							<td width="10%">{{$cita->vehiculo_placas}}</td>
							<td width="10%">{{$cita->id_cita}}</td>
							<td width="10%">{{$cita->vehiculo_modelo}}</td>
							<!--<td></td>-->
							<!--<td class="text-center">
								@if($cita->id_status==7)
									<i class="fa fa-check-square"></i>
								@else
									<i class="pe-7s-check"></i>
								@endif

							</td>-->
							<td width="5%">{{date_eng2esp_1($cita->fecha)}}</td>
							<td width="20%">
								<?php $porcentaje = 0; ?>
								@if($cita->id_status_asesor==0 && $cita->origen==1)
									<?php $porcentaje = 0; ?>
								@else
									<?php $porcentaje = $cita->porcentaje; ?>
								@endif
								<div class="progress">
								  <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: {{$porcentaje}}%;height: 40px;" aria-valuenow="{{$porcentaje}}" aria-valuemin="0" aria-valuemax="100">{{$porcentaje}}%</div>
								</div>
							</td>
							<td width="10%">{{date_eng2esp_1($cita->fecha_entrega).' '.$cita->hora_entrega}}</td>
							<td width="10%">{{$cita->estatus_tecnico}}</td>
							<td width="10%" class="text-center">
								@if($cita->id_status==3 || $cita->id_status==8 || $cita->id_status==10 || $cita->id_status==11 || $cita->id_status==9 )
									@if($cita->status_lavado==0)
									<div class="alert alert-danger" role="alert" style="background: red; border-color: red; color:white !important">
									  <strong style="font-size: 18px !important;">Planeado</strong>
									</div>
									
									@elseif($cita->status_lavado==1	)
									<div class="alert alert-danger" role="alert" style="background: orange; border-color: orange; color:white !important">
									  <strong style="font-size: 18px !important;">Lavando</strong>
									</div>
									@else
										<div class="alert alert-danger" role="alert" style="background: green; border-color: green; color:white !important">
										  <strong style="font-size: 18px !important;">Terminado</strong>
										</div>
									@endif
								@endif

							</td>
						</tr>
						@endforeach
					@else
					<tr class="text-center">
						<td colspan="10">No hay registros para mostrar...</td>
					</tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>

<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/popper/popper.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<script src="vendor/datatables/jquery.dataTables.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.js"></script>

<script src="js/sb-admin.min.js"></script>

<script src="js/sb-admin-datatables.min.js"></script>


<script src="js/custom/bootbox.min.js"></script>
<script src="js/custom/general.js"></script>
<script src="js/custom/isloading.js"></script>
<script src="js/custom/moment.js"></script>
<script src="js/custom/bootstrap-datetimepicker.js"></script>

<script src="js/custom/bootstrap-switch.js"></script>
<script src="js/custom/clockpicker.js"></script>

<script type="text/javascript">
	var site_url = "{{site_url()}}";

    var aux1 = $(window).height();   //solo la pantalla
    var aux2 = $(document).height(); 
    var inicio = 100;
    var heightPage = $(document).height();
	$('#datetimepicker1').datetimepicker({

		format: 'DD/MM/YYYY',
		icons: {
			time: "far fa-clock",
			date: "fa fa-calendar",
			up: "fa fa-arrow-up",
			down: "fa fa-arrow-down"
		},
		locale: 'es'
	});
	setInterval(function(){
		window.scrollTo(0, inicio);
		if(inicio > aux2){
			inicio = 0;
		}else{
			inicio = inicio +800;
		}

	},10000);

	$(window).scroll(function(){
	    if(($(window).scrollTop() + $(window).height()) == heightPage) {
	            buscar();
	    }      
	});

	$("#buscar").on('click',buscar);

	function buscar(){
	var url =site_url+"/citas/buscar_tablero_espera";
        ajaxLoad(url,{"fecha":$("#fecha").val()},"div_tabla","POST",function(){
      });
	} 
	
	setInterval(buscar,100000);

</script>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
</body>

</html>


