<form action="" id="frm_situacion_intelisis" method="POST">
<input type="hidden" id="id_situacion_actual" name="id_situacion_actual" value="{{$id_situacion_actual}}">
<input type="hidden" id="id_orden_save" name="id_orden_save" value="{{$id_orden_save}}">

<div class="row">
	<div class="col-sm-12">
		<label for="">Situación</label>
		{{$drop_id_situacion_intelisis}}
		<span class="error error_id_situacion_intelisis"></span>
	</div>
</div>

<br>
<div id="div_credentials" class="row d-none">
	<div class="col-sm-6">
		<label>Usuario:</label>
		<input type="text" name="user" id="user" class="form-control">
		<div class="error error_user"></div>
	</div>
	<div class="col-sm-6">
		<label>Contraseña:</label>
		<input type="password" name="password" id="password" class="form-control">
		<div class="error error_password"></div>
	</div>
</div>
</form>

<script>
	var id_situacion_actual_compare = "{{$id_situacion_actual}}";
	$("#id_situacion_intelisis").on('change',function(){
		if($(this).val()==5 || $(this).val()==6 || $(this).val()==7){
			$("#div_credentials").removeClass('d-none');
		}else{
			$("#div_credentials").addClass('d-none');
		}
	});

	if($("#id_situacion_actual").val()!=5 && $("#id_situacion_actual").val()!=6 && $("#id_situacion_actual").val()!=7){
		$("#id_situacion_intelisis").find('option[value="8"]').remove();
	}
	if(id_situacion_actual_compare==5 || id_situacion_actual_compare==6){
		$("#id_situacion_intelisis option:not(:selected)").attr("disabled", true);
		$("#id_situacion_intelisis option[value='7']").attr("disabled", false);
	}	
</script>