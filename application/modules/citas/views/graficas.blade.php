@layout('tema_luna/layout')
@section('contenido')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

  <h1>Búsquedas inteligentes</h1>
  <div class="row">
   <div class="col-sm-3 form-group">
    <label>Tipo de Búsqueda</label>
    <select name="tipo_busqueda" id="tipo_busqueda" class="form-control tipo_busquedas">
        <option value="">-- Selecciona -- </option>
        <option value="fecha">Fecha</option>
        <option value="nombre_cliente">Nombre del cliente</option>
        <option value="propuesta_folio_contrato">Nombre del operador</option>
    </select>
   </div>
</div>
<div id="container"></div>

@endsection

@section('scripts')

<script>
var site_url = "{{site_url()}}";
var chart = Highcharts.chart('container', {

    chart: {
        type: 'column'
    },

    title: {
        text: 'Gráfica de citas'
    },
    legend: {
        align: 'right',
        verticalAlign: 'middle',
        layout: 'vertical'
    },

    xAxis: {
        categories: ['1', '2', '3'],
        labels: {
            x: -10
        }
    },

    yAxis: {
        allowDecimals: false,
        title: {
            text: '# de citas'
        }
    },

    series: [{
        name: 'Periodo1',
        data: [1, 4, 3]
    }, {
        name: 'Periodo2',
        data: [6, 4, 2]
    }, {
        name: 'Periodo3',
        data: [8, 4, 3]
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    layout: 'horizontal'
                },
                yAxis: {
                    labels: {
                        align: 'left',
                        x: 0,
                        y: -5
                    },
                    title: {
                        text: null
                    }
                },
                subtitle: {
                    text: null
                },
                credits: {
                    enabled: false
                }
            }
        }]
    }
});

</script>
@endsection