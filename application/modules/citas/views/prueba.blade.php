@layout('tema_luna/layout')
@section('contenido')
        <!-- Page Title -->
        <div class="page-title">
            <div class="title_left">
                <h3>Lista de Unidades</h3>
            </div>
        </div>
        <!-- /Page Title -->
        
        <div class="clearfix"></div>

        <!-- Table -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                    <div class="x_content">
                        <br />
                         <form role="form" action="<?php echo base_url();?>index.php/citas/saveVideo" method="post" enctype="multipart/form-data">
                         <div class="form-group" id="data_1">
                                <label class="font-noraml">Documento/Foto</label>
                                <div class="input-group ">
                                    <input type="file" class="form-control" value="" name="video">
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-sm-12">
                                  <div>
                                    <button class="btn btn-sm btn-primary text-center m-t-n-xs" type="submit"><strong>Enviar </strong></button>
                                  </div>
                            
                            </div>
                          </div>
                         </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Table -->
@endsection
@section('scripts')
<script>
    ConstruirTabla("tbl_unidades","No hay registros para mostrar...");
</script>
@endsection