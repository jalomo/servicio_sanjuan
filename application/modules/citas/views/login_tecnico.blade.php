<style>
	#div_prepiking{
		overflow-y: scroll;
		max-height: 300px;
	}
</style>
<input type="hidden" id="id_operacion" name="id_operacion" value="{{$id_operacion}}">
@if($origen=='tablero')
<div class="row login">
	<div class="col-sm-12">
		<label for="">Usuario:</label>
		{{$input_usuario}}
		<span class="error error_usuario"></span>
	</div>
</div>
<div class="row login">
	<div class="col-sm-12">
		<label for="">Contraseña:</label>
		{{$input_password}}
		<span class="error error_password"></span>
	</div>
</div>
@endif
@if($idcarryover!=0)
<div class="row text-right">
	<div class="col-sm-12">
		@if($es_carryover)
		<a data-idcarryover="{{$idcarryover}}" data-idcita="{{$id_cita}}" data-id_operacion="{{$id_operacion}}" href="#" class="js_eliminar_co">eliminar Carry Over</a>
		@endif
	</div>
</div>
@endif
@if(PermisoAccionTablero('carryover_reservacion',true) && $carryover_reservacion==1 && $id_operacion =='')
<button id="asignar-op-planeacion" style="margin-top: 10px" data-idcarryover="{{$idcarryover}}" data-idcita="{{$id_cita}}" class="btn btn-warning pull-right">Asignar operación</button>
@endif
<div class="row">
	<div class="col-sm-12">
		<strong>Comentario:</strong><br><span style="text-align: justify;">{{$comentario_cita}}</span>
	</div>
</div>
<a id="ver_cotizaciones" data-idcita="{{$id_cita}}" href="#" class="">Trabajos adicionales</a>
<div class="row">
	<div class="col-sm-4">
		<strong>Placas:</strong><br><span style="text-align: justify;">{{$vehiculo_placas}}</span>
	</div>
	<div class="col-sm-6">
		<strong>Modelo:</strong><br><span style="text-align: justify;">{{$vehiculo_modelo}}</span>
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<strong>Color:</strong><br><span style="text-align: justify;">{{$color}}</span>
	</div>
	<div class="col-sm-6">
		<strong>Asesor:</strong><br><span style="text-align: justify;">{{$asesor}}</span>
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<strong>Año:</strong><br><span style="text-align: justify;">{{$vehiculo_anio}}</span>
	</div>
	<div class="col-sm-6">
		<strong>Serie:</strong><br><span style="text-align: justify;">{{$vehiculo_numero_serie}}</span>
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<strong>Prepiking:</strong><br><span style="text-align: justify;">{{$prepiking}}</span>
	</div>
	<div class="col-sm-6">
		<strong>Fecha promesa:</strong><br><span style="text-align: justify;">{{isset($ordenservicio->fecha_entrega)?date_eng2esp_1($ordenservicio->fecha_entrega).' ':'-'}}{{isset($ordenservicio->hora_entrega)?$ordenservicio->hora_entrega:'-'}}</span>
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<strong>Inicio trabajo:</strong><br><span style="text-align: justify;">{{$hora_inicio_trabajo}}</span>
	</div>
	<div class="col-sm-3">
		@if($hora_fin_trabajo_operacion=='' && $hora_fin_trabajo!='')
		<strong>Fin trabajo:</strong><br><span style="text-align: justify;">{{$hora_fin_trabajo}}</span>
		@else
		<strong>Fin trabajo:</strong><br><span style="text-align: justify;">{{$hora_fin_trabajo_operacion}}</span>
		@endif
		
	</div>
	<div class="col-sm-3">
		<strong>KPI General:</strong><br><span style="text-align: justify;">{{$hora_fin_trabajo}}</span>
	</div>
</div>
<br>
@if($origen=='refacciones')
<!-- <div class="row pull-right">
	<div class="col-sm-12 ">
		<a class="pull-right agregar_extra_prepiking" href="#" data-idcita="{{$id_cita}}" data-idprepiking="0" data-edit="1">
		<i class="pe pe-7s-plus">Agregar suplemento</i>
		</a>
	</div>	
</div> -->
@endif
<div class="row">
	<div class="col-sm-6">
		<strong>Estatus</strong><br> <strong style="font-size: 25px">{{$status_actual}}</strong>
	</div>
	<div class="col-sm-6">
		<strong>Operación</strong><br> <strong style="">{{$operacion}}</strong>
	</div>
</div>
@if($origen=='refacciones')
<div class="row" id="div_prepiking">
	<div class="col-sm-12">
		<div id="div-prep_search">
			<table id="tlb-prep" class="table table-hover">
				<thead>
					<tr>
						<th>Código</th>
						<th>Descripción</th>
					</tr>
				</thead>
				<tbody>
				@if(count($articulos_prepiking)>0)
					@foreach($articulos_prepiking as $a => $articulo)
					<tr>
						<td>{{$articulo->codigo}}</td>
						<td>{{$articulo->descripcion}}</td>
						<td>
							<!-- <a href="" data-idcita="{{$id_cita}}" class="pe pe-7s-note agregar_extra_prepiking" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar" data-idprepiking="{{$articulo->id}}" data-edit="{{($articulo->registro_individual==1)?'1':0}}"></a> -->
						</td>
						
					</tr>
					@endforeach
				@else
					<tr class="text-center">
						<td colspan="2">No hay registros para mostrar...</td>
					</tr>
				@endif
			</tbody>
			</table>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-4">
		<strong>Fecha promesa:</strong><br><span style="text-align: justify;">{{isset($ordenservicio->fecha_entrega)?date_eng2esp_1($ordenservicio->fecha_entrega).' ':'-'}}{{isset($ordenservicio->hora_entrega)?$ordenservicio->hora_entrega:'-'}}</span>
	</div>
	<div class="col-sm-4">
		<strong for="">¿Realizó prepiking?</strong><br>
		Si <input type="checkbox" name="realizo_prepiking" id="realizo_prepiking" {{($realizo_prepiking==1)?'checked':''}} >
	</div>
	<div class="col-sm-4">
		<strong>No. Orden:</strong><br><span style="text-align: justify;">{{isset($ordenservicio->consecutivo_dms)?$ordenservicio->consecutivo_dms:'N/A'}}</span>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<strong for="">Ubicación</strong><br>
		{{$ubicacion_unidad}}
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<textarea name="comentario_refacciones" id="comentario_refacciones" cols="30" rows="5" class="form-control"></textarea>
		<span class="error error_comentario"></span>
	</div>
</div>
<br>
@if($hizo_checkin)
	@if($trabajando)
	<div class="row">
		<div class="col-sm-6">
			<label for="">Usuario</label>
			<input type="text" name="usuario_tecnico" id="usuario_tecnico" class="form-control" value="{{$usuario_tecnico}}">
			<span class="error error_usuario_tecnico"></span>
		</div>
		<div class="col-sm-6">
			<label for="">Password</label>
			<input type="password" name="password_tecnico" id="password_tecnico" class="form-control">
			<span class="error error_password_tecnico"></span>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<label for="">Estatus</label>
			{{$drop_status}}
			<span class="error error_id_status"></span>
		</div>
	</div>
	@else
		<div class="alert alert-info" role="alert">
 		Para hacer el cambio de estatus es necesario que la unidad pase por el estatus de trabajando
		</div>
	@endif
@else
	<div class="alert alert-info" role="alert">
 		Es necesario que el asesor haga el check-in.
	</div>
@endif
<div class="row text-right">
	<div class="col-sm-12">
		<a href="" data-idcita="{{$id_cita}}" class="historial_comentarios_refacciones">Ver historial de comentarios</a>
	</div>
</div>
@endif

<script>
	var id_cita_s = '';
	var idprepiking_s = '';
	var origen = "{{$origen}}";
	var cita_comparar = "{{$id_cita}}";
	var CONST_NO_CITA_NEW_UPDT = "{{CONST_NO_CITA_NEW_UPDT}}";
	if(cita_comparar < CONST_NO_CITA_NEW_UPDT){
		$(".login").hide();
	}
	$(".agregar_extra_prepiking").on('click',function(e){
		e.preventDefault();
		id_cita_s = $(this).data('idcita');
		idprepiking_s = $(this).data('idprepiking');
		var editar_descripcion = $(this).data('edit');
		var url =site_url+"/citas/agregar_extra_prepiking/0";
       customModal(url,{"id_cita":id_cita_s,"idprepiking_s":idprepiking_s,"editar_descripcion":editar_descripcion,
	   "id_operacion":$("#id_operacion").val()},"GET","md",guardarPrepiking,"","Guardar","Cancelar","Guardar","modalPrep");
	});
	$("#asignar-op-planeacion").on('click',function(){
		var idcarryover = $(this).data('idcarryover');
		var id_cita = $(this).data('idcita');
		var url =site_url+"/layout/asignarOperacionPlaneacion/0";
       customModal(url,{"idcarryover":idcarryover,"id_cita":id_cita},"GET","md",guardarPlaneacion,"","Asignar","Cerrar","Guardar","modalPlaneacion");
	})
	function guardarPrepiking(){
     var url = site_url+'/citas/agregar_extra_prepiking/';
    	var codigo = $("#codigo").val();
    	var descripcion = $("#descripcion").val();
        ajaxJson(url,$("#frm-prep").serialize(),"POST","",function(result){
          
          if(isNaN(result)){
                data = JSON.parse( result );
                //Se recorre el json y se coloca el error en la div correspondiente
                $.each(data, function(i, item) {
                    $(".error_"+i).empty();
                    $(".error_"+i).append(item);
                    $(".error_"+i).css("color","red");
                });
          }else if(result==1){
             ExitoCustom("Registro guardado correctamente",function(){
             	buscarPrepiking();
             	$(".close").trigger('click');
             });
          }else{
            ErrorCustom("Error al guardar el registro");
          }
          
        });
  	} 
  	function guardarPlaneacion(){
     var url = site_url+'/layout/asignarOperacionPlaneacion/';
    	var codigo = $("#codigo").val();
    	var descripcion = $("#descripcion").val();
        ajaxJson(url,$("#frm-asignar-op").serialize(),"POST","",function(result){
          
          if(isNaN(result)){
                data = JSON.parse( result );
                //Se recorre el json y se coloca el error en la div correspondiente
                $.each(data, function(i, item) {
                    $(".error_"+i).empty();
                    $(".error_"+i).append(item);
                    $(".error_"+i).css("color","red");
                });
          }else if(result==1){
             ExitoCustom("Operación asignada correctamente",function(){
             	buscar();
             	$(".modalPlaneacion").modal('hide');
             });
          }else{
            ErrorCustom("Error al asignar la operación");
          }
          
        });
  	} 
  function buscarPrepiking(){
    var url =site_url+"/citas/buscarItemsPrepiking";
          ajaxLoad(url,{"id_cita":id_cita_s,"id_operacion":$("#id_operacion").val()},"div-prep_search","POST",function(){
    });
  } 
  if(origen=='refacciones'){
  	$("#usuario_tecnico").val('');
  }
</script>

