<div class="row">
	<div class="col-sm-12">
		<table class="table table-bordered table-striped" cellpadding="0" width="100%">
			<thead>
				<tr>
					<th>#Operación</th>
					<th>#Orden</th>
					<th>Técnico</th>
					<th>Fecha</th>
					<th>Hora inicio</th>
					<th>Hoa fin</th>
					<th>Estatus</th>
				</tr>
			</thead>
			<tbody>
				
				@foreach($registros_tc as $r =>$registro)
				<tr>
					<td>{{$registro->operacion}}</td>
					<td>{{$registro->id_cita}}</td>
					<td>{{$registro->tecnico}}</td>
					<td>{{$registro->fecha}}</td>
					<td>{{$registro->hora_inicio_dia}}</td>
					<td>{{$registro->hora_fin}}</td>
					<td>{{$registro->estatus}}</td>
				</tr>
				@endforeach
				@foreach($registros_tch as $r =>$registro)
				<tr>
					<td>{{$registro->operacion}}</td>
					<td>{{$registro->id_cita}}</td>
					<td>{{$registro->tecnico}}</td>
					<td>{{$registro->fecha}}</td>
					<td>{{$registro->hora_inicio_dia}}</td>
					<td>{{$registro->hora_fin}}</td>
					<td>{{$registro->estatus}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>