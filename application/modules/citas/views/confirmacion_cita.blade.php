@layout('layout_correo')
@section('contenido')
<tr style="border-collapse:collapse;">
  <td style="Margin:0;padding-top:10px;padding-bottom:20px;padding-left:30px;padding-right:30px;background-position:left top;background-color:#FAFAFA;" bgcolor="#fafafa" align="left">
    <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
      <tr style="border-collapse:collapse;">
        <td width="540" valign="top" align="center" style="padding:0;Margin:0;">
          <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
            <tr style="border-collapse:collapse;">
              <td align="left" style="padding:0;Margin:0;padding-bottom:10px;">
                <h2 style="Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:24px;font-style:normal;font-weight:bold;color:#212121;">{{$asunto}}</h2>
              </td>
            </tr>
            <tr style="border-collapse:collapse;">
              <td align="left" style="padding:0;Margin:0;padding-bottom:5px;">
                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
                  Estimado, <strong>{{$datos_cita['datos_nombres'].' '.$datos_cita['datos_apellido_paterno'].' '.$datos_cita['datos_apellido_materno']}}</strong>
                </p>
                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
                  Los datos de la cita con el folio #{{$id_cita}} son los 	siguientes: 
                </p>
                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
                  <strong>Fecha y hora: </strong> {{$datos_cita['fecha_hora']}} <br>
                  <strong>Asesor: </strong> {{$datos_cita['asesor']}} <br>
                </p>
                <p>
                  <strong>Si desea contactarnos vía WhatsApp puede hacerlo en este</strong> <a target="_blank" href="https://api.whatsapp.com/send?phone=+52{{$datos_cita['telefono_asesor_ws']}}&text=%20FORD%20MYLSA%20QUERÉTARO,%20Necesito%20contactar%20a%20mi%20asesor%20{{$datos_cita['asesor']}}%20sobre%20mi%20Orden%20No.%20{{$id_cita}}">enlace</a>
                </p>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
          <p>
            <strong>Adquiere tu Extensión de Garantía para tu Ford. Presiona <a href = "mailto:info@sohex.mx?subject=Adquiere tu Extensión de Garantía para tu Ford&body=Requiero una extensión de garantía, #Cita: {{$id_cita}}, Cliente: {{$datos_cita['datos_nombres']}} {{$datos_cita['datos_apellido_paterno']}} {{$datos_cita['datos_apellido_materno']}}    ">aquí!</a></strong>
          </p>
      </tr>
      @foreach($publicidad as $p => $pub)
      <tr style="margin-bottom: 10px;">
        <td>
        <img width="80%" src="{{$pub->url}}" alt=""> <br><br>
        </td>
      </tr>
      @endforeach
    </table>
  </td>
</tr>
@endsection
