<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<title>Document</title>
	<style>
		.text-center{
			text-align: center;
		}
		
		*{
			font-size: 10px !important;
		}
		label{
			font-weight: bold;
		}
		.text-header{
			font-weight: bold; 
			text-align: center;
			font-size: 10px;
		}
	</style>
</head>

<body>
	<h2 style="margin-bottom: 0px; text-align: center;font-size: 30px !important;">PLASENCIA MAZATLAN SA DE CV</h2>
	<h5 class="text-header">CARRETERA INTERNACIONAL AL NORTE NO. 3117, INFONAVIT PLAYAS, PLAZA SAN IGNACIO, MAZATLÁN, SINALOA. C.P. 82128</h5>
	<h5 class="text-header">TELÉFONO: 669 983 1400. R.F.C. PMA-980730-UHA Email: legna.cruz@fordplasencia.com.mx Web: www.ford.mx</h5>
	<h5 class="text-header">Horario de Recepción y Entrega de Unidades:</h5>
	<h5 class="text-header">Lunes a Viernes de 7:40 AM a 1:00 PM Sábado 7:40 AM a 9:00 PM</h5>
	<h5 class="text-header">Caja Lunes a Viernes 8:00 AM a 9:00 PM Sábado 9:00 AM a 3:00 PM</h5>
	<br>
	<div style="width: 100%">
		<div style="width: 50%;display: inline;float: left;">
			<strong style="font-weight: bold;">RFC:</strong> {{$orden->rfc}}
		</div>
		<div style="width: 50%;display: inline;float: right;">
			<strong style="font-weight: bold;">Remisión:</strong> {{$orden->id_cita}}
			
		</div>
	</div>
	<br>
	<div style="width: 100%">
		<div style="width: 50%%;display:inline;float: left;">
			<label for="">Código: </label><br>
			<label for="">Teléfono: </label>{{$orden->datos_telefono}}<br>
			<label for="">Nombre: </label>{{$orden->datos_nombres.' '.$orden->datos_apellido_paterno.' '.$orden->datos_apellido_materno}}<br>
			<label for="">Dirección: </label>{{$orden->estado.', '.$orden->municipio.', '.$orden->colonia.', '.$orden->calle.', '.$orden->estado.', '.$orden->noexterior}}<br>
			<label for="">Ciudad: </label>
		</div>
		<div style="width: 50%;display:inline;float: left;">
			<label for="">Tipo de orden: </label>{{$tipo_orden}}<br>
			<label for="" style="text-align: right;">Fecha: </label>{{date('Y/m/d')}}<br>
			<label for="">Orden No. </label>{{$orden->id_cita}}<br>
			<label for="">Fecha de Entrada: </label>{{date_eng2esp_1($orden->fecha_recepcion)}}<br>
			<label for="">Fecha de Salida: </label>{{date_eng2esp_1($orden->fecha_entrega)}}<br>
			<label for="">Hora de Salida: </label>{{date("H:i")}}
		</div>
	</div>
	<br>
	<table width="100%" class="table">
		<thead>
			<tr>
				<th>Marca</th>
				<th>Modelo</th>
				<th>Tipo</th>
			</tr>
		</thead>
		<tbody>
			<tr style="text-align: center !important">
				<td style="text-align: center !important">FORD</td>
				<td>{{$orden->vehiculo_anio}}</td>
				<td>{{$orden->vehiculo_modelo}}</td>
			</tr>
		</tbody>
	</table>
	<br>
	<table width="100%" class="table">
		<thead>
			<tr>
				<th>Serie</th>
				<th>Placas</th>
				<th>Kilometros</th>
				<th>Torre No.</th>
			</tr>
		</thead>
		<tbody>
			<tr style="text-align: center !important">
				<td>{{$orden->vehiculo_numero_serie}}</td>
				<td>{{$orden->vehiculo_placas}}</td>
				<td>{{number_format($orden->vehiculo_kilometraje,0)}}</td>
				<td>{{$orden->numero_interno}}</td>
			</tr>
		</tbody>
	</table>
	<br>
	<table width="50%" class="table">
		<thead>
			<tr>
				<th>Color</th>
				<th>Transmisión</th>
				<th>No.Asesor</th>
			</tr>
		</thead>
		<tbody style="text-align: right;">
			<tr>
				<td >{{$color}}</td>
				<td>{{$orden->transmision}}</td>
				<td>{{$orden->asesor}}</td>
			</tr>
		</tbody>
	</table>
	<br>
	<table width="50%" class="table">
		<thead>
			<tr>
				<th>Motor</th>
				<th>Tipo de pago</th>
			</tr>
		</thead>
		<tbody style="text-align: right;">
			<tr>
				<td>{{$orden->motor}}</td>
				<td>{{$orden->tipo_pago}}</td>
			</tr>
		</tbody>
	</table>
	<br>
	<h2 class="text-center">Cotización orden de servicio</h2>
	<table width="50%" class="table" border="1">
		<thead>
			<tr>
				<th>GP</th>
				<th>OP</th>
				<th>Descripción</th>
				<th>Cantidad</th>
				<th>Importe</th>
				<th>Descuento</th>
				<th>Total</th>
			</tr>
		</thead>
		<tbody style="text-align: right;">
			<?php $subtotal= 0;?>
        	<?php $iva = 0; ?>
				@foreach($items as $i => $value)
				<tr>
					<td colspan="3">{{$value->descripcion}}</td>
					<td class="text-center">{{number_format($value->cantidad,2)}}</td>
					<td class="text-center">{{number_format($value->precio_unitario,2)}}</td>
					<td class="text-center">{{number_format($value->descuento,2)}}%</td>
					<td class="text-center">{{number_format($value->total,2)}}</td>
					<?php $subtotal = $subtotal+$value->total ?>
				</tr>
				@endforeach
				@foreach($items_grupos as $it => $item)
				<tr>
					<td class="text-center">{{$item->grupo}}</td>
					<td class="text-center">{{$item->operacion}}</td>
					<td>{{$item->descripcion}}</td>
					<td class="text-center">{{number_format($item->cantidad,2)}}</td>
					<? $precio_sin_iva = $item->total / 1.16 ?>
					<td class="text-center">{{number_format($item->precio_unitario,2)}}</td>
					<td class="text-center">{{number_format($item->descuento,2)}}%</td>
					<td class="text-center">{{number_format($precio_sin_iva,2)}}</td>
					<?php $subtotal = $subtotal+$precio_sin_iva ?>
				</tr>
				@endforeach
				@foreach($items_gen as $itg => $item_gen)
				<tr>
					<td class="text-center">GEN</td>
					<td class="text-center">GEN</td>
					<td>{{$item_gen->descripcion}}</td>
					<td class="text-center">{{number_format($item_gen->cantidad,2)}}</td>
					<td class="text-center">{{number_format($item_gen->precio_unitario,2)}}</td>
					<td class="text-center">{{number_format($item_gen->descuento,2)}}%</td>
					<td class="text-center">{{number_format($item_gen->total,2)}}</td>
					<?php $subtotal = $subtotal+$item_gen->total ?>
				</tr>
				@endforeach
				@foreach($items_corr as $itc => $item_c)
				<tr>
					<td class="text-center"></td>
					<td class="text-center"></td>
					<td>{{$item_c->descripcion}}</td>
					<td class="text-center">{{number_format($item_c->cantidad,2)}}</td>
					<td class="text-center">{{number_format($item_c->precio_unitario,2)}}</td>
					<td class="text-center">{{number_format($item_c->descuento,2)}}%</td>
					<td class="text-center">{{number_format($item_c->total,2)}}</td>
					<?php $subtotal = $subtotal+$item_c->total ?>
				</tr>
				@endforeach
				@foreach($items_prev as $itc => $item_p)
				<tr>
					<td class="text-center"></td>
					<td class="text-center"></td>
					<td>{{$item_p->descripcion}}</td>
					<td class="text-center">{{number_format($item_p->cantidad,2)}}</td>
					<td class="text-center">{{number_format($item_p->precio_unitario,2)}}</td>
					<td class="text-center">{{number_format($item_p->descuento,2)}}%</td>
					<td class="text-center">{{number_format($item_p->total,2)}}</td>
					<?php $subtotal = $subtotal+$item_p->total ?>
				</tr>
				@endforeach
		</tbody>
		 <tfoot>
	        <tr>
	          <td colspan="5"></td>
	          <td>
	            <span>I.V.A. :</span>
	          </td>
	          <td width="15%">
	            <span style="text-align: center;">${{number_format((float)($subtotal*.16),2)}}</span>
	          </td>
	        </tr>
	        <tr>
	          <td colspan="5"></td>
	          <td>
	            <span>Subtotal:</span>
	          </td>
	          <td width="15%">
	            <span style="text-align: center;">${{number_format((float)$subtotal,2)}}</span>
	          </td>
	        </tr>
	        <tr>
	          <td colspan="5"></td>
	          <td>
	            <span>Anticipo:</span><br>
	          </td>
	          <td width="15%">
	            <span style="text-align: center;">${{number_format($orden->anticipo,2)}}</span>
	          </td>
	        </tr>
	        <tr>
	          <td colspan="5"></td>
	          <td>
	            <span>Total :</span>
	          </td>
	          <?php $total = (float)(($subtotal*.16)+$subtotal)-$orden->anticipo ?>
	          <td width="15%">
	            <span style="text-align: center;">${{number_format($total,2)}}</span>
	          </td>
	        </tr>
	      </tfoot>
	</table>
	<br>
	<h2 class="text-center">Cotización multipunto</h2>
	<br>
	 <table style="width:100%;border:1px solid #000;border-radius: 4px;margin-top:-10px;">
        <tr>
            <td style="width:10%;border-bottom:1px solid #000;" align="center"><strong>CANTIDAD</strong></td>
            <td style="width:33%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"><strong>  D E S C R I P C I Ó N</strong></td>
            <td style="width:10%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"><strong>NUM. PIEZA</strong></td>
            <td style="width:10%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"><strong>COSTO</strong></td>
            <td style="width:8%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"><strong>HORAS</strong></td>
            <td style="width:15%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"><strong>TOTAL</strong></td>
            <td style="width:10%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"><strong>AUTORIZÓ</strong></td>
        </tr>
        <?php if (isset($registrosControl)): ?>
            <?php if (count($registrosControl)>0): ?>
                <?php foreach ($registrosControl as $index => $value): ?>
                    <tr>
                        <td style="width:10%;border-bottom:1px solid #000;height:20px;" align="center">
                            <label for=""><?php echo $registrosControl[$index][0]; ?></label>
                        </td>
                        <td style="width:33%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center">
                            <label for=""><?php echo $registrosControl[$index][1]; ?></label>
                        </td>
                        <td style="width:10%;border-bottom:1px solid #000;border-left:1px solid #000;" align='center'>
                            <label for=""><?php if(isset($registrosControl[$index][6])) echo $registrosControl[$index][6]; else echo ""; ?></label>
                        </td>
                        <td style="width:10%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center">
                            <label for=""><?php echo $registrosControl[$index][2]; ?></label>
                        </td>
                        <td style="width:8%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center">
                            <label for=""><?php echo $registrosControl[$index][3]; ?></label>
                        </td>
                        <td style="width:15%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center">
                            <label for=""><?php echo $registrosControl[$index][4]; ?></label>
                        </td>
                        <td style="width:10%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center">
                              <label for=""><?php echo $registrosControl[$index][5]; ?></label>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php for ($i = 0; $i<(18-count($registrosControl)) ; $i++): ?>
                    <tr>
                        <td style="width:10%;border-bottom:1px solid #000;height:20px;" align="center"></td>
                        <td style="width:33%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                        <td style="width:8%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                        <td style="width:15%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                    </tr>
                <?php endfor; ?>
            <?php else: ?>
                <?php for ($i = 0; $i<18 ; $i++): ?>
                    <tr>
                        <td style="width:10%;border-bottom:1px solid #000;height:20px;" align="center"></td>
                        <td style="width:33%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                        <td style="width:8%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                        <td style="width:15%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                    </tr>
                <?php endfor; ?>
            <?php endif; ?>
        <?php else: ?>
          <?php for ($i = 0; $i<18 ; $i++): ?>
              <tr>
                  <td style="width:10%;border-bottom:1px solid #000;height:20px;" align="center"></td>
                  <td style="width:33%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                  <td style="width:10%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                  <td style="width:10%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                  <td style="width:8%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                  <td style="width:15%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
                  <td style="width:10%;border-bottom:1px solid #000;border-left:1px solid #000;" align="center"></td>
              </tr>
          <?php endfor; ?>
        <?php endif; ?>
        <tr>
            <td colspan="6" align="right" style="border-bottom:1px solid #000;height:20px;">
                SUB-TOTAL
            </td>
            <td align="center" style="border-bottom:1px solid #000;border-left:1px solid #000;">
                $<label for="" id="subTotalMaterialLabel"><?php if(isset($subTotalMaterial)) echo number_format($subTotalMaterial,2); else echo "0"; ?></label>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="right" style="border-bottom:1px solid #000;height:20px;">
                I.V.A.
            </td>
            <td align="center" style="border-bottom:1px solid #000;border-left:1px solid #000;">
                $<label for="" id="ivaMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format($ivaMaterial,2); else echo "0"; ?></label>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="right" style="border-bottom:1px solid #000;height:20px;">
                TOTAL
            </td>
            <td align="center" style="border-bottom:1px solid #000;border-left:1px solid #000;">
            	<?php $totalMaterial = 0; ?>
                $<label for="" id="totalMaterialLabel"><?php if(isset($totalMaterial)) echo number_format($totalMaterial,2); else echo "0"; ?></label>
            </td>
        </tr>
        <!-- <tr>
            <td colspan="6">
                <br><br>
            </td>
        </tr> -->
    </table>
    <br>
    <h1 class="text-center">TOTAL A PAGAR</h1>
	<table width="50%" class="table" border="1">
		 <tfoot>
	        <tr style="background-color: #eaeaea">
	          <td colspan="5"></td>
	          <td>
	            <span>Total :</span>
	          </td>
	          <?php $total = (float)(($subtotal*.16)+$subtotal)-$orden->anticipo ?>
	          <td width="15%">
	            <span style="text-align: center;">${{number_format($total+$totalMaterial,2)}}</span>
	          </td>
	        </tr>
	      </tfoot>
	</table>
	<br>
	<hr>
	Después de haber revisado y probado el automóvil que deja a reparación, me manifiesto conforme con los trabajos realizados, dándome por recibido en este momento de todas las refacciones y piezas usadas que les fueron cambiadas al vehículo.
</body>
</html>