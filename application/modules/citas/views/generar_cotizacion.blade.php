@layout('tema_luna/layout')
@section('contenido')
<div class="row">
	<div class="col-sm-12">
		<input type="hidden" name="id" id="id" value="{{$idorden}}">
		<button id="descripcion-orden" type="button" class="btn btn-info pull-right">Agregar descripción</button>
		
		<br>
		<hr>
		<h5 class="text-center">DESCRIPCIÓN DEL TRABAJO Y DESGLOCE DEL PRESUPUESTO</h5>
		<hr>
		<div class="row">
			<div class="col-sm-12">
				<div id="tabla-items"></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-sm-12">
				<button id="saveCotizacion" class="btn btn-success">Guardar cotización</button>
			</div>
		</div>
		<br>
		<br>
	</div>
</div>
@endsection
@section('scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<script>
  var site_url = "{{site_url()}}";
  var item = '';
  var idorden = "{{$idorden}}";
	buscarItems();
	$("#descripcion-orden").on('click',function(){
    	var url =site_url+"/citas/asignarOrden/0";
    customModal(url,{"idorden":$("#id").val(),"tipo":2},"GET","lg",GuardarItem,"","Guardar","Cancelar","Desgloce presupuesto ","modal3");
  	});
  	
  	$("body").on('click',".eliminar_item",function(e){
    e.preventDefault();
    item = $(this).data('iditem');
      ConfirmCustom("¿Está seguro de eliminar el registro?", eliminarItem,"", "Confirmar", "Cancelar");
    });

  $("#saveCotizacion").on('click',saveCotizacion);
  function GuardarItem(){
   var url = site_url+'/citas/asignarOrden';
        ajaxJson(url,$("#order-item").serialize(),"POST","",function(result){
                if(isNaN(result)){
                data = JSON.parse( result );
                //Se recorre el json y se coloca el error en la div correspondiente
                $.each(data, function(i, item) {
                    $(".error_"+i).empty();
                    $(".error_"+i).append(item);
                    $(".error_"+i).css("color","red");
                });
        }else{
            if(result==1){
              ExitoCustom("Guardado correctamente",function(){
                $(".modal3").modal('hide');
                buscarItems();
                //location.reload();

              });
            }else{
                  ErrorCustom('No se pudo guardar, intenta otra vez.');
            }
        }
        });
   }
   
   function buscarItems(){
    var url =site_url+"/citas/getTablaItemServicios";
          ajaxLoad(url,{"idorden":$("#id").val(),"tipo":2,"temporal":$("#temporal").val()},"tabla-items","POST",function(){
        });
    } 

  function eliminarItem(){
   var url = site_url+'/citas/eliminarItem';
    
        ajaxJson(url,{"item":item},"POST","",function(result){
            if(result==1){
              ExitoCustom("Eliminado correctamente",function(){
                buscarItems();

              });
            }else{
                  ErrorCustom('No se pudo eliminar, intenta otra vez.');
            }
        });
   }
   function saveCotizacion(){
   	if(validarAnticipo()){
	   	$("#id_orden_save").val($("#id").val());
	      ajaxJson(site_url+'/citas/actualizar_totales_orden',$("#frm-total-orden").serialize(),"POST","",function(result_orden){
	        if(result_orden==1){
	          ExitoCustom("Cotización generada correctamente",function(){
	          window.location.href = site_url+ "/citas/historial_orden_servicio";
	          });
	        }
	        
	      });
    }else{
    	ErrorCustom('El anticipo no puede ser mayor al total');
    }
   }
   function validarAnticipo(){
   	//alert('entre');
	    if($("#anticipo_orden").val()=='' || parseFloat($("#anticipo_orden").val())<0 ){
	      var anticipo = 0;
	      $("#anticipo_orden").val(0)
	    }else{
	      var anticipo = parseFloat($("#anticipo_orden").val());
	    }
	    var total = parseFloat($("#total_temp_orden").val());
	    if(anticipo<0 || anticipo==''){
	      anticipo = 0;
	    }
	    if(anticipo>total){
	     return false;
	    }else{
	      return true;
	    }
   }
</script>
@endsection
