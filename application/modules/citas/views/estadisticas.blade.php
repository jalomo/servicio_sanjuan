@layout('tema_luna/layout')
@section('contenido')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

@endsection

@section('scripts')
<script>
// Create the chart
var datos =JSON.parse('<?php echo json_encode($estadisticas);?>');
var array_datos = [];

$.each(datos,function (i,item) {
    array_datos.push({"name":item.status,"y":parseFloat(item.total),"drilldown":item.status})
});
Highcharts.chart('container', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Estadísticas de citas'
  },
  xAxis: {
    type: 'category'
  },
  yAxis: {
    title: {
      text: 'Cantidad'
    }

  },
  legend: {
    enabled: false
  },
  plotOptions: {
    series: {
      borderWidth: 0,
      dataLabels: {
        enabled: true,
        format: '{point.y:.1f}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.1f}</b><br/>'
  },

  "series": [
    {
      "name": "Cantidad de citas",
      "colorByPoint": true,
      "data": array_datos
    }
  ]
});
</script>
@endsection