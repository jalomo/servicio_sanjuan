@layout('tema_luna/layout')
@section('contenido')
	<h1>Lista de Citas</h1>
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered table-striped table-striped" id="tbl_citas" width="100%" cellspacing="0">
				<thead>
					<tr class="tr_principal">
						<th>ID</th>
						<th>Usuario creó</th>
						<th>Fecha de cita</th>
						<th>Nombre del cliente</th>
						<th>Teléfono del cliente</th>
						<th>Comentarios</th>
						<th>Técnico</th>
						<th>Placas</th>
						<th>Modelo</th>
						<th>Asesor</th>
						<th>Servicio</th>
						<th>Origen</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($citas as $c => $value)
						<tr>
							<?php $fecha_mostrar = $value->fecha_dia.'/'.$value->fecha_mes.'/'.$value->fecha_anio;
							$fecha_comparar = $value->fecha_anio.'-'.$value->fecha_mes.'-'.$value->fecha_dia; 
							 ?>
							<td>{{$value->id_cita}}</td>
							<td>{{$value->adminNombre}}</td>
							
							<td>{{$fecha_mostrar.' '.$value->hora}}
							 	
							</td>
							<td>{{$value->datos_nombres.' '.$value->datos_apellido_paterno.' '.$value->datos_apellido_materno}}</td>
							<td>{{$value->datos_telefono}}</td>
							<td>{{$value->comentarios_servicio}}</td>
							<td>{{$value->nombre}}</td>
							<td>{{$value->vehiculo_placas}}</td>
							<td><span title="{{$value->vehiculo_modelo}}">{{trim_text($value->vehiculo_modelo,1)}}</span></td>
							<td>{{$value->asesor}}</td>
							<td>{{$value->servicio}}</td>
							<td>
								@if($value->origen==0)
									Panel
								@elseif($value->origen==1)
									Aplicación
								@elseif($value->origen==2)
									Tablero
								@else
									Reservación sin cita
								@endif
							</td>
							<td>
								@if(PermisoAccion('editar_cita'))
								
								<a class="" href="{{base_url('citas/agendar_cita/'.$value->id_cita)}}" data-id_cita="{{$value->id_cita}}"> <i class="pe pe-7s-note"></i>
								</a>
								@endif

								@if(PermisoAccion('eliminar_cita'))
								<a href="" data-id="{{$value->id_cita}}" class="js_eliminar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Eliminar">
									<i class="pe pe-7s-trash"></i>
								</a>
								@endif

								@if(PermisoAccion('cancelar_cita'))
								<a href="" data-id="{{$value->id_cita}}" class="js_cancelar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Cancelar cita">
									<i class="pe pe-7s-close-circle"></i>
								</a>
								@endif

								@if($value->confirmada)
									<a href="" data-id="{{$value->id_cita}}" class="js_confirmar" data-valor="0" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Confirmar">
										<i class="pe-7s-switch"></i>
									</a>
								@else
									<a href="" data-id="{{$value->id_cita}}" class="js_confirmar" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="No confirmada">
										<i class="pe-7s-check"></i>
									</a>
								@endif
								<a href="{{site_url('citas/linea_tiempo/'.$value->id_cita)}}" target="_blank" data-id="{{$value->id_cita}}" class="js_linea_tiempo" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Línea de tiempo">
									<i class="pe pe-7s-clock"></i>
								</a>

								@if($value->origen==2 && $value->id_status==3)
									<?php $clase = ($value->id_status_cita==5)?"cargreen":"gris js_cambiar_status"; ?>
                      			
                      				<a style="cursor: pointer;" data-id="{{$value->id_horario}}" class=" {{$clase}} elemento_{{$value->id_horario}}"  aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Entregar unidad" data-status="5">
                      					<i class="fas fa-car"></i>
                      				</a>
								@endif
								
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection
@section('scripts')

<script>
	var site_url = "{{site_url()}}";
	var id_cita = '';
	var accion = '';
	var valor = '';
	var aPos = '';
	var id= '';
	inicializar_tabla("#tbl_citas");
	 $("body").on("click",'.js_eliminar1',function(e){
       e.preventDefault();
       var id = $(this).data('id')
       ConfirmCustom("¿Está seguro de eliminar la cita?", callbackEliminarCita(id),"", "Confirmar", "Cancelar");
      
    });
	 $("body").on('click','.js_cambiar_status',function(e){
      	e.preventDefault();
      	var fecha_actual = "{{date('d/m/Y')}}";
		var fecha =$("#fecha").val();
			id = $(this).data('id')
  		status = $(this).data('status')
  		aPos = $(this);
		ConfirmCustom("¿Está seguro de cambiar el estatus a la cita?", callbackCambiarStatus,"", "Confirmar", "Cancelar");
      });
 	$("body").on("click",'.editar_cita',function(e){
	 	e.preventDefault();
	 	id_cita = $(this).data('id_cita');
	 	accion = "editar";
	       var url =site_url+"/citas/login_editar_cita/0";
	       customModal(url,{},"GET","md",ValidarLogin,"","Ingresar","Cancelar","Editar Cita","modal1");
  	}); 
  	$("body").on("click",'.js_cancelar',function(e){
      e.preventDefault();
      id_cita = $(this).data('id');
      ConfirmCustom("¿Está seguro de cancelar la cita?", callbackCancelarCita,"", "Confirmar", "Cancelar");
    }); 
    $("body").on("click",'.js_eliminar',function(e){
	 	e.preventDefault();
	 	id_cita = $(this).data('id');
	 	mensaje ="¿Está seguro de eliminar la cita?";
	 	ConfirmCustom(mensaje, callbackEliminarCita,"", "Confirmar", "Cancelar");

  	}); 
  	$("body").on("click",'.js_confirmar',function(e){
       e.preventDefault();
       //valor es 0 cuando va rechazar y 1 cuando lo va confirmar
       aPos = $(this);
       valor = $(this).data('valor');
       if(valor==1){
       	mensaje ="¿Está seguro de confirmar la cita?";
       }else{
       	mensaje ="¿Está seguro de poner como no confirmada la cita?";
       }
       id_cita = $(this).data('id');
        ConfirmCustom(mensaje, confirmar_rechazar,"", "Confirmar", "Cancelar");
    });
    function confirmar_rechazar(){
		var url =site_url+"/citas/confirmar_rechazar/";
		ajaxJson(url,{"id":id_cita,"valor":valor},"POST","",function(result){
			if(result ==0){
					ErrorCustom('Error de petición, por favor intenta de nuevo');
				}else{
					ExitoCustom("Registro actualizado correctamente.",function(){
					$(".close").trigger("click");
						if(valor==1){
							//la está confirmando
							$(aPos).data('valor',0);
							$(aPos).attr('title','Confirmar');
							$(aPos).removeClass('fa-square-o');
							$(aPos).addClass('fa-check-square-o');
							$(aPos).attr('data-original-title','Confirmar');
							

						}else{
							//la está rechazando
							$(aPos).data('valor',1);
							$(aPos).attr('title','No confirmada');
							$(aPos).removeClass('fa-check-square-o');
							$(aPos).addClass('fa-square-o');
							$(aPos).attr('data-original-title','No confirmada');

						}
						$('[data-toggle="tooltip"]').tooltip()
					});	
					
				}
		});
	}
	function callbackEliminarCita(){
		var url =site_url+"/citas/eliminar_cita_bd/";
		ajaxJson(url,{"id":id_cita},"POST","",function(result){
			if(result ==0){
					ErrorCustom('La cita no se pudo eliminar, por favor intenta de nuevo');
				}else{
					ExitoCustom("Cita eliminada correctamente",function(){
						window.location.href = site_url+"/citas/ver_citas";
					});	
				}
		});
	}

	function ValidarLogin(){
			var url =site_url+"/citas/login_editar_cita";
			ajaxJson(url,{"usuario":$("#usuario").val(),"password":$("#password").val()},"POST","",function(result){
				if(isNaN(result)){
					data = JSON.parse( result );
					//Se recorre el json y se coloca el error en la div correspondiente
					$.each(data, function(i, item) {
						 $.each(data, function(i, item) {
		                    $(".error_"+i).empty();
		                    $(".error_"+i).append(item);
		                    $(".error_"+i).css("color","red");
		                });
					});
				}else{
					if(result <0){
						ErrorCustom('Usuario o contraseña inválidos.');
					}else if(result==0){
						ErrorCustom('El usuario no es administrador');
					}else{
						if(accion=='editar'){
							window.location.href = site_url+'/citas/agendar_cita/'+id_cita;
						}else{
							//var perfil = "{{$this->session->userdata('tipo_perfil')}}";
							//alert(perfil);
							callbackEliminarCita();
						}
						
						
					}
				}
			});
		}
	function ValidarLogin_cancelar(){
			var url =site_url+"/citas/login_editar_cita_cancelar_eliminar";
			ajaxJson(url,{"usuario":$("#usuario").val(),"password":$("#password").val()},"POST","",function(result){
				if(isNaN(result)){
					data = JSON.parse( result );
					//Se recorre el json y se coloca el error en la div correspondiente
					$.each(data, function(i, item) {
						 $.each(data, function(i, item) {
		                    $(".error_"+i).empty();
		                    $(".error_"+i).append(item);
		                    $(".error_"+i).css("color","red");
		                });
					});
				}else{
					if(result <0){
						ErrorCustom('Usuario o contraseña inválidos.');
					}else if(result==1){
						callbackCancelarCita();
						
					}else{
						ErrorCustom('El usuario no tiene permisos para cancelar una cita');
					}
				}
			});
		}
	function callbackCancelarCita(){
		var url =site_url+"/citas/cancelar_cita/";
		ajaxJson(url,{"id":id_cita},"POST","",function(result){
			if(result ==0){
					ErrorCustom('La cita no se pudo cancelar, por favor intenta de nuevo');
				}else{
					ExitoCustom("Cita cancelada correctamente",function(){
						window.location.href = site_url+"/citas/ver_citas";
					});	
				}
		});
	}
	function callbackCambiarStatus(){
		var url =site_url+"/citas/cambiar_status_cita/";
		ajaxJson(url,{"id":id,"status":status},"POST","",function(result){
			if(result ==0){
					ErrorCustom('No se pudo cambiar el estatus, por favor intenta de nuevo');
				}else{
					ExitoCustom("Estatus cambiado correctamente",function(){
						//buscar();
						$(".elemento_"+id).removeClass('cargreen');
						$(aPos).addClass('cargreen');
					});	
				}
		});
	}  
	//NUEVA VERSION
	// $("body").on("click",'.js_linea_tiempo',function(e){
	//    e.preventDefault();
	//    var url =site_url+"/layout/getOperacionesByCita/";
	//    var id_cita = $(this).data('id');
	//    ajaxJson(url,{"id_cita":id_cita},"POST","",function(result){
	// 		result = JSON.parse( result );
	// 	   if(result.cantidad==0){
	// 		ErrorCustom('La orden no tiene asignadas operaciones');
	// 	   }else{
	// 		if(result.cantidad==1){
	// 			window.location.href = site_url+"/citas/linea_tiempo/"+id_cita+'/'+result.operaciones[0].id;
	// 		}else{
	// 			var url =site_url+"/layout/lista_operaciones/0";
	//        		customModal(site_url+"/layout/lista_operaciones",{"id_cita":id_cita},"POST","md","","","","Cerrar","Línea de tiempo","modalLinea");
	// 		}
	// 	   }
	// 	});
    // });
</script>
@endsection