<h3 class="text-center">{{$status}}</h3>
<div style="overflow-y: scroll;height: 400px;" class="row">
	<div class="col-sm-12">
		<table id="tbl_proactivo" class="table table-hover table-striped">
			<thead>
				<tr class="tr_principal">
					<th>Acciones</th>
					<th>ID</th>
					<th>Cliente</th>
					<th>Vehículo</th>
					<th>Placas</th>
					<th>Fecha ingreso</th>
					<th>Fecha detenido</th>
					<th>Técnico</th>
					<th>Asesor</th>

				</tr>
			</thead>
			<tbody>
				@foreach($registros as $r => $registro)
					<tr>
						<td>
							<a data-cita="{{$registro->id_cita}}" class="js_cambiar_status" data-tecnico="{{$registro->tecnico}}" style="cursor: pointer;">
								<i class="fa fa-retweet"></i>
							</a>
							<a class="ver_comentarios" data-cita="{{$registro->id_cita}}" style="cursor: pointer;">
								<i class="pe-7s-info
pe pe-7s-info"></i>
							</a>
						</td>
						<td>{{$registro->id_cita}}</td>
						<td>{{$registro->datos_nombres.' '.$registro->datos_apellido_paterno.' '.$registro->datos_apellido_materno}}</td>
						<td>{{$registro->vehiculo_modelo}}</td>
						<td>{{$registro->vehiculo_placas}}</td>
						<td>{{$registro->fecha_ingreso.' '.$registro->hora_ingreso}}</td>
						<td>{{$this->mc->stopHourByStatus($registro->id_cita,$status)}}</td>
						<td>{{$registro->tecnico}}</td>
						<td>{{$registro->asesor}}</td>
					</tr>
					@endforeach
			</tbody>
		</table>
	</div>
</div>

<script>
	InitDT("#tbl_proactivo");


	function InitDT(tabla='',scroll=true){
        $(tabla).dataTable({
        "sPaginationType": "full_numbers",
        "scrollX": true,
        "oLanguage": {
            "oPaginate": {
                "sPrevious": "Anterior",
                "sNext": "Siguiente",
                "sLast": "Última",
                "sFirst": "Primera"
            },
            "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
            '<option value="5">5</option>' +
            '<option value="10">10</option>' +
            '<option value="20">20</option>' +
            '<option value="30">30</option>' +
            '<option value="40">40</option>' +
            '<option value="50">50</option>' +
            '<option value="-1">Todos</option>' +
            '</select> registros',
            "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
            "sInfoFiltered": " - filtrados de _MAX_ registros",
            "sInfoEmpty": "No hay resultados de búsqueda",
            "sZeroRecords": "No hay registros para mostrar...",
            "sProcessing": "Espere, por favor...",
            "sSearch": "Buscar:"
        },
    });
    }
</script>