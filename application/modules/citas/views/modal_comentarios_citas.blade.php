<div style="overflow-y: scroll;height: 400px;" class="row">
	<div class="col-sm-12">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>Fecha</th>
					<th>Comentario</th>
					<th>Tipo de comentario</th>
				</tr>
			</thead>
			<tbody>
				@if(count($comentarios)>0)
				@foreach($comentarios as $c => $value)
					<tr>
						<td width="15%">{{$value->fecha}}</td>
						<td>{{$value->comentario}}</td>
						<td>{{$value->tipo}}</td>
					</tr>
				@endforeach
				@else
				<tr class="text-center">
					<td colspan="3">Aún no se han registrado comentarios... </td>
				</tr>
				@endif
			</tbody>

		</table>
	</div>
</div>
