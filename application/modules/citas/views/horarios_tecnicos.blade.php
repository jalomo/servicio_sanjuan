@layout('tema_luna/layout')
@section('contenido')

<div class="row">
	<div class="col-sm-12">
		<span style="font-size: 30px;font-weight: bold;text-align: center;">Técnico {{$nombre_tecnico}}</span>
		<button id="regresar" class="btn btn-default pull-right">Regresar</button>
	</div>
</div>
<br>
<form action="" method="POST" id="frm">
	<input type="hidden" id="id_tecnico" name="id_tecnico" value="{{$id_tecnico}}">
	<input type="hidden" id="id_horario" name="id_horario" value="0">
	<div class="row form-group">
		<div class='col-sm-3'>
			<label for="">Fecha</label>
			<input id="fecha" name="fecha" type='date' class="form-control" />
			<span class="error error_fecha"></span>
		</div>
		<div class="col-sm-3">
			<label for="">Hora inicio trabajo</label>
			<input  type="time" class="form-control" placeholder="hh:mm" value="" name="hora_inicio" id="hora_inicio">
			<span class="error error_hora_inicio"></span>
		</div>
		<div class="col-sm-3">
			<label for="">Hora fin trabajo</label>
			<input  type="time" class="form-control" placeholder="hh:mm" value="" name="hora_fin" id="hora_fin">
			<span class="error error_hora_fin"></span>
			<br>
			<!--button id="guardar" class="btn btn-success pull-right">Guardar</button>-->
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-3">
			<label for="">Hora inicio comida</label>
			<input  type="time" class="form-control" placeholder="hh:mm" value="" name="hora_inicio_comida" id="hora_inicio_comida">
			<span class="error error_hora_inicio_comida"></span>
		</div>
		<div class="col-sm-3">
			<label for="">Hora fin comida</label>
			<input  type="time" class="form-control" placeholder="hh:mm" value="" name="hora_fin_comida" id="hora_fin_comida">
			<span class="error error_hora_fin_comida"></span>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-3">
			<label for="">Hora inicio no laboral</label>
			<input  type="time" class="form-control" placeholder="hh:mm" value="" name="hora_inicio_nolaboral" id="hora_inicio_nolaboral">
		</div>
		<div class="col-sm-3">
			<label for="">Hora fin no laboral</label>
			<input  type="time" class="form-control" placeholder="hh:mm" value="" name="hora_fin_nolaboral" id="hora_fin_nolaboral">
		</form>
		<br>
		<button type="button" id="guardar" class="btn btn-success pull-right">Guardar</button>
	</div>
</div>
<h2 class="text-center">Lista de Horarios</h2>
<div class="row">
	<div class="col-sm-4">
		<label for="">Buscar por fecha</label>
		<input type="date" name="buscar_fecha" id="buscar_fecha" value="{{$fecha}}" class="form-control">
		<span class="error error_fecha_inicio"></span>
	</div>
	<div class="col-sm-1">
		<br>
		<button id="btn_por_fecha" style="margin-top: 10px;" class="btn btn-success">Buscar</button>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-12">
		<div id="div_tabla_horarios">
			<table class="table table-bordered table-striped" cellpadding="0" width="100%">
				<thead>
					<tr>
						<th style="text-align: center;">Hora inicio trabajo</th>
						<th style="text-align: center;">Hora fin trabajo</th>
						<th style="text-align: center;">Hora inicio comida</th>
						<th style="text-align: center;">Hora fin comida</th>
						<th style="text-align: center;">Hora inicio no laboral</th>
						<th style="text-align: center;">Hora fin no laboral</th>
						<th style="text-align: center;">Acciones</th>
					</tr>
				</thead>
				<tbody>
					@if(count($horarios)>0)
					@foreach($horarios as $key => $value)
					<tr>
						<td colspan="7">
							<strong>Fecha: {{date_eng2esp_1($key)}}</strong>
						</td>
					</tr>
					@foreach($value as $k => $horario)
					<tr>
						<td class="text-center">{{substr($horario->hora_inicio,0,5)}}</td>
						<td class="text-center">{{substr($horario->hora_fin,0,5)}}</td>
						<td class="text-center">{{substr($horario->hora_inicio_comida,0,5)}}</td>
						<td class="text-center">{{substr($horario->hora_fin_comida,0,5)}}</td>
						<td class="text-center">{{substr($horario->hora_inicio_nolaboral,0,5)}}</td>
						<td class="text-center">{{substr($horario->hora_fin_nolaboral,0,5)}}</td>
						<td style="text-align: center;">
							<a href="" data-id="{{$horario->id}}" class="pe pe-7s-note js_editar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar" data-fecha="{{date2sql($horario->fecha)}}" data-horai="{{substr($horario->hora_inicio,0,5)}}" data-horaf="{{substr($horario->hora_fin,0,5)}}" data-horaic="{{substr($horario->hora_inicio_comida,0,5)}}" data-horafc="{{substr($horario->hora_fin_comida,0,5)}}" data-horainl="{{substr($horario->hora_inicio_nolaboral,0,5)}}" data-horafnl="{{substr($horario->hora_fin_nolaboral,0,5)}}" ></a>


							@if($horario->activo)
								<a href="" data-id="{{$horario->id}}" class="js_activar" data-valor="0" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Desactivar horario">
									<i class="pe-7s-switch"></i>
								</a>
							@else
								<a href="" data-id="{{$horario->id}}" class="js_activar" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activar horario">
									<i class="pe-7s-check"></i>
								</a>
							@endif
							@if($horario->motivo!='')
								<a href="" class="fas fa-comments" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="{{$horario->motivo}}"></a>
							@endif

						</td>
					</tr>
					@endforeach
					@endforeach
					@else
					<tr style="text-align: center">
						<td colspan="7">No hay registros para mostrar...</td>
					</tr>
					@endif

				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
    	var site_url = "{{site_url()}}";
    	var valor = '';
		var mensaje = '';
		var id = '';
            $(function () {
            	$('.clockpicker').clockpicker();
                $('#datetimepicker1').datetimepicker({
                	minDate: moment(),
                	format: 'DD/MM/YYYY',
                	icons: {
	                    date: "fa fa-calendar",
	                    up: "fa fa-arrow-up",
	                    down: "fa fa-arrow-down"
                	},
                	 locale: 'es'
                });
                 $('.date').datetimepicker({
                	minDate: moment(),
                	format: 'DD/MM/YYYY',
                	icons: {
	                    date: "fa fa-calendar",
	                    up: "fa fa-arrow-up",
	                    down: "fa fa-arrow-down"
                	},
                	 locale: 'es'
                });
            });
            $("#regresar").on('click',function(){
            	window.location.href = "{{base_url('citas/lista_tecnicos')}}";
            });
            $("#btn_por_fecha").on('click',function(){
        		var url =site_url+"/citas/tabla_horarios_tecnicos";
		        ajaxLoad(url,{"id":$("#id_tecnico").val(),"fecha":$("#buscar_fecha").val()},"div_tabla_horarios","POST",function(){
		        	$('[data-toggle="tooltip"]').tooltip()
		      });
            });
             $("#guardar").on('click',function(){
            	callbackGuardar();
            });
              $("body").on("click",'.js_editar',function(e){
		    	e.preventDefault();
		        $("#id_horario").val($(this).data('id')) ;
		        $("#fecha").val($(this).data('fecha'));
		        $("#hora_inicio").val($(this).data('horai'));
		        $("#hora_fin").val($(this).data('horaf'));
		        $("#hora_inicio_comida").val($(this).data('horaic'));
		        $("#hora_fin_comida").val($(this).data('horafc'));
		        $("#hora_inicio_nolaboral").val($(this).data('horainl'));
		        $("#hora_fin_nolaboral").val($(this).data('horafnl'));

    		 });
              $("body").on("click",'.js_eliminar',function(e){
		       e.preventDefault();
		       var id = $(this).data('id')
		       ConfirmCustom("¿Está seguro de eliminar el horario?", callbackEliminarCita(id),"", "Confirmar", "Cancelar");
		      
		    });
	    	$("body").on("click",'.js_activar',function(e){
		       e.preventDefault();
		       //valor es 0 cuando va desactivar y 1 cuando lo va activar
		       valor = $(this).data('valor');
		       if(valor==1){
		       	mensaje ="¿Está seguro de activar el horario?";
		       }
		       id = $(this).data('id');
		       if(valor==1){
		       	 ConfirmCustom(mensaje, callbackActivarDesactivar,"", "Confirmar", "Cancelar");
		       	}else{
		       		 var url =site_url+"/citas/agregar_motivo/";
		       		customModal(url,{},"GET","md",callbackActivarDesactivar,"","Guardar","Cancelar","Motivo para desactivar","modal3");
		       	}
		      
		      
		    });
            function buscar(){
				var url =site_url+"/citas/tabla_horarios_tecnicos";
		        ajaxLoad(url,{"id":$("#id_tecnico").val(),"fecha":$("#fecha").val()},"div_tabla_horarios","POST",function(){
		        	$('[data-toggle="tooltip"]').tooltip()
		      });
			}
			function callbackGuardar(){
				var url =site_url+"/citas/agregar_horario_tecnico";
				ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
					if(isNaN(result)){
						data = JSON.parse( result );
						//Se recorre el json y se coloca el error en la div correspondiente
						$.each(data, function(i, item) {
							 $.each(data, function(i, item) {
			                    $(".error_"+i).empty();
			                    $(".error_"+i).append(item);
			                    $(".error_"+i).css("color","red");
			                });
						});
					}else{
						if(result ==-1){
							ErrorCustom('En la fecha seleccionada ya fue asignado el horario al técnico, por favor intenta con otro');
						}else{
							if(result==0){
								ErrorCustom('No se pudo guardar el horario del técnico, por favor intenta de nuevo');
							}else if(result==-2){
								ErrorCustom('No se pueden asignar las horas laborales por que el técnico tiene asignadas citas');
							}else{
								ExitoCustom("Horario guardado correctamente",function(){
								$(".close").trigger("click");
									buscar();
									$("#id_horario").val(0) ;
							        $("#fecha").val("");
							        $("#hora_inicio").val("");
							        $("#hora_fin").val("");
							        $("#hora_inicio_comida").val("");
							        $("#hora_fin_comida").val("");
							        $("#hora_inicio_nolaboral").val("");
		        					$("#hora_fin_nolaboral").val("");
								});
							}
						}
					}
				});
			}
			function callbackEliminarCita(id){
				var url =site_url+"/citas/eliminar_cita/";
				ajaxJson(url,{"id":id},"POST","",function(result){
					if(result ==0){
							ErrorCustom('El horario no se pudo eliminar, por favor intenta de nuevo');
						}else{
							ExitoCustom("Horario eliminado correctamente",function(){
							$(".close").trigger("click");
								buscar();
							});	
						}
				});
			}
			function callbackActivarDesactivar(){
		
				if(valor==1){
					 mensaje ="Horario activado correctamente";
					activar_desactivar();
				}else{
					 mensaje ="Horario desactivado correctamente";
					var motivo = $("#motivo").val();
					if(motivo==''){
						ErrorCustom("Es necesario ingresar el motivo por el que se desactiva al horario");	
					}else{
						 mensaje ="Horario desactivado correctamente";
						activar_desactivar();
					}
					
				}
				
			}
			function activar_desactivar(){
				var url =site_url+"/citas/cambiar_status/";
				ajaxJson(url,{"id":id,"valor":valor,"tabla":'horarios_tecnicos',"motivo":$("#motivo").val()},"POST","",function(result){
					if(result ==0){
							ErrorCustom('Error al activar o desactivar el operador, por favor intenta de nuevo');
						}else{
							ExitoCustom(mensaje,function(){
							$(".close").trigger("click");
								buscar();
							});	
						}
				});
			}
        </script>
@endsection