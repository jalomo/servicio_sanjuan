<div style="overflow-y: scroll;height: 400px;" class="row">
	<div class="col-sm-12">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th width="30%">Fecha</th>
					<th>Comentario</th>
					@if(isset($tipo_reagendar_proact))
					<th>Fecha notificación</th>
					@endif
				</tr>
			</thead>
			<tbody>
				@if(count($comentarios)>0)
				@foreach($comentarios as $c => $value)
					<tr>
						<td width="30%">{{$value->fecha_creacion}}</td>
						<td>{{$value->comentario}}</td>
						@if(isset($tipo_reagendar_proact))
						<td>{{$value->fecha_notificacion}}</td>
						@endif
					</tr>
				@endforeach
				@else
				<tr class="text-center">
					@if(isset($tipo_reagendar_proact))
						<td colspan="3">Aún no se han registrado comentarios... </td>
					@else
						<td colspan="2">Aún no se han registrado comentarios... </td>
					@endif
				</tr>
				@endif
			</tbody>

		</table>
	</div>
</div>