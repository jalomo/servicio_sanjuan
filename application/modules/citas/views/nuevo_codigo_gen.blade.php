<form action="" method="POST" id="frm">
	<input type="hidden" name="id" id="id" value="{{$input_id}}">
	<div class="row form-group">
		<div class="col-sm-12">
			<label>Código</label>
			{{$input_codigo}}
			<span class="error error_codigo"></span>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-12">
			<label>Descripción</label>
			{{$input_descripcion}}
			<span class="error error_descripcion"></span>
		</div>
	</div>
</form>