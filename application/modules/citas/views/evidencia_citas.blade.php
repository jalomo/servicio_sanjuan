@layout('tema_luna/layout')
@section('contenido')
	<h1>Evidencia</h1>
	<div class="row">
		<div class="col-sm-2">
			
		</div>
		<div class="col-sm-8">
			<table id="tbl_citas" class="table table-bordered table-striped" cellpadding="0" width="100%">
				<thead>
					<tr class="tr_principal">
						<th>ID</th>
						<th>Video / Imagen</th>
						<th>Fecha de cita</th>
						<th>Nombre del cliente</th>
						<th>Comentarios</th>
					</tr>
				</thead>
				<tbody>
					@foreach($citas as $c => $value)
						<tr>
							<td>{{$value->id_cita}}</td>
							<td>
							@if($value->tipo==2)
							<span data-tipo="{{$value->tipo}}" data-video="{{$value->video}}" class="video"><a href="">Click para ver imagen</a></span>
							@else
							<span data-tipo="{{$value->tipo}}" data-video="{{$value->video}}" class="video"><a href="">Click para ver video</a></span>
							@endif
							</td>
							<?php $fecha_mostrar = $value->fecha_dia.'/'.$value->fecha_mes.'/'.$value->fecha_anio;
							$fecha_comparar = $value->fecha_anio.'-'.$value->fecha_mes.'-'.$value->fecha_dia; 
							 ?>
							<td>{{$fecha_mostrar}}
							 	
							</td>
							<td>{{$value->datos_nombres.' '.$value->datos_apellido_paterno.' '.$value->datos_apellido_materno}}</td>
							<td>{{$value->comentario}}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection
@section('scripts')
<script>
	var site_url = "{{site_url()}}";
	var id_cita = '';
	var accion = '';
	 inicializar_tabla("#tbl_citas");
	$("body").on("click",'.video',function(e){
	 	e.preventDefault();
	 	video = $(this).data('video');
	 	tipo = $(this).data('tipo');
	 	if(tipo==2){
	 		var titulo = 'Imagen';
	 	}else{
	 		var titulo = 'Video';
	 	}
	    var url =site_url+"/citas/ver_video/0";
	    customModal(url,{"video":video,"tipo":tipo},"GET","md","","","","Salir",titulo,"modal1");
  	}); 
</script>
@endsection