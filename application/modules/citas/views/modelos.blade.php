@layout('tema_luna/layout')
@section('contenido')
	<ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="#">Inicio</a>
	    </li>
	    <li class="breadcrumb-item active">Modelos</li>
  	</ol>
	<div class="row">
		<div class="col-sm-10">
			<h1>Lista de modelos</h1>
		</div>
		<div class="col-sm-1">
			<button id="agregar_modelo" class="btn btn-success pull-right">Agregar modelo</button>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div id="div_modelos">
				<table class="table table-bordered table-striped" id="tbl_modelos" width="100%" cellspacing="0">
					<thead>
						<tr class="tr_principal">
							<th>Nombre</th>
							<th>Tiempo lavado</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($modelos as $c => $value)
						<tr>
							<td>{{$value->modelo}}</td>
							<td>{{$value->tiempo_lavado}}</td>
							<td>
								<a href="" data-id="{{$value->id}}" class="pe pe-7s-note js_editar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar"></a>
								
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('scripts')

	<script>
		var site_url = "{{site_url()}}";
		inicializar_tabla("#tbl_modelos",false);
	$("#agregar_modelo").on("click",function(){
       var url =site_url+"/citas/agregar_modelo/0";
       customModal(url,{},"GET","md",callbackGuardar,"","Guardar","Cancelar","Nuevo modelo","modal1");
    });
    $("body").on("click",'.js_editar',function(e){
    	e.preventDefault();
       var id = $(this).data('id')
       var url =site_url+"/citas/agregar_modelo/"+id;
       customModal(url,{},"GET","md",callbackGuardar,"","Guardar","Cancelar","Nuevo modelo","modal1");
      
    });
    $("body").on("click",'.js_eliminar',function(e){
       e.preventDefault();
       var id = $(this).data('id')
       ConfirmCustom("¿Está seguro de eliminar el modelo?", callbackEliminarmodelo(id),"", "Confirmar", "Cancelar");
      
    });

    
	function callbackGuardar(){
		var url =site_url+"/citas/agregar_modelo";
		ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
			if(isNaN(result)){
				data = JSON.parse( result );
				//Se recorre el json y se coloca el error en la div correspondiente
				$.each(data, function(i, item) {
					 $.each(data, function(i, item) {
	                    $(".error_"+i).empty();
	                    $(".error_"+i).append(item);
	                    $(".error_"+i).css("color","red");
	                });
				});
			}else{
				if(result <0){
					ErrorCustom('El nombre del modelo ya fue registrado, por favor intenta con otro');
				}else{
					if(result==0){
						ErrorCustom('No se pudo guardar el modelo, por favor intenta de nuevo');
					}else{
						ExitoCustom("Guardado correctamente",function(){
						$(".close").trigger("click");
							buscar();
						});
					}
				}
			}
		});
	}
	function buscar(){
		var url =site_url+"/citas/tabla_modelos";
        ajaxLoad(url,{},"div_modelos","POST",function(){
    		inicializar_tabla("#tbl_modelos",false);
      });
	}
	</script>
@endsection