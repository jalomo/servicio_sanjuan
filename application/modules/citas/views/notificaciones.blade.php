@layout('tema_luna/layout')
@section('contenido')
	<ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="#">Inicio</a>
	    </li>
	    <li class="breadcrumb-item active">Notificaciones</li>
  	</ol>
  	<h1>Lista de Notificaciones</h1>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div id="div_tecnicos">
				<table class="table table-bordered table-striped" id="tbl_notificaciones" width="100%" cellspacing="0">
					<thead>
						<tr class="tr_principal">
							<th>Titulo</th>
							<th>Mensaje</th>
							<th>Fecha</th>
						</tr>
					</thead>
					<tbody>
						@foreach($notificaciones as $c => $value)
						<tr>
							<td>{{$value->titulo}}</td>
							<td>{{$value->texto}}</td>
							<td>{{$value->fecha_hora}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('scripts')

	<script>
		var site_url = "{{site_url()}}";
		inicializar_tabla("#tbl_notificaciones",false);
	</script>
@endsection