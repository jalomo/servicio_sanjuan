<form action="" id="frm_comentarios">
	<input type="hidden" name="id_cita" id="id_cita" value="{{$id_cita}}">
	<input type="hidden" name="tipo_reagendar_proact_com" id="tipo_reagendar_proact_com" value="{{$tipo_reagendar_proact_com}}">

	
	<div class="row">
		<div class="col-sm-12">
			<div class="form-group">
				<label>Titulo</label>
				<input class="form-control" name="cronoTitulo" />
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-6">
			<label for="">Fecha inicio</label>
            <div class='input-group date' id='datetimepicker2'>
                <input type="text" class="form-control" value="{{date('d/m/Y')}}" name="cronoFecha">
                <span class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </span>
            </div>
		</div>
		<div class="col-lg-6">
			<label for="">Hora</label>
			<div class="input-group clockpicker" data-autoclose="true">
				<input type="text" class="form-control" value="" name="cronoHora">
				<span class="input-group-addon">
					<span class="far fa-clock"></span>
				</span>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<label>Comentario</label>
			{{$input_comentario}}
			<span class="error error_comentario"></span>
		</div>
	</div>
</form>
<script>
	$('.clockpicker').clockpicker();
	var fecha_actual = "<?php echo date('Y-m-d') ?>";
	$('.date').datetimepicker({
			minDate: fecha_actual,
            format: 'DD/MM/YYYY',
            icons: {
                time: "far fa-clock",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            locale: 'es'
        });
</script>