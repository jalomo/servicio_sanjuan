@layout('tema_luna/layout')
@section('contenido')
	<h1>Lista de Logística</h1>
	<div class="row">
		<div class="col-sm-12 text-right">
			<a style="margin-right: 15px;" href="citas/logistica/0" class="btn btn-info">Agregar registro</a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<table id="tbl_logistica" class="table table-hover table-striped">
				<thead>
					<tr class="tr_principal">
						<th>ID</th>
						<th>Entrega cliente</th>
						<th>Eco</th>
						<th>Unidad</th>
						<th>Color</th>
						<th>Serie</th>
						<th>Cliente</th>
						<th>Vendedor</th>
						<th>Tipo operación</th>
						<th>IVA DESG</th>
						<th>Toma</th>
						<th>Hora promesa</th>
						<th>Ubicación</th>
						<th>Reprogramación</th>
						<th>Seguro</th>
						<th>Permiso</th>
						<th>Placas</th>
						<th>Accesorios</th>
						<th>Servicio</th>
						<th>Fecha Prog</th>
						<th>Hora Prog</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($logistica as $c => $value)
						<tr>
							<td>{{$value->id}}</td>
							<td>{{$value->entrega_cliente}}</td>
							<td>{{$value->eco}}</td>
							<td>{{$value->id_unidad}}</td>
							<td>{{$value->id_color}}</td>
							<td>{{$value->serie}}</td>
							<td>{{$value->cliente}}</td>
							<td>{{$value->vendedor_clave}}</td>
							<td>{{$value->tipo_operacion}}</td>
							<td>{{$value->iva_desg}}</td>
							<td>{{$value->toma}}</td>
							<td>{{$value->hora_promesa}}</td>
							<td>{{$value->ubicacion}}</td>
							<td>{{($value->reprogramacion==1)?'Si':'No'}}</td>
							<td>{{($value->seguro==1)?'Si':'No'}}</td>
							<td>{{($value->permiso==1)?'Si':'No'}}</td>
							<td>{{($value->placas==1)?'Si':'No'}}</td>
							<td>{{$value->accesorio}}</td>
							<td>{{($value->servicio==1)?'Si':'No'}}</td>
							<td>{{date_eng2esp_1($value->fechaprog)}}</td>
							<td>{{$value->horaprog}}</td>
							
							<td>
								<a href=""  class="pe pe-7s-note editar_logistica" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar" data-id_logistica="{{$value->id}}" ></a>
								<a href="" data-id="{{$value->id}}" class="pe pe-7s-trash js_eliminar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Eliminar"></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection
@section('scripts')

<script>
	var site_url = "{{site_url()}}";
	var accion = '';
	var id_logistica = '';
	 inicializar_tabla("#tbl_logistica");
 	$(".editar_logistica").on("click",function(e){
	 	e.preventDefault();
	 	id_logistica = $(this).data('id_logistica');
	 	accion = "editar";
	       var url =site_url+"/citas/login_editar_cita/0";
	       customModal(url,{},"GET","md",ValidarLogin,"","Ingresar","Cancelar","Editar","modal1");
  	}); 
    $("body").on("click",'.js_eliminar',function(e){
	 	e.preventDefault();
	 	id_logistica = $(this).data('id');
	 	accion = "eliminar";
	       var url =site_url+"/citas/login_editar_cita/0";
	       customModal(url,{},"GET","md",ValidarLogin,"","Eliminar","Cancelar","Eliminar","modal1");
  	}); 
	function callbackEliminar(){
		var url =site_url+"/citas/eliminar_logistica/";
		ajaxJson(url,{"id":id_logistica},"POST","",function(result){
			if(result ==0){
					ErrorCustom('No se pudo eliminar el registro, por favor intenta de nuevo');
				}else{
					ExitoCustom("Registro eliminado correctamente",function(){
						window.location.href = site_url+"/citas/ver_logistica";
					});	
				}
		});
	}

	function ValidarLogin(){
			var url =site_url+"/citas/login_editar_cita";
			ajaxJson(url,{"usuario":$("#usuario").val(),"password":$("#password").val()},"POST","",function(result){
				if(isNaN(result)){
					data = JSON.parse( result );
					//Se recorre el json y se coloca el error en la div correspondiente
					$.each(data, function(i, item) {
						 $.each(data, function(i, item) {
		                    $(".error_"+i).empty();
		                    $(".error_"+i).append(item);
		                    $(".error_"+i).css("color","red");
		                });
					});
				}else{
					if(result <0){
						ErrorCustom('Usuario o contraseña inválidos.');
					}else if(result==0){
						ErrorCustom('El usuario no es administrador');
					}else{
						if(accion=='editar'){
							window.location.href = site_url+'/citas/logistica/'+id_logistica;
						}else{
							callbackEliminar();
						}
						
						
					}
				}
			});
		}
</script>
@endsection