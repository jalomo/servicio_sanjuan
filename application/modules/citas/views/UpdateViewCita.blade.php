@layout('tema_luna/layout')
@section('contenido')
<form action="" method="POST" id="frm">
	<div class="row">
		<div class="col-md-3 div_cita">
			<label for=""># Cita</label>
			<input type="number" id="id_cita" name="id_cita" id="id_cita" class="form-control">
			<div class="error error_id_cita"></div>
		</div>
		<div class="col-md-1 div_cita" style="margin-top: 30px;">
			<button type="button" id="buscar" class="js_carriover btn btn-success">Buscar</button>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4">
			<label for=""># Serie</label>
			{{$input_vehiculo_numero_serie}}
			<div class="error error_vehiculo_numero_serie"></div>
		</div>
		<div class="col-sm-4">
			<label for="">Kilometraje</label>
			{{$input_vehiculo_kilometraje}}
			<div class="error error_vehiculo_kilometraje"></div>
		</div>
		<div class="col-sm-4">
			<label for="">Placas</label>
			{{$input_vehiculo_placas}}
			<div class="error error_vehiculo_placas"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4">
			<label for="">Color</label>
			{{$drop_color}}
			<div class="error error_color"></div>
		</div>
	</div>
	<br>
	<div class="row pull-right">
		<div class="col-sm-12">
			{{$btn_guardar}}
		</div>
	</div>
	<br>
	<br>
	<br>
</form>
@endsection
@section('scripts')
<script>
	var site_url = "{{site_url()}}";
	$("#id_cita").on('change',callbackBuscar);
	$("#guardar").on('click',callbackGuardar);
	$("#buscar").on('click',callbackBuscar);
	function callbackGuardar(){
		var url =site_url+"/citas/UpdateViewCita";
		ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
			if(isNaN(result)){
				data = JSON.parse( result );
				//Se recorre el json y se coloca el error en la div correspondiente
				$.each(data, function(i, item) {
					 $.each(data, function(i, item) {
	                    $(".error_"+i).empty();
	                    $(".error_"+i).append(item);
	                    $(".error_"+i).css("color","red");
	                });
				});
			}else{
				if(result==0){
					ErrorCustom('No se pudo actualizar la cita');
				}else{
					ExitoCustom("Guardado correctamente",function(){
						location.reload();
					});
				}
			}
		});
	}
	function callbackBuscar(){
		var url =site_url+"/citas/getCitaId";
		var id_cita = $("#id_cita").val();
		if(id_cita!=''){	
			ajaxJson(url,{"id_cita":id_cita},"POST","",function(result){
				result = JSON.parse( result );
				if(result.exito){
					$("#vehiculo_numero_serie").val(result.datos.vehiculo_numero_serie);
					$("#vehiculo_kilometraje").val(result.datos.vehiculo_kilometraje);
					$("#vehiculo_placas").val(result.datos.vehiculo_placas);
					$("#id_color").val(result.datos.id_color);
				}else{
					ErrorCustom("No se encontró la cita",function(){
						$("#vehiculo_numero_serie").val('');
						$("#vehiculo_kilometraje").val('');
						$("#vehiculo_placas").val('');
						$("#id_color").val('');
					});
				}
			});
		}else{
			ErrorCustom("El campo #Cita es requerido");
		}
	}
</script>
@endsection



