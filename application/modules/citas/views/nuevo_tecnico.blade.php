<form action="" method="POST" id="frm">
	<input type="hidden" name="id" id="id" value="{{$input_id}}">
	<input type="hidden" name="pass_actual" id="pass_actual" value="{{$pass_actual}}">
	<div class="row form-group">
		<div class="col-sm-10">
			<label>Nombre del técnico</label>
			{{$input_nombre}}
			<span class="error error_nombre"></span>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-10">
			<label>Usuario</label>
			{{$input_usuario}}
			<span class="error error_usuario"></span>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-10">
			<label>IdStar</label>
			{{$input_idStars}}
			<span class="error error_idStars"></span>
		</div>
	</div>
	
	<div class="row form-group">
		<div class="col-sm-10">
			<label>Contraseña</label>
			{{$input_password}}
			<span class="error error_password"></span>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-10">
			<label>Confirmar contraseña</label>
			{{$input_repassword}}
			<span class="error error_repassword"></span>
		</div>
	</div>
</form>