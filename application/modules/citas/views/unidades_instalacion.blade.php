@layout('tema_luna/layout')
@section('contenido')
	<h1>Lista de prepedidos</h1>
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered table-striped table-striped table-responsive" id="tbl_unidades" width="100%" cellspacing="0">
				<thead>
					<tr class="tr_principal">
						<th>Acciones</th>
						<th>ID</th>
						<th>Serie</th>
						<th>Marca</th>
						<th>Modelo</th>
						<th>Color</th>
						<th>Año</th>
						<th>Cliente</th>
						<th>Teléfono</th>
						<th>Email</th>
						<th>R.F.C</th>
						<th>Motor</th>
						<th>Transmisión</th>
					</tr>
				</thead>
				<tbody>
					@foreach($unidades as $c => $unidad)
						<tr>
							<td>
								<a class="" href="{{base_url('citas/agendar_cita/0/0/0/0/'.$unidad->id)}}" data-id="{{$unidad->id}}"> <i class="pe pe-7s-plus"></i>
								</a>	
							</td>
							<td>{{$unidad->id}}</td>
							<td>{{$unidad->no_serie}}</td>
							<td>{{$unidad->nombre_marca}}</td>
							<td>{{$unidad->nombre_modelo}}</td>
							<td>{{$unidad->nombre_color}}</td>
							<td>{{$unidad->nombre_anio}}</td>
							<td>{{$unidad->nombre_cliente.' '.$unidad->cliente_apellido_paterno.' '.$unidad->cliente_apellido_materno}}</td>
							<td>{{$unidad->telefono}}</td>
							<td>{{$unidad->correo_electronico}}</td>
							<td>{{$unidad->cliente_rfc}}</td>
							<td>{{$unidad->motor}}</td>
							<td>{{$unidad->transmision}}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection
@section('scripts')

<script>
	var site_url = "{{site_url()}}";
	var id = '';
</script>
@endsection