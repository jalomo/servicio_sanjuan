@layout('tema_luna/layout')
@section('contenido')
  	<form action="" method="POST" id="frm">
    <input type="hidden" name="id" id="id" value="{{$input_id}}">
      <h3>Información</h3>
    <div class="row">
      <div class="col-sm-2 form-group">
        <label for="">No. orden </label>
        {{$input_no_orden}}
        <div class="error error_no_orden"></div>
      </div>
      <div class="col-sm-2 form-group">
        <label for="">Placas</label>
        {{$input_placas}}
        <div class="error error_placas"></div>
      </div>
       <div class="col-sm-4 form-group">
        <label for="">Tipo de orden</label>
        {{$input_tipo_orden}}
        <div class="error error_tipo_orden"></div>
      </div>
      <div class="col-sm-2 form-group">
        <label for="">Código Cliente</label>
        {{$input_codigoCte}}
        <div class="error error_codigoCte"></div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-2">
        <label for="">No. Torre</label>
        {{$input_notorre}}
        <div class="error error_notorre"></div>
      </div>
      <div class="col-sm-4">
        <label for="">No. Asesor</label>
        {{$input_noasesor}}
        <div class="error error_noasesor"></div>
      </div>
      <div class='col-sm-2'>
        <label for="">Fecha de recepción</label>
          <div class="form-group1">
              <div class='input-group date' id='datetimepicker1'>
                  {{$input_fecha_recepcion}}
                  <span class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                  </span>
              </div>
          </div>
           <span class="error_fecha_recepcion"></span>
      </div>
      <div class="col-sm-2">
        <label for="">Hora recepción</label>
          <div class="input-group clockpicker" data-autoclose="true">
              {{$input_hora_recepcion}}
              <span class="input-group-addon">
                  <span class="far fa-clock"></span>
              </span>
          </div>
          <span class="error_hora_recepcion"></span>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-2">
        <label for="">Tipo de servicio</label>
        {{$input_tipo_servicio}}
        <div class="error error_tipo_servicio"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Garantía</label>
        {{$input_garantia}}
        <div class="error error_garantia"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Tipo garantía</label>
        {{$input_tipogtia}}
        <div class="error error_tipogtia"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Cargo interno</label>
        {{$input_cargo_interno}}
        <div class="error error_cargo_interno"></div>
      </div>
      <div class="col-sm-2">
        <label for="">KM actual</label>
        {{$input_km_actual}}
        <div class="error error_cargo_km_actual"></div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-3">
        <label for="">Cliente problema</label>
        {{$input_cliente_problema}}
        <div class="error error_cliente_problema"></div>
      </div>
      <div class='col-sm-2'>
        <label for="">Fecha de entrega estimada</label>
          <div class="form-group1">
              <div class='input-group date' id='datetimepicker1'>
                  {{$input_fecha_entrega_estima}}
                  <span class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                  </span>
              </div>
          </div>
           <span class="error_fecha_entrega_estima"></span>
      </div>
      <div class="col-sm-2">
        <label for="">Hora entrega estimada</label>
          <div class="input-group clockpicker" data-autoclose="true">
              {{$input_hora_entrega_estima}}
              <span class="input-group-addon">
                  <span class="far fa-clock"></span>
              </span>
          </div>
          <span class="error_hora_entrega_estima"></span>
      </div>
     <div class='col-sm-2'>
        <label for="">Fecha de remisión</label>
          <div class="form-group1">
              <div class='input-group date' id='datetimepicker1'>
                  {{$input_fecha_remision}}
                  <span class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                  </span>
              </div>
          </div>
           <span class="error_fecha_remision"></span>
      </div>
      <div class="col-sm-2">
        <label for="">Hora remisión</label>
          <div class="input-group clockpicker" data-autoclose="true">
              {{$input_hora_remision}}
              <span class="input-group-addon">
                  <span class="far fa-clock"></span>
              </span>
          </div>
          <span class="error_hora_remision"></span>
      </div>
    </div>
    <br>
    <div class="row">
      <div class='col-sm-2'>
        <label for="">Fecha de factura</label>
          <div class="form-group1">
              <div class='input-group date' id='datetimepicker1'>
                  {{$input_fecha_factura}}
                  <span class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                  </span>
              </div>
          </div>
           <span class="error_fecha_factura"></span>
      </div>
      <div class="col-sm-2">
        <label for="">Hora factura</label>
          <div class="input-group clockpicker" data-autoclose="true">
              {{$input_hora_factura}}
              <span class="input-group-addon">
                  <span class="far fa-clock"></span>
              </span>
          </div>
          <span class="error_hora_factura"></span>
      </div>
      <div class="col-sm-2">
        <label for="">Nombre</label>
        {{$input_nombre}}
        <div class="error error_nombre"></div>
      </div>
      <div class="col-sm-2">
        <label for="">AP</label>
        {{$input_ap}}
        <div class="error error_ap"></div>
      </div>
      <div class="col-sm-2">
        <label for="">AM</label>
        {{$input_am}}
        <div class="error error_am"></div>
      </div>

    </div>
    <br>
    <div class="row">
      <div class="col-sm-2">
        <label for="">Razón social</label>
        {{$input_razon_social}}
        <div class="error error_razon_social"></div>
      </div>
      <div class="col-sm-3">
        <label for="">Calle</label>
        {{$input_calle}}
        <div class="error error_calle"></div>
      </div>
       <div class="col-sm-2">
        <label for=""># Exterior</label>
        {{$input_numero_exterior}}
        <div class="error error_numero_exterior"></div>
      </div>
       <div class="col-sm-2">
        <label for=""># Interior</label>
        {{$input_numero_interior}}
        <div class="error error_numero_interior"></div>
      </div>
       <div class="col-sm-3">
        <label for="">Colonia</label>
        {{$input_colonia}}
        <div class="error error_colonia"></div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-2">
        <label for="">Municipio</label>
        {{$input_municipio}}
        <div class="error error_municipio"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Estado</label>
        {{$input_estado}}
        <div class="error error_estado"></div>
      </div>
      <div class="col-sm-2">
        <label for="">C.P.</label>
        {{$input_cp}}
        <div class="error error_cp"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Teléfono principal</label>
        {{$input_tel_principal}}
        <div class="error error_tel_principal"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Teléfono secundario</label>
        {{$input_tel_secundario}}
        <div class="error error_tel_secundario"></div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-2">
        <label for="">Teléfono adicional</label>
        {{$input_tel_adicional}}
        <div class="error error_tel_adicional"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Correo</label>
        {{$input_correo}}
        <div class="error error_correo"></div>
      </div>
      <div class="col-sm-2">
        <label for="">R.F.C.</label>
        {{$input_rfc}}
        <div class="error error_rfc"></div>
      </div>
       <div class="col-sm-2">
        <label for="">Dirigirse con</label>
        {{$input_dirigirse_con}}
        <div class="error error_dirigirse_con"></div>
      </div>
      <div class="col-sm-4">
        <label for="">Serie Larga</label>
        {{$input_serie}}
        <div class="error error_serie"></div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-2">
        <label for="">No siniestro</label>
        {{$input_no_siniestro}}
        <div class="error error_no_siniestro"></div>
      </div>
      <div class="col-sm-2">
        <label for="">No. Poliza</label>
        {{$input_no_poliza}}
        <div class="error error_no_poliza"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Flotilla</label>
        {{$input_flotilla}}
        <div class="error error_flotilla"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Servicio Express</label>
        {{$input_servicio_express}}
        <div class="error error_servicio_express"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Servicio programado</label>
        {{$input_servicio_programado}}
        <div class="error error_servicio_programado"></div>
      </div>
      <div class="col-sm-2">
        <label for="">FM</label>
        {{$input_fm}}
        <div class="error error_fm"></div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-sm-2">
        <label for="">Factura</label>
        {{$input_factura}}
        <div class="error error_factura"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Estatus</label>
        {{$input_status}}
        <div class="error error_estatus"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Total MO</label>
        {{$input_total_mo}}
        <div class="error error_total_mo"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Total MO</label>
        {{$input_total_mo}}
        <div class="error error_total_mo"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Total Ref</label>
        {{$input_total_ref}}
        <div class="error error_total_ref"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Total Hoj</label>
        {{$input_total_hoj}}
        <div class="error error_total_hoj"></div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-2">
        <label for="">Total Pin</label>
        {{$input_total_pin}}
        <div class="error error_total_pin"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Total Lub</label>
        {{$input_total_lub}}
        <div class="error error_total_lub"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Total Oc</label>
        {{$input_total_oc}}
        <div class="error error_total_oc"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Total Om</label>
        {{$input_total_om}}
        <div class="error error_total_om"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Total Descuento</label>
        {{$input_total_descuento}}
        <div class="error error_total_descuento"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Subtotal</label>
        {{$input_subtotal}}
        <div class="error error_subtotal"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-2">
        <label for="">IVA</label>
        {{$input_iva}}
        <div class="error error_iva"></div>
      </div>
      <div class="col-sm-2">
        <label for="">Total</label>
        {{$input_total}}
        <div class="error error_total"></div>
      </div>
    </div>
    <br>
  </form>
    <div class="row pull-right">
      <div class="col-sm-12 ">
        {{$btn_guardar}}
      </div>
    </div>
  	<br>
    <br>
@endsection

@section('scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<script>
  var site_url = "{{site_url()}}";
   $('.date').datetimepicker({
      format: 'DD/MM/YYYY',
      icons: {
          time: "far fa-clock",
          date: "fa fa-calendar",
          up: "fa fa-arrow-up",
          down: "fa fa-arrow-down"
      },
       locale: 'es'
    });
   $('.clockpicker').clockpicker();
   $("#guardar").on('click',function(){
     var url = site_url+'/citas/agregar_magic/0';
        ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
                if(isNaN(result)){
                data = JSON.parse( result );
                //Se recorre el json y se coloca el error en la div correspondiente
                $.each(data, function(i, item) {
                    $(".error_"+i).empty();
                    $(".error_"+i).append(item);
                    $(".error_"+i).css("color","red");
                });
        }else{
            if(result==1){
              ExitoCustom("Guardado correctamente",function(){
                window.location.href = site_url+'citas/magic';
              });
            }else{
                  ErrorCustom('No se pudo guardar, intenta otra vez.');
            }
        }
        });
  });
</script>
@endsection