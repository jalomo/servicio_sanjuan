@layout('tema_luna/layout')
@section('contenido')
<div class="row">
	<div class="col-sm-12">
		<div style="margin-top: 10px" class="alert alert-success alert-dismissible fade show" role="alert">
        Lista de estatus permitidos, <strong>Para pasar a trabajando otro #cita!</strong>:
        {{$lista_status}}
      </div>
	</div>
</div>
<br>
<form action="" method="POST" id="frm">
	<div style="margin-top: 10px" class="alert alert-info alert-dismissible fade show" role="alert">
        Es necesario poner el #Cita que no te permite cambiar a trabajando y el sistema mostrará que #cita deberás cambiar.
      </div>
	<div class="row">
		<div class="col-md-3 div_cita">
			<label for=""># Cita</label>
			<input type="number" id="id_cita" name="id_cita" id="id_cita" class="form-control">
			<div class="error error_id_cita"></div>
		</div>
		<div class="col-sm-3">
			<label for="">Operación</label> <br>
			{{$drop_operaciones}}
			<div class="error error_id_operacion"></div>
		</div>
		<div class="col-md-1 div_cita" style="margin-top: 30px;">
			<button type="button" id="buscar" class="js_carriover btn btn-success">Buscar</button>
		</div>
	</div>
</form>
<div class="row">
	<div class="col-sm-12">
		<div id="tabla-items"></div>
	</div>
</div>
@endsection
@section('scripts')
<script>
	var site_url = "{{site_url()}}";
	$("#id_cita").on('change',callbackBuscar);
	$("#id_operacion").on('change',buscarRegistros);
	$("#buscar").on('click',buscarRegistros);
	function callbackBuscar(){
		var url =site_url+"/citas/getCitaId";
		var id_cita = $("#id_cita").val();
		if(id_cita!=''){	
			getOperaciones();
		}else{
			ErrorCustom("El campo #Cita es requerido");
		}
	}
	function buscarRegistros(){
		var id_cita = $("#id_cita").val();
		var id_operacion = $("#id_operacion").val();
		if(id_cita!='' && id_operacion!=''){	
			var url =site_url+"/citas/buscarTrabajando";
          		ajaxLoad(url,{"id_cita":id_cita,"id_operacion":id_operacion},"tabla-items","POST",function(){
	    	});
		}else{
			ErrorCustom("El campo #Cita y operación son requeridos");
		}
	    
  	} 
  	function getOperaciones(){
    var url=site_url+"/admin/getOperaciones";
		$("#id_operacion").empty();
		$("#id_operacion").append("<option value=''>-- Selecciona --</option>");
		id_cita=$("#id_cita").val();
		if(id_cita!=''){
			ajaxJson(url,{"id_cita":id_cita},"POST","",function(result){
				if(result.length!=0){
					$("#id_operacion").empty();
					
					result=JSON.parse(result);
					$("#id_operacion").append("<option value=''>-- Selecciona --</option>");
					$.each(result,function(i,item){
						
						$("#id_operacion").append("<option value= '"+result[i].id+"'>"+result[i].descripcion+"</option>");
					});
				}else{
					$("#id_operacion").empty();
					$("#id_operacion").append("<option value=''>No se encontraron datos</option>");
					$("#id_status").empty();
					$("#id_status").append("<option value=''>No se encontraron datos</option>");
				}
			});
		}else{
			$("#id_operacion").empty();
			$("#id_operacion").append("<option value=''>-- Selecciona --</option>");
			$("#id_operacion").attr("disabled",true);
		}
	}
</script>
@endsection



