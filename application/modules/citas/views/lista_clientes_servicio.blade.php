<div style="overflow-y: scroll;height: 400px;" class="row">
	<div class="col-sm-12">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Acciones</th>
					<th>Nombre</th>
					<th>Correo electrónico</th>
				</tr>
			</thead>
			<tbody>
				@if(count($datos_clientes)>0)
				@foreach($datos_clientes as $c =>$cliente)
					<tr>	
						<td>
							<a class="seleccionar_cliente_servicios" href="#" data-id_cita="{{$cliente->id_cita}}">Seleccionar</a>
						</td>
						<?php $nombre_cliente = $cliente->datos_nombres.' '.$cliente->datos_apellido_paterno.' '.$cliente->datos_apellido_materno ?>
						<td>{{$nombre_cliente}}</td>
						<td>{{$cliente->datos_email}}</td>
					</tr>
				@endforeach
				@else
					<tr class="text-center">
						<td colspan="3">No hay registros para mostrar...</td>
					</tr>
				@endif
				
			</tbody>
		</table>
	</div>
</div>