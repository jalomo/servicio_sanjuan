<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<style>
		.text-center{
			text-align: center;
		}
		table {
		    border-collapse: collapse;
		    border-spacing: 0;
		    font-size: 14px;
		}
		.table{
			width: 100%;
			max-width: 100%;
			background-color: #fff;
    		margin-bottom: 0;
		}
		.table > thead > tr > th {
		    background-color: #fff;
		    vertical-align: middle;
		    font-weight: 500;
		    color: #333;
		    border-bottom: 2px solid #C3C3C3;
		}
		.table-striped > tbody > tr:nth-of-type(odd) {
		    /*background-color: #f4f4f4;*/
		}
		table > tbody > tr > td, .table > tfoot > tr > td {
		    padding: 0px;
		    line-height: 1.42857143;
		    vertical-align: top;
		    border-top: 1px solid #f0f0f0;
		}
		*{
			font-size: 10px !important;
		}
	</style>
</head>

<body>
	<h2 style="margin-bottom: 0px; text-align: center;font-size: 30px !important;">{{CONST_AGENCIA}} S.A DE C.V.</h2>
	<h4 style="margin-top: 0px; text-align: center;font-size: 20px !important; margin-bottom: 0px;">Reporte de Reservaciones</h4>
	<h5 style="margin-top:0px;text-align: center;font-size: 15px !important;">Fecha {{$fecha}} </h5>
		<table width="100%" class="table  table-bordered table-striped">
		<thead>
			<tr>
				<th width="5%">ID</th>
				<th width="5%">Hora</th>
				<th width="5%">Placas</th>
				<th>Nombre y Apellido</th>
				<th width="5%">Año</th>
				<th>Modelo</th>
				<th>Color</th>
				<th>Técnico</th>
				<th>Duración</th>
				<th>Comentarios</th>
				
			</tr>
		</thead>
		<tbody>
			@foreach($citas as $c => $value)
				<tr>
					<td colspan="10"><h3 style="font-weight: bold">{{$c}}</h3></td>
				</tr>
				@foreach($value as $v => $item)	
				<?php $fecha_mostrar = $item->fecha_dia.'/'.$item->fecha_mes.'/'.$item->fecha_anio; ?>
					<tr>
						<td width="5%">{{$item->id_cita}}</td>
						<td width="5%">{{$item->hora}}</td>
						<td width="5%">{{$item->vehiculo_placas}}</td>
						<td>{{$item->datos_nombres.' '.$item->datos_apellido_paterno.' '.$item->datos_apellido_materno}}</td>
						<td width="5%">{{$item->vehiculo_anio}}</td>
						<td><span title="{{$item->vehiculo_modelo}}">{{trim_text($item->vehiculo_modelo,1)}}</span></td>
						<td>{{$item->color}}</td>
						<td>{{$item->tecnico}}</td>
						@if($item->id_status_color==2)
							<th>{{$this->mc->getHourService($item->id_cita,$item->fecha_hora)}}</th>
						@else
							<th></th>
						@endif
						<td>{{$item->comentarios_servicio}}</td>
						
					</tr>
					
				@endforeach
			@endforeach
		</tbody>
	</table>
</body>
</html>