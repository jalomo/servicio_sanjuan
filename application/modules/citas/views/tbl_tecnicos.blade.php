<table id="tbl_tecnicos" class="table table-bordered table-striped" cellpadding="0" width="100%">
	<thead>
		<tr class="tr_principal">
			<th>Nombre</th>
			<th>Usuario</th>
			<th>IdStar</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@foreach($tecnicos as $c => $value)
		<tr>
			<td>{{$value->nombre}}</td>
			<td>{{$value->nombreUsuario}}</td>
			<td>{{$value->idStars}}</td>
			<td>
				<a href="" data-id="{{$value->id}}" class="js_editar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar">
					<i class="pe pe-7s-note"></i>
				</a>

				<a href="{{base_url('citas/horarios_tecnico/'.$value->id)}}" data-id="{{$value->id}}" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Citas">
					<i class="pe-7s-menu"></i>
				</a>
				@if($value->activo)
					<a href="" data-id="{{$value->id}}" class=" js_activar" data-valor="0" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Desactivar técnico">
						<i class="pe-7s-switch"></i>
					</a>
				@else
					<a href="" data-id="{{$value->id}}" class="js_activar" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activar técnico">
						<i class="pe-7s-check"></i>
					</a>
				@endif
				@if($value->motivo!='')
					<a href="" class="fas fa-comments" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="{{$value->motivo}}"></a>
				@endif
				
			</td>
		</tr>
		@endforeach
	</tbody>
</table>