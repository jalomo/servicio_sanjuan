@layout('tema_luna/layout')
@section('contenido')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<form action="" id="frm">
<div class="row">
  <div class="col-sm-3 form-group">
    <label>Tipo de Búsqueda</label>
    <select name="tipo_busqueda" id="tipo_busqueda" class="form-control tipo_busquedas">
        <option value="">-- Selecciona -- </option>
        <option value="asesores">Estatus asesores</option>
        <option value="tecnicos">Estatus técnicos</option>
    </select>
   </div>
  <div class="col-sm-3 div_asesores">
     <label>Estatus asesores</label>
     <?php echo $drop_estatus ?>
  </div>
  <div class="col-sm-3 div_asesores">
    <label for="">Asesor</label>
    {{$drop_asesores}}
  </div>
  <div class="col-sm-3 div_tecnicos">
     <label>Estatus técnicos</label>
     <?php echo $drop_estatus_tecnicos ?>
  </div>
  <div class="col-sm-3 div_tecnicos">
    <label for="">Técnico</label>
    {{$drop_tecnicos}}
  </div>
     <div class="col-sm-2 form-group">
         <label>Año</label>
          <div class="form-group">
              <div class='input-group date' id='datetimepicker_anio'>
                    <input type="text" name="anio" id="anio" class="form-control">
                  <span class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                  </span>
              </div>
          </div>
     </div>
     <div class="col-sm-2 form-group">
         <label>Mes</label>
         <div class="form-group">
              <div class='input-group date' id='datetimepicker_mes'>
                    <input type="text" name="mes" id="mes" class="form-control">
                  <span class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                  </span>
              </div>
          </div>
     </div>
   <div class="col-sm-2 form-group">
      <button type="button" id="buscar" style="margin-top: 25px;" class="btn btn-success">Buscar</button>
   </div>
</div>
<div class="row">
  <div class="col-sm-3">
    <label>Citas reagendadas</label>
    <input type="checkbox" name="reagendada" id="reagendada">
  </div>
  <div class="col-sm-3">
    <label>Citas canceladas</label>
    <input type="checkbox" name="cancelada" id="cancelada">
  </div>
  <div class="col-sm-3">
    <label>Citas confirmadas</label>
    <input type="checkbox" name="confirmada" id="confirmada">
  </div>
</div>
</form>
<div class="row">
    <div class="col-sm-12">
        <div id="div_busqueda">
            
        </div>
        
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
  var site_url = "{{site_url()}}";
    $(function () {
      $(".div_asesores").hide();
      $(".div_tecnicos").hide();
      $("#tipo_busqueda").on('change',function(){
        var valor = $(this).val();
        if(valor=='asesores'){
          $(".div_asesores").show('slow');
          $(".div_tecnicos").hide('slow');
        }else{
          $(".div_asesores").hide('slow');
          $(".div_tecnicos").show('slow');
        }
      });
       $('#datetimepicker_anio').datetimepicker({
           format: 'YYYY',
           locale: 'es'
        });
       $('#datetimepicker_mes').datetimepicker({
           format: "MMM",
            viewMode: "months",
            locale: 'es'
        });
        $('#datetimepicker6').datetimepicker({
           format: 'DD/MM/YYYY',
           locale: 'es'
        });
        $('#datetimepicker7').datetimepicker({
            useCurrent: true, //Important! See issue #1075
            format: 'DD/MM/YYYY',
            locale: 'es'
        });
        $("#datetimepicker6").on("dp.change", function (e) {
          console.log($('#datetimepicker7').data("DateTimePicker").minDate(e.date));
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
           console.log($('#datetimepicker6').data("DateTimePicker").maxDate(e.date));
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });
    });
     $("#buscar").on('click',function(){
        buscar();
    });
 function buscar(){
        var url =site_url+"/citas/mostrar_graficas";
        if($("#tipo_busqueda").val()==''){
               ErrorCustom('Es necesario ingresar el tipo de búsqueda');
          }else{
              if($("#anio").val()==''){
                ErrorCustom('Es necesario ingresar el año');
              }else{
                  ajaxLoad(url,$("#frm").serialize(),"div_busqueda","POST",function(result){
                });
              }
             
          }

        
        
    }
</script>
@endsection