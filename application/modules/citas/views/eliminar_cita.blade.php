@layout('tema_luna/layout')
@section('contenido')

	<div class="row">
		<div class="col-sm-3 form-control col-sm-offset-1">
			<label for="">Cita</label>
			<input required="" type="text" class="form-control" name="cita" id="cita" value="">
			<br>
			<button id="eliminar" class="btn btn-success">Eliminar</button>
		</div>
	</div>
@endsection
@section('scripts')

<script>
	var site_url = "{{site_url()}}";
	var id_cita = '';
	var accion = '';
	 inicializar_tabla("#tbl_citas");
	 $("body").on("click",'#eliminar',function(e){
       e.preventDefault();
       var id = $(this).data('id')
       ConfirmCustom("¿Está seguro de eliminar la cita?", callbackEliminarCita,"", "Confirmar", "Cancelar");
      
    });
	function callbackEliminarCita(){
		var url =site_url+"/citas/eliminar_cita_bd/";
		ajaxJson(url,{"id":$("#cita").val()},"POST","",function(result){
			if(result ==0){
					ErrorCustom('La cita no existe');
				}else{
					$("#cita").val("");
					ExitoCustom("Cita eliminada correctamente");

				}
		});
	}

</script>
@endsection