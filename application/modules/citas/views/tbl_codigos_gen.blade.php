<table id="tbl_codigos" class="table table striped table-hover" width="100%">
	<thead>
		<tr class="tr_principal">
			<th>Código</th>
			<th>Descripción</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@foreach($codigos as $c => $value)
		<tr>
			<td>{{$value->codigo}}</td>
			<td>{{$value->descripcion}}</td>
			<td>
				<a href="" data-id="{{$value->id}}" class="pe pe-7s-note js_editar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar"></a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>