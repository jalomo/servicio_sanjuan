<table id="tbl_operadores" class="table table striped table-hover" width="100%">
	<thead>
		<tr class="tr_principal">
			<th>Nombre</th>
			<th>Teléfono</th>
			<th>Correo electrónico</th>
			<th>R.F.C.</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@foreach($operadores as $c => $value)
		<tr>
			<td>{{$value->nombre}}</td>
			<td>{{$value->telefono}}</td>
			<td>{{$value->correo}}</td>
			<td>{{$value->rfc}}</td>
			<td>
				<a href="" data-id="{{$value->id}}" class="js_editar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar"><i class="pe pe-7s-note"></i></a>
				
				<a href="{{base_url('citas/lista_citas/'.$value->id)}}" data-id="{{$value->id}}" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Citas"><i class="pe-7s-menu"></i></a>
				@if($value->activo)
					<a href="" data-id="{{$value->id}}" class="js_activar" data-valor="0" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Desactivar operador"><i class="pe-7s-switch"></i></a>
				@else
					<a href="" data-id="{{$value->id}}" class="js_activar" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activar operador"><i class="pe pe-7s-check"></i></a>
				@endif

				@if($value->motivo!='')
					<a href="" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="{{$value->motivo}}"><i class="fas fa-comments"></i></a>
				@endif
				
			</td>
		</tr>
		@endforeach
	</tbody>
</table>