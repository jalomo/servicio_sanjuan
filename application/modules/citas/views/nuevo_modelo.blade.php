<form action="" method="POST" id="frm">
	<input type="hidden" name="id" id="id" value="{{$input_id}}">
	<div class="row form-group">
		<div class="col-sm-12">
			<label>Modelo</label>
			{{$input_modelo}}
			<span class="error error_modelo"></span>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-12">
			<label>Tiempo lavado</label>
			{{$input_tiempo_lavado}}
			<span class="error error_tiempo_lavado"></span>
		</div>
	</div>
</form>