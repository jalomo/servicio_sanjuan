<table class="table table-bordered table-striped" id="tbl_modelos" width="100%" cellspacing="0">
	<thead>
		<tr class="tr_principal">
			<th>Nombre</th>
			<th>Tiempo lavado</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@foreach($modelos as $c => $value)
		<tr>
			<td>{{$value->modelo}}</td>
			<td>{{$value->tiempo_lavado}}</td>
			<td>
				<a href="" data-id="{{$value->id}}" class="pe pe-7s-note js_editar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar"></a>
				
			</td>
		</tr>
		@endforeach
	</tbody>
</table>