<!DOCTYPE html>
<html>

<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <base href="{{ base_url() }}">
    @include('tema_luna/head')
    <script type="text/javascript">
        var PATH = "<?php echo site_url(); ?>";
        var PATH_BASE = "<?php echo base_url(); ?>";
        var PATH_LANGUAGE = "<?php echo base_url('js/spanish.json'); ?>";
        var CONTROLLER = "<?php echo $this->router->class; ?>";
        var FUNCTION = "<?php echo $this->router->method; ?>";

    </script>
    <style>
		.label:empty{
			visibility: visible !important;
		}
        #div_tabla2 {
            width: 110em;
            overflow-x: auto;
            white-space: nowrap;
        }

        .etiqueta {
            border-radius: .25em;
            padding: .2em .6em .3em;
            color: white;
        }

        .etiqueta_status {
            border-radius: .25em;
            padding: .2em 1.6em .3em;
            color: white;
        }

        .color_comida {
            background-color: #000;
        }

        .color_planeado {
            background-color: #6EC1FA;
        }

        .color_trabajando {
            background-color: #F79500;
        }

        .color_terminado {
            background-color: #64D37E;
        }

        .color_nollego {
            background-color: #e9e9e9;
            color: #000;
        }

        .color_NA {
            background-color: #7B6135;
            color: #fff;
        }

        body {
            background-color: #BDBDBD;
        }

        table {}

        .cerrar_sesion {
            font-weight: bold;
        }

        .color_retrasado {
            background-color: #E33C18;
        }

        .color_hora_nolaboral {
            background-color: #8181F7;
        }

        .color_pendiente_autorizacion {
            background-color: #FA58F4;
        }

        .color_pendiente_refacciones {
            background-color: #0232F6;
        }

        .color_refacciones_entregadas {
            background-color: #60FAC9;
        }

        .color_refacciones_arribo {
            background-color: #92F504;
        }

        .color_refacciones_arribo_parciales {
            background-color: #9DCF54;
        }

        .color_d_autorizacion {
            background-color: #F72E8C;
        }

        .color_d_otros {
            background-color: #B74759;
        }

        .color_d_soporte {
            background-color: #F78888;
        }

        .color_d_tot {
            background-color: #F35050;
        }

        .color_d_refacciones {
            background-color: #F9ACAC;
        }


        .color_lavado {
            background-color: #05F6FA;
        }

        .color_prueba {
            background-color: #A903F7;
        }

        .color_u_prueba {
            background-color: #8E0A90;
        }

        .color_u_inmovilizada {
            background-color: #504F50;
        }

        /* NUEVA VERSION */

        .color_sin_orden {
            background-color: #BAF9F4;
        }

        .color_operacion_terminada {
            background-color: #046634;

        }

        div.arrow-down {
            width: 0;
            height: 0;
            border-left: 15px solid transparent;
            border-right: 15px solid transparent;
            border-top: 15px solid #006600;
            font-size: 0;
            line-height: 0;
            margin-top: -10px;
            margin-bottom: 5px;
            margin-left: 15px;
        }

        /*REFERENCIAS TRANGULO*/
        div.arrow_down_reference_tiempo {
            width: 0;
            height: 0;
            border-left: 20px solid transparent;
            border-right: 20px solid transparent;
            border-top: 20px solid #006600;
            font-size: 0;
            line-height: 0;
            margin-top: -10px;
            margin-bottom: 5px;
            margin-left: 15px;
        }

        div.arrow_down_reference_atraso {
            width: 0;
            height: 0;
            border-left: 20px solid transparent;
            border-right: 20px solid transparent;
            border-top: 20px solid red;
            font-size: 0;
            line-height: 0;
            margin-top: -10px;
            margin-bottom: 5px;
            margin-left: 15px;
        }

        div.arrow_down_reference_amarillo {
            width: 0;
            height: 0;
            border-left: 20px solid transparent;
            border-right: 20px solid transparent;
            border-top: 20px solid yellow;
            font-size: 0;
            line-height: 0;
            margin-top: -10px;
            margin-bottom: 5px;
            margin-left: 15px;
        }

        div.arrow_down_reference_naranja {
            width: 0;
            height: 0;
            border-left: 20px solid transparent;
            border-right: 20px solid transparent;
            border-top: 20px solid orange;
            font-size: 0;
            line-height: 0;
            margin-top: -10px;
            margin-bottom: 5px;
            margin-left: 15px;
        }

        div.arrow_down_reference_azul {
            width: 0;
            height: 0;
            border-left: 20px solid transparent;
            border-right: 20px solid transparent;
            border-top: 20px solid lightblue;
            font-size: 0;
            line-height: 0;
            margin-top: -10px;
            margin-bottom: 5px;
            margin-left: 15px;
        }

        /*FIXED TABLERO*/
        .table-responsive>.fixed-column {
            position: absolute;
            display: inline-block;
            width: auto;
            border-right: 1px solid #ddd;
        }

        @media(min-width:768px) {
            .table-responsive>.fixed-column {
                display: none;
            }
        }

        .table-scroll {
            position: relative;
            margin: auto;
            overflow: hidden;
            border: 1px solid #000;
        }

        .table-wrap {
            width: 100%;
            overflow: auto;
        }

        .table-scroll table {
            width: 100%;
            margin: auto;
            border-spacing: 0;
        }

        .table-scroll th,
        .table-scroll td {
            padding: 10px 10px;
            border: 1px solid #fff;
            white-space: nowrap;
        }

        .table-scroll thead,
        .table-scroll tfoot {}

        .clone {
            position: absolute;
            top: 0;
            left: 0;
            pointer-events: none;
        }

        .clone th,
        .clone td {
            visibility: hidden
        }

        .clone td,
        .clone th {
            border-color: transparent
        }

        .clone tbody th {
            visibility: visible;
            color: red;
        }

        .clone .fixed-side {
            border: 1px solid #000;
            background: #eee;
            visibility: visible;
        }

        .clone thead,
        .clone tfoot {
            background: transparent;
        }

        .mr5 {
            margin-right: 5px;
        }

        .mb10 {
            margin-bottom: 10px;
        }

        .mb15 {
            margin-bottom: 15px;
        }

        @media (max-width: 576px) {
            #triangle {
                visibility: hidden;
            }

            .mt15 {
                margin-top: 15px;
            }
        }

        /** NUEVA VERSIÓN */
        .esconder_div {
            display: none;
        }

        .mostrar_div {
            visibility: visible;
        }
        }

    </style>
</head>

<body>
    <!-- Wrapper-->
    <div class="wrapper">
        <section class="content" style="margin-left:10px;margin-top:10px;">
            <div class="container-fluid">
                <div class="container-fluid panel-body">
                    <div class="row">
						<div class="col-sm-12">
							<span style="font-size: 25px;font-weight: bold;text-align: center;">Tablero de citas por técnico</span>
							<a href="citas/cerrar_sesion" class="pull-right cerrar_sesion">Cerrar Sesión</a>
						</div>
					</div>
					<div class="row form-group">
						<div class='col-sm-3'>
							<label for="">Selecciona la fecha</label>
							<input id="fecha" name="fecha" type='date' class="form-control" value="{{$fecha}}" />
							 <span class="error_fecha"></span>
						</div>
						<div class="col-sm-2">
							<br>
							<button id="buscar" style="margin-top: 8px;" class="btn btn-success">Buscar</button>
							@if($origen=='refacciones')
							<a target="_blank" href="{{site_url('citas/refacciones_filtro')}}" style="margin-top: 8px;" class="btn btn-info">Filtro diario de citas</a>
							@endif
						</div>
						<div id="arrow" class="col-sm-7 pull-right">
							<span style="margin-left: 10px" class="pull-right">click para mostrar/ocultar</span>
							<i data-toggle="tooltip" data-placement="top" title="Esconder" style="font-size: 40px" class="fa fa-chevron-down pull-right js_esconder" aria-hidden="true" data-mostrar="0"></i>
						</div>
				
				</div>
				<div id="all">
					<br>
					<div class="row">
						<div class="col-md-4 col-lg-2  mb15">
							<div class="arrow_down_reference_tiempo"></div>
							<span style="color: #000" for="">A tiempo</span>
						</div>
						<div class="col-md-4 col-lg-2 mb15">
							<div class="arrow_down_reference_amarillo"></div>
							<span style="color: #000" for="">Proximo a hora de termino</span>
						</div>
						<div class="col-md-4 col-lg-2 mb15">
							<div class="arrow_down_reference_naranja"></div>
							<span style="color: #000" for="">Detenido</span>
						</div>
						<div class="col-md-4 col-lg-2 mb15">
							<div class="arrow_down_reference_atraso"></div>
							<span style="color: #000" for="">Retrasado</span>
						</div>
						<div class="col-md-4 col-lg-2 mb15">
							<div class="arrow_down_reference_azul"></div>
							<span style="color: #000" for="">terminado</span>
						</div>
					</div>
					<h3>Estatus exterior</h3>
					<div class="row">
						<div class="col-lg-4 col-md-12">
							<span data-toggle="tooltip" data-placement="top" title="Hora de comida" class="label etiqueta_status mr5 color_comida"></span>
							<span data-toggle="tooltip" data-placement="top" title="Planeado" class="label etiqueta_status mr5 color_planeado"></span>
							<span data-toggle="tooltip" data-placement="top" title="Trabajando" class="label etiqueta_status mr5 color_trabajando"></span>
							<span data-toggle="tooltip" data-placement="top" title="Retrasado" class="label etiqueta_status mr5 color_retrasado js_ver_unidad" data-idstatus="4" ></span>
							<span data-toggle="tooltip" data-placement="top" title="Terminado" class="label etiqueta_status mr5 color_terminado"></span>
							<span data-toggle="tooltip" data-placement="top" title="Cliente no llegó" class="label etiqueta_status mr5 color_nollego"></span>
							<span data-toggle="tooltip" data-placement="top" title="Hora no laboral" class="label etiqueta_status mr5 color_hora_nolaboral"></span>
						</div>
						<div class="col-lg-4 col-md-12">
							<span data-toggle="tooltip" data-placement="top" title="Pendiente por autorización" class="label etiqueta_status mr5 color_pendiente_autorizacion js_ver_unidad" data-idstatus="6"></span>
							<span data-toggle="tooltip" data-placement="top" title="No aplica" class="label etiqueta_status mr5 color_NA"></span>
							<span data-toggle="tooltip" data-placement="top" title="Pendiente Refacciones" class="label etiqueta_status mr5 color_pendiente_refacciones"></span>
							<span data-toggle="tooltip" data-placement="top" title="Refacciones entregadas" class="label etiqueta_status mr5 color_refacciones_entregadas"></span>
							<span data-toggle="tooltip" data-placement="top" title="Arribo de refacciones" class="label etiqueta_status mr5 color_refacciones_arribo js_ver_unidad" data-idstatus="13"></span>
							<span data-toggle="tooltip" data-placement="top" title="Arribo de refacciones parciales" class="label etiqueta_status mr5 color_refacciones_arribo_parciales js_ver_unidad" data-idstatus="14"></span>
							<span data-toggle="tooltip" data-placement="top" title="Lavado" class="label etiqueta_status mr5 color_lavado"></span>
							<span data-toggle="tooltip" data-placement="top" title="Unidad en prueba" class="label etiqueta_status mr5 color_prueba"></span>
						</div>
						<div class="col-lg-4 col-md-12">
							
							<span data-toggle="tooltip" data-placement="top" title="Detenida por otros" class="label etiqueta_status mr5 color_d_otros js_ver_unidad" data-idstatus="16"></span>
							<span data-toggle="tooltip" data-placement="top" title="Detenida por soporte" class="label etiqueta_status mr5 color_d_soporte js_ver_unidad" data-idstatus="17"></span>
							<span data-toggle="tooltip" data-placement="top" title="Detendida por TOT" class="label etiqueta_status mr5 color_d_tot js_ver_unidad" data-idstatus="18"></span>
							<span data-toggle="tooltip" data-placement="top" title="Detenida por refacciones" class="label etiqueta_status mr5 color_d_refacciones js_ver_unidad" data-idstatus="19"></span>
							<span data-toggle="tooltip" data-placement="top" title="Espera de prueba" class="label etiqueta_status mr5 color_u_prueba"></span>
							<span data-toggle="tooltip" data-placement="top" title="Unidad inmovilizada" class="label etiqueta_status mr5 color_u_inmovilizada"></span>
							<span data-toggle="tooltip" data-placement="top" title="Sin orden" class="label etiqueta_status mr5 color_sin_orden"></span> <!-- NUEVA VERSION -->
							<span data-toggle="tooltip" data-placement="top" title="Operación terminada" class="label etiqueta_status mr5 color_operacion_terminada"></span> <!-- NUEVA VERSION -->
						</div>
					</div>
					<br>
					<hr>
					<div class="row">
						<div class="col-sm-6">
							<h3>Estatus interior</h3>
							<div class="row">
								<div class="col-sm-12 mb10">
									@if(count($status)>0)
										@foreach($status as $e => $value)
											 <span data-toggle="tooltip" data-placement="top" title="{{$value->nombre}}" class="label etiqueta_status mr5" style="background-color: {{$value->hexadecimal}}"></span>
										@endforeach
									@endif
								</div>
							</div>
							
						</div>
						<br>
						<div class="col-sm-6">
							<span class="pull-right">Click en la imagen para ver explicación</span>
							<img id="explicacion" class="pull-right"  src="img/tabtec03.png" alt="" style="width: 120px;height: 120px;margin: 0 auto; margin-left: 20px;">
						</div>
					</div>
				</div>
				<br>
				<div class="row mt15">
					<div class="col-sm-8 col-md-3">
						<input type="number" name="buscar_id" id="buscar_id" class="form-control" placeholder="Ingresa el ID de la cita">
					</div>
					<div class="col-sm-3">
						<button id="buscar_cita" class="btn btn-info">Buscar cita</button>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-12">
						<div id="div_tabla" class="table-scroll">
							<div class="table-wrap">
								<table class="table table-striped main-table" scope="col">
									<thead>
										<tr>
											
											<th style="color:#000 !important" width="20%" class="fixed-side" scope="col">Técnico</th>
											<th style="color:#000 !important">Hora L.</th>
											<?php
												 $time = '09:00';
												 $contador=0;
											?>
											@while($contador<$valor-6)
													<td scope="col">{{$time}}</td>
												<?php
													$timestamp = strtotime($time) + $tiempo_aumento*60;
													$time = date('H:i', $timestamp);
													$contador++;
												?>
											@endwhile
										</tr>
									</thead>
									<tbody>
										@foreach($tecnicos as $s => $value)
											<tr>
												<td class="nombre_tecnico fixed-side">{{$value->nombre}}</td>
												<td>{{substr($value->hora_inicio, 0,5).'-'.substr($value->hora_fin, 0,5)}}</td>
												<?php
												 $time = '09:00';
												 $contador=0;
												?>
												@while($contador<$valor-6)
												<?php
												//$timestamp = strtotime($hora) + 1*60;
												//$hora = date('H:i', $timestamp).':00';
												?>
													@if($value->activo)
														<?php $tiempoplus1 = strtotime($time) + 1*60;
															$tiempoplus1 = date('H:i', $tiempoplus1);
														 ?>
														@if($tiempoplus1.':00'<=$value->hora_inicio_nolaboral || $tiempoplus1.':00' >= $value->hora_fin_nolaboral)
															<?php $fecha_hora = $fecha.' '.$time;
																  $datetimenow = date('Y-m-d H:i:s');
																  if($fecha_hora>$datetimenow){
																	$clase = 'asignar_cita_tablero';
																  }else{
																	$clase = '';
																  }
                                                                  $id = str_replace(':','',$time).'_'.$value->id;
				
															 ?>
															<?php $tiempo = strtotime($time) + 1*60;
																$tiempo_comparar = date('H:i', $tiempo);
															 ?>
															<!-- validación de la hora de comida -->
															@if($tiempo_comparar.':00'>$value->hora_inicio_comida &&  $tiempo_comparar.':00'<$value->hora_fin_comida)
																<td data-id_tecnico= "{{$value->id}}" style="background-color: #000" class="" data-time="{{$time}}"></td>
															<!-- validación de la hora laboral -->
															@elseif($time.':00'>=$value->hora_inicio && $time.':00'<$value->hora_fin && $value->activo_fecha==1)
																<?php 
																$ocupado = $this->mc->HoraBetweenCita($time,$fecha,$value->id,$origen);
																$consulta = $this->db->last_query();
																
																$explode = explode('-', $ocupado);
																$resultado = $explode[0];
																
																$id_cita = $explode[1];
																$id_operacion = $explode[2];
				
																if(!$resultado){
																	$ocupado_historial = $this->mc->HoraBetweenCitaHistorial($time,$fecha,$value->id,$origen);
																	//$consulta_historial = $this->db->last_query();
																	$explode_historial = explode('-', $ocupado_historial);
																	$resultado_historial = $explode_historial[0];
																	$id_cita_historial = $explode_historial[1];
																	$id_carryover = $explode_historial[2];
																	$id_operacion = $explode_historial[3];
																	if($explode_historial[4]){
																		$color_etiqueta = 'black';
																	}else{
																		$color_etiqueta = 'white';
																	} //si es 1 tiene carryover
																	
																}else{
																	$color_etiqueta = 'white';
																}
															?>
															@if($resultado)
																<?php
																  $color_cita = $this->mc->getColorCita($id_cita,$id_operacion,$origen);
																  if(isset($color_cita)){
																	  $color_cita = explode('-', $color_cita);
																	  $color_cuadro = $color_cita[0];
																	  $color_circulo = $color_cita[1];
																	  $id_status_cita = isset($color_cita[2])?$color_cita[2]:'';
																  }else{
																	  $color_cita = '';
																	  $color_cuadro = '';
																	  $color_circulo = '';
																	  $id_status_cita = '';
																  }
																  //Comparar para ver si es el último registro de la cita
				
																?>
																@if($id_status_cita!=5)
																<!--//NUEVA VERSION -->
																	<td data-tabla_operacion="tecnicos_citas" data-idcarryover="0" data-idoperacion="{{$id_operacion}}" data-cita="{{$id_cita}}" class="cita_{{$id_cita}} js_cambiar_status" style="background-color: {{$color_cuadro}}; cursor: pointer;">
																	<div data-idoperacion="{{$id_operacion}}" class="arrow-down triangle triangle_{{$id_cita}}_{{$id_operacion}}" data-idcita="{{$id_cita}}" data-time="{{$time}}" data-idstatuscita="{{$id_status_cita}}"></div>
																	<span class="label etiqueta mr5" style="background-color: {{$color_circulo}};border: 2px solid {{$color_etiqueta}}; cursor: pointer;">{{$id_cita}}</span></td>
																@else
																<td data-id_tecnico= "{{$value->id}}" class="{{$clase}}" id="{{$id}}" data-time="{{$time}}">
																</td>
				
																@endif
																
															@elseif($resultado_historial)
															<?php
				
																  $color_cita = $this->mc->getColorCita($id_cita_historial,$id_operacion,$origen);
																  if(isset($color_cita)){
																	  $color_cita = explode('-', $color_cita);
																	  $color_cuadro = $color_cita[0];
																	  $color_circulo = $color_cita[1];
																	  $id_status_cita = isset($color_cita[2])?$color_cita[2]:'';
																  }else{
																	  $color_cita = '';
																	  $color_cuadro = '';
																	  $color_circulo = '';
																	  $id_status_cita = '';
																  }
																  
																?>
																<!--//NUEVA VERSION -->
																<td data-tabla_operacion="tecnicos_citas_historial" data-idcarryover="{{$id_carryover}}" data-idoperacion="{{$id_operacion}}" data-cita="{{$id_cita_historial}}" class="cita_{{$id_cita_historial}} js_cambiar_status" style="background-color: {{$color_cuadro}}; cursor: pointer;">
																	<div data-idoperacion="{{$id_operacion}}" class="arrow-down triangle triangle_{{$id_cita}}_{{$id_operacion}}" data-idcita="{{$id_cita}}" data-time="{{$time}}" data-idstatuscita="{{$id_status_cita}}"></div>
																	<span class="label etiqueta mr5" style="background-color: {{$color_circulo}};border: 2px solid {{$color_etiqueta}}; cursor: pointer;">{{$id_cita_historial}}</span></td>
																<!--<td data-id_tecnico= "{{$value->id}}" class="{{$clase}}" data-time="{{$time}}">-->
															@else
																<td data-id_tecnico= "{{$value->id}}" class="{{$clase}}" id="{{$id}}" data-time="{{$time}}">
																</td>
															@endif
																
															@else
																<td class="color_hora_nolaboral"></td>
															@endif
														@else <!-- validar que no tenga bloqueada la hora -->
															<td class="color_hora_nolaboral"></td>
														@endif
														
													@else <!-- validar que este activo -->
														<td class="color_hora_nolaboral"></td>
													@endif
													<?php
														$timestamp = strtotime($time) + $tiempo_aumento*60;
														$time = date('H:i', $timestamp);
														$contador++;
													?>
												@endwhile
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
                </div>
            </div>
        </section>
        <!-- End main content-->
    </div>
    <!-- End wrapper-->
    <!-- Vendor scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    @include('tema_luna/dependencias_footer')
    @if ($origen == 'refacciones')
        <script type="text/javascript">
            var site_url = "{{ site_url() }}";
            var array_triangle = [];
            $('#datetimepicker1').datetimepicker({
                //minDate: moment(),
                format: 'DD/MM/YYYY',
                icons: {
                    time: "far fa-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                locale: 'es'
            });
            $("#buscar").on('click', buscar);
            $("body").on('click', '.js_cambiar_status', function() {
                var url = site_url + "/citas/login_tecnicos/";
                id_cita = $(this).data('cita');
                //NUEVA VERSIÓN
                var id_operacion = $(this).data('idoperacion');
                usuario_tecnico = $(this).closest('tr').find('.nombre_tecnico').text();
                customModal(url, {
                        "usuario_tecnico": usuario_tecnico,
                        "id_cita": id_cita,
                        "origen": 'refacciones',
                        "id_operacion": id_operacion
                    }, "GET", "lg", GuardarComentarioRefacciones, "", "Guardar", "Cerrar", "Ingreso",
                    "modal1");
            });
            $("body").on('click', '.historial_comentarios_refacciones', function(e) {
                e.preventDefault();
                var cita = $(this).data('idcita');
                var url = site_url + "/citas/comentarios_refacciones/";
                customModal(url, {
                    "idcita": cita
                }, "POST", "md", "", "", "", "Cerrar", "Historial de comentarios", "modal11");
            });

            function buscar() {
                var url = site_url + "/citas/tabla_horarios_tecnicos_busqueda";
                ajaxLoad(url, {
                    "fecha": $("#fecha").val(),
                    "origen": 'refacciones'
                }, "div_tabla", "POST", function() {
                    updateTimes();
                    $(".main-table").clone(true).appendTo('#div_tabla').addClass('clone');
                });
            }

            function GuardarComentarioRefacciones() {
                var url = site_url + "/citas/saveComentarioRefacciones";
                var realizo_prepiking = 0;
                if ($("#realizo_prepiking").prop('checked')) {
                    var realizo_prepiking = 1;
                }
                ajaxJson(url, {
                    "idcita": id_cita,
                    "comentario": $("#comentario_refacciones").val(),
                    "id_status": $("#id_status").val(),
                    "realizo_prepiking": realizo_prepiking,
                    'usuario_tecnico': $("#usuario_tecnico").val(),
                    'password_tecnico': $("#password_tecnico").val()
                }, "POST", "", function(result) {
                    if (isNaN(result)) {
                        data = JSON.parse(result);
                        //Se recorre el json y se coloca el error en la div correspondiente
                        $.each(data, function(i, item) {
                            $.each(data, function(i, item) {
                                $(".error_" + i).empty();
                                $(".error_" + i).append(item);
                                $(".error_" + i).css("color", "red");
                            });
                        });
                    } else {
                        if (result == 0) {
                            ErrorCustom('Error al guardar el comentario');
                        } else if (result == -1) {
                            ErrorCustom('Usuario o contraseña inválidos.');
                        } else {
                            $("#comentario_refacciones").val('');
                            $(".close").trigger("click");
                            ExitoCustom("Comentario guardado correctamente", function() {
                                buscar();
                            });

                        }
                    }
                });
            }

            function ModalCambiarStatus() {
                var url = site_url + "/citas/login_tecnicos";
                usuario_ingreso = $("#usuario").val();
                password_ingreso = $("#password").val();
                ajaxJson(url, {
                    "usuario": $("#usuario").val(),
                    "password": $("#password").val()
                }, "POST", "", function(result) {
                    if (isNaN(result)) {
                        data = JSON.parse(result);
                        //Se recorre el json y se coloca el error en la div correspondiente
                        $.each(data, function(i, item) {
                            $.each(data, function(i, item) {
                                $(".error_" + i).empty();
                                $(".error_" + i).append(item);
                                $(".error_" + i).css("color", "red");
                            });
                        });
                    } else {
                        if (result == 0) {
                            ErrorCustom('Usuario o contraseña inválidos.');
                        } else {
                            $(".close").trigger("click");
                            var url = site_url + "/citas/modal_cita_tecnico";
                            customModal(url, {
                                    "id_cita": id_cita,
                                    "fecha": $("#fecha").val()
                                }, "GET", "lg", CambiarStatus, "", "Guardar", "Cancelar", "Ingreso",
                                "modal1");

                        }
                    }
                });
            }
            $(".main-table").clone(true).appendTo('#div_tabla').addClass('clone');

        </script>
    @else
        <script type="text/javascript">
            var site_url = "{{ site_url() }}";
            var id_cita = '';
            var usuario_tecnico = '';
            var idtecnico = '';
            var time = '';
            var usuario_ingreso = '';
            var password_ingreso = '';
            var array_triangle = [];
            var idcarryover = '';
            var tabla_operacion = '';
            $('#datetimepicker1').datetimepicker({
                //minDate: moment(),
                format: 'DD/MM/YYYY',
                icons: {
                    time: "far fa-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
                locale: 'es'
            });
            $("body").on('click', '.js_ver_unidadbk', function() {
                var idstatus = $(this).data('idstatus');
                var url = site_url + "/citas/ver_unidades_status/";
                customModal(url, {
                    "idstatus": idstatus
                }, "GET", "md", "", "", "", "Cerrar", "Unidades por estatus", "modalStatus");
            });
            $("body").on('click', '#explicacion', function() {
                var url = site_url + "/citas/explicacion_leyenda/";
                customModal(url, {}, "GET", "md", "", "", "", "Cerrar", "Leyendas", "explicacionM");
            });
            $("#buscar").on('click', buscar);
            $("body").on('click', '.js_cambiar_status', function() {
                var url = site_url + "/citas/login_tecnicos/";
                id_cita = $(this).data('cita');
                var idcarryover = $(this).data('idcarryover');
                tabla_operacion = $(this).data('tabla_operacion');
                id_operacion = $(this).data('idoperacion');
                usuario_tecnico = $(this).closest('tr').find('.nombre_tecnico').text();
                if (usuario_tecnico == '' || usuario_tecnico == undefined) {
                    usuario_tecnico = $(this).data('tecnico');
                }
                //NUEVA VERSION
                customModal(url, {
                    "usuario_tecnico": usuario_tecnico,
                    "id_cita": id_cita,
                    "origen": 'tablero',
                    "idcarryover": idcarryover,
                    "id_operacion": id_operacion
                }, "GET", "lg", ModalCambiarStatus, "", "Aceptar", "Cancelar", "Ingreso", "modal1");
            });

            function buscar() {
                var url = site_url + "/citas/tabla_horarios_tecnicos_busqueda";
                ajaxLoad(url, {
                    "fecha": $("#fecha").val(),
                    "origen": 'tecnicos'
                }, "div_tabla", "POST", function() {
                    updateTimes();
                    $(".main-table").clone(true).appendTo('#div_tabla').addClass('clone');
                });
            }
            //NUEVA VERSION
            function ModalCambiarStatus() {
                var url = site_url + "/citas/login_tecnicos";
                usuario_ingreso = $("#usuario").val();
                password_ingreso = $("#password").val();
                ajaxJson(url, {
                    "usuario": $("#usuario").val(),
                    "password": $("#password").val(),
                    'origen': 'taller'
                }, "POST", "", function(result) {
                    if (isNaN(result)) {
                        data = JSON.parse(result);
                        //Se recorre el json y se coloca el error en la div correspondiente
                        $.each(data, function(i, item) {
                            $.each(data, function(i, item) {
                                $(".error_" + i).empty();
                                $(".error_" + i).append(item);
                                $(".error_" + i).css("color", "red");
                            });
                        });
                    } else {
                        if (result == 0) {
                            ErrorCustom('Usuario o contraseña inválidos.');
                        } else {
                            $(".close").trigger("click");
                            var url = site_url + "/citas/modal_cita_tecnico";
                            //NUEVA VERSION
                            customModal(url, {
                                    "id_cita": id_cita,
                                    "fecha": $("#fecha").val(),
                                    "id_operacion": id_operacion
                                }, "GET", "lg", changeConfirm, "", "Guardar", "Cancelar", "Ingreso",
                                "modal1");

                        }
                    }
                });
            }

            function changeConfirm() {
                if ($("#id_status").val() == 3) {
                    ConfirmCustom("¿Estás seguro de terminar las operaciones? NO PODRÁS REGRESAR DICHO CAMBIO",
                        CambiarStatus, "", "Confirmar", "Cancelar");
                } else {
                    CambiarStatus();
                }

            }

            function CambiarStatus(id_cita) {
                var url = site_url + "/citas/cambiar_status_cita_tecnico";
                if ($("#id_status").val() == $("#id_status_actual").val()) {
                    ErrorCustom("El estatus que desea cambiar debe ser diferente al que está actualmente");
                } else {
                    if ($("#id_status_actual").val() == 1 && ($("#id_status").val() != 2 && $("#id_status").val() !=
                        4) && $("#id_status").val() != 5) {
                        ErrorCustom("Solamente puedes pasar un trabajo pendiente a trabajando o retrasado");
                    } else {
                        //NUEVA VERSION
                        var motivo = $("#motivo_detencion").val();
                        var estatus_detenido = $("#estatus_detenido").val();

                        ajaxJson(url, {
                            "id_status": $("#id_status").val(),
                            "comentarios": $("#comentarios").val(),
                            "id_cita_save": $("#id_cita_save").val(),
                            "usuario_ingreso": usuario_ingreso,
                            "password_ingreso": password_ingreso,
                            "islavado": $("#islavado").val(),
                            "tipo": $("#tipo").val(),
                            "ubicacion": $("#ubicacion_unidad").val(),
                            "tabla_operacion": tabla_operacion,
                            "id_operacion": id_operacion,
                            "motivo_detencion": motivo,
                            "estatus_detenido": estatus_detenido,
                            "id_status_actual": $("#id_status_actual").val(),
                        }, "POST", "async", function(result) {
                            if (isNaN(result)) {
                                data = JSON.parse(result);
                                //Se recorre el json y se coloca el error en la div correspondiente
                                $.each(data, function(i, item) {
                                    $.each(data, function(i, item) {
                                        $(".error_" + i).empty();
                                        $(".error_" + i).append(item);
                                        $(".error_" + i).css("color", "red");
                                    });
                                });
                            } else {
                                if (result == 0) {
                                    ErrorCustom('Error al cambiar el estatus, por favor intenta de nuevo.');
                                } else if (result == -2) {
                                    ErrorCustom(
                                        'Es necesario que el asesor determine que el cliente no llegó antes de cambiar el estatus.'
                                        );
                                } else if (result == -3) {
                                    ErrorCustom(
                                        'La unidad no puede ser cambiada al estatus de terminado por que no existe la firma del técnico en la hoja multipunto'
                                        );
                                } else if (result == -4) {
                                    ErrorCustom('Ya fue asignado otro trabajo');
                                } else if (result == -5) {
                                    ErrorCustom(
                                        'La unidad no puede ser cambiada al estatus de terminado por qué no ha pasado por el estatus de trabajando'
                                        );
                                } else if (result == -6) {
                                    ErrorCustom(
                                        'La unidad no puede ser cambiada al estatus de terminado por qué existen operaciones extras autorizadas por el cliente sin asignar a la orden'
                                        );
                                } else if (result == -7) {
                                    ErrorCustom(
                                        'La unidad no puede ser cambiada al estatus de trabajando o retrasado por que no existe un inventario en recepción'
                                        );
                                } else if (result == -8) {
                                    ErrorCustom(
                                        'Es necesario que el asesor asigne todas las operaciones para poder realizar un cambio de estatus'
                                        );
                                } else if (result == -9) {
                                    ErrorCustom(
                                        'La unidad no puede ser cambiada al estatus de terminado por qué existe una cotización pendiente'
                                        );
                                } else {
                                    ExitoCustom("Estatus cambiado con éxito", function() {
                                        $(".close").trigger("click");
                                        buscar();
                                    });
                                }
                            }
                        });
                    }
                }
            }
            $("body").on('click', ".ver_comentarios", function(e) {
                e.preventDefault();
                var idcita = $("#id_cita_save").val();
                if (idcita == '' || idcita == undefined) {
                    var idcita = $(this).data('cita');
                }
                var url = site_url + "/citas/ver_comentarios_citas";
                customModal(url, {
                    "id_cita": idcita
                }, "POST", "md", "", "", "", "Cerrar", "Lista de comentarios", "modal4");
            });
            //Ver cotizaciones
            $("body").on('click', "#ver_cotizaciones", function(e) {
                e.preventDefault();
                var idcita = $(this).data('idcita');
                var url = site_url + "/citas/ver_cotizaciones";
                customModal(url, {
                    "id_cita": idcita
                }, "POST", "md", "", "", "", "Cerrar", "Cotizaciones aceptadas", "modal4");
            });
            $("body").on('click','.asignar_cita_tablero',function(){
                 time = $(this).data('time');
                id = $(this).prop('id');
                idtecnico = $(this).data('id_tecnico');
                var url = site_url + "/citas/tabla_horarios_tecnicos_busqueda";
                ajaxLoad(url, {
                    "fecha": $("#fecha").val(),
                    "origen": 'tecnicos'
                }, "div_tabla", "POST", function() {
                    updateTimes();
                    $(".main-table").clone(true).appendTo('#div_tabla').addClass('clone');
                    if($("#"+id).hasClass('asignar_cita_tablero')){
                        customModal(site_url + "/citas/login_editar_cita/0", {}, "GET", "md", ValidarLogin, "", "Ingresar", "Cancelar", "Login", "modal8");
                    }else{
                        ErrorCustom('Ya fue ocupado el horario');
                    }
                });
            });
            function LlenarCita() {
                var url = site_url + "/citas/agendar_cita_tablero";
                customModal(url, {
                    "id_tecnico": idtecnico,
                    "fecha": $("#fecha").val(),
                    "time": time
                }, "POST", "lg", callbackGuardarCitaTablero, "", "Guardar", "Cerrar", "Asignar cita", "modal7");
            }

            function callbackGuardarCitaTablero() {
                if ($("#iscarriover").val() != '') {

                    var cantidad_radios_seleccionados = $("input:radio:checked").length;
                    if (cantidad_radios_seleccionados == 0 && !$("#carryover_reservacion").prop('checked')) {
                        ErrorCustom("Es necesario seleccionar una operación");
                    } else {
                        //Saber si es carriover
                        if ($("#iscarriover").val() == 1) {
                            var url = site_url + '/citas/modal_asignar_tecnico';

                        } else {
                            var url = site_url + '/citas/agendar_cita';
                        }
                        if (verificarFormatoHoras()) {
                            if (validarHorasIguales()) {
                                ajaxJson(url, $("#frm_cita_tablero").serialize(), "POST", "async", function(result) {
                                    if (isNaN(result)) {
                                        data = JSON.parse(result);
                                        //Se recorre el json y se coloca el error en la div correspondiente
                                        $.each(data, function(i, item) {
                                            $(".error_" + i).empty();
                                            $(".error_" + i).append(item);
                                            $(".error_" + i).css("color", "red");
                                        });
                                    } else {
                                        if (result == 1) {
                                            ExitoCustom("Guardado correctamente", function() {
                                                $(".close").trigger("click");
                                                var url = site_url +
                                                    "/citas/tabla_horarios_tecnicos_busqueda";
                                                ajaxLoad(url, {
                                                    "fecha": $("#fecha").val(),
                                                    "origen": 'tecnicos'
                                                }, "div_tabla", "POST", function() {});


                                            });
                                        } else if (result == -1) {
                                            ErrorCustom(
                                            'El horario ya fue ocupado, por favor intenta con otro');
                                        } else if (result == -2) {
                                            ErrorCustom(
                                                'La fecha de asignación al técnico debe ser mayor o igual a la fecha del asesor'
                                                );
                                        } else if (result == -3) {
                                            ErrorCustom('El técnico no labora en la hora seleccionada');
                                        } else if (result == -4) {
                                            ErrorCustom(
                                                'No se puede hacer el carryover, por qué la unidad actualmente está en un estatus no permitido para esta acción'
                                                );
                                        } else {
                                            ErrorCustom('No se pudo guardar, intenta otra vez.');
                                        }
                                    }
                                });
                            } else {
                                ErrorCustom("La hora de inicio de trabajo y hora fin NO pueden ser iguales");
                            }
                        } else {
                            ErrorCustom("Es necesario revises el formato de las horas");
                        }
                    }
                } else {
                    $(".error_carriover").empty().append("Es necesario ingresar el campo")
                    $(".error_carriover").css("color", "red");
                }
            }

            function ValidarLogin() {
                var url = site_url + "/citas/login_tecnicos";
                //var url =site_url+"/citas/login_editar_cita";
                ajaxJson(url, {
                    "usuario": $("#usuario").val(),
                    "password": $("#password").val(),
                    "origen": 'taller'
                }, "POST", "", function(result) {
                    if (isNaN(result)) {
                        data = JSON.parse(result);
                        //Se recorre el json y se coloca el error en la div correspondiente
                        $.each(data, function(i, item) {
                            $.each(data, function(i, item) {
                                $(".error_" + i).empty();
                                $(".error_" + i).append(item);
                                $(".error_" + i).css("color", "red");
                            });
                        });
                    } else {
                        if (result < 0) {
                            ErrorCustom('Usuario o contraseña inválidos.');
                        } else if (result == 0) {
                            ErrorCustom('El usuario no es administrador');
                        } else {
                            LlenarCita();


                        }
                    }
                });
            }
            $("body").on('change', '#vehiculo_placas', function() {
                var url = site_url + "/citas/getDatosByPlaca";
                ajaxJson(url, {
                    "placas": $(this).val()
                }, "POST", "", function(result) {
                    result = JSON.parse(result);
                    if (result.exito) {
                        $("#email").val(result.data.datos_email);
                        $("#datos_nombres").val(result.data.datos_nombres);
                        $("#datos_apellido_paterno").val(result.data.datos_apellido_paterno);
                        $("#datos_apellido_materno").val(result.data.datos_apellido_materno);
                        $("#datos_telefono").val(result.data.datos_telefono);
                        $("#vehiculo_anio option[value='" + result.data.vehiculo_anio + "']").attr(
                            "selected", true);
                        $("#vehiculo_modelo option[value='" + result.data.vehiculo_modelo + "']").attr(
                            "selected", true);
                        $("#id_color option[value='" + result.data.id_color + "']").attr("selected",
                            true);

                    } else {
                        $("#email").val('');
                        $("#datos_nombres").val('');
                        $("#datos_apellido_paterno").val('');
                        $("#datos_apellido_materno").val('');
                        $("#datos_telefono").val('');
                    }
                });
            });

            //Carriover
            $("body").on('click', '#btn-co', function() {
                var url = site_url + '/citas/getDatosCita';
                ajaxJson(url, {
                    "id_cita": $("#carriover").val()
                }, "POST", "", function(result) {
                    result = JSON.parse(result);

                    if (result.exito) {
                        $("#id").val(result.cita.id_cita);
                        $("#iscarriover").val(1);

                        $("#dia_completo").val(result.dia_completo);
                        $.each(result.cita, function(i, item) {
                            if (i !=
                                'fecha') { //Si no actualiza la fecha para cambiar el calendar
                                $("#" + i).val(item);
                                if (i != 'comentarios_servicio') {
                                    $("#" + i).prop('disabled', true);
                                }
                            }
                        });
                        //NUEVA VERSION
                        var html = `<table class="table">
   <thead>
   <tr>
   <th>Operación</th>
   <th>Acción</th>
   </tr>
   </thead>
   <tbody>`

                        $.each(result.operaciones, function(i, item) {
                            html +=
                                `<tr id="tr_operacion_${item.id}">
   <td>
   <input id="${item.id}" data-idop="${item.id}" style="margin-right:10px;" value="${item.id}" type="radio" name="operacion" class="js_validar_operacion"><label for="${item.id}">${item.descripcion}</label> `;


                            html += `<td>`;
                            if (item.principal == 0 && item.id_tecnico == null) {

                                html += ` <a href="#" class="js_no_aplica" data-idop="${item.id}">
    <i class="pe pe-7s-close"></i>
    </a>`;
                            }
                            html += '</td></tr>';

                        });
                        html += `</tbody>
   </table>`;

                        $("#div_operaciones").empty().append(html).css('color', 'black');
                    } else {
                        ErrorCustom(result.mensaje, function() {
                            $("#id").val(0);
                            //$("#iscarriover").val(0);
                            $("#carriover").val('');
                            $("#div_operaciones").empty().append(
                                '** Las operaciones ya fueron terminadas').css('color',
                                'red');
                            //NUEVA VERSIÓN
                            $("#hora_fin").val('');
                            $("#fecha_fin").val('');


                        });
                    }
                });

            });
            $("body").on('click', '.js_no_aplica', function(e) {
                e.preventDefault();
                idop = $(this).data('idop');
                ConfirmCustom("¿Está seguro que esta operación va incluída dentro de la operación principal?",
                    NoAplicaOperacion, "", "Confirmar", "Cancelar");
            });

            function NoAplicaOperacion() {
                var url = site_url + '/layout/operacion_no_aplica';
                ajaxJson(url, {
                    "idop": idop
                }, "POST", "", function(result) {
                    if (result == 1) {
                        $("#tr_operacion_" + idop).remove();
                    } else if (result == -1) {
                        ErrorCustom(
                            "Esta acción no se puede realizar por que la operación ya fue asignada a un técnico"
                            );
                    } else {
                        ErrorCustom("Error, por favor intenta otra vez");
                    }
                });
            }
            //NUEVA VERSION
            $("body").on('click', '.js_validar_operacion', function() {
                var url = site_url + '/layout/validarCarryover';
                ajaxJson(url, {
                    "idop": $(this).data('idop')
                }, "POST", "", function(result) {
                    if (result == 1) {
                        $("#validar_operaciones").empty().append(
                            '** Ya fue asignada la operación, se tomará como un carryover').css(
                            'color', 'red');
                    } else if (result == 2) {
                        var opciones = document.getElementsByName("operacion");
                        $(opciones).prop('checked', false);

                        $("#hora_fin").val('');
                        $("#fecha_fin").val('');
                        ErrorCustom(
                            "Ya no se puede realizar carryover de esta operación por que ya fue terminada"
                            );
                    } else {
                        $("#validar_operaciones").empty();
                    }
                });
            })
            //Fin de carriover
            //100000 1 min
            $(".main-table").clone(true).appendTo('#div_tabla').addClass('clone');
            //ELIMINAR CARRYOVER
            $("body").on('click', '.js_eliminar_co', function(e) {
                e.preventDefault();
                var url = site_url + "/citas/login_carryover/";
                idcarryover = $(this).data('idcarryover');
                id_operacion = $(this).data('id_operacion');
                id_cita = $(this).data('idcita');

                customModal(url, {
                    "idcarryover": idcarryover
                }, "GET", "lg", EliminarCarryover, "", "Aceptar", "Cancelar", "Ingreso", "modal1");
            });

            function EliminarCarryover() {
                var url = site_url + "/citas/login_carryover";
                usuario_ingreso = $("#usuario_carryover").val();
                password_ingreso = $("#password_carryover").val();
                ajaxJson(url, {
                    "usuario": $("#usuario_carryover").val(),
                    "password": $("#password_carryover").val(),
                    "idcarryover": idcarryover,
                    "id_cita": id_cita,
                    "id_operacion": id_operacion
                }, "POST", "", function(result) {
                    if (isNaN(result)) {
                        data = JSON.parse(result);
                        //Se recorre el json y se coloca el error en la div correspondiente
                        $.each(data, function(i, item) {
                            $.each(data, function(i, item) {
                                $(".error_" + i).empty();
                                $(".error_" + i).append(item);
                                $(".error_" + i).css("color", "red");
                            });
                        });
                    } else {
                        if (result == 0) {
                            ErrorCustom('Usuario o contraseña inválidos.');
                        } else if (result == 1) {
                            $(".close").trigger("click");
                            ExitoCustom("registro eliminado correctamente", function() {
                                buscar();
                            })

                        } else {
                            ErrorCustom("No se pudo eliminar el carry over (estatus no permitido)")
                        }
                    }
                });
            }

            function validarHorasIguales() {
                if (!$("#dia_completo").prop('checked')) {
                    if ($("#hora_inicio").val() == $("#hora_fin").val() && $("#hora_inicio").val() != '' && $(
                            "#hora_fin").val() != '') {
                        return false;
                    }
                }
                return true;
            }

            function verificarFormatoHoras() {
                var contador = true;
                if ($("#dia_completo").prop('checked')) {
                    $("#hora_inicio").removeClass('check_hora');
                    $("#hora_fin").removeClass('check_hora');
                    $("#hora_comienzo").addClass('check_hora');
                    $("#hora_fin_extra").addClass('check_hora');
                } else {
                    $("#hora_inicio").addClass('check_hora');
                    $("#hora_fin").addClass('check_hora');
                    $("#hora_comienzo").removeClass('check_hora');
                    $("#hora_fin_extra").removeClass('check_hora');
                }
                $.each($(".check_hora"), function(key, value) {
                    var valor = '';
                    valor = $(value).val();
                    var id = $(value).attr('id');
                    if (valor != '' && valor != 'undefined') {
                        var array_valor = valor.split(':');
                        valor = array_valor[1];
                        if (valor == 00 || valor == 20 || valor == 40) {
                            $(".error_" + id).empty();
                        } else {
                            $(".error_" + id).empty();
                            $(".error_" + id).append('El formato de la hora debe ser lapso de 30 minutos');
                            $(".error_" + id).css("color", "red");
                            contador = false;
                        }
                    }
                });
                return contador;
            }

        </script>
    @endif

    <script>
        function updateTimes() {
            //NUEVA VERSIÓN
            var id_cita_temp = 0;
            $('.triangle').each(function(i, obj) {
                var id_cita_actual = $(obj).data('idcita');
                var id_operacion_actual = $(obj).data('idoperacion');
                var id_status_cita = $(obj).data('idstatuscita');
                var tiempo_por_restar = 15;
                var tiempo_total_cita = 0;
                //esta función solo aplica para planeado, trabajando, terminado

                var last = $(".triangle_" + id_cita_actual + "_" + id_operacion_actual).last();
                var cantidad_triangulos = ($(".triangle_" + id_cita_actual + "_" + id_operacion_actual)
                    .length) / 2;
                    
                //Saco el 70% del tiempo
                var tiempo_total_cita = cantidad_triangulos * 20;
                tiempo_por_restar = tiempo_total_cita - ((70 * (tiempo_total_cita)) / 100);
                var time = $(last).data('time');
                //var arr_time = time.split(':');
                var resta_tiempo = moment.utc(time, 'hh:mm').subtract(tiempo_por_restar, 'minutes').format(
                    'HH:mm');
                array_triangle.push({
                    "id": $(last).data('idcita'),
                    "time": time,
                    'resta_tiempo': resta_tiempo,
                    "idstatus": $(last).data("idstatuscita"),
                    "id_operacion": $(last).data("idoperacion")
                });
                

            });

            var fecha = $("#fecha").val();
            var array_fecha = fecha.split('-');
            
            //var fecha_actual = new Date().getTime();
            var fecha_actual = "{{ date('Y-m-d H:i') }}";
            for (i = 0; i < array_triangle.length; ++i) {
                var status_cita = array_triangle[i].idstatus;
                var fecha_comparar = array_fecha[0] + '-' + array_fecha[1] + '-' + array_fecha[2] + ' ' +
                    array_triangle[i].time;
                var fecha_tiempo_restado = array_fecha[0] + '-' + array_fecha[1] + '-' + array_fecha[2] + ' ' +
                    array_triangle[i].resta_tiempo;

                //ESTATUS 3 = TERMINADO, 4 RETRASADO
                //PREGUNTO SI LA FECHA ACTUAL ES MAYOR A LA HORA RESTANDO 15 MIN Y MENOR A LA FECHA DEL CUADRO 
                //SI ES ASI ES AMARILLO, SI NO PUEDE SER ROJO SI ES QUE LA FECHA ACTUAL YA ES MAYOR A LA DEL CUADRITO
                if (status_cita == 3 || status_cita == 8 || status_cita == 9 || status_cita == 10 || status_cita ==
                    11) {
                    $(".triangle_" + array_triangle[i].id + "_" + array_triangle[i].id_operacion).css('border-top',
                        '15px solid lightblue');
                } else if (status_cita == 15 || status_cita == 16 || status_cita == 17 || status_cita == 18 ||
                    status_cita == 19) {
                    $(".triangle_" + array_triangle[i].id + "_" + array_triangle[i].id_operacion).css('border-top',
                        '15px solid orange');
                } else {
                    if (status_cita == 4) {
                        $(".triangle_" + array_triangle[i].id + "_" + array_triangle[i].id_operacion).css('border-top',
                            '15px solid red');
                    } else if (status_cita == 3) {
                        $(".triangle_" + array_triangle[i].id + "_" + array_triangle[i].id_operacion).css('border-top',
                            '15px solid lightblue');
                    } else if (fecha_actual > fecha_tiempo_restado && fecha_actual < fecha_comparar) {
                        $(".triangle_" + array_triangle[i].id + "_" + array_triangle[i].id_operacion).css('border-top',
                            '15px solid yellow');
                    } else {
                        if (fecha_comparar < fecha_actual) {
                            $(".triangle_" + array_triangle[i].id + "_" + array_triangle[i].id_operacion).css(
                                'border-top', '15px solid red');

                        }
                    }
                }


            }
        }
        setInterval(updateTimes, 500000);
        updateTimes();
        $("body").on('click','#buscar_cita',function(){
	  		var cita_id = $("#buscar_id").val();
			customModal(site_url+"/citas/buscar_citas_id/",{"id_cita":cita_id},"GET","md","","","Guardar","Cerrar","Búsqueda de citas","modalBusqueda");
	  	});
		$("body").on('click','.js_select_cita',function(e){
			e.preventDefault();
            var array_fecha = $(this).data('fecha').split('/');
			var fecha = array_fecha[2]+'-'+array_fecha[1]+'-'+array_fecha[0];
			$('#datetimepicker1').datetimepicker('destroy');
			$("#fecha").val(fecha);
			$("#buscar").trigger('click');
			$('#datetimepicker1').datetimepicker({
				//minDate: moment(),
				format: 'DD/MM/YYYY',
				icons: {
					time: "fa fa-clock-o",
					date: "fa fa-calendar",
					up: "fa fa-arrow-up",
					down: "fa fa-arrow-down"
				},
					locale: 'es'
			});
			$(".close").trigger('click');
		});
        $("body").on('click', '.js_esconder', () => {
            $('[data-toggle="tooltip"]').tooltip('dispose');
            const mostrar = $(".js_esconder").data('mostrar')
            if (!mostrar) {
                $("#all").addClass('esconder_div');
                $("#all").removeClass('mostrar_div');
                $("#arrow").empty().append(
                    '<span class="pull-right">click para mostrar/ocultar</span><i data-toggle="tooltip" data-placement="top" title="Mostrar" style="font-size: 40px" class="fa fa-chevron-up pull-right js_esconder" aria-hidden="true" data-mostrar="1"></i>'
                    );
            } else {
                $("#all").removeClass('esconder_div');
                $("#all").addClass('mostrar_div');
                $("#arrow").empty().append(
                    '<span class="pull-right">click para mostrar/ocultar</span><i data-toggle="tooltip" data-placement="top" title="Esconder" style="font-size: 40px" class="fa fa-chevron-down pull-right js_esconder" aria-hidden="true" data-mostrar="0"></i>'
                    )

            }
            $('[data-toggle="tooltip"]').tooltip();
        })
        //setInterval(buscar,10000);
    </script>
</body>

</html>
