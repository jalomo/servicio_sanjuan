<form action="" method="POST" id="frm">
	<input type="hidden" name="id" id="id" value="{{$input_id}}">
	<div class="row form-group">
		<div class="col-sm-12">
			<label>Nombre del operador</label>
			{{$input_nombre}}
			<span class="error error_nombre"></span>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-12">
			<label>Correo electrónico</label>
			{{$input_correo}}
			<span class="error error_correo"></span>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-12">
			<label>Teléfono</label>
			{{$input_telefono}}
			<span class="error error_telefono"></span>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-6">
			<label>R.F.C</label>
			{{$input_rfc}}
			<span class="error error_rfc"></span>
		</div>
		<div class="col-sm-6">
			<label>Horario de atención</label>
			{{$input_horario}}
			<span class="error error_horario"></span>
		</div>
	</div>
</form>