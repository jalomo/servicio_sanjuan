@layout('tema_luna/layout')
@section('contenido')

<h1>Menú de Parámetros</h1>
<div class="row">
	<div class="col-sm-3 col-sm-offset-1">
		<a href="{{base_url('citas/parametros')}}">Lapso entre citas</a><br>
		 <a href="{{base_url('citas/parametros_horarios_tecnicos')}}">Agregar Horarios técnicos</a><br>
		 <a href="{{base_url('citas/cambiar_horarios')}}">Actualizar Horarios técnicos</a>
	</div>
</div>
@endsection
@section('scripts')