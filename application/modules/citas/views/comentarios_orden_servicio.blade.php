<form action="" id="frm_comentarios">
	<input type="hidden" name="idorden" id="idorden" value="{{$idorden}}">
	<div class="row">
		<div class="col-sm-12">
			<label>Comentario</label>
			{{$input_comentario}}
			<span class="error error_comentario"></span>
		</div>
	</div>
</form>
<script>
	$('.clockpicker').clockpicker();
	var fecha_actual = "<?php echo date('Y-m-d') ?>";
	$('.date').datetimepicker({
			minDate: fecha_actual,
            format: 'DD/MM/YYYY',
            icons: {
                time: "far fa-clock",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            locale: 'es'
        });
</script>