@layout('tema_luna/layout')
@section('contenido')
	<h2>Envío de información a FORD actual / Unidades en proceso</h2>
	<br>
	<form id="frm" method="POST">
		<div class="row">
			<div class='col-md-3'>
				<label for="">Fecha inicio</label>
				<input class="form-control" type="date" name="finicio" id="finicio" value="">
				<span style="color: red" class="error error_fini"></span>
			</div>
			<div class='col-md-3'>
				<label for="">Fecha Fin</label>
				<input class="form-control" type="date" name="ffin" id="ffin" value="">
				<span style="color: red" class="error error_ffin"></span>
			</div>
			<div class="col-sm-6" style="margin-top: 33px">
				<button type="button" id="buscar" class="btn btn-success">Buscar</button>
				 <button  type="button" id="exportarExcel" data-action="citas/exportarUnidadesTaller" class="btn btn-info js_exportar pull-right"><i style="color: white;" class="fa fa-file-excel-o"></i>Exportar EXCEL</button>
			</div>
		</div>
	</form>
	<br>
	<div class="row">
	 	<div class="col-sm-4">
	 		<div style="margin-top: 10px" class="alert alert-info alert-dismissible fade show" role="alert">
		        Fecha de actualización : <strong>{{date_eng2esp_1(CONST_FECHA_CITA_NEW_UPDT)}}</strong>.
		        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	 	</div>
	</div>
	<div class="row">
		<div id="div_tabla">
			<div class="col-sm-12">
				<table id="tbl_unidades_taller" class="table table-hover table-striped">
					<thead>
						<tr class="tr_principal">
							<th>Cita</th>
							<th>Nombre del cliente</th>
							<th>Teléfono del cliente</th>
							<th>Placas</th>
							<th>Modelo</th>
							<th>Fecha recepción orden</th>
							<th>Cita previa</th>
							<th>Asesor</th>
							<th>Fecha asesor</th>
						</tr>
					</thead>
					<tbody>
						@foreach($unidades as $c => $value)
						<tr>
							<td>{{$value->id_cita}}</td>
							<td>{{$value->datos_nombres.' '.$value->datos_apellido_paterno.' '.$value->datos_apellido_materno}}</td>
							<td>{{$value->datos_telefono}}</td>
							<td>{{$value->vehiculo_placas}}</td>
							<td><span title="{{$value->vehiculo_modelo}}">{{trim_text($value->vehiculo_modelo,1)}}</span></td>
							<td>{{$value->fecha_recepcion}}</td>
							<td>{{($value->cita_previa==1)?'Si':'No'}}</td>
							<td>{{$value->asesor}}</td>
							<td>{{$value->fecha_asesor.' '.$value->hora_asesor}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
@section('scripts')

<script>
	var site_url = "{{site_url()}}";
	inicializar_tabla("#tbl_unidades_taller");
	$('.date').datetimepicker({
		format: 'DD/MM/YYYY',
		minDate : "{{CONST_FECHA_CITA_NEW_UPDT}}",
		icons: {
			time: "fa fa-clock-o",
			date: "fa fa-calendar",
			up: "fa fa-arrow-up",
			down: "fa fa-arrow-down"
		},
		locale: 'es'
	});
	$("body").on('click','#buscar',buscar);
	function buscar(e){
		e.preventDefault();
		var finicio = $("#finicio").val();
		var ffin = $("#ffin").val();
		if(finicio!=''&&ffin!=''){
			ajaxLoad(site_url+"/citas/buscar_unidades_taller/",$("#frm").serialize(),"div_tabla","POST",function(){
				inicializar_tabla("#tbl_unidades_taller");
			}); 
		}else{
			ErrorCustom('Es necesario seleccionar ambas fechas');
		}
	}
	$(".js_exportar").on('click',function(){
		var finicio = $("#finicio").val();
		var ffin = $("#ffin").val();
		if((finicio!=''&&ffin=='')||(finicio==''&&ffin!='')){
			ErrorCustom('Es necesario seleccionar ambas fechas');
		}else{
	        var action = $(this).data('action');
	        $('#frm').attr('action',action);
	        $('#frm').submit();
	    }
    });
</script>
@endsection