<table class="table table-responsive table-bordered table-hover table-striped table-responsive-sm table-responsive-md table-responsive-lg">
	<thead>
		<tr>

			<th width="5%">Asesor</th>
			<th width="10%">Placas</th>
			<th width="10%"># Orden</th>
			<th  width="10%">Vehículo</th>
			<!--<th>EXPRESS</th>-->
			<!-- <th>Refacciones</th> -->
			<th width="5%">Fecha <br> Recepción</th>
			<th width="20%">Avance</th>
			<th width="10%">Fecha promesa</th>
			<th width="10%">Estatus</th>
			<th width="10%">Lavado</th>
		</tr>
	</thead>
	<tbody>
		@if(count($citas)>0)
			@foreach($citas as $c => $cita)
			<tr>
				<td width="5%">{{$cita->asesor}}</td>
				<td width="10%">{{$cita->vehiculo_placas}}</td>
				<td width="10%">{{$cita->id_cita}}</td>
				<td width="10%">{{$cita->vehiculo_modelo}}</td>
				<!--<td></td>-->
				<!--<td class="text-center">
					@if($cita->id_status==7)
						<i class="fa fa-check-square"></i>
					@else
						<i class="pe-7s-check"></i>
					@endif

				</td>-->
				<td width="5%">{{date_eng2esp_1($cita->fecha)}}</td>
				<td width="20%">
					<?php $porcentaje = 0; ?>
					@if($cita->id_status_asesor==0 && $cita->origen==1)
						<?php $porcentaje = 0; ?>
					@else
						<?php $porcentaje = $cita->porcentaje; ?>
					@endif
					<div class="progress">
					  <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: {{$porcentaje}}%;height: 40px;" aria-valuenow="{{$porcentaje}}" aria-valuemin="0" aria-valuemax="100">{{$porcentaje}}%</div>
					</div>
				</td>
				<td width="10%">{{date_eng2esp_1($cita->fecha_entrega).' '.$cita->hora_entrega}}</td>
				<td width="10%">{{$cita->estatus_tecnico}}</td>
				<td width="10%" class="text-center">
					@if($cita->id_status==3 || $cita->id_status==8 || $cita->id_status==10 || $cita->id_status==11 || $cita->id_status==9 )
						@if($cita->status_lavado==0)
						<div class="alert alert-danger" role="alert" style="background: red; border-color: red; color:white !important">
						  <strong style="font-size: 18px !important;">Planeado</strong>
						</div>
						
						@elseif($cita->status_lavado==1	)
						<div class="alert alert-danger" role="alert" style="background: orange; border-color: orange; color:white !important">
						  <strong style="font-size: 18px !important;">Lavando</strong>
						</div>
						@else
							<div class="alert alert-danger" role="alert" style="background: green; border-color: green; color:white !important">
							  <strong style="font-size: 18px !important;">Terminado</strong>
							</div>
						@endif
					@endif

				</td>
			</tr>
			@endforeach
		@else
		<tr class="text-center">
			<td colspan="10">No hay registros para mostrar...</td>
		</tr>
		@endif
	</tbody>
</table>