<div id="tabla_precios">
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th>Descripción</th>
				<th>Precio</th>
				<th>Cantidad</th>
				<th>Total</th>
			</tr>
		</thead>
		<tbody>
			<?php $total = 0 ?>
			@if(count($precios_servicio)>0)
				@foreach($precios_servicio as $p => $precios)
				<tr>
					<td>{{$precios->descripcion}}</td>
					<td>${{$precios->precio_unitario}}</td>
					<td>{{$precios->cantidad}}</td>
					<td>${{number_format($precios->total,2)}}</td>
					<?php $total = $total + (float)$precios->total; ?>
				</tr>
				@endforeach
			@else
				<tr class="text-center">
					<td colspan="4">No hay registros para mostrar...</td>
				</tr>
			@endif
		</tbody>
		<tfoot>
			<tr class="text-right">
				<td colspan="5">
					Total: ${{number_format($total,2)}}
				</td>
			</tr>
		</tfoot>
	</table>
</div>