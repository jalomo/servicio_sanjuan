<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<table>
		<tbody>
			<tr>
				<td><img style="display: block; margin-right: auto;" tabindex="0" src="{{CONST_LOGO}}" alt="" width="400" height="120" /></td>
			</tr>
			<tr>
				<td>
					<p>Estimado, <strong>$$usuario$$</strong .</p>
					<p>Hemos recibido satisfactoriamente su solicitud como <strong>$$ProcessName$$</strong>, con número de folio de internet <strong>$$Nomenclatura$$</strong> .</p>
					<p>Para revisar el proceso favor de <a href="[[baseUrl]]">iniciar sesi&oacute;n aqu&iacute;</a></p>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>