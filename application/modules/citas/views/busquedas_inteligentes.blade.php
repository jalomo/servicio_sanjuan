@layout('tema_luna/layout')
@section('contenido')
<style>
	#tabla {
    width: 100%;
    overflow-x: auto;
    white-space: nowrap;
	}
	.multiple {
        width: 100% !important;
    }
</style>
	<ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="#">Inicio</a>
	    </li>
	    <li class="breadcrumb-item active">Técnicos</li>
  	</ol>
	<div class="row">
		<div class="col-sm-12">
			<h1>Búsquedas Inteligentes</h1>
		</div>
	</div>
	
	<form action="" method="POST" id="frm">
		<input type="hidden" name="tipo" id="tipo" value="{{$tipo}}">
		<div class="row">
			<div class="col-sm-3 form-group">
				<label>Técnico</label><br>
				{{$drop_tecnicos}}
			</div>
			<div class="col-sm-3 form-group">
				<label>Servicios</label><br>
				{{$drop_servicios}}
			</div>
			<div class="col-sm-3 form-group">
				<label>Estatus cita</label><br>
				{{$drop_status}}
			</div>
			<div class="col-sm-3 form-group">
				<label>Asesor</label><br>
				{{$drop_asesores}}
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3 form-group">
				<label>Estatus técnico</label><br>
				{{$drop_status_tecnico}}
			</div>
			<div class="col-sm-3 form-group">
				<label>Confirmada</label><br>
				{{$drop_tipo}}
			</div>
			<div class="col-sm-3 form-group">
				<label>Cancelada</label><br>
				{{$drop_tipo_canceladas}}
			</div>
			<div class="col-sm-3 form-group">
				<label>Con cita previa</label><br>
				{{$drop_tipo_cita}}
			</div>
		</div>
		<div class="row">
			<div class='col-sm-4 form-group'>
	        	<label for="">Fecha Inicio</label>
    			<input id="finicio" name="finicio" type='date' class="form-control" value="" />
             	<span class="error_fecha"></span>
	        </div>
	        <div class='col-sm-4 form-group'>
	        	<label for="">Fecha Fin</label>
	        	<input id="ffin" name="ffin" type='date' class="form-control" value="" />
	         	<span class="error_fecha"></span>
	        </div>
		</div>
		<div class="row">
			
		</div>
		<div class="row">
			<div class="col-sm-12 text-right form-group">
				<button id="buscar" class="btn btn-success">Buscar</button>
				<!--<button id="excel" class="btn btn-info">Exportar a excel</button>-->
			</div>
		</div>
	</form>
		
		
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div id="tabla">
		    	 {{$tabla}}
		   	</div>
		</div>
	</div>
	<br>
	<br>
	<br>
@endsection
@section('scripts')
<script>
	$(document).ready(function(){
		var site_url = "{{site_url()}}";
		$('.date').datetimepicker({
        	//minDate: moment(),
        	format: 'DD/MM/YYYY',
        	icons: {
                time: "far fa-clock",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
        	},
        	 locale: 'es'
        });
        $('#datetimepicker1').datetimepicker({
        	format: 'DD/MM/YYYY',
        	locale: 'es'
        });
        $('#datetimepicker2').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            format: 'DD/MM/YYYY',
        	locale: 'es'
        });
        // $("#datetimepicker1").on("dp.change", function (e) {
        //     $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
        // });
        // $("#datetimepicker2").on("dp.change", function (e) {
        //     $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
        // });

        $('.multiple').multiselect();
        $('body').on('click','.busquedalink',function(e){
			e.preventDefault();			
			url=$(this).prop('href');
			ajaxLoad(url,$("#frm").serialize(),"tabla","POST",function(){

			});
		});
		$("#buscar").on("click",function(e){
			e.preventDefault();
			url="<?php echo site_url('citas/buscar_citas') ?>";
			ajaxLoad(url,$("#frm").serialize(),"tabla","POST",function(){

			});
		});
		$("#excel").on("click",function(e){
			  window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#tbl').html()));
        e.preventDefault();

		});
		$("body").on('click',".historial_comentarios",function(e){
			e.preventDefault();
			var url =site_url+"/citas/ver_comentarios_citas";
			customModal(url,{"id_cita":$(this).data('id_cita')},"POST","md","","","","Cerrar","Lista de comentarios","modal4");
		});
		
	});
</script>
@endsection