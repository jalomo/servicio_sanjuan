@layout('tema_luna/layout')

@section('contenido')
        <!-- Page Title -->
        <div class="page-title">
            <div class="title_left">
                <h3>Lista de Unidades</h3>
            </div>
        </div>
        <!-- /Page Title -->

        <div class="clearfix"></div>

        <!-- Table -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                    <div class="x_content">
                        <br />
                        <table id="tbl_unidades" class="table table-hover">
                            <thead>
                                <tr class="verde">
                                    <th>QR</th>
                                    <th>#</th>
                                    <th>Fecha</th>
                                    <th>VIN</th>
                                    <th>Serie Corta</th>
                                    <th>Categoría</th>
                                    <th>Subcategoría</th>
                                    <th>Color</th>
                                    <th>Año</th>
                                    <th>Kilometraje</th>
                                    <!--<th>#Económico</th>-->
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($unidades as $u => $value)
                                <tr>
                                    <th>
                                        <img id="imgqr" style="width: 100px;" src="{{$value->qr}}" alt="">
                                    </th>
                                    <th>{{$value->id}}</th>
                                    <th>{{date_eng2esp_1($value->fecha)}}</th>
                                    <th>{{$value->vin}}</th>
                                    <th>{{$value->seriecorta}}</th>
                                    <th>{{$value->categoria}}</th>
                                    <th>{{$value->subcategoria}}</th>
                                    <th>{{$value->color}}</th>
                                    <th>{{$value->year}}</th>
                                    <th>{{$value->kilometraje}}</th>
                                    <!--<th>{{$value->noeconomico}}</th>-->
                                    <th>
                                        <a href="{{base_url('citas/guardarUnidad/'.$value->id)}}"><span class="pe pe-7s-note"></span></a>
                                        <a href="{{base_url('citas/generarPdfUnidad/'.$value->id)}}" ><span class="pe pe-7s-file"></span></a>
                                        
                                    </th>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Table -->
@endsection
@section('scripts')
<script>

    //ConstruirTabla("tbl_unidades","No hay registros para mostrar...");
    $('#tbl_unidades').DataTable({
    "scrollX": true,
    "language": {
        "lengthMenu": "Mostrar _MENU_ registros por pagina",
          //"info": "Mostrando pagina _PAGE_ de _PAGES_ / Mostrados: _START_ de _END_ ",
        "sInfo": "Mostrando: _START_ de _END_ - Total registros: _TOTAL_ ",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtrada de _MAX_ registros)",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "No se encontraron registros coincidentes",
        "paginate": {
        "next": "Siguiente",
        "previous": "Anterior"
      },
    }
});
</script>
@endsection
