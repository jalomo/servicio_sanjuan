@layout('tema_luna/layout')
@section('contenido')
	<ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="#">Inicio</a>
	    </li>
	    <li class="breadcrumb-item active">Asesores</li>
  	</ol>
	<div class="row">
		<div class="col-sm-10">
			<h1>Lista de asesores</h1>
		</div>
		<div class="col-sm-1">
			<button id="agregar_operador" class="btn btn-success pull-right">Agregar asesor</button>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div id="div_operadores">
				<table class="table table-bordered table-striped" id="tbl_operadores" width="100%" cellspacing="0">
					<thead>
						<tr class="tr_principal">
							<th>Nombre</th>
							<th>Teléfono</th>
							<th>Correo electrónico</th>
							<th>R.F.C.</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($operadores as $c => $value)
						<tr>
							<td>{{$value->nombre}}</td>
							<td>{{$value->telefono}}</td>
							<td>{{$value->correo}}</td>
							<td>{{$value->rfc}}</td>
							<td>
								<a href="" data-id="{{$value->id}}" class="pe pe-7s-note js_editar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar"></a>
								<!--<a href="" data-id="{{$value->id}}" class="pe-7s-trash js_eliminar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Eliminar"></a>-->
								<a href="{{base_url('citas/lista_citas/'.$value->id)}}" data-id="{{$value->id}}" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Citas">
									<i class="pe pe-7s-note2"></i>
								</a>
								@if($value->activo)
									<a href="" data-id="{{$value->id}}" class="js_activar" data-valor="0" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Desactivar operador">
										<i class="pe pe-7s-check"></i>
									</a>
								@else
									<a href="" data-id="{{$value->id}}" class="pe-7s-switch js_activar" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activar operador"></a>
								@endif

								@if($value->motivo!='')
									<a href="" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="{{$value->motivo}}">
										<i class="pe pe-7s-comment"></i>
									</a>
								@endif
								
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('scripts')

	<script>
		var site_url = "{{site_url()}}";
		var valor = '';
		var mensaje = '';
		var id = '';
		inicializar_tabla("#tbl_operadores",false);
	$("#agregar_operador").on("click",function(){
       var url =site_url+"/citas/agregar_operador/0";
       customModal(url,{},"GET","md",callbackGuardar,"","Guardar","Cancelar","Nuevo operador","modal1");
    });
    $("body").on("click",'.js_editar',function(e){
    	e.preventDefault();
       var id = $(this).data('id')
       var url =site_url+"/citas/agregar_operador/"+id;
       customModal(url,{},"GET","md",callbackGuardar,"","Guardar","Cancelar","Nuevo operador","modal2");
      
    });
    $("body").on("click",'.js_eliminar',function(e){
       e.preventDefault();
       var id = $(this).data('id')
       ConfirmCustom("¿Está seguro de eliminar el operador?", callbackEliminarOperador(id),"", "Confirmar", "Cancelar");
    });
 	$("body").on("click",'.js_activar',function(e){
       e.preventDefault();
       //valor es 0 cuando va desactivar y 1 cuando lo va activar
       valor = $(this).data('valor');
       if(valor==1){
       	mensaje ="¿Está seguro de activar el operador?";
       }else{
       		
       }
       id = $(this).data('id');
       if(valor==1){
       	 ConfirmCustom(mensaje, callbackActivarDesactivar,"", "Confirmar", "Cancelar");
       	}else{
       		 var url =site_url+"/citas/agregar_motivo/";
       		customModal(url,{},"GET","md",callbackActivarDesactivar,"","Guardar","Cancelar","Motivo para desactivar","modal3");
       	}
      
      
    });

    
	function callbackGuardar(){
		var url =site_url+"/citas/agregar_operador";
		ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
			if(isNaN(result)){
				data = JSON.parse( result );
				//Se recorre el json y se coloca el error en la div correspondiente
				$.each(data, function(i, item) {
					 $.each(data, function(i, item) {
	                    $(".error_"+i).empty();
	                    $(".error_"+i).append(item);
	                    $(".error_"+i).css("color","red");
	                });
				});
			}else{
				if(result <0){
					ErrorCustom('El nombre del operador ya fue registrado, por favor intenta con otro');
				}else{
					if(result==0){
						ErrorCustom('No se pudo guardar el operador, por favor intenta de nuevo');
					}else{
						ExitoCustom("Guardado correctamente",function(){
						$(".close").trigger("click");
							buscar();
						});
					}
				}
			}
		});
	}
	function callbackEliminarOperador(id){
		var url =site_url+"/citas/eliminar_operador/";
		ajaxJson(url,{"id":id},"POST","",function(result){
			console.log(result);
			if(result ==0){
					ErrorCustom('El operador no se pudo eliminar, por favor intenta de nuevo');
				}else{
					ExitoCustom("Operador eliminado correctamente",function(){
					$(".close").trigger("click");
						buscar();
					});	
				}
		});
	}
	function buscar(){
		var url =site_url+"/citas/tabla_operadores";
        ajaxLoad(url,{},"div_operadores","POST",function(){
    		inicializar_tabla("#tbl_operadores",false);
    		$('[data-toggle="tooltip"]').tooltip()
      });
	}
	function callbackActivarDesactivar(){
		
		if(valor==1){
			 mensaje ="Operador activado correctamente";
			activar_desactivar();
		}else{
			 mensaje ="Operador desactivado correctamente";
			var motivo = $("#motivo").val();
			if(motivo==''){
				ErrorCustom("Es necesario ingresar el motivo por el que se desactiva al asesor");	
			}else{
				 mensaje ="Operador desactivado correctamente";
				activar_desactivar();
			}
			
		}
		
	}
	function activar_desactivar(){
		var url =site_url+"/citas/cambiar_status/";
		ajaxJson(url,{"id":id,"valor":valor,"tabla":'operadores',"motivo":$("#motivo").val()},"POST","",function(result){
			if(result ==0){
					ErrorCustom('Error al activar o desactivar el operador, por favor intenta de nuevo');
				}else{
					ExitoCustom(mensaje,function(){
					$(".close").trigger("click");
						buscar();
					});	
				}
		});
	}
	</script>
@endsection