@layout('tema_luna/layout')
<style>
.row_campania{
  background: green;
  color: white;
  padding: 7px;
  margin-top: 7px;
}
</style>
@section('css_vista')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('contenido')
    <form action="" id="frm">
      <div class="row">
        <div class="col-sm-3">
          <label for="">Buscar por campo</label>
          <input class="form-control" type="text" name="buscar_campo" id="buscar_campo">
        </div>
        <div class="col-sm-2" style="margin-top:30px;">
          <button type="button" id="buscar" name="buscar" class="btn btn-info pull-right">Buscar</button>
        </div>
      </div>


    </form>
    <br>
  <div class="row">
    <div class="col-sm-12">
      <table class="table table-bordered table-striped table-hover"  id="tabla" width="100%">
            <thead>
               <tr class="tr_principal">
                    <th>ID</th>
                    <th>VIN</th>
                    <th>Cliente</th>
                    <th>Dirección1</th>
                    <th>Dirección2</th>
                    <th>Dirección3</th>
                    <th>Dirección4</th>
                    <th>Ciudad</th>
                    <th>Provincia</th>
                    <th>C.P.</th>
                    <th>Estado</th>
                    <th>ASC1</th>
                    <th>ASC2</th>
                    <th>ASC3</th>
                    <th>ASC4</th>
                    <th># Campaña</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
  </div>
@endsection

@section('scripts')

<script>
  var site_url = "{{site_url()}}";
  var id_campania = '';
  var serie = '';
  var id_campania = '';
  var accion = '';
  var valor = '';
  var aPos = '';
  var site_url = "{{site_url()}}";
  var campo = '';
  var tipo = '';
  var id_input = '';
  var valor_input = '';

    Vals = '';

    $('.date').datetimepicker({
      format: 'DD/MM/YYYY',
      icons: {
          time: "far fa-clock",
          date: "fa fa-calendar",
          up: "fa fa-arrow-up",
          down: "fa fa-arrow-down"
      },
       locale: 'es'
    });
  $(document).ready(function() {
    iniciarTabla();
    $("#buscar").on('click',function(){
      $(".error").empty();
      var tipo_busqueda = $("#tipo_busqueda").val();
      var finicio = $("#finicio").val();
      var ffin = $("#ffin").val();

      if(tipo_busqueda!=''){
        if(finicio==''){

        }else if(ffin==''){
          $(".error_ffin").text('El campo es requerido');
        }else{
          var table = $('#tabla').DataTable();
          table.destroy();
          iniciarTabla();
        }
      }else{
        var table = $('#tabla').DataTable();
        table.destroy();
        iniciarTabla();
      }

    });
    $("div.dataTables_filter input").on('change',function(e){
    });
  });

        function iniciarTabla(){

            var tabla = $("#tabla").dataTable({
                paging: true,
                bFilter: false,
                processing: true,
                responsive: true,
                serverSide:true,
                ajax: {
                  url: site_url+"/citas/getDatosAscCampanias",
                  type: 'POST',
                   //////////////////////////////////////////////////////////////// AQUI PUEDO AGREGAR MAS CAMPOS EN LA PETICION
                  data: function(data){
                     data.fecha_inicio = $("#finicio").val();
                     data.fecha_fin = $("#ffin").val();
                     data.tipo_busqueda = $("#tipo_busqueda").val();
                     data.buscar_campo = $("#buscar_campo").val();
                     data.fecha_inicio = $("#fecha_inicio").val();
                     data.fecha_fin = $("#fecha_fin").val();
                    // if(filtros.pagina){
                    //   data.start = parseInt(filtros.pagina);
                    // }
                    empieza = data.start;
                    por_pagina = data.length;
                  }
                },
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "oLanguage": {
                   "oPaginate": {
                       "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                       "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                    '<option value="5">5</option>' +
                    '<option value="10">10</option>' +
                    '<option value="20">20</option>' +
                   '<option value="30">30</option>' +
                    '<option value="40">40</option>' +
                    '<option value="50">50</option>' +
                    '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "No hay registros para mostrar...",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar"
                },
            });
        }
    $("body").on("click",'.js_comentarios',function(e){
    e.preventDefault();
    id_campania = $(this).data('id');
       var url =site_url+"/citas/comentarios_Campanias";
       customModal(url,{"id_campania":id_campania},"GET","md",ingresarComentario,"","Guardar","Cancelar","Ingresar comentario","modal1");
    });
    $("body").on("click",'.js_historial',function(e){
    e.preventDefault();
    id_campania = $(this).data('id');
       var url =site_url+"/citas/getComentariosCampanias/0";
       customModal(url,{"id_campania":id_campania},"GET","md","","","","Cerrar","Historial de comentarios","modal1");
    });

    $("body").on("click",'.js_campania',function(e){
    e.preventDefault();
    id_campania = $(this).data('id');
    serie = $(this).data('serie');
    campo = $(this).data('campo');
    tipo = $(this).data('tipo');
    ConfirmCustom("¿Está seguro de asignar la campaña?", cambiarCampania,"", "Confirmar", "Cancelar");

       customModal(url,{"id_campania":id_campania},"GET","md","","","","Cerrar","Realizó campaña","modal1");
    });

  function cambiarCampania(){
     var url =site_url+"/citas/asignar_campania_asc/0";
     ajaxJson(url,{"id_campania":id_campania,"serie":serie,"campo":campo,"tipo":tipo},"POST","",function(result){
      if(result ==0){
          ErrorCustom('Error al actualizar, por favor intenta de nuevo');
        }else{
           ExitoCustom("Registro actualiza correctamente",function(){
            location.reload();
           });

        }
    });
  }


  function ingresarComentario(){
    var url =site_url+"/citas/comentarios_Campanias";
    ajaxJson(url,$("#frm_comentarios").serialize(),"POST","",function(result){
      if(isNaN(result)){
        data = JSON.parse( result );
        //Se recorre el json y se coloca el error en la div correspondiente
        $.each(data, function(i, item) {
           $.each(data, function(i, item) {
                      $(".error_"+i).empty();
                      $(".error_"+i).append(item);
                      $(".error_"+i).css("color","red");
                  });
        });
      }else{
        if(result <0){
          ErrorCustom('No se pudo guardar el comentario, por favor intenta de nuevo');
        }else{
          $(".close").trigger('click');
           ExitoCustom("Comentario guardado con éxito");

        }
      }
    });
  }

  $("body").on("click",'.js_actualizar_asc',function(e){
    e.preventDefault();
    campo = $(this).data('campo');
    id_input = $(this).data('idcampo');
    serie = $(this).data('serie');
    id = $(this).data('id');
    valor_input = $("#"+id_input).val();

    if(valor_input==''){
      ErrorCustom("Es necesario ingresar el campo");
    }else{
      ConfirmCustom("¿Está seguro de actualizar el registro?", cambiarASC,"", "Confirmar", "Cancelar");

    }

  });


  function cambiarASC(){
    var url =site_url+"/citas/cambiarASC";
    ajaxJson(url,{"campo":campo,"valor":valor_input,"id":id},"POST","",function(result){
        if(result ==0){
          ErrorCustom('No se pudo actualizar el registro, por favor intenta de nuevo');
        }else{

          ExitoCustom("Registro actualizado con éxito",function(){
            var uncheck = valor_input+'<br><a href="#" class="pe pe-7s-check js_campania" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-id="'+id+'" data-serie="'+serie+'" data-tipo="'+valor_input+'" title="Se realizó campaña" data-campo="fcampania_'+campo+'"></a>';
            $("#"+id_input).parent("td").empty().append(uncheck);
          });

        }
    });

  }

  $("body").on("click",'.js_refacciones',function(e){
    e.preventDefault();
      var url =site_url+"/citas/refacciones_shara/";
      customModal(url,{},"POST","lg","","","","Salir","Refacciones","modal1");
    }); 


</script>
@endsection
