@layout('tema_luna/layout')
@section('contenido')
<style>
	#tabla {
    width: 100%;
    overflow-x: auto;
    white-space: nowrap;
	}
</style>
<form action="" method="POST" id="frm">
	<input type="hidden" name="tipo_reagendar_proact" id="tipo_reagendar_proact" value="{{$tipo_reagendar_proact}}">
	<div class="row">
		<div class="col-sm-3 form-group">
			<label>Tipo</label><br>
			{{$drop_tipo}}
		</div>
		<div class="col-sm-3 form-group">
			<label>Estatus cita</label><br>
			{{$drop_id_status_cita}}
		</div>
		<div class="col-sm-3 form-group">
			<label>Asesor</label><br>
			{{$drop_asesores}}
		</div>
		<div class="col-sm-3 form-group">
			<label>Técnico</label><br>
			{{$drop_tecnicos}}
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 form-group">
			<label>Cancelada</label><br>
			{{$drop_tipo_canceladas}}
		</div>
		<div class="col-sm-3 form-group">
			<label>Origen</label><br>
			{{$drop_origen}}
		</div>
		<div class="col-sm-3 form-group">
			<label>Placas</label>
			{{$input_placas}}
		</div>
		<div class="col-sm-3 form-group">
			<label>Cliente</label>
			{{$input_cliente}}
		</div>
	</div>
	<div class="row">
		<div class='col-sm-3 form-group'>
        	<label for="">Fecha Inicio</label>
        	<input id="finicio" name="finicio" type='date' class="form-control" value="" />
         	<span class="error_fecha"></span>
        </div>
        <div class='col-sm-3 form-group'>
        	<label for="">Fecha Fin</label>
        	<input id="ffin" name="ffin" type='date' class="form-control" value="" />
         	<span class="error_fecha"></span>
        </div>
	</div>
	<div class="row">
		<div class="col-sm-12 text-right form-group">
			<button id="buscar" class="btn btn-success">Buscar</button>
			<!--<button id="excel" class="btn btn-info">Exportar a excel</button>-->
		</div>
	</div>
</form>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div id="tabla">
		    	 {{$tabla}}
		   	</div>
		</div>
	</div>
@endsection
@section('scripts')
<script>
	var site_url = "{{site_url()}}";
	var id_cita = '';
	var accion = '';
	$('.date').datetimepicker({
        	//minDate: moment(),
        	format: 'DD/MM/YYYY',
        	icons: {
                time: "far fa-clock",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
        	},
        	 locale: 'es'
        });
        $('#datetimepicker1').datetimepicker({
        	format: 'DD/MM/YYYY',
        	locale: 'es'
        });
        $('#datetimepicker2').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            format: 'DD/MM/YYYY',
        	locale: 'es'
        });
        // $("#datetimepicker1").on("dp.change", function (e) {
        //     $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
        // });
        // $("#datetimepicker2").on("dp.change", function (e) {
        //     $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
        // });

        $('.multiple').multiselect();
        $('body').on('click','.busquedalink',function(e){
			e.preventDefault();			
			url=$(this).prop('href');
			ajaxLoad(url,$("#frm").serialize(),"tabla","POST",function(){

			});
		});
		$("#buscar").on("click",function(e){
			e.preventDefault();
			url="<?php echo site_url('citas/buscar_citas_reagendar') ?>";
			ajaxLoad(url,$("#frm").serialize(),"tabla","POST",function(){

			});
		});
	 $("body").on("click",'.js_eliminar1',function(e){
       e.preventDefault();
       var id = $(this).data('id')
       ConfirmCustom("¿Está seguro de eliminar la cita?", callbackEliminarCita(id),"", "Confirmar", "Cancelar");
      
    });
 	$("body").on("click",'.editar_cita',function(e){
	 	e.preventDefault();
	 	id_cita = $(this).data('id_cita');
	  	window.location.href = site_url+'/citas/agendar_cita/'+id_cita+'/1';
  	}); 
    $("body").on("click",'.js_comentarios',function(e){
	 	e.preventDefault();
	 	id_cita = $(this).data('id');
       var url =site_url+"/citas/comentarios_reagendar/0";
       customModal(url,{"id_cita":id_cita,"tipo_reagendar_proact":$("#tipo_reagendar_proact").val()},"GET","md",ingresarComentario,"","Guardar","Cancelar","Ingresar comentario","modal1");
  	}); 
  	$("body").on("click",'.js_historial',function(e){
	 	e.preventDefault();
	 	id_cita = $(this).data('id');
       var url =site_url+"/citas/historial_comentarios_reagendar/0";
       customModal(url,{"id_cita":id_cita,"tipo_reagendar_proact":$("#tipo_reagendar_proact").val()},"GET","md","","","","Cerrar","Historial de comentarios","modal1");
  	}); 
	function callbackEliminarCita(){
		var url =site_url+"/citas/eliminar_cita_bd/";
		ajaxJson(url,{"id":id_cita},"POST","",function(result){
			if(result ==0){
					ErrorCustom('La cita no se pudo eliminar, por favor intenta de nuevo');
				}else{
					ExitoCustom("Cita eliminada correctamente",function(){
						window.location.href = site_url+"/citas/ver_citas";
					});	
				}
		});
	}
	function ingresarComentario(){
		var url =site_url+"/citas/comentarios_reagendar";
		ajaxJson(url,$("#frm_comentarios").serialize(),"POST","",function(result){
			if(isNaN(result)){
				data = JSON.parse( result );
				//Se recorre el json y se coloca el error en la div correspondiente
				$.each(data, function(i, item) {
					 $.each(data, function(i, item) {
	                    $(".error_"+i).empty();
	                    $(".error_"+i).append(item);
	                    $(".error_"+i).css("color","red");
	                });
				});
			}else{
				if(result <0){
					ErrorCustom('No se pudo guardar el comentario, por favor intenta de nuevo');
				}else{
					$(".close").trigger('click');
					if($("#tipo_reagendar_proact").val()=='reagendar'){
						ExitoCustom("Comentario guardado con éxito");
					}else{
						ExitoCustom("Comentario guardado con éxito",function(){
							window.location.reload();
						});
					}
					
				}
			}
		});
	}
</script>
@endsection