<div class="row">
	<div class="col-sm-6">
		<strong>Hora comida inicio: </strong><br><span>{{$hora_comida->hora_inicio_comida}}</span>
	</div>
	<div class="col-sm-6">
		<strong>Hora comida fin: </strong><br><span>{{$hora_comida->hora_fin_comida}}</span>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		@if(count($citas_asignadas)>0)
			<table class="table table-striped table-responsive table-bordered">
				<thead>
					<tr>
						<th>Hora inicio</th>
						<th>Hora fin</th>
						<th>Fecha inicio</th>
						<th>Fecha fin</th>
					</tr>
				</thead>
				<tbody>
					@foreach($citas_asignadas as $c => $value)
						<tr>
							<th>{{$value->hora_inicio}}</th>
							<th>{{$value->hora_fin}}</th>
							<th>{{date_eng2esp_1($value->fecha)}}</th>
							<th>{{date_eng2esp_1($value->fecha_fin)}}</th>
						</tr>
					@endforeach
				</tbody>
			</table>
		@else
		<h3>No se han asignado citas a este técnico</h3>
		@endif
	</div>
</div>