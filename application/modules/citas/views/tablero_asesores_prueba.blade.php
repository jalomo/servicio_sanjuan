<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <base href="{{base_url()}}">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Ford Plasencia</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">

   <!-- CSS CUSTOM-->
  <link href="css/custom/bootstrap-datetimepicker.css" rel="stylesheet">
  <!--<link href="css/custom/datepicker3.css" rel="stylesheet">-->
  <link href="css/custom/isloading.css" rel="stylesheet">
  <link href="css/custom/style.css" rel="stylesheet">
  <link href="css/custom/bootstrap-switch.css" rel="stylesheet">
  <link href="css/custom/clockpicker.css" rel="stylesheet">
  <style>
		.gris{
			color: #000;
		}
		.verde{
			color: #1F7D31;
			background-color: transparent
		}
		body{
			background-color: #BDBDBD;
			font-weight: bold;
		}
		.container-fluid{
			color: white;
		}
	</style>
</head>

<body class="sticky-footer" id="page-top">
  <div class="content-wrapper" style="background-color:#BDBDBD ">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
		<div class="row">
			<div class="col-sm-12">
				<span style="font-size: 30px;font-weight: bold;text-align: center;">Tablero de citas por asesor</span>
				<a style="color: #fff;" href="citas/cerrar_sesion_asesor" class="pull-right cerrar_sesion">Cerrar Sesión</a>
			</div>
		</div>
		<br>
		<div class="row form-group">
		        <div class='col-sm-3'>
		        	<label for="">Selecciona la fecha</label>
		            <div class="form-group1">
		                <div class='input-group date' id='datetimepicker1'>
		                    <input id="fecha" name="fecha" type='text' class="form-control" value="{{date_eng2esp_1($fecha)}}" />
		                    <span class="input-group-addon">
		                        <span class="fa fa-calendar"></span>
		                    </span>
		                </div>
		            </div>
		             <span class="error_fecha"></span>
		        </div>
		        <div class="col-sm-2">
		        	<br>
		        	<button id="buscar" style="margin-top: 8px;" class="btn btn-success">Buscar</button>
		        </div>
		</div>
		<br>
		<div id="div_tabla">
			<div class="row">
				<div class="col-sm-6">
					{{$tabla1}}
				</div> <!-- col-md-6 -->

				<div class="col-sm-6">
					{{$tabla2}}
				</div> <!-- col-md-6 -->
			</div>
		</div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>

    <!--scripts mios -->
	 <script src="js/custom/bootbox.min.js"></script>
	 <script src="js/custom/general.js"></script>
   <script src="js/custom/isloading.js"></script>
   <script src="js/custom/moment.js"></script>
   <script src="js/custom/bootstrap-datetimepicker.js"></script>

   <script src="js/custom/bootstrap-switch.js"></script>
 	<script src="js/custom/clockpicker.js"></script>
   
       <script type="text/javascript">
    	var site_url = "{{site_url()}}";
    	var id = '' ; 
		var status = '' ;
            $(function () {
                $('#datetimepicker1').datetimepicker({
                	//minDate: moment(),
                	format: 'DD/MM/YYYY',
                	icons: {
	                    time: "far fa-clock",
	                    date: "fa fa-calendar",
	                    up: "fa fa-arrow-up",
	                    down: "fa fa-arrow-down"
                	},
                	 locale: 'es'
                });
                $("#buscar").on('click',buscar);
            });
          $("body").on('click','.js_cambiar_status',function(e){
          	e.preventDefault();
          		id = $(this).data('id')
          		status = $(this).data('status')
       			ConfirmCustom("¿Está seguro de cambiar el estatus a la cita?", callbackCambiarStatus,"", "Confirmar", "Cancelar");
          });
      	function buscar(){
		var url =site_url+"/citas/tabla_horarios_asesores";
	        ajaxLoad(url,{"fecha":$("#fecha").val()},"div_tabla","POST",function(){
	      });
		} 
		function callbackCambiarStatus(){
		var url =site_url+"/citas/cambiar_status_cita/";
		ajaxJson(url,{"id":id,"status":status},"POST","",function(result){
			if(result ==0){
					ErrorCustom('No se pudo cambiar el estatus, por favor intenta de nuevo');
				}else{
					ExitoCustom("Estatus cambiado correctamente",function(){
						buscar();
					});	
				}
		});
	}  
</script>
  </div>
</body>

</html>
