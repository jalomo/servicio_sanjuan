@layout('tema_luna/layout')
<style>
  .mayusculas{
     text-transform: uppercase;
  }
</style>
@section('contenido')
<h3>GOASIS</h3>
<div class="row">
	<div class="col-md-6">
        <label for=""># Serie</label>
        <input type="text" id="vehiculo_numero_serie" name="vehiculo_numero_serie" class="form-control mayusculas" maxlength="17">
  	</div>
	<div class="col-md-6">
        <label for="">Kilometraje</label>
        <input type="number" id="vehiculo_kilometraje" name="vehiculo_kilometraje" class="form-control" maxlength="10">
  	</div>
</div>
<br>
<div class="row pull-right">
      <div class="col-sm-12">
          <button type="button" id="btn-goasis" class="btn btn-info">VALIDAR</button>
      </div>
  </div>
@endsection

@section('scripts')
<script>
	var site_url = "{{site_url()}}";
	$("body").on("click",'#btn-goasis',function(e){
    e.preventDefault();
    var url =site_url+"/citas/g_oasis";
    if($("#vehiculo_numero_serie").val()==''){
      ErrorCustom("Es necesario agregar el #serie");
    }else{
      if($("#vehiculo_kilometraje").val()==''){
        var km = 0;
      }else{
        var km = $("#vehiculo_kilometraje").val();
      }
      var length = $("#vehiculo_numero_serie").val().length;
      if(length==17){
       customModal(url,{"numero_serie":$("#vehiculo_numero_serie").val().toUpperCase(),"kilometraje":km},"POST","md","","","","Cerrar","GOASIS","modalGoasis");
      }else{
        ErrorCustom("El campo #serie debe tener exactamente 17 caracteres");
      }
    }
  }); 
</script>
@endsection