<table id="tlb-prep" class="table table-hover">
	<thead>
		<tr>
			<th>Código</th>
			<th>Descripción</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@if(count($articulos_prepiking)>0)
			@foreach($articulos_prepiking as $a => $articulo)
			<tr>
				<td>{{$articulo->codigo}}</td>
				<td>{{$articulo->descripcion}}</td>
				<td>
					<a href="" data-idcita="{{$id_cita}}" class="pe pe-7s-note agregar_extra_prepiking" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar" data-idprepiking="{{$articulo->id}}" data-edit="{{($articulo->registro_individual==1)?'1':0}}"></a>
				</td>
				
			</tr>
			@endforeach
		@else
			<tr class="text-center">
				<td colspan="3">No hay registros para mostrar...</td>
			</tr>
		@endif
	</tbody>
</table>