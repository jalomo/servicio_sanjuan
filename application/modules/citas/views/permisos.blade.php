@layout('tema_luna/layout')
@section('contenido')

<div class="container1">
  <h2>Permisos de usuarios</h2>

  <table class="table">
    <thead>
      <tr>
        <th>Usuario</th>
        <th>CITAS SERVICIOS</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($datos as $row):?>
      <tr>
        <td><?php echo $row->adminNombre?></td>
        <td>
          <?php if($row->p_citas == 0){?>
            <input type="checkbox" name="vehicle" value="<?php echo $row->p_citas;?>" onclick="asignar_user(6,<?php echo $row->adminId;?>)">
          <?php }else{?>
            <input type="checkbox" name="vehicle" value="<?php echo $row->p_citas;?>" onclick="asignar_user(6,<?php echo $row->adminId;?>)" checked>
          <?php }?>
        </td>
      </tr>
    <?php endforeach;?>
    </tbody>
  </table>
</div>
@endsection

@section('scripts')
<script>
function asignar_user(tipo,id){
  value_json = $.ajax({
               type: "POST",
               url: '<?php echo base_url();?>index.php/citas/asigna_usuario/',
               async: false,
              data:{idadmin:id ,tipo_id:tipo },
               dataType: "text",
                success: function(data){

                  }
               }).responseText;

}

function asignar_user_gerente(tipo,id){


  value_json = $.ajax({
               type: "POST",
               url: '<?php echo base_url();?>index.php/citas/asigna_usuario_gerente/',
               async: false,
              data:{idadmin:id ,tipo_id:tipo },
               dataType: "text",
                success: function(data){

                  }
               }).responseText;

}
</script>
@endsection
