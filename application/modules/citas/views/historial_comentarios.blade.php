<div style="overflow-y: scroll;height: 400px;" class="row">
	<div class="col-sm-12">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th width="30%">Fecha</th>
					<th>Comentario</th>
				</tr>
			</thead>
			<tbody>
				@if(count($comentarios)>0)
				@foreach($comentarios as $c => $value)
					<tr>
						<td width="30%">{{$value->fecha_creacion}}</td>
						<td>{{$value->comentario}}</td>
					</tr>
				@endforeach
				@else
				<tr class="text-center">
					<td colspan="2">Aún no se han registrado comentarios... </td>
				</tr>
				@endif
			</tbody>

		</table>
	</div>
</div>