@layout('tema_luna/layout')
@section('contenido')
<h2>Agregar Horarios al técnico</h2>
<br>
<form action="" method="POST" id="frm">
  <div class="row">
    <div class="col-sm-4">
      <label>Selecciona al técnico</label>
      {{$drop_tecnicos}}
    </div>
    <div class='col-sm-4'>
      <label for="">A partir de</label>
      <input type="fecha" name="fecha" value="" class="form-control" readonly="" id="fecha">
      <span class="error error_fecha"></span>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-sm-3">
      <label for="">Hora de inicio (L-V)</label>
      <input type="time" name="hilv" id="hilv" class="form-control">
      <span class="error_hilv"></span>
    </div>
    <div class="col-sm-3">
      <label for="">Hora de fin (L-V)</label>
      <input type="time" class="form-control" name="hflv" id="hflv">
      <span class="error error_hflv"></span>
    </div>
    <div class="col-sm-3">
      <label for="">Hora inicio comida (L-V)</label>
      <input type="time" class="form-control" name="comidailv" id="comidailv">
      <span class="error error_comidailv"></span>
    </div>
    <div class="col-sm-3">
      <label for="">Hora fin comida (L-V)</label>
      <input type="time" class="form-control" name="comidaflv" id="comidaflv">
      <span class="error error_comidailv"></span>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-sm-3">
      <label for="">Hora de inicio (Sábado)</label>
      <input type="time" class="form-control" name="his" id="his">
      <span class="error_his"></span>
    </div>
    <div class="col-sm-3">
      <label for="">Hora de fin (Sábado)</label>
      <input type="time" class="form-control" name="hfs" id="hfs">
      <span class="error error_hfs"></span>
    </div>
    <div class="col-sm-3">
      <label for="">Hora inicio comida (Sábado)</label>
      <input type="time" class="form-control" name="comidasai" id="comidasai">
      <span class="error error_comidasai"></span>
    </div>
    <div class="col-sm-3">
      <label for="">Hora fin comida (Sábado)</label>
      <input type="time" class="form-control" name="comidasaf" id="comidasaf">
      <span class="error error_comidasaf"></span>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-sm-3">
      <label for="">Hora de inicio (Domingo)</label>
      <input type="time" class="form-control" name="hid" id="hid">
      <span class="error_hid"></span>
    </div>
    <div class="col-sm-3">
      <label for="">Hora de fin (Domingo)</label>
      <input type="time" class="form-control" name="hfd" id="hfd">
      <span class="error error_hfd"></span>
    </div>
    <div class="col-sm-3">
      <label for="">Hora inicio comida (Domingo)</label>
      <input type="time" class="form-control" name="comidadoi" id="comidadoi">
      <span class="error error_comidadoi"></span>
    </div>
    <div class="col-sm-3">
      <label for="">Hora fin comida (Domingo)</label>
      <input type="time" class="form-control" name="comidadof" id="comidadof">
      <span class="error error_comidadof"></span>
    </div>
  </div>
</form>
<br>
<div class="row">
  <div class="col-sm-12 text-right">
    <button id="guardar" class="btn btn-success">Guardar</button>
  </div>
</div>
@endsection
@section('scripts')

<script>
	var site_url = "{{site_url()}}";
	var id_cita = '';
	var accion = '';
	 $('.clockpicker').clockpicker();
	 $("#guardar").on('click',function(){
	 	if($("#id_tecnico").val()!=''){
			var url = site_url+'/citas/saveHorariosByTecnico';
	        ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
	                if(isNaN(result)){
	                data = JSON.parse( result );
	                //Se recorre el json y se coloca el error en la div correspondiente
	                $.each(data, function(i, item) {
	                    $(".error_"+i).empty();
	                    $(".error_"+i).append(item);
	                    $(".error_"+i).css("color","red");
	                });
	        }else{
	        		if(result==1){
	        			ExitoCustom("Guardado correctamente",function(){
	        				window.location.reload();
	        			});
	        		}
	    		}
	        });
        }else{
        	ErrorCustom("Es necesario seleccionar al técnico");
        }
	});
   $("#id_tecnico").on('change',function(){
      var valor = $(this).val();
      if(valor!=''){
        var url = site_url+'/citas/getDateByTecnico';
          ajaxJson(url,{"id_tecnico":valor},"POST","",function(result){
            $("#fecha").val(result);
          });
      }
   })
</script>
@endsection