@layout('tema_luna/layout')
@section('contenido')
	<ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="#">Inicio</a>
	    </li>
	    <li class="breadcrumb-item active">Códigos</li>
  	</ol>
	<div class="row">
		<div class="col-sm-8">
			<h1>Lista de códigos Genéricos</h1>
		</div>
		<div class="col-sm-4">
			<button id="agregar_codigo" class="btn btn-success pull-right">Agregar código</button>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div id="div_codigos">
				<table id="tbl_codigos" class="table table-bordered table-striped" width="100%" cellpadding="0">
					<thead>
						<tr class="tr_principal">
							<th>Código</th>
							<th>Descripción</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($codigos as $c => $value)
						<tr>
							<td>{{$value->codigo}}</td>
							<td>{{$value->descripcion}}</td>
							<td>
								<a href="" data-id="{{$value->id}}" class="pe pe-7s-note js_editar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar"></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('scripts')

	<script>
		var site_url = "{{site_url()}}";
		inicializar_tabla("#tbl_codigos",false);
	$("#agregar_codigo").on("click",function(){
       var url =site_url+"/citas/agregar_codigo/0";
       customModal(url,{},"GET","md",callbackGuardar,"","Guardar","Cancelar","Nuevo código","modal1");
    });
    $("body").on("click",'.js_editar',function(e){
    	e.preventDefault();
       var id = $(this).data('id')
       var url =site_url+"/citas/agregar_codigo/"+id;
       customModal(url,{},"GET","md",callbackGuardar,"","Guardar","Cancelar","Nuevo código","modal1");
      
    });

    
	function callbackGuardar(){
		var url =site_url+"/citas/agregar_codigo";
		ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
			if(isNaN(result)){
				data = JSON.parse( result );
				//Se recorre el json y se coloca el error en la div correspondiente
				$.each(data, function(i, item) {
					 $.each(data, function(i, item) {
	                    $(".error_"+i).empty();
	                    $(".error_"+i).append(item);
	                    $(".error_"+i).css("color","red");
	                });
				});
			}else{
				if(result ==-1){
					ErrorCustom('El código ya fue registrado, por favor intenta con otro');
				}else if(result ==-2){
					ErrorCustom('El código ya fue registrado como un código correctivo');
				}else if(result==0){
					ErrorCustom('No se pudo guardar el código, por favor intenta de nuevo');
				}else{
					ExitoCustom("Guardado correctamente",function(){
					$(".close").trigger("click");
						buscar();
					});
				}
			}
		});
	}
	function buscar(){
		var url =site_url+"/citas/tabla_codigos_gen";
        ajaxLoad(url,{},"div_codigos","POST",function(){
    		inicializar_tabla("#tbl_codigos",false);
      });
	}
	</script>
@endsection