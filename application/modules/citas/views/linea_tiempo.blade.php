@layout('tema_luna/layout')
<style>
	.detail span{
		font-size: 20px;
	}
</style>
@section('contenido')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>


	<ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="#">Inicio</a>
	    </li>
	    <li class="breadcrumb-item active">Línea de tiempo</li>
  	</ol>
  	<div class="row">
  		<!--<div class="col-sm-4 detail">
  			<span for="">Tiempo de cita : {{$tiempo_cita}} </span> 
  		</div>-->
  		<div class="col-sm-4 detail">
  			<span for="">Técnico : {{$tecnico_asignado}} </span> 
  		</div>
  		<div class="col-sm-4 detail">
  			<span for="">Asesor : {{$asesor_asignado}} </span> 
  		</div>
  		<div class="col-sm-4 detail">
  			<span for="">Hora de cita : {{$hora_cita}} </span> 
  		</div>
  	</div>
  	<br>
  	<div class="row">
  		<div class="col-sm-4">
  			<?php $tiempo_cita = 0 ?>
  			@foreach($tecnicos_citas as $t => $value)
  			<?php $tiempo_cita = $this->mc->diferencia_horas_laborales($value->fecha.' '.$value->hora_inicio_dia,$value->fecha_fin.' '.$value->hora_fin); ?>

				Desde {{date_eng2esp_1($value->fecha).' '.$value->hora_inicio_dia}} Hasta {{date_eng2esp_1($value->fecha_fin).' '.$value->hora_fin}} <br>
  			@endforeach
  		</div>
  		<div class="col-sm-4">
  			<label for="">Tiempo de cita: {{$tiempo_cita}} minutos </label>
  		</div>
  		<div class="col-sm-4">
  			<label for="">Fecha promesa: {{$fecha_promesa}} </label>
  		</div>
  	</div>
  	<br>
	  <div class="row">
	  	<div class="col-sm-12">
		  	<h2 class="text-center">Operación: {{$nombre_operacion}}</h2>
		  </div>
	  </div>
  	<br>
  	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered table-striped table-striped">
				<thead>
					<tr>
						<th>Usuario</th>
						<th>De</th>
						<th>A</th>
						<th>Fecha estatus anterior</th>
						<th>Fecha estatus nuevo</th>
						<th>Tipo</th>
						<th>Tiempo (minutos)</th>

					</tr>
				</thead>
				<tbody>
					<?php $suma_minutos = 0 ?>
					@foreach($diferencias as $d => $value)
						<tr>

							<td>{{$value->usuario}}</td>
							<td>
								@if($value->tipo==2 && $value->status_anterior=='')
									Planeado
								@else
								{{$value->status_anterior}}
								@endif
							</td>
							<td>{{$value->status_nuevo}}</td>
							<td>{{$value->inicio}}</td>
							<td>{{$value->fin}}</td>
							<td>{{($value->tipo==1)?'Asesor':'Técnico'}}</td>
							<?php $suma_minutos = $suma_minutos + $value->minutos_pasados ?>
							<td>{{$value->minutos_pasados}}</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr >
						<td colspan="6">
						<td>
							Tiempo de proceso: {{$this->mc->toHours($suma_minutos)}}
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<h2 class="text-center">Tiempo del proceso Asesores / Técnicos</h2>
			<div id="container" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
		</div>
	</div>

@endsection

@section('scripts')
<script>
	var array_diferencias = [];
	@foreach($diferencias as $d => $value)
		var array_diferencias_temp = [];
		var tipo = "{{($value->tipo==1)?'(A)':'(T)'}}";
		@if($value->status_anterior!='')
			array_diferencias_temp.push("{{$value->status_anterior}}",parseInt("{{$value->minutos_pasados}}"));
		@else
			@if($value->tipo==1)
			array_diferencias_temp.push("check asesor",parseInt("{{$value->minutos_pasados}}"));
			@else
				@if($value->status_anterior=='')
					array_diferencias_temp.push("Planeado",parseInt("{{$value->minutos_pasados}}"));
				@endif
			@endif
		@endif
		array_diferencias.push(array_diferencias_temp);
	@endforeach
	

	//console.log(array_diferencias);
	Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Minutos'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Minutos: <b>{point.y:.1f}</b>'
    },
    series: [{
        name: 'Population',
        data: array_diferencias,
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});
</script>

@endsection