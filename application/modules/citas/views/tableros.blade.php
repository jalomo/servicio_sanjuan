<link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>img/so.png"/>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div id="div_espera" class="col-sm-12" style="display: inline; width: 100%;float: left;">
            <div class="ibox">
                <div class="ibox-content">

                    <iframe src="{{CONST_URL_BASE_INTERNA}}tablero_espera" width="100%" height="100%">
                      <p>Your browser does not support iframes.</p>
                    </iframe>
                    <div class="clients-list">
                    </div>
                </div>
            </div>
        </div>
        <div id="div_asesores" class="col-sm-12" style="display: inline; width: 100%;float: right;">
            <div class="ibox">
                <div class="ibox-content">

                    <iframe src="{{CONST_URL_BASE_INTERNA}}tablero_asesores" width="100%" height="100%">
                      <p>Your browser does not support iframes.</p>
                    </iframe>
                    <div class="clients-list">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
<script>
    var mostrar = false;
    $("#div_asesores").hide();
    function changeTablero(){
        if(!mostrar){
            $("#div_espera").hide('slow');
            $("#div_asesores").show('slow');
        }else{
            $("#div_espera").show('slow');
            $("#div_asesores").hide('slow');
        }
        mostrar = !mostrar;
    }
    setInterval(changeTablero,100000);

</script>