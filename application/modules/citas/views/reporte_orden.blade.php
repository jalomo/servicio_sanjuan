<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<title>Document</title>
	<style>
		.text-center{
			text-align: center;
		}
		
		*{
			font-size: 10px !important;
		}
		label{
			font-weight: bold;
		}
		.text-header{
			font-weight: bold; 
			text-align: center;
			font-size: 10px;
		}
	</style>
</head>
	
<body>
	<h1 style="font-size: 20px !important;" class="text-center">{{CONST_AGENCIA}} S.A DE C.V.</h2>
	<h1 style="font-size: 20px !important;" class="text-center">Reporte de ordenes</h1>
	<table width="100%" class="table" border="1">
		<thead>
			<tr>
				<th>ID</th>
				<th>Fecha ingreso</th>
				<th>Vehiculo</th>
				<th>Asesor</th>
				<th>Cliente</th>
				<th>Placas</th>
				<th>Técnico</th>
				<th>Estatus</th>
			</tr>
		</thead>
		<tbody>
			@foreach($orden as $o => $value)
				<tr>
					<td>{{$value->identificador.'-'.$value->id_cita}}</td>
					<td>{{date_eng2esp_1($value->fecha_asesor).' '.$value->hora_asesor}}</td>
					<td>{{$value->vehiculo_modelo}}</td>
					<td>{{$value->asesor}}</td>
					<td>{{$value->datos_nombres.' '.$value->datos_apellido_paterno.' '.$value->datos_apellido_materno}}</td>
					<td>{{$value->vehiculo_placas}}</td>
					<th>{{$value->tecnico}}</th>
					<th>{{$value->status_tecnico}}</th>
				</tr>
			@endforeach
		</tbody>
		
	</table>
</body>
</html>