@layout('tema_luna/layout')
@section('contenido')
		
	<h2>Datos actuales</h2>
	<hr>
	<div class="row">
		<div class="col-sm-6">
			<strong for="">Fecha de creación:</strong>
			{{isset($cita[0]->fecha_creacion)?$cita[0]->fecha_creacion:''}}
		</div>
		<div class="col-sm-6">
			<strong for="">Usuario creación cita:</strong>
			{{isset($cita[0]->adminNombre)?$cita[0]->adminNombre:''}}
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-6">
			<strong for="">Asesor:</strong>
			{{isset($cita[0]->asesor)?$cita[0]->asesor:''}}
		</div>
		<div class="col-sm-6">
			<strong for="">Horario cita:</strong>
			{{isset($cita[0]->fecha_horario)?$cita[0]->fecha_horario.' '.$cita[0]->hora:''}}
		</div>
		
	</div>
	<br>
	<div class="row">
		<div class="col-sm-6">
			<strong for="">Técnico:</strong>
			{{isset($cita[0]->tecnico)?$cita[0]->tecnico:''}}
		</div>
		
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Técnico</th>
						<th>Hora inicio</th>
						<th>Hora fin</th>
						<th>Fecha</th>
						<th>Día completo</th>
						<th>Fecha fin</th>
						<th>Hora inicio del día</th>
					</tr>
				</thead>
				<tbody>
					@foreach($tecnicos_citas as $t =>$value)
						<tr>
							<td>{{$this->mc->getNameTecnico($value->id_tecnico)}}</td>
							<td>{{$value->hora_inicio}}</td>
							<td>{{$value->hora_fin}}</td>
							<td>{{$value->fecha}}</td>
							<td>{{($value->dia_completo==1)?'Si':'No'}}</td>
							<td>{{$value->fecha_fin}}</td>
							<td>{{$value->hora_inicio_dia}}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<br>
	<h2>Historial</h2>
	<hr>
	<div class="row">
		<div class="col-sm-12">
			<div id="div_operadores">
				<table id="tbl_operadores" class="table table striped table-hover" width="100%">
					<thead>
						<tr class="tr_principal">
							<th>Fecha</th>
							<th>Asesor</th>
							<th>Horario cita</th>
							<th>Usuario</th>
							<th>Estatus</th>
							<th>Técnico</th>
							<th>Cambio en técnicos</th>
						</tr>
					</thead>
					<tbody>
						@foreach($historial as $c => $value)
						<tr>
							<td>{{$value->created_at}}</td>
							<td>{{$value->asesor}}</td>
							<td>{{$value->fecha_horario.' '.$value->hora}}</td>
							<td>{{$value->adminNombre}}</td>
							<td>{{$value->estatus}}</td>
							<td>{{$value->tecnico}}</td>
							<td>{{$value->cambio_tecnicos}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
