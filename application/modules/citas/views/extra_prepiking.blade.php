<form action="" id="frm-prep">
	<input type="hidden" id="id_cita" name="id_cita" value="{{$id_cita}}">
	<input type="hidden" id="idprepiking_s" name="idprepiking_s" value="{{$idprepiking_s}}">
	<input type="hidden" id="editar_descripcion" name="editar_descripcion" value="{{$editar_descripcion}}">
	<!-- NUEVA VERSIÓN -->
	<input type="hidden" id="id_operacion_save" name="id_operacion_save" value="{{$id_operacion}}">
	
	<div class="row">
		<div class="col-sm-6">
			<label for="">Código</label>
			{{$input_codigo}}
			<div class="error error_codigo"></div>
		</div>
		<div class="col-sm-6">
			<label for="">Descripción</label>
			{{$input_descripcion}}
			<div class="error error_descripcion"></div>
		</div>
	</div>
</form>