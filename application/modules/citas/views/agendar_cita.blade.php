@layout('tema_luna/layout')
<style type="text/css">
.close{
  display: none
}
  .custom-radios div {
    display: inline-block;
  }
  .custom-radios input[type="radio"] {
    display: none;
  }
  .custom-radios input[type="radio"] + label {
    color: #333;
    font-family: Arial, sans-serif;
    font-size: 14px;
  }
  .custom-radios input[type="radio"] + label span {
    display: inline-block;
    width: 40px;
    height: 40px;
    margin: -1px 4px 0 0;
    vertical-align: middle;
    cursor: pointer;
    border-radius: 50%;
    border: 2px solid #FFFFFF;
    box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.33);
    background-repeat: no-repeat;
    background-position: center;
    text-align: center;
    line-height: 44px;
  }
  .custom-radios input[type="radio"] + label span img {
    opacity: 0;
    transition: all .3s ease;
  }
  .custom-radios input[type="radio"]#color-1 + label span {
    background-color: #2ecc71;
  }
  .custom-radios input[type="radio"]#color-2 + label span {
    background-color: #f1c40f;
  }
  .custom-radios input[type="radio"]#color-3 + label span {
    background-color: #e74c3c;
  }
  .custom-radios input[type="radio"]:checked + label span {
    opacity: 1;
    background: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg) center center no-repeat;
    width: 40px;
    height: 40px;
    display: inline-block;
  }
  .footer.sticky-footer{
    background: transparent !important;
  }
</style>
@section('contenido')

<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="#">Inicio</a>
  </li>
  <li class="breadcrumb-item active">Agendar cita</li>
</ol>
<div class="row">
  <div class="col-sm-8">
    <h2>Agendar Citas</h2>
  </div>
</div>
<div class="row">
  <div class="col-sm-12 text-right">
    <h2 for="" id="lbl-campania"></h2>
    <div class="custom-radios">
      <div id="div-radio-1">
        <input type="radio" id="color-1" name="campania" value="1" class="rd-campania">
        <label for="color-1">
          <span>
          </span>
        </label>
      </div>
      <div id="div-radio-2">
        <input type="radio" id="color-2" name="campania" value="2" class="rd-campania">
        <label for="color-2">
          <span>
          </span>
        </label>
      </div>
      <div id="div-radio-3">
        <input type="radio" id="color-3" name="campania" value="3" class="rd-campania">
        <label for="color-3">
          <span>
          </span>
        </label>
      </div>
    </div>
  </div>
</div>
@if($input_id!=0 && $id_status_cita_asesor!=1 && $reagendada==0 && $id_instalacion==0)
<div class="row">
  <div class="col-sm-12">
    <label for=""></label>
    <div style="margin-top: 10px" class="alert alert-warning alert-dismissible fade show" role="alert">
      <strong>La cita ya se hizo efectiva ya no se puede hacer una edición de la cita</strong>.
    </div>
  </div>
</div>
@endif

<form action="" method="POST" id="frm">
  <input type="hidden" name="id" id="id" value="{{($id_instalacion==1)?0:$input_id}}">
  <input type="hidden" name="id_horario" id="id_horario" value="{{$input_id_horario}}">
  <input type="hidden" id="realizo_servicio" name="realizo_servicio" value="{{ $input_realizo_servicio }}">
  <input type="hidden" name="id_tecnico_actual" id="id_tecnico_actual" value="{{$input_id_tecnico_actual}}">
  <input type="hidden" id="origen" name="origen" value="{{$origen}}">
  <input type="hidden" id="reagendada" name="reagendada" value="{{$reagendada}}">
  <input type="hidden" id="magic" name="magic" value="{{$magic}}">
  <input type="hidden" id="realizo_prepiking" name="realizo_prepiking" value="{{$realizo_prepiking}}">
  <input type="hidden" id="id_instalacion" name="id_instalacion" value="{{$id_instalacion}}">

  <div class="row pull-right">
      <div class="col-sm-12">
          <button type="button" id="btn-no-aut" class="btn btn-info invisible">Ver presupuestos no autorizados</button>
      </div>
  </div>
  <h4>Datos del cliente</h4>
  <hr>
  <div class="row">
    <div class="col-sm-4">
      <strong for="">Buscar cliente</strong>
      <input type="text" id="clientesearch" name="clientesearch" class="form-control">
    </div>
    <div class="col-sm-4">
      <button type="button" id="clientebuscar" style="margin-top: 20px;" class="btn btn-info">Buscar cliente</button>
    </div>
    <div class="col-sm-4">
      <button type="button" id="inventario" style="margin-top: 20px;" class="btn btn-default">Ver inventario</button>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-sm-4 form-group">
      <label for="">Correo electrónico</label>
      {{$input_email}}
      <div class="error error_email"></div>
    </div>
    <div class="col-sm-4 form-group">
      <label for="">Nombre(s)</label>
      {{$input_datos_nombres}}
      <div class="error error_nombre"></div>
    </div>
    <div class="col-sm-4 form-group">
      <label for="">Apellido Paterno</label>
      {{$input_datos_apellido_paterno}}
      <div class="error error_apellido_paterno"></div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-4 form-group">
      <label for="">Apellido Materno</label>
      {{$input_datos_apellido_materno}}
      <div class="error error_apellido_materno"></div>
    </div>
    <div class="col-sm-4 form-group">
      <label for="">Teléfono</label>
      {{$input_datos_telefono}}
      <div class="error error_telefono"></div>
    </div>
  </div>
@if($input_id!=0 && $reagendada==0 && !$existe_orden)
<div class="row">
  <div class="col-sm-12">
    <label for=""></label>
    <div style="margin-top: 10px" class="alert alert-info alert-dismissible fade show" role="alert">
      Si se requiere cambiar el técnico el proceso es el siguiente: <strong>Seleccionar el nombre del técnico en la parte de "Asignar técnico", después en la ventana emergente modificar los horarios y dar click al botón de guardar para finalizar</strong>.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-4">
    <label>Asignar técnico</label>
    {{$drop_tecnicos}}
    <br>

    <button id="asignar_tecnico" class="btn btn-info pull-right">Asignar o cambiar técnico</button>

  </div>
  <div class="col-sm-8">
    <br>
    <strong>Hora inicio trabajo:</strong>
    <span style="margin-right: 10px;" id="hora_inicio_trabajo">00:00</span>
    <strong>Hora fin trabajo:</strong>
    <span style="margin-right: 10px;" id="hora_fin_trabajo">00:00</span>
    <strong>Hora inicio comida:</strong>
    <span style="margin-right: 10px;" id="hora_inicio_comida">00:00</span>
    <strong>Hora fin comida:</strong>
    <span style="margin-right: 10px;" id="hora_fin_comida">00:00</span>
  </div>
</div>
<br>
@endif
<hr>
<h4>Datos del Vehículo</h4>
<hr>
  <div class="row"> 
    <div class="col-sm-4 form-group">
      <label for="">Número de serie</label>
      {{$input_vehiculo_numero_serie}}
      <div class="error error_vehiculo_numero_serie"></div>
    </div>
    <div class="col-sm-4 form-group">
      <label for="">Placas</label>
      {{$input_vehiculo_placas}}
      <div class="error error_vehiculo_placas"></div>
    </div>
    <div class="col-sm-4 form-group">
      <label for="">Color</label>
      {{$drop_color}}
      <div class="error error_id_color"></div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-4 form-group">
      <label for="">Año del vehículo</label>
      {{$drop_vehiculo_anio}}
      <div class="error error_vehiculo_anio"></div>
    </div>
    <div class="col-sm-4 form-group">
      <label for="">Modelo</label>
      {{$drop_vehiculo_modelo}}
      <div class="error error_vehiculo_modelo"></div>
    </div>
    <div class="col-sm-4 form-group">
      <label for="">Submodelo</label>
      {{$drop_submodelo_id}}
      <div class="error error_submodelo_id"></div>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-sm-4 form-group">
      <label for="">Servicio</label>
      {{$drop_servicio}}
      <div class="error error_servicio"></div>
    </div>
    <div class="col-md-4">
      <label for="">Kilometraje</label>
      {{$input_vehiculo_kilometraje}}
      <div class="error error error_vehiculo_kilometraje"></div>
    </div>
    <div class="col-sm-4 form-group">
      <label for="">Modelo prepiking</label>
      {{$drop_id_modelo_prepiking}}
      <div class="error error_id_modelo_prepiking"></div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-4">
      <label for="">Duda del uso de la tecnología</label>
      {{$input_duda}}
    </div>
    <div class="col-sm-4">
      <label for="">Pruebas de Manejo</label> 
      {{$input_demo}}
    </div>
    <div class="col-sm-4">
      <label for="">¿No requiere diagnóstico?</label>
      {{$input_reparacion}}
    </div>
  </div>
  <hr>
  <h4>Datos de la cita</h4>
  <hr>
  <div class="row">
    <div class="col-sm-4 form-group">
      <label for="">Asesor</label>
      {{$drop_asesor}}
      <div class="error error_asesor"></div>
    </div>
    <div class="col-sm-4 form-group">
      <label for="">Fecha</label>
      {{$drop_fecha}}
      <div class="error error_fecha"></div>
    </div>
    <div class="col-sm-4 form-group">
      <label for="">Horario</label>
      {{$drop_horario}}
      <div class="error error_horario"></div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-4 form-group">
      <label for="">Donde realizó la cita</label>
      {{$drop_opcion}}
      <div class="error error_id_opcion_cita"></div>
    </div>
    <div class="col-sm-4 form-group">
      <label for="">Transporte</label>
      {{$drop_transporte}}
      <div class="error error_transporte"></div>
    </div>
    <div class="col-sm-4 form-group">
      <label for="">Estatus</label>
      {{$drop_id_status_color}}
      <div class="error error_id_status_color"></div>
    </div>
  </div>
  @if($input_id==0 || $reagendada!=0||$id_instalacion==1)
  <div class="row">
    <div class="col-sm-4">
      <label for="">¿Cita por días completos?</label>
      {{$input_dia_completo}}
    </div>
  </div>
  <div id="div_completo">
    <div class="row">
      <div class="col-sm-4">
        <label>Asignar técnico <a href="#" class="js_ver_citas_dias">(Ver horarios)</a></label>
        {{$drop_tecnicos_dias}}
        <span class="error error_tecnico_dias"></span>
      </div>
      <div class='col-sm-2'>
        <label for="">Fecha inicio</label>
        {{$input_fecha_inicio}}
        <span class="error_fecha_inicio"></span>
      </div>
      <div class='col-sm-2'>
        <label for="">Fecha Fin</label>
        {{$input_fecha_fin}}
        <span class="error error_fecha_fin"></span>
      </div>
      <div class="col-sm-2">
        <label for="">Hora de inicio del día</label>
        {{$input_hora_comienzo}}
        <span class="error error_hora_comienzo"></span>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4"></div>
      <div class="col-sm-2">
        <label>Día extra</label>
        {{$input_fecha_parcial}}
      </div>
      <div class="col-sm-2">
        <label for="">Hora inicio trabajo</label>
        {{$input_hora_inicio_extra}}
        <span class="error error_hora_inicio_extra"></span>
      </div>
      <div class="col-sm-2">
        <label for="">Hora fin trabajo</label>
        {{$input_hora_fin_extra}}
        <span class="error error_hora_fin_extra"></span>
        <br>
      </div>
    </div>
  </div> <!-- completos -->
  <div id="div_incompleto" class="row">
    <div class="col-sm-4">
      <label>Asignar técnico <a href="" class="js_ver_citas">(Ver horarios)</a></label>
      {{$drop_tecnicos}}
      <span class="error error_tecnico"></span>
    </div>
    <div class="col-sm-4">
      <label for="">Hora inicio trabajo</label>
      {{$input_hora_inicio}}
      <span class="error error_hora_inicio"></span>
    </div>
    <div class="col-sm-4">
      <label for="">Hora fin trabajo</label>
      {{$input_hora_fin}}
      <span class="error error_hora_fin"></span>
      <br>
    </div>
  </div> <!-- 0 -->
  @endif
  <div class="row">
    <div class="col-sm-8">
      <label for="">Comentarios</label>
      {{$input_comentarios}}
      <div class="error error error_comentario"></div>
    </div>
  </div>
  <br>
  <div id="div_refacciones"></div>
</form>
<div class="row pull-right">
  <div class="col-sm-12 ">
    {{$btn_guardar}}
  </div>
</div>
<br><br>
@endsection

@section('scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<script>
  var site_url = "{{site_url()}}";
  var origen = "{{$origen}}";
  var id_save = "{{$input_id}}";
  var perfil = "{{$this->session->userdata('tipo_perfil')}}";
  var fecha_cita = $("#fecha").val();
  var fecha_comparar = "{{date('Y-m-d')}}";
  var proactivo_nueva_unidad = "{{$proactivo_nueva_unidad}}";
  var magic = "{{$magic}}";
  var id_status_cita_asesor = "{{$id_status_cita_asesor}}";
  var realizo_prepiking = "{{$realizo_prepiking}}";
  var reagendada = "{{$reagendada}}";
  var id_instalacion = "{{$id_instalacion}}";
  if(id_status_cita_asesor!=1 && id_save!=0 && reagendada==0 && id_instalacion==0){
    desabilitarAsesor();
  }
  if(realizo_prepiking==1){
    $("#id_prepiking option:not(:selected)").attr("disabled", true);
    $("#id_modelo_prepiking option:not(:selected)").attr("disabled", true);
  }
  $(".busqueda").select2();

  if(proactivo_nueva_unidad!=0){
    var url=site_url+"/citas/datosClienteUnidadesNuevasById";
    ajaxJson(url,{"id":proactivo_nueva_unidad},"POST","",function(result){
      result = JSON.parse( result );
      $("#vehiculo_numero_serie").val(result.datos.serie);
      $("#datos_nombres").val(result.datos.cliente);
      $("#datos_telefono").val(result.datos.telefono);
      $("#email").val(result.datos.correo);
      $("#vehiculo_numero_serie").trigger('change');
    });
  }
  //Cambiar cuando se haga el de nosotros de 6 meses
  if(magic!=0){
    var url=site_url+"/citas/getCitaId";
    ajaxJson(url,{"id_cita":magic},"POST","",function(result){
      result = JSON.parse( result );
      $("#datos_nombres").val(result.datos.datos_nombres);
      $("#datos_apellido_paterno").val(result.datos.datos_apellido_paterno);
      $("#datos_apellido_materno").val(result.datos.datos_apellido_materno);
      $("#vehiculo_placas").val(result.datos.vehiculo_placas);
      $("#email").val(result.datos.email);
      $("#datos_telefono").val(result.datos.datos_telefono);
      $("#vehiculo_numero_serie").val(result.datos.vehiculo_numero_serie);
      $("#vehiculo_anio").val(result.datos.vehiculo_anio);
      $("#vehiculo_modelo").val(result.datos.vehiculo_modelo);
      $("#id_color").val(result.datos.id_color);
    });
  }
  // if(magic!=0){
  //   var url=site_url+"/proactivo/getById";
  //   ajaxJson(url,{"id":magic},"POST","",function(result){
  //     result = JSON.parse( result );
  //     $("#datos_nombres").val(result.datos.cnombre);
  //     $("#datos_apellido_paterno").val(result.datos.cap);
  //     $("#datos_apellido_materno").val(result.datos.cam);
  //     $("#vehiculo_kilometraje").val(result.datos.kilometraje);
  //     $("#vehiculo_placas").val(result.datos.placas);
  //     $("#email").val(result.datos.correo);
  //     $("#datos_telefono").val(result.datos.celular);
  //     $("#vehiculo_numero_serie").val(result.datos.serie);
  //     $("#vehiculo_anio").val(result.datos.modelo);
  //     $("#vehiculo_modelo").val(result.datos.modelo);
  //   });
  // }
  if(id_save!=0){
    if(perfil==2){
      $("input").prop('readonly', true);
      $("select").prop('disabled', true);
      $("#asignar_tecnico").prop('disabled', true);
      $("#vehiculo_placas").prop('readonly', false);
      $("#vehiculo_numero_serie").prop('readonly', false);
      $("#vehiculo_kilometraje").prop('readonly', false);
      $("#email").prop('readonly', false);
    }
  }
  // lo quité
  // if(id_instalacion!=0){
  //   $("#id_status_color").val(2);
  //   $("#id_status_color option:not(:selected)").attr("disabled", true);
  //   var url=site_url+"/dms/getInfoByUnidad";
  //   ajaxJson(url,{"id":id_instalacion},"POST","",function(result){
  //     result = JSON.parse( result );
  //     $("#datos_nombres").val(result.datos[0].nombre_cliente);
  //     $("#datos_apellido_paterno").val(result.datos[0].cliente_apellido_paterno);
  //     $("#datos_apellido_materno").val(result.datos[0].cliente_apellido_materno);
  //     $("#email").val(result.datos[0].correo_electronico);
  //     $("#datos_telefono").val(result.datos[0].telefono);
  //     $("#vehiculo_numero_serie").val(result.datos[0].no_serie);
  //     $("#vehiculo_anio").val(result.datos[0].nombre_anio);
  //     $("#vehiculo_modelo").val(result.datos[0].modelo_id);
  //     $("#id_color").val(result.datos[0].color_id);
  //     getPresupuestos();
  //   });
  // }
  var input_ocupado = "{{ $input_realizo_servicio }}";
  var tecnico_actual = "{{$input_id_tecnico_actual}}";
  $(".busqueda").select2();
  if(input_ocupado==1){
    state = true;
  }else{
    state = false;
  }
  $("#asesor").on("change",function(){
    var url=site_url+"/citas/getfechas";
    $("#fecha").empty();
    $("#fecha").append("<option value=''>-- Selecciona --</option>");
    $("#fecha").attr("disabled",true);
    $("#horario").empty();
    $("#horario").append("<option value=''>-- Selecciona --</option>");
    $("#horario").attr("disabled",true);
    asesor=$(this).val();
    if(asesor!=''){
      ajaxJson(url,{"asesor":asesor},"POST","",function(result){
        if(result.length!=0){
          $("#fecha").empty();
          $("#fecha").removeAttr("disabled");
          result=JSON.parse(result);
          $("#fecha").append("<option value=''>-- Selecciona --</option>");
          $.each(result,function(i,item){
            var fecha_separada = result[i].fecha.split('-');
            $("#fecha").append("<option value= '"+result[i].fecha+"'>"+fecha_separada[2]+'/'+fecha_separada[1]+'/'+fecha_separada[0]+"</option>");
          });
        }else{
          $("#fecha").empty();
          $("#fecha").append("<option value='0'>No se encontraron datos</option>");
        }
      });
    }else{
      $("#fecha").empty();
      $("#fecha").append("<option value=''>-- Selecciona --</option>");
      $("#fecha").attr("disabled",true);

      $("#tecnicos").empty();
      $("#tecnicos").append("<option value=''>-- Selecciona --</option>");
      $("#tecnicos").attr("disabled",true);
      $("#hora_inicio").val('');
      $("#hora_fin").val('');
    }
  }); 

  $("#fecha").on("change",function(){
    var url=site_url+"/citas/getHorarios";
    fecha=$(this).val();
    asesor=$("#asesor").val();
    $("#hora_inicio").val('');
    $("#hora_fin").val('');
    if(fecha!='' && asesor!=''){
      ajaxJson(url,{"asesor":asesor,"fecha":fecha,"id_horario":$("#id_horario").val()},"POST","",function(result){
        if(result.length!=0){
          $("#horario").empty();
          $("#horario").removeAttr("disabled");
          result=JSON.parse(result);
          $("#horario").append("<option value=''>-- Selecciona --</option>");
          $.each(result,function(i,item){
            $("#horario").append("<option value= '"+result[i].id+"'>"+result[i].hora.substring(0,5)+"</option>");
          });
          var id_cita = $("#id").val();
          if(id_cita ==0){
            getTecnicos();

          }
        }else{
          $("#horario").empty();
          $("#horario").append("<option value='0'>No se encontraron datos</option>");
        }
      });
    }else{
      $("#horario").empty();
      $("#horario").append("<option value=''>-- Selecciona --</option>");
      $("#horario").attr("disabled",true);

      $("#tecnicos").empty();
      $("#tecnicos").append("<option value=''>-- Selecciona --</option>");
      $("#tecnicos").attr("disabled",true);
      $("#hora_inicio").val('');
      $("#hora_fin").val('');

      $("#tecnicos_dias").empty();
      $("#tecnicos_dias").append("<option value=''>-- Selecciona --</option>");
      $("#tecnicos_dias").attr("disabled",true);
    }
  }); 
  $("#guardar").on('click',function(){
    $("#guardar").prop('disabled',true);
    if(perfil==2 && id_save!=0){
      var url = site_url+'/citas/cambiar_comentario_servicio/0';
      ajaxJson(url,{"id":$("#id").val(),"comentario":$("#comentarios_servicio").val(),"vehiculo_placas":$("#vehiculo_placas").val(),"vehiculo_numero_serie":$("#vehiculo_numero_serie").val(),"vehiculo_kilometraje":$("#vehiculo_kilometraje").val(),"email":$("#email").val()},"POST","",function(result){
        if(isNaN(result)){
          data = JSON.parse( result );
          $.each(data, function(i, item) {
            $(".error_"+i).empty();
            $(".error_"+i).append(item);
            $(".error_"+i).css("color","red");
          });
        }else{
          if(result==1){
            ExitoCustom("Comentario actualizado correctamente",function(){
              window.location.href = site_url+'/citas/ver_citas';
            });
          }
        }
      });
    }else{
      var url = site_url+'/citas/agendar_cita/0';
      if(verificarFormatoHoras()){
        if(validarHorasIguales()){
          ajaxJson(url,$("#frm").serialize(),"POST","async",function(result){
            if(isNaN(result)){
              data = JSON.parse( result );
              $.each(data, function(i, item) {
                $(".error_"+i).empty();
                $(".error_"+i).append(item);
                $(".error_"+i).css("color","red");
              });
            }else{
              if(result==1){
                ExitoCustom("Guardado correctamente",function(){
                  window.location.href = site_url+'/citas/ver_citas';
                });
              }else if(result==-1){
                ErrorCustom('El horario ya fue ocupado, por favor intenta con otro');
              }else if(result==-2){
                ErrorCustom('La fecha de asignación al técnico debe ser mayor o igual a la fecha del asesor');
              }else if(result==-3){
                ErrorCustom('El técnico no labora en la hora seleccionada');
              }else if(result==-4){
                ErrorCustom('El horario del asesor ya fue ocupado, por favor elige otro');
              }else if(result==-5){
                ErrorCustom('El técnico no labora en ese horario (hora de comida)');
              }else{
                ErrorCustom('No se pudo guardar, intenta otra vez.');
              }
            }
          });
        }else{
          ErrorCustom("La hora de inicio de trabajo y hora fin NO pueden ser iguales");
        }
      }else{
        ErrorCustom("Es necesario revises el formato de las horas");
      }
    }
    $("#guardar").prop('disabled',false);
  });




  $("#tecnicos").on("change",function(){
    getHorariosTecnicos();
    if(id_save!=0&&reagendada!=1){
      mostrarModalHorarios();
    }
  });
  if(tecnico_actual!=0){
    getHorariosTecnicos()
  }
  $("#asignar_tecnico").on("click",function(e){
    e.preventDefault();
    mostrarModalHorarios();
  }); 
  function mostrarModalHorarios(){
    if($("#tecnicos").val()!=''){
      var url =site_url+"/citas/modal_asignar_tecnico/0";
      customModal(url,{"id":$("#id").val(),'id_tecnico':$("#id_tecnico_actual").val(),'id_tecnico_seleccionado':$("#tecnicos").val()},"GET","lg",guardarTecnico,cancelarTecnico,"Guardar","Cancelar","Asignar hora a técnico","modal1");
    }else{
      ErrorCustom("Es necesario seleccionar el técnico.");
    }
  }
  function getHorariosTecnicos(){
    tecnico=$("#tecnicos").val();
    var url = site_url+'/citas/getHorariosAsesor/';
    if(tecnico!=''){
      if($("#fecha").val()!=''){ 
        ajaxJson(url,{"id_tecnico":tecnico,'fecha':$("#fecha").val()},"POST","",function(result){

          if(result.length!=0){
            result=JSON.parse(result);
            $("#hora_inicio_trabajo").text(result[0].hora_inicio);
            $("#hora_fin_trabajo").text(result[0].hora_fin);
            $("#hora_inicio_comida").text(result[0].hora_inicio_comida);
            $("#hora_fin_comida").text(result[0].hora_fin_comida);

          }else{
            $("#hora_inicio_trabajo").text('');
            $("#hora_fin_trabajo").text('');
            $("#hora_inicio_comida").text('');
            $("#hora_fin_comida").text('');
          }
        });
      }else{
        if(origen == 'normal'){
          ErrorCustom("Es necesario seleccionar la fecha");
        }
      }
    }else{
      $("#hora_inicio_trabajo").text('');
      $("#hora_fin_trabajo").text('');
      $("#hora_inicio_comida").text('');
      $("#hora_fin_comida").text('');
    }
  }
  function cancelarTecnico(){
    $("#tecnicos").val($("#id_tecnico_actual").val());
  }





  function guardarTecnico(){
    var url = site_url+'/citas/modal_asignar_tecnico/';
    if(verificarFormatoHoras()){
      if(validarHorasIguales()){
        ajaxJson(url,{"id":$("#id").val(),'id_tecnico':$("#tecnicos").val(),'hora_inicio':$("#hora_inicio").val(),'hora_fin':$("#hora_fin").val(),'fecha':$("#fecha").val(),"id_detalle":$("#id_detalle").val(),"dia_completo":$("#dia_completo").val(),"fecha_inicio":$("#fecha_inicio").val(),"fecha_fin":$("#fecha_fin").val(),"fecha_parcial":$("#fecha_parcial").val(),"hora_inicio_extra":$("#hora_inicio_extra").val(),"hora_fin_extra":$("#hora_fin_extra").val(),"fecha_reasignar":$("#fecha_reasignar").val(),"hora_comienzo":$("#hora_comienzo").val(),"reagendada":$("#reagendada").val()},"POST","",function(result){

          if(isNaN(result)){
            data = JSON.parse( result );
            $.each(data, function(i, item) {
              $(".error_"+i).empty();
              $(".error_"+i).append(item);
              $(".error_"+i).css("color","red");
            });
          }else if(result==-1){
            ErrorCustom("El técnico ya tiene asignada una cita en ese horario");
          }else if(result==0){
            ErrorCustom("Error al asignar el técnico, por favor intenta de nuevo.");
          }else if(result==-2){
            ErrorCustom('La fecha de asignación al técnico debe ser mayor o igual a la fecha del asesor');
          }else if(result==-3){
            ErrorCustom('El técnico no labora en la hora seleccionada');
          }else{
            ExitoCustom("Horario asignado correctamente",function(){
              $("#id_tecnico_actual").val($("#tecnicos").val());
              $(".close").trigger("click");
            });
          }
        });
      }else{
        ErrorCustom("La hora de inicio de trabajo y hora fin NO pueden ser iguales");
      }
    }else{
      ErrorCustom("Es necesario revises el formato de las horas");
    }
  } 
  function getTecnicos(){
    var url=site_url+"/citas/getTecnicosByFecha";
    fecha=$("#fecha").val();
    if(fecha!=''){
      ajaxJson(url,{"fecha":fecha},"POST","",function(result){
        if(result.length!=0){
          $("#tecnicos").empty();
          $("#tecnicos").removeAttr("disabled");
          result=JSON.parse(result);
          $("#tecnicos").append("<option value=''>-- Selecciona --</option>");
          $.each(result,function(i,item){
            $("#tecnicos").append("<option value= '"+result[i].id_tecnico+"'>"+result[i].nombre+"</option>");
          });
        }else{
          $("#tecnicos").empty();
          $("#tecnicos").append("<option value='0'>No se encontraron datos</option>");
        }
      });
      getTecnicos_dias();
    }else{
      $("#tecnicos").empty();
      $("#tecnicos").append("<option value=''>-- Selecciona --</option>");
      $("#tecnicos").attr("disabled",true);

      $("#hora_inicio").val('');
      $("#hora_fin").val('');

    }
  }
  function getTecnicos_dias(){
    var url=site_url+"/citas/getTecnicosByFecha";
    fecha=$("#fecha").val();
    if(fecha!=''){
      ajaxJson(url,{"fecha":fecha},"POST","",function(result){
        if(result.length!=0){
          $("#tecnicos_dias").empty();
          $("#tecnicos_dias").removeAttr("disabled");
          result=JSON.parse(result);
          $("#tecnicos_dias").append("<option value=''>-- Selecciona --</option>");
          $.each(result,function(i,item){
            $("#tecnicos_dias").append("<option value= '"+result[i].id_tecnico+"'>"+result[i].nombre+"</option>");
          });
        }else{
          $("#tecnicos_dias").empty();
          $("#tecnicos_dias").append("<option value='0'>No se encontraron datos</option>");
        }
      });
    }else{
      $("#tecnicos_dias").empty();
      $("#tecnicos_dias").append("<option value=''>-- Selecciona --</option>");
      $("#tecnicos_dias").attr("disabled",true);

      $("#hora_inicio_dias").val('');
      $("#hora_fin_dias").val('');

    }
  }

  $(".js_dudas").on('click',function(){
    if($(this).prop('checked')){
      $(this).val(1);
    }else{
      $(this).val(1);
    }
  });
  $("body").on('change','#vehiculo_placas',function(){
    var url=site_url+"/citas/getDatosByPlaca";
    var id_comparar = $("#id").val();
    if(id_comparar==0){
      ajaxJson(url,{"placas":$(this).val()},"POST","",function(result){
        result=JSON.parse(result);
        if(result.exito){
          $(".busqueda").select2('destroy');
          $("#email").val(result.data.datos_email);
          $("#datos_nombres").val(result.data.datos_nombres);
          $("#datos_apellido_paterno").val(result.data.datos_apellido_paterno);
          $("#datos_apellido_materno").val(result.data.datos_apellido_materno);
          $("#datos_telefono").val(result.data.datos_telefono);
          $("#vehiculo_anio option[value='"+result.data.vehiculo_anio+"']").attr("selected",true);
          $("#vehiculo_modelo option[value='"+result.data.vehiculo_modelo+"']").attr("selected",true);
          $("#id_color option[value='"+result.data.id_color+"']").attr("selected",true);
          $(".busqueda").select2();
          $("#vehiculo_modelo").trigger('change');
          $("#submodelo_id").val(result.data.submodelo_id);

        }else{
          getDataDMS('placas',$("#vehiculo_placas").val());
        }
        buscar_serie();
      });
    }
  })
  $("#vehiculo_anio").on("change",function(){
    if($("#id_modelo_prepiking").val()!==''){
      $("#id_modelo_prepiking").select2('destroy')
      $("#id_modelo_prepiking").val('')
      $("#id_modelo_prepiking").select2()
    }
  });
  $("#id_modelo_prepiking").on("change",function(){
    var url ="<?php echo site_url();?>/citas/getPrepiking";
    $(".busqueda").select2('destroy');

    $("#id_prepiking").empty();
    $("#id_prepiking").append("<option value=''>-- Selecciona --</option>");
    $("#id_prepiking").attr("disabled",true);

    if($(this).val()!=0 && $("#vehiculo_anio").val()){
      ajaxJson(url,{"id_modelo_prepiking":$(this).val(),"vehiculo_anio":$("#vehiculo_anio").val()},"POST","",function(result){
        if(result.length !=0){
          $("#id_prepiking").empty();
          $("#id_prepiking").removeAttr("disabled");
          result=JSON.parse(result);
          $("#id_prepiking").append("<option value=''>-- Selecciona --</option>");
          $.each(result, function(i, item) {
            $("#id_prepiking").append("<option value= '" + result[i].servicio + "'>" + result[i].servicio + "</option>");
          });
        }else{
          $("#id_prepiking").empty();
          $("#id_prepiking").append("<option value='0'>No se encontraron datos</option>");
        }
      });
    }else{
      $("#id_prepiking").empty();
      $("#id_prepiking").append("<option value=''>-- Selecciona --</option>");
      $("#id_prepiking").attr("disabled",true);

    }
    $(".busqueda").select2();
  });
  $("#vehiculo_modelo").on("change",function(){
    var url ="<?php echo site_url();?>/citas/getSubmodelo";

    $("#submodelo_id").empty();
    $("#submodelo_id").append("<option value=''>-- Selecciona --</option>");
    $("#submodelo_id").attr("disabled",true);

    if($(this).val()!=0 && $("#vehiculo_modelo").val()){
      ajaxJson(url,{"id_modelo_prepiking":$(this).val(),"vehiculo_modelo":$("#vehiculo_modelo").val()},"POST","",function(result){
        if(result.length !=0){
          $("#submodelo_id").empty();
          $("#submodelo_id").removeAttr("disabled");
          result=JSON.parse(result);
          $("#submodelo_id").append("<option value='0'>-- Selecciona --</option>");
          $.each(result, function(i, item) {
            $("#submodelo_id").append("<option value= '" + result[i].id + "'>" + result[i].nombre + "</option>");
          });
        }else{
          $("#submodelo_id").empty();
          $("#submodelo_id").append("<option value='0'>No se encontraron datos</option>");
        }
      });
    }else{
      $("#submodelo_id").empty();
      $("#submodelo_id").append("<option value=''>-- Selecciona --</option>");
      $("#submodelo_id").attr("disabled",true);

    }
  });
  $(".addModelo").on('click',function(){
    var url =site_url+"/citas/agregar_modelo_prepiking_modal/0";
    customModal(url,{},"GET","md",agregarModelo,"","Guardar","Cancelar","Agregar modelo","modalCategoria");
  });
  function agregarModelo(){
    var url =site_url+"/citas/agregar_modelo_prepiking_modal";
    ajaxJson(url,{"modelo":$("#modelo").val()},"POST","async",function(result){
      result = JSON.parse( result );
      if(!result.exito){
        $.each(result.errors, function(i, item) {
          $(".error_"+i).empty();
          $(".error_"+i).append(item);
          $(".error_"+i).css("color","red");
        });
      }else{
        ExitoCustom("Modelo agregado correctamente",function(){
          $("#id_modelo_prepiking").append("<option value='"+result.id+"'>"+result.modelo+"</option>");
          $("#id_modelo_prepiking").val(result.id);
          $(".close").trigger('click');
          $("#id_modelo_prepiking").trigger('change');
        });
      }
    });
  }
  $("body").on('change',"#vehiculo_numero_serie",function(){
    ValidarCampania();
    getPresupuestos();
    getDataDMS('vin',$(this).val());
  });
  function desabilitarAsesor(){
    $("#asesor option:not(:selected)").attr("disabled", true);
    $("#fecha option:not(:selected)").attr("disabled", true);
    $("#horario option:not(:selected)").attr("disabled", true);
  }
  //
  if(id_save!=0){
		//$("#tecnicos option:not(:selected)").attr("disabled", true);
	}

</script>
<script src="<?php echo base_url()?>js/citas.js"></script>
<script src="<?php echo base_url()?>js/dms/generico.js"></script>
@endsection