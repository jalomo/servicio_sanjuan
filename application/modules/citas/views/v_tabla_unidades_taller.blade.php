<table id="tbl_unidades_taller" class="table table-hover table-striped">
	<thead>
		<tr class="tr_principal">
			<th>Cita</th>
			<th>Nombre del cliente</th>
			<th>Teléfono del cliente</th>
			<th>Placas</th>
			<th>Modelo</th>
			<th>Fecha recepción orden</th>
			<th>Cita previa</th>
			<th>Asesor</th>
			<th>Fecha asesor</th>
		</tr>
	</thead>
	<tbody>
		@foreach($unidades as $c => $value)
		<tr>
			<td>{{$value->id_cita}}</td>
			<td>{{$value->datos_nombres.' '.$value->datos_apellido_paterno.' '.$value->datos_apellido_materno}}</td>
			<td>{{$value->datos_telefono}}</td>
			<td>{{$value->vehiculo_placas}}</td>
			<td><span title="{{$value->vehiculo_modelo}}">{{trim_text($value->vehiculo_modelo,1)}}</span></td>
			<td>{{$value->fecha_recepcion}}</td>
			<td>{{($value->cita_previa==1)?'Si':'No'}}</td>
			<td>{{$value->asesor}}</td>
			<td>{{$value->fecha_asesor.' '.$value->hora_asesor}}</td>
		</tr>
		@endforeach
	</tbody>
</table>