@layout('tema_luna/layout')
@section('contenido')
	<form action="{{site_url('citas/saveVideoWeb')}}" method="POST" id="frm" enctype="multipart/form-data">
		<div class="row">
			<div class="col-sm-3 col-sm-offset-1">
				<label for="">Cita</label>
				<input required="" type="text" class="form-control" name="id_cita" id="cita" value="">
				<br>
				
			</div>
			<div class="col-sm-4">
				<label for="">Video</label> <br>
				<input type="file" name="video" id="video">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-7">
				<label for="">Comentario</label>
				<textarea class="form-control" name="comentario" id="comentario" cols="30" rows="10"></textarea>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-4">
				<button type="button" id="guardar" class="btn btn-success">Guardar</button>
			</div>
		</div>
		<br>
	</form>
	
@endsection
@section('scripts')

<script>
	var site_url = "{{site_url()}}";
	var id_cita = '';
	var accion = '';
	 $("body").on("click",'#guardar',validarCita);
	function validarCita(){
		var url =site_url+"/citas/anexarVideo/";
		var id_cita =$("#cita").val();
		var video =$("#video").val();
		if(id_cita=='' || video == ''){
			ErrorCustom('El campo de video o la cita son requeridos');
		}else{
			ajaxJson(url,{"id":$("#cita").val()},"POST","",function(result){
			if(result ==0){
						ErrorCustom('La cita no existe');
					}else{
						$("#frm").submit();

					}
			});
		}
		
	}

</script>
@endsection