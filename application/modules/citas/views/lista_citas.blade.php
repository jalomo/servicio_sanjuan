a@layout('tema_luna/layout')
@section('contenido')
<input type="hidden" id="id" name="id" value="{{$id}}">
<input type="hidden" id="id_cita" name="id_cita" value="">
<div class="row">
  <div class="col-sm-12">
    <span style="font-size: 30px;font-weight: bold;text-align: center;">Lista de Horarios de {{$nombre_operador}}</span>
    <button id="regresar" class="btn btn-default pull-right">Regresar</button>
  </div>
</div>
<br>
<div class="row form-group">
  <div class='col-sm-4'>
    <label for="">Fecha</label>
    <input id="horario" name="horario" type='date' class="form-control" />
    <span class="error error_horario"></span>
  </div>
  <div class="col-sm-4">
    <label for="">Hora</label>
    <input id="hora" name="hora" type='time' class="form-control" />
    <span class="error error_hora"></span><br>
    <button id="guardar" class="btn btn-success pull-right">Guardar</button>
  </div>
</div>
<div class="row form-group">
  <div class="col-sm-2"></div>
  <div class="col-sm-8">
    <div class="row">
      <div class="col-sm-4">
        <label for="">Buscar por fecha</label>
        <input type="date" name="buscar_fecha" id="buscar_fecha" value="{{$fecha}}" class="form-control">
        <span class="error error_fecha_inicio"></span>
      </div>
      <div class="col-sm-1">
        <br>
        <button id="btn_por_fecha" style="margin-top: 10px;" class="btn btn-success">Buscar</button>
      </div>
      <div class="col-sm-5 text-right">
        <br>
        <button id="guardar_todo" style="margin-top: 10px;" class="btn btn-success">Desactivar todo</button>
      </div>
    </div>
    <br>
    <form action="" id="frm-horarios">
      <input type="hidden" id="motivo_desactivar" name="motivo_desactivar">
      <div id="div_tabla_horarios">
        <table class="table table-bordered table-striped" cellpadding="0" width="100%">
          <thead>
            <tr>
              <th><input type="checkbox" name="seleccionar_todo" id="seleccionar_todo"></th>
              <th style="text-align: center;">Hora</th>
              <th style="text-align: center;">Acciones</th>
            </tr>
          </thead>
          <tbody>
            @if(count($citas)>0)
            @foreach($citas as $key => $value)
            <tr>
              <td colspan="3">
                <strong>Fecha: {{date_eng2esp_1($key)}}</strong>
              </td>
            </tr>
            @foreach($value as $k => $cita)
            <tr>
              <td>
                @if($cita->activo)
                <input class="js_seleccion check_{{$cita->id}}; ?>" type="checkbox" name="seleccion[{{$cita->id}}]" id="seleccion-{{$cita->id}}" data-valor="0" data-id="{{$cita->id}}">
                @endif
              </td>
              <td>{{substr($cita->hora,0,5)}}</td>
              <td style="text-align: center;">
                <?php
                $horario = date_eng2esp_1($cita->fecha);
                $hora = substr($cita->hora,0,5);

                ?>
                <a href="" data-id="{{$cita->id}}" class="js_editar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar" data-horario="{{date2sql($horario)}}" data-hora="{{$hora}}">
                  <i class="pe pe-7s-note"></i>
                </a>
                @if($cita->activo)
                <a href="" data-id="{{$cita->id}}" class="js_activar" data-valor="0" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Desactivar horario">
                  <i class="pe-7s-switch"></i>
                </a>
                @else
                <a href="" data-id="{{$cita->id}}" class="js_activar" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activar horario">
                  <i class="pe-7s-check"></i>
                </a>
                @endif
                @if($cita->motivo!='')
                <a class="fas fa-comments" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="{{$cita->motivo}}"></a>
                @endif
              </td>
            </tr>
            @endforeach
            @endforeach
            @else
            <tr style="text-align: center">
              <td colspan="3">No hay registros para mostrar...</td>
            </tr>
            @endif

          </tbody>
        </table>
      </div>
    </div>
  </div>
</form>
@endsection

@section('scripts')
    <script type="text/javascript">
  var site_url = "{{site_url()}}";
  var valor = '';
  var mensaje = '';
  var id = '';
   if($('.js_seleccion').length==0){
      $("#guardar_todo").hide();
    }
  $("body").on('click',"#seleccionar_todo",function(){
      $.each( $('.js_seleccion'), function( key, value ) {
        if($('#seleccionar_todo').is(':checked')){
          $(this).prop('checked','checked');
        }else{
           $(this).prop('checked','');
        }
      });
    });
   $("body").on('click','.js_seleccion',function(){
      var cantidad_total=$('.js_seleccion').length;
      var cantidad_check=$('.js_seleccion:checked').length;
      if(cantidad_total==cantidad_check){
        $("#seleccionar_todo").prop("checked",true);
      }else{
        $("#seleccionar_todo").prop("checked",false);
      }
    })
    $("body").on('click',"#guardar_todo",function(){
         ConfirmCustom("¿Está seguro de desactivar todos los horarios seleccionados?", agregar_motivo,"", "Confirmar", "Cancelar");
    });
    function agregar_motivo(){
     var url =site_url+"/citas/agregar_motivo/";
      customModal(url,{},"GET","md",guardar_todo,"","Guardar","Cancelar","Motivo para desactivar","modal3");
    }
    function guardar_todo(){
      var cantidad_total=$('.js_seleccion').length;
      var cantidad_check=$('.js_seleccion:checked').length;
      $("#motivo_desactivar").val($("#motivo").val());
     if(cantidad_check==0){
        ErrorCustom('Es necesario seleccionar por lo menos un registro');
      }else{
        contador_parciales = 0;
        var url =site_url+"/citas/desactivar_horarios_asesores/";
          ajaxJson(url,$("#frm-horarios").serialize(),"POST","",function(result){
              ExitoCustom("Todos los horarios seleccionados fueron desactivados con éxito",function(){
                $('.close').trigger('click');
               buscar();
              });
          });

      }
    }
  $('.clockpicker').clockpicker();
    $(function () {
        $('#datetimepicker1').datetimepicker({
          minDate: moment(),
          format: 'DD/MM/YYYY',
          icons: {
                time: "far fa-clock",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
          },
           locale: 'es'
        });
    });
    $('.date').datetimepicker({
      format: 'DD/MM/YYYY',
      icons: {
          time: "far fa-clock",
          date: "fa fa-calendar",
          up: "fa fa-arrow-up",
          down: "fa fa-arrow-down"
      },
       locale: 'es'
    });
    $("#regresar").on('click',function(){
      window.location.href = "{{base_url('citas')}}";
    });
    $("#btn_por_fecha").on('click',function(){
    var url =site_url+"/citas/tabla_horarios";
        ajaxLoad(url,{"id":$("#id").val(),'fecha':$("#buscar_fecha").val()},"div_tabla_horarios","POST",function(){
          $('[data-toggle="tooltip"]').tooltip();
          $('[data-toggle="tooltip"]').tooltip()
          if($('.js_seleccion').length==0){
            $("#guardar_todo").hide();
          }else{
            $("#guardar_todo").show();
          }
        });
    });
     $("#guardar").on('click',function(){
      callbackGuardar();
    });
      $("body").on("click",'.js_editar',function(e){
      e.preventDefault();
        $("#id_cita").val($(this).data('id')) ;
        $("#horario").val($(this).data('horario')) ;
        $("#hora").val($(this).data('hora')) ;

      });
      $("body").on("click",'.js_activar',function(e){
       e.preventDefault();
       //valor es 0 cuando va desactivar y 1 cuando lo va activar
       valor = $(this).data('valor');
       if(valor==1){
        mensaje ="¿Está seguro de activar el horario?";
       }
       id = $(this).data('id');
       if(valor==1){
         ConfirmCustom(mensaje, callbackActivarDesactivar,"", "Confirmar", "Cancelar");
        }else{
           var url =site_url+"/citas/agregar_motivo/";
          customModal(url,{},"GET","md",callbackActivarDesactivar,"","Guardar","Cancelar","Motivo para desactivar","modal3");
        }
      
      
    });
    function buscar(){
    var url =site_url+"/citas/tabla_horarios";
        ajaxLoad(url,{"id":$("#id").val(),'fecha':$("#buscar_fecha").val()},"div_tabla_horarios","POST",function(){
          $('[data-toggle="tooltip"]').tooltip()
          if($('.js_seleccion').length==0){
            $("#guardar_todo").hide();
          }else{
            $("#guardar_todo").show();
          }
      });
  }
  function callbackGuardar(){
    var url =site_url+"/citas/agregar_horario";
    var id_cita = $("#id_cita").val();
    ajaxJson(url,{"horario":$("#horario").val(),"id":$("#id").val(),"id_cita":id_cita,"hora":$("#hora").val()},"POST","",function(result){
      if(isNaN(result)){
        data = JSON.parse( result );
        //Se recorre el json y se coloca el error en la div correspondiente
        $.each(data, function(i, item) {
           $.each(data, function(i, item) {
                      $(".error_"+i).empty();
                      $(".error_"+i).append(item);
                      $(".error_"+i).css("color","red");
                  });
        });
      }else{
        if(result <0){
          ErrorCustom('Ya fue registrado el horario, por favor intenta con otro');
        }else{
          if(result==0){
            ErrorCustom('No se pudo guardar el horario, por favor intenta de nuevo');
          }else{
            ExitoCustom("Horario guardado correctamente",function(){
            $(".close").trigger("click");
              buscar();
              if(id_cita!=''){
                $("#horario").val("") ;
              }
                  //$("#horario").val($(this).data('horario')) ;
                  $("#hora").val("") ;
                  $("#id_cita").val($(this).data('id')) ;
            });
          }
        }
      }
    });
  }
  function callbackActivarDesactivar(){

    if(valor==1){
       mensaje ="Horario activado correctamente";
      activar_desactivar();
    }else{
       mensaje ="Horario desactivado correctamente";
      var motivo = $("#motivo").val();
      if(motivo==''){
        ErrorCustom("Es necesario ingresar el motivo por el que se desactiva al horario");  
      }else{
         mensaje ="horario desactivado correctamente";
        activar_desactivar();
      }
      
    }
    
  }
  function activar_desactivar(){
    var url =site_url+"/citas/cambiar_status/";
    ajaxJson(url,{"id":id,"valor":valor,"tabla":'aux',"motivo":$("#motivo").val()},"POST","",function(result){
      if(result ==0){
          ErrorCustom('Error al activar o desactivar el horario, por favor intenta de nuevo');
        }else{
          ExitoCustom(mensaje,function(){
          $(".close").trigger("click");
            buscar();
          }); 
        }
    });
  }
</script>
@endsection