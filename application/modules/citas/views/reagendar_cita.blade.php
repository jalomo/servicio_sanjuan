@layout('tema_luna/layout')
@section('contenido')

	<ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="#">Inicio</a>
	    </li>
	    <li class="breadcrumb-item active">Agendar cita</li>
  	</ol>
  	<div class="row">
  		<div class="col-sm-8">
  			<h2>Agendar Citas</h2>
  		</div>
  	</div>
  	
  	  
  	<hr>
  	<h3>Datos del Vehículo</h3>
  	<form action="" method="POST" id="frm">
  		<input type="hidden" name="id" id="id" value="{{$input_id}}">
  		<input type="hidden" name="id_horario" id="id_horario" value="{{$input_id_horario}}">
	 	 
      <input type="hidden" name="id_tecnico_actual" id="id_tecnico_actual" value="{{$input_id_tecnico_actual}}">
  		
  	<div class="row">
  		<div class="col-sm-4 form-group">
  			<label for="">Transporte</label>
  			{{$drop_transporte}}
  			<div class="error error_transporte"></div>
  		</div>
  		<div class="col-sm-4 form-group">
  			<label for="">Año del vehículo</label>
  			{{$drop_vehiculo_anio}}
  			<div class="error error_vehiculo_anio"></div>
  		</div>
  		<div class="col-sm-4 form-group">
  			<label for="">Placas</label>
  			{{$input_vehiculo_placas}}
  			<div class="error error_vehiculo_placas"></div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-sm-4 form-group">
  			<label for="">Modelo</label>
  			{{$drop_vehiculo_modelo}}
  			<div class="error error_vehiculo_modelo"></div>
  		</div>
  		<div class="col-sm-4 form-group">
  			<label for="">Número de serie</label>
  			{{$input_vehiculo_numero_serie}}
  			<div class="error error_numero_serie"></div>
  		</div>
  		<div class="col-sm-4 form-group">
  			<label for="">Asesor</label>
  			{{$drop_asesor}}
  			<div class="error error_asesor"></div>
  		</div>
  	</div>

  	<div class="row">
		<div class="col-sm-4 form-group">
  			<label for="">Fecha</label>
  			{{$drop_fecha}}
  			<div class="error error_fecha"></div>
  		</div>
  		<div class="col-sm-4 form-group">
  			<label for="">Horario</label>
  			{{$drop_horario}}
  			<div class="error error_horario"></div>
  		</div>
      <div class="col-sm-2 form-group">
        <label for="">Donde realizó la cita</label>
        {{$drop_opcion}}
        <div class="error error_id_opcion_cita"></div>
      </div>
  	</div>
    <div class="row">
       <div class="col-sm-3 form-group">
        <label for="">Color</label>
        {{$drop_color}}
        <div class="error error_id_color"></div>
      </div>
       <div class="col-sm-3 form-group">
        <label for="">Estatus</label>
        {{$drop_id_status_color}}
        <div class="error error_id_status_color"></div>
      </div>
    </div>
    @if($input_id==0)
    <div class="row">
      <div class="col-sm-2">
        <label for="">¿Cita por días completos?</label>
        {{$input_dia_completo}}
      </div>
    </div>
        <div id="div_completo">
          <div class="row">
            <div class="col-sm-4">
              <label>Asignar técnico <a href="" class="js_ver_citas_dias">(Ver horarios)</a></label>
              {{$drop_tecnicos_dias}}
              <span class="error error_tecnico"></span>
            </div>
            <div class='col-sm-2'>
              <label for="">Fecha inicio</label>
                <div class="form-group1">
                    <div class='input-group date' id='datetimepicker1'>
                        {{$input_fecha_inicio}}
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                </div>
                 <span class="error_fecha_inicio"></span>
            </div>
            <div class='col-sm-2'>
              <label for="">Fecha Fin</label>
                <div class="form-group1">
                    <div class='input-group date' id='datetimepicker2'>
                        {{$input_fecha_fin}}
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                </div>
                 <span class="error_fecha_fin"></span>
            </div>
            <div class="col-sm-2">
              <label for="">Hora de inicio del día</label>
                      <div class="input-group clockpicker_inicio" data-autoclose="true">
                          {{$input_hora_comienzo}}
                          <span class="input-group-addon">
                              <span class="far fa-clock"></span>
                          </span>
                      </div>
                      <span class="error_hora_hora_comienzo"></span>
            </div>
          </div>
          <div class="row">
             <div class="col-sm-4"></div>
              <div class="col-sm-2">
                <label>Día extra</label>
                  <div class="form-group1">
                    <div class='input-group date' id='datetimepicker3'>
                        {{$input_fecha_parcial}}
                        <span class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </span>
                    </div>
                </div>
              </div>
              <div class="col-sm-2">
                  <label for="">Hora inicio trabajo</label>
                          <div class="input-group clockpicker_inicio" data-autoclose="true">
                              {{$input_hora_inicio_extra}}
                              <span class="input-group-addon">
                                  <span class="far fa-clock"></span>
                              </span>
                          </div>
                          <span class="error_hora_inicio_extra"></span>
                </div>
                <div class="col-sm-2">
                  <label for="">Hora fin trabajo</label>
                          <div class="input-group clockpicker_fin" data-autoclose="true">
                             {{$input_hora_fin_extra}}
                              <span class="input-group-addon">
                                  <span class="far fa-clock"></span>
                              </span>
                          </div>
                          <span class="error_hora_fin_extra"></span>
                          <br>
                        <!--button id="guardar" class="btn btn-success pull-right">Guardar</button>-->
                </div>
            </div>
        </div> <!-- completos -->
        <div id="div_incompleto" class="row">
          <div class="col-sm-4">
            <label>Asignar técnico <a href="" class="js_ver_citas">(Ver horarios)</a></label>
            {{$drop_tecnicos}}
            <span class="error error_tecnico"></span>
          </div>
          <div class="col-sm-3">
            <label for="">Hora inicio trabajo</label>
                    <div class="input-group clockpicker_inicio" data-autoclose="true">
                        {{$input_hora_inicio}}
                        <span class="input-group-addon">
                            <span class="far fa-clock"></span>
                        </span>
                    </div>
                    <span class="error_hora_inicio"></span>
          </div>
          <div class="col-sm-3">
            <label for="">Hora fin trabajo</label>
                    <div class="input-group clockpicker_fin" data-autoclose="true">
                       {{$input_hora_fin}}
                        <span class="input-group-addon">
                            <span class="far fa-clock"></span>
                        </span>
                    </div>
                    <span class="error_hora_fin"></span>
                    <br>
                  <!--button id="guardar" class="btn btn-success pull-right">Guardar</button>-->
          </div>
        </div> <!-- 0 -->
      @endif
  	<div class="row">
  		<div class="col-sm-8">
  			<label for="">Comentarios</label>
  			{{$input_comentarios}}
  			<div class="error error_comentario"></div>
  		</div>
  	</div>
  	<br>
  	<h3>Mis datos</h3>
  	<div class="row">
  		<div class="col-sm-4 form-group">
  			<label for="">Correo electrónico</label>
  			{{$input_email}}
  			<div class="error error_email"></div>
  		</div>
  		<div class="col-sm-4 form-group">
  			<label for="">Nombre(s)</label>
  			{{$input_datos_nombres}}
  			<div class="error error_nombre"></div>
  		</div>
  		<div class="col-sm-4 form-group">
  			<label for="">Apellido Paterno</label>
  			{{$input_datos_apellido_paterno}}
  			<div class="error error_apellido_paterno"></div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-sm-4 form-group">
  			<label for="">Apellido Materno</label>
  			{{$input_datos_apellido_materno}}
  			<div class="error error_apellido_materno"></div>
  		</div>
  		<div class="col-sm-4 form-group">
  			<label for="">Teléfono</label>
  			{{$input_datos_telefono}}
  			<div class="error error_telefono"></div>
  		</div>
  		<div class="col-sm-4 form-group">
  			<label for="">Servicio</label>
  			{{$drop_servicio}}
  			<div class="error error_servicio"></div>
  		</div>
  	</div>
  </form>
  @if($input_id!=0)
    <div class="row">
      <div class="col-sm-4">
        <label>Asignar técnico</label>
        {{$drop_tecnicos}}
        <br>
       
          <button id="asignar_tecnico" class="btn btn-info pull-right">Asignar o cambiar técnico</button>
       
      </div>
      <div class="col-sm-8">
        <br>
        <strong>Hora inicio trabajo:</strong>
        <span style="margin-right: 10px;" id="hora_inicio_trabajo">00:00</span>
        <strong>Hora fin trabajo:</strong>
        <span style="margin-right: 10px;" id="hora_fin_trabajo">00:00</span>
        <strong>Hora inicio comida:</strong>
        <span style="margin-right: 10px;" id="hora_inicio_comida">00:00</span>
        <strong>Hora fin comida:</strong>
        <span style="margin-right: 10px;" id="hora_fin_comida">00:00</span>
      </div>
    </div>
    <br>
  @endif
  	<div class="row pull-right">
  		<div class="col-sm-12 ">
        {{$btn_guardar}}
  		</div>
  	</div>
  	<br><br>
@endsection

@section('scripts')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<script>
	var site_url = "{{site_url()}}";
  $("#div_completo").hide();
  var fecha_actual = "<?php echo date('Y-m-d') ?>";
  $('.date').datetimepicker({
      minDate: moment(),
      format: 'DD/MM/YYYY',
      icons: {
          time: "far fa-clock",
          date: "fa fa-calendar",
          up: "fa fa-arrow-up",
          down: "fa fa-arrow-down"
      },
       locale: 'es'
    });
  $('.clockpicker_inicio').clockpicker({
            afterDone: function() {
                 validar_fecha_inicio();
            },
    });
    $('.clockpicker_fin').clockpicker({
            afterDone: function() {
                validar_fin();
            },
    });//
    $('.clockpicker_inicio').on('change',function(){
         validar_fecha_inicio();
    });
    $('.clockpicker_fin').on('change',validar_fin);
  function validar_fecha_inicio(){
     var dt = new Date();
      if(parseInt(dt.getMinutes())<10){
        var time = dt.getHours() + ":0" + dt.getMinutes();
        
      }else{
          var time = dt.getHours() + ":" + dt.getMinutes();
      }
      if(time.length==4){
        var time = '0'+time;
      }
      var hora_inicio = $("#hora_inicio").val();
      var hora_fin = $("#hora_inicio").val();

      if(fecha_actual==$("#fecha").val()){
       
           if(hora_inicio < time){
          $("#hora_inicio").val(time);
        }
        var hora_inicio = $("#hora_inicio").val();
        $("#hora_fin").val(hora_inicio);
      }else{
        if(hora_fin<hora_inicio){
          $("#hora_fin").val(hora_inicio)
        }
      }
       
        
  }
  $("#dia_completo").on('click',function(){
    if($(this).prop('checked')){
     $("#div_completo").show('slow');
     $("#div_incompleto").hide('slow');
    }else{
       $("#div_completo").hide('slow');
      $("#div_incompleto").show('slow');
    }
  });
  $("#fecha_fin").on('focusout',function(){
     
  })
    function validar_fin(){
     var hora_fin = $("#hora_fin").val();
      var hora_inicio = $("#hora_inicio").val();

       if(hora_fin < hora_inicio){
         $("#hora_fin").val(hora_inicio);
       }
  }
  var tecnico_actual = "{{$input_id_tecnico_actual}}";
	$(".busqueda").select2();

	$("#asesor").on("change",function(){
    var url=site_url+"/citas/getfechas";
		$("#fecha").empty();
		$("#fecha").append("<option value=''>-- Selecciona --</option>");
		$("#fecha").attr("disabled",true);
		$("#horario").empty();
		$("#horario").append("<option value=''>-- Selecciona --</option>");
		$("#horario").attr("disabled",true);
		asesor=$(this).val();
		if(asesor!=''){
			ajaxJson(url,{"asesor":asesor},"POST","",function(result){
				if(result.length!=0){
					$("#fecha").empty();
					$("#fecha").removeAttr("disabled");
					result=JSON.parse(result);
					$("#fecha").append("<option value=''>-- Selecciona --</option>");
					$.each(result,function(i,item){
						var fecha_separada = result[i].fecha.split('-');
						$("#fecha").append("<option value= '"+result[i].fecha+"'>"+fecha_separada[2]+'/'+fecha_separada[1]+'/'+fecha_separada[0]+"</option>");
					});
				}else{
					$("#fecha").empty();
					$("#fecha").append("<option value='0'>No se encontraron datos</option>");
				}
			});
		}else{
			$("#fecha").empty();
			$("#fecha").append("<option value=''>-- Selecciona --</option>");
			$("#fecha").attr("disabled",true);

      $("#tecnicos").empty();
      $("#tecnicos").append("<option value=''>-- Selecciona --</option>");
      $("#tecnicos").attr("disabled",true);
      $("#hora_inicio").val('');
      $("#hora_fin").val('');
		}
	});	

	$("#fecha").on("change",function(){
		var url=site_url+"/citas/getHorarios";
		fecha=$(this).val();
		asesor=$("#asesor").val();
    $("#hora_inicio").val('');
      $("#hora_fin").val('');
		if(fecha!='' && asesor!=''){
			ajaxJson(url,{"asesor":asesor,"fecha":fecha,"id_horario":$("#id_horario").val()},"POST","",function(result){
				if(result.length!=0){
					$("#horario").empty();
					$("#horario").removeAttr("disabled");
					result=JSON.parse(result);
					$("#horario").append("<option value=''>-- Selecciona --</option>");
					$.each(result,function(i,item){
						$("#horario").append("<option value= '"+result[i].id+"'>"+result[i].hora.substring(0,5)+"</option>");
					});
          var id_cita = $("#id").val();
          if(id_cita ==0){
            getTecnicos();
           
          }
				}else{
					$("#horario").empty();
					$("#horario").append("<option value='0'>No se encontraron datos</option>");
				}
			});
		}else{
			// $("#fecha").empty();
			// $("#fecha").append("<option value=''>-- Selecciona --</option>");
			// $("#fecha").attr("disabled",true);

			$("#horario").empty();
			$("#horario").append("<option value=''>-- Selecciona --</option>");
			$("#horario").attr("disabled",true);

      $("#tecnicos").empty();
      $("#tecnicos").append("<option value=''>-- Selecciona --</option>");
      $("#tecnicos").attr("disabled",true);
      $("#hora_inicio").val('');
      $("#hora_fin").val('');

      $("#tecnicos_dias").empty();
      $("#tecnicos_dias").append("<option value=''>-- Selecciona --</option>");
      $("#tecnicos_dias").attr("disabled",true);
		}
	});	
	$("#guardar").on('click',function(){
		  ConfirmCustom("Es necesario cambiar la fecha del técnico antes de guardar, ¿Estás seguro de continuar?", guardar,"", "Confirmar", "Cancelar");
	});
  function guardar(){
       
       var url = site_url+'/citas/reagendar_cita/0';
        ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
                if(isNaN(result)){
                data = JSON.parse( result );
                //Se recorre el json y se coloca el error en la div correspondiente
                $.each(data, function(i, item) {
                    $(".error_"+i).empty();
                    $(".error_"+i).append(item);
                    $(".error_"+i).css("color","red");
                });
        }else{
            if(result==1){
              ExitoCustom("Guardado correctamente",function(){
                window.location.href = site_url+'citas/ver_citas';
              });
            }else if(result==-1){
              ErrorCustom('El horario ya fue ocupado, por favor intenta con otro');
            }else if(result==-2){
              ErrorCustom('La fecha de asignación al técnico debe ser mayor o igual a la fecha del asesor');
            }else if(result==-3){
              ErrorCustom('El técnico no labora en la hora seleccionada');
            }else{
                  ErrorCustom('No se pudo guardar, intenta otra vez.');
            }
        }
      });
  }
    $("#tecnicos").on("change",function(){
    tecnico=$(this).val();
     var url = site_url+'/citas/getHorariosAsesor/';
    if(tecnico!=''){
      if($("#fecha").val()!=''){ 
        ajaxJson(url,{"id_tecnico":tecnico,'fecha':$("#fecha").val()},"POST","",function(result){
          
          if(result.length!=0){
            result=JSON.parse(result);
            $("#hora_inicio_trabajo").text(result[0].hora_inicio);
            $("#hora_fin_trabajo").text(result[0].hora_fin);
            $("#hora_inicio_comida").text(result[0].hora_inicio_comida);
            $("#hora_fin_comida").text(result[0].hora_fin_comida);
            
          }else{
           $("#hora_inicio_trabajo").text('');
            $("#hora_fin_trabajo").text('');
            $("#hora_inicio_comida").text('');
            $("#hora_fin_comida").text('');
          }
        });
      }else{
          ErrorCustom("Es necesario seleccionar la fecha");
        
      }
    }else{
      $("#hora_inicio_trabajo").text('');
      $("#hora_fin_trabajo").text('');
      $("#hora_inicio_comida").text('');
      $("#hora_fin_comida").text('');
    }
  });
    if(tecnico_actual!=0){
      $("#tecnicos").trigger('change');
    }
   $("#asignar_tecnico").on("click",function(){
    if($("#tecnicos").val()!=''){
       var url =site_url+"/citas/modal_asignar_tecnico/0";
       customModal(url,{"id":$("#id").val(),'id_tecnico':$("#id_tecnico_actual").val(),'id_tecnico_seleccionado':$("#tecnicos").val()},"GET","lg",guardarTecnico,"","Guardar","Cancelar","Asignar hora a técnico","modal1");
    }else{
       ErrorCustom("Es necesario seleccionar el técnico.");
    }
  }); 
  function guardarTecnico(){
     var url = site_url+'/citas/modal_asignar_tecnico/';
        ajaxJson(url,{"id":$("#id").val(),'id_tecnico':$("#tecnicos").val(),'hora_inicio':$("#hora_inicio").val(),'hora_fin':$("#hora_fin").val(),'fecha':$("#fecha").val(),"id_detalle":$("#id_detalle").val(),"dia_completo":$("#dia_completo").val(),"fecha_inicio":$("#fecha_inicio").val(),"fecha_fin":$("#fecha_fin").val(),"fecha_parcial":$("#fecha_parcial").val(),"hora_inicio_extra":$("#hora_inicio_extra").val(),"hora_fin_extra":$("#hora_fin_extra").val(),"fecha_reasignar":$("#fecha_reasignar").val(),"hora_comienzo":$("#hora_comienzo").val()},"POST","",function(result){
          
          if(isNaN(result)){
                data = JSON.parse( result );
                //Se recorre el json y se coloca el error en la div correspondiente
                $.each(data, function(i, item) {
                    $(".error_"+i).empty();
                    $(".error_"+i).append(item);
                    $(".error_"+i).css("color","red");
                });
          }else if(result==-1){
             ErrorCustom("El técnico ya tiene asignada una cita en ese horario");
          }else if(result==0){
            ErrorCustom("Error al asignar el técnico, por favor intenta de nuevo.");
          }else if(result==-2){
              ErrorCustom('La fecha de asignación al técnico debe ser mayor o igual a la fecha del asesor');
          }else if(result==-3){
              ErrorCustom('El técnico no labora en la hora seleccionada');
          }else{
            ExitoCustom("Horario asignado correctamente",function(){
              // $(".close").trigger("click");
              window.location.reload();
            });
          }
          
        });
  } 
  function getTecnicos(){
      var url=site_url+"/citas/getTecnicosByFecha";
      fecha=$("#fecha").val();
      if(fecha!=''){
        ajaxJson(url,{"fecha":fecha},"POST","",function(result){
          if(result.length!=0){
            $("#tecnicos").empty();
            $("#tecnicos").removeAttr("disabled");
            result=JSON.parse(result);
            $("#tecnicos").append("<option value=''>-- Selecciona --</option>");
            $.each(result,function(i,item){
              $("#tecnicos").append("<option value= '"+result[i].id_tecnico+"'>"+result[i].nombre+"</option>");
            });
          }else{
            $("#tecnicos").empty();
            $("#tecnicos").append("<option value='0'>No se encontraron datos</option>");
          }
        });
         getTecnicos_dias();
      }else{
        $("#tecnicos").empty();
        $("#tecnicos").append("<option value=''>-- Selecciona --</option>");
        $("#tecnicos").attr("disabled",true);

        $("#hora_inicio").val('');
        $("#hora_fin").val('');
         
      }
  }
    function getTecnicos_dias(){
      var url=site_url+"/citas/getTecnicosByFecha";
      fecha=$("#fecha").val();
      if(fecha!=''){
        ajaxJson(url,{"fecha":fecha},"POST","",function(result){
          if(result.length!=0){
            $("#tecnicos_dias").empty();
            $("#tecnicos_dias").removeAttr("disabled");
            result=JSON.parse(result);
            $("#tecnicos_dias").append("<option value=''>-- Selecciona --</option>");
            $.each(result,function(i,item){
              $("#tecnicos_dias").append("<option value= '"+result[i].id_tecnico+"'>"+result[i].nombre+"</option>");
            });
          }else{
            $("#tecnicos_dias").empty();
            $("#tecnicos_dias").append("<option value='0'>No se encontraron datos</option>");
          }
        });
      }else{
        $("#tecnicos_dias").empty();
        $("#tecnicos_dias").append("<option value=''>-- Selecciona --</option>");
        $("#tecnicos_dias").attr("disabled",true);

        $("#hora_inicio_dias").val('');
        $("#hora_fin_dias").val('');
         
      }
  }
</script>
@endsection