<table class="table table-bordered table-striped" cellpadding="0" width="100%">
	<thead>
		<tr>
			<th style="text-align: center;">Hora inicio trabajo</th>
			<th style="text-align: center;">Hora fin trabajo</th>
			<th style="text-align: center;">Hora inicio comida</th>
			<th style="text-align: center;">Hora fin comida</th>
			<th style="text-align: center;">Hora inicio no laboral</th>
			<th style="text-align: center;">Hora fin no laboral</th>
			<th style="text-align: center;">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@if(count($horarios)>0)
		@foreach($horarios as $key => $value)
		<tr>
			<td colspan="7">
				<strong>Fecha: {{date_eng2esp_1($key)}}</strong>
			</td>
		</tr>
		@foreach($value as $k => $horario)
		<tr>
			<td class="text-center">{{substr($horario->hora_inicio,0,5)}}</td>
			<td class="text-center">{{substr($horario->hora_fin,0,5)}}</td>
			<td class="text-center">{{substr($horario->hora_inicio_comida,0,5)}}</td>
			<td class="text-center">{{substr($horario->hora_fin_comida,0,5)}}</td>
			<td class="text-center">{{substr($horario->hora_inicio_nolaboral,0,5)}}</td>
			<td class="text-center">{{substr($horario->hora_fin_nolaboral,0,5)}}</td>
			<td style="text-align: center;">
				<a href="" data-id="{{$horario->id}}" class="pe pe-7s-note js_editar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar" data-fecha="{{date2sql($horario->fecha)}}" data-horai="{{substr($horario->hora_inicio,0,5)}}" data-horaf="{{substr($horario->hora_fin,0,5)}}" data-horaic="{{substr($horario->hora_inicio_comida,0,5)}}" data-horafc="{{substr($horario->hora_fin_comida,0,5)}}" data-horainl="{{substr($horario->hora_inicio_nolaboral,0,5)}}" data-horafnl="{{substr($horario->hora_fin_nolaboral,0,5)}}" ></a>


				@if($horario->activo)
				<a href="" data-id="{{$horario->id}}" class="js_activar" data-valor="0" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Desactivar horario">
					<i class="pe-7s-switch"></i>
				</a>
				@else
				<a href="" data-id="{{$horario->id}}" class="js_activar" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activar horario">
					<i class="pe-7s-check"></i>
				</a>
				@endif
				@if($horario->motivo!='')
				<a href="" class="fas fa-comments" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="{{$horario->motivo}}"></a>
				@endif

			</td>
		</tr>
		@endforeach
		@endforeach
		@else
		<tr style="text-align: center">
			<td colspan="7">No hay registros para mostrar...</td>
		</tr>
		@endif

	</tbody>
</table>