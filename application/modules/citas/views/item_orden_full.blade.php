<style>
	#tabla_precios{
		overflow-y: scroll;
		height: 300px;
	}
</style>
<form action="" id="order-item-full">
<input type="hidden" id="idorden" name="idorden" value="{{$idorden}}">
<div class="row">
	<div class="col-sm-4">
		<label for="">Unidad</label>
		{{$drop_unidades}}
		<span class="error error_unidad"></span>
	</div>
	<div class="col-sm-4">
		<label for="">Grupo</label>
		<select name="grupo" id="grupo" class="form-control busqueda">
			<option value="">Selecciona</option>
			@foreach($grupos as $g => $grupo)
			<option value="{{$grupo->id}}">{{$grupo->clave.' '.$grupo->descripcion}}</option>
			@endforeach
		</select>
		<span class="error error_grupo"></span>
	</div>
	<div class="col-sm-4">
		<label for="">Operación</label>
		{{$drop_operacion}}
		<span class="error error_operacion"></span>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-12">
		<div id="tabla-servicios"></div>
	</div>
</div>
</form>

<script>
	$(".modal4").removeAttr('tabindex');
	$(".busqueda").select2();
	$(".busqueda").select2({ width: '100%' });           
	$(".contar_item").on('change',function(){
		$.each($(".contar_item"), function(i, item) {
			if(isNaN($(item).val()) || $(item).val()==''){
				$(item).val('0');
			}
        });
		var precio_item = parseFloat($("#precio_item").val());
        var cantidad_item = parseInt($("#cantidad_item").val());
        var descuento_item = parseFloat($("#descuento_item").val())/100;
        if(descuento_item>0){
        	var total = precio_item*cantidad_item;
        	var total = total-(total*descuento_item)
        }else{
        	var total = precio_item*cantidad_item;
        }
        var total_item = $("#total_item").val(total);

	});

	$("#grupo").on("change",function(){
	$(".busqueda").select2('destroy');
    var url=site_url+"/citas/getOperacionesByGroup";
		$("#operacion").empty();
		$("#operacion").append("<option value=''>-- Selecciona --</option>");
		$("#operacion").attr("disabled",true);
		grupo=$(this).val();
		var unidad = $("#unidad").val();
		if(grupo!=''){
			if(unidad!=''){
				ajaxJson(url,{"idgrupo":grupo},"POST","",function(result){
					if(result.length!=0){
						$("#operacion").empty();
						$("#operacion").removeAttr("disabled");
						result=JSON.parse(result);
						$("#operacion").append("<option value=''>-- Selecciona --</option>");
						$.each(result,function(i,item){
							$("#operacion").append("<option value= '"+item.articulo+"'>"+item.articulo+'-'+item.descripcion+"</option>");
						});
					}else{
						$("#operacion").empty();
						$("#operacion").append("<option value='0'>No se encontraron datos</option>");
					}
				});
			}else{
				$(".error_unidad").append('El campo unidad es requerido');
				$(".error_unidad").css('color','red');
				$("#grupo").val('');
			}
		}else{
			$("#operacion").empty();
			$("#operacion").append("<option value=''>-- Selecciona --</option>");
			$("#operacion").attr("disabled",true);
		}
		$(".busqueda").select2();
		//$(".busqueda").select2({ width: 'resolve' });  
	});	

	$("#operacion").on("change",function(){
    var url=site_url+"/citas/getPrecioServicio";
     ajaxLoad(url,{"codigo_intermagic":$("#operacion").val(),"unidad":$("#unidad").val()},"tabla-servicios","POST",function(){
     
     });

	});	
</script>