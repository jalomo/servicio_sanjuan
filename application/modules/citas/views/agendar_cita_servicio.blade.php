@layout('tema_luna/layout')
<style>
    .esconder {
        display: none !important;
    }

    .visible {
        visibility: visible;
    }

    html,
    body {
        height: 100%;
        width: 100%;
        margin: 0;
    }

    .custom-radios div {
        display: inline-block;
    }

    .custom-radios input[type="radio"] {
        display: none;
    }

    .custom-radios input[type="radio"]+label {
        color: #333;
        font-family: Arial, sans-serif;
        font-size: 14px;
    }

    .custom-radios input[type="radio"]+label span {
        display: inline-block;
        width: 40px;
        height: 40px;
        margin: -1px 4px 0 0;
        vertical-align: middle;
        cursor: pointer;
        border-radius: 50%;
        border: 2px solid #FFFFFF;
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.33);
        background-repeat: no-repeat;
        background-position: center;
        text-align: center;
        line-height: 44px;
    }

    .custom-radios input[type="radio"]+label span img {
        opacity: 0;
        transition: all .3s ease;
    }

    .custom-radios input[type="radio"]#color-1+label span {
        background-color: #2ecc71;
    }

    .custom-radios input[type="radio"]#color-2+label span {
        background-color: #f1c40f;
    }

    .custom-radios input[type="radio"]#color-3+label span {
        background-color: #e74c3c;
    }

    .custom-radios input[type="radio"]:checked+label span {
        opacity: 1;
        background: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/242518/check-icn.svg) center center no-repeat;
        width: 40px;
        height: 40px;
        display: inline-block;
    }

    .footer.sticky-footer {
        background: transparent !important;
    }

    .minusculas {
        text-transform: lowercase;
    }

    .bg-verde {
        background-color: #c3e6cb !important;
    }

    .bg-red {
        background-color: #f8d7da !important;
    }

    .bg-gris {
        background-color: #e2e3e5 !important;
    }

</style>
@section('contenido')
    <form action="" method="POST" id="frm_cita_tablero">
        <input type="hidden" name="id" id="id" value="{{ $input_id }}">
        <input type="hidden" name="id_cita" id="id_cita" value="{{ $id_cita }}">
        <input type="hidden" name="paquete_id" id="paquete_id" value="{{ $paquete_id }}">
        <input type="hidden" name="id_horario" id="id_horario" value="{{ $input_id_horario }}">
        <input type="hidden" id="realizo_servicio" name="realizo_servicio" value="{{ $input_realizo_servicio }}">
        <input type="hidden" id="origen" name="origen" value="tablero">
        <input type="hidden" name="id_tecnico_actual" id="id_tecnico_actual" value="{{ $input_id_tecnico_actual }}">
        <input type="hidden" name="cliente_nuevo" id="cliente_nuevo" value="1">
        <input type="hidden" name="vehiculo_nuevo" id="vehiculo_nuevo" value="0">
        <input type="hidden" name="temporal" id="temporal" value="{{ $temporal }}">
        <input type="hidden" name="meses_diferencia" id="meses_diferencia" value="0">
        {{-- Paquetes --}}
        <input type="hidden" name="id_servicio" id="id_servicio" value="">
        <input type="hidden" name="fecha_recepcion_validar" id="fecha_recepcion_validar" value="{{ $fecha_recepcion }}">
        <h1 class="text-center">Orden de servicio</h1>
        <hr>
        <div class="row">
            @if ($input_id == 0)
                <div class="col-md-3 div_cita">
                    <label for=""># Cita</label>
                    <input type="number" id="carriover" name="carriover" id="carriover" class="form-control">
                </div>
                <div class="col-md-1 div_cita" style="margin-top: 30px;">
                    <button type="button" id="btn-co" class="js_carriover btn btn-success">Buscar</button>
                </div>
                <div class="col-sm-8">
                    <label for=""></label>
                    <div style="margin-top: 10px" class="alert alert-info alert-dismissible fade show" role="alert">
                        En caso de que la unidad sea sin cita, <strong>DEJAR ESTE CAMPO EN BLANCO!</strong>.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="">Asesor</label>
                {{ $drop_asesor }}
                <div class="error error_asesor"></div>
            </div>
            <div class="col-md-4">
                <label for="">Teléfono</label>
                {{ $input_telefono_asesor }}
                <div class="error error_telefono_asesor"></div>
            </div>
            <div class="col-md-4">
                <label for="">Extensión</label>
                {{ $input_extension_asesor }}
                <div class="error error_extension_asesor"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="">Color</label>
                {{ $input_color }}
                <div class="error error_color"></div>
            </div>
            <div class="col-md-4">
                <label for="">Número interno (torre)</label>
                {{ $input_numero_interno }}
                <div class="error error_numero_interno"></div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class='col-md-3'>
                <label for="">Fecha de recepción</label>
                {{ $input_fecha_recepcion }}
                <span class="error_fecha_recepcion"></span>
            </div>
            <div class="col-md-3">
                <label for="">Hora de recepción</label>
                {{ $input_hora_recepcion }}
                <span class="error error_hora_recepcion"></span>
            </div>
            <div class='col-md-3'>
                <label for="">Fecha de entrega</label>
                {{ $input_fecha_entrega }}
                <span class="error_fecha_entrega"></span>
            </div>
            <div class="col-md-3">
                <label for="">Hora de entrega</label>
                {{ $input_hora_entrega }}
                <span class="error_hora_entrega"></span>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-sm-3">
                <label>Artículo categoría <i style="cursor: pointer;" class="pe pe-7s-plus addCategoria"
                        data-toggle="tooltip" data-placement="right" title="agregar categoría"></i></label>
                {{ $drop_idcategoria }}
                <span class="error_idcategoria"></span>
            </div>
        </div> -->
        <hr>
        <h3 class="text-center">DATOS DEL CONSUMIDOR</h3>
        <hr>
        <div class="row">
            <div class="col-sm-4">
                <strong for="">Buscar cliente</strong>
                <input type="text" id="clientesearch" name="clientesearch" class="form-control">
            </div>
            <div class="col-sm-2">
                <button type="button" id="clientebuscar" style="margin-top: 20px;" class="btn btn-info">Buscar
                    cliente</button>
            </div>
            <div class="col-md-3">
                <label for="">#Cliente / </label> Cliente nuevo <input type="checkbox" id="nuevocliente"
                    name="nuevocliente">
                {{ $input_numero_cliente }}
                <div class="error error_numero_cliente"></div>
            </div>
            <div class="col-sm-3">
                <button type="button" id="inventario" style="margin-top: 20px;" class="btn btn-default">Ver
                    inventario</button>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-2">
                <label>Tipo de cliente</label>
                {{ $drop_tipo_cliente }}
                <div class="error error_tipo_cliente"></div>
            </div>
            <div class="col-md-4 form-group">
                <label for="">Nombre(s) del Consumidor</label>
                {{ $input_datos_nombres }}
                <div class="error error_nombre"></div>
            </div>
            <div class="col-md-3 form-group">
                <label for="">Apellido Paterno</label>
                {{ $input_datos_apellido_paterno }}
                <div class="error error_apellido_paterno"></div>
            </div>
            <div class="col-md-3 form-group">
                <label for="">Apellido Materno</label>
                {{ $input_datos_apellido_materno }}
                <div class="error error_apellido_materno"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="">Nombre de la compañia</label>
                {{ $input_nombre_compania }}
                <span class="error error_nombre_compania"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="">Nombre(s) del Contacto de la compañía</label>
                {{ $input_nombre_contacto_compania }}
                <span class="error error_nombre_contacto_compania"></span>
            </div>
            <div class="col-md-4">
                <label for="">Apellido Paterno del Contacto</label>
                {{ $input_ap_contacto }}
                <span class="error error_ap_contacto"></span>
            </div>
            <div class="col-md-4">
                <label for="">Apellido Materno del Contacto</label>
                {{ $input_am_contacto }}
                <span class="error error_am_contacto"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="">Correo Electronico del Consumidor</label>
                {{ $input_email }}
                <div class="error error_email"></div>
            </div>
            <div class="col-md-4">
                <label for="">R.F.C.</label>
                {{ $input_rfc }}
                <span class="error error_rfc"></span>
            </div>
            <div class="col-md-4">
                <label for="">Correo Electrónico de la Compañía</label>
                {{ $input_correo_compania }}
                <span class="error error_correo_compania"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="">Calle</label>
                {{ $input_calle }}
                <span class="error error_calle"></span>
            </div>
            <div class="col-md-2">
                <label for="">#Exterior</label>
                {{ $input_noexterior }}
                <span class="error error_noexterior"></span>
            </div>
            <div class="col-md-2">
                <label for="">#Interior</label>
                {{ $input_nointerior }}
                <span class="error error_nointerior"></span>
            </div>
            <div class="col-md-4">
                <label for="">Colonia</label>
                {{ $input_colonia }}
                <span class="error error_colonia"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="">Municipio</label>
                {{ $input_municipio }}
                <span class="error error_municipio"></span>
            </div>
            <div class="col-md-4">
                <label for="">C.P.</label>
                {{ $input_cp }}
                <span class="error error_cp"></span>
            </div>
            <div class="col-md-4">
                <label for="">Estado</label>
                {{ $input_estado }}
                <span class="error error_estado"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label for="">Teléfono Móvil</label>
                {{ $input_telefono_movil }}
                <span class="error error_telefono_movil"></span>
            </div>
            <div class="col-md-4">
                <label for="">Otro teléfono</label>
                {{ $input_otro_telefono }}
                <span class="error error_otro_telefono"></span>
            </div>
        </div>
        <hr>
        <br>
        <br>
        <h3 class="text-center">DATOS DEL VEHÍCULO</h3>
        <div class="row">
            <div class="col-sm-12 text-right">
                <h2 for="" id="lbl-campania"></h2>
                <div class="custom-radios">
                    <div id="div-radio-1">
                        <input type="radio" id="color-1" name="campania" value="1" class="rd-campania">
                        <label for="color-1">
                            <span>
                            </span>
                        </label>
                    </div>
                    <div id="div-radio-2">
                        <input type="radio" id="color-2" name="campania" value="2" class="rd-campania">
                        <label for="color-2">
                            <span>
                            </span>
                        </label>
                    </div>
                    <div id="div-radio-3">
                        <input type="radio" id="color-3" name="campania" value="3" class="rd-campania">
                        <label for="color-3">
                            <span>
                            </span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <button type="button" id="btn-no-aut" class="btn btn-info invisible">Ver presupuestos no
                    autorizados</button>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-4">
                <label for=""></label>
                <div style="margin-top: 10px" class="alert alert-info alert-dismissible fade show" role="alert">
                    <strong>Solamente marcar check cuando se vaya actualizar la placa.</strong>
                </div>
            </div>
            <div class="col-md-4">
                <label for="">¿Nuevo vehículo?</label> <input type="checkbox" id="nuevovehiculo" name="nuevovehiculo">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 form-group">
                <label for="">Placas del vehículo</label>
                {{ $input_vehiculo_placas }}
                <div class="error error_vehiculo_placas"></div>
            </div>
            <div class="col-md-4">
                <label for="">Número de Identificación Vehicular</label>
                {{ $input_vehiculo_numero_serie }}
                <div class="error error_vehiculo_numero_serie"></div>
            </div>
            <div class="col-md-4">
                <label for="">Kilometraje</label>
                {{ $input_vehiculo_kilometraje }}
                <div class="error error_vehiculo_kilometraje"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 form-group">
                <label for="">Marca / Línea del Vehículo</label>
                {{ $drop_vehiculo_marca }}
                <div class="error error_vehiculo_marca"></div>
            </div>
            <div class="col-md-4">
                <label for="">Año Modelo</label>
                {{ $drop_vehiculo_anio }}
                <div class="error error_vehiculo_anio error_preventivo"></div>
            </div>
            <div class="col-md-4 form-group">
                <label for="">Color</label>
                {{ $drop_color }}
                <div class="error error_id_color"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 form-group">
                <label for="">Modelo</label>
                {{ $drop_vehiculo_modelo }}
                <div class="error error_vehiculo_modelo error_preventivo"></div>
            </div>
            <div class="col-sm-4 form-group">
                <label for="">Submodelo</label>
                {{$drop_submodelo_id}}
                <div class="error error_submodelo_id"></div>
              </div>
            <div class="col-md-4 form-group">
                <label for="">Tipo de orden</label>
                {{ $drop_id_tipo_orden }}
                <div class="error error_id_tipo_orden"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 form-group">
                <label for="">Tipo de operación</label>
                {{ $drop_id_tipo_operacion }}
                <div class="error error_id_tipo_operacion"></div>
            </div>
            <div class="col-md-4 form-group">
                <label for="">Estatus</label>
                {{ $drop_id_status_color }}
                <div class="error error_id_status_color"></div>
            </div>
            <div class='col-md-4'>
                <label for="">Fecha inicio vigencia de garantía</label>
                {{ $input_inicio_vigencia }}
                <span class="error_inicio_vigencia"></span>
            </div>
        </div>
        <div class="row">
            <div class='col-md-4'>
                <label for="">¿Acepta beneficios?</label>
                {{ $input_acepta_beneficios }}
                <span class="error_acepta_beneficios"></span>
            </div>
        </div>
        @if($fecha_recepcion < CONST_FECHA_CITA_PAQUETES )
            <div class="row">
                <div class="col-md-4 form-group">
                    <label for="">Motor</label>
                    {{ $input_motor }}
                    <div class="error error_motor"></div>
                </div>
                <div class="col-md-4 form-group">
                    <label for="">Transmisión</label>
                    {{ $input_transmision }}
                    <div class="error error_transmision"></div>
                </div>
                <div class="col-md-4 form-group">
                    <label for="">Cilindros</label>
                    {{ $input_cilindros }}
                    <div class="error error_cilindros "></div>
                </div>
            </div>
        @else
        <div class="row">
            <div class="col-md-4 form-group">
                <label for="">Cilindros</label>
                {{ $drop_cilindro_id }}
                <div class="error error_cilindro_id error_preventivo"></div>
            </div>
            <div class="col-md-4 form-group">
                <label for="">Transmisión</label>
                {{ $drop_transmision_id }}
                <div class="error error_transmision_id error_preventivo"></div>
            </div>
            <div class="col-md-4 form-group">
                <label for="">Combustible</label>
                {{ $drop_combustible_id }}
                <div class="error error_combustible_id error_preventivo"></div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4 form-group">
                <label for="">Motor</label>
                {{ $drop_motor_id }}
                <div class="error error_motor_id error_preventivo"></div>
            </div>
        </div>
        @endif
        
        <div class="row">
            <div class="col-md-4 div_tecnicos">
                <br>
                <label for="">¿Cita por días completos?</label>
                {{ $input_dia_completo }}
            </div>
        </div>
        @if ($input_id == 0)
            <div id="div_horarios_tecnicos">
                <div id="div_completo">
                    <div class="row">
                        <div class="col-md-4">
                            <label>Asignar técnico <a href="" class="js_ver_citas_dias">(Ver horarios)</a></label>
                            {{ $drop_tecnicos_dias }}
                            <span class="error error_tecnico_dias"></span>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class='col-sm-3'>
                            <label for="">Fecha inicio</label>
                            {{ $input_fecha_inicio }}
                            <span class="error_fecha_inicio"></span>
                        </div>
                        <div class='col-md-3'>
                            <label for="">Fecha Fin</label>
                            {{ $input_fecha_fin }}
                            <span class="error_fecha_fin"></span>
                        </div>
                        <div class="col-md-3">
                            <label for="">Hora de inicio del día</label>
                            {{ $input_hora_comienzo }}
                            <span class="error_hora_comienzo"></span>
                            <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Día extra</label>
                            {{ $input_fecha_parcial }}
                        </div>
                        <div class="col-md-3">
                            <label for="">Hora inicio trabajo</label>
                            {{ $input_hora_inicio_extra }}
                            <span class="error_hora_inicio_extra"></span>
                            <br>
                        </div>
                        <div class="col-md-3">
                            <label for="">Hora fin trabajo</label>
                            {{ $input_hora_fin_extra }}
                            <span class="error_hora_fin_extra"></span>
                            <br>
                        </div>
                    </div>
                </div>
                <div id="div_incompleto" class="row">
                    <div class="col-md-4">
                        <label>Asignar técnico <a href="" class="js_ver_citas">(Ver horarios)</a></label>
                        {{ $drop_tecnicos }}
                        <span class="error error_tecnico"></span>
                    </div>
                    <div class="col-md-3">
                        <label for="">Hora inicio trabajo</label>
                        {{ $input_hora_inicio }}
                        <span class="error error_hora_inicio"></span>
                        <br>
                    </div>
                    <div class="col-md-3">
                        <label for="">Hora fin trabajo</label>
                        {{ $input_hora_fin }}
                        <span class="error_hora_fin"></span>
                        <br>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-8">
                <label for="">Comentarios</label>
                {{ $input_comentarios }}
                <div class="error error_comentario"></div>
            </div>
        </div>
        <br>
        <br>
        <hr>
        <div class="row">
            <div class="col-sm-12" style="text-align: center;">
                <strong style="font-size: 20px;">Selecciona y agrega operaciones a la orden</strong><br>
                <br>
                @if ($puede_agregar_operacion)
                    <button style="margin-left: 10px;" id="preventivo" type="button"
                        class="btn btn-default">Preventivo</button>
                @endif
                <button style="margin-left: 10px;" id="correctivo" type="button" class="btn btn-default">Correctivo</button>
                <button style="margin-left: 10px;" id="descripcion-orden" type="button"
                    class="btn btn-default">Genérico</button>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-2">
                <div class="alert alert-success bg-verde" role="alert">
                    ASIGNADA
                </div>
            </div>
            <div class="col-sm-2">
                <div class="alert alert-danger bg-red" role="alert">
                    SIN ASIGNAR
                </div>
            </div>
            <div class="col-sm-4">
                <div class="alert alert-secondary bg-gris" role="alert" style="border:1px solid">
                    <i style="margin-right: 5px" class="pe pe-7s-close"></i>NO APLICA ASIGNACIÓN
                </div>
            </div>
        </div>
        @if ($input_id != 0)
            <br>
            <?php $clase_op_ext = ''; ?>
            <h5 class="text-center">LISTADO DE COTIZACIONES EXTRAS AUTORIZADAS POR EL CLIENTE</h5>
            <div id="div_lista_operaciones">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Cantidad</th>
                                    <th>Descripción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($operaciones_extras_orden) > 0)
                                    @foreach ($operaciones_extras_orden as $o => $operacion)
                                        @if ($operacion->id_operacion == null || $operacion->id_operacion == 0)
                                            <?php $clase_op_ext = 'bg-red'; ?>
                                        @else
                                            <?php $clase_op_ext = 'bg-verde'; ?>
                                        @endif
                                        <tr class="{{ $clase_op_ext }}">
                                            <td>{{ $operacion->cantidad }}</td>
                                            <td>{{ $operacion->descripcion }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td class="text-center" colspan="2">No se tienen registradas operaciones extras</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-12">
                <h5 class="text-center">DESCRIPCIÓN DEL TRABAJO Y DESGLOCE DEL PRESUPUESTO</h5>
                <hr>
                <div class="row">
                    <div class="col-sm-12">
                        <div id="tabla-items"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="div_refacciones"></div>
        <br>
        <h2>Horarios Técnicos</h2>
        <div class="row">
            <div class="col-sm-6">
                <div id="tbl_horarios_tecnicos">
                </div>
            </div>
        </div>
        <div id="alert-beneficios" class="row d-none">
            <div class="col-sm-12">
                <div class="alert alert-danger text-center" role="alert">
                    <h3>Este cliente es un potencial candidato para la extensión de garantías</h3>
                </div>
            </div>
        </div>
        <div id="alert-acepta-beneficios" class="row d-none">
            <div class="col-sm-12">
                <div class="alert alert-success text-center" role="alert">
                    <h3>El cliente aceptó beneficios</h3>
                </div>
            </div>
        </div>
        <br>
        <div class="row pull-right">
            <div class="col-sm-12">
                <button type="button" id="guardar" name="guardar" class="btn btn-success">Guardar</button>
            </div>
        </div>
        <br>
        <br>
        <br>
    </form>

@endsection
@section('scripts')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>js/dms/generico.js"></script>
    <script src="<?php echo base_url(); ?>js/citas.js"></script>
    <script>
        var site_url = "{{ site_url() }}";
        var item = '';
        var idorden = "{{ $input_id }}";
        var campania = "{{ $campania }}";
        var logueado = 1;
        var tipo = 0;
        var fecha_comparar = "{{ date('Y-m-d') }}";
        $(".busqueda").select2();
        if (idorden != 0) {
            $("#color-" + campania).prop('checked', true);
            buscarItems();
            logueado = 1;
            if ($("#fecha_recepcion").val() != '') {
                var array_fecha_recepcion = $("#fecha_recepcion").val().split('/');
                var recepcion = array_fecha_recepcion[2] + "/" + array_fecha_recepcion[1] + "/" + array_fecha_recepcion[0];
            } else {
                var recepcion = moment();
            }


            $('#datetimepicker4').datetimepicker({
                minDate: recepcion,
                format: 'DD/MM/YYYY',
                icons: {
                    time: "far fa-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down",
                },
                locale: 'es'
            });

            $(".editarPreventivo").on('change',function(){
                let id_status = $("#id_status_color").val();
                let preventivo = $('.js_editar_preventivo').length
                if(preventivo==1){
                    //Existe un registro
                    $('.js_editar_preventivo').trigger('click');
                }
            })
        } else {
            logueado = 1;
        }
        $("#vehiculo_kilometraje").on('change', function() {
            mostrar_ocultar_carta();
        })
        $("#inicio_vigencia").on('focusout', function() {
            var fecha = $(this).val();
            var km = parseFloat($(this).val());
            ajaxJson(site_url + "/citas/getDifMoth/", {
                fecha: fecha
            }, "POST", "", function(result) {
                $("#meses_diferencia").val(result)
                mostrar_ocultar_carta();
            });
        });
        // if(idorden!=0){
        //   $("#vehiculo_kilometraje").trigger('change');
        // }
        $("#acepta_beneficios").on('click', function() {
            mostrar_ocultar_carta();
        })

        function mostrar_ocultar_carta() {
            var km = parseFloat($("#vehiculo_kilometraje").val());
            var meses_diferencia = $("#meses_diferencia").val();
            var acepta_beneficios = $("#acepta_beneficios").prop('checked');
            if (acepta_beneficios) {
                $("#alert-beneficios").addClass('d-none');
                $("#alert-acepta-beneficios").removeClass('d-none');
            } else {
                if (($.isNumeric(km) && (km >= 55000 && km <= 60000)) || (meses_diferencia >= 34 && meses_diferencia <=
                        36)) {
                    $("#alert-beneficios").removeClass('d-none');
                } else {
                    $("#alert-beneficios").addClass('d-none');
                }
                $("#alert-acepta-beneficios").addClass('d-none');
            }

        }
        $("body").on('change', '#vehiculo_placas', function() {
            if (!$("#actualizar_placa").prop('checked')) {
                buscar_serie();
            }
        });
        $("#guardar").on('click', callbackGuardarCitaTablero);
        $("#descripcion-orden-full").on('click', function() {
            var url = site_url + "/citas/asignarOrdenFull/0";
            customModal(url, {
                "idorden": $("#id").val()
            }, "GET", "lg", saveAllItems, "", "Guardar", "Cancelar", "Asignar servicios ", "modal4");
        });
        $("#vehiculo_modelo").on("change",function(){
        var url ="<?php echo site_url();?>/citas/getSubmodelo";

        $("#submodelo_id").empty();
        $("#submodelo_id").append("<option value=''>-- Selecciona --</option>");
        $("#submodelo_id").attr("disabled",true);

        if($(this).val()!=0 && $("#vehiculo_modelo").val()){
            ajaxJson(url,{"id_modelo_prepiking":$(this).val(),"vehiculo_modelo":$("#vehiculo_modelo").val()},"POST","",function(result){
                if(result.length !=0){
                $("#submodelo_id").empty();
                $("#submodelo_id").removeAttr("disabled");
                result=JSON.parse(result);
                $("#submodelo_id").append("<option value='0'>-- Selecciona --</option>");
                $.each(result, function(i, item) {
                    $("#submodelo_id").append("<option value= '" + result[i].id + "'>" + result[i].nombre + "</option>");
                });
                }else{
                $("#submodelo_id").empty();
                $("#submodelo_id").append("<option value='0'>No se encontraron datos</option>");
                }
            });
            }else{
            $("#submodelo_id").empty();
            $("#submodelo_id").append("<option value=''>-- Selecciona --</option>");
            $("#submodelo_id").attr("disabled",true);

            }
        });
        $("#correctivo").on('click', function() {
            var url = site_url + "/correctivo/modal_correctivo/0";
            customModal(url, {
                    "idorden": $("#id").val(),
                    "temporal": $("#temporal").val(),
                    "idoperacion": '0'
                }, "GET", "lg", saveCorrectivo, "", "Guardar", "Cancelar", "Asignar servicios correctivo ",
                "modalCorrectivo");
        });
        $("#preventivo").on('click', function() {
            $("#preventivo").prop('disabled', true);
            ajaxJson(site_url + "/layout/validarPreventivo/" + $("#id").val(), {
                "temporal": $("#temporal").val(),
                "idoperacion": '0'
            }, "GET", "", function(result) {
                if (result ==1){
                    ErrorCustom("Ya fue asignado una operación de tipo preventivo");
                } else {
                    $(".error_preventivo").empty();
                    var url = site_url + "/preventivo/modal_preventivo/0";
                    let error = validarCamposPreventivo();
                    if(error){
                        ErrorCustom('Es necesario llenar los campos requeridos para dar de alta la operación')
                        $("#preventivo").prop('disabled', false);
                        return;
                    }
                    customModal(url, {
                            "cilindro_id": $("#cilindro_id" ).val(),
                            "cilindro_text": $("#cilindro_id option:selected" ).text(),

                            "transmision_id": $("#transmision_id" ).val(),
                            "transmision_text": $("#transmision_id option:selected" ).text(),

                            "combustible_id": $("#combustible_id" ).val(),
                            "combustible_text": $("#combustible_id option:selected" ).text(),

                            "motor_id": $("#motor_id" ).val(),
                            "motor_text": $("#motor_id option:selected" ).text(),

                            "vehiculo_anio": $("#vehiculo_anio" ).val(),
                            "vehiculo_modelo": $("#vehiculo_modelo" ).val(),
                            "id_servicio": $("#id_servicio" ).val(),

                            "submodelo_id": $("#submodelo_id" ).val(),
                            "submodelo_text": $("#submodelo_id option:selected" ).text(),

                            "idorden": $("#id").val(),
                            "temporal": $("#temporal").val(),
                            "idoperacion": '0'
                        }, "GET", "lg", savePreventivo, "", "Guardar", "Cancelar",
                        "Asignar servicios preventivo ", "modalPreventivo");
                }
                $("#preventivo").prop('disabled', false);
            });
        });
        $("body").on('click', '.js_editar_preventivo', function(e) {
            e.preventDefault();
            let error = validarCamposPreventivo();
            if(error){
                ErrorCustom('Es necesario llenar los campos requeridos para dar de alta la operación')
                $("#preventivo").prop('disabled', false);
                return;
            }
            var url = site_url + "/preventivo/modal_preventivo/0";
            var idoperacion = $(this).data('idop');
            customModal(url, {
                    "cilindro_id": $("#cilindro_id" ).val(),
                    "cilindro_text": $("#cilindro_id option:selected" ).text(),

                    "transmision_id": $("#transmision_id" ).val(),
                    "transmision_text": $("#transmision_id option:selected" ).text(),

                    "combustible_id": $("#combustible_id" ).val(),
                    "combustible_text": $("#combustible_id option:selected" ).text(),

                    "motor_id": $("#motor_id" ).val(),
                    "motor_text": $("#motor_id option:selected" ).text(),

                    "vehiculo_anio": $("#vehiculo_anio" ).val(),
                    "vehiculo_modelo": $("#vehiculo_modelo" ).val(),
                    "id_servicio": $("#id_servicio" ).val(),

                    "submodelo_id": $("#submodelo_id" ).val(),
                    "submodelo_text": $("#submodelo_id option:selected" ).text(),

                    "idorden": $("#id").val(),
                    "temporal": $("#temporal").val(),
                    "idoperacion": idoperacion
                }, "GET", "lg", savePreventivo, "", "Guardar", "Cancelar", "Asignar servicios preventivo ",
                "modalPreventivo");
        });
        function validarCamposPreventivo(){
            let cilindro_id = $("#cilindro_id").val();
            let transmision_id = $("#transmision_id").val();
            let combustible_id = $("#combustible_id").val();
            let motor_id = $("#motor_id").val();
            let vehiculo_anio = $("#vehiculo_anio").val();
            let vehiculo_modelo = $("#vehiculo_modelo").val();
            let error = false;
            // if(cilindro_id==''){
            //     $(".error_cilindro_id").empty().append('Campo requerido operación').css('color','red');
            //     error = true;
            // }
            // if(transmision_id==''){
            //     $(".error_transmision_id").empty().append('Campo requerido operación').css('color','red');
            //     error = true;
            // }
            // if(combustible_id==''){
            //     $(".error_combustible_id").empty().append('Campo requerido operación').css('color','red');
            //     error = true;
            // }
            if(motor_id==''){
                $(".error_motor_id").empty().append('Campo requerido operación').css('color','red');
                error = true;
            }
            if(vehiculo_anio==''){
                $(".error_vehiculo_anio").empty().append('Campo requerido operación').css('color','red');
                error = true;
            }
            if(vehiculo_modelo==''){
                $(".error_vehiculo_modelo").empty().append('Campo requerido operación').css('color','red');
                error = true;
            }
            return error;
        }
        $("body").on('click', '.js_editar_correctivo', function(e) {
            e.preventDefault();
            var url = site_url + "/correctivo/modal_correctivo/0";
            var idoperacion = $(this).data('idop');
            customModal(url, {
                "idorden": $("#id").val(),
                "temporal": $("#temporal").val(),
                "idoperacion": idoperacion
                }, "GET", "lg", saveCorrectivo, "", "Guardar", "Cancelar", "Asignar servicios correctivo ",
                "modalCorrectivo");
        });
        var input_ocupado = "{{ $input_realizo_servicio }}";
        var tecnico_actual = "{{ $input_id_tecnico_actual }}";
        $("#asesor").on("change", function() {
            var url = site_url + "/citas/getTelefonoAsesor";
            var nombre_asesor = $(this).val();
            ajaxJson(url, {
                "nombre": nombre_asesor
            }, "POST", "", function(result) {
                result = JSON.parse(result);
                $("#telefono_asesor").val(result.datos.telefono);
                $("#agente").val(result.datos.agente);
                $("#extension_asesor").val(result.datos.extension_asesor);
                $("#color").val(result.datos.color);
                $("#numero_interno").val(result.datos.numero_interno);
            });
        });
        if (tecnico_actual != 0) {
            $("#tecnicos").trigger('change');
        }
        $("#asignar_tecnico").on("click", function() {
            if ($("#tecnicos").val() != '') {
                var url = site_url + "/citas/modal_asignar_tecnico/0";
                customModal(url, {
                        "id": $("#id").val(),
                        'id_tecnico': $("#id_tecnico_actual").val()
                    }, "GET", "lg", guardarTecnico, "", "Guardar", "Cancelar", "Asignar hora a técnico",
                    "modal1");
            } else {
                ErrorCustom("Es necesario seleccionar el técnico.");
            }
        });

        function callbackGuardarCitaTablero() {
            var url = site_url + '/citas/agendar_cita_servicio';
            if (verificarFormatoHoras()) {
                if (validarHorasIguales() || $("#id_cita").val() != 0) {
                    $("#guardar").prop('disabled', true);
                    ajaxJson(url, $("#frm_cita_tablero").serialize(), "POST", "async", function(result) {
                        if (isNaN(result)) {
                            data = JSON.parse(result);
                            $.each(data, function(i, item) {
                                $(".error_" + i).empty();
                                $(".error_" + i).append(item);
                                $(".error_" + i).css("color", "red");
                            });
                            $("#guardar").prop('disabled', false);
                        } else {
                            if (result > 0) {
                                $("#id_orden_save").val(result);
                                ajaxJson(site_url + '/citas/actualizar_totales_orden', $("#frm-total-orden")
                                    .serialize(), "POST", "",
                                    function(result_orden) {
                                        if (result_orden == 1) {
                                            ExitoCustom("Guardado correctamente", function() {
                                                window.location.href = site_url +
                                                    "/orden/historial_orden_servicio";
                                            });
                                        }
                                    });
                            } else if (result == -1) {
                                ErrorCustom('El horario ya fue ocupado, por favor intenta con otro');
                            } else if (result == -2) {
                                ErrorCustom(
                                    'La fecha de asignación al técnico debe ser mayor o igual a la fecha del asesor'
                                );
                            } else if (result == -3) {
                                ErrorCustom('El técnico no labora en la hora seleccionada');
                            } else if (result == -4) {
                                ErrorCustom('Es necesario asignar por lo menos una operación a la orden');
                            }else if(result==-5){
                                ErrorCustom('La hora de entrega debe ser en el horario laboral de 08:00 a.m. a 19:00 p.m.');
                            }else if(result==-6){
                                ErrorCustom('La fecha de entrega debe ser mayor a la fecha actual');
                            }else if(result==-7){
                                ErrorCustom('Es necesario agregar un paquete para este tipo de servicio');
                            }else if(result==-8){
                                ErrorCustom('Los datos seleccionados no coinciden con los datos del paquete, paquete actual: '+$("#paquete_id").val());
                            }else {
                                ErrorCustom('No se pudo guardar, intenta otra vez.');
                            }
                            $("#guardar").prop('disabled', false);
                        }
                    });
                } else {
                    ErrorCustom("La hora de inicio de trabajo y hora fin NO pueden ser iguales");
                    $("#guardar").prop('disabled', false);
                }
            } else {
                ErrorCustom("Es necesario revises el formato de las horas");
                $("#guardar").prop('disabled', false);
            }
        }
        $("body").on('click', '#btn-co', function() {
            var url = site_url + '/citas/getDatosCitaServicios';
            var total_orden = $("#total_orden").val();
            ajaxJson(site_url + '/citas/validateBeforeMakeOrder', {
                "id_cita": $("#carriover").val()
            }, "POST", "async", function(validar) {
                validar = JSON.parse(validar);
                if (validar.exito) {
                    ajaxJson(url, {
                        "id_cita": $("#carriover").val()
                    }, "POST", "async", function(result) {
                        result = JSON.parse(result);
                        if (result.exito) {
                            $("#fecha_recepcion").val('');
                            $("#fecha_entrega").val('');
                            $("#id_cita").val(result.cita.id_cita);
                            $("#iscarriover").val(1);
                            $(".div_tecnicos").css('display', 'none');
                            $.each(result.cita, function(i, item) {
                                if (i != 'fecha' && i != 'vehiculo_marca') {
                                    $("#" + i).val(item);
                                    if(i!='vehiculo_placas' && i!='vehiculo_numero_serie' && i!='vehiculo_marca' && i!='email' && i != 'vehiculo_kilometraje' && i!='numero_cliente' && i!='datos_nombres' && i!='datos_apellido_paterno' && i!='datos_apellido_materno'){ 
                                        $("#"+i).prop('readonly',true);
                                        $("#"+i).addClass('deshabilitados',true);
                                    }
                                }
                            });
                           
                            $("#vehiculo_modelo").val(result.cita.vehiculo_modelo);
                            if(result.cita.submodelo_id!=0){
                                $("#vehiculo_modelo").trigger('change');
                                $("#submodelo_id").val(result.cita.submodelo_id);
                            }
                            $("#id_servicio").val(result.cita.id_modelo_prepiking);
                            $.each(result.orden, function(i2, item2) {
                                $("#" + i2).val(item2);
                            });
                            $("#div_horarios_tecnicos").css('display', 'none');
                            $("#horario").css('display', 'none');
                            $(".lbl-horarios").css('display', 'none');
                            $.each(result.tecnicos_citas, function(i, item) {
                                if (item.dia_completo == '0') {
                                    var dia_completo = 'No';
                                } else {
                                    var dia_completo = 'Si';
                                }
                                $("#tbl_horarios_tecnicos").empty().append(
                                    '<table class="table table-striped"><thead><tr><th>Fecha Inicio</th><th>Fecha Fin</th><th>Hora Inicio</th><th>Hora Fin</th><th>Técnico</th><th>Días Completo</th></tr></thead><tbody><tr><th>' +
                                    item.fecha + '</th><th>' + item.fecha_fin +
                                    '</th><th>' + item.hora_inicio_dia + '</th><th>' +
                                    item.hora_fin + '</th><th>' + item.tecnico +
                                    '</th><th>' + dia_completo +
                                    '</th></tr></tbody></table>');
                            });
                            $("#id_cita").removeAttr('disabled');
                            buscar_serie();
                            $("#asesor").trigger('change');
                            voltearFechas();
                            if (result.existe_orden) {
                                $('#frm_cita_tablero').find('input, textarea').attr('readonly',
                                    'true');
                                $('#frm_cita_tablero').find('select').attr('disabled', 'true');
                                $('#frm_cita_tablero').find('button').attr('disabled', 'true');
                            }
                            llenarOrdenByBusqueda();
                            $("#hora_entrega").val(result.hora_promesa_m);
                            $("#fecha_entrega").val(result.fecha_promesa_m);
                            $("#fecha_recepcion").val("{{ date('d/m/Y') }}");
                        } else {
                            ErrorCustom('La cita no se encuentra registrada', function() {
                                $("#id_cita").val(0);
                                $("#iscarriover").val(0);
                            });
                        }
                    });
                } else {
                    ErrorCustom(validar.mensaje, function() {
                        $("#id_cita").val(0);
                        $("#iscarriover").val(0);
                    });
                }
            })
            $("#total_orden").val(total_orden);
            $(".busqueda").select2();
        });
        $("body").on('change', "#vehiculo_numero_serie", function() {
            if ($("#vehiculo_numero_serie").val() != '') {
                ValidarCampania();
                getDatosBySerie();
                getPresupuestos();
            }
        });

        function saveAllItems() {
            var url = site_url + '/citas/saveAllItemsOrden';
            ajaxJson(url, $("#order-item-full").serialize(), "POST", "async", function(result) {
                if (isNaN(result)) {
                    data = JSON.parse(result);
                    $.each(data, function(i, item) {
                        $(".error_" + i).empty();
                        $(".error_" + i).append(item);
                        $(".error_" + i).css("color", "red");
                    });
                } else {
                    if (result == 1) {
                        ExitoCustom("Guardado correctamente", function() {
                            $(".modal4").modal('hide');
                            buscarItems();
                        });
                    } else {
                        ErrorCustom('No se pudo guardar, intenta otra vez.');
                    }
                }
            });
        }

        function saveCorrectivo() {
            var url = site_url + '/correctivo/modal_correctivo';
            var numberOfChecked = $('.operaciones:checked').length;
            if ($("#relacionar_op").prop('checked') && numberOfChecked == 0) {
                ErrorCustom("Es necesario relacionar por lo menos una operación extra");
            } else {
                ajaxJson(url, $("#frm-correctivo").serialize(), "POST", "async", function(result) {
                    if (isNaN(result)) {
                        data = JSON.parse(result);
                        $.each(data, function(i, item) {
                            $(".error_" + i).empty();
                            $(".error_" + i).append(item);
                            $(".error_" + i).css("color", "red");
                        });
                    } else {
                        if (result == 1) {
                            ExitoCustom("Guardado correctamente", function() {
                                $(".modalCorrectivo").modal('hide');
                                buscarItems();
                                if (idorden != 0) {
                                    buscarOperacionesExtras();
                                }
                            });
                        } else {
                            ErrorCustom('No se pudo guardar, intenta otra vez.');
                        }
                    }
                });
            }
        }

        function savePreventivo() {
            var url = site_url + '/preventivo/modal_preventivo';
            ajaxJson(url, $("#frm-preventivo").serialize(), "POST", "async", function(result) {
                if (isNaN(result)) {
                    data = JSON.parse(result);
                    $.each(data, function(i, item) {
                        $(".error_" + i).empty();
                        $(".error_" + i).append(item);
                        $(".error_" + i).css("color", "red");
                    });
                } else {
                    if (result == 1) {
                        ExitoCustom("Guardado correctamente", function() {
                            $(".modalPreventivo").modal('hide');
                            buscarItems();
                        });
                    } else {
                        ErrorCustom('No se pudo guardar, intenta otra vez.');
                    }
                }
            });
        }

        function buscarItems() {
            var url = site_url + "/citas/getTablaItemServiciosOrden";
            ajaxLoad(url, {
                "idorden": $("#id").val(),
                "logueado": logueado,
                "temporal": $("#temporal").val()
            }, "tabla-items", "POST", function() {
                ActualizarTotalesOrden();
            });
        }

        function buscarOperacionesExtras() {
            var url = site_url + "/citas/getTablaOperacionesExtras";
            ajaxLoad(url, {
                "id_cita": $("#id_cita").val()
            }, "div_lista_operaciones", "POST", function() {});
        }

        function voltearFechas() {
            var fecha_recepcion = $("#fecha_recepcion").val();
            var fecha_entrega = $("#fecha_entrega").val();
            if (fecha_recepcion != '') {
                var array_fecha_recepcion = fecha_recepcion.split('-');
                $("#fecha_recepcion").val(array_fecha_recepcion[2] + "/" + array_fecha_recepcion[1] + "/" +
                    array_fecha_recepcion[0]);
            }
            if (fecha_entrega != '') {
                var array_fecha_entrega = fecha_entrega.split('-');
                $("#fecha_entrega").val(array_fecha_entrega[2] + "/" + array_fecha_entrega[1] + "/" + array_fecha_entrega[
                    0]);
            }
        }
        $("#telefono_asesor").prop('readonly', true);
        $("#idcategoria").on("change", function() {
            var url = "<?php echo site_url(); ?>/citas/getSubcategorias";
            $("#idsubcategoria").empty();
            $("#idsubcategoria").append("<option value=''>-- Selecciona --</option>");
            $("#idsubcategoria").attr("disabled", true);

            if ($(this).val() != 0) {
                ajaxJson(url, {
                    "idcategoria": $(this).val()
                }, "POST", "", function(result) {

                    if (result.length != 0) {
                        $("#idsubcategoria").empty();
                        $("#idsubcategoria").removeAttr("disabled");
                        result = JSON.parse(result);
                        $("#idsubcategoria").append("<option value=''>-- Selecciona --</option>");
                        $.each(result, function(i, item) {
                            $("#idsubcategoria").append("<option value= '" + result[i].id + "'>" +
                                result[i].subcategoria + "</option>");
                        });
                    } else {
                        $("#idsubcategoria").empty();

                        $("#idsubcategoria").append("<option value='0'>No se encontraron datos</option>");

                    }
                });
            } else {
                $("#idsubcategoria").empty();
                $("#idsubcategoria").append("<option value=''>-- Selecciona --</option>");
                $("#idsubcategoria").attr("disabled", true);

            }
        });
        $("#descripcion-orden").on('click', function() {
            var url = site_url + "/citas/asignarOrden/0";
            customModal(url, {
                "idorden": $("#id").val(),
                "tipo": 3,
                "temporal": $("#temporal").val(),
                "idoperacion": '0'
            }, "GET", "lg", GuardarItem, "", "Guardar", "Cancelar", "Desgloce presupuesto GEN ", "modal3");
        });

        $("body").on('click', '.js_editar_gen', function(e) {
            e.preventDefault();
            var url = site_url + "/citas/asignarOrden";
            var idoperacion = $(this).data('idop');
            customModal(url, {
                "idorden": $("#id").val(),
                "temporal": $("#temporal").val(),
                "idoperacion": idoperacion,
                "tipo": 3
            }, "GET", "lg", GuardarItem, "", "Guardar", "Cancelar", "Desgloce presupuesto GEN ", "modal3");
        });

        function GuardarItem() {
            var url = site_url + '/citas/asignarOrden';
            var numberOfChecked = $('.operaciones:checked').length;
            if ($("#relacionar_op").prop('checked') && numberOfChecked == 0) {
                ErrorCustom("Es necesario relacionar por lo menos una operación extra");
            } else {
                ajaxJson(url, $("#order-item").serialize(), "POST", "async", function(result) {
                    if (isNaN(result)) {
                        data = JSON.parse(result);
                        $.each(data, function(i, item) {
                            $(".error_" + i).empty();
                            $(".error_" + i).append(item);
                            $(".error_" + i).css("color", "red");
                        });
                    } else {
                        if (result == 1) {
                            ExitoCustom("Guardado correctamente", function() {
                                $(".modal3").modal('hide');
                                buscarItems();
                                if (idorden != 0) {
                                    buscarOperacionesExtras();
                                }

                            });
                        } else {
                            ErrorCustom('No se pudo guardar, intenta otra vez.');
                        }
                    }
                });
            }
        }
        $("body").on('click', ".eliminar_item", function(e) {
            e.preventDefault();
            item = $(this).data('iditem');
            tipo = $(this).data('tipo');
            if (logueado) {
                ConfirmCustom("¿Está seguro de eliminar el registro?", eliminarItem, "", "Confirmar", "Cancelar");
            } else {
                ErrorCustom("Es necesario iniciar sesión para poder eliminar el item");
            }
        });

        function eliminarItem() {
            var url = site_url + '/citas/eliminarItem';

            ajaxJson(url, {
                "item": item,
                "tipo": tipo,
                "idorden": $("#id").val()
            }, "POST", "", function(result) {
                if (result == 1) {
                    ExitoCustom("Eliminado correctamente", function() {
                        buscarItems();
                    });
                } else if (result == -1) {
                    ErrorCustom('La operación no se puede eliminar por que ya inició un proceso');
                } else {
                    ErrorCustom('No se pudo eliminar, intenta otra vez.');
                }
            });
        }
        $("body").on('click', '.seleccionar_unidad', function(e) {
            e.preventDefault();
            var url = site_url + "/citas/datosClienteIntelisisById";
            ajaxJson(url, {
                "id": $(this).data('id')
            }, "POST", "", function(result) {
                result = JSON.parse(result);
                $("#vehiculo_numero_serie").val(result.datos.vin);
                $("#agente").val(result.datos.agente);
                $("#estado").val(result.datos.estado);
                $("#kilometraje").val(result.datos.kilometros);
                $("#calle").val(result.datos.direccion);
                $("#datos_nombres").val(result.datos.nombre);
                $("#vehiculo_placas").val(result.datos.placas);
                $("#telefono_movil").val(result.datos.telefonos);
                $("#email").val(result.datos.correo);
                $("#rfc").val(result.datos.rfc);
                $(".close").trigger('click');
                $("#vehiculo_numero_serie").trigger('change');
            });

        });
        $("#nuevocliente").on('click', function() {
            if ($("#nuevocliente").prop('checked')) {
                $("#cliente_nuevo").val(1);
            } else {
                $("#cliente_nuevo").val(0);
            }
        });

        $("#nuevovehiculo").on('click', function() {
            if ($("#nuevovehiculo").prop('checked')) {
                $("#vehiculo_nuevo").val(1);
            } else {
                $("#vehiculo_nuevo").val(0);
            }
        });


        $("#updOMagic").on('click', function() {
            var orden_magic = $("#orden_magic").val();
            if (orden_magic == '') {
                $(".error_orden_magic").empty().append('Es necesario ingresar el campo');
                $(".error_orden_magic").css("color", "red");
            } else {
                var url = site_url + "/citas/updateOrdenMagic";
                ajaxJson(url, {
                    "orden_magic": orden_magic,
                    "idorden": $("#id").val()
                }, "POST", "", function(result) {
                    if (result == 1) {
                        ExitoCustom("registro actualizado correctamente");
                    } else {
                        ErrorCustom("Error al actualizar el registro");
                    }
                });
            }

        })
        $(".addCategoria").on('click', function() {
            var url = site_url + "/citas/agregar_categoria_modal/0";
            customModal(url, {}, "GET", "md", agregarCategoria, "", "Guardar", "Cancelar", "Agregar categoría",
                "modalCategoria");
        });
        $(".addSubCategoria").on('click', function() {
            var url = site_url + "/citas/agregar_subcategoria_modal/0";
            if ($("#idcategoria").val() != '') {
                customModal(url, {}, "GET", "md", agregarSubCategoria, "", "Guardar", "Cancelar",
                    "Agregar subcategoría", "modalCategoria");
            } else {
                ErrorCustom("Es necesario seleccionar la categoría");
            }
        });

        function agregarCategoria() {
            var url = site_url + "/citas/agregar_categoria_modal";
            ajaxJson(url, {
                "categoria": $("#categoria").val()
            }, "POST", "async", function(result) {
                result = JSON.parse(result);
                if (!result.exito) {
                    $.each(result.errors, function(i, item) {
                        $(".error_" + i).empty();
                        $(".error_" + i).append(item);
                        $(".error_" + i).css("color", "red");
                    });
                } else {
                    ExitoCustom("categoría agregada correctamente", function() {
                        $("#idcategoria").append("<option value='" + result.id + "'>" + result.categoria +
                            "</option>");
                        $("#idcategoria").val(result.id);
                        $(".close").trigger('click');
                        $("#idcategoria").trigger('change');
                    });
                }
            });
        }


        function agregarSubCategoria() {
            var url = site_url + "/citas/agregar_subcategoria_modal";
            ajaxJson(url, {
                "categoria": $("#idcategoria").val(),
                "subcategoria": $("#subcategoria").val(),
                "clave": $("#clave").val()
            }, "POST", "async", function(result) {
                result = JSON.parse(result);
                if (!result.exito) {
                    $.each(result.errors, function(i, item) {
                        $(".error_" + i).empty();
                        $(".error_" + i).append(item);
                        $(".error_" + i).css("color", "red");
                    });
                } else {
                    ExitoCustom("subcategoría agregada correctamente", function() {
                        $("#idsubcategoria").append("<option value='" + result.id + "'>" + result
                            .subcategoria + "</option>");
                        $(".busqueda").select2('destroy')
                        $("#idsubcategoria").val(result.id);
                        $(".busqueda").select2();
                        $(".close").trigger('click');
                    });
                }
            });
        }

        function ActualizarTotalesOrden() {
            ajaxJson(site_url + '/citas/actualizar_totales_orden', $("#frm-total-orden").serialize(), "POST", "", function(
                result_orden) {});
        }

        function getDatosBySerie() {
            var url = site_url + "/citas/getDatosBySerie";
            var id_comparar = $("#id").val();
            if (id_comparar == 0 && ($("#id_cita").val() == '' || $("#id_cita").val() == 0)) {
                if ($("#vehiculo_numero_serie").val() != '') {
                    ajaxJson(url, {
                        "serie": $("#vehiculo_numero_serie").val()
                    }, "POST", "async", function(result) {
                        result = JSON.parse(result);
                        if (result.exito) {
                            $(".busqueda").select2('destroy');
                            llenarCamposNoVacios("email", result.data.datos_email);
                            llenarCamposNoVacios("datos_nombres", result.data.datos_nombres);
                            llenarCamposNoVacios("datos_apellido_paterno", result.data.datos_apellido_paterno);
                            llenarCamposNoVacios("datos_apellido_materno", result.data.datos_apellido_materno);
                            llenarCamposNoVacios("datos_telefono", result.data.datos_telefono);
                            llenarCamposNoVacios("vehiculo_placas", result.data.vehiculo_placas);
                            $("#vehiculo_anio option[value='" + result.data.vehiculo_anio + "']").attr("selected",
                                true);
                            $("#vehiculo_modelo").val(result.data.vehiculo_modelo);
                            $("#id_color option[value='" + result.data.id_color + "']").attr("selected", true);

                            $("#asesor option[value='" + result.data.asesor + "']").attr("selected", true);
                            $(".busqueda").select2();
                            $("#asesor").trigger('change');
                            
                            $("#vehiculo_modelo").trigger('change');
                            $("#submodelo_id").val(result.data.submodelo_id);
                            llenarOrdenByBusqueda();
                        } else {
                            getDataDMS('vin', $("#vehiculo_numero_serie").val());
                        }
                    });
                }
            }
        }

        function actualizarNombreHistorial() {
            $("#datos_nombres").val('');
            $("#datos_apellido_paterno").val('');
            $("#datos_apellido_materno").val('');
            llenarCamposNoVacios('datos_nombres', aPos.datos.nombre);
            llenarCamposNoVacios('datos_apellido_paterno', aPos.datos.ap);
            llenarCamposNoVacios('datos_apellido_materno', aPos.datos.am);
        }

        function getDataUnuevas() {
            var url = site_url + "/citas/datosUnidadesNuevas";
            if ($("#vehiculo_numero_serie").val() != '') {
                ajaxJson(url, {
                    "serie": $("#vehiculo_numero_serie").val()
                }, "POST", "", function(result) {
                    result = JSON.parse(result);
                    if (result.cantidad == 1) {
                        $("#datos_nombres").val(result.datos.cliente);
                        $("#email").val(result.datos.correo);
                        $("#vehiculo_numero_serie").val(result.datos.serie);

                        $("#rfc").val(result.datos.rfc);
                        $("#telefono_movil").val(result.datos.telefono);
                        $("#otro_telefono").val(result.datos.telefono2);
                    }
                });
            } else {}
        }
        if (idorden != 0) {
            ValidarCampania();
        }

        function getDatosBySerieConCita() {
            var url = site_url + "/citas/getDatosBySerie";
            if ($("#vehiculo_numero_serie").val() != '') {
                ajaxJson(url, {
                    "serie": $("#vehiculo_numero_serie").val()
                }, "POST", "async", function(result) {
                    result = JSON.parse(result);
                    if (result.exito) {
                        llenarCamposNoVacios("email", result.data.datos_email);
                        llenarCamposNoVacios("datos_nombres", result.data.datos_nombres);
                        llenarCamposNoVacios("datos_apellido_paterno", result.data.datos_apellido_paterno);
                        llenarCamposNoVacios("datos_apellido_materno", result.data.datos_apellido_materno);
                        llenarCamposNoVacios("datos_telefono", result.data.datos_telefono);
                        llenarCamposNoVacios("vehiculo_placas", result.data.vehiculo_placas);
                        llenarOrdenByBusqueda()
                    }
                });
            }
        }
        $("body").on('click', '.js_no_aplica', function(e) {
            e.preventDefault();
            idop = $(this).data('idop');
            ConfirmCustom("¿Está seguro que esta operación va incluída dentro de la operación principal?",
                NoAplicaOperacion, "", "Confirmar", "Cancelar");
        });

        function NoAplicaOperacion() {
            var url = site_url + '/layout/operacion_no_aplica';
            ajaxJson(url, {
                "idop": idop
            }, "POST", "", function(result) {
                if (result == 1) {
                    buscarItems();
                } else if (result == -1) {
                    ErrorCustom(
                        "Esta acción no se puede realizar por que la operación ya fue asignada a un técnico");
                } else {
                    ErrorCustom("Error, por favor intenta otra vez");
                }
            });
        }
        $("body").on("click", '.js_editar_horarios', function(e) {
            e.preventDefault();
            var url = site_url + "/citas/cambiar_hora_operacion";
            customModal(url, {
                    "principal": $(this).data('principal'),
                    "id_tecnico": $(this).data('id_tecnico'),
                    "id_operacion": $(this).data('idop'),
                    "idorden": $(this).data('idorden'),
                }, "GET", "lg", cambiarHorarioOperacion, "", "Guardar", "Cancelar", "Cambiar horario operación",
                "modal1");
        });

        const cambiarHorarioOperacion = () => {
            if (verificarFormatoHoras()) {
                if (validarHorasIguales()) {
                    var url = site_url + "/citas/cambiar_hora_operacion/0";
                    ajaxJson(url, $("#frm_horas_tecnicos").serialize(), "POST", "", function(result) {
                        if (isNaN(result)) {
                            data = JSON.parse(result);
                            $.each(data, function(i, item) {
                                $(".error_" + i).empty();
                                $(".error_" + i).append(item);
                                $(".error_" + i).css("color", "red");
                            });
                        } else if (result == -1) {
                            ErrorCustom("El técnico ya tiene asignada una cita en ese horario");
                        } else if (result == 0) {
                            ErrorCustom("Error al asignar el técnico, por favor intenta de nuevo.");
                        } else if (result == -2) {
                            ErrorCustom(
                                'La fecha de asignación al técnico debe ser mayor o igual a la fecha del asesor'
                            );
                        } else if (result == -3) {
                            ErrorCustom('El técnico no labora en la hora seleccionada');
                        } else if (result == -5) {
                            ErrorCustom('El horario es parte del horario de comida del técnico');
                        } else if (result == -6) {
                            ErrorCustom(
                                'Solamente puedes cambiar el horario de una cita que se encuentra en estatus planeado'
                            );
                        } else {
                            ExitoCustom("Horario asignado correctamente", function() {
                                $(".close").trigger("click");
                                buscarItems();
                            });
                        }
                    });
                } else {
                    ErrorCustom("La hora de inicio de trabajo y hora fin NO pueden ser iguales");
                }
            } else {
                ErrorCustom("Es necesario revises el formato de las horas");
            }
        }

        function llenarOrdenByBusqueda() {
            var url = site_url + "/citas/getDatosOrdenBySerie";
            if ($("#vehiculo_numero_serie").val() != '') {
                ajaxJson(url, {
                    "serie": $("#vehiculo_numero_serie").val()
                }, "POST", "async", function(result) {
                    result = JSON.parse(result);
                    if (result.exito) {
                        console.log(result);
                        $.each(result.orden, function(i2, item2) {
                            if (i2 != 'fecha_recepcion' && i2 != 'hora_recepcion' && i2 !=
                                'fecha_entrega' && i2 != 'hora_entrega' && i2 != 'id' && i2 != 'id_cita' &&
                                i2 != 'vehiculo_kilometraje' && i2 != 'temporal' && i2 != 'asesor' && i2 !=
                                'telefono_asesor' && i2 != 'extension_asesor' && i2 != 'extension_asesor' &&
                                i2 != 'agente' && i2 != 'color' && i2 != 'numero_interno' && i2 !=
                                'numero_cliente' && i2 != 'id_uen' && i2 != 'comentarios_servicio' && i2 !=
                                'vehiculo_modelo')
                                llenarCamposNoVacios(i2, item2)
                        });

                        $("#idcategoria").trigger('change');
                        $("#idsubcategoria").val(result.orden.idsubcategoria);
                        $("#numero_cliente").val(result.orden.numcliente);
                    }
                });
            }
        }
        //Estatus orden
        $("body").on("click", '.js_asociar_operacion', function(e) {
            e.preventDefault();
            id = $(this).data('idop');
            idorden = $(this).data('idorden');
            id_cita = $("#id_cita").val();
            var url = site_url + "/orden/asociar_operacion_folio/0";
            customModal(url, {
                    "id": id,
                    "idorden": idorden,
                    "id_cita":id_cita
                }, "POST", "md", asociarOperacionFolio, "", "Guardar", "Cancelar",
                "Asociar folio a operación", "modalAsociarOperacion");

        });

        function asociarOperacionFolio() {
            var url = site_url + "/orden/ingresarFolioOperacion/";
            if ($("#operacion_folio").val() != '') {
                const operacion_folio = $("#operacion_folio").val();
                ajaxJson(url, {
                    "folio": operacion_folio,
                    "idorden": idorden,
                    "idop": id
                }, "POST", "", function(result) {
                    if (result == 0) {
                        ErrorCustom('Error al asignar el folio, intenta otra vez');
                    } else {
                        ExitoCustom('Folio asignado correctamente', function() {
                            buscarItems();
                            $(".close").trigger('click');
                        });
                    }
                });
            } else {
                $(".error_operacion_folio").empty().append('El folio es requerido');
            }
        }

    </script>
@endsection
