@layout('tema_luna/layout')
@section('contenido')
	<ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="#">Inicio</a>
	    </li>
	    <li class="breadcrumb-item active">Técnicos</li>
  	</ol>
	<div class="row">
		<div class="col-sm-8">
			<h1>Lista de Técnicos</h1>
		</div>
		<div class="col-sm-4">
			<button id="agregar_tecnico" class="btn btn-success pull-right">Agregar técnico</button>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div id="div_tecnicos">
				<table id="tbl_tecnicos" class="table table-bordered table-striped" cellpadding="0" width="100%">
					<thead>
						<tr class="tr_principal">
							<th>Nombre</th>
							<th>Usuario</th>
							<th>IdStar</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($citas as $c => $value)
						<tr>
							<td>{{$value->nombre}}</td>
							<td>{{$value->nombreUsuario}}</td>
							<td>{{$value->idStars}}</td>
							<td>
								<a href="" data-id="{{$value->id}}" class="js_editar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar">
									<i class="pe pe-7s-note"></i>
								</a>

								<a href="{{base_url('citas/horarios_tecnico/'.$value->id)}}" data-id="{{$value->id}}" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Citas">
									<i class="pe-7s-menu"></i>
								</a>
								@if($value->activo)
									<a href="" data-id="{{$value->id}}" class=" js_activar" data-valor="0" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Desactivar técnico">
										<i class="pe-7s-switch"></i>
									</a>
								@else
									<a href="" data-id="{{$value->id}}" class="js_activar" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activar técnico">
										<i class="pe-7s-check"></i>
									</a>
								@endif
								@if($value->motivo!='')
									<a href="" class="fas fa-comments" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="{{$value->motivo}}"></a>
								@endif
								
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('scripts')

	<script>
		var site_url = "{{site_url()}}";
		var valor = '';
		var mensaje = '';
		var id = '';
		inicializar_tabla("#tbl_tecnicos",false);
	$("#agregar_tecnico").on("click",function(){
       var url =site_url+"/citas/agregar_tecnico/0";
       customModal(url,{},"GET","md",callbackGuardar,"","Guardar","Cancelar","Nuevo técnico","modal1");
    });
    $("body").on("click",'.js_editar',function(e){
    	e.preventDefault();
       var id = $(this).data('id')
       var url =site_url+"/citas/agregar_tecnico/"+id;
       customModal(url,{},"GET","md",callbackGuardar,"","Guardar","Cancelar","Nuevo técnico","modal1");
      
    });
    $("body").on("click",'.js_eliminar',function(e){
       e.preventDefault();
       var id = $(this).data('id')
       ConfirmCustom("¿Está seguro de eliminar el técnico?", callbackEliminarOperador(id),"", "Confirmar", "Cancelar");
    });
    $("body").on("click",'.js_activar',function(e){
       e.preventDefault();
       //valor es 0 cuando va desactivar y 1 cuando lo va activar
       valor = $(this).data('valor');
       if(valor==1){
       	mensaje ="¿Está seguro de activar el técnico?";
       }
       id = $(this).data('id');
       if(valor==1){
       	 ConfirmCustom(mensaje, callbackActivarDesactivar,"", "Confirmar", "Cancelar");
       	}else{
       		 var url =site_url+"/citas/agregar_motivo/";
       		customModal(url,{},"GET","md",callbackActivarDesactivar,"","Guardar","Cancelar","Motivo para desactivar","modal3");
       	}
    });

    
	function callbackGuardar(){
		var url =site_url+"/citas/agregar_tecnico";
		ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
			if(isNaN(result)){
				data = JSON.parse( result );
				//Se recorre el json y se coloca el error en la div correspondiente
				$.each(data, function(i, item) {
					 $.each(data, function(i, item) {
	                    $(".error_"+i).empty();
	                    $(".error_"+i).append(item);
	                    $(".error_"+i).css("color","red");
	                });
				});
			}else{
				if(result <0){
					ErrorCustom('El nombre del técnico ya fue registrado, por favor intenta con otro');
				}else{
					if(result==0){
						ErrorCustom('No se pudo guardar el técnico, por favor intenta de nuevo');
					}else{
						ExitoCustom("Guardado correctamente",function(){
						$(".close").trigger("click");
							buscar();
						});
					}
				}
			}
		});
	}
	function callbackEliminarOperador(id){
		var url =site_url+"/citas/eliminar_operador/";
		ajaxJson(url,{"id":id},"POST","",function(result){
			console.log(result);
			if(result ==0){
					ErrorCustom('El operador no se pudo eliminar, por favor intenta de nuevo');
				}else{
					ExitoCustom("Operador eliminado correctamente",function(){
					$(".close").trigger("click");
						buscar();
					});	
				}
		});
	}

	function buscar(){
		var url =site_url+"/citas/tabla_tecnicos";
        ajaxLoad(url,{},"div_tecnicos","POST",function(){
    		inicializar_tabla("#tbl_tecnicos",false);
    		$('[data-toggle="tooltip"]').tooltip();
      });
	}
	function callbackActivarDesactivar(){
		
		if(valor==1){
			 mensaje ="Técnico activado correctamente";
			activar_desactivar();
		}else{
			 mensaje ="Técnico desactivado correctamente";
			var motivo = $("#motivo").val();
			if(motivo==''){
				ErrorCustom("Es necesario ingresar el motivo por el que se desactiva al técnico");	
			}else{
				 mensaje ="Técnico desactivado correctamente";
				activar_desactivar();
			}
			
		}
		
	}
	function activar_desactivar(){
		var url =site_url+"/citas/cambiar_status/";
		ajaxJson(url,{"id":id,"valor":valor,"tabla":'tecnicos',"motivo":$("#motivo").val()},"POST","",function(result){
			if(result ==0){
					ErrorCustom('Error al activar o desactivar el técnico, por favor intenta de nuevo');
				}else{
					ExitoCustom(mensaje,function(){
					$(".close").trigger("click");
						buscar();
					});	
				}
		});
	}
	</script>
@endsection