<table width="100%" class="table table-bordered table-striped table-striped table-responsive">
  <thead>
  	<th>Cantidad</th>
    <th>Descripción</th>
    <th># Pieza</th>
  </thead>
  <tbody>
    @if($confirmada)
      @if(count($items)>0)
        @foreach($items as $i => $item)
          <tr>
            <td width="15%">{{number_format($item->cantidad,2)}}</td>
            <td width="25%">{{$item->descripcion}}</td>
            <td width="10%">{{$item->numero_pieza}}</td>
          </tr>
        @endforeach
      @endif
    @endif
  
    <!--lo de la multipunto aceptado -->
    @if(count($cotizacion_multipunto['cotizacion'])>0)
      @foreach($cotizacion_multipunto['cotizacion'] as $c => $cotizacion)
         <tr>
            <td width="15%">{{number_format($cotizacion['cantidad'],2)}}</td>
            <td width="25%">{{$cotizacion['descripcion']}}</td>
            <td width="10%">{{$cotizacion['pieza']}}</td>
          </tr>
      @endforeach
    @endif
  </tbody>
</table>  