<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_citas extends CI_Model{
  public $dias_transcurridos = 0;
  public $horas_transcurridos = 0;
  public $minutos_transcurridos = 0;
    public function __construct(){
        parent::__construct();
        date_default_timezone_set(CONST_ZONA_HORARIA);
        $this->load->model('layout/m_layout','ml');
    }
  //obtiene los operadores
  public function getOperadores(){
    return $this->db->where('baja_definitiva',0)->where('activo',1)->where('mostrar_tablero',1)->get('operadores')->result();
  }
  //guarda los operadores
  public function guardarOperador(){
     $id= $this->input->post('id');
     $datos = array('nombre' => $this->input->post('nombre'),
                    'telefono' => $this->input->post('telefono'),
                    'correo' => $this->input->post('correo'),
                    'rfc' => $this->input->post('rfc'),
                    'horario' => $this->input->post('horario'),
                    'fecha_creacion' =>date('Y-m-d')
      );
      $q= $this->db->where('nombre',$datos['nombre'])->where('id !=',$id)->get('operadores');
      if($q->num_rows()==1){
        echo -1;exit();
      }
      if($id==0){
          $exito = $this->db->insert('operadores',$datos);
      }else{
          $exito = $this->db->where('id',$id)->update('operadores',$datos);
      }
        if($exito){
          echo 1;die();
        } else{
          echo 0; die();
        }
  }
  //obtener el operador en base a su id
  public function getOperadorId($id=0){
    return $this->db->where('id',$id)->get('operadores')->result();
  }
  //obtener las citas de un operador
  public function getCitasOperador($id,$fecha=''){
    if($fecha!=''){
      $this->db->where('fecha',$fecha);
    }
    return $this->db->where('id_operador',$id)->order_by('fecha','desc')->order_by('hora','desc')->get('aux')->result();
  }
  public function getNameOperador($id=0){
    $q = $this->db->where('id',$id)->select('nombre')->from('operadores')->get();
    if($q->num_rows()==1){
      return $q->row()->nombre;
    }else{
      return '';
    }
  }
  //guarda el horario
  public function guardarHorario(){
      $id_operador= $this->input->post('id'); // id del operador
      $id_cita= $this->input->post('id_cita'); // id del operador
      //debug_var($_POST);die();
      $horario = date_eng2esp_1($this->input->post('horario'));

      $fecha_separada = explode('/', $horario);
      $datos = array('hora' => $this->input->post('hora'),
                     'id_operador' =>$id_operador,
                     'dia' =>$fecha_separada[0],
                     'mes' =>$fecha_separada[1],
                     'anio' =>$fecha_separada[2],
                     'fecha_creacion'=>date('Y-m-d'),
                     'fecha'=>($horario)
      );
      //debug_var($datos);die();
      $q= $this->db->where('hora',$datos['hora'])->where('dia',$fecha_separada[0])->where('mes',$datos['mes'])->where('anio',$datos['anio'])->where('id_operador',$id_operador)->get('aux');

      //echo $this->db->last_query();die();
      if($q->num_rows()==1){
        echo -1;exit();
      }
      if($id_cita==0){
          $exito = $this->db->insert('aux',$datos);
      }else{
          $exito = $this->db->where('id',$id_cita)->update('aux',$datos);
      }
        if($exito){
          echo 1;die();
        } else{
          echo 0; die();
        }
  }
  public function getIdAsesor($asesor=''){
    $q = $this->db->where('nombre',$asesor)->select('id')->from('operadores')->get();
    if($q->num_rows()==1){
      return $q->row()->id;
    }else{
      return '';
    }
  }
  public function getFechasAsesor($id_asesor,$fecha=''){
    if($fecha!=''){
      if($fecha>=date('Y-m-d')){
        $this->db->where('fecha >=',date('Y-m-d'));
      }else{
        $this->db->where('fecha >=',$fecha);
      }

    }else{
      $this->db->where('fecha >=',date('Y-m-d'));
    }

    return $this->db->where('id_operador',$id_asesor)->order_by('fecha','asc')->select('distinct(fecha)')->get('aux')->result();
  }
  public function getHorariosAsesor($id_asesor='',$fecha=''){
    date_default_timezone_set(CONST_ZONA_HORARIA);
    //date_default_timezone_set('America/Mazatlan');
    $hora = date('H').":".date('i');
    //echo $hora;die();
    if($fecha==date('Y-m-d')){
       $this->db->where('hora >=',$hora);
    }

      return $this->db->where('id_operador',$id_asesor)->where('activo',1)->order_by('hora')->where('ocupado',0)->where('fecha',$fecha)->get('aux')->result();
  }
  //obtener los horarios cuando está editando por que ocupa el id del horario
  public function getHorariosAsesor_edit($id_asesor='',$fecha='',$id_horario=''){
    $query = "SELECT * FROM aux WHERE id_operador = ".$id_asesor." AND (ocupado =0  or id=".$id_horario.") and fecha = '".$fecha."' and activo='1' order by hora ";
    return $this->db->query($query)->result();
  }
  //Funcion para guardar cada vez que reagendan
  public function saveHistorialReagendadas($id_cita=''){
    $idstatus= $this->getStatusCitaPrincipal($id_cita);
    $idhorario= $this->getIdHorarioActual($id_cita);
    $datos = array('created_at' =>date('Y-m-d H:i:s') ,
                   'id_usuario'=> $this->session->userdata('id_usuario'),
                   'id_horario'=>$idhorario,
                   'id_status'=>$idstatus,
                   'id_cita'=>$id_cita
     );
    $this->db->insert('historial_reagendadas',$datos);
    $this->db->where('id_cita',$id_cita)->set('fecha_reagendada',date('Y-m-d'))->update('citas');

  }
  //Funcion para guardarHistorial de las ediciones de las citas
 public function saveHistorialEditadas($id_cita=''){
    $idstatus= $this->getStatusCitaPrincipal($id_cita);
    $idhorario= $this->getIdHorarioActual($id_cita);
    $cambio = '';
    //debug_var($this->getDatosTecnicosCitas($id_cita));die();
    foreach ($this->getDatosTecnicosCitas($id_cita) as $key => $value) {

     $cambio.="id_tecnico: ".$this->getNameTecnico($value->id_tecnico).' hora_inicio: '.$value->hora_inicio.' hora_fin: '.$value->hora_fin.' fecha: '.$value->fecha.' dia_completo: '.$value->dia_completo.' fecha_fin: '.$value->fecha_fin.' hora_inicio_dia: '.$value->hora_inicio_dia.' activo: '.$value->activo.' historial: '.$value->historial.' <br> ';
    }
    $datos = array('created_at' =>date('Y-m-d H:i:s') ,
                   'id_usuario'=> $this->session->userdata('id_usuario'),
                   'id_horario'=>$idhorario,
                   'id_status'=>$idstatus,
                   'id_cita'=>$id_cita,
                   'id_tecnico' => $this->getTecnicoByCita($id_cita),
                   'cambio_tecnicos' =>$cambio
     );
    $this->db->insert('historial_ediciones',$datos);

  }
  //Funcion para obtener los datos de tecnicos citas
  public function getDatosTecnicosCitas($id_cita=''){
    return $this->db->where('id_cita',$id_cita)->get('tecnicos_citas')->result();
  }
  public function getPrepikingPreventivo($id_cita='',$prepiking='',$vehiculo_anio='',$id_modelo_prepiking=''){
   $partes = $this->db->where('codigo_ereact',$prepiking)
                            ->where('anio',$vehiculo_anio)
                            ->where('parte !=','N/A')
                            ->where('idmodelo',$id_modelo_prepiking)
                            ->where('p.parte !=','Mano de Obra')
                            ->join('preventivo_descripcion d','p.id_descripcion = d.id')
                            ->join('preventivo_paquetes pp','pp.id = p.id_paquete')
                            ->select('distinct(d.descripcion)as descripcion,parte,tiempo,costo,cantidad,precio')
                            ->get('preventivo p')
                            ->result();     
              
  //guardar el prepiking
  foreach ($partes as $p => $parte) {
    $prepiking_refacciones = array('codigo' => $parte->parte,
                        'descripcion' => $parte->descripcion,
                        'id_cita' => $id_cita,
                        'id_usuario' => $this->session->userdata('id_usuario'),
                        'cantidad' => $parte->cantidad,
                        'tiempo' => $parte->tiempo,
                        'costo' => $parte->costo,
    );
    $this->db->insert('prepiking_refacciones',$prepiking_refacciones);

    $datos_prepedido = array(
              'codigo' => $parte->parte,
              'descripcion' => $parte->descripcion,
              'id_cita' => $id_cita,
              'id_usuario' => $this->session->userdata('id_usuario'),
              'created_at' =>date('Y-m-d H:i:s'),
              'precio' => $parte->costo,
              'cantidad' => $parte->cantidad,
    );
    $this->db->insert('prepiking_extra_cita',$datos_prepedido);

  }                          
 }
  public function guardarCita(){
    $info = $_POST['citas'];
    $id= $this->input->post('id');

    //Guardar historial si reagendan
    if($this->input->post('reagendada')==1){
      $this->saveHistorialReagendadas($id);
    }
    //Fin historial
      //Guardar historial cuando editan
    if($id!=0){
      $this->saveHistorialEditadas($id);
    }

    //Para que se cree una nueva cuando es reagendada
    if($this->input->post('reagendada')==1){
      $id=0;
    }

    //Para que se cree una nueva cuando es instalación
    if($this->input->post('id_instalacion')==1){
      $id=0;
    }
    if($this->input->post('origen')=='tablero'){
       $origen_save = 2; // viene del tablero
      $info['fecha'] = $info['fecha'];
      $id_opcion_cita = 2;
    }else{
      $id_opcion_cita = $info['id_opcion_cita'];
      $origen_save = 0;
    }
    $fecha_separada = explode('-', $info['fecha']);
    $fecha_anio = isset($fecha_separada[0])?$fecha_separada[0]:null;
    $fecha_mes = isset($fecha_separada[1])?$fecha_separada[1]:null;
    $fecha_dia = isset($fecha_separada[2])?$fecha_separada[2]:null;
    $id_horario_save = isset($info['horario'])?$info['horario']:null;

    //Hacer validación de que no esté ocupado
    if($id_horario_save !=null && $id==0){
      $qhorario = $this->db->select('ocupado')->from('aux')->where('id',$id_horario_save)->get();
      //echo $this->db->last_query();die();
      if($qhorario->num_rows()==1){
        if($qhorario->row()->ocupado){
            echo -4;die();
        }
      }
    }
    $datos = array('transporte' => isset($info['transporte'])?$info['transporte']:null,
                   'email' => strtolower($info['email']),
                   'vehiculo_anio' => $info['vehiculo_anio'],
                   //'vehiculo_marca' => $info['vehiculo_marca'],
                   'vehiculo_modelo' => $info['vehiculo_modelo'],
                   'submodelo_id' => $info['submodelo_id'],
                   //'vehiculo_version' => $info['vehiculo_version'],
                   'vehiculo_placas' => $info['vehiculo_placas'],
                   'vehiculo_numero_serie' => $info['vehiculo_numero_serie'],
                   'comentarios_servicio' => $info['comentarios_servicio'],
                   'asesor' => $info['asesor'],
                   'id_asesor' => isset($info['asesor'])?$this->getIdAsesor($info['asesor']):'',
                   'fecha_anio' =>$fecha_anio,
                   'fecha_mes' => $fecha_mes,
                   'fecha_dia' => $fecha_dia,
                    'datos_email' => strtolower($info['email']),
                    'datos_nombres' => $info['datos_nombres'],
                    'datos_apellido_paterno' => $info['datos_apellido_paterno'],
                    'datos_apellido_materno' => $info['datos_apellido_materno'],
                    'datos_telefono' => $info['datos_telefono'],
                    'id_prepiking'=>isset($info['id_prepiking'])?$info['id_prepiking']:'',
                    'id_modelo_prepiking'=>isset($info['id_modelo_prepiking'])?$info['id_modelo_prepiking']:'',
                    'prepiking'=>isset($info['id_prepiking'])?$info['id_prepiking']:'',
                    'fecha_creacion' => date('Y-m-d'),
                    'status' =>0,
                    'servicio' => isset($info['servicio'])?$info['servicio']:null,
                    'id_horario' => $id_horario_save,
                    'id_opcion_cita' => $id_opcion_cita,
                    'id_color' => $info['id_color'],
                    'fecha' => $fecha_anio.'-'.$fecha_mes.'-'.$fecha_dia,
                    'id_status_color' => $info['id_status_color'],
                    'fecha_hora' =>$fecha_anio.'-'.$fecha_mes.'-'.$fecha_dia.' '.$this->getHora(isset($info['horario'])?$info['horario']:null),
                    'origen' => $origen_save,
                    'reagendada' => ($this->input->post('reagendada')!='')?$this->input->post('reagendada'):0,
                    //'id_usuario' => $this->session->userdata('id_usuario'),
                    //'duda' => $this->input->post('duda'),
                    //'demo' => $this->input->post('demo'),
                    'historial'=>0,
                    'cancelada'=>0,
                    'vehiculo_kilometraje' => isset($info['vehiculo_kilometraje'])?$info['vehiculo_kilometraje']:null,
                    'id_usuario_reagendo'=>($this->input->post('reagendada')==1)?$this->session->userdata('id_usuario'):null,
                     'numero_cliente' => isset($info['numero_cliente'])?$info['numero_cliente']:null,
                    'cita_previa'=>1,
                    'xehos' => isset($_POST['xehos'])?$_POST['xehos']:0
     );

    if($id==0){
      $datos['id_tecnico'] = $this->input->post('tecnico');
      $datos['fecha_creacion_all'] = date('Y-m-d H:i:s');
      $datos['id_usuario'] = $this->session->userdata('id_usuario');
      $datos['id_status'] = 1;
    }

      if($id==0){
           if(isset($_POST['dia_completo'])){
            $fecha_inserto = $fecha_anio.'-'.$fecha_mes.'-'.$fecha_dia;
              $dia_completo = 1;

              if($this->input->post('fecha_inicio')!='' && $this->input->post('fecha_fin')!=''){
                $fecha_inicio =$this->input->post('fecha_inicio');
                $fecha_fin = $this->input->post('fecha_fin');
                  //Validar que no esté ocupado

                if(!$this->validarHorarioOcupado( $fecha_inicio,$fecha_fin,$this->input->post('tecnico_dias'),$this->input->post('hora_comienzo'))){
                  //echo debug_last_query();die();
                  echo -1;die();
                }
                //Validar que la fecha del técnico sea mayor a la del asesor
                if($fecha_inicio<$fecha_inserto){
                  echo -2;exit();
                }
                //Fin validación

                //Validar que sea hora laboral del técnico
                if(!$this->getHoraLaboralTecnico($this->input->post('hora_comienzo'),$this->input->post('fecha_inicio'),$this->input->post('tecnico_dias'))){
                  echo -3;exit();
                }


                //Fin validación

              }
              $exito = $this->db->insert('citas',$datos);
              $id_cita = $this->db->insert_id();
              //insertar prepiking 
              $this->getPrepikingPreventivo($id_cita,$info['id_prepiking'],$info['vehiculo_anio'],$info['id_modelo_prepiking']);
               //SI INGRESO EL TECNICO GUARDARLO
               if($this->input->post('tecnico_dias')!=''){
                $this->db->where('id_cita',$id_cita)->set('id_tecnico',$this->input->post('tecnico_dias'))->update('citas');
                if($this->input->post('hora_comienzo')==''){
                  $hora_comienzo = '07:00';
                }else{
                   $hora_comienzo = $this->input->post('hora_comienzo');
                }
                if($this->input->post('fecha_inicio')!='' && $this->input->post('fecha_fin')!=''){
                   $horarios_tecnicos_completos = array('fecha' => $fecha_inicio,
                                           'hora_inicio' =>$hora_comienzo,
                                           'hora_fin' =>'20:00',
                                           'id_cita' =>$id_cita,
                                           'id_tecnico'=> $this->input->post('tecnico_dias'),
                                           'dia_completo'=>$dia_completo,
                                           'fecha_fin'=>$fecha_fin,
                                           'hora_inicio_dia' => $hora_comienzo
                    );
                }
                  // si llega extra de la fecha
                  if($this->input->post('hora_inicio_extra') !='' && $this->input->post('hora_fin_extra') !=''){
                     //Validar que la fecha del técnico sea mayor a la del asesor
                    if($this->input->post('fecha_parcial')<$fecha_inserto){
                      echo -2;exit();
                    }
                    if(!$this->validarDiaExtra($this->input->post('hora_inicio_extra'),$this->input->post('hora_fin_extra'),$this->input->post('fecha_parcial'),$this->input->post('tecnico_dias'))){
                    }
                    //Fin validación
                    //Validar que sea hora laboral del técnico
                    if(!$this->getHoraLaboralTecnico($this->input->post('hora_inicio_extra'),$this->input->post('fecha_parcial'),$this->input->post('tecnico_dias'),$this->input->post('hora_fin_extra'))){
                      echo -3;exit();
                    }


                    //Fin validación

                    $horarios_tecnicos = array('fecha' => $this->input->post('fecha_parcial'),
                                             'hora_inicio' =>$this->input->post('hora_inicio_extra'),
                                             'hora_fin' =>$this->input->post('hora_fin_extra'),
                                             'id_cita' =>$id_cita,
                                             'id_tecnico'=> $this->input->post('tecnico_dias'),
                                             //'dia_completo'=>$dia_completo,
                                             'dia_completo'=>0,
                                             'fecha_fin'=>$this->input->post('fecha_parcial'),
                                             'hora_inicio_dia' => $this->input->post('hora_inicio_extra')
                      );

                    if(!$this->validarDiaExtra($horarios_tecnicos['hora_inicio'],$horarios_tecnicos['hora_fin'],$horarios_tecnicos['fecha'],$this->input->post('tecnico_dias'))){
                      echo -1;die();
                    }

                    $this->db->insert('tecnicos_citas',$horarios_tecnicos);
                  }
                if($this->input->post('fecha_inicio')!='' && $this->input->post('fecha_fin')!=''){
                  $this->db->insert('tecnicos_citas',$horarios_tecnicos_completos);
                }

                }
            }else{
              $dia_completo = 0;
              $fecha_inicio =$info['fecha'];
              $fecha_fin = $info['fecha'];
              $fecha_inserto = $fecha_anio.'-'.$fecha_mes.'-'.$fecha_dia;
              if($this->input->post('tecnico')!=''){

                  //VERIFICAR SI NO TIENE YA LA CITA

                 if(!$this->validarDiaExtra($this->input->post('hora_inicio'),$this->input->post('hora_fin'),$info['fecha'],$this->input->post('tecnico'))){
                   //echo 'entre';die();
                      echo -1;die();
                    }
                   //Validar que la fecha del técnico sea mayor a la del asesor
                  if($fecha_inicio<$fecha_inserto){
                    echo -2;exit();
                  }

                  //Fuera del horario de comida inicio
                    if(!$this->validarCitaHoraComida($this->input->post('hora_inicio'),$info['fecha'],$this->input->post('tecnico'),true)) {
                      echo -5;exit();
                    }

                   //Fuera del horario de comida fin
                    if(!$this->validarCitaHoraComida($this->input->post('hora_fin'),$info['fecha'],$this->input->post('tecnico'),false)) {
                      echo -5;exit();
                    }
                  //Fin validación

                  //Validar que sea hora laboral del técnico
                  if(!$this->getHoraLaboralTecnico($this->input->post('hora_inicio'),$fecha_inserto,$this->input->post('tecnico'),$this->input->post('hora_fin'))){
                    echo -3;exit();
                  }
                  //Fin validación

                  }

              $exito = $this->db->insert('citas',$datos);
              $id_cita = $this->db->insert_id();

               //insertar prepiking 
              $this->getPrepikingPreventivo($id,$info['id_prepiking'],$info['vehiculo_anio'],$info['id_modelo_prepiking']);

               //SI INGRESO EL TECNICO GUARDARLO
               if($this->input->post('tecnico')!=''){
                $horarios_tecnicos = array('fecha' => $fecha_inicio,
                                           'hora_inicio' =>$this->input->post('hora_inicio'),
                                           'hora_fin' =>$this->input->post('hora_fin'),
                                           'id_cita' =>$id_cita,
                                           'id_tecnico'=> $this->input->post('tecnico'),
                                           'dia_completo'=>$dia_completo,
                                           'fecha_fin'=>$fecha_fin,
                                           'hora_inicio_dia' => $this->input->post('hora_inicio')

                    );
                  $this->db->insert('tecnicos_citas',$horarios_tecnicos);
                }
            }

      } else{
          $id_horario_anterior = $this->getIdHorarioActual($id);
          $this->db->where('id',$id_horario_anterior)->set('ocupado',0)->update('aux');
          $exito = $this->db->where('id_cita',$id)->update('citas',$datos);


          //actualizar el campo de ocupado en el horario
          $this->db->where('id_cita',$id)->set('realizo_servicio',$this->input->post('realizo_servicio'))->update('citas');

           //actualizar historial tecnico citas
          $this->db->where('id_cita',$id)->set('historial',0)->update('tecnicos_citas');
           //eliminar lo de prepiking
          if(!$_POST['realizo_prepiking']){
            $this->db->where('id_cita',$id)->delete('prepiking_refacciones');  
          }
          
          //insertar prepiking 
          $this->getPrepikingPreventivo($id,$info['id_prepiking'],$info['vehiculo_anio'],$info['id_modelo_prepiking']);
      }

      //Enviar correo al usuario
      // $publicidad = $this->db->select('url')->get('publicidad')->result();
       //Enviar correo al usuario
      $datos['telefono_asesor_ws'] = $this->principal->getGeneric('nombre',$datos['asesor'],'operadores')->telefono;
      if($id==0){
        correo_seguimiento_cita($datos['datos_email'],encrypt($id_cita),'Notificación y Seguimiento en Línea de Citas',($this->input->post('reparacion')!='')?1:0);
        //Actualizar la fecha a 6 meses más
        if($_POST['magic']!=0){
          $date = date('Y-m-d');
          //$newdate = strtotime ( '+6 month' , strtotime ( $date ) ) ;
          //$date = date ( 'Y-m-d' , $newdate );
          
          $dia = date("D", strtotime($date)); 
          //Si el día es sábado agregar un día, si es domingo 2

          if($dia=='Sat'){
            $newdate = strtotime ( '+2 day' , strtotime ( $date ) ) ;
            $date = date ( 'Y-m-d' , $newdate );
          }

          if($dia=='Sun'){
            $newdate = strtotime ( '+1 day' , strtotime ( $date ) ) ;
            $date = date ( 'Y-m-d' , $newdate );
          }
          //REGRESAR CUANDO YA TENGA 6 MESES
          // $array_historial_proactivo = array('id_cita' => $id_cita, 
          //                                    'id_cita'=>$_POST['magic'],
          //                                    'created_at'=>date('Y-m-d H:i:s'),
          //                                    'id_usuario' => $this->session->userdata('id_usuario'),
          // );
          // $this->db->insert('proactivo_historial_citas',$array_historial_proactivo);
          // $this->db->where('idcita',$_POST['magic'])->set('intentos',0)->set('fecha_programacion',$date)->update('proactivo_citas');

          $array_historial_proactivo = array('id_cita' => $id_cita, 
                                             'idproactivo'=>$_POST['magic'],
                                             'created_at'=>date('Y-m-d H:i:s'),
                                             'id_usuario' => $this->session->userdata('id_usuario'),
          );
          $this->db->insert('proactivo_historial_citas',$array_historial_proactivo);
          $this->db->where('idproactivo',$_POST['magic'])->set('intentos',0)->set('fecha_programacion',$date)->update('proactivo_inicial');

        }
        //Es una instalación
        if($_POST['id_instalacion']!=0){
          $detalle_prepedido = $this->curl->getinfoGet(CONST_DETALLE_PREPEDIDOS.'/'.$_POST['id_instalacion']);
          //Guardar pre pepiking cuando es una instalación
          foreach ($detalle_prepedido as $d => $detalle) {
            $datos_prepedido = array(
              'codigo' => $detalle->no_identificacion,
              'descripcion' => $detalle->descripcion,
              'id_cita' => $id_cita,
              'id_usuario' => $this->session->userdata('id_usuario'),
              'created_at' =>date('Y-m-d H:i:s'),
              'precio' => $detalle->precio,
              'cantidad' => $detalle->cantidad,
              );
            $this->db->insert('prepiking_extra_cita',$datos_prepedido);
          }
          //Actualizar el id de cita en el pre pedido
          $this->curl->getInfoPUT(CONST_ACTUALIZAR_CITA_PREPEDIDO.'/'.$_POST['id_instalacion'],array('servicio_cita_id'=>$id_cita));
        }
        //Actualizar las refacciones autorizadas en caso de que vengan
        if(isset($_POST['refacciones'])){
          $this->md->updateRefaccionesAutorizadas($_POST['refacciones']);
        }
      }else{
        correo_seguimiento_cita($datos['datos_email'],encrypt($id),'Notificación y Seguimiento en Línea de Citas',($this->input->post('reparacion')!='')?1:0);
      }

      if($exito){
        $this->db->where('id',isset($info['horario'])?$info['horario']:null)->set('ocupado',1)->update('aux');
        if($id==0){
         $this->db->where('id',isset($info['horario'])?$info['horario']:null)->set('id_status_cita',1)->update('aux');
      }
       echo 1;die();
      } else{
        echo 0; die();
      }
  }
  //Cuando es cliente nuevo
  public function saveNewCustom($id_cita=''){
    $contenedor["id_cita"] = $id_cita;
    $ch = curl_init(CONST_CLIENTE_NUEVO);
    $jsonDataEncoded = json_encode($contenedor);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonDataEncoded);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $response = curl_exec($ch);
    curl_close($ch);
  }
  //Cuando es cliente nuevo
  public function saveNewVeh($id_cita=''){
    $contenedor["id_cita"] = $id_cita;
    $ch = curl_init(CONST_VEHICULO_NUEVO);
    $jsonDataEncoded = json_encode($contenedor);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonDataEncoded);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $response = curl_exec($ch);
    curl_close($ch);
  }
  public function getHora($id_horario=''){
    $q = $this->db->where('id',$id_horario)->select('hora')->get('aux');
    if($q->num_rows()==1){
      return $q->row()->hora;
    }else{
      return '';
    }

  }
  public function getIdHorarioActual($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->where('activo',1)->select('id_horario')->get('citas');
    if($q->num_rows()==1){
      return $q->row()->id_horario;
    }else{
      return '';
    }
  }
  //obtener la cita en base a su id
  public function getCitaId($id=0){
    return $this->db->where('id_cita',$id)->get('citas')->result();
  }
  public function getHorarioId($id_horario=''){
    return $this->db->where('id',$id_horario)->get('aux')->result();
  }
  //obtiene las citas
  public function getCitas($only30=false,$carryover=false){
    if($only30){
      $fecha = date('Y-m-d');
      $nuevafecha = strtotime ( '-30 day' , strtotime ( $fecha ) ) ;
      $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
      $this->db->where('c.fecha_creacion >=',$nuevafecha.' 00:00:00');
    }

    if($carryover){
      $this->db->select('c.*,t.nombre,a.hora,co.opcion,cc.color');
      $this->db->join('tecnicos_citas_historial tch','tch.id_cita=c.id_cita','left');
      //$this->db->where('c.id_cita',18);
    }
    //

    return $this->db->from('citas c')->order_by('fecha_hora','desc')->join('aux a','c.id_horario = a.id','left')->join('tecnicos t','c.id_tecnico=t.id','left')->join('cat_transporte ct','c.transporte=ct.id','left')->join('cat_opcion_citas co','co.id=c.id_opcion_cita','left')->join('cat_colores cc','cc.id=c.id_color','left')->join(CONST_BASE_PRINCIPAL.'admin a','c.id_usuario=a.adminId')->where('c.activo',1)->where('c.historial',0)->get()->result();
  }
  //obtiene los tecnicos
  public function getTecnicos(){
    return $this->db->where('baja_definitiva',0)->get('tecnicos')->result();
  }
  //obtiene los tecnicos con su horario
  public function getTecnicosHorarios($fecha=''){
  return $this->db->select('t.id,t.nombre,ht.hora_inicio,ht.hora_fin,t.activo,ht.hora_inicio_comida,ht.hora_fin_comida,ht.hora_inicio_nolaboral,ht.hora_fin_nolaboral,ht.activo as activo_fecha')->join('horarios_tecnicos ht','ht.id_tecnico = t.id')->where('baja_definitiva',0)->where('fecha',$fecha)->get('tecnicos t')->result();

  }
    //guarda los tecnicos
  public function guardarTecnico(){
     $id= $this->input->post('id');
     if($id==0){
      $password = md5(sha1($this->input->post('password')));
     }else{
      if($this->input->post('password')!=''){
        $password = md5(sha1($this->input->post('password')));
      }else{
        $password = $this->input->post('pass_actual');
      }
     }
     $datos = array('nombre' => $this->input->post('nombre'),
                    'nombreUsuario' => $this->input->post('usuario'),
                    'idStars' => $this->input->post('idStars'),
                    'fecha_creacion' =>date('Y-m-d'),
                    'activo'=>1,
                    'password' => $password
      );
      $q= $this->db->where('nombre',$datos['nombre'])->where('id !=',$id)->get('operadores');
      if($q->num_rows()==1){
        echo -1;exit();
      }
      if($id==0){
          $exito = $this->db->insert('tecnicos',$datos);
      }else{
          $exito = $this->db->where('id',$id)->update('tecnicos',$datos);
      }
        if($exito){
          echo 1;die();
        } else{
          echo 0; die();
        }
  }
    //obtener el tecnico en base a su id
  public function getTecnicoId($id=0){
    return $this->db->where('id',$id)->get('tecnicos')->result();
  }
   //obtener las citas de un tecnivo
  public function getHorariosTecnicos($id='',$fecha=''){
    if($fecha!=''){
      $this->db->where('fecha',$fecha);
    }else{
      $this->db->where('fecha',date('Y-m-d'));
    }
    return $this->db->where('id_tecnico',$id)->order_by('fecha','desc')->get('horarios_tecnicos')->result();
  }
  public function getNameTecnico($id=0){
    $q = $this->db->where('id',$id)->select('nombre')->from('tecnicos')->get();
    if($q->num_rows()==1){
      return $q->row()->nombre;
    }else{
      return '';
    }
  }
   //guarda el horario del tecnico
  public function guardarHorario_tecnico(){

      $id_tecnico= $this->input->post('id_tecnico');
      $id_horario= $this->input->post('id_horario');

     $q = "select * from tecnicos_citas where id_tecnico = ".$id_tecnico." and fecha = '".($this->input->post('fecha'))."' and ADDTIME(TIME('".$this->input->post('hora_inicio_nolaboral').":00'),'00:01:00') BETWEEN TIME(hora_inicio) and TIME(hora_fin) and historial=0";
      $qh = $this->db->query($q)->result();
       //debug_var($qh);die();
      if(count($qh)>0){
        echo -2;exit();
      }
      $datos = array('fecha' => ($this->input->post('fecha')),
                     'hora_inicio' => $this->input->post('hora_inicio'),
                     'hora_fin' => $this->input->post('hora_fin'),
                     'hora_inicio_comida' => $this->input->post('hora_inicio_comida'),
                     'hora_fin_comida' => $this->input->post('hora_fin_comida'),
                     'hora_inicio_nolaboral' => $this->input->post('hora_inicio_nolaboral'),
                     'hora_fin_nolaboral' => $this->input->post('hora_fin_nolaboral'),
                     'fecha_creacion' => date('Y-m-d'),
                     'id_tecnico'=>$id_tecnico

      );

      $q= $this->db->where('fecha',$datos['fecha'])->where('id_tecnico',$id_tecnico)->where('id !=',$id_horario)->get('horarios_tecnicos');

      if($q->num_rows()==1){
        echo -1;exit();
      }
      if($id_horario==0){
          $exito = $this->db->insert('horarios_tecnicos',$datos);
      }else{
          $exito = $this->db->where('id',$id_horario)->update('horarios_tecnicos',$datos);
      }
        if($exito){
          echo 1;die();
        } else{
          echo 0; die();
        }
  }
  public function getCitaAsignadaId($id_tecnico='',$id_cita='',$dia_completo=''){
    if($dia_completo!=''){
      $this->db->where('dia_completo',$dia_completo);
    }
    return $this->db->where('id_cita',$id_cita)->where('id_tecnico',$id_tecnico)->get('tecnicos_citas')->result();
  }
  public function getCitaAsignadaId2($id_tecnico='',$id_cita='',$dia_completo=''){

    if($dia_completo ==0 ){
      $this->db->where('dia_completo',$dia_completo);
    }
    return $this->db->where('id_cita',$id_cita)->where('id_tecnico',$id_tecnico)->get('tecnicos_citas')->result();
  }
  public function asignarHoraTecnico(){
    $id_cita = $this->input->post('id');
    //print_r($_POST);die();
    //Guardar en el historial
    $this->saveHistorialEditadas($id_cita);
    //

    if($_POST['dia_completo']==1){
      $dia_completo = 1;
      $fecha_inicio =($this->input->post('fecha_inicio'));
      $fecha_fin = ($this->input->post('fecha_fin'));
      $id_tecnico= $this->input->post('id_tecnico');
      $tecnico_actual = $this->getTecnicoByCita($id_cita);

      //Validar que la fecha del técnico sea mayor a la del asesor
          if($fecha_inicio<($this->input->post('fecha'))){
            echo -2;exit();
          }
      //Fin validación

       //Validar que sea hora laboral del técnico
        if(!$this->getHoraLaboralTecnico($this->input->post('hora_comienzo'),($this->input->post('fecha_inicio')),$id_tecnico)){
          echo -3;exit();
        }


        // //Validar que sea hora laboral del técnico
        // if(!$this->getHoraLaboralTecnico($this->input->post('hora_inicio_extra'),($this->input->post('fecha_parcial')),$id_tecnico)){
        //   echo -3;exit();
        // }


       if(!$this->validarHorarioOcupado( $fecha_inicio,$fecha_fin,$id_tecnico,$this->input->post('hora_comienzo'),'',$id_cita)){
          echo -1;die();
        }

        $this->db->where('id_cita',$id_cita)->where('dia_completo',1)->delete('tecnicos_citas');

         if($this->input->post('hora_comienzo')==''){
            $hora_comienzo = '07:00';
          }else{
             $hora_comienzo = $this->input->post('hora_comienzo');
          }

        $datos_tecnicos = array('id_cita' => $id_cita,
                                'id_tecnico' =>$id_tecnico,
                                'hora_inicio'=>$hora_comienzo,
                                'hora_fin'=>'20:00:00',
                                'fecha'=>$fecha_inicio,
                                'fecha_fin'=>$fecha_fin,
                                'dia_completo'=>1,
                                'hora_inicio_dia' => $hora_comienzo

         );
         // si llega extra de la fecha
        if($this->input->post('hora_inicio_extra') !='' && $this->input->post('hora_fin_extra') !=''){
            //Validar que la fecha del técnico sea mayor a la del asesor
            if(($this->input->post('fecha_parcial'))<($this->input->post('fecha'))){
              echo -2;exit();
            }
            //Fin validación

            if(!$this->validarDiaExtra($this->input->post('hora_inicio_extra'),$this->input->post('hora_fin_extra'),($this->input->post('fecha_parcial')),$this->input->post('id_tecnico'),$id_cita)){
              echo -1;die();
            }
        }
        $this->db->insert('tecnicos_citas',$datos_tecnicos);
        $this->db->where('id_cita',$id_cita)->set('id_tecnico',$id_tecnico)->update('citas');
        $this->db->where('id_cita',$id_cita)->set('id_tecnico',$id_tecnico)->update('tecnicos_citas');




          // si llega extra de la fecha
          if($this->input->post('hora_inicio_extra') !='' && $this->input->post('hora_fin_extra') !=''){
            $this->db->where('id_cita',$id_cita)->where('dia_completo',0)->delete('tecnicos_citas');
                $datos_tecnicos_hora = array('id_cita' => $id_cita,
                                    'id_tecnico' =>$id_tecnico,
                                    'hora_inicio'=>$this->input->post('hora_inicio_extra'),
                                    'hora_fin'=>$this->input->post('hora_fin_extra'),
                                    'fecha'=>($this->input->post('fecha_parcial')),
                                    'fecha_fin'=>($this->input->post('fecha_parcial')),
                                    'dia_completo'=>0,
                                    'hora_inicio_dia'=>$this->input->post('hora_inicio_extra'),

             );
             $this->db->insert('tecnicos_citas',$datos_tecnicos_hora);
          }
          echo 1;exit();
    }else{
        $dia_completo = 0;
        $fecha_inicio =($this->input->post('fecha'));
        $fecha_fin = ($this->input->post('fecha'));
        $tecnico_actual = $this->getTecnicoByCita($id_cita);
       //Validar que la fecha del técnico sea mayor a la del asesor
        if(($this->input->post('fecha_reasignar'))<($this->input->post('fecha'))){
          echo -2;exit();
        }
        //Fin validación

         //Validar que sea hora laboral del técnico
        if(!$this->getHoraLaboralTecnico($this->input->post('hora_inicio'),($this->input->post('fecha_reasignar')),$this->input->post('id_tecnico'),$this->input->post('hora_fin'))){
          echo -3;exit();
        }

        if($this->input->post('id_tecnico')==$tecnico_actual){ //saber si el tecnico actual y el nuevo a guardar son los mismos
            //VERIFICAR SI NO TIENE YA LA CITA (HORA INICIO)
             //$query = "select * FROM tecnicos_citas where ADDTIME(TIME('".$this->input->post('hora_inicio').":00'),'00:01:00') BETWEEN TIME(hora_inicio) and TIME(hora_fin)  and fecha = '".$fecha_inicio."'and id_tecnico = ".$this->input->post('id_tecnico').' and id_cita !='.$id_cita;
             $query = "select * FROM tecnicos_citas where TIME(hora_inicio) between ADDTIME(TIME('".$this->input->post('hora_inicio').":00'),'00:01:00') and SUBTIME(TIME('".$this->input->post('hora_fin').":00'),'00:01:00') and fecha = '".($this->input->post('fecha_reasignar'))."'and id_tecnico =".$this->input->post('id_tecnico').' and id_cita !='.$id_cita.' and historial = 0';


             $qh = $this->db->query($query)->result();
             
            if(count($qh)>0){
              echo -1;exit();
            }

             //VERIFICAR SI NO TIENE YA LA CITA (HORA FIN)
             $query = "select * FROM tecnicos_citas where SUBTIME(TIME('".$this->input->post('hora_fin').":00'),'00:01:00') BETWEEN TIME(hora_inicio_dia) and TIME(hora_fin)  and fecha = '".($this->input->post('fecha_reasignar'))."'and id_tecnico = ".$this->input->post('id_tecnico').' and id_cita !='.$id_cita.' and historial=0 and historial = 0';
             
             $qh = $this->db->query($query)->result();
             
            if(count($qh)>0){
              echo -1;exit();
            }
            if(!$this->validarDiaExtra($this->input->post('hora_inicio'),$this->input->post('hora_fin'),($_POST['fecha_reasignar']),$this->input->post('id_tecnico'),$id_cita)){
              echo -1;die();
            }

             $this->db->where('id_cita',$id_cita)->delete('tecnicos_citas');
             //Si llega vacío la fecha de reasignar ponga la fecha de la cita
             if($this->input->post('fecha_reasignar')!=''){
              $fecha_tecnico = ($this->input->post('fecha_reasignar'));
             }else{
                 $fecha_tecnico =$fecha_inicio;
             }
              $datos_tecnicos_hora = array('id_cita' => $id_cita,
                                    'id_tecnico' =>$this->input->post('id_tecnico'),
                                    'hora_inicio'=>$this->input->post('hora_inicio'),
                                    'hora_fin'=>$this->input->post('hora_fin'),
                                    'fecha'=>$fecha_tecnico,
                                    'fecha_fin'=>$fecha_tecnico,
                                    'dia_completo'=>0,
                                    'hora_inicio_dia'=>$this->input->post('hora_inicio'),

             );
              $this->db->where('id_cita',$id_cita)->set('id_tecnico',$this->input->post('id_tecnico'))->update('citas');
             $this->db->insert('tecnicos_citas',$datos_tecnicos_hora);
             echo 1;exit();
        }else{
            $fecha_inicio = ($this->input->post('fecha_reasignar'));
            if(!$this->validarDiaExtra($this->input->post('hora_inicio'),$this->input->post('hora_fin'),$fecha_inicio,$this->input->post('id_tecnico'))){
            //if(count($qh)>0){
              echo -1;exit();
            }else{

              $this->db->where('id_cita',$id_cita)->delete('tecnicos_citas');

              $datos_tecnicos_hora = array('id_cita' => $id_cita,
                                    'id_tecnico' =>$this->input->post('id_tecnico'),
                                    'hora_inicio'=>$this->input->post('hora_inicio'),
                                    'hora_fin'=>$this->input->post('hora_fin'),
                                    'fecha'=>$fecha_inicio,
                                    'fecha_fin'=>$fecha_inicio,
                                    'dia_completo'=>0,
                                    'hora_inicio_dia'=>$this->input->post('hora_inicio'),

             );
            //debug_var($datos_tecnicos_hora);die();

             $this->db->insert('tecnicos_citas',$datos_tecnicos_hora);
              //actualizar el técnico en la cita
              $this->db->where('id_cita',$id_cita)->set('id_tecnico',$this->input->post('id_tecnico'))->update('citas');
              echo 1;exit();
            }
        }

         //SI INGRESO EL TECNICO GUARDARLO
         // if($this->input->post('tecnico')!=''){
         //  $horarios_tecnicos = array('fecha' => $fecha_inicio,
         //                             'hora_inicio' =>$this->input->post('hora_inicio'),
         //                             'hora_fin' =>$this->input->post('hora_fin'),
         //                             'id_cita' =>$id_cita,
         //                             'id_tecnico'=> $this->input->post('tecnico'),
         //                             'dia_completo'=>$dia_completo,
         //                             'fecha_fin'=>$fecha_fin
         //      );
         //    $this->db->insert('tecnicos_citas',$horarios_tecnicos);
         //  }
      }

  }
  //function que obtiene el técnico de la cita
  public function getTecnicoByCita($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->select('id_tecnico')->where('activo',1)->get('citas');
    if($q->num_rows()==1){
      return $q->row()->id_tecnico;
    }else{
      return 0;
    }

  }
  public function getTecnicosByFechaCita($fecha=''){
    //->where('ht.activo',1)
    return $this->db->where('fecha',$fecha)->select('ht.id_tecnico,nombre')->where('t.activo',1)->from('tecnicos t')->join('horarios_tecnicos ht','t.id=ht.id_tecnico','left')->get()->result();
  }
  public function getCitasAsignadasTecnico($id_tecnico='',$fecha=''){
    return $this->db->where('id_tecnico',$id_tecnico)->where('fecha',$fecha)->order_by('hora_inicio_dia')->where('historial',0)->get('tecnicos_citas')->result();
  }
  //obtiene las citas de los asesores por día
  public function getCitasByAsesor($fecha=''){
    return $this->db->select('a.hora,a.fecha,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno,vehiculo_placas,vehiculo_modelo,c.asesor')->from('citas c')->join('aux a','c.id_horario = a.id')->order_by('asesor,hora')->where('a.fecha',$fecha)->where('c.activo',1)->get()->result();
  }
  public function existe_cita($id_operador='',$fecha='',$hora=''){
    $q = $this->db->where('id_operador',$id_operador)->where('fecha',$fecha)->where('hora',$hora)->select('id,id_status_cita')->get('aux');
    if($q->num_rows()==1){
      return $q->row()->id.'-'.$q->row()->id_status_cita;
    }else{
      return '0-0';
    }
  }
  public function horario_activo($id_operador='',$fecha='',$hora=''){
    $q = $this->db->where('id_operador',$id_operador)->where('fecha',$fecha)->where('hora',$hora)->select('activo')->get('aux');
    if($q->num_rows()==1){
      return $q->row()->activo;
    }else{
      return 1;
    }
  }
  public function datos_cita($id_horario=''){
    $q = $this->db->where('id_horario',$id_horario)->where('activo',1)->select('datos_nombres,datos_apellido_paterno,datos_apellido_materno,vehiculo_placas')->get('citas');
    if($q->num_rows()==1){
      return $q->row()->datos_nombres.' '.$q->row()->datos_apellido_paterno.' '.$q->row()->datos_apellido_materno.'-'.$q->row()->vehiculo_placas;

    }else{
      return '';
    }

  }
  public function getHoraComidaTecnico($hora='',$fecha='',$id_tecnico=''){
    //$query = "select * FROM horarios_tecnicos where TIME('".$hora.":00') BETWEEN TIME(hora_inicio_comida) and TIME(hora_fin_comida)  and fecha = '".$fecha."'and id_tecnico = ".$id_tecnico;
    $query = "select * FROM horarios_tecnicos where ADDTIME(TIME('".$hora.":00'),'00:01:00') BETWEEN TIME(hora_inicio_comida) and TIME(hora_fin_comida)  and fecha = '".$fecha."'and id_tecnico = ".$id_tecnico;

     $q = $this->db->query($query)->result();
     if(count($q)>0){
      return true;
     }else{
      return false;
     }
  }
  //Revisa si es horario laboral del trabajador
  public function getHoraLaboralTecnico($hora='',$fecha='',$id_tecnico='',$hora_fin=''){
    //esta consulta trae 1 resultado si hora inicio y hora fin están en la hora laboral del técnico
    if($hora_fin!=''){
      $query = "select * FROM horarios_tecnicos where ADDTIME(TIME('".$hora.":00'),'00:01:00') BETWEEN TIME(hora_inicio) and TIME(hora_fin)  and fecha = '".$fecha."' and SUBTIME(TIME('".$hora_fin.":00'),'00:01:00') BETWEEN TIME(hora_inicio) and TIME(hora_fin) and id_tecnico = ".$id_tecnico.' and activo=1';
    }else{
      $query = "select * FROM horarios_tecnicos where ADDTIME(TIME('".$hora.":00'),'00:01:00') BETWEEN TIME(hora_inicio) and TIME(hora_fin)  and fecha = '".$fecha."' and id_tecnico = ".$id_tecnico.' and activo=1';
    }
    $q = $this->db->query($query)->result();
     //Validar que ese tecnico en sus horarios no tenga bloqueda la hora
    $query_horarios = "select * from horarios_tecnicos where id_tecnico = ".$id_tecnico." and fecha = '".$fecha."' and ADDTIME(TIME('".$hora."'),'00:01:00') BETWEEN TIME(hora_inicio_nolaboral) and TIME(hora_fin_nolaboral)";
    $qnl = $this->db->query($query_horarios)->result();
     if(count($q)>0 && count($qnl)==0){
      return true;
     }else{
      return false;
     }
  }
  //obtener el modelo en base a su id
  public function getModeloId($id=0){
    return $this->db->where('id',$id)->get('cat_modelo')->result();
  }
   //guarda los modelos
  public function guardarModelo(){
     $id= $this->input->post('id');
     $datos = array('modelo' => $this->input->post('modelo'),'tiempo_lavado' => $this->input->post('tiempo_lavado')
      );
      $q= $this->db->where('modelo',$datos['modelo'])->where('id !=',$id)->get('cat_modelo');
      if($q->num_rows()==1){
        echo -1;exit();
      }
      if($id==0){
          $exito = $this->db->insert('cat_modelo',$datos);
      }else{
          $exito = $this->db->where('id',$id)->update('cat_modelo',$datos);
      }
        if($exito){
          echo 1;die();
        } else{
          echo 0; die();
        }
  }
  public function getStatusCita($idhorario=''){
    $q = $this->db->where('id',$idhorario)->select('id_status_cita')->get('aux');
    if($q->num_rows()==1){
      return $q->row()->id_status_cita;
    }else{
      return 0;
    }
  }
  //NUEVA VERSION
  public function HoraBetweenCita($hora='',$fecha='',$id_tecnico='',$origen=''){
    $timestamp = strtotime($hora) + 1*60;
    $hora = date('H:i', $timestamp).':00';

    //me traigo las citas del técnico en el día que se eligió
    //$query = "select * FROM tecnicos_citas tc join citas c on tc.id_cita=c.id_cita where ('".$fecha."' BETWEEN tc.fecha and tc.fecha_fin)  and tc.id_tecnico = ".$id_tecnico." and tc.activo=1 and tc.historial=0";

    $query = "select * FROM tecnicos_citas tc join citas c on tc.id_cita=c.id_cita where ('".$fecha."' BETWEEN tc.fecha and tc.fecha_fin)  and tc.id_tecnico = ".$id_tecnico." and tc.activo=1 and tc.historial=0 and c.id_status not in(5)";
    if($origen=='refacciones'){
      $query.=' and c.id_status_color in (4,5,6)';
    }
    $res = $this->db->query($query)->result_array();
    $id_cita = 0;
    $contador = 0;
    //Recorro las citas y checo si la fecha es igual a la fecha de la cita entonces valido con la hora de inicio del día de lo contrario valido con la hora de inicio.
    //Si el contador es > 0 es por que encontró una cita y la retorno
    foreach ($res as $key => $value) {
      if("'".$value['fecha']."'"=="'".$fecha."'"){
        if($hora>$value['hora_inicio_dia'] && $hora<$value['hora_fin']){
          $id_cita = $value['id_cita'];
          $id_operacion = $value['id_operacion'];
          $contador=1;
        }
      }else{
        if($hora>$value['hora_inicio'] && $hora<$value['hora_fin']){
          $id_cita = $value['id_cita'];
          $id_operacion = $value['id_operacion'];
          $contador=1;
        }
      }
    }
    if($contador>0){
      return '1-'.$id_cita.'-'.$id_operacion;
    }else{
      return '0-0-0';
    }
  }
  //NUEVA VERSION
  public function HoraBetweenCitaHistorial($hora='',$fecha='',$id_tecnico='',$origen=''){
    $timestamp = strtotime($hora) + 1*60;
    $hora = date('H:i', $timestamp).':00';

    //me traigo las citas del técnico en el día que se eligió
    $query = "select * FROM tecnicos_citas_historial where ('".$fecha."' BETWEEN fecha and fecha_fin)  and id_tecnico = ".$id_tecnico." and activo=1 and historial=0"; //and carryover=1
     
    $query = "select * FROM tecnicos_citas_historial tc join citas c on tc.id_cita=c.id_cita where ('".$fecha."' BETWEEN tc.fecha and tc.fecha_fin)  and tc.id_tecnico = ".$id_tecnico." and tc.activo=1 and tc.historial=0 and c.id_status!=5"; //and tc.carryover=1
    if($origen=='refacciones'){
      $query.=' and c.id_status_color in (4,5,6)';
    }
    $res = $this->db->query($query)->result_array();
    $id_cita = 0;
    $contador = 0;
    //Recorro las citas y checo si la fecha es igual a la fecha de la cita entonces valido con la hora de inicio del día de lo contrario valido con la hora de inicio.
    //Si el contador es > 0 es por que encontró una cita y la retorno
    foreach ($res as $key => $value) {
      if("'".$value['fecha']."'"=="'".$fecha."'"){
        if($hora>$value['hora_inicio_dia'] && $hora<$value['hora_fin']){
          $id_cita = $value['id_cita'];
          $id_carryover = $value['id'];
          $id_operacion = $value['id_operacion'];
          $carryover = $value['carryover'];
          $contador=1;
        }
      }else{
        if($hora>$value['hora_inicio'] && $hora<$value['hora_fin']){
          $id_cita = $value['id_cita'];
           $id_carryover = $value['id'];
           $id_operacion = $value['id_operacion'];
           $carryover = $value['carryover'];
          $contador=1;
        }
      }
    }

    if($contador>0){
      return '1-'.$id_cita.'-'.$id_carryover.'-'.$id_operacion.'-'.$carryover;
    }else{
      return '0-0-0-0-0';
    }
  }
  //NUEVA VERSION
  public function getColorCita($id_cita='',$id_operacion='',$origen=''){
    //Acomodar la versión con las citas
    if($id_cita<=CONST_NO_CITA_NEW_UPDT||$origen=='refacciones'){
      //Versión vieja
      if($origen=='refacciones'){
        $this->db->where_in('c.id_status_color',array(4,5,6));
      }
      $q = $this->db->where('id_cita',$id_cita)->select('co.color as color,e.hexadecimal,id_status')
              ->join('cat_status_citas_tecnicos co','c.id_status=co.id')
              ->join('estatus e','e.id = c.id_status_color','left')
              ->where('c.activo',1)
              ->get('citas c');    
    }else{
      //Versión nueva
      $q = $this->db->select('co.color as color,e.hexadecimal,ao.id_status')
              ->join('ordenservicio o ','c.id_cita = o.id_cita')
              ->join('articulos_orden ao','o.id = ao.idorden')
              ->join('cat_status_citas_tecnicos co','ao.id_status=co.id')
              ->join('estatus e','e.id = c.id_status_color','left')
              ->where('c.activo',1)
              ->where('c.id_cita',$id_cita)
              ->where('ao.id',$id_operacion)
              ->get('citas c');  
    }
    if($q->num_rows()==1){
      return $q->row()->color.'-'.$q->row()->hexadecimal.'-'.$q->row()->id_status;
    }else{
      return '#BAF9F4'.'-'.'#B5B5B5';
    }
  }
  public function validarLogin(){
    if($this->input->post('usuario')==CONST_USER_MASTER && $this->input->post('password') ==CONST_PASS_MASTER){
      $this->session->set_userdata('tipo_perfil',1); //Acceso a todo
      echo 1;exit();
    }else if($this->input->post('usuario')==CONST_USER_ASESOR && $this->input->post('password') ==CONST_PASS_ASESOR){
      $this->session->set_userdata('tipo_perfil',2);
      echo 1;exit();
    }else if($this->input->post('usuario')=='hsegura' && $this->input->post('password') =='segura30'){
      $this->session->set_userdata('tipo_perfil',3);
      echo 1;exit();
    }
    //print_r($_POST);die();
    //Validar si viene del tablero
    if($this->input->post('origen')!='' && $this->input->post('origen')=='taller'){
        if($this->input->post('usuario')==CONST_USER_JEFET && $this->input->post('password') ==CONST_PASS_JEFET){
        $this->session->set_userdata('tipo_perfil',4); //Acceso a todo
        echo 1;exit();
      }
    }

    //var_dump($this->session->userdata('tipo_perfil'));
    $usuario = $this->input->post('usuario');
    $password = md5(sha1($this->input->post('password')));


    $q = $this->db->where('nombre',$usuario)->where('password',$password)->where('activo',1)->where('baja_definitiva',0)->get('tecnicos');
    if($q->num_rows()==1){
      echo 1;exit();
    }else{
      echo 0;exit();
    }
  }
  public function validarLoginCancelarEliminar(){
    if($this->input->post('usuario')==CONST_USER_MASTER && $this->input->post('password') ==CONST_PASS_MASTER){
      $this->session->set_userdata('tipo_perfil',1); //Acceso a todo
      echo 1;exit();
    }else if($this->input->post('usuario')==CONST_USER_ASESOR && $this->input->post('password') ==CONST_PASS_ASESOR){
      $this->session->set_userdata('tipo_perfil',2);
      echo 2;exit();
    }else if($this->input->post('usuario')=='hsegura' && $this->input->post('password') =='segura30'){
      $this->session->set_userdata('tipo_perfil',3);
      echo 3;exit();
    }else{
      echo -1;die();
    }
  }
  //Validar login cuando se requiere editar una cita
  public function validarLoginEditarCita(){
    $usuario = $this->input->post('usuario');
    $password = $this->input->post('password');

    $this->plani4 = $this->load->database('matriz_citas', TRUE);
    $q = $this->plani4->where('adminUsername',$usuario)->where('adminPassword',$password)->get('admin');

    if($q->num_rows()==1){
      if($q->row()->adminStatus==1){
        $this->session->set_userdata('isloggued',1);
        $exito = 1;
      }else{
        $this->session->set_userdata('isloggued',0);
        $exito = 0; // No es admi
      }
    }else{
      $this->session->set_userdata('isloggued',0);
      $exito = -1; //No existe
    }
    echo $exito;die();
  }
  //obtiene el estatus actual de la tabla de citas
  public function getStatusCitaPrincipal($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->where('activo',1)->select('id_status')->get('citas');
    if($q->num_rows()==1){
      return $q->row()->id_status;
    }else{
      return 0;
    }
  }
  //NUEVA VERSIÓN
  public function cambiar_status_cita_tecnico($id_horario='',$estatus_general=''){
    //Estatus general es para saber si actualiza la cita como tal o solo la operacion
    $id_status_actual = $this->getStatusTecnico($this->input->post('id_cita_save')); //Obtiene el estatus actual del técnico
    if($estatus_general){
      $this->db->where('id_cita',$this->input->post('id_cita_save'))->set('id_status',$this->input->post('id_status'))->update('citas');
    }
    
      $datos = array('id_cita' => $this->input->post('id_cita_save'),
                    'fecha' =>date('Y-m-d H:i:s') ,
                    'comentario' => $this->input->post('comentarios'),
                    'islavado' => $this->input->post('islavado'),
                    'tipo' => $this->input->post('tipo'),


     );
      $this->db->insert('comentarios_cita',$datos);
      if($this->input->post('id_status')==5){
        $this->db->where('id_cita',$this->input->post('id_cita_save'))->set('historial',1)->update('tecnicos_citas');
        $this->db->where('id_cita',$this->input->post('id_cita_save'))->set('historial',1)->update('citas');
        $this->db->where('id',$id_horario)->set('ocupado',0)->update('aux');
      }
      //GUARDAR HISTORIAL DE CAMBIO DE ESTATUS

      $id_tecnico = $this->getTecnicoByCita($datos['id_cita']);
      $datos_historial = array('id_cita' =>$datos['id_cita'],
                               'id_tecnico' => $id_tecnico,
                               'fecha'=> date('Y-m-d H:i:s'),
                               'id_status_anterior'=>$id_status_actual,
                               'id_status_nuevo' =>$this->input->post('id_status'),
       );
      $this->db->insert('historial_cambio_status',$datos_historial);
      return 1;die();
  }
  //Obtiene el estatus del técnico
   public function getStatusTecnico($idcita=''){
    $q = $this->db->where('id_cita',$idcita)->select('id_status')->get('citas');
    if($q->num_rows()==1){
      return $q->row()->id_status;
    }else{
      return 0;
    }
  }
  //obtiene la hora de comida
  public function getHoraComida($id_tecnico='',$fecha=''){
    return $this->db->where('id_tecnico',$id_tecnico)->where('fecha',$fecha)->select('hora_inicio_comida,hora_fin_comida')->get('horarios_tecnicos')->row();
  }
  //saber si en la cita hay días completos
  public function getDiaCompleto($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->where('dia_completo',1)->select('dia_completo')->get('tecnicos_citas');
    if($q->num_rows()==1){
      return 1;
    }else{
      return 0;
    }
  }

  //Obtiene la fecha de tecnicos citas
  public function getFechaTecnicosCita($id=''){
    $q = $this->db->where('id',$id)->select('fecha')->get('tecnicos_citas');
    if($q->num_rows()==1){
      return date_eng2esp_1($q->row()->fecha);
    }else{
      return '';
    }
  }
  //Obtiene comentario cita
  public function getDatoCita($id='',$campo='id_cita'){
    $q = $this->db->where('id_cita',$id)->where('activo',1)->select($campo)->get('citas');
    if($q->num_rows()==1){
      return $q->row()->$campo;
    }else{
      return '';
    }
  }
   //Obtiene comentario cita
  public function getColorCitaAuto($id=''){
    $q = $this->db->where('id_cita',$id)->select("ct.color")->join('cat_colores ct','c.id_color = ct.id')->where('c.activo',1)->get('citas c ');
    if($q->num_rows()==1){
      return $q->row()->color;
    }else{
      return '';
    }
  }
  //Obtiene la hora de trabajo del técnico
  public function getHoraWork($fecha='',$idtecnico=''){
    $q = $this->db->where('fecha',$fecha)->where('id_tecnico',$idtecnico)->select('hora_inicio,hora_fin')->get('horarios_tecnicos');
     if($q->num_rows()==1){
      return substr($q->row()->hora_inicio, 0,5).'-'.substr($q->row()->hora_fin, 0,5);
    }else{
      return '';
    }
  }
  public function validarHorarioOcupado($fecha_inicio='',$fecha_fin='',$id_tecnico='',$hora_inicio='',$fecha_cita='',$id_cita=''){
    $query_array = array();
    $contador = 0;
    $fecha = $fecha_inicio; //2018-02-22
    while($fecha<=$fecha_fin){
      if($fecha==$fecha_inicio){
        if($id_cita !=''){
         $query = "select * FROM tecnicos_citas where TIME(hora_inicio_dia) >= TIME('".$hora_inicio.":00')  and fecha = '".$fecha_inicio."' and historial=0 and id_tecnico =".$id_tecnico.' and id_cita !='.$id_cita;
        }else{
          $query = "select * FROM tecnicos_citas where TIME(hora_inicio_dia) >= TIME('".$hora_inicio.":00')  and fecha = '".$fecha_inicio."' and historial=0 and id_tecnico =".$id_tecnico;
        }

        $query_array[] = $query;
        $qr = $this->db->query($query)->result();
        if(count($qr)>0){
          $contador++;
        }
      }else{
        //Validar que no exista una cita después de la fecha que eligió
        if($id_cita==''){
          $query_completos = "select * FROM tecnicos_citas where ('".$fecha."'  BETWEEN fecha and fecha_fin or '".$fecha_fin."'  BETWEEN fecha and fecha_fin) and historial=0  and id_tecnico = ".$id_tecnico;
        }else{
          $query_completos = "select * FROM tecnicos_citas where ('".$fecha."'  BETWEEN fecha and fecha_fin or '".$fecha_fin."'  BETWEEN fecha and fecha_fin) and historial=0  and id_tecnico = ".$id_tecnico.' and id_cita !='.$id_cita;
        }
        $query_array[] = $query_completos;
        $qr_completos = $this->db->query($query_completos)->result();

        if(count($qr_completos)>0){
          $contador++;
        }
      }
      $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
      $fecha = date ( 'Y-m-j' , $nuevafecha );
    }
    if($contador==0){
      return $this->validarHorarioOcupadoCarryOver($fecha_inicio,$fecha_fin,$id_tecnico,$hora_inicio,$fecha_cita,$id_cita);
    }else{
      return false;
    }
  }
  public function validarHorarioOcupadoCarryOver($fecha_inicio='',$fecha_fin='',$id_tecnico='',$hora_inicio='',$fecha_cita='',$id_cita=''){
    $query_array = array();
    $contador = 0;
    $fecha = $fecha_inicio; //2018-02-22
    while($fecha<=$fecha_fin){
      if($fecha==$fecha_inicio){
        if($id_cita !=''){
         $query = "select * FROM tecnicos_citas_historial where TIME(hora_inicio_dia) >= TIME('".$hora_inicio.":00')  and fecha = '".$fecha_inicio."' and historial=0 and id_tecnico =".$id_tecnico.' and id_cita !='.$id_cita.' and carryover=1';
        }else{
          $query = "select * FROM tecnicos_citas_historial where TIME(hora_inicio_dia) >= TIME('".$hora_inicio.":00')  and fecha = '".$fecha_inicio."' and historial=0 and id_tecnico =".$id_tecnico.' and carryover=1';
        }

        $query_array[] = $query;
        $qr = $this->db->query($query)->result();
        if(count($qr)>0){
          $contador++;
        }
      }else{
        if($id_cita==''){
          $query_completos = "select * FROM tecnicos_citas_historial where ('".$fecha."'  BETWEEN fecha and fecha_fin or '".$fecha_fin."'  BETWEEN fecha and fecha_fin)  and id_tecnico = ".$id_tecnico.' and carryover=1';
        }else{
          $query_completos = "select * FROM tecnicos_citas_historial where ('".$fecha."'  BETWEEN fecha and fecha_fin or '".$fecha_fin."'  BETWEEN fecha and fecha_fin)  and id_tecnico = ".$id_tecnico.' and id_cita !='.$id_cita.' and carryover=1';
        }
        $query_array[] = $query_completos;
        $qr_completos = $this->db->query($query_completos)->result();

        if(count($qr_completos)>0){
          $contador++;
        }
      }
      $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
      $fecha = date ( 'Y-m-j' , $nuevafecha );
    }
    if($contador==0){
      return true;
    }else{
      return false;
    }
  }
  //Obtiene los asesores en base a una fecha
  public function getAsesorByFecha($fecha=''){
   $query = "select o.id,o.nombre from aux a join operadores o on a.id_operador = o.id where o.activo=1 and a.fecha = '".($fecha)."' group by o.id";
   return $this->db->query($query)->result();
  }
  public function validarDiaExtra($hora_inicio='',$hora_fin='',$fecha='',$id_tecnico='',$idcita=''){
    if($idcita!=''){
      $consulta_extra = ' and tc.id_cita !='.$idcita;
    }else{
      $consulta_extra = '';
    }
    $query = "select * FROM tecnicos_citas tc join citas c on c.id_cita = tc.id_cita where (TIME(tc.hora_inicio_dia) between ADDTIME(TIME('".$hora_inicio.":00'),'00:01:00') and SUBTIME(TIME('".$hora_fin.":00'),'00:01:00') or ADDTIME(TIME('".$hora_inicio.":00'),'00:01:00') between tc.hora_inicio_dia and tc.hora_fin) and ('".$fecha."' BETWEEN tc.fecha and tc.fecha_fin) and tc.historial=0 and tc.id_tecnico =".$id_tecnico.$consulta_extra.' and c.id_status!=5'; //and c.id_status not in(15,16,17,18,19)
    $qr = $this->db->query($query)->result();
    if(count($qr)>0){
      return false;
    }

    $query = "select * FROM tecnicos_citas_historial tc join citas c on c.id_cita = tc.id_cita where (TIME(tc.hora_inicio_dia) between ADDTIME(TIME('".$hora_inicio.":00'),'00:01:00') and SUBTIME(TIME('".$hora_fin.":00'),'00:01:00') or ADDTIME(TIME('".$hora_inicio.":00'),'00:01:00') between tc.hora_inicio_dia and tc.hora_fin) and ('".$fecha."' BETWEEN tc.fecha and tc.fecha_fin) and tc.historial=0 and tc.id_tecnico =".$id_tecnico.$consulta_extra.' and c.id_status!=5 and tc.carryover=1';
    $qr = $this->db->query($query)->result();
    if(count($qr)>0){
      return false;
    }
    return true;
  }

  public function buscar_citas($tipo=''){
      //tipo 1 busquedas inteligentes, $tipo 2 filtros
        if($tipo==''){
          $tipo = $this->input->post('tipo');
        }
        $page=$this->input->get('per_page');
          $this->load->library('pagination');
          $paginaNum=50;
          $config = array();
          $config["base_url"] = site_url() . "/citas/buscar_citas?";
          $total_row = $this->getCitasPaginador(false,'','',$tipo)[0]->id;
          $config["total_rows"] = $total_row;
          $config["per_page"] = $paginaNum;
          $config['page_query_string'] = TRUE;
          $config['display_pages'] = TRUE;
          $config['use_page_numbers'] = TRUE;
          $config['num_links'] = $total_row;
          $config['cur_tag_open'] = '&nbsp;<a class="current">';
          $config['cur_tag_close'] = '</a>';
          $config['next_link'] = 'Siguiente';
          $config['prev_link'] = 'Anterior';

          $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
          $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
          $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
          $config['clase'] = 'busquedalink';
          $config['num_links'] = 2;
          $this->pagination->initialize($config);
         if($page!=''){
          $page = (($page-1)*($config["per_page"]));
          }else{
            $page = 0;
          }
          $data["links"] = $this->pagination->create_links();
          $data['tabla'] = $this->getTabla($config["per_page"],$page,$tipo);
          $data['resultados'] = $total_row;
          $data['total'] = $this->db->select("count('id_cita')")->from('citas')->count_all_results();
        return $this->blade
                ->set_data($data)    //Datos que e van a mostrar en la página
                ->render('citas/v_busqueda',$data,TRUE); //Nombre de mi vista

    }
    public function getCitasPaginador($iscount=true,$limit='',$start='',$tipo=1,$ispdf=false){
            if($start!='' && $limit !=''){
                $this->db->limit($limit,$start);
            }else{
              if(!$ispdf){
                $this->db->limit(50,0);
              }
            }
           if($this->input->post('confirmada')){
            $tecnico = implode(',',$this->input->post('confirmada'));
            $this->db->where_in('confirmada',$tecnico,FALSE);
           }
           if($this->input->post('cancelada')){
            $tecnico = implode(',',$this->input->post('cancelada'));
            $this->db->where_in('cancelada',$tecnico,FALSE);
           }

           if($this->input->post('id_tecnico')){
            $tecnico = implode(',',$this->input->post('id_tecnico'));
            $this->db->where_in('id_tecnico',$tecnico,FALSE);
           }
           if($this->input->post('id_servicio')){
            $cadena_servicio = '';
            foreach ($this->input->post('id_servicio') as $key => $value) {
              $cadena_servicio.="'".$value."'".',';
            }
            $cadena_servicio = substr($cadena_servicio,0,-1);
            $this->db->where_in('servicio',$cadena_servicio,FALSE);
           }
           if($this->input->post('id_status')){
            $this->db->where_in('id_status_color',implode(',',$this->input->post('id_status')),FALSE);
           }
           if($this->input->post('id_asesor')){
            $cadena_asesor = '';
            foreach ($this->input->post('id_asesor') as $key => $value) {
              $cadena_asesor.="'".$value."'".',';
            }
            $cadena_asesor = substr($cadena_asesor,0,-1);
            $this->db->where_in('asesor',$cadena_asesor,FALSE);
           }
            if($this->input->post('id_status_tecnico')){
            $this->db->where_in('id_status',implode(',',$this->input->post('id_status_tecnico')),FALSE);
           }
          if($tipo==1){
              if($this->input->post('finicio')!=''){
              $this->db->where('a.fecha >=',($this->input->post('finicio')));
             }
             if($this->input->post('ffin')!=''){
              $this->db->where('a.fecha <=',($this->input->post('ffin')));
             }
          }else{
             if($this->input->post('finicio')!=''){
              $this->db->where('date(fecha_hora)',($this->input->post('finicio')));
             }else{
               //$this->db->where('date(fecha_hora)',date('Y-m-d'));
             }
          }

          if($this->input->post('cita_previa')){
            $tecnico = implode(',',$this->input->post('cita_previa'));
            $this->db->where_in('cita_previa',$tecnico,FALSE);
          }

        if($ispdf){
          $this->db->order_by('DATE_FORMAT(fecha_hora, "%T") ASC');
          $this->db->where('historial',0);
          $this->db->where('cancelada',0);
        }
        if($iscount){
            return $this->db->from('citas c')->order_by('fecha_hora','desc')->join('aux a','c.id_horario = a.id','left')->join('tecnicos t','c.id_tecnico=t.id','left')->join('cat_transporte ct','c.transporte=ct.id','left')->join('cat_opcion_citas co','co.id=c.id_opcion_cita','left')->join('cat_colores cc','cc.id=c.id_color','left')->join('estatus e','e.id = c.id_status_color','left')->select("*,e.nombre as estatus,t.nombre as tecnico,cst.status as status_tecnico,cc.color,c.confirmada")->join('cat_status_citas_tecnicos cst','c.id_status=cst.id')->get()->result();
        }else{
            return $this->db->from('citas c')->order_by('fecha_hora','desc')->join('aux a','c.id_horario = a.id','left')->join('tecnicos t','c.id_tecnico=t.id','left')->join('cat_transporte ct','c.transporte=ct.id','left')->join('cat_opcion_citas co','co.id=c.id_opcion_cita','left')->join('cat_colores cc','cc.id=c.id_color','left')->join('estatus e','e.id = c.id_status_color','left')->select("count(id_cita) as id")->join('cat_status_citas_tecnicos cst','c.id_status=cst.id')->get()->result();
        }

    }
     public function getTabla($limit='',$start='',$tipo=1){
        $tmpl = array (
        'table_open' => '<table id="tbl" class="table table-bordered table-striped table-responsive" cellspacing="0" width="100%">');
        $this->table->set_template($tmpl);
        if($tipo==1){
           $this->table->set_heading('ID','Transporte','Año vehículo','Modelo','Placas','#Serie','Servicio','Asesor','Técnico','Fecha Cita','Nombre','AP','AM','Correo','Teléfono','Estatus cita','Estatus Técnico','¿Confirmada?','Cita previa','Comentarios');
         }else{
           $this->table->set_heading('ID','Asesor','Fecha y Hora','Placas','Nombre y apellido','Año','Modelo','Color','Técnico','Comentarios','Prepiking');
         }
         $res=$this->getCitasPaginador(true,$limit,$start,$tipo);
         if(count($res)>0){
            foreach ($res as $item) {
              $fecha_mostrar = $item->fecha_dia.'/'.$item->fecha_mes.'/'.$item->fecha_anio;

                $row=array();
                if($tipo==1){
                  $row[]=$item->id_cita;
                  $row[]=$item->transporte;
                  $row[]=$item->vehiculo_anio;
                  $row[]='<span title="'.$item->vehiculo_modelo.'">'.trim_text($item->vehiculo_modelo,1).'</span>';
                  $row[]=$item->vehiculo_placas;
                  $row[]=$item->vehiculo_numero_serie;
                  $row[]=$item->servicio;
                  $row[]=$item->asesor;
                  $row[]=$item->tecnico;
                  $row[]=$fecha_mostrar.' '.$item->hora;
                  $row[]=$item->datos_nombres;
                  $row[]=$item->datos_apellido_paterno;
                  $row[]=$item->datos_apellido_materno;
                  $row[]=$item->datos_email;
                  $row[]=$item->datos_telefono;
                  $row[]=$item->estatus;
                  $row[]=$item->status_tecnico;
                  $row[]=($item->confirmada==1)?'Si':'No';
                  $row[]=($item->cita_previa==1)?'Si':'No';
                  $acciones = '';
                  $acciones.='<a href=""  class="pe-7s-info
pe pe-7s-info historial_comentarios" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Comentarios" data-id_cita="'.$item->id_cita.'" ></a><a href="'.site_url('citas/linea_tiempo/'.$item->id_cita).'" target="_blank" data-id="{{$value->id_cita}}" class="js_linea_tiempo" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Línea de tiempo"><i class="pe-7s-clock"></i></a>';
                  $row[]=$acciones;
                }else{
                  $row[]=$item->id_cita;
                   $row[]=$item->asesor;
                  $row[]=$fecha_mostrar.' '.$item->hora;                  $row[]=$item->vehiculo_placas;
                  $row[]=$item->datos_nombres.' '.$item->datos_apellido_paterno.' '.$item->datos_apellido_materno;
                 $row[]=$item->vehiculo_anio;
                 $row[]='<span title="'.$item->vehiculo_modelo.'">'.trim_text($item->vehiculo_modelo,1).'</span>';
                 $row[]=$item->color;
                 $row[]=$item->tecnico;
                 $row[]=$item->comentarios_servicio;
                   $articulos_prepiking = $this->getArticulosPrepiking($item->id_cita);
                   $cadena = '';
                   foreach ($articulos_prepiking as $a => $art) {
                     $cadena = $cadena.$art->descripcion.',';
                   }
                 $row[]= substr($cadena,0,-1);
                }
                $this->table->add_row($row);
            }
        }else{
            $row=array();
            $row[]=array('class'=>'text-center col-sm-1','data'=>'No hay registros para mostrar','colspan'=>20);
            $this->table->add_row($row);
        }
        return $this->table->generate();
    }
  //obtener los datos de logistica en base a su id
  public function getLogisticaId($id=0){
    return $this->db->where('id',$id)->get('logistica')->result();
  }
  //Guardar logística
  public function saveLogistic(){

      $id= $this->input->post('id');
      $datos = array(
                  'eco' => $this->input->post('eco'),
                  'entrega_cliente' => $this->input->post('entrega_cliente'),
                  'id_unidad' => $this->input->post('id_unidad'),
                  'id_color' => $this->input->post('id_color'),
                  'serie' => $this->input->post('serie'),
                  'cliente' => $this->input->post('cliente'),
                  'id_vendedor' => $this->input->post('id_vendedor'),
                  'tipo_operacion' => $this->input->post('tipo_operacion'),
                  'iva_desg' => $this->input->post('iva_desg'),
                  'toma' => $this->input->post('toma'),
                  'hora_promesa' => $this->input->post('hora_promesa'),
                  'ubicacion' => $this->input->post('ubicacion'),
                  'reprogramacion' => ($this->input->post('reprogramacion')!='')?1:0,
                  'seguro' => ($this->input->post('seguro')!='')?1:0,
                  'permiso' => ($this->input->post('permiso')!='')?1:0,
                  'placas' => ($this->input->post('placas')!='')?1:0,
                  'accesorio' => $this->input->post('accesorio'),
                  'servicio' => ($this->input->post('servicio')!='')?1:0,
                  'fechaprog' => ($this->input->post('fechaprog')),
                  'horaprog' => $this->input->post('horaprog'),
                   'fecha_creacion' => date('Y-m-d')

      );
      //print_r($datos);die();
      if($id==0){
          $exito = $this->db->insert('logistica',$datos);
      }else{
          $exito = $this->db->where('id',$id)->update('logistica',$datos);
      }
        if($exito){
          echo 1;die();
        } else{
          echo 0; die();
        }
  }
  public function getLogistic(){
  return $this->db->select('l.*,cm.modelo,cc.color,v.clave as vendedor_clave')->join('cat_modelo cm','l.id_unidad = cm.id')->join('cat_colores cc','l.id_color = cc.id')->join('vendedores v','l.id_vendedor = v.id')->get('logistica l')->result();
  }
  //Obtener lo que tardó el servicio
  public function getHourService($idcita=0,$fecha=''){
    $fecha = explode(' ', $fecha);
    $fecha = $fecha[0];
    $cita = $this->db->where('id_cita',$idcita)->select('id_tecnico')->get('citas');
    if($cita->num_rows()==1){
      $id_tecnico = $cita->row()->id_tecnico;
    }else{
      $id_tecnico = 0;
    }
    //Obtener los horarios del técnico en ese día
     $horario = $this->db->where('id_tecnico',$id_tecnico)->where('fecha',($fecha))->select('hora_inicio,hora_fin,hora_inicio_comida,hora_fin_comida')->get('horarios_tecnicos');

    if($horario->num_rows()==1){
      $hora_inicio = $horario->row()->hora_inicio;
      $hora_fin = $horario->row()->hora_fin;
      $hora_inicio_comida = $horario->row()->hora_inicio_comida;
      $hora_fin_comida = $horario->row()->hora_fin_comida;
    }else{
      $hora_inicio = '';
      $hora_fin = '';
      $hora_inicio_comida = '';
      $hora_fin_comida = '';
    }
    $tiempo_comida = round((strtotime($hora_fin_comida) - strtotime($hora_inicio_comida))/3600, 1);

    $tiempo_trabajo_dia = round((strtotime($hora_fin) - strtotime($hora_inicio))/3600, 1);


    //var_dump( $hora_inicio,$hora_fin,$hora_inicio_comida,$hora_fin_comida,$tiempo_comida,$tiempo_trabajo_dia );

//     $datetime1 = new DateTime('2018-02-09 12:00:00');
// $datetime2 = new DateTime('2018-02-10 20:00:00');
// $interval = $datetime1->diff($datetime2);
// print_r($interval);
    //Obtener los horarios del técnico en ese día
     $this->dias_transcurridos = 0;
     $this->horas_transcurridos = 0;
      $this->minutos_transcurridos = 0;
     $tecnicos_citas = $this->db->where('id_cita',$idcita)->get('tecnicos_citas')->result();
     if(count($tecnicos_citas)==0){
      return 0;
     }else{
      foreach ($tecnicos_citas as $key => $value) {
        if($value->dia_completo){
          $this->tiempoTranscurridoFechas($value->fecha.' '.$value->hora_inicio_dia,$value->fecha_fin.' '.$hora_fin,$tiempo_comida);
        }else{
         $this->tiempoTranscurridoFechas($value->fecha.' '.$value->hora_inicio,$value->fecha_fin.' '.$value->hora_fin,$tiempo_comida);
        }

      }
     }
     return $this->dias_transcurridos.' días, '.$this->horas_transcurridos.' hora(s), '.$this->minutos_transcurridos.' minuto(s)';
  }
    function tiempoTranscurridoFechas($fechaInicio,$fechaFin,$hora_comida){
        $fecha1 = new DateTime($fechaInicio);
        $fecha2 = new DateTime($fechaFin);
        $fecha = $fecha1->diff($fecha2);
        $tiempo = "";


        //dias
        if($fecha->d > 0)
        {
          $this->dias_transcurridos =  $this->dias_transcurridos+ $fecha->d;
        }

        //horas
        if($fecha->h > 0)
        {
           $this->horas_transcurridos =  ($this->horas_transcurridos+ $fecha->h);
        }
         //minutos
        if($fecha->i > 0)
        {
            $this->minutos_transcurridos =  ($this->minutos_transcurridos+ $fecha->i);
        }
    }
    //Obtiene las citas que no se hicieron
    public function getCitasByStatus(){
      return $this->db->select('c.*,a.id as id_auxiliar,a.id_status_cita,cs.status,a.hora,t.nombre')->join('aux a','c.id_horario = a.id')->join('cat_status_citas cs','a.id_status_cita = cs.id')->join('tecnicos t','c.id_tecnico=t.id')->where('a.id_status_cita',4)->get('citas c')->result();
    }
      public function buscar_citas_reagendar($tipo_reagendar_proact=''){
        $page=$this->input->get('per_page');
          $this->load->library('pagination');
          $paginaNum=50;
          $config = array();
          $config["base_url"] = site_url() . "/citas/buscar_citas_reagendar?";
          $total_row = $this->getCitasReagendar(false,'','',$tipo_reagendar_proact)[0]->id;
          $config["total_rows"] = $total_row;
          $config["per_page"] = $paginaNum;
          $config['page_query_string'] = TRUE;
          $config['display_pages'] = TRUE;
          $config['use_page_numbers'] = TRUE;
          $config['num_links'] = $total_row;
          $config['cur_tag_open'] = '&nbsp;<a class="current">';
          $config['cur_tag_close'] = '</a>';
          $config['next_link'] = 'Siguiente';
          $config['prev_link'] = 'Anterior';
          $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
          $config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
          $config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
          $config['clase'] = 'busquedalink';
          $config['num_links'] = 2;
          $this->pagination->initialize($config);
         if($page!=''){
          $page = (($page-1)*($config["per_page"]));
          }else{
            $page = 0;
          }
          $data["links"] = $this->pagination->create_links();
          $data['tabla'] = $this->getTablaReagendar($config["per_page"],$page,$tipo_reagendar_proact);
          $data['resultados'] = $total_row;
          $data['total'] = $total_row;
        return $this->blade
                ->set_data($data)    //Datos que e van a mostrar en la página
                ->render('citas/v_busqueda_reagendar',$data,TRUE); //Nombre de mi vista

    }
    public function getCitasReagendar($iscount=true,$limit='',$start='',$tipo_reagendar_proact='reagendar'){
        if($start!='' && $limit !=''){
            $this->db->limit($limit,$start);
        }else{
          $this->db->limit(50,0);
        }
        if($this->input->post('finicio')!=''){
          $this->db->where('date(fecha_hora) >=',($this->input->post('finicio')));
         }
         if($this->input->post('ffin')!=''){
          $this->db->where('date(fecha_hora) <=',($this->input->post('ffin')));
         }
         if($this->input->post('tipo')){
          $this->db->where_in('reagendada',implode(',',$this->input->post('tipo')),FALSE);
         }

         if($this->input->post('id_status_cita')){
          $this->db->where_in('id_status_cita',implode(',',$this->input->post('id_status_cita')),FALSE);
         }
        if($this->input->post('cancelada')){
          $tecnico = implode(',',$this->input->post('cancelada'));
          $this->db->where_in('cancelada',$tecnico,FALSE);
        }
        if($this->input->post('id_tecnico')){
          $tecnico = implode(',',$this->input->post('id_tecnico'));
          $this->db->where_in('id_tecnico',$tecnico,FALSE);
         }
        if($this->input->post('id_asesor')){
          $cadena_asesor = '';
          foreach ($this->input->post('id_asesor') as $key => $value) {
            $cadena_asesor.="'".$value."'".',';
          }
          $cadena_asesor = substr($cadena_asesor,0,-1);
          $this->db->where_in('asesor',$cadena_asesor,FALSE);
         }

        if($this->input->post('placas')!=''){
          $this->db->like('c.vehiculo_placas',$this->input->post('placas'));
        }

        if($this->input->post('cliente')!=''){
          $this->db->like('c.datos_nombres',$this->input->post('cliente'));
        }

        if($tipo_reagendar_proact!='reagendar'){
          $this->db->order_by('ultimo_comentario_llamada','desc');
        }

        if($this->input->post('origen')){
          $origen = implode(',',$this->input->post('origen'));
          $this->db->where_in('origen',$origen,FALSE);
        }

        if($iscount){
            return $this->db->select('c.*,a.id as id_auxiliar,a.id_status_cita,cs.status,a.hora,t.nombre')->join('aux a','c.id_horario = a.id','left')->join('cat_status_citas cs','a.id_status_cita = cs.id','left')->join('tecnicos t','c.id_tecnico=t.id')->get('citas c')->result();
        }else{
          return $this->db->select('count(id_cita) as id')->join('aux a','c.id_horario = a.id','left')->join('cat_status_citas cs','a.id_status_cita = cs.id','left')->join('tecnicos t','c.id_tecnico=t.id')->get('citas c')->result();
        }

    }
     public function getTablaReagendar($limit='',$start='',$tipo_reagendar_proact='reagendar'){

        $tmpl = array (
        'table_open' => '<table border="0" id="tbl" cellpadding="4" cellspacing="0" class="table table-bordered table-striped table-hover">');
        $this->table->set_template($tmpl);
        if($tipo_reagendar_proact=='reagendar'){
          $this->table->set_heading('ID','Fecha de cita','Nombre del cliente','Teléfono','Comentarios','Técnico','Placas','Modelo','Asesor','Servicio','Origen','Acciones');
        }else{
          $this->table->set_heading('ID','Fecha de cita','Nombre del cliente','Teléfono','Comentarios','Placas','Modelo','Origen','Acciones');
        }
         $res=$this->getCitasReagendar(true,$limit,$start,$tipo_reagendar_proact);
         if(count($res)>0){
            foreach ($res as $item) {
              $fecha = date('Y-m-d');
              $nuevafecha = strtotime ( '+30 day' , strtotime ( $fecha ) ) ;
              $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
              //Saber si es para proactivo y tiene menos de 30 días que se reagendó
              if($tipo_reagendar_proact=='proactivo' && ($item->fecha_reagendada<=$nuevafecha) && $item->fecha_reagendada!=''){
                $color = "green";
                $letter_color = "white";
              }else{
                $color = '';
                $letter_color = "black";
              }
              $fecha_mostrar = $item->fecha_dia.'/'.$item->fecha_mes.'/'.$item->fecha_anio;
                $row=array();
                $row[]=array('data'=>$item->id_cita,'style'=>"background-color:".$color." !important;color:".$letter_color." ");
                $row[]=$fecha_mostrar.' '.$item->hora;
                $row[]=$item->datos_nombres.' '.$item->datos_apellido_paterno.' '.$item->datos_apellido_materno;
                $row[]=$item->datos_telefono;
                $row[]= '<span data-toggle="tooltip" data-placement="top" title="'.$item->comentarios_servicio.'">'.trim_text($item->comentarios_servicio,5).'</span>' ; //
                if($tipo_reagendar_proact=='reagendar'){
                  $row[]=$item->nombre;
                }

                $row[]=$item->vehiculo_placas;
                $row[]='<span title="'.$item->vehiculo_modelo.'">'.trim_text($item->vehiculo_modelo,1).'</span>';
                if($tipo_reagendar_proact=='reagendar'){
                  $row[]=$item->asesor;
                  $row[]=$item->servicio;
                }
                $row[]=($item->origen==0)?'Panel':'Tablero';
                $acciones = '';
                $fecha_comparar = $item->fecha_anio.'-'.$item->fecha_mes.'-'.$item->fecha_dia;
                if($tipo_reagendar_proact=='reagendar' && $item->id_status_cita==4){
                  $acciones.='<a href=""  class="pe pe-7s-note editar_cita" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Reagendar" data-id_cita="'.$item->id_cita.'" ></a>';
                }
                $acciones.= '<a href="" data-id="'.$item->id_cita.'" class="pe pe-7s-comment js_comentarios" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Comentarios"></a>';

                $acciones.='<a href="" data-id="'.$item->id_cita.'" class="pe pe-7s-info pe pe-7s-info js_historial" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Historial comentarios"></a>';

                $row[]=$acciones;
                $this->table->add_row($row);
            }
        }else{
            $row=array();
            $row[]=array('class'=>'text-center col-sm-1','data'=>'No hay registros para mostrar','colspan'=>12);
            $this->table->add_row($row);
        }
        return $this->table->generate();
    }
    //guarda los comentariosa
    public function saveComentarioReagendar(){
       $id= $this->input->post('id');
       $datos = array('comentario' => $this->input->post('comentario'),
                      'id_cita' => $this->input->post('id_cita'),
                      'fecha_creacion' =>date('Y-m-d H:i:s'),
                      'id_usuario'=>$this->session->userdata('id_usuario'),
                      'fecha_notificacion' =>($this->input->post('cronoFecha')).' '.$this->input->post('cronoHora'),
        );
       if($this->input->post('tipo_reagendar_proact_com')=='reagendar'){
          $this->db->insert('historial_citas_reagendadas',$datos);
       }else{
        $this->db->where('id_cita',$datos['id_cita'])->set('ultimo_comentario_llamada',date('Y-m-d H:i:s'))->update('citas');
          $this->db->insert('historial_citas_llamadas',$datos);
       }

        //Insertar notificación
        if($this->input->post('cronoFecha')!='' && $this->input->post('cronoFecha')!='cronoHora'){
              $notific['titulo'] = $this->input->post('cronoTitulo');
              $notific['texto']= $this->input->post('comentario');
              $notific['id_user'] = $this->session->userdata('id_usuario');
              $notific['estado'] = 1;
              $notific['tipo_notificacion'] = 2;
              $notific['fecha_hora'] = ($this->input->post('cronoFecha')).' '.$this->input->post('cronoHora');
              //$notific['url'] = base_url().'index.php/bodyshop/ver_proyecto/'.$this->input->post('idp');
              $notific['estadoWeb'] = 1;
              $this->db->insert(CONST_BASE_PRINCIPAL.'noti_user',$notific);
            }

        echo 1;exit();
    }
    public function getComentariosReagendar($id_cita=''){
      if($this->input->get('tipo_reagendar_proact')=='reagendar'){
        return $this->db->where('id_cita',$id_cita)->get('historial_citas_reagendadas')->result();
      }else{
        return $this->db->where('id_cita',$id_cita)->get('historial_citas_llamadas')->result();
      }

    }
    //Obtiene las citas y sus videos
    public function getCitasEvidencia(){
      return $this->db->select('c.*,e.video,e.comentario,e.fecha_creacion,e.tipo')->join('evidencia e','e.id_cita = c.id_cita')->get('citas c')->result();
    }
    public function reagendar_cita(){
    //print_r($_POST);die();
    $info = $_POST['citas'];
    $id= $this->input->post('id');
    $fecha_separada = explode('-', $info['fecha']);
    $fecha_anio = isset($fecha_separada[0])?$fecha_separada[0]:null;
    $fecha_mes = isset($fecha_separada[1])?$fecha_separada[1]:null;
    $fecha_dia = isset($fecha_separada[2])?$fecha_separada[2]:null;
    $datos = array(
                  'fecha_anio' =>$fecha_anio,
                  'fecha_mes' => $fecha_mes,
                  'fecha_dia' => $fecha_dia,
                  'id_horario' => isset($info['horario'])?$info['horario']:null,
                  'fecha' => $fecha_anio.'-'.$fecha_mes.'-'.$fecha_dia,
                  'fecha_hora' =>$fecha_anio.'-'.$fecha_mes.'-'.$fecha_dia.' '.$this->getHora(isset($info['horario'])?$info['horario']:null),
                  'reagendada' => 1,
                  'asesor' => $info['asesor'],
                  'id_usuario_reagendo' => $this->session->userdata('id_usuario'),
                  'historial' => 0,
                  'cancelada'=>0,
                  'id_status'=>1
     );
      $id_horario_anterior = $this->getIdHorarioActual($id);
      $this->db->where('id',$id_horario_anterior)->set('ocupado',0)->update('aux');
      $exito = $this->db->where('id_cita',$id)->update('citas',$datos);
      $this->db->where('id_cita',$id)->set('historial',0)->update('tecnicos_citas');
      if($exito){
         $this->db->where('id',isset($info['horario'])?$info['horario']:null)->set('ocupado',1)->update('aux');
        echo 1;die();
      } else{
        echo 0; die();
      }
  }
   /* *****************SECCIÓN DE PARÁMETROS ************************************ */

  public function getParametros($campo='',$fecha=''){
    $q = "SELECT tiempo_cita FROM parametros WHERE '".$fecha."' between desde and hasta";
    $qh = $this->db->query($q);
    if($qh->num_rows()==1){
      return $qh->row()->$campo;
    }else{
      return '';
    }
  }
  public function getParametrosTiempoActual(){
    $q = $this->db->select('tiempo_cita')->limit(1)->order_by('hasta','desc')->get('parametros');
    if($q->num_rows()==1){
      return $q->row()->tiempo_cita;
    }else{
      return '';
    }
  }
  //obtener la fecha a partir de cuando se pueden cambiar el parámetro de los técnicos
  public function getLastDateTecnico(){
    $q = $this->db->select('fecha_fin')->limit(1)->order_by('fecha_fin','desc')->get('tecnicos_citas');
    if($q->num_rows()==1){
      $fecha= $q->row()->fecha_fin;
       $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
      return date ( 'Y-m-j' , $nuevafecha );
    }else{
      return date('Y-m-d');
    }

  }
  public function getLastDateAsesor(){
    $q = $this->db->select('date(fecha_hora) as fecha')->limit(1)->order_by('fecha','desc')->get('citas');
    if($q->num_rows()==1){
      $fecha= $q->row()->fecha;
      $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
      return date ( 'Y-m-j' , $nuevafecha );
    }else{
      return date('Y-m-d');
    }

  }
  //Función para obtener a partir de que día se pueden generar horarios para los técnicos
  public function getDateByTecnico($idtecnico=''){
      $q = $this->db->select('fecha')->limit(1)->where('id_tecnico',$idtecnico)->order_by('fecha','desc')->get('horarios_tecnicos');
    if($q->num_rows()==1){
      $fecha= $q->row()->fecha;
      $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
      return date ( 'Y-m-d' , $nuevafecha );
    }else{
     return date('Y-m-d');
    }

  }

  /* *****************FIN SECCIÓN DE PARÁMETROS ************************************ */
 public function getDatosAux($fecha='',$id_operador=''){
  return $this->db->select('a.id, a.hora, a.fecha, a.ocupado, a.activo, a.id_status_cita, a.motivo,c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno,c.vehiculo_placas,csct.status as status_tecnico,c.id_status,c.unidad_entregada,c.id_cita,c.vehiculo_modelo,e.nombre as estatus_servicio')->where('a.fecha',$fecha)->where('a.id_operador',$id_operador)->where('c.cancelada',0)->order_by('a.hora')->join('citas c','c.id_horario = a.id')->join('cat_status_citas_tecnicos csct','csct.id=c.id_status','left')->join('estatus e','c.id_status_color=e.id')->get('aux a')->result();
 }
 //Obtiene solamente los horarios de un operador de un día
 public function getDatosHorarios($fecha='',$id_operador=''){
  return $this->db->select('a.id, a.hora, a.fecha, a.ocupado, a.activo, a.id_status_cita, a.motivo')->where('a.fecha',$fecha)->where('a.id_operador',$id_operador)->order_by('a.hora')->get('aux a')->result();
 }
  public function tabla_asesores($fecha='',$valor='',$tiempo_aumento=30,$tipo=0){// el último argumento es para saber si hace el cociente en base a 0 o a 1
        $tmpl = array (
        'table_open' => '<table class="table table-bordered table-striped table-hover table-hover">');
        $this->table->set_template($tmpl);
        $this->table->set_heading('Horario','#cita','Cliente','Placas','Unidad','Servicio','Valoraciones');


         $contador_asesores = 0;
         $operadores = $this->getOperadores();
         $array_operadores = array();
         foreach ($operadores as $key => $value) {
           $array_operadores[$key] = $value;
           $array_operadores[$key]->datos_cita = $this->getDatosAux(($fecha),$value->id);
           $array_operadores[$key]->datos_horarios = $this->getDatosHorarios(($fecha),$value->id);
         }
         if(count($array_operadores)>0){
            foreach($operadores as $c => $value) {
              if($contador_asesores%2==$tipo){
                  if($value->activo==0){
                    $etiqueta = "(Desactivado)";
                    $clase_operador = 'desactivado';;
                  }else{
                    $etiqueta = "";
                    $clase_operador = 'activo';
                  }
                  $row=array();
                  $row[]=array('class'=>'text-center col-sm-1 '.$clase_operador,'data'=>$value->nombre.$etiqueta,'colspan'=>7);
                  $this->table->add_row($row);
                //EMPIEZA A PINTAR EL TIEMPO
                $time = '07:00';
                $contador=0;
                while($contador<$valor){
                  $row=array();
                  $bandera = false;
                  if(count($value->datos_cita)>0){
                   foreach ($value->datos_cita as $key => $val) {
                    if($val->hora==$time && !$bandera){
                      $activo= $val->activo;
                      $id_horario = $val->id;
                      $id_status_cita = $val->id_status_cita;
                      $id_status = $val->id_status;
                      $cliente = $val->datos_nombres.' '.$val->datos_apellido_paterno.' '.$val->datos_apellido_materno;
                      $placas = $val->vehiculo_placas;
                      $id_cita = $val->id_cita;
                      $status_tecnico = $val->status_tecnico;
                      $unidad_entregada = $val->unidad_entregada;
                      $vehiculo_modelo = $val->vehiculo_modelo;
                      $estatus_servicio = $val->estatus_servicio;
                      $bandera = true;
                    }
                   }
                  }//count
                  if($bandera){
                    //$activo = $this->horario_activo($value->id,$fecha,$time);
                    if($activo==0){
                      $style = "background-color: #7576F6 !important;";
                    }else{
                      $style = "";
                    } //activo horario
                    $row[]=array('width'=>'10%','data'=>$time,'style'=>$style);
                      $acciones = '';
                      $row[]=array('data'=>$id_cita,'style'=>$style);
                      $row[]=array('data'=>$cliente,'style'=>$style);
                      $row[]=array('data'=>$placas,'style'=>$style);
                      //$row[]=array('data'=>$status_tecnico,'style'=>$style);
                      $row[]=array('data'=>$vehiculo_modelo,'style'=>$style);
                      $row[]=array('data'=>$estatus_servicio,'style'=>$style);
                      $clase = ($id_status_cita==2)?"verde":"gris";

                      $acciones.='<a href="" data-id="'.$id_horario.'" class="pe pe-7s-check  js_cambiar_status '.$clase.'  elemento_'.$id_horario.'"  aria-hidden="true" data-toggle="tooltip" data-placement="top" data-idstatus="'.$id_status.'" title="Llegó" data-status="2" data-unidadentregada="'.$unidad_entregada.'" data-idcita="'.$id_cita.'"></a>';
                      $clase = ($id_status_cita==3)?"verde":"gris";
                      $acciones.='<a href="" data-id="'.$id_horario.'" class="pe pe-7s-clock '.$clase.' js_cambiar_status elemento_'.$id_horario.'"  aria-hidden="true" data-toggle="tooltip" data-placement="top" data-idstatus="'.$id_status.'" title="Llegó tarde" data-status="3" data-unidadentregada="'.$unidad_entregada.'" data-idcita="'.$id_cita.'"></a>';
                      $clase = ($id_status_cita==4)?"verde":"gris";
                      $acciones.= '<a href="" data-id="'.$id_horario.'" class="pe pe-7s-less '.$clase.' js_cambiar_status elemento_'.$id_horario.'"  aria-hidden="true" data-toggle="tooltip" data-placement="top" data-idstatus="'.$id_status.'" title="No llegó" data-status="4" data-unidadentregada="'.$unidad_entregada.'" data-idcita="'.$id_cita.'"></a>';

                      $clase = ($id_status_cita==5&&$unidad_entregada==1)?"verde":"gris";

                      //Sólo pueden entregarla cuando regrese de lavado
                      if($id_status==9 || $id_status==11){ //
                        $acciones.= '<a href="" data-id="'.$id_horario.'" class="pe pe-7s-car '.$clase.' js_cambiar_status elemento_'.$id_horario.'"  aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Unidad entregada" data-status="5" data-unidadentregada="'.$unidad_entregada.'" data-idstatus="'.$id_status.'" data-idcita="'.$id_cita.'"></a>';
                      }
                      $row[]=array('data'=>$acciones,'style'=>$style);
                  }else{
                    $bandera_horarios = false;
                    $activo_horario = 1;
                   if(count($value->datos_horarios)>0){
                   foreach ($value->datos_horarios as $key => $val) {
                    if($val->hora==$time && !$bandera_horarios){
                      $activo_horario= $val->activo;
                      $bandera_horarios = true;
                    }
                   }
                  }//count
                  if($activo_horario==0){
                    $clase_horario = 'desactivado';
                  }else{
                    $clase_horario = '';
                  }

                    $row[]=array('width'=>'10%','data'=>$time,'class'=>$clase_horario);
                    $row[]=array('data'=>'','colspan'=>7,'class'=>$clase_horario);
                  }

                  $this->table->add_row($row);

                  $timestamp = strtotime($time) + $tiempo_aumento*60;
                  $time = date('H:i', $timestamp);
                  $contador++;
                } // while
                  //$row[]=$item->asesor;


                } //$contador_asesores%2==$tipo
               $contador_asesores ++;
              }
        }else{
            $row=array();
            $row[]=array('class'=>'text-center col-sm-1','data'=>'No hay registros para mostrar','colspan'=>16);
            $this->table->add_row($row);
        }
        return $this->table->generate();
    }
    public function getIdCitaByIdHorario($idhorario=''){
    $q = $this->db->where('id_horario',$idhorario)->select('id_cita')->from('citas')->get();
    if($q->num_rows()==1){
      return $q->row()->id_cita;
    }else{
      return '';
    }
  } //
  public function guardarMagic(){
     $id= $this->input->post('id');

     $datos = array(
               'fecha_recepcion' => ($this->input->post('fecha_recepcion'))?($this->input->post('fecha_recepcion')):'',
               'fecha_entrega_estima' => ($this->input->post('fecha_entrega_estima'))?($this->input->post('fecha_entrega_estima')):'',
               'fecha_remision' => ($this->input->post('fecha_remision'))?($this->input->post('fecha_remision')):'',
               'fecha_factura' => ($this->input->post('fecha_factura'))?($this->input->post('fecha_factura')):'',

               'no_orden' =>$this->input->post('no_orden'),
               'placas' =>$this->input->post('placas'),
               'tipo_orden' =>$this->input->post('tipo_orden'),
               'codigoCte' =>$this->input->post('codigoCte'),
               'notorre' =>$this->input->post('notorre'),
               'noasesor' =>$this->input->post('noasesor'),
               'hora_recepcion' =>$this->input->post('hora_recepcion'),
               'tipo_servicio' =>$this->input->post('tipo_servicio'),
               'garantia' =>$this->input->post('garantia'),
               'tipogtia' =>$this->input->post('tipogtia'),
               'cargo_interno' =>$this->input->post('cargo_interno'),
               'km_actual' =>$this->input->post('km_actual'),
               'cliente_problema' =>$this->input->post('cliente_problema'),
               'hora_entrega_estima' =>$this->input->post('hora_entrega_estima'),
               'hora_remision' =>$this->input->post('hora_remision'),
               'hora_factura' =>$this->input->post('hora_factura'),
               'nombre' =>$this->input->post('nombre'),
               'ap' =>$this->input->post('ap'),
               'am' =>$this->input->post('am'),
               'razon_social' =>$this->input->post('razon_social'),
               'calle' =>$this->input->post('calle'),
               'numero_exterior' =>$this->input->post('numero_exterior'),
               'numero_interior' =>$this->input->post('numero_interior'),
               'colonia' =>$this->input->post('colonia'),
               'municipio' =>$this->input->post('municipio'),
               'estado' =>$this->input->post('estado'),
               'cp' =>$this->input->post('cp'),
               'tel_principal' =>$this->input->post('tel_principal'),
               'tel_secundario' =>$this->input->post('tel_secundario'),
               'tel_adicional' =>$this->input->post('tel_adicional'),
               'correo' =>$this->input->post('correo'),
               'rfc' =>$this->input->post('rfc'),
               'dirigirse_con' =>$this->input->post('dirigirse_con'),
               'serie ' =>$this->input->post('serie'),
               'no_siniestro' =>$this->input->post('no_siniestro'),
               'no_poliza' =>$this->input->post('no_poliza'),
               'flotilla' =>$this->input->post('flotilla'),
               'servicio_express' =>$this->input->post('servicio_express'),
               'servicio_programado' =>$this->input->post('servicio_programado'),
               'fm' =>$this->input->post('fm'),
               'factura' =>$this->input->post('factura'),
               'status' =>$this->input->post('status'),
               'total_mo' =>$this->input->post('total_mo'),
               'total_ref' =>$this->input->post('total_ref'),
               'total_hoj' =>$this->input->post('total_hoj'),
               'total_pin' =>$this->input->post('total_pin'),
               'total_lub' =>$this->input->post('total_lub'),
               'total_oc' =>$this->input->post('total_oc'),
               'total_om' =>$this->input->post('total_om'),
               'total_descuento ' =>$this->input->post('total_descuento'),
               'subtotal ' =>$this->input->post('subtotal'),
               'iva ' =>$this->input->post('iva'),
               'total' =>$this->input->post('total'),
      );
      if($id==0){
          $exito = $this->db->insert('magic',$datos);
          $id = $this->db->insert_id();
      }else{
          $exito = $this->db->where('id',$id)->update('magic',$datos);
      }
        if($exito){
          echo 1;die();
        } else{
          echo 0; die();
        }
  }
  public function getMagicId($id=''){
    return $this->db->where('id',$id)->get('magic')->result();
  }
  public function getEstadisticas(){
    return $this->db->select('a.id_status_cita,count(a.id_status_cita) as total,cs. status')->join('aux a','c.id_horario = a.id','left')->join('cat_status_citas cs','a.id_status_cita = cs.id')->group_by('a.id_status_cita')->get('citas c')->result();
  }
  //Obtiene las citas confirmadas o no
  public function getCitasConfirmadas($confirmada=1){
    $q = $this->db->where('c.confirmada',$confirmada)->select('count(c.confirmada) as total_confirmadas')->get('citas c');
    if ($q->num_rows()==1) {
      return $q->row()->total_confirmadas;
    }else{
      return 0;
    }
  }
  //Obtiene las citas canceladas.
  public function getCitasCanceladas(){
    $q = $this->db->where('c.cancelada',1)->select('count(c.cancelada) as total_canceladas')->get('citas c');
    if ($q->num_rows()==1) {
      return $q->row()->total_canceladas;
    }else{
      return 0;
    }
  }
  //Obtiene las citas canceladas.
  public function getCitasReagendadas(){
    $q = $this->db->where('c.reagendada',1)->select('count(c.reagendada) as total_reagendadas')->get('citas c');
    if ($q->num_rows()==1) {
      return $q->row()->total_reagendadas;
    }else{
      return 0;
    }
  }
  //GRÁFICAS
   //obtiene graficas
  public function getDatosGraficas(){

    //SELECT STR_TO_DATE('5/16/2011', '%c/%e/%Y')
    if($this->input->post('anio')!=''){
      $anio = $this->input->post('anio');
    }else{
      $anio = date('Y');
    }
    //print_r($_POST);die();
     if($this->input->post('mes')!=''){
      $mes = $this->input->post('mes');
      //echo $mes;die();
      // $fecha_inicio = $anio.'/'.$mes.'/01';
      // $fecha_fin = $anio.'/'.$mes.'/31';
      $fecha_inicio = "01/".$this->getMes($mes)."/".$anio;
      $fecha_fin = "31/".$this->getMes($mes)."/".$anio;
    }else{
      // $fecha_inicio = $anio.'/01/01';
      // $fecha_fin = $anio.'/12/31';
        $fecha_inicio = '01/01/'.$anio;
       $fecha_fin = '31/12/'.$anio;
    }
    //var_dump($fecha_inicio,$fecha_fin);die();
    //return $this->db->where($this->input->post('tipo_busqueda'),$this->input->post('abuscar'))->select('id,fecha_recepcion_docs')->get('bitacora_negocios')->result();
    //print_r($_POST);die();
    if($this->input->post('reagendada')!=null){
      $this->db->where('c.reagendada',1);
    }
    if($this->input->post('cancelada')!=null){
      $this->db->where('c.cancelada',1);
    }
     if($this->input->post('confirmada')!=null){
      $this->db->where('c.confirmada',1);
    }

    if($this->input->post('tipo_busqueda')=='asesores'){
      //print_r($_POST);die();
      if($this->input->post('id_status_asesor')!=''){
          $this->db->where('id_status_cita',$this->input->post('id_status_asesor'));
      }
      if($this->input->post('id_asesor')!=''){
          $this->db->where('id_operador',$this->input->post('id_asesor'));
      }
      return $this->db->where('a.fecha >=',($fecha_inicio))->where('a.fecha <=',($fecha_fin))->select('id_cita as id,a.fecha')->join('aux a','c.id_horario = a.id','left')->join('cat_status_citas cs','a.id_status_cita = cs.id')->get('citas c')->result();
    }else{
      //print_r($_POST);die();
      if($this->input->post('id_status')!=''){
          $this->db->where('id_status',$this->input->post('id_status'));
      }
      if($this->input->post('id_tecnico')!=''){
          $this->db->where('id_tecnico',$this->input->post('id_tecnico'));
      }
      return $this->db->where('c.fecha_hora >=',($fecha_inicio))->where('c.fecha_hora <=',($fecha_fin))->select('id_cita as id,c.fecha_hora as fecha')->get('citas c')->result();
    }




  }
  public function getMes($mes){
            switch ($mes) {
              case 'ene.':
                return '01';
              break;
              case 'feb.':
                return '02';
              break;
              case 'mar.':
                return '03';
              break;
              case 'abr.':
                return '04';
              break;
              case 'may.':
                return '05';
              break;
              case 'jun.':
                return '06';
              break;
              case 'jul.':
                return '07';
              break;
              case 'ago.':
                return '08';
              break;
              case 'sep.':
                return '09';
              break;
              case 'oct.':
                return '10';
              break;
              case 'nov.':
                return '11';
              break;
              case 'dic.':
                return '12';
              break;

              default:
                # code...
                break;
            }
        }
  //FIN DE GRÁFICAS
  //GUARDAR COMENTARIOS DE PH
  //guarda los comentariosa
  public function saveComentarioReagendar_PH(){
       $id= $this->input->post('id');
       $datos = array('comentario' => $this->input->post('comentario'),
                      'id_cita' => $this->input->post('id_cita'),
                      'fecha_creacion' =>date('Y-m-d H:i:s'),
                      'id_usuario'=>$this->session->userdata('id_usuario'),
                      'fecha_notificacion' =>($this->input->post('cronoFecha')).' '.$this->input->post('cronoHora'),
        );
        $this->db->insert('historial_citas_llamadas_ph',$datos);
        //Insertar notificación
        if($this->input->post('cronoFecha')!='' && $this->input->post('cronoFecha')!='cronoHora'){
              $notific['titulo'] = $this->input->post('cronoTitulo');
              $notific['texto']= $this->input->post('comentario');
              $notific['id_user'] = $this->session->userdata('id_usuario');
              $notific['estado'] = 1;
              $notific['tipo_notificacion'] = 2;
              $notific['fecha_hora'] = ($this->input->post('cronoFecha')).' '.$this->input->post('cronoHora');
              //$notific['url'] = base_url().'index.php/bodyshop/ver_proyecto/'.$this->input->post('idp');
              $notific['estadoWeb'] = 1;
              $this->db->insert(CONST_BASE_PRINCIPAL.'noti_user',$notific);
            }

        echo 1;exit();
    }
    public function getComentariosReagendar_magic($id_cita=''){
      return $this->db->where('id_cita',$id_cita)->get('historial_citas_llamadas_magic')->result();

    }
     public function getComentariosProactivoInicial($id_cita=''){
      return $this->db->where('id_cita',$id_cita)->get('historial_proactivo_inicial')->result();

    }
  //FIN COMENTARIOS MAGIC
  //Obtener origen cita
  public function getOrigenCita($id_cita){
    $q = $this->db->where('id_cita',$id_cita)->select('origen')->get('citas');
    if ($q->num_rows()==1) {
      return $q->row()->origen;
    }else{
      return '';
    }
  }

  //INICIO DE TRANSICIONES

   //Función para ver si ya existe la transición
  public function existTransition($id_cita='',$tipo=''){
    if($tipo!=''){
      $this->db->where('tipo',$tipo);
    }
    $q= $this->db->where('id_cita',$id_cita)->get('transiciones_estatus');
    //echo $this->db->last_query();die();
      if($q->num_rows()==1){
        return true;
      }else{
        return false;
      }
  }
  //Obtener la última transacción
  public function getLastTransition($id_cita='',$tipo=''){
    if($tipo!=''){
      $this->db->where('tipo',$tipo);
    }
    $q = $this->db->where('id_cita',$id_cita)->order_by('id','desc')->get('transiciones_estatus');
    if($q->num_rows()==1){
        return $q->row()->fin;
      }else if($q->num_rows()>1){
        //Cuando trae dos registros por que ya hay del técnico
        $q2 = $this->db->where('id_cita',$id_cita)->where('tipo',2)->limit(1)->order_by('id','desc')->get('transiciones_estatus');
        if($q2->num_rows()==1){
          return $q2->row()->fin;
        }else{
          return '';
        }

      }else{
        //Ver si hay un registro
        $q = $this->db->where('id_cita',$id_cita)->order_by('id','asc')->get('transiciones_estatus');
        if($q->num_rows()==1){
          return $q->row()->fin;
        }
        return '';
      }
  }
  //NUEVA VERSION
  public function getTransiciones($id_cita='',$id_operacion=''){
    return $this->db->where('id_cita',$id_cita)->where('id_operacion',$id_operacion)->where('mostrar_linea',1)->get('transiciones_estatus')->result();

  }
  //NUEVA VERSION
  public function getTransicionesbyCita($id_cita=''){
    return $this->db->where('id_cita',$id_cita)->where('mostrar_linea',1)->get('transiciones_estatus')->result();

  }
  
  public function getTransicionesAsesores($id_cita=''){
    return $this->db->select('te.*, cs.status as estatus')->where('id_cita',$id_cita)->join('cat_status_citas cs','on te.id_status_anterior = cs.id')->where('cs.id !=',1)->get('transiciones_estatus te')->result();

  }
  //Estatus Asesores
  public function getStatusAsesor($idstatus=''){
    $q = $this->db->where('id',$idstatus)->select('status')->get('cat_status_citas');
    if ($q->num_rows()==1) {
      return $q->row()->status;
    }else{
      return '';
    }
  }
  //Estatus Técnicos
  public function getStatusTecnicoTransiciones($idstatus=''){
    $q = $this->db->where('id',$idstatus)->select('status')->get('cat_status_citas_tecnicos');
    if ($q->num_rows()==1) {
      return $q->row()->status;
    }else{
      return '';
    }
  }
  //FIN DE TRANSICIONES

  public function getFechaCreacion($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->select('fecha_creacion_all')->get('citas');
    if ($q->num_rows()==1) {
      return $q->row()->fecha_creacion_all;
    }else{
      return '';
    }

  }
  //tiempo de la cita
   public function getTimeCita($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->get('tecnicos_citas');
    if ($q->num_rows()==1) {
      //NUEVA VERSION
      $fecha1 = new DateTime($q->row()->fecha.' '.$q->row()->hora_inicio_dia);//fecha inicial
      $fecha2 = new DateTime($q->row()->fecha_fin.' '.$q->row()->hora_fin);//fecha de cierre
      //var_dump($fecha1,$fecha2);
      $intervalo = $fecha1->diff($fecha2);
      //var_dump($intervalo->format('%d día(s) %H hora(s) %i minuto(s)'));die();
      return $intervalo->format('%d día(s) %H hora(s) %i minuto(s)');
    }else{
      return '';
    }
  }
  public function toHours($min,$type='')
 { //obtener segundos
   $sec = $min * 60;
 //dias es la division de n segs entre 86400 segundos que representa un dia
   $dias=floor($sec/86400);
 //mod_hora es el sobrante, en horas, de la division de días;
   $mod_hora=$sec%86400;
 //hora es la division entre el sobrante de horas y 3600 segundos que representa una hora;
   $horas=floor($mod_hora/3600);
 //mod_minuto es el sobrante, en minutos, de la division de horas;
   $mod_minuto=$mod_hora%3600;
 //minuto es la division entre el sobrante y 60 segundos que representa un minuto;
   $minutos=floor($mod_minuto/60);
   if($horas<=0)
   {
     $text = $minutos.' min';
   }
   elseif($dias<=0)
   {
     if($type=='round')
 //nos apoyamos de la variable type para especificar si se muestra solo las horas
     {
       $text = $horas.' hrs';
     }
     else
     {
       $text = $horas." hrs ".$minutos;
     }
   }
   else
   {
 //nos apoyamos de la variable type para especificar si se muestra solo los dias
     if($type=='round')
     {
       $text = $dias.' dias';
     }
     else
     {
       $text = $dias." dias ".$horas." hrs ".$minutos." min";
     }
   }
   return $text;
 }
 //Obtiene usuario de plani4
 public function getNameUser($iduser=''){
  $q = $this->db->where('adminId',$iduser)->select('adminUsername')->get(CONST_BASE_PRINCIPAL.'admin');
    if ($q->num_rows()==1) {
      return $q->row()->adminUsername;
    }else{
      return '';
    }
 }
 //Obtiene nombre técnico
 public function getTecnico($nombre='',$password=''){
  $q = $this->db->where('nombre',$nombre)->where('password',md5(sha1($password)))->select('nombre')->get('tecnicos');
    if ($q->num_rows()==1) {
      return $q->row()->nombre;
    }else{
      return '';
    }
 }
 //Obtiene nombre técnico
 public function getTecnicoIdByUser($nombre='',$password=''){
  $q = $this->db->where('nombre',$nombre)->where('password',md5(sha1($password)))->select('id')->get('tecnicos');
    if ($q->num_rows()==1) {
      return $q->row()->id;
    }else{
      return '';
    }
 }
 //Obtiene usuario de plani4
 public function getNameAsesor($iduser=''){
  $q = $this->db->where('id',$iduser)->select('nombre')->get('operadores');
    if ($q->num_rows()==1) {
      return $q->row()->nombre;
    }else{
      return '';
    }
 }
 //Obtiene el asesor de la cita
 public function getIdOperador($idhorario=''){
  $q = $this->db->where('id',$idhorario)->select('id_operador')->get('aux');
    if ($q->num_rows()==1) {
      return $q->row()->id_operador;
    }else{
      return '';
    }
 }
  public function asignarHoraTecnicoCarriOver($carryover=1,$carryover_reservacion=0){
    $id_cita = $this->input->post('id');
    if($this->input->post('dia_completo')!=null){
      $dia_completo = 1;
      $fecha_inicio =$this->input->post('fecha_inicio');
      $fecha_fin = $this->input->post('fecha_fin');
      $id_tecnico= $this->input->post('tecnico_dias');
      $tecnico_actual = $this->getTecnicoByCita($id_cita);

      //Validar que la fecha del técnico sea mayor a la del asesor
          if($fecha_inicio<$this->input->post('fecha')){
            echo -2;exit();
          }
      //Fin validación
       //Validar que sea hora laboral del técnico
        if(!$this->getHoraLaboralTecnico($this->input->post('hora_comienzo'),($this->input->post('fecha_inicio')),$id_tecnico)){
          echo -3;exit();
        }

        if(!$this->validarHorarioOcupado( $fecha_inicio,$fecha_fin,$id_tecnico,$this->input->post('hora_comienzo'),'',$id_cita)){
          echo -1;die();
        }

        //Guardar el historial
            $tecnico_cita_actual = $this->db->where('id_cita',$id_cita)->where('dia_completo',1)->get('tecnicos_citas')->result();

            foreach ($tecnico_cita_actual as $key => $value) {
               $datos_tecnicos_historial = array(
                'id' => $value->id,
                'id_cita' => $value->id_cita,
                'id_tecnico' => $value->id_tecnico,
                'hora_inicio' => $value->hora_inicio,
                'hora_fin' => $value->hora_fin,
                'fecha' => $value->fecha,
                'dia_completo' => $value->dia_completo,
                'fecha_fin' => $value->fecha_fin,
                'hora_inicio_dia' => $value->hora_inicio_dia,
                'activo' => $value->activo,
                'historial' => $value->historial,
              );
               $this->db->insert('tecnicos_citas_historial',$datos_tecnicos_historial);
            }

        $this->db->where('id_cita',$id_cita)->where('dia_completo',1)->delete('tecnicos_citas');

        //$this->db->where('id_cita',$id_cita)->set('id_status',1)->set('id_status_color',1)->update('citas');

         if($this->input->post('hora_comienzo')==''){
            $hora_comienzo = '07:00';
          }else{
             $hora_comienzo = $this->input->post('hora_comienzo');
          }

        $datos_tecnicos = array('id_cita' => $id_cita,
                                'id_tecnico' =>$id_tecnico,
                                'hora_inicio'=>'07:00:00',
                                'hora_fin'=>'20:00:00',
                                'fecha'=>$fecha_inicio,
                                'fecha_fin'=>$fecha_fin,
                                'dia_completo'=>1,
                                'hora_inicio_dia' => $hora_comienzo

         );
         // si llega extra de la fecha
        if($this->input->post('hora_inicio_extra') !='' && $this->input->post('hora_fin_extra') !=''){
            //Validar que la fecha del técnico sea mayor a la del asesor
            if($this->input->post('fecha_parcial')<($this->input->post('fecha'))){
              echo -2;exit();
            }
            //Fin validación

            if(!$this->validarDiaExtra($this->input->post('hora_inicio_extra'),$this->input->post('hora_fin_extra'),($this->input->post('fecha_parcial')),$this->input->post('tecnico_dias'),$id_cita)){
              echo -1;die();
            }
        }
        $this->db->insert('tecnicos_citas',$datos_tecnicos);
        $this->db->where('id_cita',$id_cita)->set('id_tecnico',$id_tecnico)->update('citas');
        $this->db->where('id_cita',$id_cita)->set('id_tecnico',$id_tecnico)->update('tecnicos_citas');




          // si llega extra de la fecha
          if($this->input->post('hora_inicio_extra') !='' && $this->input->post('hora_fin_extra') !=''){
            //Guardar el historial
            $tecnico_cita_actual = $this->db->where('id_cita',$id_cita)->where('dia_completo',0)->get('tecnicos_citas')->result();
            $datos_tecnicos_hora = array('id_cita' => $id_cita,
                                    'id_tecnico' =>$id_tecnico,
                                    'hora_inicio'=>$this->input->post('hora_inicio_extra'),
                                    'hora_fin'=>$this->input->post('hora_fin_extra'),
                                    'fecha'=>($this->input->post('fecha_parcial')),
                                    'fecha_fin'=>($this->input->post('fecha_parcial')),
                                    'dia_completo'=>0,
                                    'hora_inicio_dia'=>$this->input->post('hora_inicio_extra'),
                                    'carryover'=>$carryover,
                                    'carryover_reservacion' => $carryover_reservacion

             );
             $this->db->insert('tecnicos_citas_historial',$datos_tecnicos_hora);
          }
          //NUEVA VERSION
          //$this->db->where('id',$this->input->post('operacion'))->set('id_tecnico',$this->input->post('tecnico'))->set('id_status',1)->update('articulos_orden');
          //$this->db->where('id_cita',$id_cita)->set('id_operacion',$this->input->post('operacion'))->update('tecnicos_citas_historial');
          echo 1;exit();
    }else{
        $dia_completo = 0;
        $fecha_inicio =($this->input->post('fecha_inicio'));
        $fecha_fin = ($this->input->post('fecha_inicio'));
        $tecnico_actual = $this->getTecnicoByCita($id_cita);
         //Validar que sea hora laboral del técnico
        if(!$this->getHoraLaboralTecnico($this->input->post('hora_inicio'),($this->input->post('fecha_inicio')),$this->input->post('tecnico'),$this->input->post('hora_fin'))){
          echo -3;exit();
        }
        
        if($this->input->post('tecnico')==$tecnico_actual){ //saber si el tecnico actual y el nuevo a guardar son los mismos
            //VERIFICAR SI NO TIENE YA LA CITA (HORA INICIO)
           
             //$query = "select * FROM tecnicos_citas where ADDTIME(TIME('".$this->input->post('hora_inicio').":00'),'00:01:00') BETWEEN TIME(hora_inicio) and TIME(hora_fin)  and fecha = '".$fecha_inicio."'and id_tecnico = ".$this->input->post('id_tecnico').' and id_cita !='.$id_cita;
            $query = "select * FROM tecnicos_citas tc join citas c on c.id_cita = tc.id_cita where TIME(tc.hora_inicio) between ADDTIME(TIME('".$this->input->post('hora_inicio').":00'),'00:01:00') and SUBTIME(TIME('".$this->input->post('hora_fin').":00'),'00:01:00') and tc.fecha = '".($this->input->post('fecha_inicio'))."'and tc.id_tecnico =".$this->input->post('tecnico').' and tc.id_cita !='.$id_cita. ' and tc.historial=0 and c.id_status not in(5)';

            
            $qh = $this->db->query($query)->result();
             
            if(count($qh)>0){
              echo -1;exit();
            }

            //VERIFICAR SI NO TIENE YA LA CITA (HORA FIN)
            $query = "select * FROM tecnicos_citas tc join citas c on c.id_cita = tc.id_cita where SUBTIME(TIME('".$this->input->post('hora_fin').":00'),'00:01:00') BETWEEN TIME(tc.hora_inicio_dia) and TIME(tc.hora_fin)  and tc.fecha = '".($this->input->post('fecha_inicio'))."'and tc.id_tecnico = ".$this->input->post('tecnico').' and tc.historial=0 and tc.id_cita !='.$id_cita.' and c.id_status not in(5)';
            
            $qh = $this->db->query($query)->result();
            if(count($qh)>0){
              echo -1;exit();
            }
            //Guardar el historial
            $tecnico_cita_actual = $this->db->where('id_cita',$id_cita)->get('tecnicos_citas')->result();
            $fecha_tecnico =$fecha_inicio;
            $datos_tecnicos_hora = array('id_cita' => $id_cita,
                                    'id_tecnico' =>$this->input->post('tecnico'),
                                    'hora_inicio'=>$this->input->post('hora_inicio'),
                                    'hora_fin'=>$this->input->post('hora_fin'),
                                    'fecha'=>$fecha_tecnico,
                                    'fecha_fin'=>$fecha_tecnico,
                                    'dia_completo'=>0,
                                    'hora_inicio_dia'=>$this->input->post('hora_inicio'),
                                    'carryover'=>$carryover,
                                    'carryover_reservacion' => $carryover_reservacion

             );
            $this->db->where('id_cita',$id_cita)->set('id_tecnico',$this->input->post('tecnico'))->update('citas');
            $this->db->insert('tecnicos_citas_historial',$datos_tecnicos_hora);
            
            $id_tecnicos_citas = $this->db->insert_id();
            //NUEVA VERSION


            if($carryover){
              $this->db->where('id',$this->input->post('operacion'))->set('id_tecnico',$this->input->post('tecnico'))->update('articulos_orden');
            }else{
              //Si no es un carryover poner en estatus planeado la operación
              $this->db->where('id',$this->input->post('operacion'))->set('id_tecnico',$this->input->post('tecnico'))->set('id_status',1)->update('articulos_orden');
            }
             

             $this->db->where('id',$id_tecnicos_citas)->set('id_operacion',$this->input->post('operacion'))->update('tecnicos_citas_historial');
             
             echo 1;exit();
        }else{
            $fecha_inicio = ($this->input->post('fecha_inicio'));
            if(!$this->validarDiaExtra($this->input->post('hora_inicio'),$this->input->post('hora_fin'),$fecha_inicio,$this->input->post('tecnico'))){
            //if(count($qh)>0){
              echo -1;exit();
            }else{
               //Guardar el historial
            $tecnico_cita_actual = $this->db->where('id_cita',$id_cita)->get('tecnicos_citas')->result();
              $datos_tecnicos_hora = array('id_cita' => $id_cita,
                                    'id_tecnico' =>$this->input->post('tecnico'),
                                    'hora_inicio'=>$this->input->post('hora_inicio'),
                                    'hora_fin'=>$this->input->post('hora_fin'),
                                    'fecha'=>$fecha_inicio,
                                    'fecha_fin'=>$fecha_inicio,
                                    'dia_completo'=>0,
                                    'hora_inicio_dia'=>$this->input->post('hora_inicio'),
                                    'carryover'=>$carryover,

             );
              $this->db->insert('tecnicos_citas_historial',$datos_tecnicos_hora);
              //NUEVA VERSION
              $id_tecnicos_citas = $this->db->insert_id();
              //actualizar el técnico en la cita
              $this->db->where('id_cita',$id_cita)->set('id_tecnico',$this->input->post('tecnico'))->update('citas');
              //NUEVA VERSION
              if($carryover){
                $this->db->where('id',$this->input->post('operacion'))->set('id_tecnico',$this->input->post('tecnico'))->update('articulos_orden');
              }else{
                //Si no es un carryover poner en estatus planeado la operación
                $this->db->where('id',$this->input->post('operacion'))->set('id_tecnico',$this->input->post('tecnico'))->set('id_status',1)->update('articulos_orden');
              }



              $this->db->where('id',$id_tecnicos_citas)->set('id_operacion',$this->input->post('operacion'))->update('tecnicos_citas_historial');
              
              echo 1;exit();
            }
        }
      }

  }
  public function validarPaquetesIguales(){
    //Validar si hay un registro tipo preventivo
    $registro_preventivo = $this->db->where('temporal',$_POST['temporal'])->where('tipo',5)->get('articulos_orden');
    if($registro_preventivo->num_rows()==1){
      if($_POST['paquete_id']==0){
        echo -7;exit();
      }
      
      //Obtener datos del paquete
      $datos_paquete = $this->db->where('temporal',$_POST['temporal'])->limit(1)->order_by('id','desc')->get('pkt_detalle_paquete_operacion')->row();
      $clave_id = '';
      $modelo_id = $this->principal->getGeneric('modelo',$_POST['citas']['vehiculo_modelo'],'cat_modelo','id') ;
      $anio = $_POST['citas']['vehiculo_anio'];
      $cilindro = $this->principal->getGeneric('id',$_POST['cilindro_id'],'pkt_cat_cilindros','cilindros');
      $transmision = $_POST['transmision_id'];
      $motor = $_POST['motor_id'];
      $combustible = $_POST['combustible_id'];
      $submodelo_id = $_POST['citas']['submodelo_id'];

      
      $datos_paquete_submodelo = ($datos_paquete->submodelo_id == 0) ? NULL : $datos_paquete->submodelo_id;
      
      if(
        $datos_paquete->modelo_id!=$modelo_id ||
        $datos_paquete->anio!=$anio ||
        // $datos_paquete->cilindro!=$cilindro ||
        // $datos_paquete->transmision!=$transmision ||
        $datos_paquete->motor!=$motor 
        // $datos_paquete->combustible!=$combustible 
      ){
        echo -8;exit();
        
      }else{
        if((int)$submodelo_id != 0 && $datos_paquete_submodelo!=$submodelo_id){
          echo -8;exit();
        }
      }
    }
  }
  public function guardarCitaServicio(){
    $info = $_POST['citas'];
    //Validar que coincida los paquetes
    // if($_POST['citas']['id_status_color']==4||$_POST['citas']['id_status_color'] == 5||$_POST['citas']['id_status_color'] == 6){
      if($_POST['fecha_recepcion_validar'] > CONST_FECHA_CITA_PAQUETES){
        $this->validarPaquetesIguales();
      }
    // }
    
    $id= $this->input->post('id');
    $id_cita= $this->input->post('id_cita');
    $origen_save = 3;
    $info['fecha'] = date2sql($this->input->post('fecha_recepcion'));

    $hora_entrega  = $_POST['hora_entrega'];
    if($hora_entrega<'08:00'||$hora_entrega>'19:00'){
      echo -5;exit();
    }
    $hora_promesa = date2sql($this->input->post('fecha_entrega')).' '.$hora_entrega;
    if($hora_promesa<=date('Y-m-d H:i:s')){
      echo -6;exit();
    }

    $id_opcion_cita = 2;
      if($id_cita==0){
        $fecha_separada = explode('-', $info['fecha']);
        $fecha_anio = isset($fecha_separada[0])?$fecha_separada[0]:null;
        $fecha_mes = isset($fecha_separada[1])?$fecha_separada[1]:null;
        $fecha_dia = isset($fecha_separada[2])?$fecha_separada[2]:null;
        $datos = array('transporte' => isset($info['transporte'])?$info['transporte']:null,
           'email' => strtolower($info['email']),
           'vehiculo_anio' => $info['vehiculo_anio'],
           'vehiculo_modelo' => $info['vehiculo_modelo'],
           'submodelo_id' => $info['submodelo_id'],
           'vehiculo_placas' => $info['vehiculo_placas'],
           'vehiculo_numero_serie' => $info['vehiculo_numero_serie'],
           'comentarios_servicio' => $info['comentarios_servicio'],
           'asesor' => $info['asesor'],
           'fecha_anio' =>$fecha_anio,
           'fecha_mes' => $fecha_mes,
           'fecha_dia' => $fecha_dia,
            'datos_email' => strtolower($info['email']),
            'datos_nombres' => $info['datos_nombres'],
            'datos_apellido_paterno' => $info['datos_apellido_paterno'],
            'datos_apellido_materno' => $info['datos_apellido_materno'],
            //'datos_telefono' => $info['datos_telefono'],
            'id_prepiking'=>isset($info['id_prepiking'])?$info['id_prepiking']:'',
            // 'id_modelo_prepiking'=>isset($info['id_modelo_prepiking'])?$info['id_modelo_prepiking']:'',
            'fecha_creacion' => date('Y-m-d'),
            'status' =>0,
            'servicio' => isset($info['servicio'])?$info['servicio']:null,
            'id_horario' => null,
            'id_opcion_cita' => $id_opcion_cita,
            'id_color' => $info['id_color'],
            'fecha' => $fecha_anio.'-'.$fecha_mes.'-'.$fecha_dia,
            'id_status_color' => $info['id_status_color'],
            'fecha_hora' =>$fecha_anio.'-'.$fecha_mes.'-'.$fecha_dia.' '.$this->getHora(isset($info['horario'])?$info['horario']:null),
            'origen' => $origen_save,
            'reagendada' => 0,
            'historial'=>0,
            'cancelada'=>0,
            'id_status' =>1,
            'id_usuario_reagendo'=>($this->input->post('reagendada')==1)?$this->session->userdata('id_usuario'):null,
            'cita_previa'=>0,
          );

          $datos['id_tecnico'] = $this->input->post('tecnico');
          $datos['fecha_creacion_all'] = date('Y-m-d H:i:s');
          $datos['id_usuario'] = $this->session->userdata('id_usuario');
          if(isset($_POST['dia_completo'])){
            $fecha_inserto = $fecha_anio.'-'.$fecha_mes.'-'.$fecha_dia;
              $dia_completo = 1;
              if($this->input->post('fecha_inicio')!='' && $this->input->post('fecha_fin')!=''){
                $fecha_inicio =($this->input->post('fecha_inicio'));
                $fecha_fin = ($this->input->post('fecha_fin'));
                if(!$this->validarHorarioOcupado( $fecha_inicio,$fecha_fin,$this->input->post('tecnico_dias'),$this->input->post('hora_comienzo'))){
                  echo -1;die();
                }
                if($fecha_inicio<$fecha_inserto){
                  echo -2;exit();
                }
                if(!$this->getHoraLaboralTecnico($this->input->post('hora_comienzo'),($this->input->post('fecha_inicio')),$this->input->post('tecnico_dias'))){ //Validar que sea hora laboral del técnico
                  echo -3;exit();
                }
              }
              $exito = $this->db->insert('citas',$datos);
              $id_cita = $this->db->insert_id();
              if($this->input->post('tecnico_dias')!=''){
                  $this->db->where('id_cita',$id_cita)->set('id_tecnico',$this->input->post('tecnico_dias'))->update('citas');
                  if($this->input->post('hora_comienzo')==''){
                    $hora_comienzo = '07:00';
                  }else{
                     $hora_comienzo = $this->input->post('hora_comienzo');
                  }
                  if($this->input->post('fecha_inicio')!='' && $this->input->post('fecha_fin')!=''){
                    $horarios_tecnicos_completos = array('fecha' => $fecha_inicio,
                     'hora_inicio' =>$hora_comienzo,
                     'hora_fin' =>'20:00',
                     'id_cita' =>$id_cita,
                     'id_tecnico'=> $this->input->post('tecnico_dias'),
                     'dia_completo'=>$dia_completo,
                     'fecha_fin'=>$fecha_fin,
                     'hora_inicio_dia' => $hora_comienzo
                    );
                  }
                  if($this->input->post('hora_inicio_extra') !='' && $this->input->post('hora_fin_extra') !=''){
                    if(($this->input->post('fecha_parcial'))<$fecha_inserto){
                      echo -2;exit();
                    }
                    if(!$this->getHoraLaboralTecnico($this->input->post('hora_inicio_extra'),($this->input->post('fecha_parcial')),$this->input->post('tecnico_dias'),$this->input->post('hora_fin_extra'))){
                      echo -3;exit();
                    }
                    $horarios_tecnicos = array('fecha' => ($this->input->post('fecha_parcial')),
                       'hora_inicio' =>$this->input->post('hora_inicio_extra'),
                       'hora_fin' =>$this->input->post('hora_fin_extra'),
                       'id_cita' =>$id_cita,
                       'id_tecnico'=> $this->input->post('tecnico_dias'),
                       'dia_completo'=>0,
                       'fecha_fin'=>($this->input->post('fecha_parcial')),
                       'hora_inicio_dia' => $this->input->post('hora_inicio_extra')
                    );

                    // if(!$this->validarDiaExtra($this->input->post('hora_inicio_extra'),$this->input->post('hora_fin_extra'),($this->input->post('fecha_parcial')),$this->input->post('tecnico_dias'))){
                    //   echo -1;die();
                    // } //AQUI

                    if(!$this->validarDiaExtra($horarios_tecnicos['hora_inicio'],$horarios_tecnicos['hora_fin'],$horarios_tecnicos['fecha'],$this->input->post('tecnico_dias'))){
                      echo -1;die();
                    }
                    $this->db->insert('tecnicos_citas',$horarios_tecnicos);
                  }
                  if($this->input->post('fecha_inicio')!='' && $this->input->post('fecha_fin')!=''){
                    $this->db->insert('tecnicos_citas',$horarios_tecnicos_completos);
                  }
                }
            }else{ //fin dia completo
                $dia_completo = 0;
                $fecha_inicio =$info['fecha'];
                $fecha_fin = $info['fecha'];
                $fecha_inserto = $fecha_anio.'-'.$fecha_mes.'-'.$fecha_dia;
                if($this->input->post('tecnico')!=''){
                  if(!$this->validarDiaExtra($this->input->post('hora_inicio'),$this->input->post('hora_fin'),$info['fecha'],$this->input->post('tecnico'))){//VERIFICAR SI NO TIENE YA LA CITA
                    echo -1;die();
                  }
                  if($fecha_inicio<$fecha_inserto){//Validar que la fecha del técnico sea mayor a la del asesor
                    //var_dump($fecha_inicio,$fecha_inserto);exit();
                    echo -2;exit();
                  }
                  if(!$this->getHoraLaboralTecnico($this->input->post('hora_inicio'),$fecha_inserto,$this->input->post('tecnico'),$this->input->post('hora_fin'))){//Validar que sea hora laboral del técnico
                    echo -3;exit();
                  }
                }
                $exito = $this->db->insert('citas',$datos);
                $id_cita = $this->db->insert_id();
                if($this->input->post('tecnico')!=''){
                  $horarios_tecnicos = array('fecha' => $fecha_inicio,
                     'hora_inicio' =>$this->input->post('hora_inicio'),
                     'hora_fin' =>$this->input->post('hora_fin'),
                     'id_cita' =>$id_cita,
                     'id_tecnico'=> $this->input->post('tecnico'),
                     'dia_completo'=>$dia_completo,
                     'fecha_fin'=>$fecha_fin,
                     'hora_inicio_dia' => $this->input->post('hora_inicio')
                    );
                  $this->db->insert('tecnicos_citas',$horarios_tecnicos);
                }
              } // día no completo
         $cita_nueva = 1;
      }else{
        $cita_nueva = 0;
      }
      $datos_cita = array(
           'vehiculo_anio' => $info['vehiculo_anio'],
           'vehiculo_modelo' => $info['vehiculo_modelo'],
           'submodelo_id' => $info['submodelo_id'],
           'vehiculo_placas' => $info['vehiculo_placas'],
           'vehiculo_numero_serie' => $info['vehiculo_numero_serie'],
           'id_status_color'=>$info['id_status_color'],
           'comentarios_servicio' => $info['comentarios_servicio'],
           'asesor' => $info['asesor'],
            'datos_email' => strtolower($info['email']),
            'email' => strtolower($info['email']),
            'datos_nombres' => $info['datos_nombres'],
            'datos_apellido_paterno' => $info['datos_apellido_paterno'],
            'datos_apellido_materno' => $info['datos_apellido_materno'],
            'id_color' => $info['id_color'],
            //'datos_telefono' => $info['datos_telefono'],
            //'servicio' => isset($info['servicio'])?$info['servicio']:null,
            'id_prepiking'=>isset($info['id_prepiking'])?$info['id_prepiking']:'',
            // 'id_modelo_prepiking'=>isset($info['id_modelo_prepiking'])?$info['id_modelo_prepiking']:'',
          );

        //Nuevo cliente DMS
      $cliente_dms = [
        'nombre' => $info['datos_nombres'],
        'apellido_paterno' => $info['datos_apellido_paterno'],
        'apellido_materno' => $info['datos_apellido_materno'],
        'rfc' => $this->input->post('rfc'),
        'codigo_postal' => $this->input->post('cp'),
        'colonia' => $this->input->post('colonia'),
        'correo_electronico' => $info['email'],
        'direccion' => $this->input->post('calle'),
        'estado' => $this->input->post('estado'),
        'municipio' => $this->input->post('municipio'),
        'nombre_empresa' => $this->input->post('nombre_compania'),
        'numero_ext' => $this->input->post('noexterior'),
        'numero_int' => $this->input->post('nointerior'),
        'regimen_fiscal' => 'F',
        'telefono' => $this->input->post('telefono_movil'),
        'tipo_registro' => 2,
      ];
      //$cliente_id_dms = $this->m_dms->newCustomer($cliente_dms);
      $cliente_id_dms = '';
      $datos_servicio = array(
          'telefono_asesor'=>$this->input->post('telefono_asesor'),
          'extension_asesor'=>$this->input->post('extension_asesor'),
          'numero_interno'=>$this->input->post('numero_interno'),
          'fecha_recepcion'=>date('Y-m-d'),
          'hora_recepcion'=>date('H:i:s'),
          'fecha_recepcion_orden'=>date2sql($this->input->post('fecha_recepcion')),
          'hora_recepcion_orden'=>$this->input->post('hora_recepcion'),
          'fecha_entrega'=>($this->input->post('fecha_entrega')),
          'hora_entrega'=>$this->input->post('hora_entrega'),
          'nombre_compania'=>$this->input->post('nombre_compania'),
          'nombre_contacto_compania'=>$this->input->post('nombre_contacto_compania'),
          'ap_contacto'=>$this->input->post('ap_contacto'),
          'am_contacto'=>$this->input->post('am_contacto'),
          'rfc'=>$this->input->post('rfc'),
          'correo_compania'=>$this->input->post('correo_compania'),
          'calle'=>$this->input->post('calle'),
          'nointerior'=>$this->input->post('nointerior'),
          'noexterior'=>$this->input->post('noexterior'),
          'colonia'=>$this->input->post('colonia'),
          'municipio'=>$this->input->post('municipio'),
          'cp'=>$this->input->post('cp'),
          'estado'=>$this->input->post('estado'),
          'sigla_estado' => $this->principal->getGeneric('estado',$_POST['estado'],'estados','sigla'),
          'telefono_movil'=>$this->input->post('telefono_movil'),
          'otro_telefono'=>$this->input->post('otro_telefono'),
          'vehiculo_identificacion'=>$info['vehiculo_numero_serie'],
          'vehiculo_kilometraje'=>$this->input->post('vehiculo_kilometraje'),
          'id_cita' => $id_cita,
          'temporal' => $this->input->post('temporal'),
          'cita_nueva'=>$cita_nueva,
          'id_tipo_orden' => $this->input->post('id_tipo_orden'),
          'numero_cliente' => $this->input->post('numero_cliente'),
          'id_tipo_pago' => $this->input->post('id_tipo_pago'),
          'id_condicion' => $this->input->post('id_condicion'),
          'id_tipo_operacion' => $this->input->post('id_tipo_operacion'),
          'id_tipo_precio' => $this->input->post('id_tipo_precio'),
          'id_concepto' => $this->input->post('id_concepto'),
          'id_moneda' => $this->input->post('id_moneda'),
          'id_uen' => $this->input->post('id_uen'),
          'campania' =>$this->input->post('campania'),
          'color'=>$this->input->post('color'),
          'agente'=>$this->input->post('agente'),
          'almacen'=>$this->input->post('almacen'),
          'idcategoria'=>$this->input->post('idcategoria'),
          'idsubcategoria'=>$this->input->post('idsubcategoria'),
          'sucursal'=>$this->input->post('sucursal'),
          'vehiculo_marca'=>$this->input->post('vehiculo_marca'),
          'cliente_nuevo'=>$this->input->post('cliente_nuevo'),
          'vehiculo_nuevo'=>$this->input->post('vehiculo_nuevo'),
          'id_status_intelisis'=>2,
          'id_situacion_intelisis'=>1,
          'id_status'=>$this->input->post('id_status'),
          'tipo_cliente'=>$this->input->post('tipo_cliente'),
          'orden_magic'=>$this->input->post('orden_magic'),
          'modificar_promesa'=>0,
          'created_at'=>date('Y-m-d H:i:s'),
          'inicio_vigencia'=>$this->input->post('inicio_vigencia'),
          'acepta_beneficios'=>isset($_POST['acepta_beneficios'])?1:0,
          'cliente_id_dms' => $cliente_id_dms
        );

        if($_POST['fecha_recepcion_validar'] < CONST_FECHA_CITA_PAQUETES){
          $datos_servicio['motor'] = $this->input->post('motor');
          $datos_servicio['transmision'] = $this->input->post('transmision');
          $datos_servicio['cilindros'] = $this->input->post('cilindros');
        }else{
          $datos_servicio['cilindro_id'] = $this->input->post('cilindro_id');
          $datos_servicio['transmision_id'] = $this->input->post('transmision_id');
          $datos_servicio['combustible_id'] = $this->input->post('combustible_id');
          $datos_servicio['motor_id'] = $this->input->post('motor_id');
          $datos_servicio['paquete_id'] = $this->input->post('paquete_id');
        }
        //PAQUETES 
        if($id==0){
          $this->db->insert('ordenservicio',$datos_servicio);
          $idorden=$this->db->insert_id();


          //Se puede elegir si actualizar la placa
          if(isset($_POST['actualizar_placa'])) {
            if($_POST['actualizar_placa']=='on'){
              //Actualizar en el historial y en unidades nuevaspro
              $this->db->where('serie',$info['vehiculo_numero_serie'])->set('placas',$info['vehiculo_placas'])->set('correo',strtolower($info['email']))->update('magic');
            }
            
          }
          //Saber si es cliente nuevo
          if(isset($_POST['cliente_nuevo'])){
            $this->saveNewCustom($id_cita);
          }
          if(isset($_POST['vehiculo_nuevo'])&&$_POST['vehiculo_nuevo']==1){
            $this->saveNewVeh($id_cita);
          }
          //Actualizar las refacciones autorizadas en caso de que vengan
          if(isset($_POST['refacciones'])){
            $this->md->updateRefaccionesAutorizadas($_POST['refacciones']);
          }

        }else{
         $this->comparar_datos_orden($id_cita);
          //Quitar fechas y horas cuando el id es !=0
          unset($datos_servicio['fecha_recepcion']);
          unset($datos_servicio['fecha_recepcion_orden']);
          unset($datos_servicio['hora_recepcion']);
          unset($datos_servicio['hora_recepcion_orden']);
          unset($datos_servicio['inicio_vigencia']);
          unset($datos_servicio['acepta_beneficios']);
          unset($datos_servicio['created_at']);
          $datos_servicio['updated_at'] = date('Y-m-d H:i:s');

          $this->db->where('id',$id)->update('ordenservicio',$datos_servicio);
           //Guardar historial ediciones
          $array_ediciones = [
            'id_usuario' => $this->session->userdata('id_usuario'),
            'created_at' => date('Y-m-d H:i:s'),
            'id_orden' => $id
          ];
          $this->db->insert('historial_ediciones_orden',$array_ediciones);
          $this->db->where('id_cita',$id_cita)->update('citas',$datos_cita);
          $idorden=$id;
        }

        if($id_cita!=0){
          $this->db->where('id_cita',$id_cita)->update('citas',$datos_cita);
        }

        //$this->db->where('serie',$info['vehiculo_numero_serie'])->set('correo',strtolower($info['email']))->update('magic');

       $this->db->where('temporal',$this->input->post('temporal'))->where('idorden',0)->where('cancelado',1)->delete('articulos_orden');
       $this->db->where('temporal',$this->input->post('temporal'))->where('idorden',0)->set('idorden',$idorden)->update('articulos_orden');
       $this->db->where('temporal',$this->input->post('temporal'))->where('idorden',0)->set('idorden',$idorden)->update('pkt_detalle_paquete_operacion');
      $exito = true;
      //NUEVA VERSION
      $this->ml->updateInfo($idorden,$id_cita,$this->input->post('paquete_id'));
      if($exito){
        $this->db->where('id',isset($info['horario'])?$info['horario']:null)->set('ocupado',1)->update('aux');
        if($id_cita==0){
         $this->db->where('id',isset($info['horario'])?$info['horario']:null)->set('id_status_cita',1)->update('aux');
        }
        $this->crearArchivo($id_cita);
        correo_seguimiento_orden(strtolower($info['email']),encrypt($id_cita));
        echo $idorden;die();
      } else{
        echo 0; die();
      }
  }
  public function getAnticipoOrden($tipo='',$idorden=''){
    if($idorden==''){
      $idorden = $this->input->post('idorden');
    }
    $q = $this->db->where('id',$idorden)->select('anticipo,anticipo_multi')->get('ordenservicio');
    if($q->num_rows()==1){
      if($tipo==1){ //Grupos y operaciones
        return (int)$q->row()->anticipo;
      }else{
        return (int)$q->row()->anticipo_multi;
      }

    }else{
      return 0;
    }
  }
  public function getTipoOrden($id=''){
    $q = $this->db->where('id',$id)->select('tipo_orden')->get('cat_tipo_orden');
    if($q->num_rows()==1){
      return $q->row()->tipo_orden;
    }else{
      return '';
    }
  }

  //PROACTIVO INTELISIS
   public function saveComentarioProactIntelisis(){
       $id= $this->input->post('id');
       $datos = array('comentario' => $this->input->post('comentario'),
                      'id_proactivo' => $this->input->post('id_cita'),
                      'fecha_creacion' =>date('Y-m-d H:i:s'),
                      'id_usuario'=>$this->session->userdata('id_usuario'),
                      'fecha_notificacion' =>($this->input->post('cronoFecha')).' '.$this->input->post('cronoHora'),
        );
       //$this->db->where('id_cita',$datos['id_cita'])->set('ultimo_comentario_llamada',date('Y-m-d H:i:s'))->update('citas');
          $this->db->insert('historial_comentarios_pro_intelisis',$datos);

        //Insertar notificación
        if($this->input->post('cronoFecha')!='' && $this->input->post('cronoFecha')!='cronoHora'){
              $notific['titulo'] = $this->input->post('cronoTitulo');
              $notific['texto']= $this->input->post('comentario');
              $notific['id_user'] = $this->session->userdata('id_usuario');
              $notific['estado'] = 1;
              $notific['tipo_notificacion'] = 2;
              $notific['fecha_hora'] = ($this->input->post('cronoFecha')).' '.$this->input->post('cronoHora');
              //$notific['url'] = base_url().'index.php/bodyshop/ver_proyecto/'.$this->input->post('idp');
              $notific['estadoWeb'] = 1;
              $this->db->insert(CONST_BASE_PRINCIPAL.'noti_user',$notific);
            }

        echo 1;exit();
    }
    public function getComentariosProactivo_intelisis($id_proactivo=''){
      return $this->db->where('id_proactivo',$id_proactivo)->get('historial_comentarios_pro_intelisis')->result();

    }
    public function getDatosCita($id_cita=''){
      $q = $this->db->where('id_cita',$id_cita)->get('citas');
      if($q->num_rows()==1){
        return $q->row();
      }else{
        return '';
      }
    }
    public function getDatosOrdenServicioByIdCita($id_cita=''){
      $q = $this->db->where('id_cita',$id_cita)->get('ordenservicio');
      if($q->num_rows()==1){
        return $q->row();
      }else{
        return '';
      }
    }
  public function validarLoginGeneral(){
    if($this->input->post('usuario')==CONST_USER_MASTER_ORDEN && $this->input->post('password') ==CONST_PASS_MASTER_ORDEN){
      echo 1;
    }else{
      if($this->input->post('usuario')==CONST_USER_ASESOR_ORDEN && $this->input->post('password') ==CONST_PASS_ASESOR_ORDEN){
      echo 2;
      }else{
        echo 0;
      }
    }
    exit();

  }

  public function Unidad_entregada($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->select('unidad_entregada')->get('citas');
    if($q->num_rows()==1){
      return $q->row()->unidad_entregada;
    }else{
      return '';
    }
  }

  //Obtiene el estatus anterior de las transiciones por cita
  public function getLastStatusTransiciones($id_cita=''){
     $q = $this->db->where('id_cita',$id_cita)->select('status_nuevo')->order_by('id','desc')->limit(1)->get('transiciones_estatus');
      if($q->num_rows()==1){
        return $q->row()->status_nuevo;
      }else{
        return '';
      }
  }
  //Obtiene datos de la cita de la tabla tecnicos_ cita
  public function getDataTecnicosCitas($id_cita=''){
    return $this->db->where('id_cita',$id_cita)->get('tecnicos_citas')->result();
  }
  //Obtiene si la cotización fue aceptada o no
  public function EstatusCotizacionUser($idorden=''){
     $q = $this->db->where('id',$idorden)->select('cotizacion_confirmada')->get('ordenservicio');
      if($q->num_rows()==1){
        return $q->row()->cotizacion_confirmada;
      }else{
        return '';
      }
  }

  public function diferencia_horas_laborales($fecha_inicio='',$fecha_fin=''){
    if($fecha_inicio=='0000-00-00 00:00:00' || $fecha_fin == '0000-00-00 00:00:00'){
      return 0;
    }
    //NUEVA VERSIÓN
    // ESTABLISH THE MINUTES PER DAY FROM START AND END TIMES
    $start_time = '08:30:00';
	  $end_time = '19:00:00';

    $start_ts = strtotime($start_time);
    $end_ts = strtotime($end_time);
    $minutes_per_day = (int)( ($end_ts - $start_ts) / 60 )+1;

    // ESTABLISH THE HOLIDAYS
    $holidays = array
    (
    //'Feb 04', // MLK Day
    );

    // CONVERT HOLIDAYS TO ISO DATES
    foreach ($holidays as $x => $holiday)
    {
    $holidays[$x] = date('Y-m-d', strtotime($holiday));
    }

    $fecha_sol=$fecha_inicio;
    $fecha_menor=$fecha_fin;

    //var_dump($fecha_sol,$fecha_menor);die();
    // CHECK FOR VALID DATES
    $start = strtotime($fecha_sol);
    $end = strtotime($fecha_menor);
    $start_p = date('Y-m-d H:i:s', $start);
    $end_p = date('Y-m-d H:i:s', $end);

    // MAKE AN ARRAY OF DATES
    $workdays = array();
    $workminutes = array();
    // ITERATE OVER THE DAYS
    $start = $start - 60;
    while ($start < $end)
    {
    $start = $start + 60;
    // ELIMINATE WEEKENDS - SAT AND SUN
    $weekday = date('D', $start);
    //echo $weekday;die();
    //if (substr($weekday,0,1) == 'S') continue;
    // ELIMINATE HOURS BEFORE BUSINESS HOURS
    $daytime = date('H:i:s', $start);
    if(($daytime < date('H:i:s',$start_ts))) continue;
    // ELIMINATE HOURS PAST BUSINESS HOURS
    $daytime = date('H:i:s', $start);
    if(($daytime > date('H:i:s',$end_ts))) continue;
    // ELIMINATE HOLIDAYS
    $iso_date = date('Y-m-d', $start);
    if (in_array($iso_date, $holidays)) continue;
    $workminutes[] = $iso_date;
    // END ITERATOR
    }

    //
    $number_of_workminutes = (count($workminutes));
    $number_of_minutes = number_format($minutes_per_day);
    $horas_habiles = number_format($number_of_workminutes/60 ,2);

    if($number_of_workminutes>0){
      $number_of_minutes = $number_of_workminutes-1;
    }
    
     return $number_of_workminutes;

  }
  //FIN PROACTIVO ITELISIS
  public function getIdCitaByOrden($idorden=''){
    $q = $this->db->where('id',$idorden)->select('id_cita')->from('ordenservicio')->get();
    if($q->num_rows()==1){
      return $q->row()->id_cita;
    }else{
      return '';
    }
  }
  public function getIdOrdenByCita($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->limit(1)->select('id')->from('ordenservicio')->get();
    if($q->num_rows()==1){
      return $q->row()->id;
    }else{
      return '';
    }
  }
  //Saber si ya se generó la multipunto
  public function multipunto_generada($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->select('multipunto')->from('documentos_generados')->get();
    if($q->num_rows()==1){
      return $q->row()->multipunto;
    }else{
      return '';
    }
  }
  //Correo del consumidor
  public function correo_consumidor($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->select('email')->from('citas')->get();
    if($q->num_rows()==1){
      return $q->row()->email;
    }else{
      return '';
    }
  }
  //Correo de la compañia
  public function correo_compania($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->select('correo_compania')->from('ordenservicio')->get();
    if($q->num_rows()==1){
      return $q->row()->correo_compania;
    }else{
      return '';
    }
  }
  //Correo de la compañia
  public function telefono_contacto($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->select('telefono_movil')->from('ordenservicio')->get();
    if($q->num_rows()==1){
      return $q->row()->telefono_movil;
    }else{
      return '';
    }
  }
  //Buscar la serie en base a las placas en magic o intelisis
  public function getSerieLarga($placas=''){
    $q1 = $this->db->where('vehiculo_placas',$placas)->limit(1)->order_by('id_cita','desc')->select('vehiculo_numero_serie')->limit(1)->from('citas')->get();
    if($q1->num_rows()==1){
      $serie= $q1->row()->vehiculo_numero_serie;
      if($serie==''){
        $q2 = $this->db->where('placas',$placas)->select('serie')->limit(1)->from('magic')->get();
        if($q2->num_rows()==1){
          return $q2->row()->serie;
        }else{
          return '';
        }
      }else{
        return $serie;exit();
      }
    }else{
      $q2 = $this->db->where('placas',$placas)->select('serie')->limit(1)->from('magic')->get();
      if($q2->num_rows()==1){
        return $q2->row()->serie;
      }else{
        return '';
      }
    }
  }

  public function hora_promesa($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->select('fecha_entrega,hora_entrega')->from('ordenservicio')->get();
    if($q->num_rows()==1){
      return date_eng2esp_1($q->row()->fecha_entrega).' '.$q->row()->hora_entrega;
    }else{
      return '';
    }
  }
  public function only_hora_promesa($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->select('fecha_entrega,hora_entrega')->from('ordenservicio')->get();
    if($q->num_rows()==1){
      return $q->row()->hora_entrega;
    }else{
      return '';
    }
  }
  //Teléfono del asesor
  public function telefono_asesor($idorden=''){
    $q = $this->db->where('id',$idorden)->select('telefono_asesor')->from('ordenservicio')->get();
    if($q->num_rows()==1){
      return $q->row()->telefono_asesor;
    }else{
      return '';
    }
  }
  //Teléfono del cliente orden
  public function telefono_cliente_orden($idorden=''){
    $q = $this->db->where('id',$idorden)->select('telefono_movil')->from('ordenservicio')->get();
    if($q->num_rows()==1){
      return $q->row()->telefono_movil;
    }else{
      return '';
    }
  }
  //Nombre operador
  public function name_operador($id=''){
    $q = $this->db->where('id',$id)->select('nombre')->from('operadores')->get();
    if($q->num_rows()==1){
      return $q->row()->nombre;
    }else{
      return '';
    }
  }
  //Nombre operador
  public function telefono_asesor_by_nombre($nombre=''){
    $q = $this->db->where('nombre',$nombre)->from('operadores')->get();
    if($q->num_rows()==1){
      echo json_encode(array('datos'=>$q->row()));
    }else{
      return '';
    }
  }
  //correo operador
  public function correo_asesor_by_nombre($nombre=''){
    $q = $this->db->where('nombre',$nombre)->select('correo')->from('operadores')->get();
    if($q->num_rows()==1){
      return $q->row()->correo;
    }else{
      return '';
    }
  }
  //Nombre operador
  public function asesor_by_cita($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->select('asesor')->from('citas')->get();
    if($q->num_rows()==1){
      return $q->row()->asesor;
    }else{
      return '';
    }
  }
  //Nombre operador
  public function getPrepikin($id=''){
    $q = $this->db->where('id',$id)->select('servicio')->from('prepiking')->get();
    if($q->num_rows()==1){
      return $q->row()->servicio;
    }else{
      return '';
    }
  }
  public function getArticulosPrepiking($id_cita=''){
    return $this->db->where('id_cita',$id_cita)->get('prepiking_refacciones')->result();
  }
  //Obtener datos del paquete
  public function getArticulosPaquetes($id_modelo='',$servicio_id = ''){
    return $this->db->where('p.clave_id',$servicio_id)
                    ->where('r.modelo_id',$id_modelo)
                    ->join('pkt_reglas_paquetes r','r.paquete_id = p.id')
                    ->join('pkt_detalle_paquete pd','pd.paquete_id = p.id')
                    ->join('pkt_articulos pa','pa.id = pd.articulo_id')
                    ->select('pd.id as detalle_id,pd.paquete_id,pd.parte as codigo, pa.descripcion')
                    ->get('pkt_paquetes p')
                    ->result();

  }
  //Obtener subcategorias
  public function getSubcategorias($idcategoria){
    return $this->db->where('idcategoria',$idcategoria)->get('subcategorias')->result();
  }
  //Obtener prepiking
  public function getPrepiking($id_modelo_prepiking,$anio=''){
    return $this->db->select('distinct(codigo_ereact) as servicio')->where('idmodelo',$id_modelo_prepiking)->where('anio',$anio)->where('codigo !=','')->get('preventivo')->result();
   }
  //PROACTIVO UNIDADES NUEVAS
  //PROACTIVO INTELISIS
   public function saveComentarioUnidadesNuevas(){
       $id= $this->input->post('id');
       $datos = array('comentario' => $this->input->post('comentario'),
                      'id_proactivo' => $this->input->post('id_cita'),
                      'fecha_creacion' =>date('Y-m-d H:i:s'),
                      'id_usuario'=>$this->session->userdata('id_usuario'),
                      'fecha_notificacion' =>($this->input->post('cronoFecha')).' '.$this->input->post('cronoHora'),
        );
       //$this->db->where('id_cita',$datos['id_cita'])->set('ultimo_comentario_llamada',date('Y-m-d H:i:s'))->update('citas');
          $this->db->insert('historial_comentarios_unidades_nuevas',$datos);

        //Insertar notificación
        if($this->input->post('cronoFecha')!='' && $this->input->post('cronoFecha')!='cronoHora'){
              $notific['titulo'] = $this->input->post('cronoTitulo');
              $notific['texto']= $this->input->post('comentario');
              $notific['id_user'] = $this->session->userdata('id_usuario');
              $notific['estado'] = 1;
              $notific['tipo_notificacion'] = 2;
              $notific['fecha_hora'] = ($this->input->post('cronoFecha')).' '.$this->input->post('cronoHora');
              //$notific['url'] = base_url().'index.php/bodyshop/ver_proyecto/'.$this->input->post('idp');
              $notific['estadoWeb'] = 1;
              $this->db->insert(CONST_BASE_PRINCIPAL.'noti_user',$notific);
            }

        echo 1;exit();
    }
    public function getComentariosUnidadesNuevas($id_proactivo=''){
      return $this->db->where('id_proactivo',$id_proactivo)->get('historial_comentarios_unidades_nuevas')->result();

    }
    public function cotizacion_multipunto_status($idorden=''){
      $q = $this->db->select('aceptoTermino')->where('idOrden',$idorden)->get('cotizacion_multipunto');
      if($q->num_rows()==1){
        return $q->row()->aceptoTermino;
      }else{
        return '';
      }
    }
    public function firma_jefe_taller($idorden=''){
      $q = $this->db->select('firmaJefeTaller')->join('multipunto_general MG','MG.id = MG3.idOrden ','left')->where('MG.orden',$idorden)->get('multipunto_general_3 MG3');
      if($q->num_rows()==''){
        return 0;
      }else{
        return true;
      }
    }
    public function firma_cliente($idorden=''){
      $q = $this->db->select('firmaCliente')->join('multipunto_general MG','MG.id = MG3.idOrden ','left')->where('MG.orden',$idorden)->get('multipunto_general_3 MG3');
      if($q->num_rows()==0){
        return false;
      }else if($q->row()->firmaCliente==''){
        return false;
      }else{
        return true;
      }
    }
    //FIN PROACTIVO UNIDADES NUEVAS
    public function datosClienteIntelisis($cliente=''){
      return $this->db->where('cliente',$cliente)->get('clientes_intelisis')->result();
    }
    public function datosClienteIntelisisById($id=''){
      return $this->db->where('id',$id)->get('clientes_intelisis')->row();
    }
    public function datosClienteUnidadesNuevasById($id=''){
      return $this->db->where('id',$id)->get('proactivo_unidades')->row();
    }
    public function datosMagicId($id=''){
      return $this->db->where('id',$id)->get('magic')->row();
    }
    public function datosMagicSerie($serie=''){
      return $this->db->where('serie',$serie)->limit(1)->order_by('id','desc')->get('magic')->result();
    }
    public function datosMagicPlacas($placas=''){
      return $this->db->where('placas',$placas)->limit(1)->order_by('id','desc')->get('magic')->result();
    }
    public function datosUnuevas($serie=''){
      return $this->db->where('serie',$serie)->limit(1)->order_by('id','desc')->get('proactivo_unidades')->result();
    }
    public function qr($url,$serie){

    //cargamos la librería
    $this->load->library('ciqrcode');
    //hacemos configuraciones
    $params['data'] = $serie;
    $params['level'] = 'H';
    $params['size'] = 5;
    //decimos el directorio a guardar el codigo qr, en este
    //caso una carpeta en la raíz llamada qr_code
    $anio=date('Y');
    $mes=date('m');
    $carpeta = 'assets/images/qr/'.$serie;

      if (!file_exists($carpeta)) {
          mkdir($carpeta, 0777, true);
      }

    $nombre_imagen=$carpeta.'/'.$serie.'.png';
    $params['savename'] = FCPATH.$nombre_imagen;
    //generamos el código qr
    $this->ciqrcode->generate($params);
    return $nombre_imagen;
  }
  
  //SABER SI YA SE GENERO LA MULTIPUNTO
   public function GeneroMultipunto($idorden=''){
    $q = $this->db->where('idOrden',$idorden)->select('multipuntoPDF')->from('estados_pdf')->get();
    if($q->num_rows()==1){
      return $q->row()->multipuntoPDF;
    }else{
      return 0;
    }
  }
  public function hm_firmaTec($id_cita='')
  {
      $query = $this->db->where('mg.orden',$id_cita)
              ->join('multipunto_general_3 AS mg3','mg3.idOrden = mg.id')
              ->select('mg3.firmaAsesor ,mg3.firmaTecnico ,mg3.firmaJefeTaller ,mg3.firmaCliente')
              ->limit(1)
              ->get('multipunto_general AS mg');
      if($query->num_rows()==1){
        if($query->row()->firmaTecnico!=''){
          return true;
        }
      }
      return false;
  }
  //Valida sólo la campaña
  public function validarCampania($serie=''){
    $serie = str_replace('-', '', $serie);
    $q = $this->db->where('vin',$serie)->get('campanias');
    if($q->num_rows()==0){
      return 0;
    }else{
      return 1;
    }
  }
  //Valida sólo la campaña y tipo
  public function validarCampaniaTipo($serie='',$tipo=''){
    $serie = str_replace('-', '', $serie);
    $tipo = str_replace('-', '', $tipo);
    $q = $this->db->where('vin',$serie)->where('tipo',$tipo)->get('campanias');
    if($q->num_rows()==0){
      return 0;
    }else{
      return 1;
    }
  }
  //obtener items de la orden de grupo y operacion
  public function getGruposAndOperaciones($idorden=''){
  return $this->db->select('a.*, g.clave as grupo, go.articulo as operacion')->join('grupos g','a.grupo = g.id')->join('grupo_operacion go','go.articulo= a.operacion and go.idgrupo = a.grupo')->where('idorden',$idorden)->where('tipo',1)->get('articulos_orden a')->result();
  }
  //Actualizar IVA Y TOTAL CUANDO SE CONFIRME LA ORDEN
  public function actualizarIVAyTotal($idorden='',$iva=0,$total=0){
    $q = $this->db->where('id',$idorden)->select('total_orden,restante,iva,anticipo')->get('ordenservicio');

    if($q->num_rows()==1){
      $datos = array('total_orden' =>(float)$q->row()->total_orden+$total,
                    'restante' =>(float)($q->row()->total_orden+$total)-$q->row()->anticipo,
                    'iva' =>(float)$q->row()->iva+$iva,
                    'modificar_promesa'=>true,
                  );

      $this->db->where('id',$idorden)->update('ordenservicio',$datos);
      return true;
    }
  }
  //Modelo
  public function get_result($campo,$value,$tabla){
        $result = $this->db->where($campo,$value)->get($tabla)->result();
        return $result;
  }
  //Obtener fecha con base al status de las transiciones
  //Nueva version
  public function getFechaTransicion($status='',$id_cita='',$id_operacion=''){
    if($id_operacion!=''){
      $this->db->where('id_operacion',$id_operacion);
    }
    $q = $this->db->select('fin')->limit(1)->where('status_nuevo',$status)->where('id_cita',$id_cita)->get('transiciones_estatus');
    if($q->num_rows()==1){
      return $q->row()->fin;
    }else{
      return '';
    }
  }
  //Que no pueda poner otro trabajando si ya existe uno
  public function ValidarUnTrabajoTecnico($id_tecnico='',$id_cita=''){
    $query = "select id_status from citas where id_tecnico = ".$id_tecnico." and id_status not in (3,4,5,6,7,1,11,12,9,13,14,15,16,17,18,19,20,21,22) and fecha_hora > '2019-09-01' and id_cita !=".$id_cita;
    $datos = $this->db->query($query)->result();
    if(count($datos)>0){
      return false;
    }else{
      return true;
    }
  }
  //Datos del mantenimiento correctivo
  public function getManteminientoPreventivo(){
    $this->db->where('idmodelo',$this->input->post('modelo'));
    $this->db->where('anio',$this->input->post('anio'));
    $this->db->where('id_tipo',$this->input->post('tipo_preventivo'));

    $this->db->where('codigo_ereact',$this->input->post('codigo_mo'));
    $this->db->where('id_motor',$this->input->post('motor_preventivo'));

    $this->db->where('codigo',$this->input->post('codigo'));
    $this->db->where('id_descripcion',$this->input->post('descripcion'));
    $this->db->where('id_catalogo',$this->input->post('catalogo_preventivo'));
    return $this->db->get('v_mantenimiento_preventivo')->row();
  }

  public function getStatusIntelisis($id_status=''){
    $q = $this->db->where('id',$id_status)->get('cat_status_intelisis_orden');
    if($q->num_rows()==1){
      return $q->row()->status;
    }else{
      return '';
    }
  }
  public function getSituacionOrden($idorden=''){
    $q = $this->db->where('id',$idorden)->get('ordenservicio');
    if($q->num_rows()==1){
      return $q->row()->id_situacion_intelisis;
    }else{
      return '';
    }
  }
  public function getSituacionIntelisis($id_situacion=''){
    $q = $this->db->where('id',$id_situacion)->get('cat_situacion_intelisis');
    if($q->num_rows()==1){
      return $q->row()->situacion;
    }else{
      return '';
    }
  }
  public function getStatusIntelisisOrden($id_status=''){
    $q = $this->db->where('id',$id_status)->get('cat_status_orden');
    if($q->num_rows()==1){
      return $q->row()->status;
    }else{
      return '';
    }
  }
  public function getUbicacionUnidad($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->get('citas');
    if($q->num_rows()==1){
      return $q->row()->ubicacion_unidad;
    }else{
      return '';
    }
  }
  //Obtener el estatus anterior antes de detener la cita por parte del asesor
   public function getLastStatus($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->get('citas');
    if($q->num_rows()==1){
      return $q->row()->id_ultimo_status;
    }else{
      return 1;
    }
  }
  //Validación para que no actualice el último
  public function validarUltimoStatus($id_cita=''){
    $id_ultimo_status = $this->getLastStatus($id_cita);
    if($id_ultimo_status==15 ||$id_ultimo_status==16||$id_ultimo_status==17||$id_ultimo_status==18||$id_ultimo_status==19  ){
      //Si entra aquí no debo actualizar
      return false;
    }else{
      //Si entra aquí debo actualizar
      return false;
    }
  }

  //PROACTIVO INTELISIS
   public function saveComentarioCampanias(){
       $id= $this->input->post('id');
       $datos = array('comentario' => $this->input->post('comentario'),
                      'id_campania' => $this->input->post('id_cita'),
                      'fecha_creacion' =>date('Y-m-d H:i:s'),
                      'id_usuario'=>$this->session->userdata('id_usuario'),
                      'fecha_notificacion' =>($this->input->post('cronoFecha')).' '.$this->input->post('cronoHora'),
        );
       //$this->db->where('id_cita',$datos['id_cita'])->set('ultimo_comentario_llamada',date('Y-m-d H:i:s'))->update('citas');
          $this->db->insert('historial_comentarios_campanias',$datos);

        //Insertar notificación
        if($this->input->post('cronoFecha')!='' && $this->input->post('cronoFecha')!='cronoHora'){
              $notific['titulo'] = $this->input->post('cronoTitulo');
              $notific['texto']= $this->input->post('comentario');
              $notific['id_user'] = $this->session->userdata('id_usuario');
              $notific['estado'] = 1;
              $notific['tipo_notificacion'] = 2;
              $notific['fecha_hora'] = ($this->input->post('cronoFecha')).' '.$this->input->post('cronoHora');
              //$notific['url'] = base_url().'index.php/bodyshop/ver_proyecto/'.$this->input->post('idp');
              $notific['estadoWeb'] = 1;
              $this->db->insert(CONST_BASE_PRINCIPAL.'noti_user',$notific);
            }

        echo 1;exit();
    }
    public function getComentariosCampanias($id_campania=''){
      return $this->db->where('id_campania',$id_campania)->get('historial_comentarios_campanias')->result();

    }
  //PERMISOS

  public function update($campo,$value,$tabla,$data){
      return $this->db->where($campo,$value)->update($tabla,$data);
    }
  public function get_row($campo,$value,$tabla){
        return $this->db->where($campo,$value)->get($tabla)->row();
  }

  //FIN PERMISOS
  //Función para saber si la cita no cae en la hora de comida
  public function validarCitaHoraComida($hora='',$fecha='',$idtecnico='',$hora_inicio=false){
    $fecha = ($fecha);
    if($hora_inicio){
       $query = "select * FROM horarios_tecnicos where ADDTIME(TIME('".$hora.":00'),'00:01:00') between hora_inicio_comida and hora_fin_comida  and fecha = '".$fecha."'and id_tecnico =".$idtecnico;
    }else{
      $query = "select * FROM horarios_tecnicos where SUBTIME(TIME('".$hora.":00'),'00:01:00') between hora_inicio_comida and hora_fin_comida  and fecha = '".$fecha."'and id_tecnico =".$idtecnico;
    }

   
    $qh = $this->db->query($query)->result();
    if(count($qh)>0){
      return false;
    }else{
      return true;
    }
  //COMENTARIOS ORDEN DE SERVICIO
}
 public function saveComentario_ordenServicio(){
     $id= $this->input->post('id');
     $datos = array('comentario' => $this->input->post('comentario'),
                    'idorden' => $this->input->post('idorden'),
                    'fecha_creacion' =>date('Y-m-d H:i:s'),
                    'id_usuario'=>$this->session->userdata('id_usuario'),
      );
        $this->db->insert('historial_comentarios_orden_servicio',$datos);

      echo 1;exit();
  }
  public function getComentarios_orden_servicio($idorden=''){
      return $this->db->where('idorden',$idorden)->get('historial_comentarios_orden_servicio')->result();

  }
  //Esta funcion regresa de la tabla mensajes_enviados_orden para saber si un email, o sms ya fueron enviados
  public function getMessageSend(){
    $datos = $this->db->get('mensajes_enviados_orden')->result();
    $regreso = array();
    foreach ($datos as $key => $dato) {
        //$keyname = $dato->idorden.'-'.$dato->tipo_mensaje.'-'.$dato->enviado;
        // $regreso->$keyname = $dato->enviado;
      $regreso[] = $dato->idorden.'-'.$dato->tipo_mensaje.'-'.$dato->enviado;
    }
    return $regreso;
  }

  //Validar si tiene una cotización extra
  public function validarCotizacionExtra($idorden=''){
    $q = $this->db->where('idorden',$idorden)->where('tipo',2)->get('articulos_orden')->result();
    if(count($q)>0) {
      return true;
    }else{
      return false;
    }
  }

  /* *** FUNCIÓN PARA ACTUALIZAR TOTALES DE ORDEN *** */
  public function updateOrdenTotal($idorden=''){
   /* 5 = Preventivo
    4 = Correctivo
    3 = GENÉRICO
    2 = Cotización extra de la orden */

    
    // $mantenimientos = $this->db->where_in('tipo',array(3,4,5))->get($articulos_orden)->result();
    // foreach ($mantenimientos as $m => $man) {
    //   # code...
    // }
  }
  //guarda los operadores
  public function guardarExtraPrepiking(){
     $id= $this->input->post('idprepiking_s');
     $editar_descripcion= $this->input->post('editar_descripcion'); //Si puede editar la descripcion entonces el registro si es individual, de lo contario no

     //print_r($_POST);die();
     $datos = array('codigo' => $this->input->post('codigo'),
                    'descripcion' => $this->input->post('descripcion'),
                    'id_cita' => $this->input->post('id_cita'),
                    'id_usuario' => $this->session->userdata('id_usuario'),
                    'created_at' =>date('Y-m-d H:i:s'),
                    'registro_individual'=>$editar_descripcion,
                    'id_operacion'=>$this->input->post('id_operacion_save'),
      );
     if($id==0){
        $exito = $this->db->insert('prepiking_extra_cita',$datos);
        $id = $this->db->insert_id();
     }else{
        $exito = $this->db->where('id',$id)->update('prepiking_extra_cita',$datos);
     }
      
      if($exito){
        echo 1;
      } else{
        echo 0;
      }
      exit();
  }
  //Obtener el estatus anterior de la cita
  public function getStatusAnterior($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->select('id_status_anterior')->get('citas');
    if($q->num_rows()==1) {
      return $q->row()->id_status_anterior;
    }else{
      return '';
    }
  }
  //Obtener la fecha de una cita
  public function getDateCita($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->limit(1)->select('fecha')->get('tecnicos_citas');
    if($q->num_rows()==1) {
      return $q->row()->fecha;
    }else{
      return false;
    }
  }
  //Obtiene  si es login de usuario de refacciones
  public function getUserRefacciones($usuario='',$password){
    $q = $this->db->where('adminUsername',$usuario)->where('adminPassword',$password)->where('p_refacciones',1)->get(CONST_BASE_PRINCIPAL.'admin');
    if ($q->num_rows()==1) {
      return true;
    }else{
      return false;
    }
  }
  public function getClienteCita($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->get('citas');
    if ($q->num_rows()==1) {
      return $q->row()->datos_nombres.' '.$q->row()->datos_apellido_paterno.' '.$q->row()->datos_apellido_materno;
    }else{
      return '';
    }
  }
  public function validarAutorizacionGerencia($id_cita=''){
    //Saber si ya actualizó gerencia
      $q = $this->db->where('id_cita',$id_cita)->select('status_gerente')->get('ordenservicio');
      if ($q->num_rows()==1) {
         if($q->row()->status_gerente){
          return 1;
         }else{
          return 0;
         }
      }else{
        return 0;
      }
  }
  //Obtiene firma del jefe de taller
  public function recuperarFirmaJDT($idOrden = ''){
      $q = $this->db->where('orden',$idOrden)->select("mg3.firmaJefeTaller")->join('multipunto_general_3 mg3','mg3.idOrden = mg.id')->get('multipunto_general mg');
      if($q->num_rows()==1){
          return 1;
      }else{
          return 0;
      }
  }
  public function GuardarDescripcionesPaquetes($id_cita='',$idservicio='',$idmodelo){
    
    if($this->input->post()){
        $descripciones = $this->db->where('p.idmodelo',$idmodelo)->where('p.id',$idservicio)->join('prepiking p','pa.idprepiking = p.id')->select('pa.codigo,pa.descripcion,p.servicio')->get('prepiking_articulos pa')->result();
        foreach ($descripciones as $key => $value) {
          $datos_prepiking = array('codigo' =>$value->codigo, 
                                   'descripcion'=>$value->descripcion,
                                   'id_cita'=>$id_cita,
                                   'created_at'=>date('Y-m-d H:i:s'),
                                   'registro_individual'=>0,
                                   'id_usuario'=>$this->session->userdata('id_usuario'),
          );
          $this->db->insert('prepiking_extra_cita',$datos_prepiking);
          # code...
        }
    }
  }
  public function getPrepikingDescripcionId($id=0){
    return $this->db->where('id',$id)->get('prepiking_extra_cita')->result();
  }
  //Validar si la orden tiene artículos
  public function validOrderArt(){
    $data = $this->db->where('temporal',$this->input->post('temporal'))->get('articulos_orden')->result();
    if(count($data)>0){
      return true;
    }else{
      return false;
    }
  }
  //Obtener la hora de detenido 
  public function stopHourByStatus($id_cita='',$status=''){
    $q = $this->db->select('fin')->where('id_cita',$id_cita)->where('status_nuevo',$status)->order_by('id','desc')->limit(1)->get('transiciones_estatus');
    if ($q->num_rows()==1) {
      return $q->row()->fin;
    }else{
      return '';
    }
  }
  public function validOrder($idcita=''){
    $q = $this->db->where('id_cita',$idcita)->get('ordenservicio');
    if($q->num_rows()==1){
      return true;
    }else{
      return false;
    }
  }
public function ReplyFilesOrder($id_cita='',$id_cita_nuevo=''){

      $contenedor["id_cita"] = $id_cita;
      $contenedor["id_cita_nuevo"] = $id_cita_nuevo;
      $url = CONST_URL_CONEXION."conexion/DuplicarRegistros/duplicar_Registro";
      $ch = curl_init($url);
      //Creamos el objeto json para enviar por url los datos del formulario
      $jsonDataEncoded = json_encode($contenedor);
      //Para que la peticion no imprima el resultado como una cadena, y podamos manipularlo
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      //Adjuntamos el json a nuestra petición
      curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonDataEncoded);
      //Agregamos los encabezados del contenido
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

      //Obtenemos la respuesta
      $response = curl_exec($ch);

      //sdebug_var($response);die();
      // Se cierra el recurso CURL y se liberan los recursos del sistema
      curl_close($ch);
      $result = json_decode($response,true);
    }
    //ASOCIAR ORDEN
  public function asociarOrden($idcita=''){
    return $this->replyCita($idcita);
  }
  public function replyCita($idcita=''){
    $cita = $this->db->where('id_cita',$idcita)->get('citas');
    if($cita->num_rows()==1){
    $cita = $cita->row();
    $datos = array(
                    'transporte' => $cita->transporte,
                    'email' => $cita->email,
                    'vehiculo_anio' => $cita->vehiculo_anio,
                    'vehiculo_modelo' => $cita->vehiculo_modelo,
                    'vehiculo_version' => $cita->vehiculo_version,
                    'vehiculo_placas' => $cita->vehiculo_placas,
                    'vehiculo_numero_serie' => $cita->vehiculo_numero_serie,
                    'comentarios_servicio' => $cita->comentarios_servicio,
                    'asesor' => $cita->asesor,
                    'fecha_anio' => $cita->fecha_anio,
                    'fecha_mes' => $cita->fecha_mes,
                    'fecha_dia' => $cita->fecha_dia,
                    'datos_email' => $cita->datos_email,
                    'datos_nombres' => $cita->datos_nombres,
                    'datos_apellido_paterno' => $cita->datos_apellido_paterno,
                    'datos_apellido_materno' => $cita->datos_apellido_materno,
                    'datos_telefono' => $cita->datos_telefono,
                    'fecha_creacion' => date('Y-m-d'),
                    'status' => $cita->status,
                    'servicio' => $cita->servicio,
                    'fecha' => $cita->fecha,
                    'id_horario' => '',
                    'realizo_servicio' => $cita->realizo_servicio,
                    'fecha_hora' => $cita->fecha_hora,
                    'id_tecnico' => $cita->id_tecnico,
                    'id_opcion_cita' => $cita->id_opcion_cita,
                    'id_color' => $cita->id_color,
                    'id_status' => 1,
                    'comentarios_cita' => $cita->comentarios_cita,
                    'id_status_color' => $cita->id_status_color,
                    'origen' => $cita->origen,
                    'activo' => $cita->activo,
                    'reagendada' => $cita->reagendada,
                    'confirmada' => $cita->confirmada,
                    'id_usuario' => $cita->id_usuario,
                    'id_usuario_reagendo' => $cita->id_usuario_reagendo,
                    'id_servicio_tecnico' => $cita->id_servicio_tecnico,
                    'duda' => $cita->duda,
                    'demo' => $cita->demo,
                    'historial' => $cita->historial,
                    'cancelada' => $cita->cancelada,
                    'ultimo_comentario_llamada' => $cita->ultimo_comentario_llamada,
                    'fecha_creacion_all' => date('Y-m-d H:i:s'),
                    'fecha_reagendada' => $cita->fecha_reagendada,
                    'id_asesor' => $cita->id_asesor,
                    'unidad_entregada' => $cita->unidad_entregada,
                    'id_prepiking' => $cita->id_prepiking,
                    'realizo_prepiking' => $cita->realizo_prepiking,
                    'id_modelo_prepiking' => $cita->id_modelo_prepiking,
                    'fecha_termino_lavado' => $cita->fecha_termino_lavado,
                    'ubicacion_unidad' => $cita->ubicacion_unidad,
                    'id_ultimo_status' => $cita->id_ultimo_status,
                    'numero_cliente' => $cita->numero_cliente,
                    'fecha_entrega_unidad' => $cita->fecha_entrega_unidad,
                    'vehiculo_kilometraje' => $cita->vehiculo_kilometraje,
                    'cita_previa' => 0,
                    'id_status_anterior' => $cita->id_status_anterior,
                    'reparacion' => $cita->reparacion,
                    'orden_magic' => $cita->orden_magic,
                    'orden_asociada' => $idcita,
    );
    $this->db->insert('citas',$datos);
      $id_cita_new = $this->db->insert_id();
      //REPLICAR TÉCNICOS CITAS
      $tecnicos_citas = $this->db->where('id_cita',$idcita)->get('tecnicos_citas')->result();
      foreach ($tecnicos_citas as $key => $value) {
        $array_tc = array('id_cita' => $id_cita_new,
                          'id_tecnico' => $value->id_tecnico,
                          'hora_inicio' => $value->hora_inicio,
                          'hora_fin' => $value->hora_fin,
                          'fecha' => $value->fecha,
                          'dia_completo' => $value->dia_completo,
                          'fecha_fin' => $value->fecha_fin,
                          'hora_inicio_dia' => $value->hora_inicio_dia,
                          'activo' => 0,
                          'historial' => 1,
        );
        $this->db->insert('tecnicos_citas',$array_tc);
      }
      //REPLICAR ORDEN
      $orden = $this->db->where('id_cita',$idcita)->get('ordenservicio');
      //echo $this->db->last_query();die();
      if($orden->num_rows()==1){
        $orden = $orden->row();
        $id_orden_old = $orden->id;
        $array_orden = array(
            'telefono_asesor' => $orden->telefono_asesor,
            'extension_asesor' => $orden->extension_asesor,
            'numero_interno' => $orden->numero_interno,
            'fecha_recepcion' => $orden->fecha_recepcion,
            'hora_recepcion' => $orden->hora_recepcion,
            'fecha_entrega' => $orden->fecha_entrega,
            'hora_entrega' => $orden->hora_entrega,
            'nombre_compania' => $orden->nombre_compania,
            'nombre_contacto_compania' => $orden->nombre_contacto_compania,
            'ap_contacto' => $orden->ap_contacto,
            'am_contacto' => $orden->am_contacto,
            'rfc' => $orden->rfc,
            'correo_compania' => $orden->correo_compania,
            'calle' => $orden->calle,
            'nointerior' => $orden->nointerior,
            'noexterior' => $orden->noexterior,
            'colonia' => $orden->colonia,
            'municipio' => $orden->municipio,
            'cp' => $orden->cp,
            'estado' => $orden->estado,
            'telefono_movil' => $orden->telefono_movil,
            'otro_telefono' => $orden->otro_telefono,
            'vehiculo_identificacion' => $orden->vehiculo_identificacion,
            'vehiculo_kilometraje' => $orden->vehiculo_kilometraje,
            'id_cita' => $id_cita_new,
            'total_orden' => $orden->total_orden,
            'restante' => $orden->restante,
            'anticipo' => $orden->anticipo,
            'iva' => $orden->iva,
            'temporal' => '',
            'cita_nueva' => $orden->cita_nueva,
            'id_tipo_orden' => 2,
            'motor' => $orden->motor,
            'transmision' => $orden->transmision,
            'numero_cliente' => $orden->numero_cliente,
            'id_tipo_pago' => $orden->id_tipo_pago,
            'id_condicion' => $orden->id_condicion,
            'id_tipo_operacion' => $orden->id_tipo_operacion,
            'id_tipo_precio' => $orden->id_tipo_precio,
            'id_concepto' => $orden->id_concepto,
            'id_moneda' => $orden->id_moneda,
            'id_uen' => $orden->id_uen,
            'campania' => $orden->campania,
            'cotizacion_confirmada' => $orden->cotizacion_confirmada,
            'fecha_confirmacion' => $orden->fecha_confirmacion,
            'total_orden_multi' => $orden->total_orden_multi,
            'restante_multi' => $orden->restante_multi,
            'anticipo_multi' => $orden->anticipo_multi,
            'iva_multi' => $orden->iva_multi,
            'status_lavado' => $orden->status_lavado,
            'color' => $orden->color,
            'agente' => $orden->agente,
            'almacen' => $orden->almacen,
            'idcategoria' => $orden->idcategoria,
            'idsubcategoria' => $orden->idsubcategoria,
            'total_horas' => $orden->total_horas,
            'sucursal' => $orden->sucursal,
            'vehiculo_marca' => $orden->vehiculo_marca,
            'cliente_nuevo' => $orden->cliente_nuevo,
            'id_status_intelisis' => $orden->id_status_intelisis,
            'id_situacion_intelisis' => $orden->id_situacion_intelisis,
            'id_status' => $orden->id_status,
            'vehiculo_nuevo' => $orden->vehiculo_nuevo,
            'created_at' => date('Y-m-d H:i:s'),
            'archivoInventario' => $orden->archivoInventario,
            'fecha_finalizacion' => $orden->fecha_finalizacion,
            'fecha_inicio' => $orden->fecha_inicio,
            'tipo_cliente' => $orden->tipo_cliente,
            'cilindros' => $orden->cilindros,
            'inicio_vigencia' => $orden->inicio_vigencia,
            'modificar_promesa' => $orden->modificar_promesa,
            'cilindro_id' => $orden->cilindro_id,
            'combustible_id' => $orden->combustible_id,
            'motor_id' => $orden->motor_id,
            'transmision_id' => $orden->transmision_id,
            'paquete_id' => $orden->paquete_id,
        );
        $this->db->insert('ordenservicio',$array_orden);
        $id_order_new = $this->db->insert_id();

        $articulos_orden = $this->db->where('idorden',$id_orden_old)->get('articulos_orden')->result();
        foreach ($articulos_orden as $a => $articulo) {
          $array_art_orden = array(
                                  'descripcion' => $articulo->descripcion, 
                                  'idorden' => $id_order_new,
                                  'precio_unitario' => $articulo->precio_unitario ,
                                  'cantidad' => $articulo->cantidad, 
                                  'descuento' => $articulo->descuento, 
                                  'total' => $articulo->total ,
                                  'id_usuario' => $this->session->userdata('id_usuario'), 
                                  'created_at' => date('Y-m-d H:i:s'), 
                                  'updated_at' =>'', 
                                  'temporal' => '', 
                                  'operacion' => $articulo->operacion ,
                                  'tipo' => $articulo->tipo, 
                                  'total_horas' => $articulo->total_horas ,
                                  'costo_hora' => $articulo->costo_hora, 
                                  'grupo' => $articulo->grupo, 
          );
        }
        $this->db->insert('articulos_orden',$array_art_orden);
      } //existe la orden
      //FIN ORDEN REPLICAR
      $this->ReplyFilesOrder($idcita,$id_cita_new);
      $this->GenerateTXTOrderA($idcita,$id_cita_new);
      return $id_cita_new;
    }//Cita
    else{
      return -1;
    }
  }
  public function GenerateTXTOrderA($id_cita_old='',$id_cita_new){
      $contenedor["id_cita"] = $id_cita_new;
      $contenedor["id_cita_asociado"] = $id_cita_old;
      $url = CONST_ASSOCIATED_DATA;
      $ch = curl_init($url);
      //Creamos el objeto json para enviar por url los datos del formulario
      $jsonDataEncoded = json_encode($contenedor);
      //Para que la peticion no imprima el resultado como una cadena, y podamos manipularlo
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      //Adjuntamos el json a nuestra petición
      curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonDataEncoded);
      //Agregamos los encabezados del contenido
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      //Obtenemos la respuesta
      $response = curl_exec($ch);
      // Se cierra el recurso CURL y se liberan los recursos del sistema
      curl_close($ch);
      $result = json_decode($response,true);
      var_dump($result);
    }
  //Validar si pasó la unidad por trabajando
  public function unidadTrabajando($idcita=''){
    $query = "select * from transiciones_estatus where id_cita = ".$idcita." and (status_nuevo = 'trabajando' or status_anterior = 'trabajando')";
    $result = $this->db->query($query)->result();
    if(count($result)>0){
      return true;
    }else{
      return false;
    }
  }
  public function retornoTemporal(){
    $temporal = $this->random(20);

    $q = $this->db->where('temporal',$temporal)->get('articulos_orden');
    if($q->num_rows()==1){
      $this->retornoTemporal();
    }else{
      return $temporal;
    }

  }
  function random($num){
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        for ($i = 0; $i < $num; $i++) {
             $string .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $string;
  }
   //obtener el operador en base a su id
  public function getOperacionId($id=0){
    $q = $this->db->where('id',$id)->get('articulos_orden');
    if($q->num_rows()==1){
      return $q->row();
    }else{
      return false;
    }
  }
  //obtener el código genérico en base a su id
  public function getCodigoId($id=0){
    return $this->db->where('id',$id)->get('codigos_gen')->result();
  }
  public function guardarCodigoGen(){
     $id= $this->input->post('id');
     $datos = array('codigo' => $this->input->post('codigo'),
                    'descripcion' => $this->input->post('descripcion'),
                    'id_usuario' => $this->session->userdata('id_usuario')
      );
      $q= $this->db->where('codigo',$datos['codigo'])->where('id !=',$id)->get('codigos_gen');
      if($q->num_rows()==1){
        echo -1;exit();
      }
      $q= $this->db->where('codigo_operacion_ereact',$datos['codigo'])->get('correctivo');
      if($q->num_rows()>0){
        echo -2;exit();
      }
      if($id==0){
          $exito = $this->db->insert('codigos_gen',$datos);
      }else{
          $exito = $this->db->where('id',$id)->update('codigos_gen',$datos);
      }
        if($exito){
          echo 1;die();
        } else{
          echo 0; die();
        }
  }
  //Obtener operaciones extras
  public function operacionesExtras($id_cita='',$id_operacion=0){
    //Si viene la operación es que están editando y deben salir todas de lo contrario sólo deben salir las NO asignadas
    if($id_operacion==0){
      $this->db->where('id_operacion',0);
    }else{
      $this->db->where('id_operacion',$id_operacion);
      $this->db->or_where('id_operacion',0);
    }
    return $this->db->where('id_cita',$id_cita)->where('autoriza_cliente',1)->where('pza_garantia',0)->where('activo',1)->get('presupuesto_multipunto')->result();
  }
  //Cambiar horario operacion
  public function cambiarHorarioOperacion(){
    
    $id_cita = $this->getIdCitaByOrden($_POST['id_orden_horario']);    
    if($this->input->post('dia_completo')==1){
      $dia_completo = 1;
      $id_operacion = $_POST['id_detalle'];
      $fecha_inicio =($this->input->post('fecha_inicio'));
      $fecha_fin = ($this->input->post('fecha_fin'));
      $id_tecnico= $this->input->post('id_tecnico');
      $tecnico_actual = $this->principal->getGeneric('id',$_POST['id_detalle'],'articulos_orden','id_tecnico');
      $id_status_actual = $this->principal->getGeneric('id',$_POST['id_detalle'],'articulos_orden','id_status');

      if($id_status_actual!=1){
        echo -6;exit();
      }

      if(!$this->getHoraLaboralTecnico($this->input->post('hora_comienzo'),($this->input->post('fecha_inicio')),$id_tecnico)){
        echo -3;exit();
      }
      if(!$this->validarHorarioOcupado( $fecha_inicio,$fecha_fin,$id_tecnico,$this->input->post('hora_comienzo'),'',$id_cita)){
        echo -1;die();
      }

       //Antes de actualizar guardar historial

      $this->db->where('dia_completo',1);
      $registro_old = $this->principal->getGeneric('id_operacion',$_POST['id_detalle'],$_POST['tabla']);

      $registro_anterior_texto = 'id_tecnico: '.$registro_old->id_tecnico.' hora_inicio_dia '.$registro_old->hora_inicio_dia.' hora_fin: '.$registro_old->hora_fin.' fecha: '.$registro_old->fecha.' fecha_fin: '.$registro_old->fecha_fin.' id_operacion: '.$registro_old->id_operacion.' dia_completo: '.$registro_old->dia_completo;
      

      if($this->input->post('hora_comienzo')==''){
        $hora_comienzo = '07:00:00';
      }else{
        $hora_comienzo = $this->input->post('hora_comienzo');
      }

      $datos_tecnicos_hora = array(
        'id_tecnico' =>$this->input->post('id_tecnico'),
        'hora_inicio'=>$hora_comienzo,
        'hora_fin'=>'20:00:00',
        'fecha'=>$fecha_inicio,
        'fecha_fin'=>$fecha_fin,
        'hora_inicio_dia'=>$hora_comienzo,
        'dia_completo' => 1

      );
      $datos_bitacora = array(
        'id_operacion' =>$id_operacion,
        'id_usuario'=>$this->session->userdata('id_usuario'),
        'created_at' => date('Y-m-d H:i:s'),
        'registro_anterior' =>$registro_anterior_texto,
        'tabla' => $_POST['tabla']

      );

      
      $this->db->insert('bitacora_horario_operacion',$datos_bitacora);
      

      $this->db->where('id_operacion',$id_operacion)->where('dia_completo',1)->update($_POST['tabla'],$datos_tecnicos_hora);
      //Actualizar el técnico en la tabla de articulos_orden
      $this->db->where('id',$id_operacion)->set('id_tecnico',$_POST['id_tecnico'])->update('articulos_orden');
      
      if($this->input->post('hora_inicio_extra') !='' && $this->input->post('hora_fin_extra') !='' && $this->input->post('fecha_parcial') !=''){
        
        if(($this->input->post('fecha_parcial'))<($this->input->post('fecha_inicio'))){
          echo -2;exit();
        }
        if(!$this->validarDiaExtra($this->input->post('hora_inicio_extra'),$this->input->post('hora_fin_extra'),($this->input->post('fecha_parcial')),$this->input->post('id_tecnico'),$id_cita)){
          echo -1;die();
        }
        
        $this->db->where('dia_completo',0);
        $registro_old = $this->principal->getGeneric('id_operacion',$_POST['id_detalle'],$_POST['tabla']);

        $registro_anterior_texto = 'id_tecnico: '.$registro_old->id_tecnico.' hora_inicio_dia '.$registro_old->hora_inicio_dia.' hora_fin: '.$registro_old->hora_fin.' fecha: '.$registro_old->fecha.' fecha_fin: '.$registro_old->fecha_fin.' id_operacion: '.$registro_old->id_operacion.' dia_completo: '.$registro_old->dia_completo;
        

        $datos_tecnicos_hora = array(
          'id_tecnico' =>$this->input->post('id_tecnico'),
          'hora_inicio'=>$_POST['hora_inicio_extra'],
          'hora_fin'=>$this->input->post('hora_fin_extra'),
          'fecha'=>($_POST['fecha_parcial']),
          'fecha_fin'=>($_POST['fecha_parcial']),
          'hora_inicio_dia'=>$_POST['hora_inicio_extra'],
          'dia_completo' => 0

        );
        
        $datos_bitacora = array(
          'id_operacion' =>$id_operacion,
          'id_usuario'=>$this->session->userdata('id_usuario'),
          'created_at' => date('Y-m-d H:i:s'),
          'registro_anterior' =>$registro_anterior_texto,
          'tabla' => $_POST['tabla']

        );

        $this->db->insert('bitacora_horario_operacion',$datos_bitacora);

        $this->db->where('id_operacion',$id_operacion)->where('dia_completo',0)->update($_POST['tabla'],$datos_tecnicos_hora);
      }else{
        //Si no llega nada de extra, borrar
        $this->db->where('id_operacion',$id_operacion)->where('dia_completo',0)->delete($_POST['tabla']);
      }
      //Actualizar técnico cita
      if($_POST['tabla']=='tecnicos_citas'){
        $this->db->where('id_cita',$id_cita)->set('id_tecnico',$_POST['id_tecnico'])->update('citas');
      }
      echo 1;exit();
    }else{
      $dia_completo = 0;
      $id_operacion = $_POST['id_detalle'];
      $fecha_inicio =($this->input->post('fecha_reasignar'));
      $fecha_fin = ($this->input->post('fecha_reasignar'));
      $tecnico_actual = $this->principal->getGeneric('id',$_POST['id_detalle'],'articulos_orden','id_tecnico');

      $id_status_actual = $this->principal->getGeneric('id',$_POST['id_detalle'],'articulos_orden','id_status');

      if($id_status_actual!=1){
        echo -6;exit();
      }
      if(!$this->getHoraLaboralTecnico($this->input->post('hora_inicio'),$fecha_inicio,$this->input->post('id_tecnico'),$this->input->post('hora_fin'))){
        echo -3;exit();
      }

      if(!$this->validarCitaHoraComida($this->input->post('hora_inicio'),$_POST['fecha_reasignar'],$this->input->post('id_tecnico'),true)) {
        echo -5;exit();
      }
      $query = "select * FROM tecnicos_citas tc join citas c on c.id_cita = tc.id_cita where TIME(tc.hora_inicio) between ADDTIME(TIME('".$this->input->post('hora_inicio').":00'),'00:01:00') and SUBTIME(TIME('".$this->input->post('hora_fin').":00'),'00:01:00') and tc.fecha = '".($this->input->post('fecha_reasignar'))."'and tc.id_tecnico =".$this->input->post('id_tecnico').' and (tc.id_operacion !='.$id_operacion. ' or tc.id_operacion is null) and tc.historial=0 and c.id_status not in(5)';
      $qh = $this->db->query($query)->result();
      if(count($qh)>0){
        echo -1;exit();
      }
      $query = "select * FROM tecnicos_citas tc join citas c on c.id_cita = tc.id_cita where SUBTIME(TIME('".$this->input->post('hora_fin').":00'),'00:01:00') BETWEEN TIME(tc.hora_inicio_dia) and TIME(tc.hora_fin)  and tc.fecha = '".($this->input->post('fecha_reasignar'))."'and tc.id_tecnico = ".$this->input->post('id_tecnico').' and (tc.id_operacion !='.$id_operacion. ' or tc.id_operacion is null) and tc.historial=0 and c.id_status not in(5)';
      $qh = $this->db->query($query)->result();

      if(count($qh)>0){

        echo -1;exit();
      }
      $query = "select * FROM tecnicos_citas_historial tc join citas c on c.id_cita = tc.id_cita where TIME(tc.hora_inicio) between ADDTIME(TIME('".$this->input->post('hora_inicio').":00'),'00:01:00') and SUBTIME(TIME('".$this->input->post('hora_fin').":00'),'00:01:00') and tc.fecha = '".($this->input->post('fecha_reasignar'))."'and tc.id_tecnico =".$this->input->post('id_tecnico').' and (tc.id_operacion !='.$id_operacion. ' or tc.id_operacion is null) and tc.historial=0 and c.id_status not in(5)';
      $qh = $this->db->query($query)->result();
      if(count($qh)>0){
        echo -1;exit();
      }
      $query = "select * FROM tecnicos_citas_historial tc join citas c on c.id_cita = tc.id_cita where SUBTIME(TIME('".$this->input->post('hora_fin').":00'),'00:01:00') BETWEEN TIME(tc.hora_inicio_dia) and TIME(tc.hora_fin)  and tc.fecha = '".($this->input->post('fecha_reasignar'))."'and tc.id_tecnico = ".$this->input->post('id_tecnico').' and (tc.id_operacion !='.$id_operacion. ' or tc.id_operacion is null) and tc.historial=0 and c.id_status not in(5)';
      $qh = $this->db->query($query)->result();

      if(count($qh)>0){

        echo -1;exit();
      }
      $fecha_tecnico =$fecha_inicio;
      $datos_tecnicos_hora = array(
        'id_tecnico' =>$this->input->post('id_tecnico'),
        'hora_inicio'=>$this->input->post('hora_inicio'),
        'hora_fin'=>$this->input->post('hora_fin'),
        'fecha'=>($_POST['fecha_reasignar']),
        'fecha_fin'=>($_POST['fecha_reasignar']),
        'hora_inicio_dia'=>$this->input->post('hora_inicio'),

      );
      //Antes de actualizar guardar historial
      $registro_old = $this->principal->getGeneric('id_operacion',$_POST['id_detalle'],$_POST['tabla']);
      $registro_anterior_texto = 'id_tecnico: '.$registro_old->id_tecnico.' hora_inicio_dia '.$registro_old->hora_inicio_dia.' hora_fin: '.$registro_old->hora_fin.' fecha: '.$registro_old->fecha.' fecha_fin: '.$registro_old->fecha_fin.' id_operacion: '.$registro_old->id_operacion;
      
      
      $datos_bitacora = array(
        'id_operacion' =>$id_operacion,
        'id_usuario'=>$this->session->userdata('id_usuario'),
        'created_at' => date('Y-m-d H:i:s'),
        'registro_anterior' =>$registro_anterior_texto,
        'tabla' => $_POST['tabla']

      );
      $this->db->insert('bitacora_horario_operacion',$datos_bitacora);
      $this->db->where('id_operacion',$id_operacion)->update($_POST['tabla'],$datos_tecnicos_hora);
      //Actualizar el técnico en la tabla de articulos_orden
      $this->db->where('id',$id_operacion)->set('id_tecnico',$_POST['id_tecnico'])->update('articulos_orden');
      //Actualizar técnico cita
      if($_POST['tabla']=='tecnicos_citas'){
        $this->db->where('id_cita',$id_cita)->set('id_tecnico',$_POST['id_tecnico'])->update('citas');
      }
      echo 1;exit();
    }
  }
  public function getCitaOrdenFord(){
    if(!$_POST){
      $this->db->where('o.fecha_recepcion >=',CONST_FECHA_CITA_NEW_UPDT);
    }else{
      if($_POST['finicio']!='' && $_POST['ffin']!=''){
        $this->db->where('o.fecha_recepcion >=',date2sql($_POST['finicio']));
        $this->db->where('o.fecha_recepcion <=',date2sql($_POST['ffin']));
      }
    }
    return $this->db
                    ->where('c.unidad_entregada',0)
                    ->where('o.cancelada',0)
                    ->where('o.generado_ford',0)
                    //->where('o.fecha_factura is null')
                    ->where('c.id_cita >',CONST_NO_CITA_NEW_UPDT)
                    ->where('o.id_tipo_orden',1)
                    ->where('c.id_status_color !=',3)
                    ->join('ordenservicio o','o.id_cita = c.id_cita')
                     ->join('aux a','c.id_horario=a.id','left')
                    ->select('c.*,o.*,o.id as idorden,c.unidad_entregada,a.fecha as fecha_asesor,a.hora as hora_asesor')
                    ->get('citas c')
                    ->result();
  }
  public function crearArchivo($idOrden = ''){
    $this->curl->curlPost(CONST_FILE,['idCita' => $idOrden], true);
  }
   //Validar si se puede enviar el mensaje de nuevo
   public function validateMessage($tipo_mensaje='',$idorden=''){
    $q = $this->db->where('tipo_mensaje',$tipo_mensaje)
                     ->where('idorden',$idorden)
                     ->select('created_at')
                     ->order_by('created_at','desc')
                     ->limit(1)
                     ->get('mensajes_enviados_orden');
    if($q->num_rows()==1){
      $diferencia = minutosTranscurridos($q->row()->created_at,date('Y-m-d H:i:s'));
      if($diferencia>=30){
        return true;
      }
      return false;
    }
    return true;
  }
  public function getTablaRefacciones($datos=array()){
    $tmpl = array (
    'table_open' => '<table border="0" id="tbl" cellpadding="4" cellspacing="0" class="table table-striped table-bordered table-hover">');
    $this->table->set_template($tmpl);
    $this->table->set_heading('Cantidad','Descripción','#Pieza','Precio unitario','Horas MO','Costo MO','Total');

    foreach($datos[0] as $d => $contenido){
      foreach($contenido as $r => $result){
        $row=array();
        $datos_final_mostrar = explode('||',$result);
        foreach ($datos_final_mostrar as $key => $value) {
          $row[]=$value;
          
        }
        $this->table->add_row($row);        
      }
       
      }
   return $this->table->generate();
  }
  //Saber si ya firmó el presupuesto el asesor 
  public function firmoAsesor($id_cita=''){
     $q = $this->db->select('firma_asesor')
                  ->where('id_cita',$id_cita)
                  ->get('presupuesto_registro');
    if($q->num_rows()==0){
      return false;
    }else{
      if($q->row()->firma_asesor!=''){
        return true;
      }
      return false;
    }
  }
  //validar si ya se asoció Orden
  public function ordenesAsociadas($id_cita=''){
    $data = $this->db->where('orden_asociada',$id_cita)->get('citas')->result();
    if(count($data)>0){
      return false;
    }
    return true;
  }
  public function comparar_datos_orden($id_cita=''){
    $post = [];
    $post = $_POST;
    $citas = $post['citas'];
    //Quito el post de citas por que es una propiedad del array para luego meterlo directo al array
    //principal del post
    unset($post['citas']);
    foreach($citas as $c => $cita){
      $post[$c] = $cita;
    }
    $post['fecha_entrega'] = date2sql($post['fecha_entrega']);
    $campos = $this->db->select('campos')->get('campos_comparar')->row()->campos;
    $sql = "select $campos from citas c join ordenservicio o on c.id_cita = o.id_cita where c.id_cita = $id_cita";
    $result = $this->db->query($sql)->result();
    $array_cambios = [];
    if(count($result)>0){
      foreach($result[0] as $r => $res){
        if(isset($post[$r]) && $result[0]->$r != $post[$r]){
          $array_cambios[$r]['campo_anterior'] = $result[0]->$r;
          $array_cambios[$r]['campo_nuevo'] = $post[$r];
        }
      }
      $this->db->insert('campos_editados_orden',[
        'cambio' => json_encode($array_cambios),
        'id_usuario' => $this->session->userdata('id_usuario'),
        'id_cita' => $id_cita,
        'created_at' => date('Y-m-d H:i:s')
      ]);
    }
  }
  
}
