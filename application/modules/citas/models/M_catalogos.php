<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_catalogos extends CI_Model{

    public function __construct(){
        parent::__construct();
    }
    public function get($tabla='',$order='',$asc='asc',$where=array()){
    	if($order!=''){
    		$this->db->order_by($order,$asc);
    	}
    	if(count($where)>0){
            $this->db->where($where);
    		
    	}
    	return $this->db->get($tabla)->result();
    }
    public function get_result_field($campo_1 = "",$value_1 = "",$campo_2 = "",$value_2 = "",$tabla = ""){
        $result = $this->db->where($campo_1,$value_1)->where($campo_2,$value_2)->get($tabla)->result();
        return $result;
    }
}
