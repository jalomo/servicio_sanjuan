<?php
/**

 **/
class Mapi_sa extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->base = $this->load->database('base_sa', TRUE);
    }

	public function save_register($table, $data)
    {
        $this->base->insert($table, $data);
        return $this->base->insert_id();
    }

    public function get_result_table($tabla){
      $data = $this->base->get($tabla);
      return $data->result();
    }

    public function get_row_table($tabla,$id_tabla,$id){
      $this->base->where($id_tabla,$id);
      $data = $this->base->get($tabla);
      return $data->result();
    }
  }
