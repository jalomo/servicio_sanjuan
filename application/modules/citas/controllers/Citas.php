<?php
//https://github.com/Eonasdan/bootstrap-datetimepicker/issues/1996
defined('BASEPATH') OR exit('No direct script access allowed');
use \Mpdf\Mpdf;
use Spipu\Html2Pdf\Html2Pdf;
require_once APPPATH . "/third_party/PHPExcel.php";
class Citas extends MX_Controller {
   public function __construct()
    {
      parent::__construct();
      $this->load->model('m_citas','mc');
      $this->load->model('m_catalogos','mcat');
      $this->load->model('excel/m_excel','mexcel');
      $this->load->model('dms/m_dms','m_dms'); 
      $this->load->model('dmspaquetes/M_dmspaquetes','mp');
      $this->load->model('layout/m_layout','ml'); //NUEVA VERSION
      $this->load->helper(array('dompdf','correo','notificaciones','correos_usuario'));
      $this->load->model('principal');
      $this->load->library(array('table','zip'));
      date_default_timezone_set(CONST_ZONA_HORARIA);


    }
  public function index()
  {
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $datos['operadores'] = $this->mc->getOperadores();
    $this->blade->set_data($datos)->render('operadores');
  }
  function agregar_operador($id=0)
    {
      if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
      {
        $this->output->set_status_header('409');
        exit();
      }else if($this->session->userdata('id_usuario')==''){
      redirect('login');
      }

      if($this->input->post()){
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('telefono', 'teléfono', 'trim|required|numeric|exact_length[10]');
        $this->form_validation->set_rules('correo', 'correo', 'trim|valid_email|required');
        $this->form_validation->set_rules('rfc', 'rfc', 'trim|required');
        $this->form_validation->set_rules('horario', 'horario', 'trim|required');
       if ($this->form_validation->run()){
          $this->mc->guardarOperador();
          }else{
             $errors = array(
                  'nombre' => form_error('nombre'),
                  'telefono' => form_error('telefono'),
                  'correo' => form_error('correo'),
                  'rfc' => form_error('rfc'),
                  'horario' => form_error('horario'),
             );
            echo json_encode($errors); exit();
          }
      }

      if($id==0){
        $info=new Stdclass();


      }else{
        $info= $this->mc->getOperadorId($id);
        $info=$info[0];
      }


      $data['input_id'] = $id;
      $data['input_nombre'] = form_input('nombre',set_value('nombre',exist_obj($info,'nombre')),'class="form-control" id="nombre" ');

      $data['input_telefono'] = form_input('telefono',set_value('telefono',exist_obj($info,'telefono')),'class="form-control"  id="telefono" maxlength="10" ');

      $data['input_correo'] = form_input('correo',set_value('correo',exist_obj($info,'correo')),'class="form-control" id="correo" ');

      $data['input_rfc'] = form_input('rfc',set_value('rfc',exist_obj($info,'rfc')),'class="form-control" id="rfc" ');
      $data['input_horario'] = form_input('horario',set_value('horario',exist_obj($info,'horario')),'class="form-control" id="horario" ');
      $this->blade->render('nuevo_operador',$data);
  }
  public function tabla_operadores(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $datos['operadores'] = $this->mc->getOperadores();
   $this->blade->set_data($datos)->render('tbl_operadores');
  }
  public function eliminar_operador(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    if($this->db->where('id',$this->input->post('id'))->delete('operadores')){
      echo 1;
    }else{
      echo 0;
    }
  }
  public function lista_citas($id=''){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $data['fecha'] = date('Y-m-d');
    $citas = $this->mc->getCitasOperador($id,$data['fecha']);
    $citas_ordenadas= array();
    foreach ($citas as $c => $value) {
      $citas_ordenadas[$value->fecha][] = $value;
    }

    $this->blade->set_data(array('citas'=>$citas_ordenadas,'id'=>$id,'nombre_operador'=>$this->mc->getNameOperador($id),'fecha'=>$data['fecha']))->render('lista_citas');
  }
  public function tabla_horarios(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
      $citas = $this->mc->getCitasOperador($this->input->post('id'),date2sql($this->input->post('fecha')));
      $citas_ordenadas= array();
      foreach ($citas as $c => $value) {
        $citas_ordenadas[$value->fecha][] = $value;
      }
      $this->blade->set_data(array('citas'=>$citas_ordenadas))->render('tbl_horarios');
  }
  public function agregar_horario($id=0)
    {
      if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
      {
        $this->output->set_status_header('409');
        exit();
      }else if($this->session->userdata('id_usuario')==''){
      redirect('login');
      }
        $this->form_validation->set_rules('horario', 'horario', 'trim|required');
        $this->form_validation->set_rules('hora', 'hora', 'trim|required');
      if ($this->form_validation->run()){
          $this->mc->guardarHorario();
          }else{
            $errors = array(
                'horario' => form_error('horario'),
                 'hora' => form_error('hora'),
            );
            echo json_encode($errors); exit();
      }
   }
   //elimina la cita
  public function eliminar_cita(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    if($this->db->where('id',$this->input->post('id'))->delete('horarios')){
      echo 1;
    }else{
      echo 0;
    }
  }
   //activa o desactiva el horario
  public function cambiar_status(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    if($this->db->where('id',$this->input->post('id'))->set("activo",$this->input->post('valor'))->set("motivo",$this->input->post('motivo'))->update($this->input->post('tabla'))){
      echo 1;
    }else{
      echo 0;
    }
  }
  public function agendar_cita($id=0,$reagendar=0,$proactivo_nueva_unidad=0,$magic=0,$id_instalacion=0){ // el segundo parámetro es para saber si reagendó 0=no, 1 = si

    
    //Validar el permiso para editar
    if($id!=0){
      if(!PermisoAccion('editar_cita')){
        redirect(site_url('citas/ver_citas'));
      }
    }else{
      if(!PermisoAccion('agendar_cita')){
        redirect(site_url('citas/ver_citas'));
      }
    }
    if($this->input->post()){
        $this->form_validation->set_rules('citas[vehiculo_anio]', 'año del vehículo', 'trim|required|numeric');
        $this->form_validation->set_rules('citas[vehiculo_placas]', 'placas', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('citas[vehiculo_modelo]', 'modelo', 'trim|required');
        if($this->input->post('id')==0){
          $this->form_validation->set_rules('citas[vehiculo_numero_serie]', 'número de serie', 'trim|exact_length[17]');
        }else{
          $this->form_validation->set_rules('citas[vehiculo_numero_serie]', 'número de serie', 'trim|required|exact_length[17]');
        }
        $this->form_validation->set_rules('citas[email]', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('citas[datos_nombres]', 'nombre(s)', 'trim|required');
        $this->form_validation->set_rules('citas[datos_apellido_paterno]', 'apellido paterno', 'trim|required');
        $this->form_validation->set_rules('citas[datos_apellido_materno]', 'apellido materno', 'trim|required');
        $this->form_validation->set_rules('citas[datos_telefono]', 'teléfono', 'trim|numeric|exact_length[10]|required');
      $this->form_validation->set_rules('citas[asesor]', 'asesor', 'trim|required');
        //origen viene si se guarda del tablero "tablero" si no viene normal
        if($this->input->post('origen')=='normal'){
           $this->form_validation->set_rules('citas[id_opcion_cita]', 'donde realizó cita', 'trim|required');
            $this->form_validation->set_rules('citas[transporte]', 'transporte', 'trim|required');
            $this->form_validation->set_rules('citas[servicio]', 'servicio', 'trim|required');
            $this->form_validation->set_rules('citas[horario]', 'horario', 'trim|required');
            $this->form_validation->set_rules('citas[fecha]', 'fecha', 'trim|required');

         }else{
           $this->form_validation->set_rules('citas[id_opcion_cita]', 'donde realizó cita', 'trim');
            $this->form_validation->set_rules('citas[transporte]', 'transporte', 'trim');
            $this->form_validation->set_rules('citas[servicio]', 'servicio', 'trim');
            $this->form_validation->set_rules('citas[horario]', 'horario', 'trim');
            $this->form_validation->set_rules('citas[fecha]', 'fecha', 'trim');
         }

        $this->form_validation->set_rules('citas[id_color]', 'color', 'trim|required');
        $this->form_validation->set_rules('citas[id_status_color]', 'estatus', 'trim|required');
        if($this->input->post('tecnico_dias')!=''){
          $this->form_validation->set_rules('fecha_inicio', 'fecha inicio', 'trim');
          $this->form_validation->set_rules('fecha_fin', 'fecha fin', 'trim');
        }
        if($this->input->post('id')==0 && $this->input->post('dia_completo')==null){
          $this->form_validation->set_rules('tecnico', 'técnico', 'trim|required');
        }
        if($this->input->post('id')==0 && $this->input->post('dia_completo')!=null){
          $this->form_validation->set_rules('tecnico_dias', 'técnico', 'trim|required');
        }

        if($this->input->post('id')==0 && $this->input->post('tecnico')!='' && $this->input->post('dia_completo')==null){
            $this->form_validation->set_rules('tecnico', 'tecnico', 'trim|required');
            $this->form_validation->set_rules('hora_inicio', 'hora inicio', 'trim|required');
            $this->form_validation->set_rules('hora_fin', 'hora fin', 'trim');
        }

        $this->form_validation->set_rules('citas[vehiculo_kilometraje]', 'kilometraje', 'trim|numeric|min_length[3]');
        
       
        if($_POST['citas']['id_status_color']==5||$_POST['citas']['id_status_color'] == 4||$_POST['citas']['id_status_color'] == 6){
          $this->form_validation->set_rules('citas[id_modelo_prepiking]', ' ', 'trim');
          $this->form_validation->set_rules('citas[id_prepiking]', ' ', 'trim');
         } 
       if ($this->form_validation->run()){
          $this->mc->guardarCita();
          }else{
             $errors = array(
                  'transporte' => form_error('citas[transporte]'),
                  'vehiculo_anio' => form_error('citas[vehiculo_anio]'),
                  'vehiculo_placas' => form_error('citas[vehiculo_placas]'),
                  //'vehiculo_marca' => form_error('citas[vehiculo_marca]'),
                  'vehiculo_modelo' => form_error('citas[vehiculo_modelo]'),
                  //'vehiculo_version' => form_error('citas[vehiculo_version]'),
                  'vehiculo_numero_serie' => form_error('citas[vehiculo_numero_serie]'),
                  'asesor' => form_error('citas[asesor]'),
                  'fecha' => form_error('citas[fecha]'),
                  'horario' => form_error('citas[horario]'),
                  'email' => form_error('citas[email]'),

                  'nombre' => form_error('citas[datos_nombres]'),
                  'apellido_paterno' => form_error('citas[datos_apellido_paterno]'),
                  'apellido_materno' => form_error('citas[datos_apellido_materno]'),
                  'id_status_color' => form_error('citas[id_status_color]'),
                  'telefono' => form_error('citas[datos_telefono]'),
                  'servicio' => form_error('citas[servicio]'),
                  'id_opcion_cita' => form_error('citas[id_opcion_cita]'),
                  'id_color' => form_error('citas[id_color]'),
                  'tecnico' => form_error('tecnico'),
                  'tecnico_dias' => form_error('tecnico_dias'),
                  'hora_inicio' => form_error('hora_inicio'),
                  'hora_fin' => form_error('hora_fin'),
                  'fecha_inicio' => form_error('fecha_inicio'),
                  'fecha_fin' => form_error('fecha_fin'),
                  'vehiculo_kilometraje' => form_error('citas[vehiculo_kilometraje]'),
                  'id_modelo_prepiking' => form_error('citas[id_modelo_prepiking]'),
                  'id_prepiking' => form_error('citas[id_prepiking]'),
             );
            echo json_encode($errors); exit();
          }
      }

      if($id==0){
        $info=new Stdclass();
        $info_horario=new Stdclass();
        $id_horario = 0;
        $realizo_servicio = 0;
        $tecnico_actual = 0;
        $dia_completo = 0;
        $demo = 0;
        $duda = 0;
        $fecha_verificar = date('Y-m-d');
        $origen = 1; //viene desde el formulario
        $reparacion = 0;
        $id_status_cita_asesor =1;
      }else{
        $info= $this->mc->getCitaId($id);
        $info=$info[0];
        $origen = $info->origen;

        $id_status_cita_asesor = $this->mc->getStatusCita($this->mc->getIdHorarioActual($id));
        $info_horario= $this->mc->getHorarioId($info->id_horario);
        if(count($info_horario)>0){
          $info_horario=$info_horario[0];

          $id_horario = $info->id_horario;
          $realizo_servicio = $info->realizo_servicio;
          $fecha_verificar = $info_horario->fecha;
        }else{
          $info_horario=new Stdclass();
          $id_horario = 0;
          $realizo_servicio = 0;
          $fecha_verificar = date('Y-m-d');
        }
        $tecnico_actual = $info->id_tecnico;
        $dia_completo = 0;

        $demo = $info->demo;
        $duda = $info->duda;
         $reparacion = $info->reparacion;
      }
      if($origen==2){
        $disabled_tablero = 'disabled';
      }else{
         $disabled_tablero = '';
      }

      $data['input_id'] = $id;
      $data['reagendada'] = $reagendar;
      $data['proactivo_nueva_unidad'] = $proactivo_nueva_unidad;
      $data['magic'] = $magic;
      $data['id_instalacion'] = $id_instalacion;
      $data['input_id_horario'] = $id_horario;
      $data['input_realizo_servicio'] = $realizo_servicio;
      $data['input_id_tecnico_actual'] = $tecnico_actual;
      $data['fecha_verificar'] = $fecha_verificar;
      if($origen==1 || $origen == 0){
        $data['origen'] = 'normal';
      }else{
        $data['origen'] = 'tablero';
      }

      // 1viene desde el formulario, 2 desde el tablero
      $data['drop_transporte'] = form_dropdown('citas[transporte]',array_combos($this->mcat->get('cat_transporte','transporte'),'id','transporte',TRUE),set_value('transporte',exist_obj($info,'transporte')),'class="form-control" id="transporte" '.$disabled_tablero.'');

      $email_info = set_value('email',exist_obj($info,'email'));
      if($email_info ==''){
        $email_info = 'notienecorreo@notienecorreo.com.mx';
      }
      
      $data['input_email'] = form_input('citas[email]',$email_info,'class="form-control minusculas" rows="5" id="email" ');

      $data['drop_vehiculo_anio'] = form_dropdown('citas[vehiculo_anio]',array_combos($this->mcat->get('cat_anios','anio','desc'),'anio','anio',TRUE),set_value('vehiculo_anio',exist_obj($info,'vehiculo_anio')),'class="form-control busqueda" id="vehiculo_anio"');

      $data['drop_vehiculo_marca'] = form_dropdown('citas[vehiculo_marca]',array_combos($this->mcat->get('cat_marca','marca'),'marca','marca',TRUE),set_value('vehiculo_marca',exist_obj($info,'vehiculo_marca')),'class="form-control" id="vehiculo_marca"');

      $data['drop_vehiculo_modelo'] = form_dropdown('citas[vehiculo_modelo]',array_combos($this->mcat->get('cat_modelo','modelo'),'modelo','modelo',TRUE),set_value('vehiculo_modelo',exist_obj($info,'vehiculo_modelo')),'class="form-control busqueda" id="vehiculo_modelo"');

      if($id==0){
        $submodelos = array();
      }else{
        $mod = set_value('vehiculo_modelo',exist_obj($info,'vehiculo_modelo'));
        $modelo_id = $this->principal->getGeneric('modelo', $mod, 'cat_modelo','id');
        $submodelos = $this->mcat->get('pkt_subarticulos','nombre','',['articulo_id'=>$modelo_id]);
      }
      $data['drop_submodelo_id'] = form_dropdown('citas[submodelo_id]',array_combos($submodelos,'id','nombre',TRUE),set_value('submodelo_id',exist_obj($info,'submodelo_id')),'class="form-control" id="submodelo_id"');

      $data['drop_vehiculo_version'] = form_dropdown('citas[vehiculo_version]',array_combos($this->mcat->get('cat_version','version'),'version','version',TRUE),set_value('vehiculo_version',exist_obj($info,'vehiculo_version')),'class="form-control" id="vehiculo_version"');

      $data['input_vehiculo_placas'] = form_input('citas[vehiculo_placas]',set_value('vehiculo_placas',exist_obj($info,'vehiculo_placas')),'class="form-control" rows="5" id="vehiculo_placas" ');

      $data['input_vehiculo_numero_serie'] = form_input('citas[vehiculo_numero_serie]',set_value('vehiculo_numero_serie',exist_obj($info,'vehiculo_numero_serie')),'class="form-control" rows="5" id="vehiculo_numero_serie" maxlength="17" ');

        $data['input_comentarios'] = form_textarea('citas[comentarios_servicio]',set_value('comentarios_servicio',exist_obj($info,'comentarios_servicio')),'class="form-control" id="comentarios_servicio" rows="2"');
        $id_modelo_prepiking = exist_obj($info,'id_modelo_prepiking');
        $data['drop_id_modelo_prepiking'] = form_dropdown('citas[id_modelo_prepiking]',array_combos($this->mcat->get('pkt_claves','codigo'),'id','codigo',TRUE),set_value('id_modelo_prepiking',$id_modelo_prepiking),'class="form-control busqueda" id="id_modelo_prepiking"');
        $data['drop_id_prepiking'] = form_dropdown('citas[id_prepiking]',array_combos($this->mcat->get('prepiking','id','asc',array('idmodelo'=>$id_modelo_prepiking)),'servicio','servicio',TRUE),set_value('prepiking',exist_obj($info,'prepiking')),'class="form-control busqueda" id="id_prepiking"');
        if($id==0){
          $opciones_fecha = array('' =>'-- Selecciona --', );
          $disabled = "disabled";
        }else{
          if(isset($info_horario->fecha)){
            $fecha = $info_horario->fecha;
          }else{
            $fecha = date('Y-m-d');
          }
          $id_asesor=$this->mc->getIdAsesor(exist_obj($info,'asesor'));
          $opciones_fecha = array_combos_fechas($this->mc->getFechasAsesor($id_asesor,$fecha),'fecha','fecha',TRUE);
          $disabled = '';
        }
        if($origen==2){
          $data['drop_fecha'] = form_input('citas[fecha]',date_eng2esp_1(set_value('fecha',exist_obj($info,'fecha'))),'class="form-control" id="fecha" readonly');

          $data['drop_asesor'] = form_input('citas[asesor]',set_value('asesor',exist_obj($info,'asesor')),'class="form-control" id="asesor" readonly');
        }else{
          $data['drop_fecha'] = form_dropdown('citas[fecha]',$opciones_fecha,set_value('fecha',exist_obj($info_horario,'fecha')),'class="form-control" id="fecha" '.$disabled.' '.$disabled_tablero.' ');

          $data['drop_asesor'] = form_dropdown('citas[asesor]',array_combos($this->mcat->get('operadores','nombre','',array('activo'=>1)),'nombre','nombre',TRUE),set_value('asesor',exist_obj($info,'asesor')),'class="form-control" id="asesor" '.$disabled_tablero.'  ');
        }
        if($id==0){
          $opciones_horario = array('' =>'-- Selecciona --', );
        }else{
          $id_asesor=$this->mc->getIdAsesor(exist_obj($info,'asesor'));
          //echo $this->db->last_query();die();
          $horario_id = set_value('id',exist_obj($info_horario,'id'));
          if($origen==1 || $origen==0){
             $opciones_horario = array_combos($this->mc->getHorariosAsesor_edit($id_asesor,set_value('fecha',exist_obj($info_horario,'fecha')),$horario_id),'id','hora',TRUE);
          }else{
            $opciones_horario = array();
          }
        }

        $data['drop_horario'] = form_dropdown('citas[horario]',$opciones_horario,set_value('id',exist_obj($info_horario,'id')),'class="form-control" id="horario" '.$disabled.' '.$disabled_tablero.' ');
        $data['input_datos_nombres'] = form_input('citas[datos_nombres]',set_value('datos_nombres',exist_obj($info,'datos_nombres')),'class="form-control"  id="datos_nombres" ');

        $data['input_datos_apellido_paterno'] = form_input('citas[datos_apellido_paterno]',set_value('datos_apellido_paterno',exist_obj($info,'datos_apellido_paterno')),'class="form-control"  id="datos_apellido_paterno" ');

         $data['input_datos_apellido_materno'] = form_input('citas[datos_apellido_materno]',set_value('datos_apellido_materno',exist_obj($info,'datos_apellido_materno')),'class="form-control"  id="datos_apellido_materno" ');

          $data['input_datos_telefono'] = form_input('citas[datos_telefono]',set_value('datos_telefono',exist_obj($info,'datos_telefono')),'class="form-control"  id="datos_telefono" maxlength="10" ');

          $data['drop_servicio'] = form_dropdown('citas[servicio]',array_combos($this->mcat->get('cat_servicios','servicio'),'servicio','servicio',TRUE),set_value('servicio',exist_obj($info,'servicio')),'class="form-control" id="servicio" '.$disabled_tablero.'');

          $data['input_numero_cliente'] = form_input('citas[numero_cliente]',set_value('numero_cliente',exist_obj($info,'numero_cliente')),'class="form-control"  id="numero_cliente"');

        if($id==0 || $origen == 2 || $origen == 3){
          $fecha= date('Y-m-d');
        }else{

          $fecha = $info_horario->fecha;
        }

        $data['drop_opcion'] = form_dropdown('citas[id_opcion_cita]',array_combos($this->mcat->get('cat_opcion_citas','opcion'),'id','opcion',TRUE),set_value('id_opcion_cita',exist_obj($info,'id_opcion_cita')),'class="form-control" id="id_opcion_cita"');

        $data['drop_color'] = form_dropdown('citas[id_color]',array_combos($this->mcat->get('cat_colores','color'),'id','color',TRUE),set_value('id_color',exist_obj($info,'id_color')),'class="form-control busqueda" id="id_color"');

        //echo set_value('id_tecnico',exist_obj($info,'id_tecnico'));die();
        //if($id!=0){
        $data['drop_tecnicos'] = form_dropdown('tecnico',array_combos($this->mc->getTecnicosByFechaCita($fecha),'id_tecnico','nombre',TRUE),set_value('id_tecnico',exist_obj($info,'id_tecnico')),'class="form-control" id="tecnicos" '.$disabled.' ');
        //}
      $data['btn_guardar'] = form_button('btnguardar','Guardar cita',' id="guardar" class="btn btn-success btn-block pull-right"');

      $data['input_hora_inicio'] = form_input('hora_inicio',set_value('hora_inicio',exist_obj($info,'hora_inicio')),'class="form-control check_hora" id="hora_inicio"','time');

      $data['input_hora_fin'] = form_input('hora_fin',set_value('hora_fin',exist_obj($info,'hora_fin')),'class="form-control check_hora" id="hora_fin"','time');

      $data['input_dia_completo'] = form_checkbox('dia_completo',$dia_completo,($dia_completo==1)?'checked':'','class="" id="dia_completo" ');

      //Cuando son días completos
      $data['input_fecha_inicio'] = form_input('fecha_inicio',$this->input->post('fecha'),'class="form-control" id="fecha_inicio" ','date');

      $data['input_fecha_fin'] = form_input('fecha_fin',set_value('fecha_fin',exist_obj($info,'fecha_fin')),'class="form-control" id="fecha_fin"','date');

      $data['drop_tecnicos_dias'] = form_dropdown('tecnico_dias',array_combos($this->mc->getTecnicosByFechaCita($fecha),'id_tecnico','nombre',TRUE),set_value('id_tecnico',exist_obj($info,'id_tecnico')),'class="form-control" id="tecnicos_dias" '.$disabled.' ');

      $data['input_fecha_parcial'] = form_input('fecha_parcial',set_value('fecha_inicio',exist_obj($info,'fecha_parcial')),'class="form-control" id="fecha_parcial"','date');

       $data['input_hora_inicio_extra'] = form_input('hora_inicio_extra',set_value('hora_inicio',exist_obj($info,'hora_inicio')),'class="form-control check_hora" id="hora_inicio_extra"','time');

      $data['input_hora_fin_extra'] = form_input('hora_fin_extra',set_value('hora_fin',exist_obj($info,'hora_fin')),'class="form-control check_hora" id="hora_fin_extra"','time');

      //si es por día completo saber cuando empieza
      $data['input_hora_comienzo'] = form_input('hora_comienzo',set_value('hora_inicio_dia',exist_obj($info,'hora_inicio_dia')),'class="form-control check_hora" id="hora_comienzo"','time');

      $data['drop_id_status_color'] = form_dropdown('citas[id_status_color]',array_combos($this->mcat->get('estatus','nombre'),'id','nombre',TRUE),set_value('id_status_color',exist_obj($info,'id_status_color')),'class="form-control busqueda" id="id_status_color"');

       $data['input_demo'] = form_checkbox('demo',$demo,($demo==1)?'checked':'','class="js_dudas" id="demo" ');

       $data['input_duda'] = form_checkbox('duda',$duda,($duda==1)?'checked':'','class="js_dudas" id="duda" ');
      $data['input_numero_cliente'] = form_input('numero_cliente',set_value('numero_cliente',exist_obj($info,'numero_cliente')),'class="form-control" id="numero_cliente" ');

      $data['input_vehiculo_kilometraje'] = form_input('citas[vehiculo_kilometraje]',set_value('vehiculo_kilometraje',exist_obj($info,'vehiculo_kilometraje')),'class="form-control" id="vehiculo_kilometraje" ');
      $data['id_status_cita_asesor'] = $id_status_cita_asesor;
      $data['existe_orden'] = $this->ml->existeOrden($id); //NUEVA VERSION
      $data['realizo_prepiking'] = exist_obj($info,'realizo_prepiking');
      $data['input_reparacion'] = form_checkbox('reparacion',$reparacion,($reparacion==1)?'checked':'','class="js_dudas" id="reparacion" ');
      $this->blade->render('agendar_cita',$data);
  }
  public function getfechas(){
    if($this->input->post()){
      $id_asesor=$this->mc->getIdAsesor($this->input->post("asesor"));
      $fechas=$this->mc->getFechasAsesor($id_asesor);
      echo json_encode($fechas);
    }else{
    echo 'Nada';
    }

  }

   public function getHorarios(){
    if($this->input->post()){
      $id_asesor=$this->mc->getIdAsesor($this->input->post("asesor"));
      $id_horario=$this->input->post("id_horario");
      $fecha = $this->input->post('fecha');
      //esta validación la puse por que si está editando debe ir por los horarios que no están ocupados y además el que ya tenía registrado
      if($id_horario==0){
        $horarios=$this->mc->getHorariosAsesor($id_asesor,$fecha);
      }else{
         $horarios=$this->mc->getHorariosAsesor_edit($id_asesor,$fecha,$id_horario);
      }
      echo json_encode($horarios);
    }else{
    echo 'Nada';
    }

  }
  public function ver_citas(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $data['citas'] = $this->mc->getCitas(true);
    $this->blade->render('ver_citas',$data);
  }
  public function ver_todas_citas(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $this->ver_all_citas();
  }
  public function eliminar_cita_panel(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $id_horario = $this->mc->getIdHorarioActual($this->input->post('id'));
    if($this->db->where('id_cita',$this->input->post('id'))->set('activo',0)->update('citas')){
       $this->db->where('id',$id_horario)->set('ocupado',0)->update('aux');
       $this->db->where('id_cita',$this->input->post('id'))->set('activo',0)->update('tecnicos_citas');
      echo 1;
    }else{
      echo 0;
    }
  }
  public function graficas(){

      $data= array();
      $this->blade->render('graficas',$data);
  }
  function agregar_tecnico($id=0)
    {
      if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
      {
        $this->output->set_status_header('409');
        exit();
      }else if($this->session->userdata('id_usuario')==''){
      redirect('login');
      }

      if($this->input->post()){
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
        $this->form_validation->set_rules('usuario', 'usuario', 'trim|required');
        $id = $this->input->post('id');
        if($id==0){
          $this->form_validation->set_rules('password', 'contraseña', 'trim|required|matches[repassword]');
          $this->form_validation->set_rules('repassword', 'confirmar contraseña', 'trim|required');
        }else{
            if($this->input->post('password')!='' || $this->input->post('repassword')!=''){
                $this->form_validation->set_rules('password', 'contraseña', 'trim|required|matches[repassword]');
                $this->form_validation->set_rules('repassword', 'confirmar contraseña', 'trim|required');
            }
        }
        $this->form_validation->set_rules('idStars', 'idStar', 'trim|required');
       if ($this->form_validation->run()){
          $this->mc->guardarTecnico();
          }else{
             $errors = array(
                  'nombre' => form_error('nombre'),
                  'usuario' => form_error('usuario'),
                  'password' =>form_error('password'),
                  'repassword' =>form_error('repassword'),
                  'idStars' =>form_error('idStars'),
             );
            echo json_encode($errors); exit();
          }
      }
      if($id==0){
        $info=new Stdclass();
        $contraseña = '';

      }else{
        $info= $this->mc->getTecnicoId($id);
        $info=$info[0];
        $contraseña = $info->password;
      }
      $data['input_id'] = $id;
      $data['pass_actual'] = $contraseña;
      $data['input_nombre'] = form_input('nombre',set_value('nombre',exist_obj($info,'nombre')),'class="form-control" rows="5" id="nombre" ');
      $data['input_usuario'] = form_input('usuario',set_value('usuario',exist_obj($info,'nombreUsuario')),'class="form-control" rows="5" id="usuario" ');
      $data['input_password'] = form_password('password',"",'class="form-control" rows="5" id="password" ');
      $data['input_repassword'] = form_password('repassword',"",'class="form-control" id="repassword" ');
      $data['input_idStars'] = form_input('idStars',set_value('idStars',exist_obj($info,'idStars')),'class="form-control" id="idStars" ');
      $this->blade->render('nuevo_tecnico',$data);
  }
  public function lista_tecnicos()
  {
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $datos['citas'] = $this->mc->getTecnicos();
    $this->blade->set_data($datos)->render('tecnicos');
  }
   public function tabla_tecnicos(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
     $datos['tecnicos'] = $this->mc->getTecnicos();
     $this->blade->set_data($datos)->render('tbl_tecnicos');
  }
  public function horarios_tecnico($id=''){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $data['fecha'] = date('Y-m-d');
    $horarios = $this->mc->getHorariosTecnicos($id,$data['fecha']);
    $horarios_ordenadas= array();
    foreach ($horarios as $c => $value) {
      $horarios_ordenadas[$value->fecha][] = $value;
    }
    $data['id_tecnico'] = $id;
    $data['nombre_tecnico'] = $this->mc->getNameTecnico($id);
    $data['horarios'] = $horarios_ordenadas;
    $this->blade->set_data($data)->render('horarios_tecnicos');
  }
  public function tabla_horarios_tecnicos(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $horarios = $this->mc->getHorariosTecnicos($this->input->post('id'),date2sql($this->input->post('fecha')));
    $horarios_ordenadas= array();
    foreach ($horarios as $c => $value) {
      $horarios_ordenadas[$value->fecha][] = $value;
    }
    $data['horarios'] = $horarios_ordenadas;
      $this->blade->set_data($data)->render('tbl_horarios_tecnico');
  }
  public function agregar_horario_tecnico($id=0)
    {
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
      $this->form_validation->set_rules('fecha', 'fecha', 'trim|required');
      $this->form_validation->set_rules('hora_inicio', 'hora inicio', 'trim|required');
      $this->form_validation->set_rules('hora_fin', 'hora fin', 'trim|required');
      $this->form_validation->set_rules('hora_inicio_comida', 'hora inicio comida', 'trim');
      $this->form_validation->set_rules('hora_fin_comida', 'hora fin comida', 'trim');
      if ($this->form_validation->run()){
          $this->mc->guardarHorario_tecnico();
          }else{
            $errors = array(
                'fecha' => form_error('fecha'),
                'hora_inicio' => form_error('hora_inicio'),
                'hora_fin' => form_error('hora_fin'),
                'hora_inicio_comida' => form_error('hora_inicio_comida'),
                'hora_fin_comida' => form_error('hora_fin_comida'),
            );
            echo json_encode($errors); exit();
      }
   }
   public function getHorariosAsesor(){
    if($this->input->post()){
      $id_tecnico=$this->input->post("id_tecnico");
      $fecha = $this->input->post('fecha');

      $horarios=$this->mc->getHorariosTecnicos($id_tecnico,date2sql($fecha));
      echo json_encode($horarios);
    }else{
    echo 'Nada';
    }

  }
   public function getTecnicosByFecha(){
    if($this->input->post()){

      $fecha = $this->input->post('fecha');
      $tecnicos=$this->mc->getTecnicosByFechaCita(date_esp2eng($fecha));
      echo json_encode($tecnicos);
    }else{
    echo 'Nada';
    }

  }
  public function asignar_tecnico(){
    if($this->db->where('id_cita',$this->input->post('id'))->set('id_tecnico',$this->input->post('id_tecnico'))->update('citas')){
      echo 1; exit();
    }else{
      echo 0 ; exit();
    }
  }
  public function modal_asignar_tecnico(){
      $id_cita = $this->input->get('id');
      $id_tecnico = $this->input->get('id_tecnico');

        if($this->input->post()){
        if($this->input->post('iscarriover')!=null){
            if($this->input->post('dia_completo')!=null){
               $this->form_validation->set_rules('fecha_fin', 'fecha fin', 'trim|required');
            }else{
              $this->form_validation->set_rules('hora_inicio', 'hora inicio', 'trim|required');
              $this->form_validation->set_rules('hora_fin', 'hora fin', 'trim|required');
            }
        }else{
          $this->form_validation->set_rules('hora_inicio', 'hora inicio', 'trim|required');
          $this->form_validation->set_rules('hora_fin', 'hora fin', 'trim|required');
        }
       if ($this->form_validation->run()){

          if($this->input->post('iscarriover')!=null){
            $carryover = $this->ml->isCarryover($this->input->post('operacion'));
            $carryover_reservacion = (isset($_POST['carryover_reservacion'])) ? 1 : 0;
            //Validar en la tabla de cat_status_citas_tecnicos si el estatus es permitido para carryover
            $estatus_permitido_carryover = $this->ml->esEstatusPermitidoCarryOver($this->input->post('operacion'),$carryover_reservacion);
            if(!$estatus_permitido_carryover){
              echo -4;exit();
            }
            $this->mc->asignarHoraTecnicoCarriOver($carryover,$carryover_reservacion);
          }else{
            $this->mc->asignarHoraTecnico();
          }

          }else{
             $errors = array(
                  'hora_inicio' => form_error('hora_inicio'),
                  'hora_fin' => form_error('hora_fin'),
                  'fecha_fin' => form_error('fecha_fin'),
             );
            echo json_encode($errors); exit();
          }
      }

      if($id_tecnico==0){
        $info=new Stdclass();
        $id = 0;
        $dia_completo = 0;
        $info_dia_completo = new Stdclass();
        $info_dia_extra = new Stdclass();
      }else{
        $info= $this->mc->getCitaAsignadaId($id_tecnico,$id_cita,'');
        $info=$info[0];
        $id = $info->id;
        //funcion para saber si en la cita hay un registro con días completo
        $info_dia_completo = $this->mc->getCitaAsignadaId($id_tecnico,$id_cita,1);

        if(count($info_dia_completo)>0){
           $info_dia_completo = $info_dia_completo[0];
        }else{
            $info_dia_completo = new Stdclass();
        }

        $info_dia_extra = $this->mc->getCitaAsignadaId2($id_tecnico,$id_cita,0);
        if(count($info_dia_extra)>0){
          $info_dia_extra = $info_dia_extra[0];
        }else{
           $info_dia_extra = new Stdclass();
        }


        $dia_completo = $this->mc->getDiaCompleto($id_cita);
      }


      $data['input_id_detalle_horarios'] = $id;
      $data['input_id_tecnico'] = $id_tecnico;
      $data['id_tecnico_seleccionado'] = $this->input->get('id_tecnico_seleccionado');

      $data['input_hora_inicio'] = form_input('hora_inicio',substr(set_value('hora_inicio',exist_obj($info,'hora_inicio_dia')),0,5),'class="form-control check_hora" id="hora_inicio"','time');

      $data['input_hora_fin'] = form_input('hora_fin',substr(set_value('hora_fin',exist_obj($info,'hora_fin')),0,5),'class="form-control check_hora" id="hora_fin"','time');

      //Aveces pueden reasignar una cita a un técnico por eso pongo el campo para que lo reasigne en otra fecha
       $data['input_fecha_reasignar'] = form_input('fecha_reasignar',date2sql($this->mc->getFechaTecnicosCita($id)),'class="form-control" id="fecha_reasignar"','date');

      //extra
       $data['input_dia_completo'] = form_checkbox('dia_completo',$dia_completo,($dia_completo==1)?true:false,'class="" id="dia_completo" ');

      //Cuando son días completos
      $data['input_fecha_inicio'] = form_input('fecha_inicio',set_value('fecha',exist_obj($info_dia_completo,'fecha')),'class="form-control" id="fecha_inicio"','date');

      $data['input_fecha_fin'] = form_input('fecha_fin',set_value('fecha_fin',exist_obj($info_dia_completo,'fecha_fin')),'class="form-control" id="fecha_fin"','date');

      if($dia_completo==1){
        $valor_fecha_parcial = set_value('fecha',exist_obj($info_dia_extra,'fecha'));
        $valor_hora_inicio = set_value('hora_inicio',exist_obj($info_dia_extra,'hora_inicio'));
        $valor_hora_fin = set_value('hora_fin',exist_obj($info_dia_extra,'hora_fin'));
      }else{
        $valor_fecha_parcial ='';
        $valor_hora_inicio ='';
        $valor_hora_fin = '';
      }

      $data['input_fecha_parcial'] = form_input('fecha_parcial',$valor_fecha_parcial,'class="form-control" id="fecha_parcial"','date');

       $data['input_hora_inicio_extra'] = form_input('hora_inicio_extra',$valor_hora_inicio,'class="form-control check_hora" id="hora_inicio_extra"','time');

      $data['input_hora_fin_extra'] = form_input('hora_fin_extra',$valor_hora_fin,'class="form-control check_hora" id="hora_fin_extra"','time');

        //si es por día completo saber cuando empieza
      $data['input_hora_comienzo'] = form_input('hora_comienzo',set_value('hora_inicio_dia',exist_obj($info_dia_completo,'hora_inicio_dia')),'class="form-control check_hora" id="hora_comienzo"','time');


      $data['dia_completo'] = $dia_completo;

      $this->blade->render('modal_asignar_tecnico',$data);
  }
  public function modal_ver_citas_asignadas(){
    $data['id_tecnico'] = $this->input->post('id_tecnico');
    if($this->input->post('fecha')!=null){
      $fecha = $this->input->post('fecha');
    }else{
      $fecha = date('Y-m-d');
    }
    $data['input_fecha'] = $fecha;
    $data['citas_asignadas'] = $this->mc->getCitasAsignadasTecnico($data['id_tecnico'],$data['input_fecha']);
    $data['hora_comida'] = $this->mc->getHoraComida($data['id_tecnico'],$data['input_fecha']);
    $this->blade->render('modal_citas_asignadas',$data);
  }
  //Obtiene por cuanto se va dividir para hacer el tablero de asesores y técnicos
  public function getValor($tiempo=''){
    switch ($tiempo) {
       case 30:
         return 27;
         break;
      case 20:
         return 34;
         break;
      case 15:
         return 53;
         break;
      case 79:
         return 10;
         break;

       default:
        return 30;
         break;
     }
  }
  public function tablero_asesores(){
    $datos['fecha'] = date('Y-m-d');
    $datos['tiempo_aumento'] = $this->mc->getParametros('tiempo_cita',$datos['fecha']);
    $datos['valor'] = $this->getValor($datos['tiempo_aumento']);

    $datos['tabla1'] = $this->mc->tabla_asesores($datos['fecha'],$datos['valor'],$datos['tiempo_aumento'],0); // el último argumento es para saber si hace el cociente en base a 0 o a 1
    $datos['tabla2'] = $this->mc->tabla_asesores($datos['fecha'],$datos['valor'],$datos['tiempo_aumento'],1); // el último argumento es para saber si hace el cociente en base a 0 o a 1
    $this->blade->render('tablero_asesores',$datos);
  }
  public function tabla_horarios_asesores(){
    $datos['fecha'] = $this->input->post('fecha');
    $datos['tiempo_aumento'] = $this->mc->getParametros('tiempo_cita',$datos['fecha']);
    $datos['valor'] = $this->getValor($datos['tiempo_aumento']);
    $datos['tabla1'] = $this->mc->tabla_asesores($datos['fecha'],$datos['valor'],$datos['tiempo_aumento'],0); // el último argumento es para saber si hace el cociente en base a 0 o a 1
    $datos['tabla2'] = $this->mc->tabla_asesores($datos['fecha'],$datos['valor'],$datos['tiempo_aumento'],1); // el último argumento es para saber si hace el cociente en base a 0 o a 1
    $this->blade->render('tabla_horarios_asesores',$datos);
  }
  public function tablero_tecnicos(){
    //$this->session->set_userdata('login',0);
     if($this->session->userdata('login')){
      $datos['fecha'] = date('Y-m-d');
      $datos['tecnicos'] = $this->mc->getTecnicosHorarios(date('Y-m-d'));
      $datos['origen'] = 'tecnicos';
      $datos['status'] = $this->db->where('activo',1)->get('estatus')->result();
      $datos['tiempo_aumento'] = $this->mc->getParametros('tiempo_cita',$datos['fecha']);
      $datos['valor'] = $this->getValor($datos['tiempo_aumento']);
      $this->blade->render('tablero_tecnicos',$datos);
    }else{
      $this->blade->render('login',array('origen'=>'tecnicos'));
    }

  }
   public function tabla_horarios_tecnicos_busqueda(){
    $datos['fecha'] = $this->input->post('fecha');
    $datos['tecnicos'] = $this->mc->getTecnicosHorarios($datos['fecha']);
    $datos['tiempo_aumento'] = $this->mc->getParametros('tiempo_cita',$datos['fecha']);
    $datos['valor'] = $this->getValor($datos['tiempo_aumento']);
    $datos['origen'] = $_POST['origen'];
    $this->blade->render('tabla_horarios_tecnicos',$datos);
  }
  public function refacciones(){
    if($this->session->userdata('login_refacciones')){
      $datos['fecha'] = date('Y-m-d');
      $datos['tecnicos'] = $this->mc->getTecnicosHorarios($datos['fecha']);
      $datos['origen'] = 'refacciones';
      $datos['status'] = $this->db->get('estatus')->result();
      $datos['tiempo_aumento'] = $this->mc->getParametros('tiempo_cita',$datos['fecha']);
      $datos['valor'] = $this->getValor($datos['tiempo_aumento']);
      $this->blade->render('tablero_tecnicos',$datos);
    }else{
      $this->blade->render('login',array('origen'=>'refacciones'));
    }

  }
  public function validar_login(){
    if($this->input->post()){
      $usuario = $this->input->post('usuario');
      $password = $this->input->post('password');
      $origen = $this->input->post('origen');
      $datos['origen'] = $origen;
      if($usuario==CONST_USER_TABLERO && $password ==CONST_PASS_TABLERO){
        if($origen=='tecnicos'){
           $this->session->set_userdata('login',1);
            redirect('citas/tablero_tecnicos');
        }else{
           $this->session->set_userdata('login_refacciones',1);
            redirect('citas/refacciones');
        }
      }else{
        $this->session->set_userdata('login',0);
        $datos['error'] = "Usuario o contraseña incorrectos";
        $this->blade->render('login',$datos);

      }

    }

  }
   public function validar_login_asesores(){
    if($this->input->post()){
      $usuario = $this->input->post('usuario');
      $password = $this->input->post('password');
      if($usuario=="asesor" && $password =="asesor2018"){
        $this->session->set_userdata('login_asesores',1);
        redirect('citas/tablero_asesores');
      }else{
        $this->session->set_userdata('login_asesores',0);
        $datos['error'] = "Usuario o contraseña incorrectos";
        $this->blade->render('login_asesores',$datos);

      }
    }
  }
  public function cerrar_sesion(){
     $this->session->sess_destroy();
     $this->session->set_userdata('login',0);
     redirect('citas/tablero_tecnicos');
  }
   public function cerrar_sesion_asesor(){
     $this->session->sess_destroy();
     $this->session->set_userdata('login_asesores',0);
     redirect('citas/tablero_asesores');
  }
  public function modelos()
  {
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $datos['modelos'] = $this->mcat->get('cat_modelo','modelo');
    $this->blade->set_data($datos)->render('modelos');
  }
  function agregar_modelo($id=0)
    {
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }

      if($this->input->post()){
        $this->form_validation->set_rules('modelo', 'modelo', 'trim|required');
        $this->form_validation->set_rules('tiempo_lavado', 'tiempo lavado', 'trim|required|numeric');
       if ($this->form_validation->run()){
          $this->mc->guardarModelo();
          }else{
             $errors = array(
                  'modelo' => form_error('modelo'),
                  'tiempo_lavado' => form_error('tiempo_lavado'),
             );
            echo json_encode($errors); exit();
          }
      }

      if($id==0){
        $info=new Stdclass();
      }else{
        $info= $this->mc->getModeloId($id);
        $info=$info[0];
      }
      $data['input_id'] = $id;
      $data['input_modelo'] = form_input('modelo',set_value('modelo',exist_obj($info,'modelo')),'class="form-control" rows="5" id="modelo" ');
      $data['input_tiempo_lavado'] = form_input('tiempo_lavado',set_value('tiempo_lavado',exist_obj($info,'tiempo_lavado')),'class="form-control" rows="5" id="tiempo_lavado" ');
      $this->blade->render('nuevo_modelo',$data);
  }
  public function tabla_modelos(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $datos['modelos'] = $this->mcat->get('cat_modelo','modelo');
   $this->blade->set_data($datos)->render('tbl_modelos');
  }
  public function codigos_gen() {
    $datos['codigos'] = $this->mcat->get('codigos_gen','codigo');
    $this->blade->set_data($datos)->render('codigos_gen');
  }
  function agregar_codigo($id=0){
      if($this->input->post()){
        $this->form_validation->set_rules('codigo', 'código', 'trim|required');
        $this->form_validation->set_rules('descripcion', 'descripción', 'trim|required');
       if ($this->form_validation->run()){
          $this->mc->guardarCodigoGen();
          }else{
             $errors = array(
                  'codigo' => form_error('codigo'),
                  'descripcion' => form_error('descripcion'),
             );
            echo json_encode($errors); exit();
          }
      }
      if($id==0){
        $info=new Stdclass();
      }else{
        $info= $this->mc->getCodigoId($id);
        $info=$info[0];
      }


      $data['input_id'] = $id;
      $data['input_codigo'] = form_input('codigo',set_value('codigo',exist_obj($info,'codigo')),'class="form-control" rows="5" id="codigo" ');
       $data['input_descripcion'] = form_input('descripcion',set_value('descripcion',exist_obj($info,'descripcion')),'class="form-control" rows="5" id="descripcion" ');
      $this->blade->render('nuevo_codigo_gen',$data);
  }
  public function tabla_codigos_gen(){
   $datos['codigos'] = $this->mcat->get('codigos_gen','codigo');
   $this->blade->set_data($datos)->render('tbl_codigos_gen');
  }
  public function anios(){
    for ($i=2018; $i >= 1950 ; $i--) {
      echo $i;
       $this->db->set('anio',$i)->insert('cat_anios');
    }
  }
  //elimina la cita
  public function cambiar_status_cita(){
    $id_cita = $_POST['id_cita'];
    //Datos de la cita de los técnicos para validar que no exista otra cita al cambiar el estatus
    $tecnicos_citas = $this->db->where('id_cita',$id_cita)->limit(1)->get('tecnicos_citas')->result();
    if(count($tecnicos_citas)>0){
      $validacion = $this->mc->validarDiaExtra($tecnicos_citas[0]->hora_inicio_dia,$tecnicos_citas[0]->hora_fin,$tecnicos_citas[0]->fecha,$tecnicos_citas[0]->id_tecnico,$tecnicos_citas[0]->id_cita);
      //Regresa false si ya quieren cambiar el estatus del asesor y cruza con una cita
      
      if(!$validacion && $_POST['status']!=5){
        echo -1; exit();
      }

    }
    //validar la situacion
    if ($_POST['status']==5) {
      $situacion_actual = $this->ml->getSituacionOrden($id_cita);
      if($situacion_actual!=6){
        echo -2;exit();
      }
      //Firma cliente
      if(!$this->mc->firma_cliente($id_cita)){
        echo -3;exit();
      }
    }
    
    if($this->db->where('id',$this->input->post('id'))->set('id_status_cita',$this->input->post('status'))->update('aux')){

      $id_cita = $this->mc->getIdCitaByIdHorario($this->input->post('id'));

      //FALTA LA VALIDACIÓN
      if($this->input->post('status')==4){
        $this->db->where('id_cita',$id_cita)->set('historial',0)->update('tecnicos_citas');
        $this->db->where('id_cita',$id_cita)->set('historial',0)->update('citas');
      }

      if($this->input->post('status')==3 || $this->input->post('status')==2){
        $this->db->where('id_cita',$id_cita)->set('id_status',1)->update('citas');
        $this->db->where('id_cita',$id_cita)->set('historial',0)->update('tecnicos_citas');
        $this->db->where('id_cita',$id_cita)->set('historial',0)->update('citas');
        //NUEVA VERSIÓN
        //Actualizar ordenes por cita 
        $this->ml->updateStatusOperaciones($id_cita,1);
      }

      //GUARDAR TRANSICIONES DE LOS ESTATUS DE ASESOR

      //Verificar si no existe la transaccion traerme la de creación de la cita 3,4,5,6
      if($this->mc->existTransition($id_cita,1)){
        $inicio = $this->mc->getLastTransition($id_cita);
        $mostrar_linea = 1;
      }else{
        $mostrar_linea = 0;
        $inicio = $this->mc->getFechaCreacion($id_cita);
      }
      $id_operador = $this->mc->getIdOperador($this->input->post('id'));
      $datos_transiciones = array('status_anterior' => $this->mc->getLastStatusTransiciones($id_cita),
                                  'status_nuevo'=> $this->mc->getStatusAsesor($this->input->post('status')),
                                  'fecha_creacion'=>date('Y-m-d H:i:s'),
                                  'tipo'=>1, //asesor,
                                  'inicio'=>$inicio,
                                  'fin'=>date('Y-m-d H:i:s'),
                                  'usuario'=>$this->mc->name_operador($id_operador),
                                  'id_cita'=>$id_cita,
                                  'mostrar_linea'=>$mostrar_linea,
                                  'general'=>1,
                                  'minutos_transcurridos'=>dateDiffMinutes($inicio , date('Y-m-d H:i:s'))
      );
      $this->db->insert('transiciones_estatus',$datos_transiciones);
      //Si la unidad es entregada
      if($this->input->post('status')==5){
        $this->db->where('id_cita',$id_cita)->set('unidad_entregada',1)->update('citas');
        $this->db->where('id_cita',$id_cita)->set('orden_cerrada',1)->update('RO_Data_Sample');

        //Si el estatus es lavado terminado poner la hora
         $this->db->where('id_cita',$id_cita)->set('fecha_termino_lavado',date('Y-m-d H:i:s'))->set('fecha_entrega_unidad',date('Y-m-d H:i:s'))->update('citas');

        //CUANDO LA UNIDAD ES ENTREGADA TAMBIÉN GUARDAR LA SITUACION DE ORDEN CERRADA / ENTREGADA Y EL HISTORIAL
        $idorden = $this->mc->getIdOrdenByCita($id_cita); 
        $id_situacion_anterior = $this->mc->getSituacionIntelisis($this->mc->getSituacionOrden($idorden));

        $this->db->where('id',$idorden)->set('id_situacion_intelisis',5)->update('ordenservicio');
          $datos_historial = array('situacion_anterior'=>$id_situacion_anterior,
            'situacion_nueva'=>$this->mc->getSituacionIntelisis(5),
            'usuario'=>$this->mc->getNameUser($this->session->userdata('id_usuario')),
            'created_at'=>date('Y-m-d H:i:s'),
            'idorden'=>$idorden
            );
        $this->db->insert('historial_cambio_situacion_intelisis',$datos_historial);

        //Actualizar el estatus de intelisis
        $this->db->where('id',$idorden)->set('id_status_intelisis',3)->update('ordenservicio');
      }
      //FIN DE TRANSICIONES
      // if($this->input->post('status')==3){
      //    $this->db->where('id_cita',$id_cita)->set('id_status',1)->update('citas');
      // }
      echo 1;
    }else{
      echo 0;
    }
  }
  public function login_tecnicos(){
       if($this->input->post()){
        $this->form_validation->set_rules('usuario', 'usuario', 'trim|required');
        $this->form_validation->set_rules('password', 'contraseña', 'trim|required');

       if ($this->form_validation->run()){
          $this->mc->validarLogin();
          }else{
             $errors = array(
                  'usuario' => form_error('usuario'),
                  'password' => form_error('password'),
             );
            echo json_encode($errors); exit();
          }
      }
     $data['input_usuario'] = form_input('usuario',$this->input->get('usuario_tecnico'),'class="form-control" rows="5" id="usuario" ');

     $data['input_password'] = form_password('password',"",'class="form-control" rows="5" id="password" ');
     $data['comentario_cita'] = $this->mc->getDatoCita($this->input->get('id_cita'),'comentarios_servicio');
     $data['vehiculo_placas'] = $this->mc->getDatoCita($this->input->get('id_cita'),'vehiculo_placas');
     $data['vehiculo_anio'] = $this->mc->getDatoCita($this->input->get('id_cita'),'vehiculo_anio');
     $data['vehiculo_numero_serie'] = $this->mc->getDatoCita($this->input->get('id_cita'),'vehiculo_numero_serie');
     $data['vehiculo_modelo'] = $this->mc->getDatoCita($this->input->get('id_cita'),'vehiculo_modelo');
     $data['asesor'] = $this->mc->getDatoCita($this->input->get('id_cita'),'asesor');
     $data['color'] = $this->mc->getColorCitaAuto($this->input->get('id_cita'));
     $data['id_cita'] =$this->input->get('id_cita');

     $data['origen'] = $this->input->get('origen');
     $id_prepiking = $this->mc->getDatoCita($this->input->get('id_cita'),'id_prepiking');

     $id_modelo_prepiking = $this->mc->getDatoCita($this->input->get('id_cita'),'id_modelo_prepiking');

     $data['ordenservicio'] = $this->mc->getDatosOrdenServicioByIdCita($data['id_cita']);
     $data['realizo_prepiking'] = $this->mc->getDatoCita($this->input->get('id_cita'),'realizo_prepiking');
     $data['prepiking'] = $this->mc->getPrepikin($id_prepiking);
     //PAQUETES
     $fecha_creacion = $this->mc->getDatoCita($this->input->get('id_cita'),'fecha_creacion');
     if($fecha_creacion < CONST_FECHA_CITA_PAQUETES){
        //NUEVA VERSIÓN
        $data['articulos_prepiking'] = $this->mc->getArticulosPrepiking($data['id_cita'],$this->input->get('id_operacion'));
     }else{
        $id_modelo = $this->principal->getGeneric('modelo',$data['vehiculo_modelo'],'cat_modelo','id');
        $data['articulos_prepiking'] = $this->mc->getArticulosPaquetes($id_modelo,$id_modelo_prepiking);
     }
     $data['extra_prepiking'] = $this->db->where('id_cita',$this->input->get('id_cita'))->get('prepiking_extra_cita')->result();
     $data['hora_inicio_trabajo'] = $this->mc->getFechaTransicion('trabajando',$this->input->get('id_cita'),$this->input->get('id_operacion'));
     
     $data['hora_fin_trabajo'] = $this->mc->getFechaTransicion('terminado',$this->input->get('id_cita'));
     $data['hora_fin_trabajo_operacion'] = $this->mc->getFechaTransicion('Operación terminada',$this->input->get('id_cita'),$this->input->get('id_operacion'));
     $id_status_cita = $this->mc->getStatusCitaPrincipal($data['id_cita']); //2y6

     //Si ya se termino bloquear el cambio de estatus para refacciones
    if($id_status_cita==3 || $id_status_cita==5 || $id_status_cita==8 || $id_status_cita==9|| $id_status_cita==10|| $id_status_cita==11){
      $disabled= "disabled";
    }else{
      $disabled = '';
    }
     $array_estatus_refacciones = $this->db->where_in('id',array(7,13,14))->get('cat_status_citas_tecnicos')->result();
     $data['drop_status'] = form_dropdown('id_status',array_combos($array_estatus_refacciones,'id','status',TRUE),$id_status_cita,'class="form-control" id="id_status" '.$disabled.'  ');
     $data['usuario_tecnico'] = $this->input->get('usuario_tecnico');
     $data['idcarryover'] = $this->input->get('idcarryover');
     $data['ubicacion_unidad'] = $this->mc->getUbicacionUnidad($data['id_cita']);//
    $id_estatus_asesor = $this->mc->getStatusCita($this->mc->getDatoCita($this->input->get('id_cita'),'id_horario'));//CUANDO EL id_status_cita =1 ES 
    if($id_estatus_asesor==1){
        $data['hizo_checkin'] = 0;
    }else{
      $data['hizo_checkin'] = 1;
    }
    $data['trabajando'] = $this->mc->unidadTrabajando($data['id_cita']);
    $data['status_actual'] = $this->mc->getStatusTecnicoTransiciones($this->mc->getStatusTecnico($data['id_cita']));
    //NUEVA VERSIÓN
    $data['operacion'] = $this->ml->getOperacionById($this->input->get('id_operacion'),'descripcion');    
    $data['id_operacion'] = $this->input->get('id_operacion');


    if($this->input->get('id_operacion')!=''){
      $q = $this->db->where('id',$this->input->get('idcarryover'))->get('tecnicos_citas_historial');
      if($q->num_rows()==1){
        $data['es_carryover'] = $q->row()->carryover;
      }else{
         $data['es_carryover'] = 0;
      }
    }else{
      $data['es_carryover'] = 0;
    }
    $data['carryover_reservacion'] = $this->ml->getCarryOverReservacion($data['idcarryover']); // 1 ES QUE SE HIZO EL CARRYOVER EN PLANEADO DESDE LA RESERVACIÓN
    
    $this->blade->render('login_tecnico',$data);
  }
  public function modal_cita_tecnico(){
      $id_cita = $this->input->get('id_cita');
      $fecha = date2sql($this->input->get('fecha'));
      //NUEVA VERSION
      $idorden = $this->mc->getIdOrdenByCita($id_cita);
      $id_status_cita = $this->ml->getStatusOrden($idorden,$this->input->get('id_operacion')); //2y6
      
      $id_horario_actual = $this->mc->getIdHorarioActual($id_cita);
      //Obtener el estatus que trae la cita del asesor
      $id_estatus_asesor = $this->mc->getStatusCita($id_horario_actual);//CUANDO EL id_status_cita =1 ES QUE EL ASESOR NO HA HECHO CHECK-IN
      $origen_cita = $this->mc->getOrigenCita($id_cita); //2 es del tablero
      $id_status_anterior = $this->mc->getStatusAnterior($id_cita);

      $disabled= '';
      $data['terminar_dias_previos'] = 0;
      if($fecha != date('Y-m-d')){
        $data['dia_actual'] = false;
        $data['terminar_dias_previos'] = $this->ml->TerminarEstatusDiasPrevios($id_status_cita);
        if($data['terminar_dias_previos']==0){
          $disabled= 'disabled';
        }

        //Si está en detenida por autorización ver si tiene cotización rechazada
        if($id_status_cita==15 && $this->ml->cotizacionRechazada($id_cita)){
          //Si el estatus es detenida por autorización y la cotización se rechazó por completo
          $disabled= '';
        }else{
          $disabled= 'disabled';
        }
        
      }else{
        $data['dia_actual'] = true;
      }
      //NUEVA VERSION
      if($id_status_cita==3 || $id_status_cita==23 || $id_status_cita==8 || $id_status_cita==9 || $id_status_cita==10 || $id_status_cita==11){
        $disabled = 'disabled';
      }
      if($id_estatus_asesor==1 && $origen_cita !=2){
        $data['hizo_checkin'] = 0;
      }else{
        $data['hizo_checkin'] = 1;
      }
      
      //Perfil 4 es comentario para lavado
       $data['islavado'] = 0;
      if($this->session->userdata('tipo_perfil')==4){
        $data['islavado'] = 1;
      }
      $data['id_cita_save'] = $id_cita;//
      $data['id_status_actual'] = $id_status_cita;//
      $data['id_estatus_asesor'] = $id_estatus_asesor;//
      $data['ubicacion_unidad'] = $this->mc->getUbicacionUnidad($id_cita);//
      $data['existe_orden'] = $this->ml->existeOrden($id_cita); //NUEVA VERSION
      $data['se_trabajo'] = $this->ml->OperacionTrabajando($this->input->get('id_operacion'));
      
      
      //Si no llegó solo lo pueden cambiar a que no llegó
      if($id_estatus_asesor==4){
        $lista_status = $this->mcat->get('cat_status_citas_tecnicos','status','',array('activo'=>1,'id'=>5));
        $data['es_estatus_detenido'] = 0;
      }else{
        //NUEVA VERSION
        //Si es una sola operación salga el estatus de terminado pero no el de terminar operación
        //Si hay más de una operación validar que todas estén terminadas para que aparezca el estatus de terminado
        $cantidad_operaciones = $this->ml->countOperaciones($idorden);
        if($cantidad_operaciones==1){
          $this->db->where_not_in('id',23); //No salga operación terminada
        }else{
          $operaciones_terminadas = $this->ml->validarUltimaOperacion($idorden); //Función para saber si la operación actual puede detonar el terminar
          if(!$operaciones_terminadas){
            $this->db->where_not_in('id',3); //No salga para terminar
          }else{
            $this->db->where_not_in('id',23); //No salga para terminar operación, si no terminar todo
          }
        }
        $lista_status = $this->mcat->get('cat_status_citas_tecnicos','status','',array('activo'=>1));
        //Saber si el estatus es detenido o no


        $es_estatus_detenido = $this->ml->getStatusId($id_status_cita);
        if($es_estatus_detenido!=null){
          $data['es_estatus_detenido'] = $es_estatus_detenido->estatus_detenido;
        }else{
          $data['es_estatus_detenido'] =0;
        }
        
      }
      //Nombre del estatus
      $nombre_estatus = $this->mc->getStatusTecnicoTransiciones($id_status_cita);
      
      $data['motivo_detencion'] = $this->ml->getMotivoDetencion($nombre_estatus,$this->input->get('id_operacion'));
      
      $data['drop_status'] = form_dropdown('id_status',array_combos($lista_status,'id','status',TRUE),$id_status_cita,'class="form-control" id="id_status" '.$disabled.' ');

      $data['input_comentarios'] = form_textarea('comentarios',"",'class="form-control" rows="5" id="comentarios" '.$disabled.' ');

       $opciones_comentarios = array('' =>'-- Selecciona --','Por autorizar'=>'Por autorizar','Por refacciones'=>'Por refacciones','Por diagnóstico'=>'Por diagnóstico' );
      $data['drop_tipo_comentario'] = form_dropdown('tipo',$opciones_comentarios,'','class="form-control" id="tipo" '.$disabled.' ');


      $this->blade->render('cambiar_status_cita_tecnico',$data);
  }
  public function info(){
    echo phpinfo();
  }
  public function cambiar_status_cita_tecnico(){
      $multipunto_generada_firma_tecnico = $this->mc->hm_firmaTec($this->input->post('id_cita_save'));
      if($this->input->post()){
         
          $idorden = $this->mc->getIdOrdenByCita($this->input->post('id_cita_save')); //Id orden por cita
          $this->form_validation->set_rules('id_status', 'estatus', 'trim|required');
          if($this->input->post('estatus_detenido')==1){
            $this->form_validation->set_rules('motivo_detencion', 'motivo detención', 'trim|required');
          }

        if ($this->form_validation->run()){
          $idhorario = $this->mc->getIdHorarioActual($this->input->post('id_cita_save'));
          $id_status_cita = $this->mc->getStatusCita($idhorario);
          $origen_cita = $this->mc->getOrigenCita($this->input->post('id_cita_save'));
          $id_status_anterior = $this->mc->getStatusTecnico($this->input->post('id_cita_save')); 
          if($this->input->post('id_status')==5 && $id_status_cita !=4){
            //id_status_cita es igual a 4 cuando el cliente no llego
            //$this->input->post('id_status') es igual a 5 cuando el cliente no llegó
            if($origen_cita!=2){ //cuando es 2 es que no ocupa decir el asesor que no llegó.
              echo -2;exit();
            }
          }
          //No puede poner otro trabajando si ya existe uno
          $idtecnico = $this->mc->getTecnicoIdByUser($this->input->post('usuario_ingreso'),$this->input->post('password_ingreso'));
              if($this->input->post('id_status')==2){
                //NUEVA VERSION
              if(!$this->ml->ValidarUnTrabajoTecnicoOperacion($idtecnico,$this->input->post('id_operacion'))){
                echo -4;exit();
              }
              
            }
            if($this->input->post('id_status')==2 || $this->input->post('id_status')==4){
              //Validar si va pasar a trabajando exista un diagnóstico
              if(!$this->ml->existeDiagnostico($this->input->post('id_cita_save'))){
                echo -7;exit();
              }
            }
          //Validar que haya pasado por lo menos por trabajando si la quiere terminar
          if(($this->input->post('id_status')==3||$this->input->post('id_status')==23) && !$this->ml->unidadTrabajando($this->input->post('id_cita_save'),$this->input->post('id_operacion'))){
            echo -5;exit();
          }

          //Si no hay multipunto no deja
          if($this->input->post('id_status')==3){
            if($multipunto_generada_firma_tecnico==0){
              echo -3; exit();
            }
            $existenOpExtras = $this->ml->validarExisteOperacionesExtras($this->input->post('id_cita_save'));
            if($existenOpExtras){
            echo -6;exit();
            }
            //si existe una cotización sin afectar no se puede dar por terminado
            if($this->ml->existeCotizacion($this->input->post('id_cita_save'))){
              echo -9;exit();
            }
          }

          //Validar que estén todas las operaciones asignadas por el asesor
          if(!$this->ml->OperacionesAsignadas($idorden) && $this->input->post('id_status')!=5){
            echo -8;exit();
          }

          
          //NUEVA VERSION
          $estatus_general = $this->ml->getTipoStatus($this->input->post('id_status'));
          $this->mc->cambiar_status_cita_tecnico($idhorario,$estatus_general);
           //GUARDAR TRANSICIONES DE LOS ESTATUS DE ASESOR

          $status_actual = $this->input->post('status');
          
          //NUEVA VERSION
          $this->ml->insertTransicion($estatus_general);

          //CUANDO ES LAVADO
          if($this->input->post('id_status')==3){
            $datos_cita = $this->mc->getDatosCita($this->input->post('id_cita_save'));
            
            $fecha = date('Y-m-d');
            $hora = date('H:i');
            $id_cita = $this->input->post('id_cita_save');
            $anio =  $datos_cita->vehiculo_anio;
            $modelo =  $datos_cita->vehiculo_modelo;
            $placas =  $datos_cita->vehiculo_placas;
            $hora_promesa = $this->mc->only_hora_promesa($this->input->post('id_cita_save'));
            $comentario = $this->input->post('comentarios');
            

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,CONST_LAVADO);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,
                        "fecha=".$fecha."&hora=".$hora."&id_cita=".$id_cita.""."&anio=".$anio.""."&modelo=".$modelo.""."&placas=".$placas.""."&hora_promesa=".$hora_promesa.""."&comentario=".$comentario."");
            // Receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec($ch);
            
            
            $response = curl_exec($ch);
            $err = curl_error($ch);
            curl_close ($ch);
            
            $mensaje_sms = CONST_AGENCIA_MENSAJE.", UNIDAD TERMINADA, #cita: ".$id_cita." el día ".date('Y-m-d H:i:s');
            //Enviar mensaje al asesor
            $this->db->where('id_cita',$id_cita)->set('id_status',3)->update('citas');//
          }

          //Enviar mensajes en unidad de prueba
          if($this->input->post('id_status')==12){
            $datos_cita = $this->mc->getDatosCita($this->input->post('id_cita_save'));
            $fecha = $datos_cita->fecha;
            $hora = date('H:i');
            $id_cita = $this->input->post('id_cita_save');
            $anio =  $datos_cita->vehiculo_anio;
            $modelo =  $datos_cita->vehiculo_modelo;
            $placas =  $datos_cita->vehiculo_placas;

          }

          //Si es 7 (entrega de refacciones poner donde esta ubicada la unidad)
          if($this->input->post('id_status')==7){
            $this->db->where('id_cita',$this->input->post('id_cita_save'))->set('ubicacion_unidad',$this->input->post('ubicacion'))->update('citas');
          }
          
          if($this->input->post('id_status')!=5){
            $this->db->where('id_cita',$this->input->post('id_cita_save'))->set('id_status_anterior',$id_status_anterior)->update('citas'); 
            //NUEVA VERSION
            $this->db->where('id',$this->input->post('id_operacion'))->set('id_status',$this->input->post('id_status'))->update('articulos_orden');
            
            //Si es estatus detención actualizar en la tabla de ordenes el motivo
            if(isset($_POST['estatus_detenido'])&&$_POST['estatus_detenido']){
              $this->db->where('id',$_POST['id_operacion'])->set('motivo_detencion',$_POST['motivo_detencion'])->set('fecha_detencion',date('Y-m-d H:i:s'))->update('articulos_orden');
            }else{
               $this->db->where('id',$_POST['id_operacion'])->set('motivo_detencion',null)->update('articulos_orden');
            }

            $this->db->where('id_cita',$this->input->post('id_cita_save'))->set('id_status_anterior',$id_status_anterior)->set('id_status',$_POST['id_status'])->update('citas'); 

          }
          //Enviar el correo de cambio de estatus
          if($this->input->post('id_status')!=3&&$this->input->post('id_status')!=5&&$this->input->post('id_status')!=4){
            $datos_cita = $this->mc->getDatosCita($_POST['id_cita_save']);
            $estatus_cambio = $this->principal->getGeneric('id',$_POST['id_status'],'cat_status_citas_tecnicos','status');
            correo_cambio_estatus($datos_cita->datos_email,encrypt($_POST['id_cita_save']),encrypt($_POST['id_operacion']),$estatus_cambio);
          }
          
          echo 1;die();

         
          }else{
             $errors = array(
                  'id_status' => form_error('id_status'),
                  'motivo_detencion' => form_error('motivo_detencion'),
             );
            echo json_encode($errors); exit();
          }
      }

  }
  //Enviar mensajes cuando es unidad de prueba
  public function mensaje_unidad_prueba($placas='',$celular_sms='',$idcita=''){
    $mensaje_sms = "FORD PLASENCIA, unidad de prueba, placas: ".$placas.', #orden: '.$idcita;
     $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,CONST_URL_NOTIFICACION_APP);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                "celular=".$celular_sms."&mensaje=".$mensaje_sms."&sucursal=".CONST_BID);
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close ($ch);
  }
  public function comentarios_refacciones(){
    $data['comentarios'] = $this->db->where('idcita',$this->input->post('idcita'))->order_by('fecha','desc')->get('comentarios_refacciones')->result();
    $this->blade->render('lista_comentarios_refacciones',$data);
  }
  public function saveComentarioRefacciones(){
    if($this->input->post()){
          if($this->input->post('realizo_prepiking')){
            $this->form_validation->set_rules('comentario', 'comentario', 'trim');
          }else{
            $this->form_validation->set_rules('comentario', 'comentario', 'trim');
          }

          if($this->input->post('id_status')!='') {
            $this->form_validation->set_rules('usuario_tecnico', 'usuario', 'trim|required');
            $this->form_validation->set_rules('password_tecnico', 'contraseña', 'trim|required');
          }else{
            $this->form_validation->set_rules('usuario_tecnico', 'usuario', 'trim');
           $this->form_validation->set_rules('password_tecnico', 'contraseña', 'trim');
          }
          //
           $this->form_validation->set_rules('id_status', ' ', 'trim');
           

       if ($this->form_validation->run()){
        //Validar login
          if($this->input->post('id_status')!='') {
            if(!$this->mc->getUserRefacciones($this->input->post('usuario_tecnico'),$this->input->post('password_tecnico'))){
              echo -1;exit();
            }
          }
           $this->db->set('idcita',$this->input->post('idcita'))->set('comentario',$this->input->post('comentario'))->set('fecha',date('Y-m-d H:i:s'))->insert('comentarios_refacciones');

           //GUARDAR TRANSICIONES

           //if($this->input->post('id_status')==7 || $this->input->post('id_status')==13 || $this->input->post('id_status')==14) {
            if($this->input->post('id_status')!='') {
            $id_status_anterior = $this->mc->getStatusCitaPrincipal($this->input->post('idcita')); //estatus anterior para transiciones
            //Actualizar status
            if($this->db->where('id_cita',$this->input->post('idcita'))->set('id_status',$this->input->post('id_status'))->set('realizo_prepiking',$this->input->post('realizo_prepiking'))->update('citas')){
               $id_status_actual = $this->mc->getStatusTecnico($this->input->post('idcita')); //Obtiene el estatus actual del técnico
              $id_tecnico = $this->mc->getTecnicoByCita($this->input->post('idcita'));
              $datos_historial = array('id_cita' =>$this->input->post('idcita'),
                               'id_tecnico' => $id_tecnico,
                               'fecha'=> date('Y-m-d H:i:s'),
                               'id_status_anterior'=>$id_status_actual,
                               'id_status_nuevo' =>$this->input->post('id_status'),
                );
              $this->db->insert('historial_cambio_status',$datos_historial);
            }
            $inicio = $this->mc->getLastTransition($this->input->post('idcita'));
            $datos_transiciones = array('status_anterior' => $this->mc->getLastStatusTransiciones($this->input->post('idcita')),
                                      'status_nuevo'=> $this->mc->getStatusTecnicoTransiciones($this->input->post('id_status')),
                                      'fecha_creacion'=>date('Y-m-d H:i:s'),
                                      'tipo'=>2, //técnico,
                                      'inicio'=>$inicio,
                                      'fin'=>date('Y-m-d H:i:s'),
                                      'usuario'=>$this->input->post('usuario_tecnico'),
                                      'id_cita'=>$this->input->post('idcita'),
                                      'minutos_transcurridos'=>dateDiffMinutes($inicio , date('Y-m-d H:i:s'))
              );
              $this->db->insert('transiciones_estatus',$datos_transiciones);

              $datos_notificacion = array('titulo'=>'Entrega de refacciones',
                                          'texto' => 'Llegaron refacciones del la cita con id: '.$this->input->post('idcita'),
                                          'id_user' => '',
                                          'estado'=>0,
                                          'url'=>'',
                                          'estadoweb'=>1,
                                          'tipo_notificacion'=>3,
                                          'fecha_hora'=>date('Y-m-d H:i:s')
              );
              $this->db->insert(CONST_BASE_PRINCIPAL.'noti_user',$datos_notificacion);
           }else{
              $this->db->where('id_cita',$this->input->post('idcita'))->set('realizo_prepiking',$this->input->post('realizo_prepiking'))->update('citas');
           }

          //nombre del asesor
          $nombre_asesor = $this->mc->asesor_by_cita($this->input->post('idcita'));
          $celular_sms = $this->mc->telefono_asesor_by_nombre($nombre_asesor);

          $estatus_mensaje = $this->mc->getStatusTecnicoTransiciones($this->input->post('id_status'));
          $mensaje_sms = "FORD PLASENCIA!, ".$estatus_mensaje." de la cita #".$this->input->post('idcita')." el día ".date('Y-m-d H:i:s');

          //Enviar mensaje al asesor
          $this->sms_general($celular_sms,$mensaje_sms);//$celular_sms

           echo 1;die();
          }else{
             $errors = array(
                  'comentario' => form_error('comentario'),
                  'id_status' => form_error('id_status'),
                  'usuario_tecnico' => form_error('usuario_tecnico'),
                  'password_tecnico' => form_error('password_tecnico'),
             );
            echo json_encode($errors); exit();
          }
      }
  }
  public function generarhorarios(){
    ini_set('max_execution_time', 30000);
    //echo 'entre';die();
    $this->db->where('id',21);
    $tecnicos = $this->db->get('tecnicos')->result();
      foreach ($tecnicos as $key => $value) {

        $fecha = date('Y-m-j');
        //437
        while($fecha<'2019-12-31'){
           $dia = date('l', strtotime($fecha));
        if($dia!='Sunday'){

         if($dia=='Saturday'){
            $hora_inicio = "07:40:00";
            $hora_fin = "13:00:00";
            $hora_inicio_comida = "00:00:00";
            $hora_fin_comida = "00:00:00";
          }else{
            $hora_inicio = "07:40:00";
            $hora_fin = "18:30:00";
            $hora_inicio_comida = "13:00:00";
            $hora_fin_comida = "15:00:00";
          }
          $citas = array('fecha' => $fecha,
                         'hora_inicio' =>$hora_inicio,
                         'hora_fin' => $hora_fin,
                         'hora_inicio_comida' =>$hora_inicio_comida,
                         'hora_fin_comida' =>$hora_fin_comida,
                         'fecha_creacion'=>date('Y-m-d'),
                         'id_tecnico'=>$value->id,
                         'activo'=>1

           );
            $this->db->insert('horarios_tecnicos',$citas);


        }
            $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
            $fecha = date ( 'Y-m-j' , $nuevafecha );
        }


      }
  }
  public function generarhorariosos(){
    ini_set('max_execution_time', 30000);
    $this->db->where('id',9);
    $tecnicos = $this->db->get('tecnicos')->result();
      foreach ($tecnicos as $key => $value) {

        $fecha = date('Y-m-j');
        //437
        while($fecha<'2019-12-31'){
           $dia = date('l', strtotime($fecha));
        if($dia=='Sunday'){

          $hora_inicio = "10:00:00";
          $hora_fin = "15:00:00";
          $hora_inicio_comida = "00:00:00";
          $hora_fin_comida = "00:00:00";

          $citas = array('fecha' => $fecha,
                         'hora_inicio' =>$hora_inicio,
                         'hora_fin' => $hora_fin,
                         'hora_inicio_comida' =>$hora_inicio_comida,
                         'hora_fin_comida' =>$hora_fin_comida,
                         'fecha_creacion'=>date('Y-m-d'),
                         'id_tecnico'=>$value->id,
                         'activo'=>1

           );
            $this->db->insert('horarios_tecnicos',$citas);


        }
            $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
            $fecha = date ( 'Y-m-j' , $nuevafecha );
        }


      }
  }
  public function v_horarios(){
    //Obtener la última fecha
    $q = $this->db->select('fecha')->order_by('fecha','desc')->limit(1)->get('aux');
    if($q->num_rows()==1){
      $nuevafecha = strtotime ( '+1 day' , strtotime ($q->row()->fecha) ) ;
      $fecha = date ( 'Y-m-d' , $nuevafecha );
    }else{
      $nuevafecha = strtotime ( '+1 day' , strtotime (date('Y-m-d')) ) ;
      $fecha = date ( 'Y-m-d' , $nuevafecha );
    }
    $data['fecha_apartir'] = $fecha;
    //echo $fecha;die();
    $this->blade->render('v_generar_horarios_aux',$data);
  }
   public function generarhorariosAux(){
      ini_set('max_execution_time', 300);

     if($this->input->post('fecha_apartir') >= date2sql($this->input->post('fecha_hasta'))) {
      echo -1;die();
     }
    $operadores = $this->db->get('operadores')->result();
    $random = $this->random(10);
    $contador = 0;
      foreach ($operadores as $key => $value) {
        $contador++;
        $fecha = $this->input->post('fecha_apartir');
        while($fecha<date2sql($this->input->post('fecha_hasta'))){
          $time = '07:40';
          $contador=0;
          while($contador<34){
            $fecha_parcial = explode('-', $fecha);
            $aux = array('hora' => $time,
                       'id_operador' =>$value->id,
                       'fecha_creacion'=>date('Y-m-d'),
                        'dia' => $fecha_parcial[2],
                        'mes' => $fecha_parcial[1],
                        'anio' => $fecha_parcial[0],
                        'fecha'=>$fecha,
                        'ocupado'=>0,
                        'activo'=>1,
                        'id_status_cita'=>1,
                        'random'=>$random

            );
            $timestamp = strtotime($time) + 20*60;
            $time = date('H:i', $timestamp);
            $contador++;
            $this->db->insert('aux',$aux);
          }
          $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
          $fecha = date ( 'Y-m-d' , $nuevafecha );
        }
      }

      echo $contador;
  }
  function agregar_motivo($id=0)
    {


      if($this->input->post()){
        $this->form_validation->set_rules('motivo', 'motivo', 'trim|required');
       if ($this->form_validation->run()){
          $this->mc->guardarOperador();
          }else{
             $errors = array(
                  'motivo' => form_error('motivo'),
             );
            echo json_encode($errors); exit();
          }
      }

      if($id==0){
        $info=new Stdclass();


      }else{
        $info= $this->mc->getOperadorId($id);
        $info=$info[0];
      }


      $data['input_id'] = $id;
      $data['input_motivo'] = form_input('motivo',set_value('motivo',exist_obj($info,'motivo')),'class="form-control" rows="5" id="motivo" ');
      $this->blade->render('motivo_desactivar',$data);
  }
  // public function generarHorariosAux(){
  //   $time = '08:00';
  //   $contador=0;
  //   while($contador<27){


  //     $timestamp = strtotime($time) + 30*60;
  //     $time = date('H:i', $timestamp);
  //     $contador++;
  //   }
  // }
  public function login_editar_cita(){
       if($this->input->post()){
        $this->form_validation->set_rules('usuario', 'usuario', 'trim|required');
        $this->form_validation->set_rules('password', 'contraseña', 'trim|required');

       if ($this->form_validation->run()){
          $this->mc->validarLogin();
          exit();
          }else{
             $errors = array(
                  'usuario' => form_error('usuario'),
                  'password' => form_error('password'),
             );
            echo json_encode($errors); exit();
          }
      }


      $data['input_usuario'] = form_input('usuario',$this->input->get('usuario_tecnico'),'class="form-control" rows="5" id="usuario" ');

     $data['input_password'] = form_password('password',"",'class="form-control" rows="5" id="password" ');
     $this->blade->render('login_editar_cita',$data);
  }
  public function login_editar_cita_cancelar_eliminar(){
       if($this->input->post()){
        $this->form_validation->set_rules('usuario', 'usuario', 'trim|required');
        $this->form_validation->set_rules('password', 'contraseña', 'trim|required');

       if ($this->form_validation->run()){
          $this->mc->validarLoginCancelarEliminar();
          exit();
          }else{
             $errors = array(
                  'usuario' => form_error('usuario'),
                  'password' => form_error('password'),
             );
            echo json_encode($errors); exit();
          }
      }


      $data['input_usuario'] = form_input('usuario',$this->input->get('usuario_tecnico'),'class="form-control" rows="5" id="usuario" ');

     $data['input_password'] = form_password('password',"",'class="form-control" rows="5" id="password" ');
     $this->blade->render('login_editar_cita',$data);
  }
  //Modal para ver los comentarios de las citas
  public function ver_comentarios_citas(){
    if($this->session->userdata('tipo_perfil')==4){
      $this->db->where('islavado',1);
    }
    $data['comentarios'] = $this->db->where('id_cita',$this->input->post('id_cita'))->order_by('fecha','desc')->get('comentarios_cita')->result();
    $this->blade->render('modal_comentarios_citas',$data);
  }
  public function getIdHorario(){
    $horarios = $this->db->select('id_horario')->from('citas')->get()->result();
   $cadena = '';
   foreach ($horarios as $h => $value) {
    $cadena.= $value->id_horario.',';
   }
   echo $cadena;die();
    debug_var($horarios);die();
  }
  public function eliminar_cita_bd(){
    if($this->input->post()){
      $id_cita = $this->input->post('id');
      $hayCita = $this->mc->getCitaId($id_cita);
      if(count($hayCita)==0){
        echo 0;die();
      }
      $id_horario = $this->mc->getDatoCita($id_cita,"id_horario");
      $this->db->where('id',$id_horario)->set('ocupado',0)->update('aux');
      $this->db->where('id_cita',$id_cita)->delete('tecnicos_citas');
      $this->db->where('id_cita',$id_cita)->delete('tecnicos_citas_historial');
      $this->db->where('id_cita',$id_cita)->delete('citas');

      //Guardar quien la eliminó
      $datos_eliminacion = array('id_cita' => $id_cita, 
                                 'created_at' => date('Y-m-d H:i:s'),
                                 'id_usuario'=>$this->session->userdata('id_usuario'),
      );
      $this->db->insert('historial_eliminaciones',$datos_eliminacion);
       echo 1;die();
    }
    $this->blade->render('eliminar_cita');
  }
 public function agendar_cita_tablero($id=0){

       $info=new Stdclass();
        $info_horario=new Stdclass();
        $id_horario = 0;
        $realizo_servicio = 0;
        $tecnico_actual = 0;
        $dia_completo = 0;
        $fecha_verificar = date('Y-m-d');

      $data['input_id'] = $id;
      $data['input_id_horario'] = $id_horario;
      $data['input_realizo_servicio'] = $realizo_servicio;
      $data['input_id_tecnico_actual'] = $tecnico_actual;
      $data['fecha_verificar'] = $fecha_verificar;
      $data['drop_transporte'] = form_dropdown('citas[transporte]',array_combos($this->mcat->get('cat_transporte','transporte'),'id','transporte',TRUE),set_value('transporte',exist_obj($info,'transporte')),'class="form-control" id="transporte"');


      $email_info = set_value('email',exist_obj($info,'email'));
      if($email_info ==''){
        $email_info = 'notienecorreo@notienecorreo.com.mx';
      }

       $data['input_email'] = form_input('citas[email]',$email_info,'class="form-control minusculas" rows="5" id="email" ');

      $data['drop_vehiculo_anio'] = form_dropdown('citas[vehiculo_anio]',array_combos($this->mcat->get('cat_anios','anio','desc'),'anio','anio',TRUE),set_value('vehiculo_anio',exist_obj($info,'vehiculo_anio')),'class="form-control busqueda" id="vehiculo_anio"');

       $data['drop_vehiculo_marca'] = form_dropdown('citas[vehiculo_marca]',array_combos($this->mcat->get('cat_marca','marca'),'marca','marca',TRUE),set_value('vehiculo_marca',exist_obj($info,'vehiculo_marca')),'class="form-control" id="vehiculo_marca"');

        $data['drop_vehiculo_modelo'] = form_dropdown('citas[vehiculo_modelo]',array_combos($this->mcat->get('cat_modelo','modelo'),'modelo','modelo',TRUE),set_value('vehiculo_modelo',exist_obj($info,'vehiculo_modelo')),'class="form-control busqueda" id="vehiculo_modelo"');

       $data['drop_vehiculo_version'] = form_dropdown('citas[vehiculo_version]',array_combos($this->mcat->get('cat_version','version'),'version','version',TRUE),set_value('vehiculo_version',exist_obj($info,'vehiculo_version')),'class="form-control" id="vehiculo_version"');

        $data['input_vehiculo_placas'] = form_input('citas[vehiculo_placas]',set_value('vehiculo_placas',exist_obj($info,'vehiculo_placas')),'class="form-control" rows="5" id="vehiculo_placas" ');

        $data['input_vehiculo_numero_serie'] = form_input('citas[vehiculo_numero_serie]',set_value('vehiculo_numero_serie',exist_obj($info,'vehiculo_numero_serie')),'class="form-control" rows="5" id="vehiculo_numero_serie" maxlength="17" ');

        $data['input_comentarios'] = form_textarea('citas[comentarios_servicio]',set_value('comentarios_servicio',exist_obj($info,'comentarios_servicio')),'class="form-control" id="comentarios_servicio" rows="2"');

        //echo set_value('asesor',exist_obj($info,'asesor'));die();
        $data['drop_asesor'] = form_dropdown('citas[asesor]',array_combos($this->mc->getAsesorByFecha($this->input->post('fecha')),'nombre','nombre',TRUE),set_value('asesor',exist_obj($info,'asesor')),'class="form-control" id="asesor" ');

         $id_asesor=$this->mc->getIdAsesor(exist_obj($info,'asesor'));

          $disabled = '';

          $data['drop_fecha'] = form_input('citas[fecha]',$this->input->post('fecha'),'class="form-control"  id="fecha" readonly ');

        if($id==0){
          $opciones_horario = array('' =>'-- Selecciona --', );
        }else{
          $id_asesor=$this->mc->getIdAsesor(exist_obj($info,'asesor'));
          //echo $this->db->last_query();die();
          $horario_id = set_value('id',exist_obj($info_horario,'id'));
          $opciones_horario = array_combos($this->mc->getHorariosAsesor_edit($id_asesor,set_value('fecha',exist_obj($info_horario,'fecha')),$horario_id),'id','hora',TRUE);
        }

        $data['drop_horario'] = form_dropdown('citas[horario]',$opciones_horario,set_value('id',exist_obj($info_horario,'id')),'class="form-control" id="horario" '.$disabled.' ');





        $data['input_datos_nombres'] = form_input('citas[datos_nombres]',set_value('datos_nombres',exist_obj($info,'datos_nombres')),'class="form-control"  id="datos_nombres" ');

        $data['input_datos_apellido_paterno'] = form_input('citas[datos_apellido_paterno]',set_value('datos_apellido_paterno',exist_obj($info,'datos_apellido_paterno')),'class="form-control"  id="datos_apellido_paterno" ');

         $data['input_datos_apellido_materno'] = form_input('citas[datos_apellido_materno]',set_value('datos_apellido_materno',exist_obj($info,'datos_apellido_materno')),'class="form-control"  id="datos_apellido_materno" ');

          $data['input_datos_telefono'] = form_input('citas[datos_telefono]',set_value('datos_telefono',exist_obj($info,'datos_telefono')),'class="form-control"  id="datos_telefono" maxlength="10" ');

          $data['drop_servicio'] = form_dropdown('citas[servicio]',array_combos($this->mcat->get('cat_servicios','servicio'),'servicio','servicio',TRUE),set_value('servicio',exist_obj($info,'servicio')),'class="form-control" id="servicio"');

          
        if($id==0){
          //Nueva versión
          if($_POST['fecha']!=''){
            $fecha = date2sql($_POST['fecha']);
          }else{
            $fecha= date('Y-m-d');
          }
          
        }else{

          $fecha = $info_horario->fecha;
        }



        $data['drop_color'] = form_dropdown('citas[id_color]',array_combos($this->mcat->get('cat_colores','color'),'id','color',TRUE),set_value('id_color',exist_obj($info,'id_color')),'class="form-control busqueda" id="id_color"');

        //if($id!=0){


      $data['drop_tecnicos'] = form_dropdown('tecnico',array_combos($this->mc->getTecnicosByFechaCita($fecha),'id_tecnico','nombre',TRUE),$this->input->post('id_tecnico'),'class="form-control" id="tecnicos" readonly ');
        //}
      $data['btn_guardar'] = form_button('btnguardar','Guardar cita',' id="guardar" class="btn btn-success btn-block pull-right"');

      $data['input_hora_inicio'] = form_input('hora_inicio',$this->input->post('time'),'class="form-control" id="hora_inicio" readonly ','time');

      $data['input_hora_fin'] = form_input('hora_fin',set_value('hora_fin',exist_obj($info,'hora_fin')),'class="form-control" id="hora_fin"','time');

      $data['input_dia_completo'] = form_checkbox('dia_completo',$dia_completo,($dia_completo==1)?'checked':'','class="" id="dia_completo" ');

      //Cuando son días completos


      $data['input_fecha_inicio'] = form_input('fecha_inicio',$this->input->post('fecha'),'class="form-control" id="fecha_inicio" readonly ');

      $data['input_fecha_fin'] = form_input('fecha_fin',set_value('fecha_fin',exist_obj($info,'fecha_fin')),'class="form-control" id="fecha_fin" ','date');

      $data['drop_tecnicos_dias'] = form_dropdown('tecnico_dias',array_combos($this->mc->getTecnicosByFechaCita($fecha),'id_tecnico','nombre',TRUE),$this->input->post('id_tecnico'),'class="form-control" id="tecnicos_dias" readonly ');

      $data['input_fecha_parcial'] = form_input('fecha_parcial',set_value('fecha_inicio',exist_obj($info,'fecha_parcial')),'class="form-control" id="fecha_parcial" ','date');

       $data['input_hora_inicio_extra'] = form_input('hora_inicio_extra',"",'class="form-control" id="hora_inicio_extra" ','time');

      $data['input_hora_fin_extra'] = form_input('hora_fin_extra',set_value('hora_fin',exist_obj($info,'hora_fin')),'class="form-control" id="hora_fin_extra"','time');

      //si es por día completo saber cuando empieza
      $data['input_hora_comienzo'] = form_input('hora_comienzo',$this->input->post('time'),'class="form-control" id="hora_comienzo" readonly ','time');

       $data['drop_id_status_color'] = form_dropdown('citas[id_status_color]',array_combos($this->mcat->get('estatus','nombre'),'id','nombre',TRUE),set_value('id_status_color',exist_obj($info,'id_status_color')),'class="form-control busqueda" id="id_status_color"');
      $this->blade->render('agendar_cita_tablero',$data);

  }
  public function updateHorarios(){
    $horarios = $this->db->where('fecha >=','2018-02-03')->get('horarios_tecnicos')->result();

    foreach ($horarios as $h => $value) {
       $dia = date('l', strtotime($value->fecha));
       if($dia=='Saturday' && $value->id_tecnico == 6){
        $this->db->where('id',$value->id)->set('hora_inicio','08:00:00')->set('hora_fin','13:00:00')->set('hora_inicio_comida','00:00:00')->set('hora_fin_comida','00:00:00')->update('horarios_tecnicos');

       }
       //Oswaldo
       if($dia=='Saturday' && $value->id_tecnico == 9){
        $this->db->where('id',$value->id)->set('hora_inicio','09:00:00')->set('hora_fin','15:00:00')->set('hora_inicio_comida','00:00:00')->set('hora_fin_comida','00:00:00')->update('horarios_tecnicos');

       }
       //Enrique
       if($dia=='Sunday' && $value->id_tecnico == 14){
        $this->db->where('id',$value->id)->set('hora_inicio','10:00:00')->set('hora_fin','14:00:00')->set('hora_inicio_comida','00:00:00')->set('hora_fin_comida','00:00:00')->update('horarios_tecnicos');

       }
    }
    //Pendiente por autorización (#FA58F4) ... morado mas light (#8181F7)
  }
  public function busquedas_inteligentes(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $data['tabla'] = $this->mc->buscar_citas(1);
    $data['tipo'] = 1;
    $data['drop_tecnicos'] = form_dropdown('id_tecnico[]',array_combos($this->mcat->get('tecnicos','nombre'),'id','nombre',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_tecnico"');

    $data['drop_servicios'] = form_dropdown('id_servicio[]',array_combos($this->mcat->get('cat_servicios','servicio'),'servicio','servicio',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_servicio"');

   $data['drop_status'] = form_dropdown('id_status[]',array_combos($this->mcat->get('estatus','nombre'),'id','nombre',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_status"');

    $data['drop_asesores'] = form_dropdown('id_asesor[]',array_combos($this->mcat->get('operadores','nombre'),'nombre','nombre',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_asesor"');

   $data['drop_status_tecnico'] = form_dropdown('id_status_tecnico[]',array_combos($this->mcat->get('cat_status_citas_tecnicos','status'),'id','status',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_status_tecnico"');

   $tipos = array('1' =>'Confirmada','0'=>'No confirmada');
    $data['drop_tipo'] = form_dropdown('confirmada[]',$tipos,"",'class="form-control multiple" multiple="multiple" id="confirmada"');

    $tipos_cancelada = array('1' =>'Canceladas');
    $data['drop_tipo_canceladas'] = form_dropdown('cancelada[]',$tipos_cancelada,"",'class="form-control multiple" multiple="multiple" id="cancelada"');

    $cita_previa = array('0' =>'No','1'=>'Si');
    $data['drop_tipo_cita'] = form_dropdown('cita_previa[]',$cita_previa,"",'class="form-control multiple" multiple="multiple" id="cita_previa"');
    $this->blade->render('busquedas_inteligentes',$data);
  }
  public function buscar_citas(){
    echo $this->mc->buscar_citas();

  }
  public function ver_logistica(){
    $data['logistica'] = $this->mc->getLogistic();
    $this->blade->render('ver_logistica',$data);
  }
  public function logistica($id=0){
       if($this->input->post()){
        $this->form_validation->set_rules('eco', 'eco', 'trim|required');
        $this->form_validation->set_rules('entrega_cliente', 'entrega al cliente', 'trim|required');
        $this->form_validation->set_rules('id_unidad', 'unidad', 'trim|required');
        $this->form_validation->set_rules('id_color', 'color', 'trim|required');
        $this->form_validation->set_rules('serie', 'serie', 'trim|required');
        $this->form_validation->set_rules('cliente', 'cliente', 'trim|required');
        $this->form_validation->set_rules('id_vendedor', 'vendedor', 'trim|required');
        $this->form_validation->set_rules('tipo_operacion', 'tipo de operación', 'trim|required');
        $this->form_validation->set_rules('iva_desg', 'IVA DESG', 'trim|required|numeric');
        $this->form_validation->set_rules('toma', 'toma', 'trim|required');
        $this->form_validation->set_rules('hora_promesa', 'hora promesa', 'trim|required');
        $this->form_validation->set_rules('ubicacion', 'ubicación', 'trim|required');
        $this->form_validation->set_rules('reprogramacion', 'reprogramacion', 'trim');
        $this->form_validation->set_rules('seguro', 'seguro', 'trim');
        $this->form_validation->set_rules('permiso', 'permiso', 'trim');
        $this->form_validation->set_rules('placas', 'placas', 'trim');
        $this->form_validation->set_rules('accesorio', 'accesorio', 'trim|required');
        $this->form_validation->set_rules('servicio', 'servicio', 'trim');
        $this->form_validation->set_rules('fechaprog', 'fechaprog', 'trim|required');
        $this->form_validation->set_rules('horaprog', 'horaprog', 'trim|required');

       if ($this->form_validation->run()){
          $this->mc->saveLogistic();
          exit();
          }else{
             $errors = array(
                  'eco' => form_error('eco'),
                  'entrega_cliente' => form_error('entrega_cliente'),
                  'id_unidad' => form_error('id_unidad'),
                  'id_color' => form_error('id_color'),
                  'serie' => form_error('serie'),
                  'cliente' => form_error('cliente'),
                  'id_vendedor' => form_error('id_vendedor'),
                  'tipo_operacion' => form_error('tipo_operacion'),
                  'iva_desg' => form_error('iva_desg'),
                  'toma' => form_error('toma'),
                  'hora_promesa' => form_error('hora_promesa'),
                  'ubicacion' => form_error('ubicacion'),
                  'reprogramacion' => form_error('reprogramacion'),
                  'seguro' => form_error('seguro'),
                  'permiso' => form_error('permiso'),
                  'placas' => form_error('placas'),
                  'accesorio' => form_error('accesorio'),
                  'servicio' => form_error('servicio'),
                  'fechaprog' => form_error('fechaprog'),
                  'horaprog' => form_error('horaprog'),

             );
            echo json_encode($errors); exit();
          }
      }

      if($id==0){
        $info=new Stdclass();
        $reprogramacion = 0;
        $seguro = 0;
        $permiso = 0;
        $placas = 0;
        $servicio = 0;
        $fechaprog = 0;
      }else{
        $info= $this->mc->getLogisticaId($id);
        $info=$info[0];
        $reprogramacion = $info->reprogramacion ;
        $seguro = $info->seguro ;
        $permiso = $info->permiso ;
        $placas = $info->placas ;
        $servicio = $info->servicio ;
        $fechaprog = $info->fechaprog ;

      }
      $data['input_id'] = $id;

    $data['input_entrega_cliente'] = form_input('entrega_cliente',set_value('entrega_cliente',exist_obj($info,'entrega_cliente')),'class="form-control" id="entrega_cliente" ');

    $data['input_eco'] = form_input('eco',set_value('eco',exist_obj($info,'eco')),'class="form-control" id="eco" ');

    $data['drop_id_unidad'] = form_dropdown('id_unidad',array_combos($this->mcat->get('cat_modelo','modelo'),'id','modelo',TRUE),set_value('id_unidad',exist_obj($info,'id_unidad')),'class="form-control busqueda" id="id_unidad" ');

     $data['drop_id_color'] = form_dropdown('id_color',array_combos($this->mcat->get('cat_colores','color'),'id','color',TRUE),set_value('id_color',exist_obj($info,'id_color')),'class="form-control busqueda" id="id_color" ');

    $data['input_serie'] = form_input('serie',set_value('serie',exist_obj($info,'serie')),'class="form-control" id="serie" ');

    $data['input_cliente'] = form_input('cliente',set_value('cliente',exist_obj($info,'cliente')),'class="form-control" id="cliente" ');

    $data['drop_id_vendedor'] = form_dropdown('id_vendedor',array_combos($this->mcat->get('vendedores','clave'),'id','clave',TRUE),set_value('id_vendedor',exist_obj($info,'id_vendedor')),'class="form-control busqueda" id="id_vendedor" ');

     $data['input_tipo_operacion'] = form_input('tipo_operacion',set_value('tipo_operacion',exist_obj($info,'tipo_operacion')),'class="form-control" id="tipo_operacion" ');

    $data['input_iva_desg'] = form_input('iva_desg',set_value('iva_desg',exist_obj($info,'iva_desg')),'class="form-control" id="iva_desg" ');

    $data['input_toma'] = form_input('toma',set_value('toma',exist_obj($info,'toma')),'class="form-control" id="toma" ');

   $data['input_hora_promesa'] = form_input('hora_promesa',set_value('hora_promesa',exist_obj($info,'hora_promesa')),'class="form-control" id="hora_promesa" ');

    $data['input_ubicacion'] = form_input('ubicacion',set_value('ubicacion',exist_obj($info,'ubicacion')),'class="form-control" id="ubicacion" ');

    $data['input_reprogramacion'] = form_checkbox('reprogramacion',$reprogramacion,($reprogramacion==1)?'checked':'','class="js_check" id="reprogramacion" ');

    $data['input_seguro'] = form_checkbox('seguro',$seguro,($seguro==1)?'checked':'','class="js_check" id="seguro" ');

    $data['input_permiso'] = form_checkbox('permiso',$permiso,($permiso==1)?'checked':'','class="js_check" id="permiso" ');

    $data['input_placas'] = form_checkbox('placas',$placas,($placas==1)?'checked':'','class="js_check" id="placas" ');

    $data['input_accesorio'] = form_input('accesorio',set_value('accesorio',exist_obj($info,'accesorio')),'class="form-control" id="accesorio" ');

    $data['input_servicio'] = form_checkbox('servicio',$servicio,($servicio==1)?'checked':'','class="js_check" id="servicio" ');



    $data['input_fechaprog'] = form_input('fechaprog',date_eng2esp_1(set_value('fechaprog',exist_obj($info,'fechaprog'))),'class="form-control" id="fechaprog" ');

    $data['input_horaprog'] = form_input('horaprog',set_value('horaprog',exist_obj($info,'horaprog')),'class="form-control" id="horaprog" ');

    $data['btn_guardar'] = form_button('btnguardar','Guardar',' id="guardar" class="btn btn-success btn-block pull-right"');
     $this->blade->render('logistica',$data);
  }
  public function eliminar_logistica(){
    if($this->db->where('id',$this->input->post('id'))->delete('logistica')){
      echo 1;
    }else{
      echo 0;
    }
  }
  public function filtro_citas(){
    $data['tabla'] = $this->mc->buscar_citas(2);
    $data['tipo'] = 2;
    $data['drop_tecnicos'] = form_dropdown('id_tecnico[]',array_combos($this->mcat->get('tecnicos','nombre'),'id','nombre',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_tecnico"');

    $data['drop_servicios'] = form_dropdown('id_servicio[]',array_combos($this->mcat->get('cat_servicios','servicio'),'servicio','servicio',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_servicio"');

   $data['drop_status'] = form_dropdown('id_status[]',array_combos($this->mcat->get('estatus','nombre'),'id','nombre',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_status"');

    $data['drop_asesores'] = form_dropdown('id_asesor[]',array_combos($this->mcat->get('operadores','nombre'),'nombre','nombre',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_asesor"');

   $data['drop_status_tecnico'] = form_dropdown('id_status_tecnico[]',array_combos($this->mcat->get('cat_status_citas_tecnicos','status'),'id','status',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_status_tecnico"');

    $this->blade->render('filtro_citas',$data);
  }
  public function refacciones_filtro(){
    $data['tabla'] = $this->mc->buscar_citas(2);
    $data['tipo'] = 2;
    $data['drop_tecnicos'] = form_dropdown('id_tecnico[]',array_combos($this->mcat->get('tecnicos','nombre'),'id','nombre',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_tecnico"');

    $data['drop_servicios'] = form_dropdown('id_servicio[]',array_combos($this->mcat->get('cat_servicios','servicio'),'servicio','servicio',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_servicio"');

   $data['drop_status'] = form_dropdown('id_status[]',array_combos($this->mcat->get('estatus','nombre'),'id','nombre',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_status"');

    $data['drop_asesores'] = form_dropdown('id_asesor[]',array_combos($this->mcat->get('operadores','nombre'),'nombre','nombre',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_asesor"');

   $data['drop_status_tecnico'] = form_dropdown('id_status_tecnico[]',array_combos($this->mcat->get('cat_status_citas_tecnicos','status'),'id','status',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_status_tecnico"');

   $data['sinmenu'] = true;
    $this->blade->render('filtro_citas',$data);
  }
  public function exportar_citas(){
    $citas = $this->mc->getCitasPaginador(true,'','',$this->input->post('tipo'),true);
    $datos_citas = array();
    foreach ($citas as $key => $value) {
        $datos_citas[$value->asesor][] = $value;
    }
    $datos['citas'] = $datos_citas;
    $datos['fecha'] = $this->input->post('finicio');
    $vista=$this->blade->render('citas/reporte_citas_asesor',$datos,TRUE);
    pdf_create_horizontal($vista, 'reporte_reservaciones_'.date2sql($this->input->post('finicio')));
  }
  public function contacto_proactivo(){
    $this->reagendar('proactivo');
  }
  public function reagendar($tipo_reagendar_proact='reagendar'){
    if(!PermisoModulo('citas','reagendar')){

      redirect(site_url('citas/ver_citas'));
    }
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
      redirect('login');
    }
    //Obtiene las citas que no llegaron para reagendarlas
    $data['tabla'] = $this->mc->buscar_citas_reagendar($tipo_reagendar_proact);
    $tipos = array('0' =>'Normal','1'=>'Reagendada');
    $data['drop_tipo'] = form_dropdown('tipo[]',$tipos,"",'class="form-control multiple" multiple="multiple" id="tipo"');

    $data['drop_id_status_cita'] = form_dropdown('id_status_cita[]',array_combos($this->mcat->get('cat_status_citas','status'),'id','status',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_status_cita"');

    $data['drop_tecnicos'] = form_dropdown('id_tecnico[]',array_combos($this->mcat->get('tecnicos','nombre'),'id','nombre',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_tecnico"');

    $data['drop_asesores'] = form_dropdown('id_asesor[]',array_combos($this->mcat->get('operadores','nombre'),'nombre','nombre',TRUE,FALSE,FALSE),"",'class="form-control multiple" multiple="multiple" id="id_asesor"');

    $tipos_cancelada = array('1' =>'Canceladas');
    $data['drop_tipo_canceladas'] = form_dropdown('cancelada[]',$tipos_cancelada,"",'class="form-control multiple" multiple="multiple" id="cancelada"');

    $origen = array('0' =>'Panel','2'=>'Tablero');
    $data['drop_origen'] = form_dropdown('origen[]',$origen,"",'class="form-control multiple" multiple="multiple" id="origen"');


    $data['citas'] = $this->mc->getCitasByStatus();

    $data['input_placas'] = form_input('placas','','class="form-control" rows="5" id="placas" ');

    $data['input_cliente'] = form_input('cliente','','class="form-control" rows="5" id="cliente" ');
    $data['tipo_reagendar_proact'] = $tipo_reagendar_proact; // saber si es proactivo o reagendar
    $this->blade->render('lista_citas_reagendar',$data);
  }
   public function buscar_citas_reagendar(){
    echo $this->mc->buscar_citas_reagendar($this->input->post('tipo_reagendar_proact'));

  }
   public function reagendar_cita($id=0){ // el segundo parámetro es para saber si reagendó 0=no, 1 = si
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }

    if($this->input->post()){
      $this->form_validation->set_rules('citas[asesor]', 'asesor', 'trim|required');
      $this->form_validation->set_rules('citas[horario]', 'horario', 'trim|required');
      $this->form_validation->set_rules('citas[fecha]', 'fecha', 'trim|required');


        if($this->input->post('tecnico_dias')!=''){
          $this->form_validation->set_rules('fecha_inicio', 'fecha inicio', 'trim');
          $this->form_validation->set_rules('fecha_fin', 'fecha fin', 'trim');
        }

       if ($this->form_validation->run()){
          $this->mc->reagendar_cita();
          }else{
             $errors = array(
                  'asesor' => form_error('citas[asesor]'),
                  'fecha' => form_error('citas[fecha]'),
                  'horario' => form_error('citas[horario]'),
             );
            echo json_encode($errors); exit();
          }
      }
      $info= $this->mc->getCitaId($id);
      $info=$info[0];
      $origen = $info->origen;

      $info_horario= $this->mc->getHorarioId($info->id_horario);
      if(count($info_horario)>0){
        $info_horario=$info_horario[0];

        $id_horario = $info->id_horario;
        $realizo_servicio = $info->realizo_servicio;
        $fecha_verificar = $info_horario->fecha;
      }else{
        $info_horario=new Stdclass();
        $id_horario = 0;
        $realizo_servicio = 0;
        $fecha_verificar = date('Y-m-d');
      }
      $tecnico_actual = $info->id_tecnico;
      $dia_completo = 0;

      $data['input_id'] = $id;
      $data['input_id_horario'] = $id_horario;
      $data['input_id_tecnico_actual'] = $tecnico_actual;
      $data['fecha_verificar'] = $fecha_verificar;

      // 1viene desde el formulario, 2 desde el tablero
      $data['drop_transporte'] = form_dropdown('citas[transporte]',array_combos($this->mcat->get('cat_transporte','transporte'),'id','transporte',TRUE),set_value('transporte',exist_obj($info,'transporte')),'class="form-control" id="transporte" disabled');

      $email_info = set_value('email',exist_obj($info,'email'));
      if($email_info ==''){
        $email_info = 'notienecorreo@notienecorreo.com.mx';
      }

       $data['input_email'] = form_input('citas[email]',$email_info,'class="form-control minusculas" rows="5" id="email" disabled ');

      $data['drop_vehiculo_anio'] = form_dropdown('citas[vehiculo_anio]',array_combos($this->mcat->get('cat_anios','anio','desc'),'anio','anio',TRUE),set_value('vehiculo_anio',exist_obj($info,'vehiculo_anio')),'class="form-control busqueda" id="vehiculo_anio" disabled');

       $data['drop_vehiculo_marca'] = form_dropdown('citas[vehiculo_marca]',array_combos($this->mcat->get('cat_marca','marca'),'marca','marca',TRUE),set_value('vehiculo_marca',exist_obj($info,'vehiculo_marca')),'class="form-control" id="vehiculo_marca" disabled');

        $data['drop_vehiculo_modelo'] = form_dropdown('citas[vehiculo_modelo]',array_combos($this->mcat->get('cat_modelo','modelo'),'modelo','modelo',TRUE),set_value('vehiculo_modelo',exist_obj($info,'vehiculo_modelo')),'class="form-control busqueda" id="vehiculo_modelo" disabled');

       $data['drop_vehiculo_version'] = form_dropdown('citas[vehiculo_version]',array_combos($this->mcat->get('cat_version','version'),'version','version',TRUE),set_value('vehiculo_version',exist_obj($info,'vehiculo_version')),'class="form-control" id="vehiculo_version" disabled');

        $data['input_vehiculo_placas'] = form_input('citas[vehiculo_placas]',set_value('vehiculo_placas',exist_obj($info,'vehiculo_placas')),'class="form-control" rows="5" id="vehiculo_placas" disabled ');

        $data['input_vehiculo_numero_serie'] = form_input('citas[vehiculo_numero_serie]',set_value('vehiculo_numero_serie',exist_obj($info,'vehiculo_numero_serie')),'class="form-control" rows="5" id="vehiculo_numero_serie" disabled');

        $data['input_comentarios'] = form_textarea('citas[comentarios_servicio]',set_value('comentarios_servicio',exist_obj($info,'comentarios_servicio')),'class="form-control" id="comentarios_servicio" rows="2" disabled');

        //echo set_value('asesor',exist_obj($info,'asesor'));die();
        if(isset($info_horario->fecha)){
          $fecha = $info_horario->fecha;
        }else{
          $fecha = date('Y-m-d');
        }
        $id_asesor=$this->mc->getIdAsesor(exist_obj($info,'asesor'));
          $opciones_fecha = array_combos_fechas($this->mc->getFechasAsesor($id_asesor,$fecha),'fecha','fecha',TRUE);
        $disabled = '';

        $data['drop_fecha'] = form_dropdown('citas[fecha]',$opciones_fecha,set_value('fecha',exist_obj($info_horario,'fecha')),'class="form-control" id="fecha" ');

        $data['drop_asesor'] = form_dropdown('citas[asesor]',array_combos($this->mcat->get('operadores','nombre','',array('activo'=>1)),'nombre','nombre',TRUE),set_value('asesor',exist_obj($info,'asesor')),'class="form-control" id="asesor" ');

        $id_asesor=$this->mc->getIdAsesor(exist_obj($info,'asesor'));
          //echo $this->db->last_query();die();
        $horario_id = set_value('id',exist_obj($info_horario,'id'));
        if($origen==1 || $origen==0){
           $opciones_horario = array_combos($this->mc->getHorariosAsesor_edit($id_asesor,set_value('fecha',exist_obj($info_horario,'fecha')),$horario_id),'id','hora',TRUE);
        }else{
          $opciones_horario = array();
        }


        $data['drop_horario'] = form_dropdown('citas[horario]',$opciones_horario,set_value('id',exist_obj($info_horario,'id')),'class="form-control" id="horario" ');


        $data['input_datos_nombres'] = form_input('citas[datos_nombres]',set_value('datos_nombres',exist_obj($info,'datos_nombres')),'class="form-control"  id="datos_nombres" disabled ');

        $data['input_datos_apellido_paterno'] = form_input('citas[datos_apellido_paterno]',set_value('datos_apellido_paterno',exist_obj($info,'datos_apellido_paterno')),'class="form-control"  id="datos_apellido_paterno" disabled ');

         $data['input_datos_apellido_materno'] = form_input('citas[datos_apellido_materno]',set_value('datos_apellido_materno',exist_obj($info,'datos_apellido_materno')),'class="form-control"  id="datos_apellido_materno" disabled ');

          $data['input_datos_telefono'] = form_input('citas[datos_telefono]',set_value('datos_telefono',exist_obj($info,'datos_telefono')),'class="form-control"  id="datos_telefono" maxlength="10" disabled ');

          $data['drop_servicio'] = form_dropdown('citas[servicio]',array_combos($this->mcat->get('cat_servicios','servicio'),'servicio','servicio',TRUE),set_value('servicio',exist_obj($info,'servicio')),'class="form-control" id="servicio" disabled');

        $fecha = $info_horario->fecha;

        $data['drop_opcion'] = form_dropdown('citas[id_opcion_cita]',array_combos($this->mcat->get('cat_opcion_citas','opcion'),'id','opcion',TRUE),set_value('id_opcion_cita',exist_obj($info,'id_opcion_cita')),'class="form-control" id="id_opcion_cita" disabled');

        $data['drop_color'] = form_dropdown('citas[id_color]',array_combos($this->mcat->get('cat_colores','color'),'id','color',TRUE),set_value('id_color',exist_obj($info,'id_color')),'class="form-control busqueda" id="id_color" disabled');

        //if($id!=0){
          $data['drop_tecnicos'] = form_dropdown('tecnico',array_combos($this->mc->getTecnicosByFechaCita($fecha),'id_tecnico','nombre',TRUE),set_value('id_tecnico',exist_obj($info,'id_tecnico')),'class="form-control" id="tecnicos" '.$disabled.' ');
        //}
      $data['btn_guardar'] = form_button('btnguardar','Guardar cita',' id="guardar" class="btn btn-success btn-block pull-right"');

      $data['input_hora_inicio'] = form_input('hora_inicio',set_value('hora_inicio',exist_obj($info,'hora_inicio')),'class="form-control" id="hora_inicio" ');

      $data['input_hora_fin'] = form_input('hora_fin',set_value('hora_fin',exist_obj($info,'hora_fin')),'class="form-control" id="hora_fin" ');

      $data['input_dia_completo'] = form_checkbox('dia_completo',$dia_completo,($dia_completo==1)?'checked':'','class="" id="dia_completo" ');

      //Cuando son días completos
      $data['input_fecha_inicio'] = form_input('fecha_inicio',$this->input->post('fecha'),'class="form-control" id="fecha_inicio" ');

      $data['input_fecha_fin'] = form_input('fecha_fin',set_value('fecha_fin',exist_obj($info,'fecha_fin')),'class="form-control" id="fecha_fin" ');

      $data['drop_tecnicos_dias'] = form_dropdown('tecnico_dias',array_combos($this->mc->getTecnicosByFechaCita($fecha),'id_tecnico','nombre',TRUE),set_value('id_tecnico',exist_obj($info,'id_tecnico')),'class="form-control" id="tecnicos_dias" '.$disabled.' ');

      $data['input_fecha_parcial'] = form_input('fecha_parcial',set_value('fecha_inicio',exist_obj($info,'fecha_parcial')),'class="form-control" id="fecha_parcial" ');

       $data['input_hora_inicio_extra'] = form_input('hora_inicio_extra',set_value('hora_inicio',exist_obj($info,'hora_inicio')),'class="form-control" id="hora_inicio_extra" ');

      $data['input_hora_fin_extra'] = form_input('hora_fin_extra',set_value('hora_fin',exist_obj($info,'hora_fin')),'class="form-control" id="hora_fin_extra" ');

      //si es por día completo saber cuando empieza
      $data['input_hora_comienzo'] = form_input('hora_comienzo',set_value('hora_inicio_dia',exist_obj($info,'hora_inicio_dia')),'class="form-control" id="hora_comienzo" ');

       $data['drop_id_status_color'] = form_dropdown('citas[id_status_color]',array_combos($this->mcat->get('estatus','nombre'),'id','nombre',TRUE),set_value('id_status_color',exist_obj($info,'id_status_color')),'class="form-control busqueda" id="id_status_color" disabled');

      $this->blade->render('reagendar_cita',$data);
  }
  public function comentarios_reagendar(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
       if($this->input->post()){
       $this->form_validation->set_rules('comentario', 'comentario', 'trim|required');
       if ($this->form_validation->run()){

          $cliente = $this->mc->getClienteCita($this->input->post('id_cita'));
          $mensaje_sms = "FORD PLASENCIA! C. Proactivo #CITA ".$this->input->post('id_cita').' Fecha: '.date_eng2esp_1($this->input->post('cronoFecha')).' '.$this->input->post('cronoHora');

          //$celular_sms = '5525710867';
          //$this->sms_general($celular_sms,$mensaje_sms);
          $this->mc->saveComentarioReagendar();
          exit();
          }else{
             $errors = array(
                  'comentario' => form_error('comentario'),
             );
            echo json_encode($errors); exit();
          }
      }
      $data['id_cita'] = $this->input->get('id_cita');
      $data['input_comentario'] = form_textarea('comentario',"",'class="form-control" id="comentario" rows="2"');
      $data['tipo_reagendar_proact_com'] = $this->input->get('tipo_reagendar_proact');
      $this->blade->render('citas/comentario_reagendar',$data);
  }
  public function historial_comentarios_reagendar(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $data['comentarios'] = $this->mc->getComentariosReagendar($this->input->get('id_cita'));
    $data['tipo_reagendar_proact'] = $this->input->get('tipo_reagendar_proact');
     $this->blade->render('citas/historial_comentario_reagendar',$data);
  }
  public function historial_comentarios_reagendar_magic(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $data['comentarios'] = $this->mc->getComentariosReagendar_magic($this->input->get('id_cita'));
    $data['tipo_reagendar_proact'] = $this->input->get('tipo_reagendar_proact');
     $this->blade->render('citas/historial_comentario_reagendar_magic',$data);

  }
  public function historial_comentarios_proactivo_inicial(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $data['comentarios'] = $this->mc->getComentariosProactivoInicial($this->input->get('id_cita'));
    $data['tipo_reagendar_proact'] = $this->input->get('tipo_reagendar_proact');
     $this->blade->render('citas/historial_comentario_reagendar_magic',$data);

  }
 public function prueba(){
    $this->blade->render('prueba');
  }
  public function anexarVideo(){
    if($this->input->post()){
      $id_cita = $this->input->post('id');
      $hayCita = $this->mc->getCitaId($id_cita);
      if(count($hayCita)==0){
        echo 0;die();
      }else{
        echo 1;die();
      }
    }
    $this->blade->render('subir_video');
  }
  public function saveVideoWeb(){
       $img_path="";
       if($_FILES['video']['name'] != ''){

          $name = date('dmyHis').'_'.str_replace(" ", "", $_FILES['video']['name']);
          $path_to_save = 'videos/'.date('Hms');
          if(!file_exists($path_to_save)){
            mkdir($path_to_save, 0777, true);
           }
           $img_path = $path_to_save."/video".$name;
           move_uploaded_file($_FILES['video']['tmp_name'], $img_path);

       }
     $datos = array('video' => $img_path,
                    'id_cita' => $this->input->post('id_cita'),
                    'comentario' => $this->input->post('comentario'),
                    'fecha_creacion'=>date('Y-m-d H:i:s')
      );

     $this->db->insert('evidencia',$datos);
     redirect('citas/evidencia');

  }
  public function saveVideo(){
       $img_path="";
       if($_FILES['video']['name'] != ''){

          $name = date('dmyHis').'_'.str_replace(" ", "", $_FILES['video']['name']);
          $path_to_save = 'videos/'.date('Hms');
          if(!file_exists($path_to_save)){
            mkdir($path_to_save, 0777, true);
           }
           $img_path = $path_to_save."/video".$name;
           move_uploaded_file($_FILES['video']['tmp_name'], $img_path);

       }
     $datos = array('video' => $img_path,
                    'id_cita' => $this->input->post('id_cita'),
                    'comentario' => $this->input->post('comentario'),
                    'fecha_creacion'=>date('Y-m-d H:i:s')
      );

     $this->db->insert('evidencia',$datos);
    echo json_encode(array('Exito'=>true,'mensaje'=>'Evidencia guardada con éxito'));
    die();

  }
  //Api para el ingreso
  public function ingreso()
    {

      //Parametros por post : adminUsername,password
      $q = $this->db->where('adminUsername',$this->input->post('adminUsername'))->where('adminPassword',$this->input->post('adminPassword'))->get(CONST_BASE_PRINCIPAL.'admin');
      if($q->num_rows()==1){
       echo json_encode(array('Exito'=>true,'id'=>$q->row()->adminId));
      }
      else{
        echo json_encode(array('Exito'=>false,'id'=>0));
      }
      exit();
  }
  public function evidencia(){
    //Obtiene las citas que no llegaron para reagendarlas
    $data['citas'] = $this->mc->getCitasEvidencia();
    $this->blade->render('evidencia_citas',$data);
  }
  public function ver_video(){
    $data['video'] = $this->input->get('video');
    $data['tipo'] = $this->input->get('tipo'); //1 es video, 2 imagen
    $this->blade->render('video',$data);
  }
  //rechazar o confirmar cita
  public function confirmar_rechazar(){
    if($this->db->where('id_cita',$this->input->post('id'))->set('confirmada',$this->input->post('valor'))->update('citas')){
      echo 1;
    }else{
      echo 0;
    }
  }
  public function prueba3(){
    $hora = '08:00';
    $timestamp = strtotime($hora) + 1*60;
    $hora = date('H:i', $timestamp).':00';
      $query = "select * FROM tecnicos_citas where ('2018-03-22' BETWEEN fecha and fecha_fin) and id_tecnico = 17 and activo=1";
     //echo $query;die();
    $res = $this->db->query($query)->result_array();
    $contador = 0;
    $id_cita = 0;
    debug_var($res);
    foreach ($res as $key => $value) {
      if($value['fecha']=='2018-03-22'){
        if($hora>$value['hora_inicio_dia'] && $hora<$value['hora_fin']){
          echo 'si es';
          $id_cita = $value['id_cita'];
          $contador ++;
        }
      }else{
        if($hora>$value['hora_inicio'] && $hora<$value['hora_fin']){
          echo 'si es';
          $id_cita = $value['id_cita'];
          $contador ++;
        }
      }
    }
    if($contador>0){
      echo '1-'.$id_cita.'<br>';
    }else{
      echo '0-0'.'<br>';
    }
  }
  public function prueba4($hora='08:00',$fecha='2018-03-22',$id_tecnico=17){
    $timestamp = strtotime($hora) + 1*60;
    $hora = date('H:i', $timestamp).':00';
      $query = "select * FROM tecnicos_citas where ('".$fecha."' BETWEEN fecha and fecha_fin)  and id_tecnico = ".$id_tecnico." and activo=1";
    //echo $query;die();
    $res = $this->db->query($query)->result_array();
    $id_cita = 0;
     $bandera = 0;
     debug_var($res);
    foreach ($res as $key => $value) {
      var_dump($value['fecha'],"'".$fecha."'");
      if("'".$value['fecha']."'"=="'".$fecha."'"){
        if($hora>$value['hora_inicio_dia'] && $hora<$value['hora_fin']){
          $id_cita = $value['id_cita'];
          $bandera=1;
        }
      }else{
        if($hora>$value['hora_inicio'] && $hora<$value['hora_fin']){
          $id_cita = $value['id_cita'];
          $bandera=1;
        }
      }
    }
    if($bandera>0){
      echo '1-'.$id_cita.'-'.$bandera;
    }else{
      echo '0-0'.'-'.$bandera;
    }
  }
  /* *****************SECCIÓN DE PARÁMETROS ************************************ */
  public function menu_parametros(){
    $this->blade->render('menu_parametros');
  }
  public function parametros(){
    $tiempo_aumento = $this->mc->getParametrosTiempoActual();
    //echo $this->db->last_query();die();
    $opciones = array('30' =>'30' ,'20'=>'20','15'=>'15' );
    $datos['tiempo_aumento'] = form_dropdown('tiempo_aumento',$opciones,$tiempo_aumento,'class="form-control" id="tiempo_aumento"');

    $datos['fecha_tecnicos'] = $this->mc->getLastDateTecnico();
    $datos['fecha_asesores'] = $this->mc->getLastDateAsesor();
    $datos['valor'] = $this->getValor($tiempo_aumento);
    $this->blade->set_data($datos)->render('parametros');
  }
   public function generarhorariosAuxFormulario(){
    ini_set('max_execution_time', 100000);
    $operadores = $this->db->get('operadores')->result();
    $fecha = $this->input->post('fecha_asesores');
    $q = $this->db->select('id')->from('parametros')->limit(1)->order_by('hasta','desc')->get();
    if($q->num_rows()==1){
      $id_actualizar = $q->row()->id;
    }else{
      $id_actualizar = 0;
    }
    $nuevafecha = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
    //Actualizar la fecha del parámetro anterior
    $this->db->where('id',$id_actualizar)->set('hasta',date ( 'Y-m-j',$nuevafecha ))->update('parametros');
    //Insertar el nuevo registro de parámetro
    $this->db->set('tiempo_cita',$this->input->post('tiempo_aumento'))->set('desde',$fecha)->set('hasta','2035-12-31')->insert('parametros');
    $this->db->where('fecha>=',$fecha)->delete('aux');
      foreach ($operadores as $key => $value) {
        $fecha = $this->input->post('fecha_asesores');
        while($fecha<'2019-12-31'){
          $time = '07:00';
          $contador=0;

          while($contador<$this->input->post('valor')){
            $fecha_parcial = explode('-', $fecha);
            $aux = array('hora' => $time,
                       'id_operador' =>$value->id,
                       'fecha_creacion'=>$fecha_parcial[0].'-'.$fecha_parcial[1].'-'.$fecha_parcial[2],
                        'dia' => $fecha_parcial[2],
                        'mes' => $fecha_parcial[1],
                        'anio' => $fecha_parcial[0],
                        'fecha'=>$fecha,
                        'ocupado'=>0,
                        'activo'=>1,
                        'id_status_cita'=>1

            );
            $timestamp = strtotime($time) + $this->input->post('tiempo_aumento')*60;
            $time = date('H:i', $timestamp);
            $contador++;
            $this->db->insert('aux',$aux);
          }
          $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
          $fecha = date ( 'Y-m-d' , $nuevafecha );
        }


      }
      echo 1;die();
  }
  public function parametros_horarios_tecnicos(){
    $where = array('activo',1,'baja_definitiva'=>0);
    $data['drop_tecnicos'] = form_dropdown('id_tecnico',array_combos($this->mcat->get('tecnicos','nombre','',$where),'id','nombre',TRUE),"",'class="form-control" id="id_tecnico" ');
    $this->blade->render('citas/parametros_horarios_tecnicos',$data);
  }
  //Función para obtener a partir de que día se pueden generar horarios para los técnicos
  public function getDateByTecnico($idtecnico=''){
    echo $this->mc->getDateByTecnico($this->input->post('id_tecnico'));
  }
  public function saveHorariosByTecnico(){
    ini_set('max_execution_time', 30000);
    $fecha = $this->input->post('fecha');
    $id_tecnico = $this->input->post('id_tecnico');
    $nuevafecha = strtotime ( '+1 year' , strtotime ( $fecha ) ) ;
    $fecha_hasta = date ( 'Y-m-d' , $nuevafecha );
    //var_dump($fecha_hasta);die();
    while($fecha<$fecha_hasta){
     $dia = date('l', strtotime($fecha));
      if($dia!='Sunday'){

       if($dia=='Saturday'){
          $hora_inicio = $this->input->post('his');
          $hora_fin = $this->input->post('hfs');
          $hora_inicio_comida = $this->input->post('comidasai');
          $hora_fin_comida =$this->input->post('comidasaf');
        }else{
          $hora_inicio = $this->input->post('hilv');
          $hora_fin = $this->input->post('hflv');
          $hora_inicio_comida = $this->input->post('comidailv');
          $hora_fin_comida = $this->input->post('comidaflv');
        }
        $citas = array('fecha' => $fecha,
                       'hora_inicio' =>$hora_inicio,
                       'hora_fin' => $hora_fin,
                       'hora_inicio_comida' =>$hora_inicio_comida,
                       'hora_fin_comida' =>$hora_fin_comida,
                       'fecha_creacion'=>date('Y-m-d'),
                       'id_tecnico'=>$id_tecnico,
                       'activo'=>1

         );
          $this->db->insert('horarios_tecnicos',$citas);


      }else{ //es domingo
          if($this->input->post('hid')!='' && $this->input->post('hfd') !='' ){
            $hora_inicio = $this->input->post('hid');
            $hora_fin = $this->input->post('hfd');
            $hora_inicio_comida = $this->input->post('comidadoi');
            $hora_fin_comida = $this->input->post('comidadof');

            $citas = array('fecha' => $fecha,
                       'hora_inicio' =>$hora_inicio,
                       'hora_fin' => $hora_fin,
                       'hora_inicio_comida' =>$hora_inicio_comida,
                       'hora_fin_comida' =>$hora_fin_comida,
                       'fecha_creacion'=>date('Y-m-d'),
                       'id_tecnico'=>$id_tecnico,
                       'activo'=>1

            );
            $this->db->insert('horarios_tecnicos',$citas);
          }

      }
          $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
          $fecha = date ( 'Y-m-d' , $nuevafecha );
      } //while
      echo 1;die();
  }
  public function cambiar_horarios(){
    $data['drop_tecnicos'] = form_dropdown('id_tecnico',array_combos($this->mcat->get('tecnicos','nombre'),'id','nombre',TRUE),"",'class="form-control" id="id_tecnico" ');
    $this->blade->render('citas/cambiar_horarios',$data);
  }

  public function updateHorariosByTecnico(){
    $horarios = $this->db->where('fecha >=',date2sql($this->input->post('fecha')))->where('id_tecnico',$this->input->post('id_tecnico'))->get('horarios_tecnicos')->result();
      //echo $this->db->last_query();die();
      $hilv = $this->input->post('hilv');
      $hflv = $this->input->post('hflv');
      $comidailv = $this->input->post('comidailv');
      $comidaflv = $this->input->post('comidaflv');
      $his = $this->input->post('his');
      $hfs = $this->input->post('hfs');
      $comidasai = $this->input->post('comidasai');
      $comidasaf = $this->input->post('comidasaf');
      $hid = $this->input->post('hid');
      $hfd = $this->input->post('hfd');
      $comidadoi = $this->input->post('comidadoi');
      $comidadof = $this->input->post('comidadof');
    foreach ($horarios as $h => $value) {
       $dia = date('l', strtotime($value->fecha));

       if($dia=='Saturday'){
        $this->db->where('id',$value->id)->set('hora_inicio',$his)->set('hora_fin',$hfs)->set('hora_inicio_comida',$comidasai)->set('hora_fin_comida',$comidasaf)->update('horarios_tecnicos');

       }else if($dia=='Sunday'){
        if($hid=='' && $hfd==''){
           $this->db->where('id',$value->id)->delete('horarios_tecnicos');
         }else{
            $this->db->where('id',$value->id)->set('hora_inicio',$hid)->set('hora_fin',$hfd)->set('hora_inicio_comida',$comidadoi)->set('hora_fin_comida',$comidadof)->update('horarios_tecnicos');
          }

       }else{
          $this->db->where('id',$value->id)->set('hora_inicio',$hilv)->set('hora_fin',$hflv)->set('hora_inicio_comida',$comidailv)->set('hora_fin_comida',$comidaflv)->update('horarios_tecnicos');
          //echo $this->db->last_query();die();
       }

    }
    echo 1;die();
    // Pendiente por autorización (#FA58F4) ... morado mas light (#8181F7)
  }

  /* *****************FIN SECCIÓN DE PARÁMETROS ************************************ */
  public function cambiar_comentario_servicio(){
    if($this->input->post()){
        $this->form_validation->set_rules('comentario', 'comentario', 'trim|required');
        $this->form_validation->set_rules('vehiculo_numero_serie', 'número de serie', 'trim|required');
        $this->form_validation->set_rules('email', 'correo electrónico', 'trim|valid_email|required');
       if ($this->form_validation->run()){
            $datos = array('comentarios_servicio' => $this->input->post('comentario'),
                           'vehiculo_placas' => $this->input->post('vehiculo_placas'),
                           'vehiculo_numero_serie' => $this->input->post('vehiculo_numero_serie'),
                           'vehiculo_kilometraje' => $this->input->post('vehiculo_kilometraje'),
                           'email' =>strtolower($this->input->post('email')),

            );
            $this->db->where('id_cita',$this->input->post('id'))->update('citas',$datos);
            echo 1;die();
          }else{
             $errors = array(
                  'comentario' => form_error('comentario'),
                  'numero_serie' => form_error('vehiculo_numero_serie'),
                  'email' => form_error('email'),
             );
            echo json_encode($errors); exit();
          }
      }
  }
   public function menu(){
    $this->blade->render('citas/menu');
   }
  public function cancelar_cita(){
    $idhorario = $this->mc->getIdHorarioActual($this->input->post('id'));
    $this->db->where('id_cita',$this->input->post('id'))->set('id_status',1)->set('cancelada',1)->update('citas');

    $this->db->where('id_cita',$this->input->post('id'))->set('historial',1)->update('tecnicos_citas');
    $this->db->where('id_cita',$this->input->post('id'))->set('historial',1)->update('citas');
    $this->db->where('id',$idhorario)->set('ocupado',0)->update('aux');

    echo 1;die();

  }
  public function getDatosByPlaca(){
    $q = $this->db->where('vehiculo_placas',$this->input->post('placas'))->limit(1)->get('citas');
    if($q->num_rows()==1){
      echo json_encode(array('exito'=>true,'data'=>$q->row()));
    }else{
      echo json_encode(array('exito'=>false));
    }
    exit();

  }
  public function getDatosBySerie(){
    $q = $this->db->where('vehiculo_numero_serie',$this->input->post('serie'))->limit(1)->get('citas');
    if($q->num_rows()==1){
      echo json_encode(array('exito'=>true,'data'=>$q->row()));
    }else{
      echo json_encode(array('exito'=>false));
    }
    exit();

  }
  public function proactivo_historial($proactivo=false)
  {
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $this->blade->render('proactivo_historial',array('proactivo'=>$proactivo));
  }

  public function getDatosDt(){
    $datos = $_POST;
    $mismo = $datos['draw'];
    $pagina = $datos['start'];
    $porpagina = $datos['length'];
    if(isset($_POST['buscar_campo'])){
      $busqueda = $_POST['buscar_campo'];
      $this->filtros($busqueda);
    }
    if(!empty($_POST['fecha_inicio']) ||!empty($_POST['fecha_fin'])){
       $this->filtros_proactivo_historial_fechas();
    }
    $total = $this->db->select('count(id) as total')->get('magic')->row()->total;
    if(isset($_POST['buscar_campo'])){
      $busqueda = $_POST['buscar_campo'];
      $this->filtros($busqueda);
    }
    if(!empty($_POST['fecha_inicio']) ||!empty($_POST['fecha_fin'])){
       $this->filtros_proactivo_historial_fechas();
    }
    $info = $this->db->limit($porpagina,$pagina)->get('magic')->result();
    $data = array();
    foreach ($info as $key => $value) {
      if($_POST['proactivo']){
        $dat = array();
            $dat[] = $value->id ;
            $dat[] = $value->nombre ;
            $dat[] = $value->ap ;
            $dat[] = $value->am ;
            $dat[] = $value->placas ;
            $dat[] = $value->telefono ;
            $dat[] = $value->tel_secundario ;
            $dat[] = $value->correo ;
        $acciones = '';
        $acciones.= '<a href="citas/agregar_magic/'.$value->id.'" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa pe pe-7s-note"></i></a>';

       $acciones.= '<a href="" data-id="'.$value->id.'" class="pe-7s-comment js_comentarios" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Comentarios"></a>
        <a href="" data-id="'.$value->id.'" class="js_historial" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Historial comentarios"><i class="pe-7s-info
pe pe-7s-info"></i></a>';


        $dat[] = $acciones;
        $data[] = $dat;
      }else{
        $dat = array();
            $dat[] = $value->id ;
            $dat[] = $value->serie ;
            $dat[] = $value->fecha ;
            $dat[] = $value->nombre_completo;
            $dat[] = $value->contacto ;
            $dat[] = $value->direccion ;
            $dat[] = $value->numero ;
            $dat[] = $value->colonia ;
            $dat[] = $value->estado ;
            $dat[] = $value->poblacion;
            $dat[] = $value->telefono ;
            $dat[] = $value->celular ;
            $dat[] = $value->rfc ;
            $dat[] = $value->correo ;
            $dat[] = $value->correo2 ;
            $dat[] = $value->observaciones ;
            $dat[] = $value->asesor ;
            $dat[] = $value->placas ;
            $dat[] = $value->auto ;
            $dat[] = $value->color ;
            $dat[] = $value->kilometraje ;
            $dat[] = $value->cilindros ;
            $dat[] = $value->modelo ;
            $dat[] = $value->motor ;
            $dat[] = $value->transmision ;
            $dat[] = $value->nombre ;
            $dat[] = $value->ap ;
            $dat[] = $value->am ;
            $dat[] = $value->ordent ;

        $acciones = '';
         $acciones.= '<a href="citas/agendar_cita/0/0/0/'.$value->id.'" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Agendar cita"><i class="pe pe-7s-plus"></i></a>';
        $acciones.= '<a href="" data-id="'.$value->id.'" class=" js_comentarios" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Comentarios"><i class="pe-7s-comment"></i></a>
    
        <a href="" data-id="'.$value->id.'" class="js_historial" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Historial comentarios"><i class="pe-7s-info"></i></a>

        <a href="" data-id="'.$value->id.'" class="js_info" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Información del cliente"><i class="pe-7s-id"></i></a>';

        $dat[] = $acciones;
        $data[] = $dat;
      }

    }

    $retornar = array('draw' => intval($mismo),
                      'recordsTotal' => intval($total),
                      'recordsFiltered' => intval($total),
                      'data' => $data);
    $this->output->set_content_type('application/json')->set_output(json_encode($retornar));
  }
  public function filtros($busqueda='',$letra_orden=''){
    if($busqueda!=''){
      if($letra_orden==''){
        $this->db->or_like('id',$busqueda);
        $this->db->or_like('serie',$busqueda);
        $this->db->or_like('fecha',$busqueda);
        $this->db->or_like('nombre_completo',$busqueda);
        $this->db->or_like('contacto',$busqueda);
        $this->db->or_like('direccion',$busqueda);
        $this->db->or_like('numero',$busqueda);
        $this->db->or_like('colonia',$busqueda);
        $this->db->or_like('estado',$busqueda);
        $this->db->or_like('poblacion',$busqueda);
        $this->db->or_like('telefono',$busqueda);
        $this->db->or_like('celular',$busqueda);
        $this->db->or_like('rfc',$busqueda);
        $this->db->or_like('correo',$busqueda);
        $this->db->or_like('correo2',$busqueda);
        $this->db->or_like('observaciones',$busqueda);
        $this->db->or_like('asesor',$busqueda);
        $this->db->or_like('placas',$busqueda);
        $this->db->or_like('auto',$busqueda);
        $this->db->or_like('color',$busqueda);
        $this->db->or_like('kilometraje',$busqueda);
        $this->db->or_like('cilindros',$busqueda);
        $this->db->or_like('modelo',$busqueda);
        $this->db->or_like('motor',$busqueda);
        $this->db->or_like('transmision',$busqueda);
        $this->db->or_like('nombre',$busqueda);
        $this->db->or_like('ap',$busqueda);
        $this->db->or_like('am',$busqueda);
        $this->db->or_like('ordent',$busqueda);
      }else{
        $this->db->or_like('ordent',$letra_orden);
      }
    }


  }
  public function filtros_proactivo_historial_fechas(){
      if($_POST['fecha_inicio']!='' && $_POST['fecha_inicio']!=''){
        $this->db->where('fecha >=',date2sql($_POST['fecha_inicio']));
        $this->db->where('fecha <=',date2sql($_POST['fecha_fin']));
      }elseif($_POST['fecha_inicio']!='' && $_POST['fecha_inicio']==''){
        $this->db->where('fecha >=',date2sql($_POST['fecha_inicio']));
      }elseif($_POST['fecha_inicio']=='' && $_POST['fecha_inicio']!=''){
        $this->db->where('fecha <=',date2sql($_POST['fecha_fin']));
      }
  }
  function agregar_magic($id=0)
    {
      if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
      {
        $this->output->set_status_header('409');
        exit();
      }else if($this->session->userdata('id_usuario')==''){
      redirect('login');
      }

      if($this->input->post()){
        $this->form_validation->set_rules('no_orden', 'no_orden', 'trim|required');
       if ($this->form_validation->run()){
          $this->mc->guardarMagic();
          }else{
             $errors = array(
                  'nombre' => form_error('nombre'),
             );
            echo json_encode($errors); exit();
          }
      }

      if($id==0){
        $info=new Stdclass();


      }else{
        $info= $this->mc->getMagicId($id);
        $info=$info[0];
      }


      $data['input_id'] = $id;

   $data['input_no_orden'] = form_input('no_orden',set_value('no_orden',exist_obj($info,'no_orden')),'class="form-control" id="no_orden"');

   $data['input_placas'] = form_input('placas',set_value('placas',exist_obj($info,'placas')),'class="form-control" id="placas"');

   $data['input_tipo_orden'] = form_input('tipo_orden',set_value('tipo_orden',exist_obj($info,'tipo_orden')),'class="form-control" id="tipo_orden"');

   $data['input_codigoCte'] = form_input('codigoCte',set_value('codigoCte',exist_obj($info,'codigoCte')),'class="form-control" id="codigoCte"');

   $data['input_notorre'] = form_input('notorre',set_value('notorre',exist_obj($info,'notorre')),'class="form-control" id="notorre"');

   $data['input_noasesor'] = form_input('noasesor',set_value('noasesor',exist_obj($info,'noasesor')),'class="form-control" id="noasesor"');

   $data['input_fecha_recepcion'] = form_input('fecha_recepcion',date_eng2esp_1(set_value('fecha_recepcion',exist_obj($info,'fecha_recepcion'))),'class="form-control" id="fecha_recepcion"');

   $data['input_hora_recepcion'] = form_input('hora_recepcion',set_value('hora_recepcion',exist_obj($info,'hora_recepcion')),'class="form-control" id="hora_recepcion"');

   $data['input_tipo_servicio'] = form_input('tipo_servicio',set_value('tipo_servicio',exist_obj($info,'tipo_servicio')),'class="form-control" id="tipo_servicio"');

   $data['input_garantia'] = form_input('garantia',set_value('garantia',exist_obj($info,'garantia')),'class="form-control" id="garantia"');

   $data['input_tipogtia'] = form_input('tipogtia',set_value('tipogtia',exist_obj($info,'tipogtia')),'class="form-control" id="tipogtia"');

   $data['input_cargo_interno'] = form_input('cargo_interno',set_value('cargo_interno',exist_obj($info,'cargo_interno')),'class="form-control" id="cargo_interno"');

   $data['input_km_actual'] = form_input('km_actual',set_value('km_actual',exist_obj($info,'km_actual')),'class="form-control" id="km_actual"');

   $data['input_cliente_problema'] = form_input('cliente_problema',set_value('cliente_problema',exist_obj($info,'cliente_problema')),'class="form-control" id="cliente_problema"');

   $data['input_fecha_entrega_estima'] = form_input('fecha_entrega_estima',date_eng2esp_1(set_value('fecha_entrega_estima',exist_obj($info,'fecha_entrega_estima'))),'class="form-control" id="fecha_entrega_estima"');

   $data['input_hora_entrega_estima'] = form_input('hora_entrega_estima',set_value('hora_entrega_estima',exist_obj($info,'hora_entrega_estima')),'class="form-control" id="hora_entrega_estima"');

   $data['input_fecha_remision'] = form_input('fecha_remision',date_eng2esp_1(set_value('fecha_remision',exist_obj($info,'fecha_remision'))),'class="form-control" id="fecha_remision"');

   $data['input_hora_remision'] = form_input('hora_remision',set_value('hora_remision',exist_obj($info,'hora_remision')),'class="form-control" id="hora_remision"');

   $data['input_fecha_factura'] = form_input('fecha_factura',date_eng2esp_1(set_value('fecha_factura',exist_obj($info,'fecha_factura'))),'class="form-control" id="fecha_factura"');

   $data['input_hora_factura'] = form_input('hora_factura',set_value('hora_factura',exist_obj($info,'hora_factura')),'class="form-control" id="hora_factura"');

   $data['input_nombre'] = form_input('nombre',set_value('nombre',exist_obj($info,'nombre')),'class="form-control" id="nombre"');

   $data['input_ap'] = form_input('ap',set_value('ap',exist_obj($info,'ap')),'class="form-control" id="ap"');

   $data['input_am'] = form_input('am',set_value('am',exist_obj($info,'am')),'class="form-control" id="am"');

   $data['input_razon_social'] = form_input('razon_social',set_value('razon_social',exist_obj($info,'razon_social')),'class="form-control" id="razon_social"');

   $data['input_calle'] = form_input('calle',set_value('calle',exist_obj($info,'calle')),'class="form-control" id="calle"');

   $data['input_numero_exterior'] = form_input('numero_exterior',set_value('numero_exterior',exist_obj($info,'numero_exterior')),'class="form-control" id="numero_exterior"');

   $data['input_numero_interior'] = form_input('numero_interior',set_value('numero_interior',exist_obj($info,'numero_interior')),'class="form-control" id="numero_interior"');

   $data['input_colonia'] = form_input('colonia',set_value('colonia',exist_obj($info,'colonia')),'class="form-control" id="colonia"');

   $data['input_municipio'] = form_input('municipio',set_value('municipio',exist_obj($info,'municipio')),'class="form-control" id="municipio"');

   $data['input_estado'] = form_input('estado',set_value('estado',exist_obj($info,'estado')),'class="form-control" id="estado"');

   $data['input_cp'] = form_input('cp',set_value('cp',exist_obj($info,'cp')),'class="form-control" id="cp"');

   $data['input_tel_principal'] = form_input('tel_principal',set_value('tel_principal',exist_obj($info,'tel_principal')),'class="form-control" id="tel_principal"');

   $data['input_tel_secundario'] = form_input('tel_secundario',set_value('tel_secundario',exist_obj($info,'tel_secundario')),'class="form-control" id="tel_secundario"');

   $data['input_tel_adicional'] = form_input('tel_adicional',set_value('tel_adicional',exist_obj($info,'tel_adicional')),'class="form-control" id="tel_adicional"');

   $data['input_correo'] = form_input('correo',set_value('correo',exist_obj($info,'correo')),'class="form-control" id="correo"');

   $data['input_rfc'] = form_input('rfc',set_value('rfc',exist_obj($info,'rfc')),'class="form-control" id="rfc"');

   $data['input_dirigirse_con'] = form_input('dirigirse_con',set_value('dirigirse_con',exist_obj($info,'dirigirse_con')),'class="form-control" id="dirigirse_con"');

   $data['input_serie_larga'] = form_input('serie_larga',set_value('serie_larga',exist_obj($info,'serie_larga')),'class="form-control" id="serie_larga"');

   $data['input_no_siniestro'] = form_input('no_siniestro',set_value('no_siniestro',exist_obj($info,'no_siniestro')),'class="form-control" id="no_siniestro"');

   $data['input_no_poliza'] = form_input('no_poliza',set_value('no_poliza',exist_obj($info,'no_poliza')),'class="form-control" id="no_poliza"');

   $data['input_flotilla'] = form_input('flotilla',set_value('flotilla',exist_obj($info,'flotilla')),'class="form-control" id="flotilla"');

   $data['input_servicio_express'] = form_input('servicio_express',set_value('servicio_express',exist_obj($info,'servicio_express')),'class="form-control" id="servicio_express"');

   $data['input_servicio_programado'] = form_input('servicio_programado',set_value('servicio_programado',exist_obj($info,'servicio_programado')),'class="form-control" id="servicio_programado"');

   $data['input_fm'] = form_input('fm',set_value('fm',exist_obj($info,'fm')),'class="form-control" id="fm"');

   $data['input_factura'] = form_input('factura',set_value('factura',exist_obj($info,'factura')),'class="form-control" id="factura"');

   $data['input_status'] = form_input('status',set_value('status',exist_obj($info,'status')),'class="form-control" id="status"');

   $data['input_total_mo'] = form_input('total_mo',set_value('total_mo',exist_obj($info,'total_mo')),'class="form-control" id="total_mo"');

   $data['input_total_ref'] = form_input('total_ref',set_value('total_ref',exist_obj($info,'total_ref')),'class="form-control" id="total_ref"');

   $data['input_total_hoj'] = form_input('total_hoj',set_value('total_hoj',exist_obj($info,'total_hoj')),'class="form-control" id="total_hoj"');

   $data['input_total_pin'] = form_input('total_pin',set_value('total_pin',exist_obj($info,'total_pin')),'class="form-control" id="total_pin"');

   $data['input_total_lub'] = form_input('total_lub',set_value('total_lub',exist_obj($info,'total_lub')),'class="form-control" id="total_lub"');

   $data['input_total_oc'] = form_input('total_oc',set_value('total_oc',exist_obj($info,'total_oc')),'class="form-control" id="total_oc"');

   $data['input_total_om'] = form_input('total_om',set_value('total_om',exist_obj($info,'total_om')),'class="form-control" id="total_om"');

   $data['input_total_descuento'] = form_input('total_descuento',set_value('total_descuento',exist_obj($info,'total_descuento')),'class="form-control" id="total_descuento"');

   $data['input_subtotal'] = form_input('subtotal',set_value('subtotal',exist_obj($info,'subtotal')),'class="form-control" id="subtotal"');

   $data['input_iva'] = form_input('iva',set_value('iva',exist_obj($info,'iva')),'class="form-control" id="iva"');

   $data['input_total'] = form_input('total',set_value('total',exist_obj($info,'total')),'class="form-control" id="total"');

   $data['btn_guardar'] = form_button('btnguardar','Guardar cita',' id="guardar" class="btn btn-success btn-block pull-right"');
      $this->blade->render('nuevo_magic',$data);
  }
  public function cambiar_magic(){
    if (function_exists("set_time_limit") == TRUE AND @ini_get("safe_mode") == 0)
    {
        @set_time_limit(100000);
    }
    $magic = $this->db->get('magic2')->result();
    foreach ($magic as $key => $value) {
      $datos = array(
               'fecha_recepcion' => ($value->fecha_recepcion!='')?date2sql($value->fecha_recepcion):'',
               'fecha_entrega_estima' => ($value->fecha_entrega_estima!='')?date2sql($value->fecha_entrega_estima):'',
               'fecha_remision' => ($value->fecha_remision!='')?date2sql($value->fecha_remision):'',
               'fecha_factura' => ($value->fecha_factura!='')?date2sql($value->fecha_factura):'',
               'id'=> $value->id,
               'no_orden' =>$value->no_orden,
               'placas' =>$value->placas,
               'tipo_orden' =>$value->tipo_orden,
               'codigoCte' =>$value->codigoCte,
               'notorre' =>$value->notorre,
               'noasesor' =>$value->noasesor,
               'hora_recepcion' =>$value->hora_recepcion,
               'tipo_servicio' =>$value->tipo_servicio,
               'garantia' =>$value->garantia,
               'tipogtia' =>$value->tipogtia,
               'cargo_interno' =>$value->cargo_interno,
               'km_actual' =>$value->km_actual,
               'cliente_problema' =>$value->cliente_problema,
               'hora_entrega_estima' =>$value->hora_entrega_estima,
               'hora_remision' =>$value->hora_remision,
               'hora_factura' =>$value->hora_factura,
               'nombre' =>$value->nombre,
               'ap' =>$value->ap,
               'am' =>$value->am,
               'razon_social' =>$value->razon_social,
               'calle' =>$value->calle,
               'numero_exterior' =>$value->numero_exterior,
               'numero_interior' =>$value->numero_interior,
               'colonia' =>$value->colonia,
               'municipio' =>$value->municipio,
               'estado' =>$value->estado,
               'cp' =>$value->cp,
               'tel_principal' =>$value->tel_principal,
               'tel_secundario' =>$value->tel_secundario,
               'tel_adicional' =>$value->tel_adicional,
               'correo' =>$value->correo,
               'rfc' =>$value->rfc,
               'dirigirse_con' =>$value->dirigirse_con,
               'serie_larga ' =>$value->serie_larga,
               'no_siniestro' =>$value->no_siniestro,
               'no_poliza' =>$value->no_poliza,
               'flotilla' =>$value->flotilla,
               'servicio_express' =>$value->servicio_express,
               'servicio_programado' =>$value->servicio_programado,
               'fm' =>$value->fm,
               'factura' =>$value->factura,
               'status' =>$value->status,
               'total_mo' =>$value->total_mo,
               'total_ref' =>$value->total_ref,
               'total_hoj' =>$value->total_hoj,
               'total_pin' =>$value->total_pin,
               'total_lub' =>$value->total_lub,
               'total_oc' =>$value->total_oc,
               'total_om' =>$value->total_om,
               'total_descuento ' =>$value->total_descuento,
               'subtotal ' =>$value->subtotal,
               'iva ' =>$value->iva,
               'total' =>$value->total,
      );
    $this->db->insert('magic',$datos);
    }
  }
  //ESTADÍSTICAS
  public function estadisticas(){
    $datos['drop_estatus']=form_dropdown('id_status_asesor',array_combos($this->mcat->get('cat_status_citas'),'id','status',true),"",'class="form-control busqueda" id="id_status_asesor" ');
    $datos['drop_asesores']=form_dropdown('id_asesor',array_combos($this->mcat->get('operadores'),'id','nombre',true),"",'class="form-control busqueda" id="id_asesor" ');
    $datos['drop_estatus_tecnicos']=form_dropdown('id_status',array_combos($this->mcat->get('cat_status_citas_tecnicos'),'id','status',true),"",'class="form-control busqueda" id="id_status" ');
    $datos['drop_tecnicos']=form_dropdown('id_tecnico',array_combos($this->mcat->get('tecnicos'),'id','nombre',true),"",'class="form-control busqueda" id="id_tecnico" ');
    $this->blade->render('panel_estadisticas',$datos);
  }
  public function mostrar_graficas(){
    $fechas = array();
    $datos = $this->mc->getDatosGraficas();
    foreach ($datos as $key => $value) {
      $fechas[] = date2sql($value->fecha);
    }
    $array_final = $this->fecha($fechas);
    if(!isset($array_final['p1'])){
      $datos_vista['p1'] = array(0,0,0,0,0,0,0,0,0,0,0,0);
    }else{
      $datos_vista['p1'] = $array_final['p1'];
    }


    if(!isset($array_final['p2'])){
      $datos_vista['p2'] = array(0,0,0,0,0,0,0,0,0,0,0,0);  ;
    }else{
      $datos_vista['p2'] = $array_final['p2'];
    }


    if(!isset($array_final['p3'])){
      $datos_vista['p3'] = array(0,0,0,0,0,0,0,0,0,0,0,0);  ;
    }else{
      $datos_vista['p3'] =  $array_final['p3'];
    }


    if(!isset($array_final['p4'])){
      $datos_vista['p4'] = array(0,0,0,0,0,0,0,0,0,0,0,0);  ;
    }else{
      $datos_vista['p4'] =  $array_final['p4'];
    }
    $datos_vista['nombre_grafica'] = 'por estatus';

    echo $this->blade->render('graficas_estadisticas', $datos_vista, TRUE);

  }
  public function fecha($fecha = array()){
    $array = array();
    $valorp1 = 0;
    $valorp2 = 0;
    $valorp3 = 0;
    $valorp4 = 0;
    foreach ($fecha as $key => $value) {
      $dia = (int)date("d", strtotime($value));
      $mes = date("n", strtotime($value));
      if($dia>= 1 && $dia <=8){
        //periodo 1
        if(isset($array['p1'][$mes])){
          $valorp1 = $array['p1'][$mes] +1;
          $array['p1'][$mes] = $valorp1;
        }else{
          $array['p1'][$mes] = 1;
        }
      }else if($dia>= 9 && $dia <=16){
        //periodo 2
        if(isset($array['p2'][$mes])){
          $valorp2 = $array['p2'][$mes] +1;
          $array['p2'][$mes] = $valorp2;
        }else{
          $array['p2'][$mes] = 1;
        }
      }else if($dia>= 17 && $dia <=24){
        //periodo 3
        if(isset($array['p3'][$mes])){
          $valorp3 = $array['p3'][$mes] +1;
          $array['p3'][$mes] = $valorp3;
        }else{
          $array['p3'][$mes] = 1;
        }
      }else if($dia>= 25 && $dia <=31){
        //periodo 4
        if(isset($array['p4'][$mes])){
          $valorp4 = $array['p4'][$mes] +1;
          $array['p4'][$mes] = $valorp4;
        }else{
          $array['p4'][$mes] = 1;
        }
      }

    }

    $array_final = array();
    foreach ($array as $key => $value) {
      for ($i=1; $i <=12 ; $i++) {
        if(isset($value[$i])){
          $array_final[$key][$i] = $value[$i];
        }else{
          $array_final[$key][$i] = 0;
        }
      }

    }
    return $array_final;
  }
  //FIN DE ESTADÍSTICAS

  //PH PARA GUARDAR COMENTARIOS
  public function comentarios_proactivo_historial(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
       if($this->input->post()){
       $this->form_validation->set_rules('comentario', 'comentario', 'trim|required');
       if ($this->form_validation->run()){
          $this->mc->saveComentarioReagendar_PH();
          exit();
          }else{
             $errors = array(
                  'comentario' => form_error('comentario'),
             );
            echo json_encode($errors); exit();
          }
      }
      $data['id_cita'] = $this->input->get('id_cita');
      $data['input_comentario'] = form_textarea('comentario',"",'class="form-control" id="comentario" rows="2"');

      $this->blade->render('citas/comentarios_proactivo_historial',$data);
  }
  public function historial_comentarios_proactivo_historial(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $data['comentarios'] = $this->mc->getComentariosReagendar_magic($this->input->get('id_cita'));
    $data['tipo_reagendar_proact'] = $this->input->get('tipo_reagendar_proact');
     $this->blade->render('citas/historial_comentarios_proactivo_historial',$data);

  }

  //FIN DE MAGIC


  //TRANSICIONES
  //NUEVA VERSION
  public function linea_tiempo($id_cita=0){
    $data['transiciones'] = $this->mc->getTransicionesbyCita($id_cita);
    
    $data['lista_operaciones'] = $this->ml->getOperacionesByCita($id_cita);
    //debug_var($data['lista_operaciones']);die();
    $array_diferencias = array();
    $array_diferencias_asesores = array();
    foreach ($data['transiciones'] as $key => $value) {
      $array_diferencias[$key] = $value;
      $diferenca_minutos = $this->mc->diferencia_horas_laborales($value->inicio,$value->fin);

      $array_diferencias[$key]->tiempo_transcurrido = $diferenca_minutos;

      $array_diferencias[$key]->minutos_pasados = $diferenca_minutos ;

    }

    //FIN ASESORES
    $data['diferencias'] = $array_diferencias;
     //Timpo de la cita
    $data['tecnico_asignado'] = $this->mc->getNameTecnico($this->mc->getTecnicoByCita($id_cita));

    $data['tiempo_cita'] = $this->mc->getTimeCita($id_cita);
    
    //obtengo el horario actual para obtener el operador
    $horario_actual = $this->mc->getIdHorarioActual($id_cita);
    //obtengo el id del asesor
    $asesor_asignado = $this->mc->getIdOperador($horario_actual);
    $data['asesor_asignado'] = $this->mc->getNameAsesor($asesor_asignado);

    $data['hora_cita'] = $this->mc->getHora($horario_actual);
    $data['tecnicos_citas'] = $this->mc->getDataTecnicosCitas($id_cita);
    $data['fecha_promesa'] = $this->mc->hora_promesa($id_cita);
    $data['id_cita'] = $id_cita;
    echo $this->blade->render('layout/linea_tiempo',$data,true);
  }
  //FIN TRANSICIONES

  //CARRIOVER
    public function agendar_cita_carriover($id=0,$reagendar=0){ // el segundo parámetro es para saber si reagendó 0=no, 1 = si

    if($this->input->post()){

        $this->form_validation->set_rules('citas[vehiculo_anio]', 'año del vehículo', 'trim|required|numeric');
        $this->form_validation->set_rules('citas[vehiculo_placas]', 'placas', 'trim|required');
        //$this->form_validation->set_rules('citas[vehiculo_marca]', 'marca', 'trim|required');
        $this->form_validation->set_rules('citas[vehiculo_modelo]', 'modelo', 'trim|required');
        //$this->form_validation->set_rules('citas[vehiculo_version]', 'versión', 'trim|required');
        $this->form_validation->set_rules('citas[vehiculo_numero_serie]', 'número de serie', 'trim');



        $this->form_validation->set_rules('citas[email]', 'email', 'trim|required|valid_email');

        $this->form_validation->set_rules('citas[datos_nombres]', 'nombre(s)', 'trim|required');
        $this->form_validation->set_rules('citas[datos_apellido_paterno]', 'apellido paterno', 'trim|required');
        $this->form_validation->set_rules('citas[datos_apellido_materno]', 'apellido materno', 'trim');
        $this->form_validation->set_rules('citas[datos_telefono]', 'teléfono', 'trim|required|numeric|exact_length[10]');
      $this->form_validation->set_rules('citas[asesor]', 'asesor', 'trim|required');
        //origen viene si se guarda del tablero "tablero" si no viene normal
        if($this->input->post('origen')=='normal'){
           $this->form_validation->set_rules('citas[id_opcion_cita]', 'donde realizó cita', 'trim|required');
            $this->form_validation->set_rules('citas[transporte]', 'transporte', 'trim|required');
            $this->form_validation->set_rules('citas[servicio]', 'servicio', 'trim|required');
            $this->form_validation->set_rules('citas[horario]', 'horario', 'trim|required');
            $this->form_validation->set_rules('citas[fecha]', 'fecha', 'trim|required');

         }else{
           $this->form_validation->set_rules('citas[id_opcion_cita]', 'donde realizó cita', 'trim');
            $this->form_validation->set_rules('citas[transporte]', 'transporte', 'trim');
            $this->form_validation->set_rules('citas[servicio]', 'servicio', 'trim');
            $this->form_validation->set_rules('citas[horario]', 'horario', 'trim');
            $this->form_validation->set_rules('citas[fecha]', 'fecha', 'trim');
         }

        $this->form_validation->set_rules('citas[id_color]', 'color', 'trim|required');
        $this->form_validation->set_rules('citas[id_status_color]', 'estatus', 'trim|required');
        if($this->input->post('tecnico_dias')!=''){
          $this->form_validation->set_rules('fecha_inicio', 'fecha inicio', 'trim');
          $this->form_validation->set_rules('fecha_fin', 'fecha fin', 'trim');
        }
        if($this->input->post('id')==0 && $this->input->post('dia_completo')==null){
          $this->form_validation->set_rules('tecnico', 'técnico', 'trim|required');
        }
        if($this->input->post('id')==0 && $this->input->post('dia_completo')!=null){
          $this->form_validation->set_rules('tecnico_dias', 'técnico', 'trim|required');
        }

        if($this->input->post('id')==0 && $this->input->post('tecnico')!='' && $this->input->post('dia_completo')==null){
            $this->form_validation->set_rules('tecnico', 'tecnico', 'trim|required');
            $this->form_validation->set_rules('hora_inicio', 'hora inicio', 'trim|required');
            $this->form_validation->set_rules('hora_fin', 'hora fin', 'trim|required');
        }

       if ($this->form_validation->run()){
          $this->mc->guardarCita();
          }else{
             $errors = array(
                  'transporte' => form_error('citas[transporte]'),
                  'vehiculo_anio' => form_error('citas[vehiculo_anio]'),
                  'vehiculo_placas' => form_error('citas[vehiculo_placas]'),
                  //'vehiculo_marca' => form_error('citas[vehiculo_marca]'),
                  'vehiculo_modelo' => form_error('citas[vehiculo_modelo]'),
                  //'vehiculo_version' => form_error('citas[vehiculo_version]'),
                  'vehiculo_numero_serie' => form_error('citas[vehiculo_numero_serie]'),
                  'asesor' => form_error('citas[asesor]'),
                  'fecha' => form_error('citas[fecha]'),
                  'horario' => form_error('citas[horario]'),
                  'email' => form_error('citas[email]'),

                  'nombre' => form_error('citas[datos_nombres]'),
                  'apellido_paterno' => form_error('citas[datos_apellido_paterno]'),
                  'apellido_materno' => form_error('citas[datos_apellido_materno]'),
                  'id_status_color' => form_error('citas[id_status_color]'),
                  'telefono' => form_error('citas[datos_telefono]'),
                  'servicio' => form_error('citas[servicio]'),
                  'id_opcion_cita' => form_error('citas[id_opcion_cita]'),
                  'id_color' => form_error('citas[id_color]'),
                  'tecnico' => form_error('tecnico'),
                  'tecnico_dias' => form_error('tecnico_dias'),
                  'hora_inicio' => form_error('hora_inicio'),
                  'hora_fin' => form_error('hora_fin'),
                  'fecha_inicio' => form_error('fecha_inicio'),
                  'fecha_fin' => form_error('fecha_fin'),
             );
            echo json_encode($errors); exit();
          }
      }

      if($id==0){
        $info=new Stdclass();
        $info_horario=new Stdclass();
        $id_horario = 0;
        $realizo_servicio = 0;
        $tecnico_actual = 0;
        $dia_completo = 0;
        $demo = 0;
        $duda = 0;
        $fecha_verificar = date('Y-m-d');
        $origen = 1; //viene desde el formulario
      }else{
        $info= $this->mc->getCitaId($id);
        $info=$info[0];
        $origen = $info->origen;

        $info_horario= $this->mc->getHorarioId($info->id_horario);
        if(count($info_horario)>0){
          $info_horario=$info_horario[0];

          $id_horario = $info->id_horario;
          $realizo_servicio = $info->realizo_servicio;
          $fecha_verificar = $info_horario->fecha;
        }else{
          $info_horario=new Stdclass();
          $id_horario = 0;
          $realizo_servicio = 0;
          $fecha_verificar = date('Y-m-d');
        }
        $tecnico_actual = $info->id_tecnico;
        $dia_completo = 0;

        $demo = $info->demo;
        $duda = $info->duda;

      }
      if($origen==2){
        $disabled_tablero = 'disabled';
      }else{
         $disabled_tablero = '';
      }

      $data['input_id'] = $id;
      $data['reagendada'] = $reagendar;
      $data['input_id_horario'] = $id_horario;
      $data['input_realizo_servicio'] = $realizo_servicio;
      $data['input_id_tecnico_actual'] = $tecnico_actual;
      $data['fecha_verificar'] = $fecha_verificar;
      if($origen==1 || $origen == 0){
        $data['origen'] = 'normal';
      }else{
        $data['origen'] = 'tablero';
      }

      // 1viene desde el formulario, 2 desde el tablero
      $data['drop_transporte'] = form_dropdown('citas[transporte]',array_combos($this->mcat->get('cat_transporte','transporte'),'id','transporte',TRUE),set_value('transporte',exist_obj($info,'transporte')),'class="form-control" id="transporte" '.$disabled_tablero.'');

      $email_info = set_value('email',exist_obj($info,'email'));
      if($email_info ==''){
        $email_info = 'notienecorreo@notienecorreo.com.mx';
      }

       $data['input_email'] = form_input('citas[email]',$email_info,'class="form-control minusculas" rows="5" id="email" ');

      $data['drop_vehiculo_anio'] = form_dropdown('citas[vehiculo_anio]',array_combos($this->mcat->get('cat_anios','anio','desc'),'anio','anio',TRUE),set_value('vehiculo_anio',exist_obj($info,'vehiculo_anio')),'class="form-control busqueda" id="vehiculo_anio"');

       $data['drop_vehiculo_marca'] = form_dropdown('citas[vehiculo_marca]',array_combos($this->mcat->get('cat_marca','marca'),'marca','marca',TRUE),set_value('vehiculo_marca',exist_obj($info,'vehiculo_marca')),'class="form-control" id="vehiculo_marca"');

        $data['drop_vehiculo_modelo'] = form_dropdown('citas[vehiculo_modelo]',array_combos($this->mcat->get('cat_modelo','modelo'),'modelo','modelo',TRUE),set_value('vehiculo_modelo',exist_obj($info,'vehiculo_modelo')),'class="form-control busqueda" id="vehiculo_modelo"');

       $data['drop_vehiculo_version'] = form_dropdown('citas[vehiculo_version]',array_combos($this->mcat->get('cat_version','version'),'version','version',TRUE),set_value('vehiculo_version',exist_obj($info,'vehiculo_version')),'class="form-control" id="vehiculo_version"');

        $data['input_vehiculo_placas'] = form_input('citas[vehiculo_placas]',set_value('vehiculo_placas',exist_obj($info,'vehiculo_placas')),'class="form-control" rows="5" id="vehiculo_placas" ');

        $data['input_vehiculo_numero_serie'] = form_input('citas[vehiculo_numero_serie]',set_value('vehiculo_numero_serie',exist_obj($info,'vehiculo_numero_serie')),'class="form-control" rows="5" id="vehiculo_numero_serie" ');

        $data['input_comentarios'] = form_textarea('citas[comentarios_servicio]',set_value('comentarios_servicio',exist_obj($info,'comentarios_servicio')),'class="form-control" id="comentarios_servicio" rows="2"');

        //echo set_value('asesor',exist_obj($info,'asesor'));die();


        if($id==0){
          $opciones_fecha = array('' =>'-- Selecciona --', );
          $disabled = "disabled";
        }else{
          if(isset($info_horario->fecha)){
            $fecha = $info_horario->fecha;
          }else{
            $fecha = date('Y-m-d');
          }
          $id_asesor=$this->mc->getIdAsesor(exist_obj($info,'asesor'));
          $opciones_fecha = array_combos_fechas($this->mc->getFechasAsesor($id_asesor,$fecha),'fecha','fecha',TRUE);
          $disabled = '';
        }

        if($origen==2){
          $data['drop_fecha'] = form_input('citas[fecha]',date_eng2esp_1(set_value('fecha',exist_obj($info,'fecha'))),'class="form-control" id="fecha" readonly');

          $data['drop_asesor'] = form_input('citas[asesor]',set_value('asesor',exist_obj($info,'asesor')),'class="form-control" id="asesor" readonly');

        }else{
          $data['drop_fecha'] = form_dropdown('citas[fecha]',$opciones_fecha,set_value('fecha',exist_obj($info_horario,'fecha')),'class="form-control" id="fecha" '.$disabled.' '.$disabled_tablero.' ');

          $data['drop_asesor'] = form_dropdown('citas[asesor]',array_combos($this->mcat->get('operadores','nombre','',array('activo'=>1)),'nombre','nombre',TRUE),set_value('asesor',exist_obj($info,'asesor')),'class="form-control" id="asesor" '.$disabled_tablero.'  ');

        }

        if($id==0){
          $opciones_horario = array('' =>'-- Selecciona --', );
        }else{
          $id_asesor=$this->mc->getIdAsesor(exist_obj($info,'asesor'));
          //echo $this->db->last_query();die();
          $horario_id = set_value('id',exist_obj($info_horario,'id'));
          if($origen==1 || $origen==0){
             $opciones_horario = array_combos($this->mc->getHorariosAsesor_edit($id_asesor,set_value('fecha',exist_obj($info_horario,'fecha')),$horario_id),'id','hora',TRUE);
          }else{
            $opciones_horario = array();
          }

        }

         $data['drop_horario'] = form_dropdown('citas[horario]',$opciones_horario,set_value('id',exist_obj($info_horario,'id')),'class="form-control" id="horario" '.$disabled.' '.$disabled_tablero.' ');





        $data['input_datos_nombres'] = form_input('citas[datos_nombres]',set_value('datos_nombres',exist_obj($info,'datos_nombres')),'class="form-control"  id="datos_nombres" ');

        $data['input_datos_apellido_paterno'] = form_input('citas[datos_apellido_paterno]',set_value('datos_apellido_paterno',exist_obj($info,'datos_apellido_paterno')),'class="form-control"  id="datos_apellido_paterno" ');

         $data['input_datos_apellido_materno'] = form_input('citas[datos_apellido_materno]',set_value('datos_apellido_materno',exist_obj($info,'datos_apellido_materno')),'class="form-control"  id="datos_apellido_materno" ');

          $data['input_datos_telefono'] = form_input('citas[datos_telefono]',set_value('datos_telefono',exist_obj($info,'datos_telefono')),'class="form-control"  id="datos_telefono" maxlength="10" ');

          $data['drop_servicio'] = form_dropdown('citas[servicio]',array_combos($this->mcat->get('cat_servicios','servicio'),'servicio','servicio',TRUE),set_value('servicio',exist_obj($info,'servicio')),'class="form-control" id="servicio" '.$disabled_tablero.'');

        if($id==0 || $origen == 2){
          $fecha= date('Y-m-d');
        }else{

          $fecha = $info_horario->fecha;
        }

        $data['drop_opcion'] = form_dropdown('citas[id_opcion_cita]',array_combos($this->mcat->get('cat_opcion_citas','opcion'),'id','opcion',TRUE),set_value('id_opcion_cita',exist_obj($info,'id_opcion_cita')),'class="form-control" id="id_opcion_cita"');

        $data['drop_color'] = form_dropdown('citas[id_color]',array_combos($this->mcat->get('cat_colores','color'),'id','color',TRUE),set_value('id_color',exist_obj($info,'id_color')),'class="form-control busqueda" id="id_color"');

        //echo set_value('id_tecnico',exist_obj($info,'id_tecnico'));die();
        //if($id!=0){
          $data['drop_tecnicos'] = form_dropdown('tecnico',array_combos($this->mc->getTecnicosByFechaCita($fecha),'id_tecnico','nombre',TRUE),set_value('id_tecnico',exist_obj($info,'id_tecnico')),'class="form-control" id="tecnicos" '.$disabled.' ');
          //echo $this->db->last_query();die();
        //}
      $data['btn_guardar'] = form_button('btnguardar','Guardar cita',' id="guardar" class="btn btn-success btn-block pull-right"');

      $data['input_hora_inicio'] = form_input('hora_inicio',set_value('hora_inicio',exist_obj($info,'hora_inicio')),'class="form-control" id="hora_inicio" ');

      $data['input_hora_fin'] = form_input('hora_fin',set_value('hora_fin',exist_obj($info,'hora_fin')),'class="form-control" id="hora_fin" ');

      $data['input_dia_completo'] = form_checkbox('dia_completo',$dia_completo,($dia_completo==1)?'checked':'','class="" id="dia_completo" ');

      //Cuando son días completos
      $data['input_fecha_inicio'] = form_input('fecha_inicio',$this->input->post('fecha'),'class="form-control" id="fecha_inicio" ');

      $data['input_fecha_fin'] = form_input('fecha_fin',set_value('fecha_fin',exist_obj($info,'fecha_fin')),'class="form-control" id="fecha_fin" ');

      $data['drop_tecnicos_dias'] = form_dropdown('tecnico_dias',array_combos($this->mc->getTecnicosByFechaCita($fecha),'id_tecnico','nombre',TRUE),set_value('id_tecnico',exist_obj($info,'id_tecnico')),'class="form-control" id="tecnicos_dias" '.$disabled.' ');

      $data['input_fecha_parcial'] = form_input('fecha_parcial',set_value('fecha_inicio',exist_obj($info,'fecha_parcial')),'class="form-control" id="fecha_parcial" ');

       $data['input_hora_inicio_extra'] = form_input('hora_inicio_extra',set_value('hora_inicio',exist_obj($info,'hora_inicio')),'class="form-control" id="hora_inicio_extra" ');

      $data['input_hora_fin_extra'] = form_input('hora_fin_extra',set_value('hora_fin',exist_obj($info,'hora_fin')),'class="form-control" id="hora_fin_extra" ');

      //si es por día completo saber cuando empieza
      $data['input_hora_comienzo'] = form_input('hora_comienzo',set_value('hora_inicio_dia',exist_obj($info,'hora_inicio_dia')),'class="form-control" id="hora_comienzo" ');

       $data['drop_id_status_color'] = form_dropdown('citas[id_status_color]',array_combos($this->mcat->get('estatus','nombre'),'id','nombre',TRUE),set_value('id_status_color',exist_obj($info,'id_status_color')),'class="form-control busqueda" id="id_status_color"');

       $data['input_demo'] = form_checkbox('demo',$demo,($demo==1)?'checked':'','class="js_dudas" id="demo" ');

       $data['input_duda'] = form_checkbox('duda',$duda,($duda==1)?'checked':'','class="js_dudas" id="duda" ');
    $this->blade->render('agendar_cita_carriover',$data);
  }
  public function getDatosCita(){
    $mensaje = '';
    $datos_cita = $this->db->where('id_cita',$this->input->post('id_cita'))->get('citas');
    $tecnicos_citas_dia = $this->db->where('id_cita',$this->input->post('id_cita'))->where('dia_completo',0)->get('tecnicos_citas');
    $tecnicos_citas_dia_completo = $this->db->where('id_cita',$this->input->post('id_cita'))->where('dia_completo',1)->get('tecnicos_citas');
    $operaciones = $this->ml->getOperacionesByCita($this->input->post('id_cita'),true); //NUEVA VERSION

    $operaciones_sin_tecnico = $this->ml->getOperacionesSinTecnico($this->input->post('id_cita')); //NUEVA VERSION

    
    $exito = false;
    if($datos_cita->num_rows()){
      $exito = true;
      $cita = $datos_cita->row();
    }else{
      $cita = '';
      $mensaje = "La cita no se encuentra registrada";
    }
    $dia_completo = 0;
    //Mandar el de día normal
    if($tecnicos_citas_dia->num_rows()==1){
      $cita_tecnico = $tecnicos_citas_dia->row();
    }else{
      $cita_tecnico = '';
    }
    //Verificar si tiene alguno por día completo
    if($tecnicos_citas_dia_completo->num_rows()==1){
      $dia_completo = 1;
      $cita_tecnico_completo = $tecnicos_citas_dia_completo->row();
    }else{
      $cita_tecnico_completo = '';
    }
    //Validar que la cita no esté en un estatus de terminado
    $id_status = $this->mc->getStatusCitaPrincipal($this->input->post('id_cita'));
    if($id_status==3||$id_status==5||$id_status==8||$id_status==9||$id_status==10||$id_status==11){
      $exito = false;
      $mensaje = "Una vez terminada la cita no se puede realizar el carryover";
    }

    $idorden = $this->mc->getIdOrdenByCita($this->input->post('id_cita')); //Id orden por cita
    
    $todas_operaciones_terminadas = $this->ml->validarTodasOperacionesTerminadas($idorden);
    //lquery();die();

    //Si todas las operaciones ya fueron terminadas no deje asignar 
    if($todas_operaciones_terminadas){
      $exito = false;
      $mensaje = "Una vez terminadas las operaciones no se puede realizar el carryover";
    }
    // if(!$this->mc->validOrder($this->input->post('id_cita'))){
    //   $exito = false;
    //   $mensaje = "No se puede realizar carryover por que no se ha generado una orden";
    // }
    //NUEVA VERSION
    echo json_encode(array('exito'=>$exito,'cita'=>$cita
      ,"dia_completo"=>$dia_completo,"cita_tecnico"=>$cita_tecnico,
      'cita_tecnico_completo'=>$cita_tecnico_completo
      ,"mensaje"=>$mensaje,
      "operaciones"=>$operaciones,
      "operaciones_sin_tecnico"=>$operaciones_sin_tecnico
      )
    );
  }
  public function getDatosCitaServicios(){
    $datos_cita = $this->db->where('id_cita',$this->input->post('id_cita'))->get('citas');
    $tecnicos_citas = $this->db->select('tc.*,t.nombre as tecnico')->where('id_cita',$this->input->post('id_cita'))->join('tecnicos t','tc.id_tecnico=t.id')->get('tecnicos_citas tc')->result();

    //Regresar fecha y hora promesa
    if(count($tecnicos_citas)==1){
      $newDate = $tecnicos_citas[0]->fecha.' '.$tecnicos_citas[0]->hora_fin;
      $date = new DateTime($newDate);
      $date->modify('+40 minute');
      $fecha_promesa_m = $date->format('Y-m-d');
      $hora_promesa_m = $date->format('H:i');
    }else if(count($tecnicos_citas)==2){
      $newDate = $tecnicos_citas[1]->fecha.' '.$tecnicos_citas[1]->hora_fin;
      $date = new DateTime($newDate);
      $date->modify('+40 minute');
      $fecha_promesa_m = $date->format('Y-m-d');
      $hora_promesa_m = $date->format('H:i');
    }else{
      $fecha_promesa_m = '';
      $hora_promesa_m = '';
    }


    $exito = false;
    if($datos_cita->num_rows()){
      $exito = true;
      $cita = $datos_cita->row();
    }else{
      $datos_cita = $this->db->where('vehiculo_placas',$this->input->post('id_cita'))->limit(1)->get('citas');
      if($datos_cita->num_rows()){
        $exito = true;
        $cita = $datos_cita->row();
      }else{
        $cita = '';
      }
    }

    if($exito){
      $qorden = $this->db->where('id_cita',$cita->id_cita)->get('ordenservicio');
      if($qorden->num_rows()==1){
        $existe_orden = 1;
        $orden = $qorden->row();
      }else{
        $orden = new Stdclass();
        $existe_orden = 0;
      }
    }else{
      $orden = new Stdclass();
      $existe_orden = 0;
    }
    echo json_encode(array('exito'=>$exito,'cita'=>$cita,"tecnicos_citas"=>$tecnicos_citas,'orden'=>$orden,"existe_orden"=>$existe_orden,"fecha_promesa_m"=>$fecha_promesa_m,"hora_promesa_m"=>$hora_promesa_m));
  }
  public function saveCitaCarriover(){

    if($this->input->post('dia_completo')==0){
      $this->form_validation->set_rules('tecnico', 'técnico', 'trim|required');
    }else{
      $this->form_validation->set_rules('tecnico_dias', 'técnico', 'trim|required');
    }
    if($this->input->post('dia_completo')==1){

    }
    if ($this->form_validation->run()){
    }else{
      $errors = array(
        'tecnico' => form_error('tecnico'),
        'tecnico_dias' => form_error('tecnico_dias'),
        'hora_inicio' => form_error('hora_inicio'),
        'hora_fin' => form_error('hora_fin'),
        'fecha_inicio' => form_error('fecha_inicio'),
        'fecha_fin' => form_error('fecha_fin'),
      );
      echo json_encode($errors); exit();
    }

  }
  //FIN CARRIOVER
  //Ver el historial de cambio de ediciones de citas
  public function historialEdiciones($id_cita=''){
    $data['historial'] = $this->db->where('id_cita',$id_cita)->select('he.created_at,he.cambio_tecnicos,ad.adminNombre,a.hora,e.nombre as estatus,t.nombre as tecnico,o.nombre as asesor,a.fecha as fecha_horario')->join(CONST_BASE_PRINCIPAL.'admin ad','he.id_usuario = ad.adminId','left')->join('aux a','a.id = he.id_horario','left')->join('estatus e','e.id = he.id_status','left')->join('tecnicos t','t.id = he.id_tecnico','left')->order_by('he.created_at','desc')->join('operadores o','o.id=a.id_operador','left')->get('historial_ediciones he')->result();
    $data['cita'] = $this->db->where('id_cita',$id_cita)->select('c.fecha_creacion,ad.adminNombre,a.hora,e.nombre as estatus,t.nombre as tecnico,o.nombre as asesor,a.fecha as fecha_horario')->join(CONST_BASE_PRINCIPAL.'admin ad','c.id_usuario = ad.adminId','left')->join('aux a','a.id = c.id_horario','left')->join('estatus e','e.id = c.id_status','left')->join('tecnicos t','t.id = c.id_tecnico','left')->order_by('c.fecha_creacion','desc')->join('operadores o','o.id=a.id_operador','left')->get('citas c')->result();

    $data['tecnicos_citas'] = $this->mc->getDatosTecnicosCitas($id_cita);

    //debug_var($data['cita']);die();
    $this->blade->render('v_historial_ediciones',$data);

  }
  //Fin de historial
  public function getTablaOperacionesExtras(){
    $data['operaciones_extras_orden'] = $this->mc->operacionesExtras($_POST['id_cita']);
    $this->blade->render('tlb_operaciones_extras',$data);
  }
  //SERVICIO
  public function cita_servicio($id=0){
        if($id!=0){
          if(!PermisoAccion('editar_orden')){
              redirect(site_url('orden/historial_orden_servicio'));
          }
        }
        $info=new Stdclass();
        $info_horario=new Stdclass();
        $info_servicio=new Stdclass();
        $id_horario = 0;
        $realizo_servicio = 0;
        $tecnico_actual = 0;
        $dia_completo = 0;
        $fecha_verificar = date('Y-m-d');

        if($id==0){
        $info=new Stdclass();
        $info_horario=new Stdclass();
        $id_horario = 0;
        $realizo_servicio = 0;
        $tecnico_actual = 0;
        $dia_completo = 0;
        $demo = 0;
        $duda = 0;
        $fecha_verificar = date('Y-m-d');
        $temporal = $this->mc->retornoTemporal();
        $id_cita = 0;
        $campania = 0;
        $paquete_id = 0;
        $puede_agregar_operacion = true; // Cuando es 0 siempre se puede agregar una operación
        $acepta_beneficios = 0;
      }else{
        $info_servicio = $this->db->where('id',$id)->get('ordenservicio')->row();
        $info= $this->mc->getCitaId($info_servicio->id_cita);
        $info=$info[0];
        $campania = $info_servicio->campania;
        $origen = $info->origen;
        $temporal = $info_servicio->temporal;
        $paquete_id = $info_servicio->paquete_id;

        $info_horario= $this->mc->getHorarioId($info->id_horario);
        if(count($info_horario)>0){
          $info_horario=$info_horario[0];

          $id_horario = $info->id_horario;
          $realizo_servicio = $info->realizo_servicio;
          $fecha_verificar = $info_horario->fecha;
        }else{
          $info_horario=new Stdclass();
          $id_horario = 0;
          $realizo_servicio = 0;
          $fecha_verificar = date('Y-m-d');
        }
        $tecnico_actual = $info->id_tecnico;
        $dia_completo = 0;
        $demo = $info->demo;
        $duda = $info->duda;
        $id_cita = $info_servicio->id_cita;

        $unidad_entregada = $this->mc->Unidad_entregada($id_cita);

        if($unidad_entregada==1 ||$info_servicio->id_status_intelisis==4 ||$info_servicio->id_situacion_intelisis==6){
          redirect(base_url('orden/historial_orden_servicio'));
        }

        //Estatus actual de la cita
        $id_status_actual_cita = $this->mc->getStatusCitaPrincipal($id_cita);
        switch ($id_status_actual_cita) {
          case 5:
          case 8:
          case 9:
          case 10:
          case 11:
          case 3:
            $puede_agregar_operacion = false;
          break;
          default:
            $puede_agregar_operacion = true;
          break;
        }
        $data['operaciones_extras_orden'] = $this->principal->operacionesExtrasByCita($id_cita);
        $acepta_beneficios = $info_servicio->acepta_beneficios;

      }


      //Si ya se entregó no se puede editar nada

      $data['input_id'] = $id;
      $data['paquete_id'] = $paquete_id;
      $data['id_cita'] = $id_cita;
      $data['input_id_horario'] = $id_horario;
      $data['input_realizo_servicio'] = $realizo_servicio;
      $data['input_id_tecnico_actual'] = $tecnico_actual;
      $data['fecha_verificar'] = $fecha_verificar;
      $data['campania'] = $campania;
      $data['temporal'] = $temporal;
      $data['puede_agregar_operacion'] = $puede_agregar_operacion; // de acuerdo al estatus se puede agregar o no operaciones
      
      $data['drop_transporte'] = form_dropdown('citas[transporte]',array_combos($this->mcat->get('cat_transporte','transporte'),'id','transporte',TRUE),set_value('transporte',exist_obj($info,'transporte')),'class="form-control" id="transporte"');

      $email_info = set_value('email',exist_obj($info,'email'));
      if($email_info ==''){
        $email_info = 'notienecorreo@notienecorreo.com.mx';
      }

       $data['input_email'] = form_input('citas[email]',$email_info,'class="form-control minusculas" rows="5" id="email" ');

      $data['drop_vehiculo_anio'] = form_dropdown('citas[vehiculo_anio]',array_combos($this->mcat->get('cat_anios','anio','desc'),'anio','anio',TRUE),set_value('vehiculo_anio',exist_obj($info,'vehiculo_anio')),'class="form-control editarPreventivo" id="vehiculo_anio"');



      $data['drop_vehiculo_modelo'] = form_dropdown('citas[vehiculo_modelo]',array_combos($this->mcat->get('cat_modelo','modelo'),'modelo','modelo',TRUE),set_value('vehiculo_modelo',exist_obj($info,'vehiculo_modelo')),'class="form-control busqueda editarPreventivo" id="vehiculo_modelo"');

        if($id==0){
          $submodelos = array();
        }else{
          $mod = set_value('vehiculo_modelo',exist_obj($info,'vehiculo_modelo'));
          $modelo_id = $this->principal->getGeneric('modelo', $mod, 'cat_modelo','id');
          $submodelos = $this->mcat->get('pkt_subarticulos','nombre','',['articulo_id'=>$modelo_id]);
        }
        $data['drop_submodelo_id'] = form_dropdown('citas[submodelo_id]',array_combos($submodelos,'id','nombre',TRUE),set_value('submodelo_id',exist_obj($info,'submodelo_id')),'class="form-control" id="submodelo_id"');

        $data['drop_vehiculo_version'] = form_dropdown('citas[vehiculo_version]',array_combos($this->mcat->get('cat_version','version'),'version','version',TRUE),set_value('vehiculo_version',exist_obj($info,'vehiculo_version')),'class="form-control" id="vehiculo_version"');

        $data['input_vehiculo_placas'] = form_input('citas[vehiculo_placas]',set_value('vehiculo_placas',exist_obj($info,'vehiculo_placas')),'class="form-control" rows="5" id="vehiculo_placas" ');

        $data['input_vehiculo_numero_serie'] = form_input('citas[vehiculo_numero_serie]',set_value('vehiculo_numero_serie',exist_obj($info,'vehiculo_numero_serie')),'class="form-control" rows="5" id="vehiculo_numero_serie" maxlength="17" ');

        $data['input_comentarios'] = form_textarea('citas[comentarios_servicio]',set_value('comentarios_servicio',exist_obj($info,'comentarios_servicio')),'class="form-control" id="comentarios_servicio" rows="2"');

        //echo set_value('asesor',exist_obj($info,'asesor'));die();
        $data['drop_asesor'] = form_dropdown('citas[asesor]',array_combos($this->mcat->get('operadores','','',array('activo'=>1)),'nombre','nombre',TRUE),set_value('asesor',exist_obj($info,'asesor')),'class="form-control" id="asesor" ');

         $id_asesor=$this->mc->getIdAsesor(exist_obj($info,'asesor'));

          $disabled = '';

          $data['drop_fecha'] = form_input('citas[fecha]',$this->input->post('fecha'),'class="form-control"  id="fecha" readonly ');

        $opciones_horario = array('' =>'-- Selecciona --', );

         $data['drop_horario'] = form_dropdown('citas[horario]',$opciones_horario,set_value('id',exist_obj($info_horario,'id')),'class="form-control" id="horario" '.$disabled.' ');



        $opciones_tipo_cliente = array('' =>'-- Selecciona --','Sr.'=>'Sr.','Sra.'=>'Sra.','Empresa.'=>'Empresa.' );

        $data['drop_tipo_cliente'] = form_dropdown('tipo_cliente',$opciones_tipo_cliente,set_value('tipo_cliente',exist_obj($info_servicio,'tipo_cliente')),'class="form-control" id="tipo_cliente" '.$disabled);

        $data['input_datos_nombres'] = form_input('citas[datos_nombres]',set_value('datos_nombres',exist_obj($info,'datos_nombres')),'class="form-control"  id="datos_nombres" ');

        $data['input_datos_apellido_paterno'] = form_input('citas[datos_apellido_paterno]',set_value('datos_apellido_paterno',exist_obj($info,'datos_apellido_paterno')),'class="form-control"  id="datos_apellido_paterno" ');

         $data['input_datos_apellido_materno'] = form_input('citas[datos_apellido_materno]',set_value('datos_apellido_materno',exist_obj($info,'datos_apellido_materno')),'class="form-control"  id="datos_apellido_materno" ');

          $data['input_datos_telefono'] = form_input('citas[datos_telefono]',set_value('datos_telefono',exist_obj($info,'datos_telefono')),'class="form-control"  id="datos_telefono" maxlength="10" ');

          $data['drop_servicio'] = form_dropdown('citas[servicio]',array_combos($this->mcat->get('cat_servicios','servicio'),'servicio','servicio',TRUE),set_value('servicio',exist_obj($info,'servicio')),'class="form-control" id="servicio"');

        $id_modelo_prepiking = exist_obj($info,'id_modelo_prepiking');
        $data['drop_id_modelo_prepiking'] = form_dropdown('citas[id_modelo_prepiking]',array_combos($this->mcat->get('modelos_ford','modelo'),'id','modelo',TRUE),set_value('id_modelo_prepiking',$id_modelo_prepiking),'class="form-control" id="id_modelo_prepiking"');


        $data['drop_id_prepiking'] = form_dropdown('citas[id_prepiking]',array_combos($this->mcat->get('prepiking','id','asc',array('idmodelo'=>$id_modelo_prepiking)),'id','servicio',TRUE),set_value('id_prepiking',exist_obj($info,'id_prepiking')),'class="form-control" id="id_prepiking"');

        if($id==0){
          $fecha= date('Y-m-d');
        }else{
          $fecha= date('Y-m-d');
          //$fecha = $info_horario->fecha_entrega;
        }



        $data['drop_color'] = form_dropdown('citas[id_color]',array_combos($this->mcat->get('cat_colores','color'),'id','color',TRUE),set_value('id_color',exist_obj($info,'id_color')),'class="form-control" id="id_color"');

        //if($id!=0){

          $data['drop_tecnicos'] = form_dropdown('tecnico',array_combos($this->mc->getTecnicosByFechaCita($fecha),'id_tecnico','nombre',TRUE),$this->input->post('id_tecnico'),'class="form-control" id="tecnicos" ');
        //}
        $data['btn_guardar'] = form_button('btnguardar','Guardar cita',' id="guardar" class="btn btn-success btn-block pull-right"');

      $data['input_hora_inicio'] = form_input('hora_inicio',$this->input->post('time'),'class="form-control check_hora" id="hora_inicio"','time');

      $data['input_hora_fin'] = form_input('hora_fin',set_value('hora_fin',exist_obj($info,'hora_fin')),'class="form-control check_hora" id="hora_fin"','time');

      $data['input_dia_completo'] = form_checkbox('dia_completo',$dia_completo,($dia_completo==1)?'checked':'','class="" id="dia_completo" ');

      //Cuando son días completos


      $data['input_fecha_inicio'] = form_input('fecha_inicio',$this->input->post('fecha'),'class="form-control" id="fecha_inicio"','date');

      $data['input_fecha_fin'] = form_input('fecha_fin',set_value('fecha_fin',exist_obj($info,'fecha_fin')),'class="form-control" id="fecha_fin"','date');

      $data['drop_tecnicos_dias'] = form_dropdown('tecnico_dias',array_combos($this->mc->getTecnicosByFechaCita($fecha),'id_tecnico','nombre',TRUE),$this->input->post('id_tecnico'),'class="form-control" id="tecnicos_dias" readonly ');

      $data['input_fecha_parcial'] = form_input('fecha_parcial',set_value('fecha_inicio',exist_obj($info,'fecha_parcial')),'class="form-control" id="fecha_parcial"','date');

       $data['input_hora_inicio_extra'] = form_input('hora_inicio_extra',"",'class="form-control check_hora" id="hora_inicio_extra"','time');

      $data['input_hora_fin_extra'] = form_input('hora_fin_extra',set_value('hora_fin',exist_obj($info,'hora_fin')),'class="form-control check_hora" id="hora_fin_extra"','time');

      //si es por día completo saber cuando empieza
      $data['input_hora_comienzo'] = form_input('hora_comienzo',$this->input->post('time'),'class="form-control check_hora" id="hora_comienzo"','time');

      $data['drop_id_status_color'] = form_dropdown('citas[id_status_color]',array_combos($this->mcat->get('estatus','nombre'),'id','nombre',TRUE),set_value('id_status_color',exist_obj($info,'id_status_color')),'class="form-control" id="id_status_color"');

      $data['input_motor'] = form_input('motor',set_value('motor',exist_obj($info_servicio,'motor')),'class="form-control" id="motor" ');

      $data['input_transmision'] = form_input('transmision',set_value('transmision',exist_obj($info_servicio,'transmision')),'class="form-control" id="transmision" ');

      $data['input_cilindros'] = form_input('cilindros',set_value('cilindros',exist_obj($info_servicio,'cilindros')),'class="form-control" id="cilindros" ');

      //SERVICIO
      $data['input_telefono_asesor'] = form_input('telefono_asesor',set_value('telefono_asesor',exist_obj($info_servicio,'telefono_asesor')),'class="form-control" id="telefono_asesor" maxlength="10" ');

      $data['input_extension_asesor'] = form_input('extension_asesor',set_value('extension_asesor',exist_obj($info_servicio,'extension_asesor')),'class="form-control" id="extension_asesor" ');

      $data['input_numero_interno'] = form_input('numero_interno',set_value('numero_interno',exist_obj($info_servicio,'numero_interno')),'class="form-control" id="numero_interno" ');

      $data['input_color'] = form_input('color',set_value('color',exist_obj($info_servicio,'color')),'class="form-control" id="color" ');

      $data['input_agente'] = form_input('agente',set_value('agente',exist_obj($info_servicio,'agente')),'class="form-control" id="agente" maxlength="6" ');

      $opciones_almacen = array('' =>'-- Selecciona --','SS1'=>'SS1');

      $data['drop_almacen'] = form_dropdown('almacen',$opciones_almacen,set_value('almacen',exist_obj($info_servicio,'almacen')),'class="form-control" id="almacen" ');

      $opciones_sucursal = array('' =>'-- Selecciona --','1'=>'1');

      $data['drop_sucursal'] = form_dropdown('sucursal',$opciones_sucursal,set_value('sucursal',exist_obj($info_servicio,'sucursal')),'class="form-control" id="sucursal" ');

      if($id!=0){
        $disabled = 'disabled';
      }else{
        $disabled = '';
      }

       $fecha_recepcion = date_eng2esp_1(set_value('fecha_recepcion',exist_obj($info_servicio,'fecha_recepcion')));

      if($fecha_recepcion==''){
        $fecha_recepcion = date('d/m/Y');
      }

      $data['input_fecha_recepcion'] = form_input('fecha_recepcion',$fecha_recepcion,'class="form-control" id="fecha_recepcion" readonly="true" '.$disabled.' ');


     $hora_recepcion = set_value('hora_recepcion',exist_obj($info_servicio,'hora_recepcion'));
      if($hora_recepcion==''){
        $hora_recepcion = date('H:i');
      }

      $data['input_hora_recepcion'] = form_input('hora_recepcion',$hora_recepcion,'class="form-control" id="hora_recepcion" readonly="true" '.$disabled.' ');

      $data['input_fecha_entrega'] = form_input('fecha_entrega',(set_value('fecha_entrega',exist_obj($info_servicio,'fecha_entrega'))),'class="form-control" id="fecha_entrega"','date');

      $data['input_hora_entrega'] = form_input('hora_entrega',set_value('hora_entrega',exist_obj($info_servicio,'hora_entrega')),'class="form-control check_hora" id="hora_entrega"','time');

    $data['input_nombre_compania'] = form_input('nombre_compania',set_value('nombre_compania',exist_obj($info_servicio,'nombre_compania')),'class="form-control" id="nombre_compania" ');

    $data['input_nombre_contacto_compania'] = form_input('nombre_contacto_compania',set_value('nombre_contacto_compania',exist_obj($info_servicio,'nombre_contacto_compania')),'class="form-control" id="nombre_contacto_compania" ');

    $data['input_ap_contacto'] = form_input('ap_contacto',set_value('ap_contacto',exist_obj($info_servicio,'ap_contacto')),'class="form-control" id="ap_contacto" ');

    $data['input_am_contacto'] = form_input('am_contacto',set_value('am_contacto',exist_obj($info_servicio,'am_contacto')),'class="form-control" id="am_contacto" ');

    $data['input_correo_consumidor'] = form_input('correo_consumidor',set_value('correo_consumidor',exist_obj($info_servicio,'correo_consumidor')),'class="form-control" id="correo_consumidor" ');


    $data['input_rfc'] = form_input('rfc',set_value('rfc',exist_obj($info_servicio,'rfc')),'class="form-control" id="rfc" maxlength="13" ');


    $email_compania = set_value('correo_compania',exist_obj($info_servicio,'correo_compania'));
    if($email_compania ==''){
      $email_compania = 'notienecorreo@notienecorreo.com.mx';
    }

    $data['input_correo_compania'] = form_input('correo_compania',$email_compania,'class="form-control minusculas" id="correo_compania" ');

    $data['input_calle'] = form_input('calle',set_value('calle',exist_obj($info_servicio,'calle')),'class="form-control" id="calle" ');

    $data['input_nointerior'] = form_input('nointerior',set_value('nointerior',exist_obj($info_servicio,'nointerior')),'class="form-control" id="nointerior" ');


    $data['input_noexterior'] = form_input('noexterior',set_value('noexterior',exist_obj($info_servicio,'noexterior')),'class="form-control" id="noexterior" ');

    $data['input_colonia'] = form_input('colonia',set_value('colonia',exist_obj($info_servicio,'colonia')),'class="form-control" id="colonia" ');

    $data['input_municipio'] = form_input('municipio',set_value('municipio',exist_obj($info_servicio,'municipio')),'class="form-control" id="municipio" ');

    $data['input_cp'] = form_input('cp',set_value('cp',exist_obj($info_servicio,'cp')),'class="form-control" id="cp" ');

    //$data['input_estado'] = form_input('estado',set_value('estado',exist_obj($info_servicio,'estado')),'class="form-control" id="estado" ');

    $data['input_estado'] = form_dropdown('estado',array_combos($this->mcat->get('estados','estado'),'estado','estado',TRUE),set_value('estado',exist_obj($info_servicio,'estado')),'class="form-control" id="estado"');

    $data['input_telefono_movil'] = form_input('telefono_movil',set_value('telefono_movil',exist_obj($info_servicio,'telefono_movil')),'class="form-control" id="telefono_movil" ');

    $data['input_otro_telefono'] = form_input('otro_telefono',set_value('otro_telefono',exist_obj($info_servicio,'otro_telefono')),'class="form-control" id="otro_telefono" ');


    $data['input_vehiculo_identificacion'] = form_input('vehiculo_identificacion',set_value('vehiculo_identificacion',exist_obj($info_servicio,'vehiculo_identificacion')),'class="form-control" id="vehiculo_identificacion" ');


    $data['input_vehiculo_kilometraje'] = form_input('vehiculo_kilometraje',set_value('vehiculo_kilometraje',exist_obj($info_servicio,'vehiculo_kilometraje')),'class="form-control" id="vehiculo_kilometraje" ');

    $data['drop_id_tipo_orden'] = form_dropdown('id_tipo_orden',array_combos($this->mcat->get('cat_tipo_orden','tipo_orden'),'id','tipo_orden',TRUE),set_value('id_tipo_orden',exist_obj($info_servicio,'id_tipo_orden')),'class="form-control" id="id_tipo_orden"');

    $data['input_numero_cliente'] = form_input('numero_cliente',set_value('numero_cliente',exist_obj($info_servicio,'numero_cliente')),'class="form-control" id="numero_cliente" ');

    $data['drop_id_tipo_pago'] = form_dropdown('id_tipo_pago',array_combos($this->mcat->get('cat_tipo_pago','tipo_pago'),'id','tipo_pago',TRUE),set_value('id_tipo_pago',exist_obj($info_servicio,'id_tipo_pago')),'class="form-control" id="id_tipo_pago"');




    $data['drop_id_condicion'] = form_dropdown('id_condicion',array_combos($this->mcat->get('cat_condicion','condicion'),'id','condicion',TRUE),set_value('id_condicion',exist_obj($info_servicio,'id_condicion')),'class="form-control" id="id_condicion"');

    $data['drop_id_tipo_operacion'] = form_dropdown('id_tipo_operacion',array_combos($this->mcat->get('cat_tipo_operacion','tipo_operacion'),'id','tipo_operacion',TRUE),set_value('id_tipo_operacion',exist_obj($info_servicio,'id_tipo_operacion')),'class="form-control" id="id_tipo_operacion"');

    $data['drop_id_tipo_precio'] = form_dropdown('id_tipo_precio',array_combos($this->mcat->get('cat_tipo_precio','tipo_precio'),'id','tipo_precio',TRUE),set_value('id_tipo_precio',exist_obj($info_servicio,'id_tipo_precio')),'class="form-control" id="id_tipo_precio"');

    $data['drop_id_concepto'] = form_dropdown('id_concepto',array_combos($this->mcat->get('cat_concepto','concepto'),'id','concepto',TRUE),set_value('id_concepto',exist_obj($info_servicio,'id_concepto')),'class="form-control" id="id_concepto"');

    $data['drop_id_moneda'] = form_dropdown('id_moneda',array_combos($this->mcat->get('cat_moneda','moneda'),'id','moneda',TRUE),set_value('id_moneda',exist_obj($info_servicio,'id_moneda')),'class="form-control" id="id_moneda"');



    $data['drop_id_uen'] = form_dropdown('id_uen',array_combos($this->mcat->get('cat_uen','uen'),'id','uen',TRUE),set_value('id_uen',exist_obj($info_servicio,'id_uen')),'class="form-control" id="id_uen"');

    $idcategoria = exist_obj($info_servicio,'idcategoria');
    $data['drop_idcategoria'] = form_dropdown('idcategoria',array_combos($this->mcat->get('categorias','categoria'),'id','categoria',TRUE),set_value('idcategoria',$idcategoria),'class="form-control" id="idcategoria"');


    $data['drop_idsubcategoria'] = form_dropdown('idsubcategoria',array_combos($this->mcat->get('subcategorias','subcategoria','',array('idcategoria'=>$idcategoria)),'id','subcategoria',TRUE),set_value('idsubcategoria',exist_obj($info_servicio,'idsubcategoria')),'class="form-control" id="idsubcategoria"');

    if($id==0){
      $marca_select = 'FORD';
    }else{
      $marca_select = set_value('vehiculo_marca',exist_obj($info_servicio,'vehiculo_marca'));
    }
    $data['drop_vehiculo_marca'] = form_dropdown('vehiculo_marca',array_combos($this->mcat->get('cat_marca','marca'),'marca','marca',TRUE),$marca_select,'class="form-control" id="vehiculo_marca"');

    $data['drop_status_intelisis'] = form_dropdown('id_status_intelisis',array_combos($this->mcat->get('cat_status_intelisis_orden','status'),'id','status',TRUE),set_value('id_status_intelisis',exist_obj($info_servicio,'id_status_intelisis')),'class="form-control" id="id_status_intelisis"');

    $data['drop_situacion_intelisis'] = form_dropdown('id_situacion_intelisis',array_combos($this->mcat->get('cat_situacion_intelisis','situacion'),'id','situacion',TRUE),set_value('id_situacion_intelisis',exist_obj($info_servicio,'id_situacion_intelisis')),'class="form-control" id="id_situacion_intelisis"');

    $data['drop_status_orden'] = form_dropdown('id_status',array_combos($this->mcat->get('cat_status_orden','status'),'id','status',TRUE),set_value('id_status',exist_obj($info_servicio,'id_status')),'class="form-control" id="id_status"');


     $data['input_orden_magic'] = form_input('orden_magic',set_value('orden_magic',exist_obj($info_servicio,'orden_magic')),'class="form-control" id="orden_magic" ');
     $disabled_vigencia = ($id!=0)?'disabled':'';
     $data['input_inicio_vigencia'] = form_input('inicio_vigencia',(set_value('inicio_vigencia',exist_obj($info_servicio,'inicio_vigencia'))),'class="form-control" id="inicio_vigencia" '.$disabled_vigencia.'  ','date');

     $data['input_acepta_beneficios'] = form_checkbox('acepta_beneficios',$acepta_beneficios,($acepta_beneficios==1)?'checked':'','class="" id="acepta_beneficios" '.$disabled_vigencia.' ');
    
     //PAQUETES
     $data['drop_cilindro_id'] = form_dropdown('cilindro_id',array_combos($this->mcat->get('pkt_cat_cilindros','cilindros','asc'),'id','cilindros',TRUE),set_value('cilindro_id',exist_obj($info_servicio,'cilindro_id')),'class="form-control vehiculo_modelo" id="cilindro_id"');
     $data['drop_motor_id'] = form_dropdown('motor_id',array_combos($this->mcat->get('pkt_cat_motor','motor'),'id','motor',TRUE),set_value('motor_id',exist_obj($info_servicio,'motor_id')),'class="form-control busqueda vehiculo_modelo" id="motor_id"');
     $data['drop_transmision_id'] = form_dropdown('transmision_id',array_combos($this->mcat->get('pkt_cat_transmision','transmision'),'id','transmision',TRUE),set_value('transmision_id',exist_obj($info_servicio,'transmision_id')),'class="form-control vehiculo_modelo" id="transmision_id"');
     $data['drop_combustible_id'] = form_dropdown('combustible_id',array_combos($this->mcat->get('pkt_cat_combustibles','combustible'),'id','combustible',TRUE),set_value('combustible_id',exist_obj($info_servicio,'combustible_id')),'class="form-control vehiculo_modelo" id="combustible_id"');


     $data['fecha_recepcion'] = date2sql($fecha_recepcion);
    //FIN DE SERVICIO
    $this->blade->render('agendar_cita_servicio',$data);

  }
  public function agendar_cita_servicio(){
    if($this->input->post()){
      $id = $this->input->post('id');
      $id_cita = $this->input->post('id_cita');
      //Validar si la orden tiene artículos
      if(!$this->mc->validOrderArt()){
        echo -4;exit();
      }

      if($id_cita==0){
        $this->form_validation->set_rules('citas[vehiculo_placas]', 'placas', 'trim|required|min_length[5]');

        $this->form_validation->set_rules('citas[vehiculo_modelo]', 'modelo', 'trim|required');
        $this->form_validation->set_rules('citas[email]', 'email', 'trim|required|valid_email');

        $this->form_validation->set_rules('citas[datos_nombres]', 'nombre(s)', 'trim|required');
        $this->form_validation->set_rules('citas[datos_apellido_paterno]', 'apellido paterno', 'trim|required');
        $this->form_validation->set_rules('citas[datos_apellido_materno]', 'apellido materno', 'trim|required');
        $this->form_validation->set_rules('am_contacto', 'apellido materno', 'trim|required');
        $this->form_validation->set_rules('citas[asesor]', 'asesor', 'trim|required');
        $this->form_validation->set_rules('citas[id_opcion_cita]', 'donde realizó cita', 'trim');
        $this->form_validation->set_rules('citas[transporte]', 'transporte', 'trim');
        $this->form_validation->set_rules('citas[servicio]', 'servicio', 'trim');
        $this->form_validation->set_rules('citas[horario]', 'horario', 'trim');
        $this->form_validation->set_rules('citas[fecha]', 'fecha', 'trim');
        $this->form_validation->set_rules('citas[id_color]', 'color', 'trim|required');
        if($this->input->post('tecnico_dias')!=''){
          $this->form_validation->set_rules('fecha_inicio', 'fecha inicio', 'trim');
          $this->form_validation->set_rules('fecha_fin', 'fecha fin', 'trim');
        }
        if($this->input->post('id')==0 && $this->input->post('dia_completo')==null){
          $this->form_validation->set_rules('tecnico', 'técnico', 'trim|required');
          $this->form_validation->set_rules('hora_inicio', 'hora inicio', 'trim|required');
          $this->form_validation->set_rules('hora_fin', 'hora fin', 'required|trim');
        }
        if($this->input->post('id')==0 && $this->input->post('dia_completo')!=null){
          $this->form_validation->set_rules('tecnico_dias', 'técnico', 'trim|required');
        }

        $this->form_validation->set_rules('nombre_contacto_compania', 'nombre compañía', 'trim|required');
        $this->form_validation->set_rules('ap_contacto', 'apellido paterno', 'trim|required');
        $this->form_validation->set_rules('rfc', 'R.F.C.', 'trim|required');
        $this->form_validation->set_rules('correo_compania', 'correo', 'trim|valid_email|required');
        $this->form_validation->set_rules('calle', 'calle', 'trim|required');
        $this->form_validation->set_rules('noexterior', '# exterior', 'trim|required');
        $this->form_validation->set_rules('colonia', 'colonia', 'trim|required');
        $this->form_validation->set_rules('municipio', 'municipio', 'trim|required');
        $this->form_validation->set_rules('cp', 'C.P.', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('estado', 'estado', 'trim|required');
        $this->form_validation->set_rules('telefono_movil', 'teléfono', 'trim|required|exact_length[10]');
        $this->form_validation->set_rules('otro_telefono', 'otro teléfono', 'trim|exact_length[10]');
        $this->form_validation->set_rules('vehiculo_identificacion', '# identificación', 'trim');
        $this->form_validation->set_rules('vehiculo_kilometraje', 'kilometraje', 'trim|numeric|required|min_length[3]');
        $this->form_validation->set_rules('id_tipo_orden', 'tipo de orden', 'trim');
        $this->form_validation->set_rules('motor', 'motor', 'trim|required');
        $this->form_validation->set_rules('transimision', 'transimisión', 'trim');

        $this->form_validation->set_rules('fecha_recepcion', 'fecha recepción', 'trim|required');
        $this->form_validation->set_rules('hora_recepcion', 'hora recepción', 'trim|required');
        $this->form_validation->set_rules('fecha_entrega', 'fecha entrega', 'trim|required');
        $this->form_validation->set_rules('hora_entrega', 'hora entrega', 'trim|required');
      }else{
        $this->form_validation->set_rules('citas[email]', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('nombre_contacto_compania', 'nombre compañía', 'trim|required');
        $this->form_validation->set_rules('ap_contacto', 'apellido paterno', 'trim|required');
        $this->form_validation->set_rules('rfc', 'R.F.C.', 'trim|required');
        $this->form_validation->set_rules('citas[email]', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('correo_compania', 'correo', 'trim|valid_email|required');
        $this->form_validation->set_rules('calle', 'calle', 'trim|required');
        $this->form_validation->set_rules('noexterior', '# exterior', 'trim|required');
        $this->form_validation->set_rules('colonia', 'colonia', 'trim|required');
        $this->form_validation->set_rules('municipio', 'municipio', 'trim|required');
        $this->form_validation->set_rules('estado', 'estado', 'trim|required');
        $this->form_validation->set_rules('telefono_movil', 'teléfono', 'trim|required|exact_length[10]');
        $this->form_validation->set_rules('otro_telefono', 'otro teléfono', 'trim|exact_length[10]');
        $this->form_validation->set_rules('vehiculo_identificacion', '# identificación', 'trim');
        $this->form_validation->set_rules('vehiculo_kilometraje', 'kilometraje', 'trim|numeric|required');
        $this->form_validation->set_rules('id_tipo_orden', 'tipo de orden', 'trim');
        $this->form_validation->set_rules('transimision', 'transimisión', 'trim');

        $this->form_validation->set_rules('fecha_recepcion', 'fecha recepción', 'trim');
        $this->form_validation->set_rules('hora_recepcion', 'hora recepción', 'trim');
        $this->form_validation->set_rules('fecha_entrega', 'fecha entrega', 'trim|required');
        $this->form_validation->set_rules('hora_entrega', 'hora entrega', 'trim|required');
        $this->form_validation->set_rules('cp', 'C.P.', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('vehiculo_kilometraje', 'kilometraje', 'trim|numeric|required|min_length[3]');
      }

      $this->form_validation->set_rules('citas[vehiculo_numero_serie]', 'número de serie', 'exact_length[17]|required|trim');
      $this->form_validation->set_rules('citas[id_status_color]', 'estatus', 'trim');
      $this->form_validation->set_rules('telefono_asesor', 'teléfono', 'trim|numeric|exact_length[10]|required');
      //$this->form_validation->set_rules('agente', 'agente', 'trim|required');


      $this->form_validation->set_rules('id_status_intelisis', 'estatus', 'trim');//required
      $this->form_validation->set_rules('id_situacion_intelisis', 'situación', 'trim');//required
      $this->form_validation->set_rules('id_status', 'estatus orden', 'trim');//required
      $this->form_validation->set_rules('tipo_cliente', 'tipo de cliente', 'trim|required');//required
      

      $this->form_validation->set_rules('idcategoria', 'categoría', 'trim');
      $this->form_validation->set_rules('idsubcategoria', 'subcategoría', 'trim');
      $this->form_validation->set_rules('vehiculo_marca', 'marca', 'trim|required');
      $this->form_validation->set_rules('citas[vehiculo_anio]', 'modelo', 'trim|required');
      $this->form_validation->set_rules('id_tipo_orden', 'tipo de orden', 'trim|required');
      $this->form_validation->set_rules('id_tipo_operacion', 'tipo de operación', 'trim|required');
      $this->form_validation->set_rules('citas[id_status_color]', 'estatus', 'trim|required');
      
      $this->form_validation->set_rules('numero_interno', 'torre', 'trim|numeric');
      $this->form_validation->set_rules('id_uen', 'UEN', 'trim');
      $this->form_validation->set_rules('almacen', 'almacén', 'trim');
      $this->form_validation->set_rules('agente', 'agente', 'trim');
      $this->form_validation->set_rules('color', 'color', 'trim|required');
      $this->form_validation->set_rules('numero_interno', '# interno', 'trim|numeric|required');
      $this->form_validation->set_rules('extension_asesor', 'extensión', 'trim|required');
      if($this->input->post('id')==0 && ($_POST['id_tipo_orden']==1||$_POST['citas']['id_status_color']==5||$_POST['citas']['id_status_color']==6)){
        $this->form_validation->set_rules('inicio_vigencia', 'inicio de vigencia', 'trim|required'); //
      }

      //PAQUETES
      if($_POST['fecha_recepcion_validar'] < CONST_FECHA_CITA_PAQUETES){

        $this->form_validation->set_rules('cilindro_id', 'cilindros', 'trim');
        $this->form_validation->set_rules('transmision_id', 'transmisión', 'trim');
        $this->form_validation->set_rules('combustible_id', 'combustible', 'trim');
        $this->form_validation->set_rules('motor_id', 'motor', 'trim');


        $this->form_validation->set_rules('cilindros', 'cilindros', 'trim|numeric|required');
        $this->form_validation->set_rules('transmision', 'transmisión', 'trim|required');
        $this->form_validation->set_rules('motor', 'motor', 'trim|required');
      }else{

        $this->form_validation->set_rules('cilindros', 'cilindros', 'trim|numeric');
        $this->form_validation->set_rules('transmision', 'transmisión', 'trim');
        $this->form_validation->set_rules('motor', 'motor', 'trim');

        $this->form_validation->set_rules('cilindro_id', 'cilindros', 'trim|required');
        $this->form_validation->set_rules('transmision_id', 'transmisión', 'trim|required');
        $this->form_validation->set_rules('combustible_id', 'combustible', 'trim|required');
        $this->form_validation->set_rules('motor_id', 'motor', 'trim|required');
      }
      if ($this->form_validation->run()){
        $this->mc->guardarCitaServicio();
      }else{

          $errors = array(
            'transporte' => form_error('citas[transporte]'),
            'vehiculo_anio' => form_error('citas[vehiculo_anio]'),
            'vehiculo_placas' => form_error('citas[vehiculo_placas]'),
            'vehiculo_modelo' => form_error('citas[vehiculo_modelo]'),
            'vehiculo_numero_serie' => form_error('citas[vehiculo_numero_serie]'),
            'asesor' => form_error('citas[asesor]'),
            'fecha' => form_error('citas[fecha]'),
            'horario' => form_error('citas[horario]'),
            'email' => form_error('citas[email]'),

            'nombre' => form_error('citas[datos_nombres]'),
            'apellido_paterno' => form_error('citas[datos_apellido_paterno]'),
            'apellido_materno' => form_error('citas[datos_apellido_materno]'),
            'id_status_color' => form_error('citas[id_status_color]'),
            'telefono' => form_error('citas[datos_telefono]'),
            'servicio' => form_error('citas[servicio]'),
            'id_opcion_cita' => form_error('citas[id_opcion_cita]'),
            'id_color' => form_error('citas[id_color]'),
            'tecnico' => form_error('tecnico'),
            'tecnico_dias' => form_error('tecnico_dias'),
            'hora_inicio' => form_error('hora_inicio'),
            'hora_fin' => form_error('hora_fin'),
            'fecha_inicio' => form_error('fecha_inicio'),
            'fecha_fin' => form_error('fecha_fin'),
            // 'id_modelo_prepiking' => form_error('citas[id_modelo_prepiking]'),
            // 'id_prepiking' => form_error('citas[id_prepiking]'),

            'telefono_asesor'=> form_error('telefono_asesor'),
            'nombre_compania'=> form_error('nombre_compania'),
            'nombre_contacto_compania'=> form_error('nombre_contacto_compania'),
            'ap_contacto'=> form_error('ap_contacto'),
            'am_contacto'=> form_error('am_contacto'),
            'rfc'=> form_error('rfc'),
            'correo_compania'=> form_error('correo_compania'),
            'calle'=> form_error('calle'),
            'noexterior'=> form_error('noexterior'),
            'colonia'=> form_error('colonia'),
            'municipio'=> form_error('municipio'),
            'cp'=> form_error('cp'),
            'estado'=> form_error('estado'),
            'telefono_movil'=> form_error('telefono_movil'),
            'otro_telefono'=> form_error('otro_telefono'),
            'vehiculo_identificacion'=> form_error('vehiculo_identificacion'),
            'vehiculo_kilometraje'=> form_error('vehiculo_kilometraje'),
            'id_tipo_orden'=> form_error('id_tipo_orden'),
            'transimision'=> form_error('transimision'),
            //'agente'=> form_error('agente'),
            'id_status_intelisis'=> form_error('id_status_intelisis'),
            'id_situacion_intelisis'=> form_error('id_situacion_intelisis'),
            'id_status'=> form_error('id_status'),
            'fecha_recepcion'=> form_error('fecha_recepcion'),
            'hora_recepcion'=> form_error('hora_recepcion'),
            'fecha_entrega'=> form_error('fecha_entrega'),
            'hora_entrega'=> form_error('hora_entrega'),
            'tipo_cliente'=> form_error('tipo_cliente'),

            'idcategoria'=> form_error('idcategoria'),
            'idsubcategoria'=> form_error('idsubcategoria'),
            'vehiculo_marca'=> form_error('vehiculo_marca'),
            'vehiculo_anio'=> form_error('citas[vehiculo_anio]'),
            'id_tipo_orden'=> form_error('id_tipo_orden'),
            'id_tipo_operacion'=> form_error('id_tipo_operacion'),
            'id_status_color'=> form_error('citas[id_status_color]'),
            'numero_interno'=> form_error('numero_interno'),
            'id_uen'=> form_error('id_uen'),
            'almacen'=> form_error('almacen'),
            'agente'=> form_error('agente'),
            'color'=> form_error('color'),
            'numero_interno'=> form_error('numero_interno'),
            'extension_asesor'=> form_error('extension_asesor'),
            'inicio_vigencia'=> form_error('inicio_vigencia'),
            //PAQUETES
            'motor'=> form_error('motor'),
            'cilindros'=> form_error('cilindros'),
            'transmision'=> form_error('transmision'),

            'cilindro_id'=> form_error('cilindro_id'),
            'transmision_id'=> form_error('transmision_id'),
            'combustible_id'=> form_error('combustible_id'),
            'motor_id'=> form_error('motor_id'),

          );

        echo json_encode($errors); exit();
      }
    }
  }
  public function buscar_orden_servicio(){
      if($_POST['id_asesor']!=''){
        $this->db->where('c.asesor',$_POST['id_asesor']);
      }

      if($_POST['id_status_search']!=''){
        $this->db->where('o.id_status_intelisis',$_POST['id_status_search']);
      }
      if($_POST['id_situacion_intelisis_search']!=''){
        $this->db->where('o.id_situacion_intelisis',$_POST['id_situacion_intelisis_search']);
      }

      if($_POST['cita_previa']!=''){
        $this->db->where('c.cita_previa',$_POST['cita_previa']);
      }

      if($_POST['finicio']!=''){
        $this->db->where('tc.fecha >=',date2sql($_POST['finicio']));
      }
      if($_POST['ffin']!=''){
        $this->db->where('tc.fecha <=',date2sql($_POST['ffin']));
      }
      $data['orden'] =$this->db->select('o.*,c.*, csio.status as estatus_intelisis, csit.situacion as situacion_intelisis, cso.status as estatus_orden,cso.id as id_estatus_orden,cto.identificador')
                              ->join('citas c','c.id_cita=o.id_cita')
                              ->join('cat_status_intelisis_orden csio','o.id_status_intelisis = csio.id','left')
                              ->join('cat_situacion_intelisis csit','o.id_situacion_intelisis = csit.id','left')
                              ->join('cat_status_orden cso','cso.id = o.id_status','left')
                              ->join('cat_tipo_orden cto','o.id_tipo_orden=cto.id','left')
                              ->join('tecnicos_citas tc','c.id_cita=tc.id_cita')
                              ->order_by('o.id','desc')
                              ->get('ordenservicio o')
                              ->result();
    
      $data['mensajes_enviados'] = $this->mc->getMessageSend();
      $this->blade->set_data($data)->render('tabla_ordenes_servicio');

  }

  public function asignarOrden(){
      if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
      {
        $this->output->set_status_header('409');
        exit();
      }else if($this->session->userdata('id_usuario')==''){
        redirect('login');
      }

      $idorden = $this->input->get('idorden');

      if($this->input->post()){
          $this->form_validation->set_rules('descripcion_item', 'descripción', 'trim|required');
          $this->form_validation->set_rules('precio_item', 'precio', 'trim|numeric|required');
          $this->form_validation->set_rules('cantidad_item', 'cantidad', 'trim|numeric|required');
          $this->form_validation->set_rules('descuento_item', 'descuento', 'trim|numeric');
          $this->form_validation->set_rules('total_horas', 'total horas', 'trim|numeric');
          $this->form_validation->set_rules('costo_hora', 'costo hora', 'trim|numeric');
          $this->form_validation->set_rules('codigo', 'código', 'trim|required');
          if ($this->form_validation->run()){

            $datos_item_orden = array('descripcion' => $this->input->post('descripcion_item'),
                                      'idorden' => $this->input->post('idorden'),
                                      'precio_unitario' => $this->input->post('precio_item'),
                                      'cantidad' => $this->input->post('cantidad_item'),
                                      'descuento' => $this->input->post('descuento_item'),
                                      'total' => $this->input->post('total_item'),
                                      'id_usuario' => $this->session->userdata('id_usuario'),
                                      'created_at' => date('Y-m-d H:i:s'),
                                      'temporal' => $this->input->post('temporal_save'),
                                      'tipo'=>$this->input->post('tipo'), //particular (no es de grupos)
                                      'total_horas' => $this->input->post('total_horas'),
                                      'costo_hora' => $this->input->post('costo_hora'),
                                      'grupo' => $this->input->post('codigo'),
            );

            if($this->input->post('idoperacion_save')==0){
              if(isset($_POST['relacionar_op'])){
                $datos_item_orden['cotizacion_extra'] = 1;
              }
              $exito = $this->db->insert('articulos_orden',$datos_item_orden);
              $idarticulo = $this->db->insert_id();
              if(isset($_POST['relacionar_op'])){
               
                //Asignar la operación a la mano de obra
                if(isset($_POST['opext'])){
                  foreach ($_POST['opext'] as $o => $op) {
                    $this->db->where('id',$o)->set('id_operacion',$idarticulo)->update('presupuesto_multipunto');
                  }
                }
              }

            }else{
              $datos_item_orden['id_usuario_edito'] = $this->session->userdata('id_usuario');
              $exito = $this->db->where('id',$this->input->post('idoperacion_save'))->update('articulos_orden',$datos_item_orden);
              $idarticulo = $this->input->post('idoperacion_save');
              //Si id_status_op != 1 y != null o != '' quiere decir que todavia puede editar de lo contrario es que ya se hizo un cambio de estatus
              if($_POST['id_status_op']==''||$_POST['id_status_op']==null||$_POST['id_status_op']==1){
                 $this->db->where('id_cita',$_POST['id_cita'])->set('id_operacion',null)->where('id_operacion',$idarticulo)->update('presupuesto_multipunto');
                if(isset($_POST['opext'])){
                  foreach ($_POST['opext'] as $o => $op) {
                    $this->db->where('id',$o)->set('id_operacion',$idarticulo)->update('presupuesto_multipunto');
                  }
                }
              }
            }
            if($exito){
              echo 1;
            }else{
              echo 0;
            }
            exit();

          }else{
            $errors = array(
              'descripcion_item' => form_error('descripcion_item'),
              'precio_item' => form_error('precio_item'),
              'cantidad_item' => form_error('cantidad_item'),
              'descuento_item' => form_error('descuento_item'),
              'costo_hora' => form_error('costo_hora'),
              'codigo' => form_error('codigo'),
            );
            echo json_encode($errors); exit();
          }
        }

      $id = $_GET['idoperacion'];
      if($id==0){
        $info=new Stdclass();
        $valor_cantidad = 1;
         $data['cotizacion_extra'] = 0;  // Si es 1 se hizo con la relación a las partes
        $data['id_status'] = null;
      }else{
        $info= $this->mc->getOperacionId($id);
        $valor_cantidad = $info->cantidad;
         $data['cotizacion_extra'] = $info->cotizacion_extra;  // Si es 1 se hizo con la relación a las partes
        $data['id_status'] = $info->id_status;
      }


    $data['input_descripcion'] = form_input('descripcion_item',set_value('descripcion',exist_obj($info,'descripcion')),'class="form-control" id="descripcion_item" ');

    $data['input_precio'] = form_input('precio_item',set_value('precio',exist_obj($info,'precio_unitario')),'class="form-control contar_item cantidad" id="precio_item" ');

    $data['input_cantidad'] = form_input('cantidad_item',set_value('cantidad',$valor_cantidad),'class="form-control contar_item cantidad" id="cantidad_item" ');

    $data['input_descuento'] = form_input('descuento_item',set_value('descuento',exist_obj($info,'descuento')),'class="form-control contar_item cantidad" id="descuento_item" maxlength="2" ');

     $data['input_total_horas'] = form_input('total_horas',set_value('total_horas',exist_obj($info,'total_horas')),'class="form-control contar_item" id="total_horas" maxlength="6" ');

    $data['input_total'] = form_input('total_item',set_value('total',exist_obj($info,'total')),'class="form-control cantidad contar_item" id="total_item" readonly ');

    $data['input_costo_hora'] = form_input('costo_hora',CONST_COSTO_MANO_OBRA,'class="form-control contar_item" id="costo_hora" ');

    $data['drop_codigo'] = form_dropdown('codigo',array_combosCodigosGen($this->mcat->get('codigos_gen','codigo_ford'),'codigo_ford','codigo',TRUE),set_value('grupo',exist_obj($info,'grupo')),'class="form-control buscar" id="codigo" ');


    $data['idorden'] = $idorden;
    $data['id_cita'] = $this->mc->getIdCitaByOrden($idorden);
    $data['operaciones_extras'] = $this->mc->operacionesExtras($data['id_cita'],$_GET['idoperacion']);
    $data['temporal_save'] = $this->input->get('temporal');
    $data['tipo'] = $this->input->get('tipo');
    $data['idoperacion_save'] = $_GET['idoperacion'];
    $this->blade->render('item_orden',$data);
  }
   function random($num){
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        for ($i = 0; $i < $num; $i++) {
             $string .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $string;
    }
  public function getTablaItemServicios(){
    $tipo = $this->input->post('tipo'); //1 grupos 2 cotización externa
    $idorden = $this->input->post('idorden'); //1 grupos 2 cotización externa
    //if($this->input->post('idorden')=='' || $this->input->post('idorden')==0){
    $this->db->where('tipo',$tipo);
    $this->db->where('cancelado',0);

    if($tipo==1){
      if($idorden==0){
        $this->db->where('temporal',$this->input->post('temporal'));
      }else{
         $this->db->where('idorden',$this->input->post('idorden'));
      }
    }else{
      if($idorden==0){
        $this->db->where('temporal',$this->input->post('temporal'));
      }else{
         $this->db->where('idorden',$this->input->post('idorden'));
      }
    }

    //}else{
     // $this->db->where('idorden',$this->input->post('idorden'));
    //}
    $data['items'] = $this->db->get('articulos_orden')->result();
    $data['anticipo'] = $this->mc->getAnticipoOrden($tipo);
    $data['tipo'] = $tipo;
    $data['id_orden_save'] = $idorden;

    //debug_var($data);die();
    $this->blade->render('tabla_item_orden',$data);
  }
  public function getTablaItemServiciosOrden(){
    $idorden = $this->input->post('idorden'); //1 grupos 2 cotización externa
    $query = "select * from articulos_orden where (tipo=1 or tipo = 3 or tipo=4 or tipo=5)";
    if($idorden==0){
      $query = $query.' and cancelado = 0 and temporal = "'.$this->input->post('temporal').'"';
    }else{
      $query = $query.' and cancelado = 0 and idorden = "'.$this->input->post('idorden').'"';
    }

    $data['items'] = $this->db->query($query)->result();
    $data['anticipo'] = $this->mc->getAnticipoOrden(1); //Le mando 1 para que me traiga el anticipo de la orden, 2 sería multipunto
    $data['id_orden_save'] = $idorden;
    $this->blade->render('tabla_item_orden_complete',$data);
  }
  //Actualiza los totales de la orden
  public function actualizar_totales_orden(){
    //print_r($_POST);die();
    if($this->input->post('tipo')==1 || $this->input->post('tipo')==''){ //Grupos
      $datos_orden = array('total_orden'=>$this->input->post('total_orden'),//
                         'restante'=>(float)$this->input->post('total_temp_orden')-(float)$this->input->post('anticipo_orden'),
                         'anticipo'=>$this->input->post('anticipo_orden'),
                         'iva'=>$this->input->post('iva_orden')
      );
    }else{ //cotización extra
      $datos_orden = array('total_orden_multi'=>$this->input->post('total_temp_orden'),
                         'restante_multi'=>(float)$this->input->post('total_temp_orden')-(float)$this->input->post('anticipo_orden'),
                         'anticipo_multi'=>$this->input->post('anticipo_orden'),
                         'iva_multi'=>$this->input->post('iva_orden')
      );
    }

    $this->db->where('id',$this->input->post('id_orden_save'))->update('ordenservicio',$datos_orden);
    echo 1;die();

  }
  public function eliminarItem(){
    $q = $this->db->limit(1)->where('id',$this->input->post('item'))->select('id_tecnico')->get('articulos_orden');
    if($q->num_rows()==1){
      if($q->row()->id_tecnico!=''){
        echo -1;exit();
      }else{
        if($_POST['idorden']==0){
          //Aquí si se elimina por que quedarían registros sin razón de ser
          $this->db->where('id',$this->input->post('item'))->delete('articulos_orden');
        }else{
          $this->db->where('id',$this->input->post('item'))->set('cancelado',1)->update('articulos_orden');
        }
        
        $tipo = $this->input->post('tipo');
      }
    }else{
      if($_POST['idorden']==0){
        //Aquí si se elimina por que quedarían registros sin razón de ser
        $this->db->where('id',$this->input->post('item'))->delete('articulos_orden');
      }else{
        $this->db->where('id',$this->input->post('item'))->set('cancelado',1)->update('articulos_orden');
      }
      $tipo = $this->input->post('tipo');
    }
    echo 1;exit();
  }
  public function exportar_orden_pdf(){
    ini_set('memory_limit', '-1');
    $data['orden'] = $this->mexcel->exportarOrden();
    $vista= $this->blade->render('citas/reporte_orden',$data,TRUE);
    pdf_create_horizontal($vista, 'reporte_orden_'.date('Y-m-d'));
    $this->m_pdf->pdf->Output('reporte_orden.pdf', "D"); //D
  }
  public function exportar_orden_servicio($idorden=''){
    $data['items'] = $this->db->where('idorden',$idorden)->get('articulos_orden')->result();
    $data['orden'] = $this->db->where('o.id',$idorden)->join('citas c','c.id_cita=o.id_cita')->join('cat_tipo_pago cp','o.id_tipo_pago=cp.id','left')->select('o.*,c.*,cp.tipo_pago')->get('ordenservicio o')->row();
    $data['tipo_orden'] = $this->mc->getTipoOrden($data['orden']->id_tipo_orden);
    $data['tecnicos_citas'] = $this->db->where('tc.id_cita',$data['orden']->id_cita)->join('tecnicos t','tc.id_tecnico=t.id')->select('tc.*,t.nombre as tecnico')->get('tecnicos_citas tc')->result();
    $data['color'] = $this->mc->getColorCitaAuto($data['orden']->id_tipo_orden);
    $data['anticipo'] = $this->mc->getAnticipoOrden();
    $data['confirmada'] = $this->mc->EstatusCotizacionUser($idorden);
    $html = $this->blade->render("ordenservicio_pdf", $data, TRUE);
    $this->load->library('m_pdf');
    $this->m_pdf->pdf->AddPage('P', // L - landscape, P - portrait
    '', '', '', '',
    10, // margin_left
    10, // margin right
    1, // margin top
    10, // margin bottom
    10, // margin header
    10); // margin footer
    $this->m_pdf->pdf->WriteHTML($html);
    $carpeta = '';
    $this->m_pdf->pdf->Output(); //D

  }
  public function exportar_remision($idorden){
    //LAS TIPO 2 SON LAS QUE SE HACEN EXTRA Y DEBE ESTAR CONFIRMADA PARA QUE APAREZCAN
    //TIPO 3 SON DEL GRUPO GENERICO
    //TIPO 1 SON LAS QUE TIENEN GRUPO Y OPERACIÓN
    $confirmada= $this->mc->EstatusCotizacionUser($idorden);
    if($confirmada){
      $data['items'] = $this->db->where('idorden',$idorden)->where('tipo',2)->get('articulos_orden')->result();
    }else{
      $data['items'] = array();
    }

    //TIPO GEN
    $data['items_gen'] = $this->db->where('idorden',$idorden)->where('tipo',3)->get('articulos_orden')->result();

    $data['items_corr'] = $this->db->where('idorden',$idorden)->where('tipo',4)->get('articulos_orden')->result();

    $data['items_prev'] = $this->db->where('idorden',$idorden)->where('tipo',5)->get('articulos_orden')->result();

    $data['anticipo'] = $this->mc->getAnticipoOrden(2,$idorden);

   $data['items_grupos'] = $this->mc->getGruposAndOperaciones($idorden);

   //ITEMS DE LA MULTIPUNTO
  $id_cita = $this->mc->getIdCitaByOrden($idorden);
  //Verificamos si se imprimira en formulario o en pdf
  if (!isset($data["destinoVista"])) {
      $data["destinoVista"] = "Formulario";
  }

  if (!isset($data["tipoRegistro"])) {
      $data["tipoRegistro"] = "";
  }

  $query = $this->mc->get_result("idOrden",$id_cita,"cotizacion_multipunto");
  foreach ($query as $row){
      $data["registrosControl"] = $this->createCells(explode("|",$row->contenido));
      $data["subTotalMaterial"] = $row->subTotal;
      $data["ivaMaterial"] = $row->iva;
      $data["totalMaterial"] = $row->total;
      $data["firmaAsesor"] = $row->firmaAsesor;
      $data["archivo"] = explode("|",$row->archivos);
      $data["archivoTxt"] = $row->archivos;
      $data["aceptoTermino"] = $row->aceptoTermino;
      $data["refacciones"] = $row->aprobacionRefaccion;
  }

  $data["idOrden"] = $id_cita;
  //FIN ITEMS MULTIPUNTO
   //debug_var($data);die();
   $data['orden'] = $this->db->where('o.id',$idorden)->join('citas c','c.id_cita=o.id_cita')->join('cat_tipo_pago cp','o.id_tipo_pago=cp.id','left')->select('o.*,c.*,cp.tipo_pago,cto.identificador')->join('cat_tipo_orden cto','o.id_tipo_orden=cto.id','left')->get('ordenservicio o')->row();
    $data['tipo_orden'] = $this->mc->getTipoOrden($data['orden']->id_tipo_orden);

    $data['color'] = $this->mc->getColorCitaAuto($data['orden']->id_tipo_orden);
    $vista= $this->blade->render('citas/pdf_remision',$data,TRUE);
    //print_r($vista);die();
     $this->load->library('m_pdf');
     //generate the PDF from the given html
      $this->m_pdf->pdf->WriteHTML($vista);
      //download it.
      $carpeta = '';
      $this->m_pdf->pdf->Output('orden_remision.pdf', "D"); //D
  }
  //COTIZACIONES SHARA
   //Recuperamos la informacion de la cotizacion
    public function itemsMultipunto($idOrden = 0)
    {
        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        if (!isset($datos["tipoRegistro"])) {
            $datos["tipoRegistro"] = "";
        }

        $query = $this->mc->get_result("idOrden",$idOrden,"cotizacion_multipunto");
        foreach ($query as $row){
            $datos["registrosControl"] = $this->createCells(explode("|",$row->contenido));
            $datos["subTotalMaterial"] = $row->subTotal;
            $datos["ivaMaterial"] = $row->iva;
            $datos["totalMaterial"] = $row->total;
            $datos["firmaAsesor"] = $row->firmaAsesor;
            $datos["archivo"] = explode("|",$row->archivos);
            $datos["archivoTxt"] = $row->archivos;
            $datos["aceptoTermino"] = $row->aceptoTermino;
            $datos["refacciones"] = $row->aprobacionRefaccion;
        }

        $datos["idOrden"] = $idOrden;
        return $datos;
    }

  //Separamos las filas recibidas de la tabla por celdas
    public function createCells($renglones = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($renglones) {
            if (count($renglones)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($renglones) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$renglones[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
  }
  //FIN COTIZACIONES SHARA
  public function cantidad(){

  $numero = 13041111.22;
  $cambio = valorEnLetras($numero);


  echo "numero = $numero";
  echo "<br>";
  echo "cambio = $cambio";

  }

  //FIN DE SERVICIO

  //APP
  public function insertar_orden(){
      $data['diagnostico'] = $this->input->post('diagnostico');
      $data['fecha_elaboracion'] = $this->input->post('fecha_elaboracion');
      $data['fecha_aceptacion'] = $this->input->post('fecha_aceptacion');
      $data['nombre_asesor'] = $this->input->post('nombre_asesor');
      $data['nombre_consumidor'] = $this->input->post('nombre_consumidor');
      $data['llavero'] = $this->input->post('llavero');
      $data['seguro_rines'] = $this->input->post('seguro_rines');
      $data['indicadores_de_falla'] = $this->input->post('indicadores_de_falla');
      $data['rociadores_limpiaparabrisas'] = $this->input->post('rociadores_limpiaparabrisas');
      $data['claxon'] = $this->input->post('claxon');
      $data['luces_delanteras'] = $this->input->post('luces_delanteras');
      $data['luces_traseras'] = $this->input->post('luces_traseras');
      $data['luces_stop'] = $this->input->post('luces_stop');
      $data['radio_caratula'] = $this->input->post('radio_caratula');
      $data['pantallas'] = $this->input->post('pantallas');
      $data['aire_acondicionado'] = $this->input->post('aire_acondicionado');
      $data['encendedor'] = $this->input->post('encendedor');
      $data['vidrios'] = $this->input->post('vidrios');
      $data['espejos'] = $this->input->post('espejos');
      $data['seguros_electricos'] = $this->input->post('seguros_electricos');
      $data['disco_compacto'] = $this->input->post('disco_compacto');
      $data['asientos_vestiduras'] = $this->input->post('asientos_vestiduras');
      $data['tapete_cajuela'] = $this->input->post('tapete_cajuela');
      $data['herramienta'] = $this->input->post('herramienta');
      $data['gato_llave'] = $this->input->post('gato_llave');
      $data['reflejantes'] = $this->input->post('reflejantes');
      $data['cables'] = $this->input->post('cables');
      $data['extintor'] = $this->input->post('extintor');
      $data['llanta_refaccion'] = $this->input->post('llanta_refaccion');
      $data['tapones_rueda'] = $this->input->post('tapones_rueda');
      $data['gomas_limpiadores'] = $this->input->post('gomas_limpiadores');
      $data['antena'] = $this->input->post('antena');
      $data['tapon_gasolina'] = $this->input->post('tapon_gasolina');
      $data['poliza_garantia'] = $this->input->post('poliza_garantia');
      $data['manual'] = $this->input->post('manual');
      $data['seguro_rines2'] = $this->input->post('seguro_rines2');
      $data['certificacion_verificacion'] = $this->input->post('certificacion_verificacion');
      $data['tarjeta_circulacion'] = $this->input->post('tarjeta_circulacion');
      $data['nivel_gasolina'] = $this->input->post('nivel_gasolina');
      $data['articulos_personales'] = $this->input->post('articulos_personales');
      $data['cuales'] = $this->input->post('cuales');
      $data['desea_reportar_algo_mas'] = $this->input->post('desea_reportar_algo_mas');
      $data[''] = $this->input->post('numero_orden');
      $this->db->insert('ordenes_app', $data);
      $id_orden=  $this->db->insert_id();
      $response['error'] = 0;
      $response['id'] = $id_orden;
      echo json_encode($response);
    }
  //FIN APP



  //VER CITAS PAGINADO

  public function ver_all_citas()
  {
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $this->blade->render('ver_all_citas');
  }
  public function getDatosAllCitas(){
    $datos = $_POST;
    $mismo = $datos['draw'];
    $pagina = $datos['start'];
    $porpagina = $datos['length'];

    //if(isset($_POST['search']['value'])){
    if(isset($_POST['buscar_campo'])){
      $busqueda = $_POST['buscar_campo'];
      $this->filtros_all_citas($busqueda);
    }

    $total = $this->db->join('aux a','c.id_horario = a.id','left')->join('tecnicos t','c.id_tecnico=t.id','left')->join('cat_transporte ct','c.transporte=ct.id','left')->join('cat_opcion_citas co','co.id=c.id_opcion_cita','left')->join('cat_colores cc','cc.id=c.id_color','left')->where('c.activo',1)->where('c.historial',0)->select('count(id_cita) as total')->get('citas c')->row()->total;

    if(isset($_POST['buscar_campo'])){
      $busqueda = $_POST['buscar_campo'];
      $this->filtros_all_citas($busqueda);

    }
    $info = $this->db->from('citas c')->order_by('fecha_hora','desc')->join('aux a','c.id_horario = a.id','left')->join('tecnicos t','c.id_tecnico=t.id','left')->join('cat_transporte ct','c.transporte=ct.id','left')->join('cat_opcion_citas co','co.id=c.id_opcion_cita','left')->join('cat_colores cc','cc.id=c.id_color','left')->where('c.activo',1)->where('c.historial',0)->limit($porpagina,$pagina)->get()->result();
    $data = array();
    foreach ($info as $key => $value) {
        $dat = array();

            $fecha_mostrar = $value->fecha_dia.'/'.$value->fecha_mes.'/'.$value->fecha_anio;
            $fecha_comparar = $value->fecha_anio.'-'.$value->fecha_mes.'-'.$value->fecha_dia;

            if($value->origen==0){
              $origen = 'Panel';
            }elseif($value->origen==1){
              $origen = 'Aplicación';
            }else{
              $origen = 'Tablero';
            }

            $dat[] = $value->id_cita ;
            $dat[] = $value->transporte ;
            $dat[] = $value->comentarios_servicio;
            $dat[] = $value->vehiculo_anio ;
            $dat[] = '<span title="'.$value->vehiculo_modelo.'">'.trim_text($value->vehiculo_modelo,1).'</span>';

            $dat[] = $value->vehiculo_placas ;
            $dat[] = $value->vehiculo_numero_serie;
            $dat[] = $value->servicio ;
            $dat[] = $value->asesor ;
            $dat[] = $value->nombre ;

            $dat[] = $fecha_mostrar.' '.$value->hora;

            $dat[] = $value->datos_nombres ;
            $dat[] = $value->datos_apellido_paterno ;
            $dat[] = $value->datos_apellido_materno ;
            $dat[] = $value->datos_telefono ;
            $dat[] = $value->opcion ;
            $dat[] = $value->color ;
            $dat[] = $origen ;
            $dat[] = ($value->reagendada==1)?'Reagendada':'normal';

        $acciones = '';

        if(PermisoAccion('editar_cita')){
        $acciones.= '<a href="'.site_url('/citas/agendar_cita/'.$value->id_cita).'"  class="pe pe-7s-note" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar" data-id_cita="'.$value->id_cita.'" ></a>';
        }

        if(PermisoAccion('eliminar_cita')){
          $acciones.= ' <a href="" data-id="'.$value->id_cita.'" class="js_eliminar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="pe pe-7s-trash"></i></a>';
        }  

        if(PermisoAccion('cancelar_cita')){
          $acciones.= '<a href="" data-id="'.$value->id_cita.'" class=" js_cancelar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Cancelar cita"><i class="pe pe-7s-close-circle"></i></a>';
        }
        if($value->confirmada){
          $acciones.= '<a href="" data-id="'.$value->id_cita.'" class=" js_confirmar" data-valor="0" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Confirmar"><i class="pe pe-7s-switch"></i></a>';
        }else{
          $acciones.= '<a href="" data-id="'.$value->id_cita.'" class="js_confirmar" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="No confirmada"><i class="pe pe-7s-switch"></i></a>';
        }

        $dat[] = $acciones;
        $data[] = $dat;
    }
    //debug_var($data);die();

        $retornar = array('draw' => intval($mismo),
                        'recordsTotal' => intval($total),
                        'recordsFiltered' => intval($total),
                        'data' => $data);
    $this->output->set_content_type('application/json')->set_output(json_encode($retornar));
  }
  public function filtros_all_citas($busqueda=''){
     if($busqueda!=''){

      $this->db->or_like('id_cita',$busqueda);
      $this->db->or_like('c.transporte',$busqueda);
      $this->db->or_like('comentarios_servicio',$busqueda);
      $this->db->or_like('vehiculo_anio',$busqueda);
      $this->db->or_like('vehiculo_modelo',$busqueda);
      $this->db->or_like('vehiculo_placas',$busqueda);
      $this->db->or_like('vehiculo_numero_serie',$busqueda);
      $this->db->or_like('servicio',$busqueda);
      $this->db->or_like('asesor',$busqueda);
      $this->db->or_like('nombre',$busqueda);
      //$this->db->or_like('fecha_mostrar',$busqueda);
      $this->db->or_like('datos_nombres',$busqueda);
      $this->db->or_like('datos_apellido_paterno',$busqueda);
      $this->db->or_like('datos_apellido_materno',$busqueda);
      $this->db->or_like('datos_telefono',$busqueda);
      $this->db->or_like('opcion',$busqueda);
      $this->db->or_like('color',$busqueda);
      $this->db->or_like('origen',$busqueda);
      $this->db->or_like('reagendada',$busqueda);

     }
  }

  //TABLERO ESPERA
  public function tablero_espera(){

  //Sacar las citas con el status 11 que ya pasaron los 30 min
    $query_citas = "select id_cita from citas where id_status = 11
    and fecha_termino_lavado + INTERVAL 30 MINUTE < NOW();";

    $id_citas = $this->db->query($query_citas)->result();
    if(count($id_citas)>0){
      $cadena = '';
      foreach ($id_citas as $key => $value) {
        $cadena.= $value->id_cita.',';
      }
      $cadena = substr($cadena,0,-1);
      $this->db->where_not_in('c.id_cita',$cadena);
    }

  $data['citas'] = $this->db->select('c.*,os.status_lavado,a.id_status_cita as id_status_asesor,cst.porcentaje,os.fecha_entrega,os.hora_entrega,cst.status as estatus_tecnico,cat.categoria, subcat.subcategoria')->from('citas c')->order_by('fecha_hora','desc')->join('aux a','c.id_horario = a.id','left')->join('tecnicos t','c.id_tecnico=t.id','left')->join('cat_transporte ct','c.transporte=ct.id','left')->join('cat_opcion_citas co','co.id=c.id_opcion_cita','left')->join('cat_colores cc','cc.id=c.id_color','left')->where('c.activo',1)->where('c.historial',0)->where('c.fecha',date('Y-m-d'))->join('ordenservicio os','os.id_cita=c.id_cita','left')->order_by('c.fecha','desc')->join('cat_status_citas_tecnicos cst','cst.id=c.id_status','left')->join('categorias cat','cat.id = os.idcategoria','left')->join('subcategorias subcat','subcat.id = os.idsubcategoria','left')->get()->result();
  // echo $this->db->last_query();die();
    //debug_var($data);die();
    $data['fecha'] = date('Y-m-d');
    $this->blade->render('tablero_espera',$data);
  }
  public function buscar_tablero_espera(){
    $fecha = date2sql($this->input->post('fecha'));

    //Sacar las citas con el status 11 que ya pasaron los 30 min
    $query_citas = "select id_cita from citas where id_status = 11
    and fecha_termino_lavado + INTERVAL 30 MINUTE < NOW();";

    $id_citas = $this->db->query($query_citas)->result();
    if(count($id_citas)>0){
      $cadena = '';
      foreach ($id_citas as $key => $value) {
        $cadena.= $value->id_cita.',';
      }
      $cadena = substr($cadena,0,-1);
      $this->db->where_not_in('c.id_cita',$cadena);
    }

    $data['citas'] = $this->db->select('c.*,,os.status_lavado,a.id_status_cita as id_status_asesor,cst.porcentaje,os.fecha_entrega,os.hora_entrega,cst.status as estatus_tecnico,cat.categoria, subcat.subcategoria')->from('citas c')->order_by('fecha_hora','desc')->join('aux a','c.id_horario = a.id','left')->join('tecnicos t','c.id_tecnico=t.id','left')->join('cat_transporte ct','c.transporte=ct.id','left')->join('cat_opcion_citas co','co.id=c.id_opcion_cita','left')->join('cat_colores cc','cc.id=c.id_color','left')->where('c.activo',1)->where('c.historial',0)->where('c.fecha',$fecha)->join('ordenservicio os','os.id_cita=c.id_cita','left')->order_by('c.fecha','desc')->join('cat_status_citas_tecnicos cst','cst.id=c.id_status','left')->join('categorias cat','cat.id = os.idcategoria','left')->join('subcategorias subcat','subcat.id = os.idsubcategoria','left')->get()->result();
    //echo $this->db->last_query();die();
    $data['fecha'] = $fecha;
    $this->blade->render('tabla_tablero_espera',$data);
  }
  //Obtiene la serie de la tabla de citas o magic en base a la placa
  public function getSerie(){
     echo $this->mc->getSerieLarga($this->input->post('placas'));
  }
  //FIN ESPERA

  //FIN CITAS PAGINADO

  //LISTA PARA ENTREGAR UNIDADES DEL ASESOR
  public function unidades_por_entregar(){
    //Trae las citas por asesor pendientes de entregar
    $this->db->where('c.id_asesor',$this->session->userdata('id_usuario'));
        $data['citas'] = $this->db->from('citas c')->join('aux a','c.id_horario = a.id','left')->join('tecnicos t','c.id_tecnico=t.id','left')->where('c.activo',1)->where('c.historial',0)->where('c.origen',2)->where('c.id_status',3)->get()->result();
    $this->blade->render('lista_unidades_entregar',$data);
  }

  //fin lista aprobar


  //PROACTIVO

  public function proactivo_intelisis(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $this->get_contacto_proactivo();
  }
  public function get_contacto_proactivo()
  {
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    //$datos['lista'] = $this->mc->getOutbound();
    //debug_var($datos['lista']);die();
    $this->blade->render('contacto_proactivo_intelisis');
  }
  public function getDatosProactivoIntelisis(){
    $datos = $_POST;
    //print_r($_POST);die();
    //print_r($_POST['search']['value']);die();
    $mismo = $datos['draw'];
    $pagina = $datos['start'];
    $porpagina = $datos['length'];

    //if(isset($_POST['search']['value'])){
    if(isset($_POST['buscar_campo'])){
      $busqueda = $_POST['buscar_campo'];
      $this->filtros_proactivo_intelisis($busqueda);
    }

    $total = $this->db->select('count(id) as total')->get('proactivo')->row()->total;

    //echo $todos;die();
    //if(isset($_POST['search']['value'])){
    if(isset($_POST['buscar_campo'])){
      $busqueda = $_POST['buscar_campo'];
      $this->filtros_proactivo_intelisis($busqueda);

    }
    //echo 'entre';die();

    $info = $this->db->from('proactivo')->limit($porpagina,$pagina)->get()->result();

    $data = array();
    foreach ($info as $key => $value) {
      $dat = array();

      $dat[] = $value->id ;
      $dat[] = $value->fecha_emision ;
      $dat[] = $value->articulo ;
      $dat[] = $value->cliente ;
      $dat[] = $value->correo ;
      $dat[] = $value->telefono ;


      $acciones = '';
      $acciones.= '<a href="" data-id="'.$value->id.'" class="pe-7s-comment js_comentarios" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Comentarios"></a>
        <a href="" data-id="'.$value->id.'" class="pe-7s-info
pe pe-7s-info js_historial" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Historial comentarios"></a>';

      $dat[] = $acciones;
      $data[] = $dat;
    }
    //debug_var($data);die();

    $retornar = array('draw' => intval($mismo),
                        'recordsTotal' => intval($total),
                        'recordsFiltered' => intval($total),
                        'data' => $data);
    $this->output->set_content_type('application/json')->set_output(json_encode($retornar));
  }
  public function filtros_proactivo_intelisis($busqueda=''){
     if($busqueda!=''){

      $this->db->or_like('fecha_emision',$busqueda);
      $this->db->or_like('articulo',$busqueda);
      $this->db->or_like('cliente',$busqueda);
      $this->db->or_like('correo',$busqueda);
      $this->db->or_like('telefono',$busqueda);

     }
  }

  public function changeProactivo(){
    $proactivo = $this->db->get('proactivo2')->result();
    foreach ($proactivo as $key => $value) {
     $datos = array('fecha_emision' => date2sql($value->fecha_emision),
                    'articulo' => $value->articulo,
                    'cliente' => $value->cliente,
                    'correo' => $value->correo,
                    'telefono' => $value->telefono,
                    'mes'=> '08'

      );
     $this->db->insert('proactivo',$datos);
    }
  }
  public function comentarios_Proactivo_Intelisis(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
       if($this->input->post()){
       $this->form_validation->set_rules('comentario', 'comentario', 'trim|required');
       if ($this->form_validation->run()){
          $this->mc->saveComentarioProactIntelisis();
          exit();
          }else{
             $errors = array(
                  'comentario' => form_error('comentario'),
             );
            echo json_encode($errors); exit();
          }
      }
      $data['id_cita'] = $this->input->get('id_proactivo');
      $data['input_comentario'] = form_textarea('comentario',"",'class="form-control" id="comentario" rows="2"');

      $this->blade->render('citas/comentario_reagendar_magic',$data);
  }
  public function historial_comentarios_proactivo_intelisis(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    //print_r($_POST);die();
    $data['comentarios'] = $this->mc->getComentariosProactivo_intelisis($this->input->get('id_proactivo'));
    $data['tipo_reagendar_proact'] = $this->input->get('tipo_reagendar_proact');
     $this->blade->render('citas/historial_comentario_reagendar_magic',$data);

  }

  //FIN INTELISIS


  //AGREGAR LOS ITEM DE LA ORDEN POR SU CODIGO
  //Cuando funciona con base a un código
  public function asignarOrdenFull(){
      if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
      {
        $this->output->set_status_header('409');
        exit();
      }else if($this->session->userdata('id_usuario')==''){
        redirect('login');
      }

      $idorden = $this->input->get('idorden');

      //
      $data['drop_unidades'] = form_dropdown('unidad',array_combos($this->mcat->get('modelos_ford','modelo'),'modelo','modelo',TRUE),"",'class="form-control busqueda" id="unidad" ');

      $data['grupos'] = $this->db->order_by('clave')->get('grupos')->result();

      $data['drop_operacion'] = form_dropdown('operacion',array(),"",'class="form-control busqueda" id="operacion" ');

    $data['idorden'] = $idorden;
    $this->blade->render('item_orden_full',$data);
  }

  public function getOperacionesByGroup(){
    if($this->input->post()){
      $idgrupo=$this->input->post("idgrupo");
      $operaciones = $this->db->where('idgrupo',$idgrupo)->get('grupo_operacion')->result();
      //echo $this->db->last_query();die();
      //debug_var($operaciones);die();
      echo json_encode($operaciones);
    }else{
    echo 'Nada';
    }

  }
  public function getPrecioServicio(){
    //print_r($_POST);die();
    $this->db->where('UNIDAD',$this->input->post('unidad'));
    $datos['precios_servicio'] = $this->db->where('codigo_intermagic',$this->input->post('codigo_intermagic'))->get('precios_servicio')->result();
    //echo $this->db->last_query();die();
    $this->blade->render('tabla_lista_precios_servicio',$datos);
  }
  public function saveAllItemsOrden(){
    //print_r($_POST);die();
      if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
      {
        $this->output->set_status_header('409');
        exit();
      }else if($this->session->userdata('id_usuario')==''){
        redirect('login');
      }
      $idorden = $this->input->get('idorden');
      if($this->input->post()){
        $this->form_validation->set_rules('grupo', 'grupo', 'trim|required');
        $this->form_validation->set_rules('operacion', 'operación', 'trim|required');
        $this->form_validation->set_rules('unidad', 'unidad', 'trim|required');

        if ($this->form_validation->run()){
          $precios_servicio = $this->db->where('codigo_intermagic',$this->input->post('operacion'))->where('UNIDAD',$this->input->post('unidad'))->get('precios_servicio')->result();
          //Eliminar si ya existe
          //$this->db->where('operacion',$this->input->post('operacion'))->delete('articulos_orden');
          foreach ($precios_servicio as $key => $precios) {
              $datos_item_orden = array(
                'descripcion' => $precios->descripcion,
                'idorden' => $this->input->post('idorden'),
                'precio_unitario' => $precios->precio_unitario,
                'cantidad' =>$precios->cantidad,
                'descuento' => 0,
                'total' => $precios->total,
                'id_usuario' => $this->session->userdata('id_usuario'),
                'created_at' => date('Y-m-d H:i:s'),
                'temporal' => $this->session->userdata('temporal_orden'),
                'operacion'=>$this->input->post('operacion'),
                'grupo'=>$this->input->post('grupo'),
                'unidad'=>$this->input->post('unidad'),
                'tipo'=>1 //es grupos
              );
            $exito = $this->db->insert('articulos_orden',$datos_item_orden);
          }
          echo 1;
          exit();

        }else{
          $errors = array(
            'grupo' => form_error('grupo'),
            'operacion' => form_error('operacion'),
            'unidad' => form_error('unidad'),
          );
          echo json_encode($errors); exit();
        }
      }


  }
  public function validarCampania(){
    $q = $this->db->where('vin',$this->input->post('serie'))->get('campanias')->result();
    if(count($q)==0){
      echo json_encode(array('registros'=>false));
    }else{
      $cadena = '';
      foreach ($q as $key => $value) {
        $cadena.=$value->tipo.',';
      }
      $cadena = substr($cadena,0,-1);
      echo json_encode(array('registros'=>true,'datos'=>$cadena));
    }
  }
  //FIN
  public function html2pdf($idorden=''){
     $data['items'] = $this->db->where('idorden',$idorden)->get('articulos_orden')->result();
    $data['orden'] = $this->db->where('o.id',$idorden)->join('citas c','c.id_cita=o.id_cita')->join('cat_tipo_pago cp','o.id_tipo_pago=cp.id','left')->select('o.*,c.*,cp.tipo_pago')->get('ordenservicio o')->row();
    //echo $this->db->last_query();die();
    $data['tipo_orden'] = $this->mc->getTipoOrden($data['orden']->id_tipo_orden);
    $data['tecnicos_citas'] = $this->db->where('tc.id_cita',$data['orden']->id_cita)->join('tecnicos t','tc.id_tecnico=t.id')->select('tc.*,t.nombre as tecnico')->get('tecnicos_citas tc')->result();
    $data['color'] = $this->mc->getColorCitaAuto($data['orden']->id_tipo_orden);
    //debug_var($data['orden']);die();

            $html2pdf = new Html2Pdf('P', 'Legal', 'en',TRUE,'UTF-8',NULL);
        try {
            $html = $this->blade->render("html2pdf", $data, TRUE);

            $this->load->library('m_pdf');

            //print_r($html);die();
            $this->m_pdf->pdf->AddPage('P', // L - landscape, P - portrait
            '', '', '', '',
            10, // margin_left
            10, // margin right
            1, // margin top
            10, // margin bottom
            10, // margin header
            10); // margin footer
            $this->m_pdf->pdf->WriteHTML($html);
            $carpeta = '';

            $this->m_pdf->pdf->Output(); //D

            //print_r($html);die();
            //$vista= $this->blade->render('citas/pdf_ordenservicio',$data,TRUE);
            // $this->blade->render('html2pdf');
            // $html2pdf->setDefaultFont('Arial');     //Helvetica
            // $html2pdf->WriteHTML($html);
            // $html2pdf->setTestTdInOnePage(FALSE);
            // $html2pdf->Output();
        } catch (Html2PdfException $e) {
            $html2pdf->clean();
            $formatter = new ExceptionFormatter($e);
            echo $formatter->getHtmlMessage();
        }

  }
  public function entrega_lavado(){
    $id_cita = $this->input->post('id_cita');
    $usuario = $this->input->post('usuario');
    $status = $this->input->post('status'); // 8 Lavado planeado 10 Lavando 11 Lavado Terminado

     //GUARDAR TRANSICIONES DE LOS ESTATUS DE ASESOR
    //print_r($_POST);die();
    //Verificar si no existe la transaccion traerme la de creación de la cita
    //NUEVA VERSIÓN
    $inicio = $this->mc->getLastTransition($id_cita);
    $datos_transiciones = array('status_anterior' => $this->mc->getLastStatusTransiciones($id_cita),
                                'status_nuevo'=> $this->mc->getStatusTecnicoTransiciones($status),
                                'fecha_creacion'=>date('Y-m-d H:i:s'),
                                'tipo'=>2, //técnico,
                                'inicio'=>$inicio,
                                'fin'=>date('Y-m-d H:i:s'),
                                'usuario'=>$usuario,
                                'id_cita'=>$id_cita,
                                'general'=>1,
                                'minutos_transcurridos'=>dateDiffMinutes($inicio , date('Y-m-d H:i:s')),
                                'islavado'=>1,
    );
    $this->db->where('id_cita',$id_cita)->set('id_status',9)->update('citas');
    $this->db->where('id_cita',$id_cita)->set('JobCompletionDate',date('Y-m-d H:i:s'))->update('ordenservicio');
    $this->db->insert('transiciones_estatus',$datos_transiciones);
    //ENVIAR MENSAJE
    $datos_cita = $this->mc->getDatosCita($id_cita);
    $celular_sms = $this->mc->telefono_contacto($id_cita);
    $data_orden = $this->principal->getGeneric('id_cita',$id_cita,'ordenservicio');
    $mensaje_sms = CONST_AGENCIA_MENSAJE.", Unidad lista para entrega, orden #".$id_cita." placas: ".$datos_cita->vehiculo_placas;
    if($data_orden->autorizacion_ausencia&&!$data_orden->firma_diagnostico){
      $mensaje_sms = CONST_AGENCIA_MENSAJE.", Unidad lista, duración de entrega 30min, Aprox, orden #".$id_cita." placas: ".$datos_cita->vehiculo_placas;
    }

    if($data_orden->autorizacion_ausencia&&!$data_orden->firma_diagnostico){
      $url_firma = CONST_URL_BASE.'Documentacion_PorFirmar/'.encrypt($id_cita);
      $cuerpo = $this->blade->render('correo_firmas_cliente',array('url_firma'=>$url_firma,"id_cita"=>$id_cita),true);
      enviar_correo($datos_cita->datos_email,"Firmas del cliente!",$cuerpo,array());
    }
    $this->sms_general_texto($celular_sms,$mensaje_sms);
  }
  public function saveTransicionLavado(){
    $id_cita = $this->input->post('id_cita');
    $usuario = $this->input->post('usuario');
    $status = $this->input->post('status'); // 8 Lavado planeado 10 Lavando 11 Lavado Terminado
     //GUARDAR TRANSICIONES DE LOS ESTATUS DE ASESOR

    //Verificar si no existe la transaccion traerme la de creación de la cita
    $inicio = $this->mc->getLastTransition($id_cita);
    $datos_transiciones = array('status_anterior' => $this->mc->getLastStatusTransiciones($id_cita),
                                'status_nuevo'=> $this->mc->getStatusTecnicoTransiciones($status),
                                'fecha_creacion'=>date('Y-m-d H:i:s'),
                                'tipo'=>2, //técnico,
                                'inicio'=>$inicio,
                                'fin'=>date('Y-m-d H:i:s'),
                                'usuario'=>$usuario,
                                'id_cita'=>$id_cita,
                                'general'=>1,
                                'minutos_transcurridos'=>dateDiffMinutes($inicio , date('Y-m-d H:i:s')),
                                'islavado'=>1
    );
    $this->db->insert('transiciones_estatus',$datos_transiciones);
    $datos_cita = $this->mc->getDatosCita($_POST['id_cita']);
    if($status==11){
      $this->db->where('id_cita',$id_cita)->set('JobCompletionDate',date('Y-m-d H:i:s'))->update('ordenservicio');
      correo_fin_reparacion($datos_cita->datos_email,encrypt($id_cita));
    }else{
      correo_cambio_estatus($datos_cita->datos_email,encrypt($_POST['id_cita']),'',$datos_transiciones['status_nuevo']);
    }
  }
  public function status_lavado(){
    $id_cita = $this->input->post('id_cita');
    $status_lavado = $this->input->post('status_lavado');

    $this->db->where('id_cita',$id_cita)->set('status_lavado',$status_lavado)->update('ordenservicio');
  }
  public function generar_cotizacion($id_orden=''){
    $data['idorden'] =$id_orden;
    $this->blade->set_data($data)->render('generar_cotizacion');

  }
  public function login_general(){
       if($this->input->post()){
        $this->form_validation->set_rules('usuario', 'usuario', 'trim|required');
        $this->form_validation->set_rules('password', 'contraseña', 'trim|required');

       if ($this->form_validation->run()){
          $this->mc->validarLoginGeneral();
          }else{
             $errors = array(
                  'usuario' => form_error('usuario'),
                  'password' => form_error('password'),
             );
            echo json_encode($errors); exit();
          }
      }


      $data['input_usuario'] = form_input('usuario',$this->input->get('usuario_tecnico'),'class="form-control" rows="5" id="usuario" ');

     $data['input_password'] = form_password('password',"",'class="form-control" rows="5" id="password" ');

    $this->blade->render('login_general',$data);
  }
  public function enviar_cotizacion(){
    $idorden = $this->input->post('idorden');

    //Validar que exista una cotización
    if(!$this->mc->validarCotizacionExtra($idorden)){
      //echo -2;exit();
    }
    $this->db->where('idorden',$idorden);
    $data['items'] = $this->db->where('tipo',2)->get('articulos_orden')->result();
    //echo $this->db->last_query();die();
    $data['anticipo'] = $this->mc->getAnticipoOrden(2,$idorden);
    //echo $this->db->last_query();die();
    $html = $this->blade->render("pdf_cotizacion", $data, TRUE);
    //print_r($html);die();
    $this->load->library('m_pdf');
    $this->m_pdf->pdf->AddPage('P', // L - landscape, P - portrait
    '', '', '', '',
    10, // margin_left
    10, // margin right
    1, // margin top
    10, // margin bottom
    10, // margin header
    10); // margin footer
    $this->m_pdf->pdf->WriteHTML($html);
    $archivo = 'cotizaciones/'.date('YmdHis').$idorden.'.pdf';
    $this->m_pdf->pdf->Output($archivo); //D
    $id_cita = $this->mc->getIdCitaByOrden($idorden);

    $correo_consumidor = $this->mc->correo_consumidor($id_cita);
    $correo_compania = $this->mc->correo_compania($id_cita);
    //var_dump($correo_consumidor,$correo_compania);die();
    $cuerpo = $this->blade->render('enviar_cotizacion',array('idorden'=>$idorden,"id_cita"=>$id_cita),true);
    if($correo_consumidor!= $correo_compania){
      if($correo_consumidor!=''){
      enviar_correo($correo_consumidor,"¡Cotización!",$cuerpo,array());
      }
      if($correo_compania!=''){
        enviar_correo($correo_compania,"¡Cotización!",$cuerpo,array());
      }
    }else{
       enviar_correo($correo_consumidor,"¡Cotización!",$cuerpo,array());
    }

    //Actualizar que ya se envió el mensaje de esa orden
    $datos_mensaje_enviado = array('tipo_mensaje' =>'email','idorden'=>$idorden,'enviado'=>1 );
    $this->db->insert('mensajes_enviados_orden',$datos_mensaje_enviado);
    echo 1;exit();

  }
  public function enviar_cotizacion_multipunto(){
    $idorden = $this->input->post('idorden');
    $id_cita = $this->mc->getIdCitaByOrden($idorden);
    $correo_consumidor = $this->mc->correo_consumidor($id_cita);
    $correo_compania = $this->mc->correo_compania($id_cita);
    if(!$this->mc->firmoAsesor($id_cita)){
      echo -1;exit();
    }
    //$cuerpo = $this->blade->render('enviar_cotizacion_multipunto',array('idorden'=>$idorden,"id_cita"=>$id_cita,"url"=>$this->input->post('url')),true);
    if($correo_consumidor!= $correo_compania){
      if($correo_consumidor!=''){
      correo_presupuesto($correo_consumidor,encrypt($id_cita));
      }
      if($correo_compania!=''){
        correo_presupuesto($correo_compania,encrypt($id_cita));
      }
    }else{
      correo_presupuesto($correo_consumidor,encrypt($id_cita));
    }

    //Actualizar que ya se envió el sms de esa orden
    $datos_mensaje_enviado = array('tipo_mensaje' =>'email_cotizacion_multipunto','idorden'=>$idorden,'enviado'=>1 );
    $datos_mensaje_enviado['created_at'] = date('Y-m-d H:i:s');
    $this->db->insert('mensajes_enviados_orden',$datos_mensaje_enviado);
    echo 1;exit();

  }
  public function visualizar_cotizacion($idorden=''){

    //$id_cita = $this->mc->getIdCitaByOrden($idorden);
    $this->db->where('idorden',$idorden);
    $data['items'] = $this->db->where('tipo',2)->get('articulos_orden')->result();
    //debug_var($data);die();
    $data['anticipo'] = $this->mc->getAnticipoOrden(2,$idorden);
    $html = $this->blade->render("pdf_cotizacion", $data, TRUE);
    //print_r($html);die();
    $this->load->library('m_pdf');
    $this->m_pdf->pdf->AddPage('P', // L - landscape, P - portrait
    '', '', '', '',
    10, // margin_left
    10, // margin right
    1, // margin top
    10, // margin bottom
    10, // margin header
    10); // margin footer
    $this->m_pdf->pdf->WriteHTML($html);

    $this->m_pdf->pdf->Output(); //D
  }
  public function confirmar_cotizacion($id_cita=''){
    //$idorden = $this->mc->getIdOrdenByCita($id_cita); //Id orden por cita
    $idorden = $id_cita; //Id orden por cita
    //$id_cita = $this->mc->getIdCitaByOrden($idorden);
    $data['items'] = $this->db->where('idorden',$idorden)->where('tipo',2)->get('articulos_orden')->result();
    $data['anticipo'] = $this->mc->getAnticipoOrden(2,$idorden);
    $data['idorden'] = $idorden;
    $data['confirmada'] = $this->mc->EstatusCotizacionUser($idorden);
    //debug_var($data);die();
    $html = $this->blade->render("confirmar_cotizacion", $data,true);
    print_r($html);die();

  }
  //Actualizar el estatus de la cotizacion si la acepta o no
  public function EstatusCotizacion(){
    //Confirmo la cita
    
    //Actualizar totales si confirma
    if($this->input->post('status')){
      $this->mc->actualizarIVAyTotal($this->input->post('idorden'),$this->input->post('iva'),$this->input->post('total'));
    }
    $exito = $this->db->where('id',$this->input->post('idorden'))->set('cotizacion_confirmada',$this->input->post('status'))->set('fecha_confirmacion',date('Y-m-d H:i:s'))->update('ordenservicio');

    $id_cita = $this->mc->getIdCitaByOrden($this->input->post('idorden'));
    $nombre_asesor = $this->mc->asesor_by_cita($id_cita);
    $correo_asesor = $this->mc->correo_asesor_by_nombre($nombre_asesor);
    //$correo_asesor = "albertopitava@gmail.com";
    if($exito){
      $data_correo['info_correo'] = array('idorden'=>$this->mc->getIdCitaByOrden($this->input->post('idorden')),
                          'cotizacion_confirmada'=>$this->input->post('status'),
                          'fecha'=>date('Y-m-d H:i:s'),
      );
      $cuerpo = $this->blade->render('respuesta_cotizacion',$data_correo,true);
      if($this->input->post('status')==1){
        $status = 'CONFIRMADA';
      }else{
        $status = 'RECHAZADA';
      }
      $mensaje_sms = "La cotización de la orden #".$id_cita." ha sido ".$status." por el usuario el día ".$data_correo['info_correo']['fecha'];
      $celular_sms = $this->mc->telefono_asesor($this->input->post('idorden'));
      enviar_correo($correo_asesor,"Respuesta de la cotización por el usuario!",$cuerpo,array());
      //Enviar mensaje al asesor
      $this->sms_general($celular_sms,$mensaje_sms);
      echo 1;
    }else{
      echo 0;
    }
    exit();

  }
  public function sms($idorden=''){
    $idorden = $this->input->post('idorden');
    $id_cita = $this->mc->getIdCitaByOrden($idorden);

    //Validar media hora entre mensajes
    if(!$this->mc->validateMessage('sms',$idorden)){
      echo -3;exit();
    }
    //Validar que exista una cotización
    if(!$this->mc->validarCotizacionExtra($idorden)){
      //echo -2;exit();
    }
    $link_1_orden = CONST_URL_BASE."Cotizacion_Ext_Cliente/".$id_cita;
    $celular_sms = $this->mc->telefono_contacto($id_cita);
    $mensaje_sms = CONST_AGENCIA_MENSAJE.", visualiza la cotizacion: ".$link_1_orden;
     $this->sms_general_texto($celular_sms,$mensaje_sms);
    //Actualizar que ya se envió el sms de esa orden
    $datos_mensaje_enviado = array('tipo_mensaje' =>'sms','idorden'=>$idorden,'enviado'=>1 );
    $datos_mensaje_enviado['created_at'] = date('Y-m-d H:i:s');
    $this->db->insert('mensajes_enviados_orden',$datos_mensaje_enviado);
    echo 1;die();
  }
  public function sms_multipunto($idorden=''){
    $idorden = $this->input->post('idorden');
    $id_cita = $this->mc->getIdCitaByOrden($idorden);
    //Validar media hora entre mensajes
    if(!$this->mc->validateMessage('sms_cotizacion_multipunto',$idorden)){
      echo -3;exit();
    }
    $link_1_orden = $this->input->post('url');
    $celular_sms = $this->mc->telefono_contacto($id_cita);
    $mensaje_sms = CONST_AGENCIA_MENSAJE.", cotizacion multipunto ".$link_1_orden;
    $this->sms_general_texto($celular_sms,$mensaje_sms);
    //Actualizar que ya se envió el sms de esa orden
    $datos_mensaje_enviado = array('tipo_mensaje' =>'sms_cotizacion_multipunto','idorden'=>$idorden,'enviado'=>1 );
    $datos_mensaje_enviado['created_at'] = date('Y-m-d H:i:s');
    $this->db->insert('mensajes_enviados_orden',$datos_mensaje_enviado);
    echo 1;exit();
  }
  public function sms_general($celular_sms='',$mensaje_sms=''){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,CONST_URL_NOTIFICACION_APP);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
                "celular=".$celular_sms."&mensaje=".$mensaje_sms."&sucursal=".CONST_BID);
    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);
    curl_close ($ch);
  }
  public function sms_general_texto($celular='',$mensaje=''){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,SMS);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,
                  "celular=".$celular."&mensaje=".$mensaje."&sucursal=".CONST_BID."");
      // Receive server response ...
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $server_output = curl_exec($ch);
      $result = json_decode($server_output,true);
      curl_close ($ch);
  }
  //Enviar multipunto / orden
  public function sms_multipunto_orden(){
    //Validar media hora entre mensajes
    if(!$this->mc->validateMessage($this->input->post('tipomensaje'),$_POST['idorden'])){
      echo -3;exit();
    }
    $mensaje_sms = CONST_AGENCIA_MENSAJE." ".$this->input->post('tipo')." ".$this->input->post('url_archivo');
    $celular_sms = $this->mc->telefono_cliente_orden($this->input->post('idorden'));
    $this->sms_general_texto($celular_sms,$mensaje_sms);
    $datos_mensaje_enviado = array('tipo_mensaje' =>$this->input->post('tipomensaje'),'idorden'=>$this->input->post('idorden'),'enviado'=>1 );
    $datos_mensaje_enviado['created_at'] = date('Y-m-d H:i:s');
    $this->db->insert('mensajes_enviados_orden',$datos_mensaje_enviado);
    echo 1;die();
  }
  public function enviar_correo_orden_multipunto(){
    $idorden =  $this->input->post('idorden');
    $tipo = $this->input->post('tipo');
    $url = $this->input->post('url_archivo');
    
    if($_POST['tipo']=='orden'){
      $titulo_correo = '¡Envío de documentos de la orden!';
    }else{
      $titulo_correo = '¡Envío de documentos hoja multipunto!';
    }
    $id_cita = $this->mc->getIdCitaByOrden($idorden);

    $correo_consumidor = $this->mc->correo_consumidor($id_cita);
    $correo_compania = $this->mc->correo_compania($id_cita);
    $datos = array('idorden'=>$idorden,'tipo'=>$tipo,'url'=>$url,'titulo'=>$titulo_correo);
    $cuerpo = $this->blade->render('correo_orden_multipunto',$datos,true);

    if($correo_consumidor!= $correo_compania){
      if($correo_consumidor!=''){
      enviar_correo($correo_consumidor,$titulo_correo,$cuerpo,array());
      }
      if($correo_compania!=''){
        enviar_correo($correo_compania,$titulo_correo,$cuerpo,array());
      }
    }else{
       enviar_correo($correo_consumidor,$titulo_correo,$cuerpo,array());
    }

    $datos_mensaje_enviado = array('tipo_mensaje' =>$this->input->post('tipomensaje'),'idorden'=>$idorden,'enviado'=>1 );
    $datos_mensaje_enviado['created_at'] = date('Y-m-d H:i:s');
    $this->db->insert('mensajes_enviados_orden',$datos_mensaje_enviado);

    echo 1;exit();

  }
  //Teléfono del asesor
  public function getTelefonoAsesor(){
    echo $this->mc->telefono_asesor_by_nombre($this->input->post('nombre'));
  }
  public function menu_asesor(){
    $this->blade->render('menu_asesor');
  }
  //Obtener las subcategorias
  public function getSubcategorias(){
      if($this->input->post()){
        $idcategoria=$this->input->post("idcategoria");
        $subcategorias=$this->mc->getSubcategorias($idcategoria);
        echo json_encode($subcategorias);
      }else{
      echo 'Nada';
      }

  }
  //Obtener prepiking con base a modelo
  public function getPrepiking(){
      if($this->input->post()){
        $prepiking=$this->mc->getPrepiking($this->input->post("id_modelo_prepiking"),$this->input->post("vehiculo_anio"));
        echo json_encode($prepiking);
      }else{
      echo 'Nada';
      }
  }
  //Obtener submodelo con base a modelo
  public function getSubmodelo(){
    if($this->input->post()){
      $modelo_id = $this->principal->getGeneric('modelo', $_POST['vehiculo_modelo'], 'cat_modelo','id');
      $submodelos=$this->db->where('articulo_id',$modelo_id)->where('activo',1)->get('pkt_subarticulos')->result();
      echo json_encode($submodelos);
    }else{
    echo 'Nada';
    }

}

   //PROACTIVO UNIDADES NUEVAS
  public function proactivo_unidades_nuevas(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $this->get_proactivo_unidades();
  }
   public function get_proactivo_unidades()
  {
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    //$datos['lista'] = $this->mc->getOutbound();
    $this->blade->render('proactivo_unidades_nuevas');
  }
  public function getDatosProactivoUnidadesNuevas(){
    $datos = $_POST;
    $mismo = $datos['draw'];
    $pagina = $datos['start'];
    $porpagina = $datos['length'];
    if(isset($_POST['buscar_campo'])){
      $busqueda = $_POST['buscar_campo'];
      $this->filtros_proactivo_unidades_nuevas($busqueda);
    }
    if(!empty($_POST['fecha_inicio']) ||!empty($_POST['fecha_fin'])){
      $this->filtros_proactivo_unidades_nuevas_fechas();
    }
    if($_POST['ucampania']==1){
      $this->db->join('asc_campanias c','c on p.serie = c.vin');
    }else{
      $this->db->join('asc_campanias c','c on p.serie = c.vin','left');
    }
    $total_registros = $this->db->select('p.serie')->order_by('fechafac','desc')->get('proactivo_unidades p')->result();
    $array_total = [];
    foreach ($total_registros as $key => $value) {
      $array_total[$value->serie] = $value;
    }
    $total = count($array_total);
    if(isset($_POST['buscar_campo'])){
      $busqueda = $_POST['buscar_campo'];
      $this->filtros_proactivo_unidades_nuevas($busqueda);
    }

     if(!empty($_POST['fecha_inicio']) ||!empty($_POST['fecha_fin'])){
      $this->filtros_proactivo_unidades_nuevas_fechas();
    }
    $this->db->select('p.*,c.id as id_campania');
    if($_POST['ucampania']==1){
      $this->db->join('asc_campanias c','c on p.serie = c.vin');
    }else{
      $this->db->join('asc_campanias c','c on p.serie = c.vin','left');
    }
    $info = $this->db->from('proactivo_unidades p')->limit($porpagina,$pagina)->get()->result();

    $array_data = [];
    foreach ($info as $key => $value) {
      $array_data[$value->serie] = $value;
    }
    $data = array();
    foreach ($array_data as $key => $value) {
      $dat = array();
      $tiene_campania = $this->mc->validarCampania($value->serie);
      if($tiene_campania){
         $dat[] = '<span class="row_campania">'.$value->id.'</span>';
      }else{
        $dat[] = $value->id;
      }

      //$serie = str_replace('-', '', $value->serie);
      $serie = $value->serie;
      $dat[] = $serie ;
      $dat[] = date_eng2esp_1($value->fechafac) ;
      $dat[] = $value->cliente ;
      $dat[] = $value->contacto ;
      $dat[] = $value->direccion ;
      $dat[] = $value->telefono ;
      $dat[] = $value->telefono2 ;
      $dat[] = $value->telefono3 ;
      $dat[] = $value->correo ;
      $dat[] = $value->vendedor ;
      $dat[] = $value->tipovta ;
      $dat[] = $value->economico ;
      $dat[] = $value->descripcion ;
      $dat[] = $value->rfc ;
      $dat[] = $value->poblacion ;
      $dat[] = date_eng2esp_1($value->fecha_campania);



      $acciones = '';
      $acciones.= '<a href="" data-id="'.$value->id.'" class=" js_comentarios" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Comentarios"><i class="pe pe-7s-comment"></i></a>

        <a href="" data-id="'.$value->id.'" class=" js_historial" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Historial comentarios"><i class="pe pe-7s-info
pe pe-7s-info"></i></a>

        <a href="'.base_url('citas/agendar_cita/0/0/'.$value->id).'" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Agendar cita"><i class="pe pe-7s-plus"></i></a>';

      //if($tiene_campania){
        if($value->realizo_campania){
          $acciones .= '<a href="#" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Campaña realizada"><i class="fa fa-check"></i></a>';
        }else{
          $acciones .= '<a href="#" class=" js_campania" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-id="'.$value->id.'" data-serie="'.$serie.'" title="Se realizó campaña"><i class="pe pe-7s-check"></i></a>';
        }
      //}
      $acciones .= '<a href="" data-id="'.$value->id.'" class=" js_refacciones" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Refacciones" data-target="#busquedaRefacciones" data-toggle="modal"><i class="fa fa-wrench"></i></a>';
      $dat[] = $acciones;
      $data[] = $dat;
    }
    //debug_var($data);die();

    $retornar = array('draw' => intval($mismo),
                        'recordsTotal' => intval($total),
                        'recordsFiltered' => intval($total),
                        'data' => $data);
    $this->output->set_content_type('application/json')->set_output(json_encode($retornar));
  }
  public function filtros_proactivo_unidades_nuevas($busqueda=''){
     if($busqueda!=''){
      $this->db->or_like('p.serie',$busqueda);
      $this->db->or_like('p.fechafac',$busqueda);
      $this->db->or_like('p.cliente',$busqueda);
      $this->db->or_like('p.contacto',$busqueda);
      $this->db->or_like('p.direccion',$busqueda);
      $this->db->or_like('p.direccion',$busqueda);
      $this->db->or_like('p.telefono',$busqueda);
      $this->db->or_like('p.telefono2',$busqueda);

      $this->db->or_like('p.telefono3',$busqueda);
      $this->db->or_like('p.correo',$busqueda);
      $this->db->or_like('p.vendedor',$busqueda);
      $this->db->or_like('p.tipovta',$busqueda);
      $this->db->or_like('p.economico',$busqueda);
      $this->db->or_like('p.descripcion',$busqueda);
      $this->db->or_like('p.rfc',$busqueda);
      $this->db->or_like('p.poblacion',$busqueda);

     }
  }
  public function filtros_unidades_nuevas_fecha_campanias(){

      if($_POST['fecha_inicio']!='' && $_POST['fecha_inicio']!=''){
        $this->db->where('fecha_campania >=',date2sql($_POST['fecha_inicio']));
        $this->db->where('fecha_campania <=',date2sql($_POST['fecha_fin']));
      }elseif($_POST['fecha_inicio']!='' && $_POST['fecha_inicio']==''){
        $this->db->where('fecha_campania >=',date2sql($_POST['fecha_inicio']));
      }elseif($_POST['fecha_inicio']=='' && $_POST['fecha_inicio']!=''){
        $this->db->where('fecha_campania <=',date2sql($_POST['fecha_fin']));
      }
  }
  public function filtros_proactivo_unidades_nuevas_fechas(){
      //Si llega fcampania 1 debe filtrar por la fecha de la campaña si no por la fecha de venta
      if($_POST['fcampania']){
        $campo_buscar = 'fecha_campania';
      }else{
        $campo_buscar = 'fechafac';
      }
      if($_POST['fecha_inicio']!='' && $_POST['fecha_inicio']!=''){
        $this->db->where($campo_buscar.' >=',date2sql($_POST['fecha_inicio']));
        $this->db->where($campo_buscar.' <=',date2sql($_POST['fecha_fin']));
      }elseif($_POST['fecha_inicio']!='' && $_POST['fecha_inicio']==''){
        $this->db->where($campo_buscar.' >=',date2sql($_POST['fecha_inicio']));
      }elseif($_POST['fecha_inicio']=='' && $_POST['fecha_inicio']!=''){
        $this->db->where($campo_buscar.' <=',date2sql($_POST['fecha_fin']));
      }
  }
  public function comentarios_Proactivo_Unidades_Nuevas(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
       if($this->input->post()){
       $this->form_validation->set_rules('comentario', 'comentario', 'trim|required');
       if ($this->form_validation->run()){
          $this->mc->saveComentarioUnidadesNuevas();
          exit();
          }else{
             $errors = array(
                  'comentario' => form_error('comentario'),
             );
            echo json_encode($errors); exit();
          }
      }
      $data['id_cita'] = $this->input->get('id_proactivo');
      $data['input_comentario'] = form_textarea('comentario',"",'class="form-control" id="comentario" rows="2"');

      $this->blade->render('citas/comentario_reagendar_magic',$data);
  }
  public function historial_comentarios_unidades_nuevas(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    //print_r($_POST);die();
    $data['comentarios'] = $this->mc->getComentariosUnidadesNuevas($this->input->get('id_proactivo'));
    $data['tipo_reagendar_proact'] = $this->input->get('tipo_reagendar_proact');
     $this->blade->render('citas/historial_comentario_reagendar_magic',$data);

  }
  public function datosClienteIntelisis(){
    $datos_clientes = $this->mc->datosClienteIntelisis($this->input->post('cliente'));
    if(count($datos_clientes)==1){
      echo json_encode(array('cantidad'=>1,'datos'=>$datos_clientes[0]));
    }else{
      echo json_encode(array('cantidad'=>count($datos_clientes),'datos'=>array()));
    }
  }
  public function modalClientesIntelisis(){
    $data['datos_clientes'] = $this->mc->datosClienteIntelisis($this->input->post('cliente'));
    $this->blade->render('citas/modal_clientes',$data);
  }
  public function datosClienteIntelisisById(){
    $datos = $this->mc->datosClienteIntelisisById($this->input->post('id'));
    echo json_encode(array('cantidad'=>1,'datos'=>$datos));
  }
  //Datos de unidades nuevas
  public function datosClienteUnidadesNuevasById(){
    $datos = $this->mc->datosClienteUnidadesNuevasById($this->input->post('id'));
    echo json_encode(array('cantidad'=>1,'datos'=>$datos));
  }
  public function datosMagicId(){
    $datos = $this->mc->getDatosCita($this->input->post('id'));
    echo json_encode(array('cantidad'=>1,'datos'=>$datos));
  }
  public function datosMagicSerie(){
    $datos = $this->mc->datosMagicSerie($this->input->post('serie'));
    if(count($datos)==1){
      echo json_encode(array('cantidad'=>1,'datos'=>$datos[0]));
    }else{
       echo json_encode(array('cantidad'=>0,'datos'=>array()));
    }
  }
  public function datosMagicPlacas(){
    $datos = $this->mc->datosMagicPlacas($this->input->post('placas'));
    if(count($datos)==1){
      echo json_encode(array('cantidad'=>1,'datos'=>$datos[0]));
    }else{
       echo json_encode(array('cantidad'=>0,'datos'=>array()));
    }

  }
  public function datosUnidadesNuevas(){
    $datos = $this->mc->datosUnuevas($this->input->post('serie'));
    if(count($datos)==1){
      echo json_encode(array('cantidad'=>1,'datos'=>$datos[0]));
    }else{
       echo json_encode(array('cantidad'=>0,'datos'=>array()));
    }

  }
  //Obtener el último cliente
  public function getLastCliente(){

  }
  public function asignar_campania(){
    $this->db->where('id',$this->input->post('id_proactivo'))->set('realizo_campania',1)->set('fecha_campania',date('Y-m-d'))->update('proactivo_unidades');

    //Saber si no existe guardar en tabla campanias
    if(!$this->mc->validarCampania($this->input->post('serie'))){
      $this->db->set('vin',$this->input->post('serie'))->insert('campanias');
    }
    echo 1;die();
  }
  //Cambiar que se realizó campaña de asc
  public function asignar_campania_asc(){
    //print_r($_POST);die();

    $this->db->where('id',$this->input->post('id_campania'))->set($this->input->post('campo'),date('Y-m-d'))->update('asc_campanias');

    //Saber si no existe guardar en tabla campanias
    if(!$this->mc->validarCampaniaTipo($this->input->post('serie'),$this->input->post('tipo'))){
      $this->db->set('vin',$this->input->post('serie'))->set('tipo',$this->input->post('tipo'))->insert('campanias');
    }
    echo 1;die();
  }

  //FIN PROACTIVO UNIDADES NUEVAS

  function agregar_categoria_modal($id=0)
    {
      if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
      {
        $this->output->set_status_header('409');
        exit();
      }else if($this->session->userdata('id_usuario')==''){
      redirect('login');
      }

      if($this->input->post()){
        $this->form_validation->set_rules('categoria', 'categoría', 'trim|required');
       if ($this->form_validation->run()){
          $this->db->set('categoria',$this->input->post('categoria'))->insert('categorias');
          echo json_encode(array('exito'=>true,'id'=>$this->db->insert_id(),'categoria'=>$this->input->post('categoria'))); exit();
          }else{
             $errors = array(
                  'categoria' => form_error('categoria'),
             );
             echo json_encode(array('exito'=>false,'errors'=>$errors)); exit();
          }
      }

      $info=new Stdclass();

      $data['input_categoria'] = form_input('categoria',set_value('categoria',exist_obj($info,'categoria')),'class="form-control" id="categoria" ');
      $this->blade->render('agregar_categoria',$data);
  }
  function agregar_subcategoria_modal($id=0)
    {
      if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
      {
        $this->output->set_status_header('409');
        exit();
      }else if($this->session->userdata('id_usuario')==''){
      redirect('login');
      }

      if($this->input->post()){
        $this->form_validation->set_rules('subcategoria', 'subcategoría', 'trim|required');
        $this->form_validation->set_rules('clave', 'clave', 'trim|required');
       if ($this->form_validation->run()){
          $this->db->set('subcategoria',$this->input->post('subcategoria'))->set('idcategoria',$this->input->post('categoria'))->set('clave',$this->input->post('clave'))->insert('subcategorias');
          echo json_encode(array('exito'=>true,'id'=>$this->db->insert_id(),'subcategoria'=>$this->input->post('subcategoria'),'clave'=>$this->input->post('clave'))); exit();
          }else{
             $errors = array(
                  'subcategoria' => form_error('subcategoria'),
                  'clave' => form_error('clave'),
             );
             echo json_encode(array('exito'=>false,'errors'=>$errors)); exit();
          }
      }

      $info=new Stdclass();

      $data['input_subcategoria'] = form_input('subcategoria',set_value('subcategoria',exist_obj($info,'subcategoria')),'class="form-control" id="subcategoria" ');
      $data['input_clave'] = form_input('clave',set_value('clave',exist_obj($info,'clave')),'class="form-control" id="clave" ');
      $this->blade->render('agregar_subcategoria',$data);
  }
  public function getModelosPreventivo(){
    if($this->input->post()){
      $modelos = $this->db->where('p.codigo_ereact',$this->input->post('codigo_mo'))->select('distinct(pm.id) as id,pm.modelo')->join('preventivo_modelos pm','p.idmodelo = pm.id')->get('preventivo p')->result();
      echo json_encode($modelos);
    }else{
    echo 'Nada';
    }
  }
  public function getOnlyCodigo(){
    $q = $this->db->where('codigo_ereact',$this->input->post('codigo_mo'))->limit(1)->select('codigo')->get('preventivo');
    if($q->num_rows()==1){
      echo $q->row()->codigo;
    }else{
      return '';
    }

  }
  public function getTiposPreventivo(){
    if($this->input->post()){
      $tipos = $this->db->where('p.codigo_ereact',$this->input->post('codigo_mo'))->where('p.idmodelo',$this->input->post('modelo'))->select('distinct(pt.id) as id, tipo')->join('preventivo_cat_tipo pt','p.id_tipo = pt.id')->get('preventivo p')->result();
      echo json_encode($tipos);
    }else{
    echo 'Nada';
    }

  }

  public function getMotorPreventivo(){
    if($this->input->post()){
      $motores = $this->db->where('p.codigo_ereact',$this->input->post('codigo_mo'))->where('p.idmodelo',$this->input->post('modelo'))->where('p.anio',$this->input->post('anio'))->select('distinct(pm.id) as id, pm.motor')->join('preventivo_motor pm','p.id_motor = pm.id')->get('preventivo p')->result();
      //echo $this->db->last_query();die();
      echo json_encode($motores);
    }else{
    echo 'Nada';
    }
  }
  public function getDescripcionPreventivo(){
    if($this->input->post()){
      $descripciones = $this->db->where('p.codigo_ereact',$this->input->post('codigo_mo'))->where('p.idmodelo',$this->input->post('modelo'))->where('p.id_motor',$this->input->post('motor'))->where('p.anio',$this->input->post('anio'))->select('distinct(pd.id) as id, pd.descripcion')->join('preventivo_descripcion pd','id_descripcion = pd.id')->get('preventivo p')->result();
      echo json_encode($descripciones);
    }else{
    echo 'Nada';
    }
  }
  public function getCatalogoPreventivo(){
    if($this->input->post()){
      $catalogos = $this->db->where('p.codigo_ereact',$this->input->post('codigo_mo'))->where('p.idmodelo',$this->input->post('modelo'))->where('p.id_tipo',$this->input->post('tipo'))->where('p.id_motor',$this->input->post('motor'))->where('p.id_descripcion',$this->input->post('descripcion'))->select('distinct(pc.id) as id, pc.catalogo')->join('preventivo_catalogo pc','p.id_catalogo = pc.id')->get('preventivo p')->result();
      echo json_encode($catalogos);
    }else{
    echo 'Nada';
    }
  }
  public function getAllPreventivo(){
    if($this->input->post()){
      $this->db->where('idmodelo',$this->input->post('modelo'));
      $this->db->where('anio',$this->input->post('anio')); //
      //$this->db->where('id_tipo',$this->input->post('tipo'));

      $this->db->where('codigo_ereact',$this->input->post('codigo_mo'));
      $this->db->where('id_motor',$this->input->post('motor'));

      //$this->db->where('codigo',$this->input->post('codigo')); //
      //$this->db->where('id_descripcion',$this->input->post('descripcion'));
      //$this->db->where('id_catalogo',$this->input->post('catalogo')); //
      $this->db->select('distinct(descripcion)');
      $informacion_preventivo = $this->db->get('v_mantenimiento_preventivo')->result();
      //echo $this->db->last_query();die();
      if(count($informacion_preventivo)>0){
        $registros = true;
      }else{
        $registros = false;
      }
      echo json_encode(array('registros'=>$registros,'informacion_preventivo'=>$informacion_preventivo));
    }else{
    echo 'Nada';
    }
  }
  public function tableros(){
    $this->blade->render('tableros');
  }
//ASC CAMPAÑAS
//PROACTIVO UNIDADES NUEVAS
   public function asc_campanias()
  {
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $this->blade->render('asc_campanias');
  }
  public function getDatosAscCampanias(){
    $datos = $_POST;
    $mismo = $datos['draw'];
    $pagina = $datos['start'];
    $porpagina = $datos['length'];
    if(isset($_POST['buscar_campo'])){
      $busqueda = $_POST['buscar_campo'];
      $this->filtros_asc_campanias($busqueda);
    }
    $total = $this->db->select('count(id) as total')->get('asc_campanias')->row()->total;
    if(isset($_POST['buscar_campo'])){
      $busqueda = $_POST['buscar_campo'];
      $this->filtros_asc_campanias($busqueda);
    }
    $info = $this->db->from('asc_campanias')->limit($porpagina,$pagina)->get()->result();
    $data = array();
    foreach ($info as $key => $value) {
      $dat = array();
      $dat[] = $value->id;

      $dat[] = $value->vin ;
      $dat[] = $value->primer_nombre.' '.$value->segundo_nombre.' '.$value->apellidos ;
      $dat[] = $value->direccion ;
      $dat[] = $value->direccion2 ;
      $dat[] = $value->direccion3 ;
      $dat[] = $value->direccion4 ;
      $dat[] = $value->ciudad ;
      $dat[] = $value->provincia ;
      $dat[] = $value->cp ;
      $dat[] = $value->estado ;

      if($value->asc1!=''){

        if($this->mc->validarCampaniaTipo($value->vin,$value->asc1)){
          $dat[] =$value->asc1.'<br><a href="#" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Campaña realizada"><i class="fa fa-check"></i></a>';
        }else{
          $dat[] = $value->asc1.'<br><a href="#" class="js_campania" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-id="'.$value->id.'" data-serie="'.$value->vin.'" data-tipo="'.$value->asc1.'" title="Se realizó campaña" data-campo="fcampania_asc1"><i class="pe-7s-check"></i></a>';
        }
      }else{
        $dat[] = '<input id="asc1_'.$value->id.'"></input> <i data-campo="asc1" class="fa fa-save js_actualizar_asc" data-idcampo="asc1_'.$value->id.'" data-id="'.$value->id.'" style="cursor:pointer" data-serie="'.$value->vin.'"></i>';

      }


      if($value->asc2!=''){
        if($this->mc->validarCampaniaTipo($value->vin,$value->asc2)){
          $dat[] =$value->asc2.'<br><a href="#" class="fa fa-check" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Campaña realizada"></a>';
        }else{
          $dat[] = $value->asc2.'<br><a href="#" class="pe pe-7s-check js_campania" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-id="'.$value->id.'" data-serie="'.$value->vin.'" data-tipo="'.$value->asc2.'" title="Se realizó campaña" data-campo="fcampania_asc2"></a>';
        }
      }else{
        $dat[] = '<input id="asc2_'.$value->id.'" ></input> <i data-campo="asc2" class="fa fa-save js_actualizar_asc" data-idcampo="asc2_'.$value->id.'" data-id="'.$value->id.'" style="cursor:pointer" data-serie="'.$value->vin.'"></i>';
      }

      if($value->asc3!=''){
        if($this->mc->validarCampaniaTipo($value->vin,$value->asc3)){
          $dat[] =$value->asc3.'<br><a href="#" class="fa fa-check" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Campaña realizada"></a>';
        }else{
          $dat[] = $value->asc3.'<br><a href="#" class="pe pe-7s-check js_campania" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-id="'.$value->id.'" data-serie="'.$value->vin.'" data-tipo="'.$value->asc3.'" title="Se realizó campaña" data-campo="fcampania_asc3"></a>';
        }
      }else{
        $dat[] = '<input id="asc3_'.$value->id.'"></input> <i data-campo="asc3" class="fa fa-save js_actualizar_asc" data-idcampo="asc3_'.$value->id.'" data-id="'.$value->id.'" style="cursor:pointer" data-serie="'.$value->vin.'"></i>';
      }

      if($value->asc4!=''){
        if($this->mc->validarCampaniaTipo($value->vin,$value->asc4)){
          $dat[] =$value->asc4.'<br><a href="#" class="fa fa-check" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Campaña realizada"></a>';
        }else{
          $dat[] = $value->asc4.'<br><a href="#" class="pe pe-7s-check js_campania" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-id="'.$value->id.'" data-serie="'.$value->vin.'" data-tipo="'.$value->asc4.'" title="Se realizó campaña" data-campo="fcampania_asc4"></a>';
        }
      }else{
        $dat[] = '<input id="asc4_'.$value->id.'"></input> <i data-campo="asc4" class="fa fa-save js_actualizar_asc" data-idcampo="asc4_'.$value->id.'" data-id="'.$value->id.'" style="cursor:pointer" data-serie="'.$value->vin.'"></i>';
      }

      $dat[] = $value->campania;

      $acciones = '';
      $acciones.= '<a href="" data-id="'.$value->id.'" class=" js_comentarios" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Comentarios"><i class="pe-7s-comment"></i></a>
        
        <a href="" data-id="'.$value->id.'" class="js_historial" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Historial comentarios"><i class="pe-7s-info
pe pe-7s-info"></i></a>';
      $acciones.= '<a href="" data-id="'.$value->id.'" class=" js_refacciones" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Refacciones" data-target="#busquedaRefacciones" data-toggle="modal"><i class="fa fa-wrench"></i></a>';

      $dat[] = $acciones;

      $data[] = $dat;
    }
    //debug_var($data);die();

    $retornar = array('draw' => intval($mismo),
                        'recordsTotal' => intval($total),
                        'recordsFiltered' => intval($total),
                        'data' => $data);
    $this->output->set_content_type('application/json')->set_output(json_encode($retornar));
  }
  public function filtros_asc_campanias($busqueda=''){
     if($busqueda!=''){
      //print_r($_POST);die();
      $this->db->or_like('vin',$busqueda);
      $this->db->or_like('primer_nombre',$busqueda);
      $this->db->or_like('segundo_nombre',$busqueda);
      $this->db->or_like('apellidos',$busqueda);
      $this->db->or_like('direccion',$busqueda);
      $this->db->or_like('direccion2',$busqueda);
      $this->db->or_like('direccion3',$busqueda);
      $this->db->or_like('direccion4',$busqueda);
      $this->db->or_like('ciudad',$busqueda);
      $this->db->or_like('provincia',$busqueda);
      $this->db->or_like('cp',$busqueda);
      $this->db->or_like('estado',$busqueda);
      $this->db->or_like('asc1',$busqueda);
      $this->db->or_like('asc2',$busqueda);
      $this->db->or_like('asc3',$busqueda);
      $this->db->or_like('asc4',$busqueda);
      $this->db->or_like('campania',$busqueda);

     }
  }
  //Actualizar manualmente los campos de ASC de las campañas
  public function cambiarASC(){
    $this->db->where('id',$this->input->post('id'))->set($this->input->post('campo'),$this->input->post('valor'))->update('asc_campanias');
    echo 1;exit();
  }
  public function comentarios_Campanias(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
       if($this->input->post()){
       $this->form_validation->set_rules('comentario', 'comentario', 'trim|required');
       if ($this->form_validation->run()){
          $this->mc->saveComentarioCampanias();
          exit();
          }else{
             $errors = array(
                  'comentario' => form_error('comentario'),
             );
            echo json_encode($errors); exit();
          }
      }
      $data['id_cita'] = $this->input->get('id_campania');
      $data['input_comentario'] = form_textarea('comentario',"",'class="form-control" id="comentario" rows="2"');

      $this->blade->render('citas/comentario_reagendar_magic',$data);
  }
  public function getComentariosCampanias(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    //print_r($_GET);die();
    $data['comentarios'] = $this->mc->getComentariosCampanias($this->input->get('id_campania'));
    $data['tipo_reagendar_proact'] = $this->input->get('tipo_reagendar_proact');
     $this->blade->render('citas/historial_comentario_reagendar_magic',$data);

  }
//FIN ASC CAMPAÑAS
//Estatus intelisis
  public function cambiar_status_intelisis(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
      {
        $this->output->set_status_header('409');
        exit();
      }else if($this->session->userdata('id_usuario')==''){
      redirect('login');
      }

      if($this->input->post()){
        $this->form_validation->set_rules('id_status_intelisis', 'estatus', 'trim|required');
       if ($this->form_validation->run()){

          $this->db->where('id',$this->input->post('id_orden_save'))->set('id_status_intelisis',$this->input->post('id_status_intelisis'))->update('ordenservicio');

          $datos_historial = array('estatus_anterior'=>$this->mc->getStatusIntelisis($this->input->post('id_status_actual')),
            'estatus_nuevo'=>$this->mc->getStatusIntelisis($this->input->post('id_status_intelisis')),
            'usuario'=>$this->mc->getNameUser($this->session->userdata('id_usuario')),
            'created_at'=>date('Y-m-d H:i:s'),
            'idorden'=>$this->input->post('id_orden_save')
            );
          $this->db->insert('historial_cambio_status_intelisis',$datos_historial);
          echo 1;exit();

          }else{
             $errors = array(
                  'id_status_intelisis' => form_error('id_status_intelisis'),
             );
             echo json_encode(array('exito'=>false,'errors'=>$errors)); exit();
          }
      }

      $info=new Stdclass();

    $data['drop_id_status_intelisis'] = form_dropdown('id_status_intelisis',array_combos($this->mcat->get('cat_status_intelisis_orden','status'),'id','status',TRUE),$this->input->get('id_status'),'class="form-control" id="id_status_intelisis" ');

    $data['id_status_actual'] = $this->input->get('id_status');
    $data['id_orden_save'] = $this->input->get('id');

      $this->blade->render('cambiar_status_intelisis',$data);
  }
  //Situación intelisis
  public function cambiar_situacion_orden(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
      {
        $this->output->set_status_header('409');
        exit();
      }else if($this->session->userdata('id_usuario')==''){
      redirect('login');
      }

      if($this->input->post()){
        $id_cita = $this->mc->getIdCitaByOrden($this->input->post('id_orden_save'));

        if($_POST['id_situacion_intelisis']==5||$_POST['id_situacion_intelisis']==6||$_POST['id_situacion_intelisis']==7){
          $this->form_validation->set_rules('user', 'usuario', 'trim|required');
          $this->form_validation->set_rules('password', 'contraseña', 'trim|required');
        }
        $this->form_validation->set_rules('id_situacion_intelisis', 'estatus', 'trim|required');
       if ($this->form_validation->run()){

        if($_POST['id_situacion_intelisis']==5||$_POST['id_situacion_intelisis']==6||$_POST['id_situacion_intelisis']==7){
          //Validar usuario y contraseña para cerrar la orden
         if($_POST['user']!=CONST_USER_SITUACION || $_POST['password']!=CONST_PASS_SITUACION){
            echo json_encode(array('exito'=>false,'message'=>"No tienes permisos para realizar esta acción")); exit();
         }
        }

        //Si la va cerrar checar las validaciones de la firma de taller y autorización de gerencia
        if($_POST['id_situacion_intelisis']==6){
          

          $data_orden = $this->principal->getGeneric('id_cita',$id_cita,'ordenservicio');
          $data_cita = $this->principal->getGeneric('id_cita',$id_cita,'citas');
          if($data_orden->inicio_vigencia!=''){
            $kilometraje = (float)$data_orden->vehiculo_kilometraje;
            $firma_diagnostico = $data_orden->firma_diagnostico;
            $firma_vc = $data_orden->firma_vc;
            if(!$firma_diagnostico){
              //echo json_encode(array('exito'=>false,'message'=>"No es posible realizar el cierre de orden por la falta de firma del cliente por autorización de ausencia")); exit();
            }
            if(!$firma_vc && ($data_cita->id_status_color==1||$data_cita->id_status_color==5)){
              echo json_encode(array('exito'=>false,'message'=>"No se puede cerrar la orden por que en el documento de la voz cliente no existe la firma del mismo")); exit();
            }
            //La validación de la carta se hace si el cliente NO acepta los beneficios
            if(!$data_orden->acepta_beneficios){
              //Obtener la diferencia entre el inicio de vigencia y la fecha actual
              $meses_diferencia = (int)diferenciaMeses($data_orden->inicio_vigencia,date('Y-m-d'));
              if(($kilometraje>=55000&&$kilometraje<=60000)&&($meses_diferencia>=34&&$meses_diferencia<36)){
                //Verificar si ya tiene la carta de renuncia a beneficios
                $carta_renuncia = $this->principal->getGeneric('id_cita',$id_cita,'carta_renuncia');
                if(!$carta_renuncia){
                  echo json_encode(array('exito'=>false,'message'=>"No es posible realizar el cierre de orden por la ausencia de la carta de renuncia a beneficios")); exit();
                }
              }
            }
          }
          if(!$this->mc->validarAutorizacionGerencia($id_cita)){
            //echo -3;exit();
          }
          if(!$this->mc->recuperarFirmaJDT($id_cita)){
            echo json_encode(array('exito'=>false,'message'=>"No se puede realizar esta acción por que aún no se tiene la firma del jefe de taller")); exit();
          }
          //Validar que esté en un estatus permitido 
          $id_status_cita = $this->mc->getStatusCitaPrincipal($id_cita);
          if($id_cita > CONST_NO_CITA_NEW_UPDT){
            if($id_status_cita!=9&&$id_status_cita!=11){
              echo json_encode(array('exito'=>false,'message'=>"Sólo se puede cerrar una orden si la cita tiene un estatus de Lavado terminado o listo para entrega")); exit();
            }
          }

          if($id_cita>CONST_CITA_ID_DMS_CLIENTES){
            $response_crear_cuenta = $this->m_dms->crear_cuenta_dms($id_cita,$data_orden->id_tipo_orden);
            if(!$response_crear_cuenta['exito']){
              echo json_encode(array('exito'=>false,'message'=> $response_crear_cuenta['message'])); exit();
            }
            //
          }
          //Conectar con dms para cerrar la orden
          //$this->m_dms->dms_cerrar_orden($id_cita);
        }
          $this->db->where('id',$this->input->post('id_orden_save'))->set('id_situacion_intelisis',$this->input->post('id_situacion_intelisis'))->update('ordenservicio');

          $datos_historial = array('situacion_anterior'=>$this->mc->getSituacionIntelisis($this->input->post('id_situacion_actual')),
            'situacion_nueva'=>$this->mc->getSituacionIntelisis($this->input->post('id_situacion_intelisis')),
            'usuario'=>$this->mc->getNameUser($this->session->userdata('id_usuario')),
            'created_at'=>date('Y-m-d H:i:s'),
            'idorden'=>$this->input->post('id_orden_save')
            );
          $this->db->insert('historial_cambio_situacion_intelisis',$datos_historial);
          //SI la detiene, cambiar el estatus de la cita
          $idsituacion = $this->input->post('id_situacion_intelisis');
          $id_cita = $this->mc->getIdCitaByOrden($this->input->post('id_orden_save'));
          //Estatus actual de la cita
          $id_status_actual_cita = $this->mc->getStatusCitaPrincipal($id_cita);
          $bandera = false;
          switch ($idsituacion) {
            case 11: //Detenida por TOT
              $id_status_cita = 18;
              $bandera = true;
              break;
            case 12: //Detenida por refacciones
              $id_status_cita = 19;
              $bandera = true;
              break;
            case 13: //Detenida por Soporte
              $id_status_cita = 17;
              $bandera = true;
              break;
            case 14: //Detenida por Otros
              $id_status_cita = 16;
              $bandera = true;
              break;
            case 8: //Detenida por Autorización
              $id_status_cita = 15;
              $bandera = true;
              break;
            case 10: //Reactivar
            //Cuando se reactiva voy a dejar en el estatus anterior en el que estaba
              $id_status_anterior = $this->mc->getLastStatus($id_cita);
              //NUEVA VERSIÓN
              //$this->db->where('id_cita',$id_cita)->set('id_ultimo_status',null)->set('id_status',$id_status_anterior)->update('citas');
              $bandera = false;
              break;
            default:
              # code...
              break;
          }
          if($bandera){
            //Validar si tiene un estatus diferente de detenido
             $id_status_anterior = $this->mc->getLastStatus($id_cita);
             //$this->mc->validarUltimoStatus($id_cita)
            if($id_status_anterior=='' ||$id_status_anterior==0){
              //Si no está en algún estados de detenido
              $this->db->where('id_cita',$id_cita)->set('id_ultimo_status',$id_status_actual_cita)->update('citas');
            }

          //$this->db->where('id_cita',$id_cita)->set('id_status',$id_status_cita)->update('citas');

           //Verificar si no existe la transaccion traerme la de creación de la cita
          $inicio = $this->mc->getLastTransition($id_cita);
          if($inicio==''){
            $inicio = $this->mc->getFechaCreacion($id_cita);
          }
          $datos_transiciones = array('status_anterior' => $this->mc->getLastStatusTransiciones($id_cita),
                                      'status_nuevo'=> $this->mc->getStatusTecnicoTransiciones($id_cita),
                                      'fecha_creacion'=>date('Y-m-d H:i:s'),
                                      'tipo'=>2, //técnico,
                                      'inicio'=>$inicio,
                                      'fin'=>date('Y-m-d H:i:s'),
                                      'usuario'=>'',
                                      'id_cita'=>$id_cita,
                                      'general'=>1,
                                      'minutos_transcurridos'=>dateDiffMinutes($inicio , date('Y-m-d H:i:s'))
          );
          $this->db->insert('transiciones_estatus',$datos_transiciones);
          }

          //Si es orden cerrada / no entregada poner el campo de cierre_orden
          if($_POST['id_situacion_intelisis']==6){
            $this->db->where('id_cita',$id_cita)->set('cierre_orden',date('Y-m-d H:i:s'))->update('ordenservicio');
          }
          //Cancelar DMS
          if($_POST['id_situacion_intelisis']==7){
            eliminar_orden_dms($id_cita);
          }
          echo json_encode(array('exito'=>true,'message'=>"Registro actualizado correctamente")); exit();

        }else{
             $errors = array(
                  'id_situacion_intelisis' => form_error('id_situacion_intelisis'),
                  'user' => form_error('user'),
                  'password' => form_error('password'),
             );
             echo json_encode(array('exito'=>false,'message'=>"Registro actualizado correctamente","validations"=>json_encode($errors))); exit();
             
          }
      }

      $info=new Stdclass();

    $data['drop_id_situacion_intelisis'] = form_dropdown('id_situacion_intelisis',array_combos($this->mcat->get('cat_situacion_intelisis','situacion','',array('visible'=>1)),'id','situacion',TRUE),$this->input->get('id_situacion'),'class="form-control" id="id_situacion_intelisis" ');

    $data['id_situacion_actual'] = $this->input->get('id_situacion');
    $data['id_orden_save'] = $this->input->get('id');

      $this->blade->render('cambiar_situacion_orden',$data);
  }
  //Estatus intelisis
  public function cambiar_status_orden(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
      {
        $this->output->set_status_header('409');
        exit();
      }else if($this->session->userdata('id_usuario')==''){
      redirect('login');
      }

      if($this->input->post()){
        $this->form_validation->set_rules('id_status_orden', 'estatus', 'trim|required');
       if ($this->form_validation->run()){

          $this->db->where('id',$this->input->post('id_orden_save'))->set('id_status',$this->input->post('id_status_orden'))->update('ordenservicio');

          $datos_historial = array('estatus_anterior'=>$this->mc->getStatusIntelisisOrden($this->input->post('id_status_actual')),
            'estatus_nuevo'=>$this->mc->getStatusIntelisisOrden($this->input->post('id_status_orden')),
            'usuario'=>$this->mc->getNameUser($this->session->userdata('id_usuario')),
            'created_at'=>date('Y-m-d H:i:s'),
            'idorden'=>$this->input->post('id_orden_save')
            );
          $this->db->insert('historial_cambio_status_orden',$datos_historial);

          //Si es iniciada actualizar en la orden servicio
          if($this->input->post('id_status_orden')==1){
             $this->db->where('id',$this->input->post('id_orden_save'))->set('fecha_inicio',date('Y-m-d H:i:s'))->update('ordenservicio');
          }

          //Si es finalizada actualizar en la orden servicio
          if($this->input->post('id_status_orden')==2){
             $this->db->where('id',$this->input->post('id_orden_save'))->set('fecha_finalizacion',date('Y-m-d H:i:s'))->update('ordenservicio');
          }
          echo 1;exit();

          }else{
             $errors = array(
                  'id_status_orden' => form_error('id_status_orden'),
             );
             echo json_encode(array('exito'=>false,'errors'=>$errors)); exit();
          }
      }

      $info=new Stdclass();

    $data['drop_id_status'] = form_dropdown('id_status_orden',array_combos($this->mcat->get('cat_status_orden','status'),'id','status',TRUE),$this->input->get('id_status'),'class="form-control" id="id_status_orden" ');

    $data['id_status_actual'] = $this->input->get('id_status');
    $data['id_orden_save'] = $this->input->get('id');

      $this->blade->render('cambiar_status_orden_intelisis',$data);
  }
  public function cotizacion_individual($idorden=''){
    $data['idordensave'] = $idorden;
    $data['id_cita'] = $this->mc->getIdCitaByOrden($idorden);
    $this->blade->render('cotizacion_individual_iframe',$data);
  }
  //Validamos el formulario
    public function validateForm()
    {

        $datos["vista"] = $this->input->post("modoVistaFormulario");
        $datos["idOrden"] = $this->input->post("idOrdenTemp");
        $datos["tipoRegistro"] = $this->input->post("tipoRegistro");
        $datos["modoGuardado"] = $this->input->post("modoGuardado");

        if ($datos["vista"] == "1") {
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden de servicio requerido.', 'required|callback_existencia');
            //Comprobamos que se hayan guardado al menos un elemento
            $this->form_validation->set_rules('registros', 'Campo requerido.', 'callback_registros');

        }else {
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden de servicio requerido.', 'required');
            //Comprobamos que se hayan guardado al menos un elemento
            $this->form_validation->set_rules('registros', 'Campo requerido.', 'callback_registros');

        }

        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('registros','No se ha guardado ningun elemento');
        $this->form_validation->set_message('existencia','El No. de Orden ya existe. Favor de verificar el No. de Orden.');

        //Si pasa las validaciones enviamos la informacion al servidor
        $this->getDataForm($datos);
        // if($this->form_validation->run()!=false){
        //     //Recuperamos la informacion del formulario
        //     $datos["mensaje"]="0";
        //     $this->getDataForm($datos);
        // }else{
        //     $datos["mensaje"]="0";
        //     if ($datos["vista"] == "1") {
        //         $this->index($datos["idOrden"],$datos);
        //     }else {
        //         if ($datos["vista"] == "3") {
        //             $this->comodinCliente($datos["idOrden"],$datos);
        //         }elseif ($datos["vista"] == "4") {
        //             $this->comodinEditaSU($datos["idOrden"],$datos);
        //         } else {
        //             $this->setData($datos["idOrden"],$datos);
        //         }
        //     }

        // }
    }
    //Validar requisitos minimos P1
    public function registros()
    {
        $return = FALSE;
        if ($this->input->post("registros")) {
            if (count($this->input->post("registros")) >0){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Recuperamos la información del formulario
    public function getDataForm($datos = NULL)
    {
        $registro = date("Y-m-d H:i");
        //Creamos el array para guardado
        if ($datos["vista"] == "1") {
            $dataCoti = array(
                'idCotizacion' => NULL,
                'idOrden' => $this->input->post("idOrdenTemp"),
            );
        }

        //$dataCoti['contenido'] = $this->mergeOfString($this->input->post("registros"),"");
        $dataCoti['contenido'] = $this->input->post("registros");
        //debug_var($dataCoti['contenido']);die();

        foreach ($dataCoti['contenido'] as $key => $contenido) {
          //[0] => 1_descripcion_100_2002_1_SI_123a_2102_SI
          if($contenido!=''){
            $content_split = explode('_',$contenido);
            //debug_var($content_split);die();
              $articulos_orden= array (
              'descripcion' =>$content_split[1],
              'idorden' =>$this->input->post("idordensave"),
              'precio_unitario' =>$content_split[2],
              'cantidad' =>$content_split[0],
              'descuento' =>0,
              'total' =>$content_split[4],
              'id_usuario' =>$this->session->userdata('id_usuario'),
              'created_at' =>date('Y-m-d H:i:s'),
              'tipo' =>2,
              'total_horas' =>$content_split[3],
              'costo_hora' =>$content_split[2],
              'existe_pieza' =>$content_split[8],
              'numero_pieza' =>$content_split[6],
            );
            $this->db->insert('articulos_orden',$articulos_orden);
          }
        }
        $dataCoti['subTotal'] = $this->input->post("subTotalMaterial");
        $dataCoti['iva'] = $this->input->post("ivaMaterial");
        $dataCoti['total'] = $this->input->post("totalMaterial");

        echo 1;exit();
    }
      //Validar Radio
    public function validateRadio($campo = NULL,$elseValue = 0)
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = $campo;
        }
        return $respuesta;
    }
  public function ver_cotizaciones($id_cita=''){
    $idorden = $this->mc->getIdOrdenByCita($this->input->post('id_cita')); //Id orden por cita
    //$id_cita = $this->mc->getIdCitaByOrden($idorden);
    $data['items'] = $this->db->where('idorden',$idorden)->where('tipo',2)->get('articulos_orden')->result();
    $data['cotizacion_multipunto'] = $this->getCotizacionExtraMultipunto($this->input->post('id_cita'));
    //debug_var($data['cotizacion_multipunto']);die();
    $data['anticipo'] = $this->mc->getAnticipoOrden(2,$idorden);
    $data['idorden'] = $idorden;
    $data['confirmada'] = $this->mc->EstatusCotizacionUser($idorden);
   $this->blade->render("tabla_cotizacion_aceptadas", $data);
  }
  //Recuperamos la informacion de la cotizacion (solo aceptadas)
  public function getCotizacionExtraMultipunto($idOrden  = "")
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = [];

        try {
            // $idOrden = $_POST["idOrden"];

            $contenedor = [];
            //Recuperamos la información de la cotizacion
            $revision = $this->mc->get_result("idOrden",$idOrden,"cotizacion_multipunto");
            foreach ($revision as $row) {
                $idCotizacion = $row->idCotizacion;
                $acepta = $row->aceptoTermino;
                $cuerpo = $this->createCells(explode("|",$row->contenido));
                $subtotal = $row->subTotal;
                $iva = $row->iva;
                $cancelado = $row->cancelado;
                $total = $row->total;
                $comentario = $row->comentario;
            }

            //Comprobamos que se haya cargado
            if (isset($idCotizacion)) {
                //Verificamos la respuesta afirmativa
                if (($acepta == "Si")||($acepta == "Val")) {
                    $articulos = [];
                    //Comprobamos si la cotizacion se acepto completa o por partes
                    if ($acepta == "Si") {
                        //Recuperamos los elementos de la cotización
                        foreach ($cuerpo as $index => $valor) {
                            $articulos["cantidad"] = $cuerpo[$index][0];
                            $articulos["descripcion"] = $cuerpo[$index][1];
                            $articulos["pieza"] = $cuerpo[$index][6];
                            $articulos["costo"] = $cuerpo[$index][2];

                            $contenedor["cotizacion"][] = $articulos;
                        }
                    }else {
                        //Recuperamos los elementos de la cotización
                        foreach ($cuerpo as $index => $valor) {
                            if ($cuerpo[$index][9] == "Aceptada") {
                                $articulos["cantidad"] = $cuerpo[$index][0];
                                $articulos["descripcion"] = $cuerpo[$index][1];
                                $articulos["pieza"] = $cuerpo[$index][6];
                                $articulos["costo"] = $cuerpo[$index][2];

                                $contenedor["cotizacion"][] = $articulos;
                            }
                        }
                    }

                    $contenedor["respuesta"] = "OK";
                }else {
                    $contenedor["cotizacion"] = array();
                    $contenedor["respuesta"] = "Cotización Rechazada";
                }
            } else {
                $contenedor["cotizacion"] = array();
                $contenedor["respuesta"] = "ERROR";
            }
        } catch (\Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        //$respuesta = json_encode($contenedor);
        return $contenedor;
    }
  //PERMISOS
  public function usuarios_permisos(){

    $data['datos'] = $this->db->get(CONST_BASE_PRINCIPAL.'admin')->result();
    $this->blade->render('permisos',$data);

  }

  public function asigna_usuario(){
    $id_admin = $this->input->post('idadmin');
    $tipo = $this->input->post('tipo_id');

    $usuario = $this->mc->get_row("adminId",$id_admin,CONST_BASE_PRINCIPAL."admin");


    switch ($tipo) {
    case 1:
        if($usuario->p_seguros == 1){
          $data['p_seguros'] = 0;
        }else{
          $data['p_seguros'] = 1;
        }
        $this->mc->update("adminId",$id_admin,CONST_BASE_PRINCIPAL."admin",$data);
        break;
    case 2:
        if($usuario->p_seguros == 1){
          $data['p_logistica'] = 0;
        }else{
          $data['p_logistica'] = 1;
        }
        $this->mc->update("adminId",$id_admin,CONST_BASE_PRINCIPAL."admin",$data);
        break;
    case 3:
        if($usuario->p_seguros == 1){
          $data['p_recepcion'] = 0;
        }else{
          $data['p_recepcion'] = 1;
        }
        $this->mc->update("adminId",$id_admin,CONST_BASE_PRINCIPAL."admin",$data);
        break;
    case 4:
        if($usuario->p_seguros == 1){
          $data['p_admin'] = 0;
        }else{
          $data['p_admin'] = 1;
        }
        $this->mc->update("adminId",$id_admin,CONST_BASE_PRINCIPAL."admin",$data);
        break;
    case 5:
        if($usuario->p_seguros == 1){
          $data['p_facturacion'] = 0;
        }else{
          $data['p_facturacion'] = 1;
        }
        $this->mc->update("adminId",$id_admin,CONST_BASE_PRINCIPAL."admin",$data);
        break;
    case 6:
        if($usuario->p_citas == 1){
          $data['p_citas'] = 0;
        }else{
          $data['p_citas'] = 1;
        }
        $this->mc->update("adminId",$id_admin,CONST_BASE_PRINCIPAL."admin",$data);
        break;
    case 7:
        if($usuario->p_oasis == 1){
          $data['p_oasis'] = 0;
        }else{
          $data['p_oasis'] = 1;
        }
        $this->mc->update("adminId",$id_admin,CONST_BASE_PRINCIPAL."admin",$data);
        break;
      case 8:
        if($usuario->p_parametros == 1){
          $data['p_parametros'] = 0;
        }else{
          $data['p_parametros'] = 1;
        }
        $this->mc->update("adminId",$id_admin,CONST_BASE_PRINCIPAL."admin",$data);
        break;
     }
  }
  public function refacciones_shara(){
      $this->blade->render('refacciones_shara');
  }
  public function modal_multipunto(){
   $this->blade->render('modal_multipunto');
  }
  public function modal_cotizaciones_rechazadas(){
   $this->blade->render('cotizaciones_rechazadas');
  }
  public function upload_file_shara(){
    $data['id'] = $this->input->post('id');
    $this->blade->render('upload_file_shara',$data);
  }
  // GUARDAR COMENTARIOS ORDEN DE SERVICIO
  public function comentarios_orden_servicio(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
       if($this->input->post()){
       $this->form_validation->set_rules('comentario', 'comentario', 'trim|required');
       if ($this->form_validation->run()){
          $this->mc->saveComentario_ordenServicio();
          exit();
          }else{
             $errors = array(
                  'comentario' => form_error('comentario'),
             );
            echo json_encode($errors); exit();
          }
      }
      $data['idorden'] = $this->input->get('idorden');
      $data['input_comentario'] = form_textarea('comentario',"",'class="form-control" id="comentario" rows="2"');

      $this->blade->render('citas/comentarios_orden_servicio',$data);
  }
  public function historial_comentarios_orden_servicio(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
    $data['comentarios'] = $this->mc->getComentarios_orden_servicio($this->input->get('idorden'));
    $data['tipo_reagendar_proact'] = $this->input->get('tipo_reagendar_proact');
     $this->blade->render('citas/historial_comentarios',$data);

  }
  //Reemplazar guion de proactivo historiales
  public function guion_proactivo_unidades(){
    $unidades = $this->db->get('proactivo_unidades')->result();
    foreach ($unidades as $key => $value) {
       $serie = str_replace('-', '', $value->serie);
      $this->db->where('id',$value->id)->set('serie',$serie)->update('proactivo_unidades');
    }
  }
  //Reemplazar fecha de factura unidades nuevas
  public function parseDateUnidadesN(){
    $unidades = $this->db->get('proactivo_unidades')->result();
    foreach ($unidades as $key => $value) {
       $arrFecha = str_split($value->fechafac,2);
       $fecha = $arrFecha[0].$arrFecha[1].'/'.$arrFecha[2].'/'.$arrFecha[3];
      $this->db->where('id',$value->id)->set('fechafac',$fecha)->update('proactivo_unidades');
    }
  }
  //AGREGAR MODELO PREPIKING
  function agregar_modelo_prepiking_modal($id=0)
    {
      if($this->input->post()){
        $this->form_validation->set_rules('modelo', 'modelo', 'trim|required');
       if ($this->form_validation->run()){
          $this->db->set('modelo',$this->input->post('modelo'))->insert('modelos_ford ');
          echo json_encode(array('exito'=>true,'id'=>$this->db->insert_id(),'modelo'=>$this->input->post('modelo'))); exit();
          }else{
             $errors = array(
                  'modelo' => form_error('modelo'),
             );
             echo json_encode(array('exito'=>false,'errors'=>$errors)); exit();
          }
      }

      $info=new Stdclass();

      $data['input_modelo'] = form_input('modelo',set_value('modelo',exist_obj($info,'modelo')),'class="form-control" id="modelo" ');
      $this->blade->render('agregar_modelo_ford',$data);
  }
  public function getItemsCotizacionExtra(){
    //$idorden = $this->mc->getIdOrdenByCita($id_cita); //Id orden por cita
    $idorden = $this->input->post('idorden');
    //$id_cita = $this->mc->getIdCitaByOrden($idorden);
    $data['items'] = $this->db->where('idorden',$idorden)->where('tipo',2)->get('articulos_orden')->result();
    $data['anticipo'] = $this->mc->getAnticipoOrden(2,$idorden);
    $data['idorden'] = $idorden;
    $data['confirmada'] = $this->mc->EstatusCotizacionUser($idorden);
    //debug_var($data);die();
    echo $this->blade->render("datos_cotizacion_extra", $data,true);
  }
  function agregar_extra_prepiking($id=0)
    {
    if($this->input->post()){
        $this->form_validation->set_rules('codigo', 'código', 'trim|required|max_length[20]');
        $this->form_validation->set_rules('descripcion', 'descripción', 'trim|required|max_length[50]');
       if ($this->form_validation->run()){
          $this->mc->guardarExtraPrepiking();
          }else{
             $errors = array(
                  'codigo' => form_error('codigo'),
                  'descripcion' => form_error('descripcion'),
             );
            echo json_encode($errors); exit();
          }
      }

      $id = $this->input->get('idprepiking_s');

      if($id==0){
        $info=new Stdclass();
      }else{
        $info= $this->mc->getPrepikingDescripcionId($id);
        $info=$info[0];
      }

      $data['input_id'] = $id;
      $data['input_codigo'] = form_input('codigo',set_value('codigo',exist_obj($info,'codigo')),'class="form-control" rows="5" id="codigo" ');
      $data['input_descripcion'] = form_input('descripcion',set_value('codigo',exist_obj($info,'descripcion')),'class="form-control" rows="5" id="descripcion" ');
      $data['id_cita'] = $this->input->get('id_cita');
      $data['idprepiking_s'] = $this->input->get('idprepiking_s');
      $data['editar_descripcion'] = $this->input->get('editar_descripcion');
      $data['id_operacion'] = $this->input->get('id_operacion');
      
      $this->blade->render('extra_prepiking',$data);
  }
  public function buscarItemsPrepiking(){
    $data['articulos_prepiking'] = $this->mc->getArticulosPrepiking($this->input->post('id_cita'),$this->input->post('id_operacion'));
    $data['id_cita'] = $this->input->post('id_cita');
    $this->blade->render('tabla_items_prepiking',$data);
  }
  //buscar la fecha de una cita para ponerla en el tablero
  public function buscar_fecha_cita(){
    $fecha = $this->mc->getDateCita($this->input->post('id_cita'));
    if(!$fecha){
      echo json_encode(array('exito'=>false));
    }else{
      echo json_encode(array('exito'=>true,'fecha'=>date2sql($fecha)));
    }
  }
  //Actualizar orden mágic
  public function updateOrdenMagic(){
    $this->db->where('id',$this->input->post('idorden'))->set('orden_magic',$this->input->post('orden_magic'))->update('ordenservicio');
    echo 1; exit();
  }
  public function ver_unidades_status(){
    $idstatus = $this->input->get('idstatus');
    $data['status'] = $this->mc->getStatusTecnicoTransiciones($idstatus);
    $data['registros'] = $this->db->select('c.id_cita,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno,o.fecha_entrega as fecha_promesa,o.hora_entrega as hora_promesa,c.asesor, t.nombre as tecnico,a.fecha as fecha_ingreso,a.hora as hora_ingreso,vehiculo_modelo,vehiculo_placas')->where('c.id_status',$idstatus)->join('aux a','a.id=c.id_horario','left')->join('ordenservicio o','o.id_cita=c.id_cita','left')->join('tecnicos t','t.id=c.id_tecnico')->get('citas c')->result();
    $this->blade->render('unidades_por_status',$data);
  }
  public function notificaciones(){
    $data['notificaciones'] = $this->db->where('id_user',$this->session->userdata('id_usuario'))->where('tipo_notificacion',2)->where('estadoWeb',1)->get(CONST_BASE_PRINCIPAL.'noti_user')->result(); 
    //debug_var($data);die();
    $this->blade->set_data($data)->render('notificaciones');
  }
  public function documentacion_ordenes(){
    $this->blade->render('documentacion_ordenes');
  }
  public function addCampania(){
    if($this->input->post()){
        $this->form_validation->set_rules('serie_save', 'serie', 'trim|required');
        $this->form_validation->set_rules('campania', 'campaña', 'trim|required');
        if ($this->form_validation->run()){
          $datos = array('vin' =>$this->input->post('serie_save'),
                         'tipo' =>$this->input->post('campania'),
                         'id_usuario' =>$this->session->userdata('id_usuario'),
                         'created_at'=>date('Y-m-d H:i:s')

          );
          $exito = $this->db->insert('campanias',$datos);
          if($exito){
            echo 1;
          }else{
            echo 0;
          }
          exit();

        }else{
          $errors = array(
            'serie' => form_error('serie'),
            'campania' => form_error('campania'),
          );
          echo json_encode($errors); exit();
        }
      }
    $info=new Stdclass();
    $data['input_serie'] = form_input('serie_save',$this->input->get('serie'),'class="form-control" id="serie_save" readonly ');
    $data['input_campania'] = form_input('campania','','class="form-control" id="campania" ');
    $this->blade->render('NewCampania',$data);
  }
  public function validateBeforeMakeOrder(){
    $id_cita = $this->input->post('id_cita');
    $id_horario = $this->mc->getIdHorarioActual($id_cita);
    $status_actual = $this->mc->getStatusCita($id_horario);

    $cita = $this->db->where('id_cita',$id_cita)->get('citas');
    if($cita->num_rows()==0){
        echo json_encode(array('exito'=>false,"mensaje"=>"La cita no se encuentra registrada")); exit();
    }

    if($status_actual==2 || $status_actual==3){
      $id_status_cita = $this->mc->getStatusCitaPrincipal($id_cita);
      if($id_status_cita==5){
        echo json_encode(array('exito'=>false,"mensaje"=>"No se puede realizar la orden por que se encuentra en un estatus que el cliente no llegó"));
      }else{
        echo json_encode(array('exito'=>true));
      }
    }else{
      echo json_encode(array('exito'=>false,"mensaje"=>"Sólamente se puede registrar la orden si el cliente llegó o llegó tarde"));
    }
  }
   public function login_carryover(){
       if($this->input->post()){
        $this->form_validation->set_rules('usuario', 'usuario', 'trim|required');
        $this->form_validation->set_rules('password', 'contraseña', 'trim|required');
        if ($this->form_validation->run()){
            if($this->input->post('usuario')==CONST_USER_CARRYOVER && $this->input->post('password')==CONST_PASS_CARRYOVER){
              $estatus_permitido_carryover = $this->ml->esEstatusPermitidoCarryOver($this->input->post('id_operacion'));
              if(!$estatus_permitido_carryover){
                echo -1;exit();
              }
              $id_status = $this->mc->getStatusCitaPrincipal($this->input->post('id_cita'));
              if($id_status==3||$id_status==5||$id_status==8||$id_status==9||$id_status==10||$id_status==11){
                echo -1;exit();
              }else{
                $this->db->where('id',$this->input->post('idcarryover'))->set('historial',1)->update('tecnicos_citas_historial');
              }
              echo 1;exit();
            }else{
              echo 0; exit();
            }
        }else{
           $errors = array(
                'usuario' => form_error('usuario'),
                'password' => form_error('password'),
           );
          echo json_encode($errors); exit();
        }
      }
      $data['input_usuario'] = form_input('usuario_carryover',$this->input->get('usuario_tecnico'),'class="form-control" rows="5" id="usuario_carryover" ');

     $data['input_password'] = form_password('password_carryover',"",'class="form-control" rows="5" id="password_carryover" ');
     $data['idcarryover'] = $this->input->get('idcarryover');    
     $this->blade->render('login_carryover',$data);
  }
  public function updateCitas10min(){
    $fecha = date('Y-m-d');
    $date = new DateTime();
    $hora = $date->modify('-10 minute');
    $hora_new =  $date->format('H:i');

    $fecha = $date->format('Y-m-d');
    $registros = $this->db->select('a.id,c.id_cita,c.id_horario')->join('aux a','c.id_horario = a.id')->where('a.fecha',$fecha)->where('a.hora <',$hora_new)->where('a.id_status_cita',1)->where('c.id_status',1)->where('cita_previa',1)->get('citas c')->result();
    foreach ($registros as $key => $value) {
      $this->db->where('id',$value->id)->set('id_status_cita',4)->update('aux');
      $this->db->where('id_cita',$value->id_cita)->set('id_status',5)->update('citas');

      //GUARDAR TRANSICIONES DE LOS ESTATUS DE ASESOR
      $id_operador = $this->mc->getIdOperador($value->id_horario);
      $datos_transiciones = array('status_anterior' => $this->mc->getLastStatusTransiciones($value->id_cita),
                                  'status_nuevo'=> $this->mc->getStatusAsesor(4),
                                  'fecha_creacion'=>date('Y-m-d H:i:s'),
                                  'tipo'=>1, //asesor,
                                  'inicio'=>$this->mc->getFechaCreacion($value->id_cita),
                                  'fin'=>date('Y-m-d H:i:s'),
                                  'usuario'=>'ADMINISTRADOR',
                                  'id_cita'=>$value->id_cita,
                                  'mostrar_linea'=>0,
                                  'motivo'=>'NO LLEGÓ CLIENTE DESPUÉS DE 10 MIN',
                                  'general'=>1,
                                  'minutos_transcurridos'=>dateDiffMinutes($this->mc->getFechaCreacion($value->id_cita) , date('Y-m-d H:i:s'))
      );
      $this->db->insert('transiciones_estatus',$datos_transiciones);
      //FIN TRANSICIÓN ASESOR

      //TRANSICIÓN TÉCNICO
      $datos_transiciones = array('status_anterior' => $this->mc->getLastStatusTransiciones($value->id_cita),
                                      'status_nuevo'=> $this->mc->getStatusTecnicoTransiciones(5),
                                      'fecha_creacion'=>date('Y-m-d H:i:s'),
                                      'tipo'=>2, //técnico,
                                      'inicio'=>$this->mc->getLastTransition($value->id_cita),
                                      'fin'=>date('Y-m-d H:i:s'),
                                      'usuario'=>'ADMINISTRADOR',
                                      'id_cita'=>$value->id_cita,
                                      'motivo'=>'NO LLEGÓ CLIENTE DESPUÉS DE 10 MIN',
          );
          $this->db->insert('transiciones_estatus',$datos_transiciones);
          $this->db->where('id_cita',$value->id_cita)->set('id_status_anterior',1)->update('citas');
    }
  }
  //Asociar orden
  public function asociarOrden(){
    if(!$this->mc->ordenesAsociadas($this->input->post('idcita'))){
      echo -1;exit();
    }
    echo $this->mc->asociarOrden($this->input->post('idcita'));
  }
  public function getDatosOrdenByCita(){
    $datos_orden = $this->db->where('c.id_cita',$this->input->post('id_cita'))
                            ->join('citas c','o.id_cita=c.id_cita')
                            ->order_by('c.id_cita','desc')
                            ->limit(1)
                            ->get('ordenservicio o');
    $exito = false;
    if($datos_orden->num_rows()){
      $exito = true;
      $orden = $datos_orden->row();
    }else{
      $orden = '';
    }
    echo json_encode(array('exito'=>$exito,'orden'=>$orden));
  }
  public function getDatosOrdenBySerie(){
    $datos_orden = $this->db->where('vehiculo_numero_serie',$this->input->post('serie'))
                            ->join('citas c','o.id_cita=c.id_cita')
                            ->order_by('c.id_cita','desc')
                            ->limit(1)
                            ->select('o.*,c.*,o.numero_cliente as numcliente')
                            ->get('ordenservicio o');
    
    $exito = false;
    if($datos_orden->num_rows()){
      $exito = true;
      $orden = $datos_orden->row();
    }else{
      $orden = '';
    }
    echo json_encode(array('exito'=>$exito,'orden'=>$orden));
  }

  
  public function validarCambioStatus(){
    $array_status = array(3,4,5,6,7,1,11,12,9,13,14,15,16,17,18,19,20,21,22,23);
    $lista_status = $this->db->select('status')->where_in('id',$array_status)->get('cat_status_citas_tecnicos')->result();
    $cadena = '';
    foreach ($lista_status as $key => $value) {
      $cadena.=$value->status.',';
    }
    $datos['lista_status'] = substr($cadena, 0,-1);
    $datos['drop_operaciones'] = form_dropdown('id_operacion',array_combos(array(),'','',TRUE),'','class="form-control" id="id_operacion"');
    $this->blade->render('validarTrabajando',$datos);
  }
  public function buscarTrabajando(){
  //Id técnico
    //print_r($_POST);die();
    $array_status = array(3,4,5,6,7,1,11,12,9,13,14,15,16,17,18,19,20,21,22,23);
    $q = $this->db->where('id',$this->input->post('id_operacion'))->select('id_tecnico')->from('articulos_orden')->get();
    if($q->num_rows()==1){
      $id_tecnico = $q->row()->id_tecnico;
    }else{
      $id_tecnico =0;
    }
  $datos['registros_tc'] = $this->db->select('a.descripcion as operacion,a.idorden,o.id_cita,t.nombre as tecnico,tc.fecha,tc.hora_inicio_dia,tc.hora_fin,cs.status as estatus')
                        ->join('tecnicos_citas tc','tc.id_operacion = a.id')
                        ->join('tecnicos t','tc.id_tecnico = t.id')
                        ->join('cat_status_citas_tecnicos cs','a.id_status = cs.id')
                        ->join('ordenservicio o','o.id=a.idorden')
                        ->where_not_in('a.id_status',$array_status)
                        ->where('date(a.created_at) <',date('Y-m-d'))
                        ->where('a.id !=',$this->input->post('id_operacion'))
                        ->where('a.id_tecnico',$id_tecnico)
                        ->where('a.no_planificado',0)
                        ->where('o.id_cita >',CONST_NO_CITA_NEW_UPDT)
                        ->get('articulos_orden a')
                        ->result();
  $datos['registros_tch'] = $this->db->select('a.descripcion as operacion,a.idorden,o.id_cita,t.nombre as tecnico,tc.fecha,tc.hora_inicio_dia,tc.hora_fin,cs.status as estatus')
                        ->join('tecnicos_citas_historial tc','tc.id_operacion = a.id')
                        ->join('tecnicos t','tc.id_tecnico = t.id')
                        ->join('cat_status_citas_tecnicos cs','a.id_status = cs.id')
                        ->join('ordenservicio o','o.id=a.idorden')
                        ->where_not_in('a.id_status',$array_status)
                        ->where('date(a.created_at) <',date('Y-m-d'))
                        ->where('a.id !=',$this->input->post('id_operacion'))
                        ->where('a.id_tecnico',$id_tecnico)
                        ->where('tc.carryover',0)
                        ->where('o.id_cita >',CONST_NO_CITA_NEW_UPDT)
                        ->get('articulos_orden a')
                        ->result();                        
 
  $this->blade->render('tabla_trabajos',$datos);                        
}
public function explicacion_leyenda(){
  $this->blade->render('explicacion_leyenda');
}
public function desactivar_horarios_asesores(){
    foreach ($_POST['seleccion'] as $key => $value) {
      $this->db->where('id',$key)->set("activo",0)->set("motivo",$this->input->post('motivo_desactivar'))->update('aux');  
    }
    echo 1;exit();
}
public function en_correo(){
  $cuerpo = $this->blade->render('citas/correo_prueba',array(),TRUE);
  //print_r($cuerpo);die();
  enviar_correo('albertopitava@gmail.com',"¡Cotización!",$cuerpo,array());
}

  public function UpdateViewCita(){

    if($this->input->post()){
        $this->form_validation->set_rules('vehiculo_numero_serie', '# serie', 'trim|exact_length[17]|required');
        $this->form_validation->set_rules('id_cita', '# cita', 'trim|required');
        $this->form_validation->set_rules('vehiculo_kilometraje', 'kilometraje', 'trim|required|numeric');
        $this->form_validation->set_rules('vehiculo_placas', 'placas', 'trim|required');
        $this->form_validation->set_rules('id_color', 'color', 'trim|required');

        
        
       if ($this->form_validation->run()){
            $datos = array('vehiculo_numero_serie' => $this->input->post('vehiculo_numero_serie'), 
                          'vehiculo_kilometraje' => $this->input->post('vehiculo_kilometraje'), 
                          'vehiculo_placas' => $this->input->post('vehiculo_placas'), 
                          'id_color' => $this->input->post('id_color'), 
            );
            $this->db->where('id_cita',$this->input->post('id_cita'))->update('citas',$datos);
            echo 1;exit();
          }else{
             $errors = array(
                  'id_cita' => form_error('id_cita'),
                  'vehiculo_numero_serie' => form_error('vehiculo_numero_serie'),
                  'vehiculo_kilometraje' => form_error('vehiculo_kilometraje'),
                  'vehiculo_placas' => form_error('vehiculo_placas'),
                  'id_color' => form_error('id_color'),
             );
            echo json_encode($errors); exit();
          }
      }

    $data['input_vehiculo_numero_serie'] = form_input('vehiculo_numero_serie','','class="form-control"  id="vehiculo_numero_serie" maxlength="17" ');

    $data['input_vehiculo_kilometraje'] = form_input('vehiculo_kilometraje','','class="form-control"  id="vehiculo_kilometraje" ');

    $data['input_vehiculo_placas'] = form_input('vehiculo_placas','','class="form-control"  id="vehiculo_placas" ');

    $data['drop_color'] = form_dropdown('id_color',array_combos($this->mcat->get('cat_colores','color'),'id','color',TRUE),"",'class="form-control busqueda" id="id_color"');


    $data['btn_guardar'] = form_button('btnguardar','Guardar cita',' id="guardar" class="btn btn-success btn-block pull-right"');
    $this->blade->render('UpdateViewCita',$data);
  }
  public function getCitaId(){
    $info= $this->mc->getCitaId($this->input->post('id_cita'));
    if(count($info)==1){
      echo json_encode(array('exito'=>true,'datos'=>$info[0]));
    }else{
      echo json_encode(array('exito'=>false));
    }
  }

//Cambiar hora de la operación
public function cambiar_hora_operacion(){
      $idorden = $this->input->get('idorden');
      $id_cita = $this->mc->getIdCitaByOrden($idorden);
      $id_tecnico = $this->input->get('id_tecnico');
      $principal = $this->input->get('principal');
      $tabla = 'tecnicos_citas';
      $id_operacion = $this->input->get('id_operacion');
      if(!$principal){
        $tabla = 'tecnicos_citas_historial';
      }
      if($this->input->post()){
        if($this->input->post('dia_completo')==1){
          $this->form_validation->set_rules('id_tecnico', 'técnico', 'trim|required');
          $this->form_validation->set_rules('fecha_inicio', 'fecha inicio', 'trim|required');
          $this->form_validation->set_rules('fecha_fin', 'fecha fin', 'trim|required');
          $this->form_validation->set_rules('hora_comienzo', 'hora inicio día', 'trim|required');
          
        }else{
          $this->form_validation->set_rules('id_tecnico', 'técnico', 'trim|required');
          $this->form_validation->set_rules('hora_inicio', 'hora inicio', 'trim|required');
          $this->form_validation->set_rules('hora_fin', 'hora fin', 'trim|required');
        }
       if ($this->form_validation->run()){
          $this->mc->cambiarHorarioOperacion();

          }else{
             $errors = array(
                  'id_tecnico' => form_error('id_tecnico'),
                  'hora_inicio' => form_error('hora_inicio'),
                  'hora_fin' => form_error('hora_fin'),
                  'fecha_inicio' => form_error('fecha_inicio'),
                  'fecha_fin' => form_error('fecha_fin'),
             );
            echo json_encode($errors); exit();
          }
      }
      $info= $this->ml->getCitaAsignadaId($id_tecnico,$id_operacion,'',$tabla);
      $info=$info[0];
      $id = $info->id_operacion;
      //funcion para saber si en la cita hay un registro con días completo
      $info_dia_completo = $this->ml->getCitaAsignadaId($id_tecnico,$id_operacion,1,$tabla);
      if(count($info_dia_completo)>0){
         $info_dia_completo = $info_dia_completo[0];
      }else{
          $info_dia_completo = new Stdclass();
      }
      $info_dia_extra = $this->ml->getCitaAsignadaId2($id_tecnico,$id_operacion,0,$tabla);
      if(count($info_dia_extra)>0){
        $info_dia_extra = $info_dia_extra[0];
      }else{
         $info_dia_extra = new Stdclass();
      }
      $dia_completo = $this->ml->getDiaCompleto($id_operacion,$tabla);


      $data['input_id_detalle_horarios'] = $id;
      $data['input_id_tecnico'] = $id_tecnico;
      $data['tabla'] = $tabla;
      $data['id_tecnico_seleccionado'] = $this->input->get('id_tecnico_seleccionado');
      $data['idorden'] = $this->input->get('idorden');
      $data['input_hora_inicio'] = form_input('hora_inicio',substr(set_value('hora_inicio',exist_obj($info,'hora_inicio_dia')),0,5),'class="form-control check_hora" id="hora_inicio"','time');
      
      $data['input_hora_fin'] = form_input('hora_fin',substr(set_value('hora_fin',exist_obj($info,'hora_fin')),0,5),'class="form-control check_hora" id="hora_fin"','time');
      //Aveces pueden reasignar una cita a un técnico por eso pongo el campo para que lo reasigne en otra fecha
       $data['input_fecha_reasignar'] = form_input('fecha_reasignar',date2sql($this->ml->getFechaTecnicosCita($id,$tabla)),'class="form-control" id="fecha_reasignar"','date');
      //extra

      //Cuando son días completos
      $data['input_fecha_inicio'] = form_input('fecha_inicio',(set_value('fecha',exist_obj($info_dia_completo,'fecha'))),'class="form-control" id="fecha_inicio"','date');

      $data['input_fecha_fin'] = form_input('fecha_fin',(set_value('fecha_fin',exist_obj($info_dia_completo,'fecha_fin'))),'class="form-control" id="fecha_fin"','date');

      if($dia_completo==1){
        $valor_fecha_parcial = (set_value('fecha',exist_obj($info_dia_extra,'fecha')));
        $valor_hora_inicio = set_value('hora_inicio',exist_obj($info_dia_extra,'hora_inicio'));
        $valor_hora_fin = set_value('hora_fin',exist_obj($info_dia_extra,'hora_fin'));
      }else{
        $valor_fecha_parcial ='';
        $valor_hora_inicio ='';
        $valor_hora_fin = '';
      }

      $data['input_fecha_parcial'] = form_input('fecha_parcial',$valor_fecha_parcial,'class="form-control" id="fecha_parcial"','date');

       $data['input_hora_inicio_extra'] = form_input('hora_inicio_extra',$valor_hora_inicio,'class="form-control check_hora" id="hora_inicio_extra"','time');

      $data['input_hora_fin_extra'] = form_input('hora_fin_extra',$valor_hora_fin,'class="form-control check_hora" id="hora_fin_extra"','time');

        //si es por día completo saber cuando empieza
      $data['input_hora_comienzo'] = form_input('hora_comienzo',set_value('hora_inicio_dia',exist_obj($info_dia_completo,'hora_inicio_dia')),'class="form-control check_hora" id="hora_comienzo"','time');

      $data['drop_tecnicos'] = form_dropdown('id_tecnico',array_combos($this->mcat->get('tecnicos','nombre','',array('activo'=>1,'baja_definitiva'=>0)),'id','nombre',TRUE,FALSE,FALSE),$id_tecnico,'class="form-control" id="id_tecnico"');

      $data['dia_completo'] = $dia_completo;

      $this->blade->render('cambiar_hora_operacion',$data);
}
  //Saber si existe la orden por id horario
  public function existeOrdebByHorario(){
    $q = $this->db->where('a.id',$_POST['idhorario'])
                  ->join('citas c','c.id_horario=a.id')
                  ->join('ordenservicio o','c.id_cita = o.id_cita')
                  ->get('aux a');
    echo $q->num_rows();
    exit();
  }
public function validarGO(){
  $this->blade->render('validar_go');
}
public function g_oasis(){
    $data['numero_serie'] = $this->input->post('numero_serie');
    $data['kilometraje'] = $this->input->post('kilometraje');
    $this->blade->render('g_oasis',$data);
}
public function unidades_instalacion(){
  $data['unidades'] = $this->curl->getinfoGet(CONST_PREPEDIDOS);
  $this->blade->render('unidades_instalacion',$data);
}
public function updateModelos(){
  $modelos = $this->curl->getinfoGet(CONST_CATALOGO_AUTOS);
  foreach ($modelos as $m => $modelo) {
    $datos = array( 'modelo' => $modelo->nombre, 
                    'tiempo_lavado' => $modelo->tiempo_lavado, 
                    'categoria_id' => $modelo->categoria_id, 

    );
    $this->db->insert('cat_modelo',$datos);
  }
}
public function test(){
  echo encrypt(100).'<br>';
  echo decrypt('Nzc4MzI1Nzc=');
  exit();
  $this->curl->getInfoPUT(CONST_ACTUALIZAR_CITA_PREPEDIDO.'/1',array('servicio_cita_id'=>'174701'));
}
function parse_xml() { 
  $uri = 'http://codigo87.com/brk/prueba.xml'; 
  $response = file_get_contents($uri); 
   debug_var($response);
  $items_names = $this->myxml($response); 
 
} 
function myxml($xml_str) { 
    $items = array(); 
    $xml_doc = new SimpleXMLElement($xml_str); 
    debug_var($xml_doc);die();
    foreach ($xml_doc->item as $item) { 
        $items []= $item->name; 
    } 
 
    return $items; 
} 
public function xml(){
  $url = "http://www.semov-servicios.col.gob.mx/wssefome/ws_sp.php?dato=12356";
  $json = file_get_contents($url);
  $json_data = json_decode($json, true);
  echo '<pre>';
  print_r($json_data);
  echo '</pre>';
  die();
}
public function xml_auth(){
  $username = 'user';
  $password = 'pass';
  $context = stream_context_create(array(
      'http' => array(
          'header'  => "Authorization: Basic " . base64_encode("$username:$password")
      )
  ));
  $data = file_get_contents('http://www.semov-servicios.col.gob.mx/wssefome/ws_foto.php?curp=PIVL911222HCMTZS05&cat=1', false, $context);
  $json_data = json_decode($data, true);
  echo ' <img src="data:image/png;base64, '.$json_data['data'].'" alt="Red dot" />';
}
  public function unidades_en_taller(){
    $data['unidades'] = $this->mc->getCitaOrdenFord();
    $this->blade->render('v_unidades_taller',$data);
  }
  public function buscar_unidades_taller(){
    $data['unidades'] = $this->mc->getCitaOrdenFord();
    $this->blade->render('v_tabla_unidades_taller',$data);
  }
  public function exportarUnidadesTaller(){
        $this->load->library('PHPExcel');
        // create file name
        $fileName = 'unidades_proceso_'.time().'.xlsx';  
        // load excel library
        $unidades = $this->mc->getCitaOrdenFord();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', '# Cita');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Cliente');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Telefono');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Placas');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Modelo');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Fecha recepcion');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Cita previa');  
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Asesor');  
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Fecha asesor');            
        // set Row
        $rowCount = 2;
        foreach ($unidades as $list) {
          $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $list->id_cita);
          $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount,  $list->datos_nombres.' '.$list->datos_apellido_paterno.' '.$list->datos_apellido_materno);
          $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $list->datos_telefono);
          $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $list->fecha_entrega.' '.$list->hora_entrega);
          $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $list->vehiculo_modelo);
          $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, date_eng2esp($list->fecha_recepcion));
          $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, ($list->cita_previa==1)?'Si':'No');
          $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $list->asesor);
          $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $list->fecha_asesor.' '.$list->hora_asesor);
          $rowCount++;
        }
        $filename = "unidades_proceso_". date("Y-m-d-H-i-s").".csv";
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0'); 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
        $objWriter->save('php://output'); 
  }
  public function prueba_correo(){
    $cuerpo = 'Hola mundo';
    enviar_correo('albertopitava@gmail.com',"¡Cotización!",$cuerpo,array());
  }
  public function prueba_mensaje(){
    $mensaje = 'orden https://sohexdms.net/cs/companycars/servicios_ford/dms_taller/OrdenServicio_PDF/MTM1OTg0NDA4NTgw';
    $celular = '3123094368';
    $id_cita = 11;
    //$this->sms_general_texto($celular,$mensaje);
    $datos_cita = $this->mc->getDatosCita($id_cita);
    $celular_sms = $this->mc->telefono_contacto($id_cita);
    
    $mensaje_sms = CONST_AGENCIA_MENSAJE.", Unidad lista para entrega, orden #".$id_cita." placas: ".$datos_cita->vehiculo_placas;
    
    $this->sms_general_texto($celular_sms,$mensaje_sms);

  }
  public function getDifMoth(){
    $meses =  (int)diferenciaMeses($_POST['fecha'],date('Y-m-d'));
    echo $meses;exit();
  }
  public function datosOrdenesOld(){
    $datos = $this->db->where('placas',$_POST['busqueda'])->or_where('serie',$_POST['busqueda'])->get('ordenes_old')->row();
    echo json_encode(array('datos'=>$datos));
  }
  public function ViewPresupuestos($serie=''){

      $contenedor["serie"] = $this->input->post('serie');


      $url = CONST_URL_CONEXION."api/DuplicarRegistros/ordenes_serie";
      $ch = curl_init($url);
      //Creamos el objeto json para enviar por url los datos del formulario
      $jsonDataEncoded = json_encode($contenedor);
      //Para que la peticion no imprima el resultado como una cadena, y podamos manipularlo
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      //Adjuntamos el json a nuestra petición
      curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonDataEncoded);
      //Agregamos los encabezados del contenido
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

      //Obtenemos la respuesta
      $response = curl_exec($ch);

      //debug_var($response);die();
      // Se cierra el recurso CURL y se liberan los recursos del sistema
      curl_close($ch);
      $result = json_decode($response,true);
      if($this->input->post('mostrar_tabla')){
        $datos = $result;
        echo $this->mc->getTablaRefacciones($datos);
        
      }else{
        if(count($result)>0){
         echo 1;
        }else{
          echo 0;
        } 
      }
      
      exit();
  }
  //Búsqueda de un cliente por nombre
  public function getDataByNombre(){
    if (function_exists("set_time_limit") == TRUE AND @ini_get("safe_mode") == 0)
    {
        @set_time_limit(1000000);
    }
    //Ismodal es para mostrar el modal en caso de que sean varios clientes
    $modal = $this->input->post('modal');
    $datos_clientes = $this->db->like('datos_nombres',$_POST['nombre'])->or_like('datos_apellido_paterno',$_POST['nombre'])
        ->or_like('datos_apellido_materno',$_POST['nombre'])
        ->join('ordenservicio o','o.id_cita=c.id_cita','left')
        ->get('citas c')->result();
    if(!$modal){
      if(is_array($datos_clientes)){
        if(count($datos_clientes)==1){
          echo json_encode(array('cantidad'=>1,'datos'=>$datos_clientes));
        }else{
          echo json_encode(array('cantidad'=>count($datos_clientes),'datos'=>array()));
        }
      }else{
        echo json_encode(array('cantidad'=>0,'datos'=>array()));
      }
    }else{
      $data['datos_clientes'] = $datos_clientes;
      $this->blade->render('lista_clientes_servicio',$data);
    }
  }
  //Obtener todos los datos de la cita y orden
  public function getFullDataCita(){
    $datos_cita = $this->db->where('c.id_cita',$_POST['id_cita'])
        ->join('ordenservicio o','o.id_cita=c.id_cita','left')
        ->get('citas c')->row();
    echo json_encode(array('cantidad'=>1,'datos'=>$datos_cita));
  }
  public function encrypt(){
    echo encrypt(43988);
  }
  public function decrypt(){
    echo decrypt(43988);
  }
  public function buscar_citas_id(){
    $data['tecnicos_citas'] = $this->db->join('tecnicos t','t.id=tc.id_tecnico')->select('t.nombre as tecnico,tc.*')->where('id_cita',$_GET['id_cita'])->get('tecnicos_citas tc')->result();
    $data['tecnicos_citas_historial'] = $this->db->join('tecnicos t','t.id=tc.id_tecnico')->select('t.nombre as tecnico,tc.*')->where('id_cita',$_GET['id_cita'])->get('tecnicos_citas_historial tc')->result();
    $this->blade->render('citas_id',$data);
  }
  public function test_cancelar_dms(){
    print_r(eliminar_orden_dms(188395));
  }
  public function prueba_cierre_orden(){
    $json = '{"folio_id": 6668,"cliente_id": "2501","concepto": "SERVICIO 180000 KMS","importe": "3600","total": "3600","fecha": "2022-05-14","usuario_gestor_id": null,"tipo_forma_pago_id": 1,"tipo_pago_id": 23,"plazo_credito_id": 14,"enganche": 0,"tasa_interes": 0,"intereses": 0,"estatus_cuenta_id": 1,"numero_orden": "188336T","comentarios": "{\"serie\":\"1FM5K7D85DGB65754\",\"tipo_vehiculo\":\"EXPLORER\",\"placas\":\"UME922A\",\"color\":\"ROJO MANZANA\",\"orden\":\"188336\",\"version\":null,\"fecha_recepcion\":\"14\\/05\\/2022\",\"hora_recepcion\":\"08:05:29\",\"asesor\":\"HERIBERTO ALMARAZ GONZALEZ\",\"trasmision\":\"AUTOM\\u00c1TICA\",\"nombre_compania\":\"FEDERICO CASTA\\u00d1EDA SALDIVAR \",\"nombre_contacto_compania\":\"FEDERICO\",\"ap_contacto\":\"CASTA\\u00d1EDA\",\"am_contacto\":\"SALDIVAR\",\"correo_compania\":\"castanedaf@gmail.com\",\"calle\":\"CUMBRES\",\"nointerior\":\"6\",\"noexterior\":\"93\",\"colonia\":\"COLINAS DEL SUR\",\"municipio\":\"CORERGIDORA\",\"cp\":\"76903\",\"estado\":\"Queretaro\",\"sigla_estado\":\"QE\",\"rfc\":\"XAXX010101000\",\"km\":\"180693\",\"modelo\":\"2013\"}","updated_at": "2022-05-14T18:07:03.000000Z","created_at": "2022-05-14T18:07:03.000000Z","id": 1903}';
    // echo '<pre>';
    // print_r((json_decode($json)));die();
    $this->m_dms->crear_cuenta_dms(188336,1);
  }
} //FIN CONTROLADOR
