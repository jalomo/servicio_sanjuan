<?php
//https://github.com/Eonasdan/bootstrap-datetimepicker/issues/1996
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MX_Controller {
	 public function __construct()
    {
      parent::__construct();
      date_default_timezone_set(CONST_ZONA_HORARIA);
    }

  //APP
 public function insertar_orden(){
      $data['diagnostico'] = $this->input->post('diagnostico');
      $data['fecha_elaboracion'] = $this->input->post('fecha_elaboracion');
      $data['fecha_aceptacion'] = $this->input->post('fecha_aceptacion');
      $data['nombre_asesor'] = $this->input->post('nombre_asesor');
      $data['nombre_consumidor'] = $this->input->post('nombre_consumidor');
      $data['llavero'] = $this->input->post('llavero');
      $data['seguro_rines'] = $this->input->post('seguro_rines');
      $data['indicadores_de_falla'] = $this->input->post('indicadores_de_falla');
      $data['rociadores_limpiaparabrisas'] = $this->input->post('rociadores_limpiaparabrisas');
      $data['claxon'] = $this->input->post('claxon');
      $data['luces_delanteras'] = $this->input->post('luces_delanteras');
      $data['luces_traseras'] = $this->input->post('luces_traseras');
      $data['luces_stop'] = $this->input->post('luces_stop');
      $data['radio_caratula'] = $this->input->post('radio_caratula');
      $data['pantallas'] = $this->input->post('pantallas');
      $data['aire_acondicionado'] = $this->input->post('aire_acondicionado');
      $data['encendedor'] = $this->input->post('encendedor');
      $data['vidrios'] = $this->input->post('vidrios');
      $data['espejos'] = $this->input->post('espejos');
      $data['seguros_electricos'] = $this->input->post('seguros_electricos');
      $data['disco_compacto'] = $this->input->post('disco_compacto');
      $data['asientos_vestiduras'] = $this->input->post('asientos_vestiduras');
      $data['tapete_cajuela'] = $this->input->post('tapete_cajuela');
      $data['herramienta'] = $this->input->post('herramienta');
      $data['gato_llave'] = $this->input->post('gato_llave');
      $data['reflejantes'] = $this->input->post('reflejantes');
      $data['cables'] = $this->input->post('cables');

      $this->db->insert('ordenes_app', $data);
      $id_orden=  $this->db->insert_id();

      $data1['extintor'] = $this->input->post('extintor');
      $data1['llanta_refaccion'] = $this->input->post('llanta_refaccion');
      $data1['tapones_rueda'] = $this->input->post('tapones_rueda');
      $data1['gomas_limpiadores'] = $this->input->post('gomas_limpiadores');
      $data1['antena'] = $this->input->post('antena');
      $data1['tapon_gasolina'] = $this->input->post('tapon_gasolina');
      $data1['poliza_garantia'] = $this->input->post('poliza_garantia');
      $data1['manual'] = $this->input->post('manual');
      $data1['seguro_rines2'] = $this->input->post('seguro_rines2');
      $data1['certificacion_verificacion'] = $this->input->post('certificacion_verificacion');
      $data1['tarjeta_circulacion'] = $this->input->post('tarjeta_circulacion');
      $data1['nivel_gasolina'] = $this->input->post('nivel_gasolina');
      $data1['articulos_personales'] = $this->input->post('articulos_personales');
      $data1['cuales'] = $this->input->post('cuales');
      $data1['desea_reportar_algo_mas'] = $this->input->post('desea_reportar_algo_mas');
      $data1['numero_orden'] = $this->input->post('numero_orden');

      $this->db->update('ordenes_app', $data1, array('id'=>$id_orden));

      $data3['costo'] = $this->input->post('costo');
      $data3['dia'] = $this->input->post('dia');
      $data3['mes'] = $this->input->post('mes');
      $data3['anio'] = $this->input->post('anio');
      $data3['firma_asesor'] = $this->input->post('firma_asesor');
      $data3['firma_presupuesto'] = $this->input->post('firma_presupuesto');

      $this->db->update('ordenes_app', $data3, array('id'=>$id_orden));

      $response['error'] = 0;
      $response['id'] = $id_orden;
      echo json_encode($response); //
    }
  //FIN APP

}
