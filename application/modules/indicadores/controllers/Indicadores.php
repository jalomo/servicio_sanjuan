<?php
//https://techarise.com/import-excel-file-mysql-codeigniter/
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . "/third_party/PHPExcel.php";
class Indicadores extends MX_Controller {
public $titulos = array(
					'1' => 'Número de operadores en contacto proactivo',
					'2' => 'Número de citas realizadas por proactivo',
					'3' => 'Número de citas asistidas por proactivo',
					'4' => 'Número de citas programadas por el departamento de citas',
					'5' => 'Número de citas asistidas por el departamento de citas',
					'6' => '# de no show',
					'7' => 'Porcentaje de citas recuperadas por recontacto no show',
					'8' => 'Número de citas atendidas a tiempo',
					'9' => 'Número de citas entregadas a tiempo',
					'10' => 'Número de citas reagendadas',
					'11' => 'Productividad de los técnicos en horas',
					'12' => 'Productividad de los técnicos en monto de venta',
					'13' => 'Eficiencia de horas vendidas vs horas tabuladas',
					'14' => 'Porcentaje de utilización del tiempo',
					'15' => 'Venta de mano de obra taller',
					'16' => 'Venta de mano de obra mantenimiento',
					'17' => 'Venta de mano de obra garantía',
					'18' => 'Venta de mano de obra interna',
					'19' => 'Monto de gastos generados por el departamento',
					'20' => 'Ordenes abiertas con vehículos en taller',
					'21' => 'Ordenes abiertas con vehículos fuera del taller',
					'22' => 'Ordenes cerradas en cero sin cargo por servicio',
					'23' => 'Tiempo promedio de reparación por unidad',
					'24' => 'Tiempo promedio de estadía por unidad en taller desde recepción a entrega',
					'25' => 'Ordenes de garantía abiertas',
					'26' => 'Ordenes de garantía emitidas a planta',
					'27' => 'Ordenes de garantía cobradas a planta',
					'28' => 'Ordenes de garantía con problemas de autorización',
					'29' => 'Número de citas sin estatus',
					'30' => 'Número de citas sin reservación',
					'31' => 'Número de citas eliminadas',
);
public function __construct()
{
  parent::__construct();
  $this->load->model('citas/m_catalogos','mcat');
  $this->load->model('m_indicadores','me');
  date_default_timezone_set(CONST_ZONA_HORARIA);

   if(!PermisoModulo()){
   	 redirect(site_url('citas/ver_citas'));
   }
}
 public function gerencia(){
  	$this->blade->render('estadisticas_iframe');
  }
  public function index() {
  	$datos['operadores_contacto'] = $this->me->getNameOperadores();
    $datos['citas_proactivo'] = $this->me->citasPorProactivo();
	
	$this->blade->render('estadisticas',$datos);
	//debug_var($datos);die();
  }
  public function citas_proactivo($id=''){
  	$usuarios_contacto = $this->db->select('a.adminId,a.adminNombre')
	    					 ->join(CONST_BASE_PRINCIPAL.'admin a','p.id_usuario = a. adminId')
	    					 ->group_by('a.adminId,a.adminNombre')
	    					 ->get('proactivo_historial_citas p')
	    					 ->result();
	   //lquery();die();
 	$datos['drop_usuarios'] = form_dropdown('id_usuario',array_combos($usuarios_contacto,'adminId','adminNombre',TRUE),'','class="form-control" id="id_usuario" ');

 	$datos['citas_realizadas_contacto'] = $this->me->citasPorProactivo();
	$datos['citas_asistidas_contacto'] = $this->me->citasAsistidasProactivo();
	if(!$this->input->post()){
		if(isset($this->titulos[$id])){
			$datos['titulo'] = $this->titulos[$id];
		}else{
			$datos['titulo'] = '';
		}
		
	}
 	if($this->input->post()){
 		echo json_encode(array('citas_realizadas_contacto'=>$datos['citas_realizadas_contacto'],'citas_asistidas_contacto'=>$datos['citas_asistidas_contacto']));die();
 	}
  	
  	
  	$this->blade->render('citas_proactivo',$datos);
  }
  public function citas_departamento($id=''){
  	$asesores = $this->db->select('id,nombre')
	    					 ->get('operadores')
	    					 ->result();
 	$datos['drop_asesores'] = form_dropdown('id_asesor',array_combos($asesores,'nombre','nombre',TRUE),'','class="form-control" id="id_asesor" ');
 	
 	if($this->input->post()){
 		$id = $_POST['id'];
 	}
 	if($id==4){
 		//Número de citas programadas por el departamento de citas
 		$datos['citas'] = $this->me->getCitasGeneradas();
 	}else if($id==5){
 		//Número de citas asistidas por el departamento de citas
 		$datos['citas'] = $this->me->getCitasEfectivasNoEfectivas(true);
 		
 	}else if($id==6){
 		//Porcentaje de no show
 		$datos['citas'] = $this->me->getCitasEfectivasNoEfectivas(false);
 	}else if($id==7){
 		//Porcentaje de citas recuperadas por recontacto no show
 		$citas_reagendadas = $this->me->getCitasReagendadas();
 		$noshow = $this->me->getCitasEfectivasNoEfectivas(false);
 		if($noshow==0){
			$datos['citas'] = 0;
		}else{
			$datos['citas'] = ($citas_reagendadas * 100) / $noshow;
		}
 	}else if($id==8){
 		//Número de citas atendidas a tiempo
 		$datos['citas'] = $this->me->citasaTiempo();
 	}else if($id==9){
 		//Número de citas entregadas a tiempo
 		$datos['citas'] = $this->me->citasEntregadasAtiempo();
 	}else if($id==10){
 		//Número de citas reagendadas
 		$datos['citas'] = $this->me->getCitasReagendadas();
 	}else if($id==29){
 		//Número de citas sin estatus
 		$datos['citas'] = $this->me->getCitasEfectivasNoEfectivas(false,true);
 	}else if($id==30){
 		//Número de citas sin reservación
 		$datos['citas'] = $this->me->citasSinReservacion();
 	}else if($id==31){
 		//Número de citas eliminadas
 		$datos['citas'] = $this->me->citasEliminadas();
 	}
	if(!$this->input->post()){
		if(isset($this->titulos[$id])){
			$datos['titulo'] = $this->titulos[$id];
			$datos['id'] = $id;
		}else{
			$datos['titulo'] = '';
			$datos['id'] = '';
		}
	}
	//print_r($_POST);die();
 	if($this->input->post()){
 		echo json_encode(array(
 						'citas'=>$datos['citas'],
 						'titulo'=>$_POST['titulo'],
 		));
 		die();
 	}
  	
  	$this->blade->render('citas_departamento',$datos);
  }
  //Productividad de los técnicos en horas //todo lo que esté trabajando
  public function productividad_tecnicos(){
  	$asesores = $this->me->getAsesores();
  	//debug_var($asesores);die();
  	$tecnicos = $this->db->select('distinct(usuario)')
  							->where('usuario !=','')
  							->where('usuario !=','ADMINISTRADOR')
  							->where('mostrar_linea','1')
  							->where('status_anterior','trabajando')
  							->where_not_in('usuario',array($asesores))
  							->where_not_in('status_anterior',array('Llegó tarde','Llegó'))
  							->where_not_in('status_nuevo',array('Llegó tarde','Llegó'))
	    					 ->get('transiciones_estatus')
	    					 ->result();
 	$datos['drop_tecnicos'] = form_dropdown('tecnico',array_combos($tecnicos,'usuario','usuario',TRUE),'','class="form-control" id="tecnico" ');
  	$productividad_tecnicos = $this->me->getProductividadTecnico();
  	$array_productividad = array();
  	

  	foreach ($productividad_tecnicos as $key => $value) {
  		$array_productividad[$value->usuario][] = $value;
  	}
  	
  	$array_tiempo = array();
  	foreach ($array_productividad as $key => $tecnico) {
  		
  		
  		
  		$suma_total = 0;
  		foreach($tecnico as $t => $info){
  			$suma_minutos = 0;
  			$suma_minutos = $this->me->diferencia_horas_laborales($info->inicio,$info->fin);
  			$suma_total = $suma_minutos + $suma_total;
  		}
  		$array_tiempo[$key] = round($suma_total/60,2);
  	}

  	if($this->input->post()){
  		$array_ajax = array();
  		
  		foreach ($array_tiempo as $a => $valor) {
  			$array_ajax[] = array('tecnico'=>$a,'valor'=>$valor);
  		}

  		echo json_encode(array('datos'=>$array_ajax));die();
 	}

  	$datos['datos_tecnicos'] = $array_tiempo;

  	$this->blade->render('productividad_tecnicos',$datos);
  }
  //Ordenes abiertas con vehiculos dentro/fuera taller
  public function ordenes_abiertas_dentro_fuera_taller($id=0){
  	$asesores = $this->db->select('id,nombre')
	    					 ->get('operadores')
	    					 ->result();
 	$datos['drop_asesores'] = form_dropdown('id_asesor',array_combos($asesores,'nombre','nombre',TRUE),'','class="form-control" id="id_asesor" ');

  	$datos['ordenes_abiertas_taller'] = $this->me->ordenesAbiertasCerradas(true);
  	//lquery();die();
	$datos['ordenes_cerradas_taller'] = $this->me->ordenesAbiertasCerradas(false);

	if(!$this->input->post()){
		if(isset($this->titulos[$id])){
			$datos['titulo'] = $this->titulos[$id];
		}else{
			$datos['titulo'] = '';
		}
		
	}
	if($this->input->post()){
 		echo json_encode(array(
 						'ordenes_abiertas_taller'=>$datos['ordenes_abiertas_taller'],
 						'ordenes_cerradas_taller'=>$datos['ordenes_cerradas_taller'],
 		));

 		die();
 	}
  	
  	$this->blade->render('ordenes_abiertas_dentro_fuera_taller',$datos);
  }

	//Tiempo promedio de reparación por unidad
  public function tiempoPromedioReparacionUnidad($id=0){

  	$status = $this->db->select('id,nombre')
	    					 ->get('estatus')
	    					 ->result();
 	$datos['drop_estatus'] = form_dropdown('id_status_color',array_combos($status,'id','nombre',TRUE),'','class="form-control" id="id_status_color" ');


  	if(!$this->input->post()){
		if(isset($this->titulos[$id])){
			$datos['titulo'] = $this->titulos[$id];
		}else{
			$datos['titulo'] = '';
		}
		
	}

	$tiempo_promedio = $this->me->tiempoPromedioReparacionUnidad();
	$estatus = 'CAMPAÑAS';
	$array_suma = array();
	foreach ($tiempo_promedio as $key => $value) {

		if($estatus==$value->estatus){
			if(!isset($array_suma[$value->estatus])){
				$array_suma[$value->estatus] = 0;
			}
			$array_suma[$value->estatus] = $array_suma[$value->estatus] +$this->me->getTiempoReparacionPorUnidad($value->id_cita);
		}else{
			$estatus=$value->estatus;

		}
	}
	if($this->input->post()){
  		$array_ajax = array();
  		
  		foreach ($array_suma as $e => $valor) {
  			$array_ajax[] = array('estatus'=>$e,'valor'=>$valor);
  		}

  		echo json_encode(array('datos'=>$array_ajax));die();
 	}
	$datos['promedio'] = $array_suma;
	$this->blade->render('tiempo_promedio_reparacion',$datos);
	
  }
  public function tiempo_promedio_estadia_unidad($id=0){
  	$unidades = $this->me->unidadesEntregadas();
  	$suma = 0;
  	foreach ($unidades as $key => $unidad) {
  		$suma = $suma +$this->me->getTiempoEstadiaPorUnidad($unidad->id_cita);
  	}
  	if(!$this->input->post()){
		if(isset($this->titulos[$id])){
			$datos['titulo'] = $this->titulos[$id];
		}else{
			$datos['titulo'] = '';
		}
		
	}else{
		echo json_encode(array('promedio'=>$suma/60));die();
	}
  	$datos['promedio'] = $suma/60;
	$this->blade->render('tiempo_promedio_estadia',$datos);
  }
  public function ventas(){
  	$this->blade->render('ventas');
  }
  public function garantiasCantidad($id=0){
  	$data['url'] = CONST_URL_BASE.'Indicador_GarantiasConteo/'.$id;
  	$this->blade->render('garantias',$data);
  }
  public function garantias_monto_pesos($id=0){
  	$data['url'] = CONST_URL_BASE.'Indicador_GarantiasMonto/'.$id;
  	$this->blade->render('garantias',$data);
  }


  

  
}
