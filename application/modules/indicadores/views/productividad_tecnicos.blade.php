@layout('tema_luna/layout')
@section('contenido')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<h2 class="text-center">Productividad de los técnicos en horas</h2>
<form action="" id="frm">
	<div class="row">
        <div class="col-md-3 col-sm-6">
            <label for="">Técnico</label>
            {{$drop_tecnicos}}
        </div>
		<div class='col-md-3 col-sm-6'>
            <label for="">Fecha inicio</label>
            <div class="form-group1">
                <div class='input-group date' id='datetimepicker1'>
                    <input type="text" name="finicio" id="finicio" value="">
                    <span class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                    </span>
                </div>
            </div>
            <span style="color: red" class="error error_fini"></span>
        </div>
        <div class='col-md-3 col-sm-6'>
            <label for="">Fecha Fin</label>
            
            <div class='input-group date' id='datetimepicker2'>
                <input type="text" name="ffin" id="ffin" value="">
                <span class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </span>
            </div>
            
            <span style="color: red" class="error error_ffin"></span>
        </div>
        <div class="col-md-3 col-sm-6">
            <button type="button" style="margin-top: 30px" id="buscar" class="btn btn-success">Buscar</button>
        </div>
	</div>
</form>
<div class="row">
	<div class="col-sm-12">
		<h2 class="text-center"></h2>
		<div id="container" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
	</div>
</div>
@endsection

@section('scripts')
<script>
	 $("#datetimepicker1").on("dp.change", function (e) {
      $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
    });
    $("#datetimepicker2").on("dp.change", function (e) {
        $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
    });
	var site_url = "{{site_url()}}";
	$('.date').datetimepicker({
      format: 'DD/MM/YYYY',
      icons: {
          time: "far fa-clock",
          date: "fa fa-calendar",
          up: "fa fa-arrow-up",
          down: "fa fa-arrow-down"
      },
       locale: 'es'
    });

    

    var array_datos = [];
    @foreach($datos_tecnicos as $t => $value)
        
        array_datos.push({
            name: "{{$t}}",
            y: parseFloat("{{$value}}")
        });
    @endforeach


	// Create the chart
var chart = Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.0f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
    },

    series: [
        {
            name: "#horas",
            colorByPoint: true,
            data: array_datos
        }
    ]
});
$('#buscar').on('click', function() {
    if($("#finicio").val()=='' || $("#finicio").val()==''){
        ErrorCustom("Es necesario ingresar ambas fechas");
    }else{
        var url =site_url+"/indicadores/productividad_tecnicos/";
        ajaxJson(url,$("#frm").serialize(),"POST","sync",function(result){
            result = JSON.parse( result );
            var array_datos = [];

            $.each(result.datos,function(i,item){
                array_datos.push({
                    name: item.tecnico,
                    y: parseFloat(item.valor,)
                });
            });
            actualizarChart(array_datos);
            
        });
        
    }
    
    
});
function actualizarChart(array_datos = []){                 
    chart.update({
            series: [
                {
                    name: "#Citas",
                    colorByPoint: true,
                    data: array_datos
                }
            ]
    })
    chart.redraw()
    
}
</script>
@endsection