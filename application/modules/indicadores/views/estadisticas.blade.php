@layout('tema_luna/layout')
@section('contenido')
<h2 class="text-center"> <i class="fa fa-bar-chart" aria-hidden="true"></i> Indicadores <i class="fa fa-bar-chart" aria-hidden="true"></i></h2>
<div class="container bg-light border pb-2">
<nav class="navbar navbar-light ">
  <form class="form-inline">
    <input class="form-control mr-sm-4 srch" type="text" placeholder="Buscar indicador">
  </form>
</nav>
<div id="accordion" role="tablist">
  <div class="card">
    <div class="card-header" role="tab" id="headingOne">
      <span class="badge badge-primary badge-pill incat-count">3</span>
        <a target="_blank" data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Contacto Proactivo
        </a>
    </div>
    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
      <div class="card-body p-0">
        <ul class="list-group cat-list">
    	<li class="list-group-item">Número de operadores en contacto proactivo: <strong>{{$operadores_contacto}}</strong></li>
         <li class="list-group-item"><a target="_blank" href="{{base_url('indicadores/citas_proactivo/2')}}">Número de citas realizadas por proactivo </a></li>
         <li class="list-group-item"><a target="_blank" href="{{base_url('indicadores/citas_proactivo/3')}}">Número de citas asistidas por proactivo</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingTwo">
      
        <span class="badge badge-primary badge-pill incat-count">10</span>
        <a target="_blank" class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Departamento de citas
        </a>
      
    </div>
    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="card-body p-0">
        <ul class="list-group cat-list">
         <li class="list-group-item"><a target="_blank" href="{{base_url('indicadores/citas_departamento/4')}}">Número de citas programadas por el departamento de citas </a></li>
         <li class="list-group-item"><a target="_blank" href="{{base_url('indicadores/citas_departamento/5')}}">Número de citas asistidas por el departamento de citas </a></li>
         <li class="list-group-item"><a target="_blank" href="{{base_url('indicadores/citas_departamento/6')}}"># de no show </a></li>
         <li class="list-group-item"><a target="_blank" href="{{base_url('indicadores/citas_departamento/7')}}">Porcentaje de citas recuperadas por recontacto no show  </a></li>
         <li class="list-group-item"><a target="_blank" href="{{base_url('indicadores/citas_departamento/8')}}">Número de citas atendidas a tiempo </a></li>
         <li class="list-group-item"><a target="_blank" href="{{base_url('indicadores/citas_departamento/9')}}">Número de citas entregadas a tiempo</a> </li>
         <li class="list-group-item"><a target="_blank" href="{{base_url('indicadores/citas_departamento/10')}}">Número de citas reagendadas</a></li>

         <li class="list-group-item"><a target="_blank" href="{{base_url('indicadores/citas_departamento/29')}}">Número de citas sin estatus</a></li>

         <li class="list-group-item"><a target="_blank" href="{{base_url('indicadores/citas_departamento/30')}}">Número de citas sin reservación</a></li>

         <li class="list-group-item"><a target="_blank" href="{{base_url('indicadores/citas_departamento/31')}}">Número de citas eliminadas</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingTwo">
      
        <span class="badge badge-primary badge-pill incat-count">2</span>
        <a target="_blank" class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Técnicos
        </a>
      
    </div>
    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="card-body p-0">
        <ul class="list-group cat-list">
         <li class="list-group-item"><a target="_blank" href="{{base_url('indicadores/productividad_tecnicos/11')}}">Productividad de los técnicos en horas 
		</a></li>
         <li class="list-group-item">Productividad de los técnicos en monto de venta </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingTwo">
      
        <span class="badge badge-primary badge-pill incat-count">4</span>
        <a target="_blank" class="collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          Ventas
        </a>
      
    </div>
    <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="card-body p-0">
        <ul class="list-group cat-list">

        <li class="list-group-item">
          <a target="_blank" href="{{base_url('indicadores/ventas')}}">Venta de mano de obra taller </a> 
        </li>
        <li class="list-group-item">
          <a target="_blank" href="{{base_url('indicadores/ventas')}}">Venta de mano de obra mantenimiento </a> 
        </li>
		    <li class="list-group-item">
          <a target="_blank" href="{{base_url('indicadores/ventas')}}">Venta de mano de obra garantía 
          </a> 
        </li>
		    <li class="list-group-item">
          <a target="_blank" href="{{base_url('indicadores/ventas')}}">Venta de mano de obra interna </a> 
      </li>
        </ul>
      </div>
    </div>

  </div>

  <div class="card">
    <div class="card-header" role="tab" id="headingTwo">
        <span class="badge badge-primary badge-pill incat-count">7</span>
        <a target="_blank" class="collapsed" data-toggle="collapse" href="#collapseFifth" aria-expanded="false" aria-controls="collapseFifth">
          Orden / cantidad de garantías
        </a>
    </div>
    <div id="collapseFifth" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="card-body p-0">
        <ul class="list-group cat-list">

        <li class="list-group-item"><a target="_blank" href="{{base_url('indicadores/ordenes_abiertas_dentro_fuera_taller/20')}}">Ordenes abiertas con vehículos en taller </a></li>
        <li class="list-group-item">
          <a target="_blank" href="{{base_url('indicadores/garantiasCantidad/1')}}">Ordenes de garantía abiertas</a> 
        </li>
    		<li class="list-group-item">
          <a target="_blank" href="{{base_url('indicadores/garantiasCantidad/2')}}">Ordenes de garantía emitidas a planta</a> 
        </li>
    		<li class="list-group-item">
          <a target="_blank" href="{{base_url('indicadores/garantiasCantidad/3')}}">Ordenes de garantía cobradas a planta</a> 
        </li>
    		<li class="list-group-item">
          <a target="_blank" href="{{base_url('indicadores/garantiasCantidad/4')}}">Ordenes de garantía con problemas de autorización</a>
        </li>
    		<li class="list-group-item">
          <a target="_blank" href="{{base_url('indicadores/ventas')}}">Ordenes cerradas en cero sin cargo por servicio </a>
        </li>
    		<li class="list-group-item"><a target="_" href="{{base_url('indicadores/ordenes_abiertas_dentro_fuera_taller/21')}}">Ordenes abiertas con vehículos fuera del taller </a></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- garantias monto en pesos --->
   <div class="card">
    <div class="card-header" role="tab" id="headingTwo">
        <span class="badge badge-primary badge-pill incat-count">7</span>
        <a target="_blank" class="collapsed" data-toggle="collapse" href="#collapseGarantias" aria-expanded="false" aria-controls="collapseGarantias">
          Ordenes con Garantías (Monto en pesos)
        </a>
    </div>
    <div id="collapseGarantias" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="card-body p-0">
        <ul class="list-group cat-list">

        <li class="list-group-item">
          <a target="_blank" href="{{base_url('indicadores/garantias_monto_pesos/1')}}">Ordenes de garantía abiertas</a> 
        </li>
        <li class="list-group-item">
          <a target="_blank" href="{{base_url('indicadores/garantias_monto_pesos/2')}}">Ordenes de garantía emitidas a planta</a> 
        </li>
        <li class="list-group-item">
          <a target="_blank" href="{{base_url('indicadores/garantias_monto_pesos/3')}}">Ordenes de garantía cobradas a planta</a> 
        </li>
        <li class="list-group-item">
          <a target="_blank" href="{{base_url('indicadores/garantias_monto_pesos/4')}}">Ordenes de garantía con problemas de autorización</a>
        </li>
        </ul>
      </div>
    </div>
  </div4

   <div class="card">
    <div class="card-header" role="tab" id="headingTwo">
      
        <span class="badge badge-primary badge-pill incat-count">5</span>
        <a target="_blank" class="collapsed" data-toggle="collapse" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
          General
        </a>
    </div>
    <div id="collapseSix" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="card-body p-0">
        <ul class="list-group cat-list">
		<li class="list-group-item">
			<a target="_blank" href="{{base_url('indicadores/tiempo_promedio_estadia_unidad/24')}}">	Tiempo promedio de estadía por unidad en taller desde recepción a entrega 
			</a>
        <li class="list-group-item">
        	Eficiencia de horas vendidas vs horas tabuladas 
        </li>
        <li class="list-group-item">Porcentaje de utilización del tiempo </li>
		<li class="list-group-item">Monto de gastos generados por el departamento </li>
		<li class="list-group-item">Tiempo promedio de reparación por unidad </li>
		</li>
        </ul>
      </div>
    </div>
  </div>

</div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function(){
    var site_url = "{{site_url()}}";
    $('.cat-list li').addClass('fnd');
    function counter_set()
    {
        $('.cat-list').each(function() {
        var cnt = $(this).children('.cat-list li.fnd').length;
      
        $(this).parent().parent().parent().find('.incat-count').text(cnt);
                                        });
    }
    
    counter_set();
    
    $('.srch').keyup(function(){
        var txt = $(this).val().toLowerCase();
        $('.cat-list li').filter(function(){
            var mt = $(this).text().toLowerCase().indexOf(txt) > -1;
            $(this).toggle(mt);
            $(this).toggleClass('fnd', mt);
        });
        counter_set();
  });
});
</script>
@endsection