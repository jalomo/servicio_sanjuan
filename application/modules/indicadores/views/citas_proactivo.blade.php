@layout('tema_luna/layout')
@section('contenido')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<h2 class="text-center">{{$titulo}}</h2>
<form action="" id="frm">
	<div class="row">
        <div class="col-md-3 col-sm-6">
            <label for="">Usuario</label>
            {{$drop_usuarios}}
        </div>
		<div class='col-md-3 col-sm-6'>
            <label for="">Fecha inicio</label>
            <div class="form-group1">
                <div class='input-group date' id='datetimepicker1'>
                    <input type="text" name="finicio" id="finicio" value="">
                    <span class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                    </span>
                </div>
            </div>
            <span style="color: red" class="error error_fini"></span>
        </div>
        <div class='col-md-3 col-sm-6'>
            <label for="">Fecha Fin</label>
            
            <div class='input-group date' id='datetimepicker2'>
                <input type="text" name="ffin" id="ffin" value="">
                <span class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </span>
            </div>
            
            <span style="color: red" class="error error_ffin"></span>
        </div>
        <div class="col-md-3 col-sm-6">
            <button type="button" style="margin-top: 30px" id="buscar" class="btn btn-success">Buscar</button>
        </div>
	</div>
</form>
<div class="row">
	<div class="col-sm-12">
		<h2 class="text-center"></h2>
		<div id="container" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
	</div>
</div>
@endsection

@section('scripts')
<script>
	 $("#datetimepicker1").on("dp.change", function (e) {
      $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
    });
    $("#datetimepicker2").on("dp.change", function (e) {
        $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
    });
	var site_url = "{{site_url()}}";
	$('.date').datetimepicker({
      format: 'DD/MM/YYYY',
      icons: {
          time: "far fa-clock",
          date: "fa fa-calendar",
          up: "fa fa-arrow-up",
          down: "fa fa-arrow-down"
      },
       locale: 'es'
    });


	// Create the chart
var chart = Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: "{{$titulo}}"
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.0f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
    },

    series: [
        {
            name: "#Citas",
            colorByPoint: true,
            data: [
                {
                    name: "# de citas realizadas por proactivo",
                    y: {{$citas_realizadas_contacto}},
                    drilldown: "# de citas realizadas por proactivo"
                },
                {
                    name: "Número de citas asistidas por proactivo",
                    y: {{$citas_asistidas_contacto}},
                    drilldown: "Número de citas asistidas por proactivo"
                },
                
            ]
        }
    ]
});
$('#buscar').on('click', function() {
    if($("#finicio").val()=='' || $("#finicio").val()==''){
        ErrorCustom("Es necesario ingresar ambas fechas");
    }else{
        var url =site_url+"/indicadores/citas_proactivo/";
        ajaxJson(url,$("#frm").serialize(),"POST","sync",function(result){
            result = JSON.parse( result );
                actualizarChart(result.citas_realizadas_contacto,result.citas_asistidas_contacto);
            
        });
        
    }
    
    
});
function actualizarChart(citas_realizadas_contacto=0,citas_asistidas_contacto=0){

    var series= [
                    {
                        name: "# de citas realizadas por proactivo",
                        y: parseInt(citas_realizadas_contacto),
                        drilldown: "# de citas realizadas por proactivo"
                    },
                    {
                        name: "Número de citas asistidas por proactivo",
                        y: parseInt(citas_asistidas_contacto),
                        drilldown: "Número de citas asistidas por proactivo"
                    },
                        
                ]                   
    chart.update({
            series: [
                {
                    name: "#Citas",
                    colorByPoint: true,
                    data: series
                }
            ]
    })
    chart.redraw()
    
}
</script>
@endsection