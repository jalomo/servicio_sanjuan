<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_indicadores extends CI_Model{
	public function __construct(){
		parent::__construct();
		date_default_timezone_set(CONST_ZONA_HORARIA);
	}
	//Operadores en contactoo proactivo
	public function getNameOperadores(){
	    $nombres =  $this->db->select('a.adminNombre')
	    					 ->join(CONST_BASE_PRINCIPAL.'admin a','p.id_usuario = a. adminId')
	    					 ->group_by('a.adminNombre')
	    					 ->get('proactivo_historial p')
	    					 ->result();

	    $cadena = '';
	   	foreach ($nombres as $key => $value) {
	   		$cadena.=$value->adminNombre.',';
	   	}
	   	return substr($cadena, 0,-1);
	   	
	}
	//Citas por proactivo
	public function citasPorProactivo(){
		if($this->input->post()){
	      $this->db->where('created_at >=',date2sql($_POST['finicio']));
	      $this->db->where('created_at <=',date2sql($_POST['ffin']));
	      if($_POST['id_usuario']!=''){
	        $this->db->where('id_usuario',$_POST['id_usuario']);
	      }
	    }
		return $this->db->select('count(id) as total')->get('proactivo_historial_citas')->row()->total;
	}
	//Citas asistidas por proactivo
	public function citasAsistidasProactivo(){
		return $this->db->select('count(c.id_cita) as total')
						->join('citas c','p.id_cita = c.id_cita')
						->join('aux a','c.id_horario = a.id')
						->where_in('a.id_status_cita',array(2,3))
						->get('proactivo_historial_citas p')
						->row()
						->total;
	}
	//Citas generadas pro el departamento de citas
	public function getCitasGeneradas(){
	    if($this->input->post()){
	      $this->db->where('c.fecha >=',date2sql($_POST['finicio']));
	      $this->db->where('c.fecha <=',date2sql($_POST['ffin']));
	      if($_POST['id_asesor']!=''){
	        $this->db->where('asesor',$_POST['id_asesor']);
	      }
	    }
	    //$this->db->where_in('a.id_status_cita',array(1));
	    return $this->db->select('count(id_cita)  as total_citas')->join('aux a','c.id_horario = a.id')->get('citas c')->row()->total_citas;
  	}
  	public function getCitasEfectivasNoEfectivas($mostrarEfectivas=true,$soloSinStatus=false){
  		if($soloSinStatus){
  			$this->db->where_in('a.id_status_cita',array(1));
  		}else{
		    if($mostrarEfectivas){
		      $this->db->where_in('a.id_status_cita',array(2,3,5));
		    }else{
		      $this->db->where_in('a.id_status_cita',array(4));
		    }
		}
		if($this->input->post()){
	      $this->db->where('c.fecha >=',date2sql($_POST['finicio']));
	      $this->db->where('c.fecha <=',date2sql($_POST['ffin']));
	      if($_POST['id_asesor']!=''){
	        $this->db->where('c.asesor',$_POST['id_asesor']);
	      }
	    }
	    
	    return $this->db->select('count(c.id_cita) as total_citas_efectivas')
	                  ->join('aux a','c.id_horario = a.id')
	                  ->get('citas c')
	                  ->row()
	                  ->total_citas_efectivas;
  	}
  	//Citas sin reservacion
  	public function citasSinReservacion(){
  		if($this->input->post()){
	      $this->db->where('c.fecha >=',date2sql($_POST['finicio']));
	      $this->db->where('c.fecha <=',date2sql($_POST['ffin']));
	      if($_POST['id_asesor']!=''){
	        $this->db->where('c.asesor',$_POST['id_asesor']);
	      }
	    }
	    return $this->db->select('count(c.id_cita) as total')
    					//->join('ordenservicio o','c.id_cita = o.id_cita')
	    				->where('cita_previa',0)
    					->get('citas c')
		                 ->row()
		                 ->total;

  	}
  	public function getCitasReagendadas(){
	    if($this->input->post()){
	      $this->db->where('c.fecha >=',date2sql($_POST['finicio']));
	      $this->db->where('c.fecha <=',date2sql($_POST['ffin']));
	      if($_POST['id_asesor']!=''){
	        $this->db->where('c.asesor',$_POST['id_asesor']);
	      }
	    }
    	return $this->db->select('count(c.id_cita) as total_reagendadas')
                  ->where('c.reagendada',1)
                  ->get('citas c')
                  ->row()
                  ->total_reagendadas;
    }
    //Ordenes Abiertas / Cerradas en taller
    public function ordenesAbiertasCerradas($abiertas=1){
    	if($this->input->post()){
	      $this->db->where('c.fecha >=',date2sql($_POST['finicio']));
	      $this->db->where('c.fecha <=',date2sql($_POST['ffin']));
	      if($_POST['id_asesor']!=''){
	        $this->db->where('c.asesor',$_POST['id_asesor']);
	      }
	    }
    	return $this->db->select('count(c.id_cita) as total')
    					->join('ordenservicio o','c.id_cita = o.id_cita')
    					//->where('unidad_entregada is null')
    					->where('unidad_entregada',$abiertas)
    					->get('citas c')
		                 ->row()
		                 ->total;

    }
    //Productividad de técnicos
    public function getProductividadTecnico(){
    	if($this->input->post()){
	      $this->db->where('fin >=',date2sql($_POST['finicio']));
	      $this->db->where('fin <=',date2sql($_POST['ffin']));
	      if($_POST['tecnico']!=''){
	        $this->db->where('usuario',$_POST['tecnico']);
	      }
	    }
    	
    	
    	return $this->db->where('status_anterior','trabajando')->where('usuario is not null')->where('usuario !=','')->where('usuario !=','ADMINISTRADOR')->where('mostrar_linea',1)->where_not_in('status_anterior',array('Llegó tarde','Llegó'))->where_not_in('status_nuevo',array('Llegó tarde','Llegó'))->get('transiciones_estatus')->result();
    }
    //Citas atendidas a tiempo
    public function citasaTiempo(){
    	$where = '';
    	if($this->input->post()){
	      $where.= ' and c.fecha >='.date2sql($_POST['finicio']);
	      $where.= ' and c.fecha <='.date2sql($_POST['finicio']);
	      if($_POST['id_asesor']!=''){
	      	$where.= ' and c.asesor ='.'"'.$_POST['id_asesor'].'"';
	      }
	    }
    	$query = " select count(c.id_cita) as total_atiempo
        from citas c
		join aux a on c.id_horario = a.id
    	where a.id_status_cita in (2,3)".$where."
		and (select fecha_creacion from transiciones_estatus
     		 where id_cita =c.id_cita and status_nuevo = 'llegó'limit 1) < CONCAT(a.fecha,' ', ADDTIME(TIME(a.hora),'00:10'))";

     	//echo $query;die();
     	return $this->db->query($query)->result()[0]->total_atiempo;
    }
    //Citas entregadas a tiempo
    public function citasEntregadasAtiempo(){
    	$where = '';
    	if($this->input->post()){
	      $where.= ' and c.fecha >='.date2sql($_POST['finicio']);
	      $where.= ' and c.fecha <='.date2sql($_POST['finicio']);
	      if($_POST['id_asesor']!=''){
	      	$where.= ' and c.asesor ='.'"'.$_POST['id_asesor'].'"';
	      }
	    }
    	$query = "select count(c.id_cita) as total_entregadas_tiempo
 		from citas c
 		join ordenservicio o on c.id_cita = o.id_cita
     	where concat(o.fecha_entrega,' ',o.hora_entrega) >= c.fecha_entrega_unidad".$where;
     	return $this->db->query($query)->result()[0]->total_entregadas_tiempo;

    }
    public function diferencia_horas_laborales($fecha_inicio='',$fecha_fin=''){
    if($fecha_inicio=='0000-00-00 00:00:00' || $fecha_fin == '0000-00-00 00:00:00'){
      return 0;
    }
    // ESTABLISH THE MINUTES PER DAY FROM START AND END TIMES
    $start_time = '09:00:00';
    $end_time = '18:00:00';

    $start_ts = strtotime($start_time);
    $end_ts = strtotime($end_time);
    $minutes_per_day = (int)( ($end_ts - $start_ts) / 60 )+1;

    // ESTABLISH THE HOLIDAYS
    $holidays = array
    (
    //'Feb 04', // MLK Day
    );

    // CONVERT HOLIDAYS TO ISO DATES
    foreach ($holidays as $x => $holiday)
    {
    $holidays[$x] = date('Y-m-d', strtotime($holiday));
    }

    $fecha_sol=$fecha_inicio;
    $fecha_menor=$fecha_fin;

    //var_dump($fecha_sol,$fecha_menor);die();
    // CHECK FOR VALID DATES
    $start = strtotime($fecha_sol);
    $end = strtotime($fecha_menor);
    $start_p = date('Y-m-d H:i:s', $start);
    $end_p = date('Y-m-d H:i:s', $end);

    // MAKE AN ARRAY OF DATES
    $workdays = array();
    $workminutes = array();
    // ITERATE OVER THE DAYS
    $start = $start - 60;
    while ($start < $end)
    {
    $start = $start + 60;
    // ELIMINATE WEEKENDS - SAT AND SUN
    $weekday = date('D', $start);
    //echo $weekday;die();
    //if (substr($weekday,0,1) == 'S') continue;
    // ELIMINATE HOURS BEFORE BUSINESS HOURS
    $daytime = date('H:i:s', $start);
    if(($daytime < date('H:i:s',$start_ts))) continue;
    // ELIMINATE HOURS PAST BUSINESS HOURS
    $daytime = date('H:i:s', $start);
    if(($daytime > date('H:i:s',$end_ts))) continue;
    // ELIMINATE HOLIDAYS
    $iso_date = date('Y-m-d', $start);
    if (in_array($iso_date, $holidays)) continue;
    $workminutes[] = $iso_date;
    // END ITERATOR
    }

    //
    $number_of_workminutes = (count($workminutes));
    $number_of_minutes = number_format($minutes_per_day);
    $horas_habiles = number_format($number_of_workminutes/60 ,2);

    if($number_of_workminutes>0){
      $number_of_minutes = $number_of_workminutes-1;
    }
     return $number_of_workminutes;

  }
  public function getAsesores(){
  	$asesores = $this->db->get('operadores')->result();
  	$array_asesores = array();
  	foreach ($asesores as $key => $value) {
  		$array_asesores[] = "'".$value->nombre."'";
  		
  	}
  	return implode(',', $array_asesores);
  }
  //Tiempo promedio reparación por unidad
  public function tiempoPromedioReparacionUnidad(){

  	if($this->input->post()){
      $this->db->where('c.fecha >=',date2sql($_POST['finicio']));
      $this->db->where('c.fecha <=',date2sql($_POST['ffin']));
      if($_POST['id_status_color']!=''){
        $this->db->where('c.id_status_color',$_POST['id_status_color']);
      }
    }

	return $this->db->select('e.nombre as estatus,c.id_cita')
					->join('ordenservicio o','o.id_cita = c.id_cita ')
					->join('estatus e','c.id_status_color = e.id')
					->order_by('e.nombre')
					->get('citas c ')
					->result();
  }

  //Obtener el tiempo de una unidad
  public function getTiempoReparacionPorUnidad($id_cita= ''){
  	$info = $this->db->where('id_cita',$id_cita)->where('status_anterior','trabajando')->get('transiciones_estatus')->result();
  	$suma = 0;
  	foreach ($info as $key => $value) {
  		$suma = $suma + $this->diferencia_horas_laborales($value->inicio,$value->fin);
  	}
  	return $suma;
  }
  //Obtener el tiempo de una unidad
  public function getTiempoEstadiaPorUnidad($id_cita= ''){
  	$info = $this->db->where('id_cita',$id_cita)->get('transiciones_estatus')->result();
  	$suma = 0;
  	foreach ($info as $key => $value) {
  		$suma = $suma + $this->diferencia_horas_laborales($value->inicio,$value->fin);
  	}
  	return $suma;
  }
  //Unidades entregadas
  public function unidadesEntregadas(){
  	if($this->input->post()){
      $this->db->where('c.fecha >=',date2sql($_POST['finicio']));
      $this->db->where('c.fecha <=',date2sql($_POST['ffin']));
    }
  	return $this->db->where('unidad_entregada',1)->get('citas c')->result();
  }
  //Citas eliminadas
  public function citasEliminadas(){
  	if($this->input->post()){
      $this->db->where('created_at >=',date2sql($_POST['finicio']));
      $this->db->where('created_at <=',date2sql($_POST['ffin']));
    }
    return $this->db->select('count(id_cita) as total')
    					->get('historial_eliminaciones')
		                 ->row()
		                 ->total;

  }

	   
} 