<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_integracion extends CI_Model{
  private $_batchImport;
  public $estatus_operacion_terminada = 'Operación terminada';
  public $estatus_terminado = 'terminado';

  
	public function __construct(){
		parent::__construct();
		date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  //FIN PROACTIVO ITELISIS
  public function getIdCitaByOrden($idorden=''){
    $q = $this->db->where('id',$idorden)->select('id_cita')->from('ordenservicio')->get();
    if($q->num_rows()==1){
      return $q->row()->id_cita;
    }else{
      return '';
    }
  }
  public function PlannedStart($id_operacion='',$principal=0){
      //Principal es para saber si voy a la tabla de tecnicos_citas(1) o tecnicos_citas_historial 
      $tabla = 'tecnicos_citas';
      if(!$principal){
          $this->db->where('carryover',0);
          $tabla = 'tecnicos_citas_historial';
      }
      $q = $this->db->where('id_operacion',$id_operacion)->where('historial',0)->get($tabla)->result();
      
      if(count($q)==1){
          return $q[0]->fecha.' '.$q[0]->hora_inicio_dia;
      }else if(count($q)==2){
          //Falta cuando son dos
          $fecha1 = $q[0]->fecha.' '.$q[0]->hora_inicio_dia;
          $fecha2 = $q[1]->fecha.' '.$q[1]->hora_inicio_dia;

          if($fecha1<=$fecha2){
            return $fecha1;
          }else{
            return $fecha2;
          }
      }else{
          return '';
      }
  }
  public function PlannedFinish($id_operacion=''){

      $array_fechas = array();
      $fechas_tecnicos_citas = $this->db->where('id_operacion',$id_operacion)->where('historial',0)->get('tecnicos_citas')->result();
      foreach($fechas_tecnicos_citas as $f => $value){
        $array_fechas[] = $value->fecha_fin.' '.$value->hora_fin;
      }

      //Tecnicos citas historial
      $fechas_tecnicos_citas_historial = $this->db->where('id_operacion',$id_operacion)->where('historial',0)->get('tecnicos_citas_historial')->result();
      foreach($fechas_tecnicos_citas_historial as $f => $value){
        $array_fechas[] = $value->fecha_fin.' '.$value->hora_fin;
      }
      return $this->orderData($array_fechas);
  }
  public function ActualJob($id_operacion=''){
    $q = $this->db->where('id_operacion',$id_operacion)->limit(1)->where('status_nuevo','trabajando')->get('transiciones_estatus');
    if($q->num_rows()==1){
      return $q->row()->fecha_creacion;
    }else{
      return '';
    }
  }
  public function FinishJob($id_operacion=''){
    $q = $this->db->where('id_operacion',$id_operacion)->order_by('id','desc')->limit(1)->where('status_nuevo',$this->estatus_operacion_terminada)->get('transiciones_estatus');
    if($q->num_rows()==1){
      return $q->row()->fecha_creacion;
    }else{
      $q = $this->db->where('id_operacion',$id_operacion)->order_by('id','desc')->limit(1)->where('status_nuevo',$this->estatus_terminado)->get('transiciones_estatus');
      if($q->num_rows()==1){
        return $q->row()->fecha_creacion;
      }else{
        return '';
      }
    }
  }
  //Obtener las citas con ordenes con el cierre de orden
  public function getCitaOrdenFord(){
    return $this->db
                    //->where('c.unidad_entregada',0)
                    //->or_where('c.unidad_entregada is null')
                    ->where('o.generado_ford',0)
                    ->where('o.fecha_factura is null')
                    ->where('c.id_cita >',CONST_NO_CITA_NEW_UPDT)
                    ->where('o.id_tipo_orden',1)
                    ->where('o.fecha_recepcion >=',CONST_FECHA_CITA_NEW_UPDT)
                    ->where('c.id_status_color !=',3)
                    ->join('ordenservicio o','o.id_cita = c.id_cita')
                    ->select('c.*,o.*,o.id as idorden,c.unidad_entregada')
                    ->get('citas c')
                    ->result();
  }
  //Obtiene el ID estar de un asesor por nombre
  public function getIdStarAsesorNombre($nombre=''){
    if($nombre==''){
      return '';
    }
    $q = $this->db->where('nombre',$nombre)->select('idStars')->get('operadores');
    if($q->num_rows()==1){
      return $q->row()->idStars;
    }else{
      return '';
    }
  }
  //Obtiene el ID estar de un asesor por nombre
  public function getIdStarLavador($id_lavador=''){
    $q = $this->db->where('id',$id_lavador)->select('idStars')->get('lavadores');
    if($q->num_rows()==1){
      return $q->row()->idStars;
    }else{
      return '';
    }

  }
  //Obtener el check-in de la cita
  public function getCheckIn($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->select('fecha_creacion')->where_in('status_nuevo',array('Llegó','Llegó tarde'))->limit(1)->get('transiciones_estatus');
    if($q->num_rows()==1){
      return $q->row()->fecha_creacion;
    }else{
      return '';
    }
  }
  //Firma de documentos (sería orden parte 2)
  public function getFirmaClienteOP2($id_cita=''){
    $q = $this->db->where('noServicio',$id_cita)->select('fecha_actualiza')->get('diagnostico');
    if($q->num_rows()==1){
      return $q->row()->fecha_actualiza;
    }else{
      return '';
    }
  }
  //Hora de inicio de la cita
  public function JobCardCreatedDate($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)->where('dia_completo',0)->get('tecnicos_citas');
    if($q->num_rows()==1){
      return $q->row()->fecha.' '.$q->row()->hora_inicio_dia;
    }else{
      return '';
    }
  }
  //Fecha y hora de la cita
  public function getFechaCita($idhorario=''){
    $q = $this->db->where('id',$idhorario)->get('aux');
    if($q->num_rows()==1){
      return $q->row()->fecha.' '.$q->row()->hora;
    }else{
      return '';
    }
  }
  //Funcion para saber si ya existe el registro en data sample
  public function existeDataSample($id_cita=''){
      $q = $this->db->where('id_cita',$id_cita)->get('RO_Data_Sample');
      if($q->num_rows()==1){
        return $q->row()->fecha.' '.$q->row()->hora;
      }else{
        return '';
      }
  }

    //Consultas para generar CSV
    public function datos_RO_Data_Sample()
    {
        $query = $this->db->where('orden_cerrada','0')
            ->select('*')
            ->where('Signed_Documents !=','0000-00-00 00:00:00')
            ->where('Signed_Documents !=','')
            ->where('o.generado_ford',0)
            ->join('ordenservicio o','o.id_cita = r.id_cita')
            ->get('RO_Data_Sample r')
            ->result();
        return $query;
    }

    public function datos_data_services()
    {
        $query = $this->db->where('ld.orden_cerrada','0')
            ->join('RO_Data_Services AS ds','ds.RO_Number = ld.RO_Number')
            ->select('ds.*')
            ->get('RO_Data_Sample AS ld')
            ->result();
        return $query;
    }

    public function datos_data_parts()
    {
        $query = $this->db->where('ld.orden_cerrada','0')
            ->join('RO_Data_Parts AS dp','dp.RO_Number = ld.RO_Number')
            ->select('dp.*')
            ->get('RO_Data_Sample AS ld')
            ->result();
        return $query;
    }
    //Saber si existe el registro RO_Data_Sample
    public function existeRO_Data_Sample($id_cita=''){
      $q = $this->db->where('id_cita',$id_cita)->get('RO_Data_Sample');
      if($q->num_rows()==1){
        return true;
      }else{
        return false;
      }
    }
    //Saber si existe el registro RO_Data_Services
    public function existeRO_Data_Services($idarticulo=''){
      $q = $this->db->where('idarticulo',$idarticulo)->get('RO_Data_Services');
      if($q->num_rows()==1){
        return true;
      }else{
        return false;
      }
    }
    //Saber si existe el registro RO_Data_Services de lavado
    public function existeRO_Data_Services_Lavado($RO_Number_Service=''){
      $q = $this->db->where('RO_Number',$RO_Number_Service)->where('lavado',1)->get('RO_Data_Services');
      if($q->num_rows()==1){
        return true;
      }else{
        return false;
      }
    }
    //Saber si existe el registro RO_Data_PARTS
    public function existeRO_Data_Parts($idoperacion='',$id_registro='',$cotizacion_extra=0){
      
      $q = $this->db->where('idoperacion',$idoperacion)->where('id_registro',$id_registro)->where('cotizacion_extra',$cotizacion_extra)->get('RO_Data_Parts');
      if($q->num_rows()==1){
        return true;
      }else{
        return false;
      }
    }
    public function Planned_Job_Start($id_cita=''){
      $q = $this->db->where('id_cita',$id_cita)->select('fecha,hora_inicio_dia')->limit(1)->order_by('id','desc')->get('tecnicos_citas');
      if($q->num_rows()==1){
        return $q->row()->fecha.' '. $q->row()->hora_inicio_dia;
      }else{
        return '';
      }
    }
    public function Planned_Job_Finish($id_cita){
      $array_fechas = array();
      $fechas_tecnicos_citas = $this->db->where('id_cita',$id_cita)->where('historial',0)->get('tecnicos_citas')->result();
      foreach($fechas_tecnicos_citas as $f => $value){
        $array_fechas[] = $value->fecha_fin.' '.$value->hora_fin;
      }

      //Tecnicos citas historial
      $fechas_tecnicos_citas_historial = $this->db->where('id_cita',$id_cita)->where('historial',0)->get('tecnicos_citas_historial')->result();
      foreach($fechas_tecnicos_citas_historial as $f => $value){
        $array_fechas[] = $value->fecha_fin.' '.$value->hora_fin;
      }
     

      return $this->orderData($array_fechas);

    }

    public function orderData($datos = array()){

      if(!isset($datos[0])){
        return '';
      }
      $mayor = $datos[0];
      foreach($datos as $d => $dato){
        if($mayor <= $dato){
          
          $mayor = $dato;
        }
      }
      return $mayor;
    }
    //Actual_Job_Start (obtengo el primer registro en trabajando)
    public function Actual_Job_Start($id_cita=''){
      $q = $this->db->where('id_cita',$id_cita)->where('status_nuevo','trabajando')->limit(1)->get('transiciones_estatus');
      if($q->num_rows()==1){
        return $q->row()->fin;
      }else{
        return '';
      }
    }
    //Actual_Job_Finish (obtengo el último registro en terminado)
    public function Actual_Job_Finish($id_cita=''){
      $q = $this->db->where('id_cita',$id_cita)->where('status_nuevo','terminado')->limit(1)->order_by('id','desc')->get('transiciones_estatus');
      if($q->num_rows()==1){
        return $q->row()->fin;
      }else{
        return '';
      }
    }
    //Job_Completion_Date fecha unidad entregada
    public function Job_Completion_Date($id_cita=''){
      $q = $this->db->where('id_cita',$id_cita)->get('citas');
      if($q->num_rows()==1){
        return $q->row()->fecha_entrega_unidad;
      }else{
        return '';
      }
    }

    //Sacar la situación de la orden
    public function RO_Type_Code($idorden=''){
      $q = $this->db->where('id',$idorden)->get('ordenservicio');
      if($q->num_rows()==1){
        $id_status_intelisis=  $q->row()->id_status_intelisis;
        $cancelada=  $q->row()->cancelada;
        if($cancelada==1){
          return 'X';
        }
        if($id_status_intelisis!=4){
          return 'C';
        }else{
          return 'X';
        }
      }else{
        return 'X';
      }
    }

    //Obtener datos de la orden con base a su id
    public function getDataOrderId($idorden=''){
      return $this->db->where('id',$idorden)->get('ordenservicio')->row();
    }
    //Obtener el codigo_departamento
    // public function getCodigoDepartamento($idorden=''){
    //   $q = $this->db->where('o.id',$idorden)
    //                 ->join('cat_tipo_orden c','o.id_tipo_orden = c.id')
    //                 ->get('ordenservicio o');
    //   if($q->num_rows()==1){
    //     return $q->row()->codigo_departamento;
    //   }else{
    //     return '';
    //   }                                
    // }
     public function getCodigoDepartamento($id_cita=''){
      $q = $this->db->where('c.id_cita',$id_cita)
                    ->join('estatus e','e.id=c.id_status_color')
                    ->limit(1)
                    ->select('e.codigo')
                    ->get('citas c');
      if($q->num_rows()==1){
        return $q->row()->codigo;
      }else{
        return '';
      }                                
    }
    //Obtener IdStar técnico que tiene la operación
    public function getIdStarTecnicoByOperacion($idoperacion=''){
      $q = $this->db->where('a.id',$idoperacion)
                    ->select('t.nombre as tecnico,t.idStars')
                    ->join('tecnicos t','a.id_tecnico = t.id')
                    ->get('articulos_orden a');
      if($q->num_rows()==1){
        return $q->row()->idStars;
      }else{
        return null;
      }    
    }

    //Line_Status saber en que estatus se encuentra la operación
    public function Line_Status($idoperacion=''){
      // T = En tiempo
      // A = Próximo a hora de término
      // R = Retrasado
      // D = Detenido
      // F = Finalizado
      // C = Cancelado
      // N = No planificado

      $id_status_operacion = $this->getDataOperacionId($idoperacion)->id_status;
      $principal = $this->getDataOperacionId($idoperacion)->principal;
      switch ($id_status_operacion) {
        case '':
          return 'T';
          break;
        case 4:
          return 'R';
          break;
        case 15:
        case 16:
        case 17:
        case 18:
        case 19:
        case 20:
        case 21:
        case 22:
          return 'D';
          break;
        case 3:
        case 9:
        case 23:
            return 'F';
            break;
        case 5:
          return 'C';
          break;
        default:
          return $this->tiempoOperacion($idoperacion,$principal);
          break;
      }
      
    }
  //Validar tiempo operacion 
  public function tiempoOperacion($id_operacion='',$principal=0){
    //En esta funcion voy a ver el horario asignado (fecha en que termina) para la cita, eje: si es un registro debería terminar
    //el 2020-03-03 12:00:00 le resto 15 minutos (2020-03-03 11:45:00) y si la fecha actual es mayor a esa fecha y menor a la de la cita
    //Entonces se considera que está próximo a la fecha de termino, si la fecha actual es mayor a la fecha de la cita está retrasado
    //si la fecha actual es mejor a la fecha de la cita, entonces está en tiempo, cuando son dos registros por el día completo, voy a una función
    //que regresa la fecha mayor entre esos días y hago el mismo proceso


    
    $fecha_actual = date("Y-m-d H:i:s");
    $fecha_cita =  $this->getAllDates($id_operacion);

    
    $time = strtotime($fecha_cita);
    $time = $time - (15 * 60);
    $fecha_proximo_terminar = date("Y-m-d H:i:s", $time); //Le resté 15 min a la fecha

      
      if($fecha_actual >= $fecha_proximo_terminar && $fecha_actual <= $fecha_cita){
        return 'A';
      }else if($fecha_actual <=$fecha_proximo_terminar){
        return 'T';
      }else if($fecha_actual >= $fecha_cita){
        return 'R';
      }

  }
  public function getAllDates($id_operacion=''){
    $array_fechas = array();
    $fechas_tecnicos_citas = $this->db->where('id_operacion',$id_operacion)->where('historial',0)->get('tecnicos_citas')->result();
    foreach($fechas_tecnicos_citas as $f => $value){
      $array_fechas[] = $value->fecha_fin.' '.$value->hora_fin;
    }

    //Tecnicos citas historial
    $fechas_tecnicos_citas_historial = $this->db->where('id_operacion',$id_operacion)->where('historial',0)->get('tecnicos_citas_historial')->result();
    foreach($fechas_tecnicos_citas_historial as $f => $value){
      $array_fechas[] = $value->fecha_fin.' '.$value->hora_fin;
    }
    return $this->orderData($array_fechas);
  }
  //Obtener los datos de la operacion mediante su ID
  public function getDataOperacionId($idoperacion=''){
    return $this->db->where('id',$idoperacion)->get('articulos_orden')->row();
  }
  //Obtener el id orden con base a una operacion
  public function getIdOrdenByOperacion($idoperacion=''){
    $q = $this->db->where('id',$idoperacion)
                  ->get('articulos_orden');
    if($q->num_rows()==1){
      return $q->row()->idorden;
    }else{
      return '';
    }              
                    
  }
  public function get_result($campo,$value,$tabla){
      $result = $this->db->where($campo,$value)->get($tabla)->result();
      return $result;
  }
  //Obtener el tipo de cita para el archivo DATA SAMPLE
  public function getTipoCita($id_opcion_cita=''){
    $q = $this->db->where('id',$id_opcion_cita)
                  ->get('cat_opcion_citas');
    if($q->num_rows()==1){
      return $q->row()->codigo;
    }else{
      return '';
    }                              
  }  
  public function getBilledHour($idoperacion='',$principal=0){
      if($principal){
        $tabla = 'tecnicos_citas';
      }else{
        $tabla = 'tecnicos_citas_historial';
        $this->db->where('carryover',0);
      }
      $cita = $this->db->where('id_operacion',$idoperacion)->where('historial',0)->get($tabla)->result();
      
      if(count($cita)==1){
        $fecha_fin_cita = $cita[0]->fecha_fin.' '.$cita[0]->hora_fin;
        $fecha_inicio_cita = $cita[0]->fecha.' '.$cita[0]->hora_inicio_dia;
        $fecha1 = new DateTime($fecha_inicio_cita);//fecha inicial
        $fecha2 = new DateTime($fecha_fin_cita);//fecha de cierre

        $intervalo = $fecha1->diff($fecha2);
        //Cómo es un registro sólo saco la diferencia entre las dos fechas
        return $this->getHour((int)$intervalo->format('%m'),(int)$intervalo->format('%d'),(int)$intervalo->format('%H'),(int)$intervalo->format('%i'));
      }else if(count($cita)==2){
        //Si son dos registros es que uno es día completo y el otro no entonces sólo saco la diferencia de los dos registros como si fuera uno
        $total_horas = 0;
        $fecha1 = new DateTime($cita[0]->fecha.' '.$cita[0]->hora_inicio_dia);
        $fecha2 = new DateTime($cita[0]->fecha_fin.' '.$cita[0]->hora_fin);
        $intervalo = $fecha1->diff($fecha2);

        $total_horas = $total_horas + $this->getHour((int)$intervalo->format('%m'),(int)$intervalo->format('%d'),(int)$intervalo->format('%H'),(int)$intervalo->format('%i'));

        $fecha1 = new DateTime($cita[1]->fecha.' '.$cita[1]->hora_inicio_dia);
        $fecha2 = new DateTime($cita[1]->fecha_fin.' '.$cita[1]->hora_fin);
        $intervalo = $fecha1->diff($fecha2);

        $total_horas = $total_horas + $this->getHour((int)$intervalo->format('%m'),(int)$intervalo->format('%d'),(int)$intervalo->format('%H'),(int)$intervalo->format('%i'));

        return $total_horas;
      }else{
        return '';
      }

    
  }   
  function getHour($meses='',$dias='',$horas='',$minutos='',$lavado=0){
    
    
    $suma_horas = 0;
    if($meses!=''){
      $suma_horas = $suma_horas + ($meses*30*24);
    }
    if($dias!=''){
      $suma_horas = $suma_horas + ($dias*24);
    }
    $suma_horas = $suma_horas+$horas;

    
    if($minutos>=30){
      $suma_horas = $suma_horas+0.5;
    }else{
       $suma_horas = $suma_horas+0.5;
    }


    
    
    return $suma_horas;
  }        
  //Saber el # de envío que sigue para los archivos de ford
  public function versionArchivoFORD(){
    $q = $this->db->where('fecha_envio',date('Y-m-d'))
                  ->select('numero_envio')
                  ->get('bitacora_envio_ford');
    if($q->num_rows()==1){
      return (int)$q->row()->numero_envio+1;
    }else{
      return 1;
    }
  }

  //Actualizar el # de envío uno más
  public function updateNumEnvio(){
    $q = $this->db->where('fecha_envio',date('Y-m-d'))
                  ->select('numero_envio')
                  ->get('bitacora_envio_ford');
    if($q->num_rows()==1){
      $this->db->where('fecha_envio',date('Y-m-d'))->set('numero_envio',(int)$q->row()->numero_envio+1)->set('updated_at',date('Y-m-d H:i:s'))->update('bitacora_envio_ford');
    }else{
      $this->db->set('fecha_envio',date('Y-m-d'))->set('created_at',date('Y-m-d H:i:s'))->set('numero_envio',1)->insert('bitacora_envio_ford');
    }
  }
  //Obtener fecha y hora de promesa
  public function getFechaPromesa($id_cita=''){
    $q = $this->db->where('id_cita',$id_cita)
                  ->get('ordenservicio');
    if($q->num_rows()==1){
      return $q->row()->fecha_entrega.' '.$q->row()->hora_entrega;
    }else{
      return '';
    }
  }
  //Obtener el inicio de lavado 
  public function Actual_Start_Wash($id_cita=''){
     $q = $this->db->where('id_cita',$id_cita)
                   ->where('status_nuevo','Lavando')
                   ->limit(1)
                  ->get('transiciones_estatus');
    if($q->num_rows()==1){
      return $q->row()->fecha_creacion;
    }else{
      return '';
    }
  }
   //Obtener el fin de lavado 
  public function Actual_Finish_Wash($id_cita=''){
     $q = $this->db->where('id_cita',$id_cita)
                   ->where('status_nuevo','Lavado Terminado')
                   ->limit(1)
                   ->order_by('id','desc')
                  ->get('transiciones_estatus');
    if($q->num_rows()==1){
      return $q->row()->fecha_creacion;
    }else{
      return '';
    }
  }
  //Obtener tiempo de lavado por cita y modelo
  public function getTiempoLavadoModelo($modelo=''){
    $q = $this->db->where('modelo',$modelo)
                  ->get('cat_modelo');
    if($q->num_rows()==1){
      return $q->row()->tiempo_lavado;
    }else{
      return 30;
    }
  }
  //Suma total de los minutos de detencion por operacion
  public function getSumaTotalOperacion($id_operacion=''){
    $q = $this->db->select('sum(tiempo_detencion) as suma_minutos')->where('id_status !=',4)->where('id_operacion',$id_operacion)->get('operaciones_detenidas');
    if($q->num_rows()==1){
      return $q->row()->suma_minutos;
    }else{
      return null;
    }
  }
  //Encontrar la razón de la detención del mayor tiempo por operacion
  public function getRazonDetencion($id_operacion=''){
    $q = $this->db->select('motivo_detencion')
                  ->order_by('tiempo_detencion','desc')
                  ->where('id_operacion',$id_operacion)
                  ->where('id_status !=',4)
                  ->limit(1)
                  ->get('operaciones_detenidas');
    if($q->num_rows()==1){
      return $q->row()->motivo_detencion;
    }else{
      return null;
    }
  }

      
  //FIN CONSULTAS

	  
} 