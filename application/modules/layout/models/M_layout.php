<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_layout extends CI_Model
{
  private $_batchImport;
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  public function updateInfo($idorden = '', $id_cita = '', $paquete_id = '')
  {


    if ($_POST['id'] == 0) { //Se está creando la orden
      if ($_POST['id_cita'] != 0) { //Se creó la orden con una cita previa
        $datos_cita = $this->getCitaId($_POST['id_cita']);
        $id_operacion = $this->getPrincipalOperacion($idorden);
        $this->db->where('id', $id_operacion)->set('principal', 1)->set('no_planificado', 0)->set('id_tecnico', $datos_cita->id_tecnico)->set('id_status', 1)->update('articulos_orden');
        //Actualizar el prepiking_extra_cita
        $id_operacion_principal_preventivo = $this->getPrincipalOperacion($idorden, true);
        $this->db->where('id_cita', $_POST['id_cita'])->set('id_operacion', $id_operacion_principal_preventivo)->update('prepiking_extra_cita');
      } else {

        //Obtener el técnico que se guardó
        if ($_POST['tecnico'] != '') {
          $id_tecnico = $_POST['tecnico'];
        } else {
          $id_tecnico = $_POST['tecnico_dias'];
        }
        $id_operacion = $this->getPrincipalOperacion($idorden);

        $this->db->where('idorden', $idorden)->set('principal', 1)->set('no_planificado', 0)->where('id', $id_operacion)->set('id_tecnico', $id_tecnico)->set('id_status', 1)->update('articulos_orden');
        //Actualizar el prepiking_extra_cita
        $id_operacion_principal_preventivo = $this->getPrincipalOperacion($idorden, true);
        $this->db->where('id_cita', $_POST['id_cita'])->set('id_operacion', $id_operacion_principal_preventivo)->update('prepiking_extra_cita');
      }
      //Actualizar en tecnicos_citas cual será la orden principal
      $id_operacion = $this->getPrincipalOperacion($idorden);

      //Actualice las partes de la orden 
      if ($id_operacion_principal_preventivo != '') {
        $this->savePartsPreventivo($id_operacion_principal_preventivo, $id_cita, $paquete_id);
      }
      $this->db->where('id_cita', $id_cita)->set('id_operacion', $id_operacion)->update('tecnicos_citas');
    }else{
      $paquete_id_actual = $this->principal->getGeneric('id_cita', $id_cita, 'ordenservicio', 'paquete_id');
      if($paquete_id_actual!=$paquete_id){
        //Se actualizó el paquete
        //Borrar lo que ya había del otro paquete
        $this->db->where('id_cita',$id_cita)->delete('prepiking_extra_cita');
        //insertar el nuevo paquete
        $id_operacion_principal_preventivo = $this->getPrincipalOperacion($idorden, true);
        if ($id_operacion_principal_preventivo != '') {
          $this->savePartsPreventivo($id_operacion_principal_preventivo, $id_cita, $paquete_id);
        }
      }
     
    }
  }

  //Función que obtiene la operación principal de la orden
  public function getPrincipalOperacion($idorden = '', $soloPreventivo = false)
  {
    //soloPreventivo es para cuando regresa únicamente la de tipo preventivo
    //var_dump($idorden,$soloPreventivo);die();
    $q = $this->db->where('idorden', $idorden)->where('tipo', 5)->limit(1)->get('articulos_orden');
    if ($q->num_rows() == 1) {
      return $q->row()->id;
    } else {
      if (!$soloPreventivo) {
        $q = $this->db->where('idorden', $idorden)->limit(1)->get('articulos_orden');
        if ($q->num_rows() == 1) {
          return $q->row()->id;
        } else {
          return -1;
        }
      } else {
        return -1;
      }
    }
  }
  //Funcion para guardar las partes de preventivo
  public function savePartsPreventivo($id_operacion = '', $id_cita = '', $paquete_id = '')
  {
    if ($id_operacion != '' && $paquete_id != '') {
      $arr_parts = $this->db->where('d.paquete_id', $paquete_id)
        ->join('pkt_articulos a', 'd.articulo_id = a.id')
        ->select('d.*,a.descripcion')
        ->get('pkt_detalle_paquete d')
        ->result();

      foreach ($arr_parts as $p => $parte) {
        $array_partes = array(
          'codigo' => $parte->parte,
          'cantidad' => $parte->cantidad,
          'precio' => $parte->total_refaccion,
          'descripcion' => $parte->descripcion,
          'tiempo' => $parte->tiempo_tabulado,
          'id_cita' => $id_cita,
          'created_at' => date('Y-m-d H:i:s'),
          'id_operacion' => $id_operacion,
          'tipo_preventivo' => 1
        );
        $this->db->insert('prepiking_extra_cita', $array_partes);
      }
    }
  }
  //obtener la cita en base a su id
  public function getCitaId($id = 0)
  {
    return $this->db->where('id_cita', $id)->get('citas')->row();
  }
  public function insertTransicion($estatus_general = 0)
  {

    //Verificar si no existe la transaccion traerme la de creación de la cita
    //Si el estatus actual es 1 es que se va hacer por primera vez el cambio
    if ($_POST['id_status_actual'] == 1) {
      $inicio = $this->getLastTransition($this->input->post('id_cita_save'), '');
    } else {
      $inicio = $this->getLastTransition($this->input->post('id_cita_save'), '', $this->input->post('id_operacion'));
    }
    if ($inicio == '') {
      $inicio = $this->getFechaCreacion($this->input->post('id_cita_save'));
    }
    $idorden = $this->mc->getIdOrdenByCita($this->input->post('id_cita_save'));

    //NUEVA VERSION
    //Obtener datos del estatus
    $id_status_anterior = $this->getIdStatusByNombre($this->getLastStatusTransiciones($this->input->post('id_cita_save'), $this->input->post('id_operacion')));

    $datos_estatus = $this->getStatusId($id_status_anterior);
    //Si el estatus == 3 y ya se terminaron todas las operaciones guardar en la línea de tiempo

    if ($this->input->post('id_status') != 5) {
      if ($this->ml->validarTodasOperacionesTerminadas($idorden)) {
        $this->ml->insertTransicion($estatus_general);
      }
    }


    if ($this->input->post('id_status') == 3) {
      //Seleccionar las operaciones para terminarlas
      $resto_operaciones = $this->db->where('idorden', $idorden)->where('no_planificado', 0)->get('articulos_orden')->result();

      foreach ($resto_operaciones as $r => $val) {
        $datos_transiciones = array(
          'status_anterior' => $this->getLastStatusTransiciones($this->input->post('id_cita_save'), $val->id),
          'status_nuevo' => $this->getStatusTecnicoTransiciones($this->input->post('id_status')),
          'fecha_creacion' => date('Y-m-d H:i:s'),
          'tipo' => 2, //técnico,
          'inicio' => $inicio,
          'fin' => date('Y-m-d H:i:s'),
          'usuario' => $this->getTecnico($this->input->post('usuario_ingreso'), $this->input->post('password_ingreso')),
          'id_cita' => $this->input->post('id_cita_save'),
          'general' => 0,
          'id_operacion' => $val->id,
          'id_tecnico' => $val->id_tecnico,
          'minutos_transcurridos' => dateDiffMinutes($inicio, date('Y-m-d H:i:s'))
        );
        $this->db->insert('transiciones_estatus', $datos_transiciones);
        $this->db->where('id', $val->id)->set('id_status', 3)->update('articulos_orden');
      }
    } else {
      $horarios_cita = $this->getDataHorarioCita();
      $datos_transiciones = array(
        'status_anterior' => $this->getLastStatusTransiciones($this->input->post('id_cita_save'), $this->input->post('id_operacion')),
        'status_nuevo' => $this->getStatusTecnicoTransiciones($this->input->post('id_status')),
        'fecha_creacion' => date('Y-m-d H:i:s'),
        'tipo' => 2, //técnico,
        'inicio' => $inicio,
        'fin' => date('Y-m-d H:i:s'),
        'usuario' => $this->getTecnico($this->input->post('usuario_ingreso'), $this->input->post('password_ingreso')),
        'id_cita' => $this->input->post('id_cita_save'),
        'general' => $estatus_general,
        'id_operacion' => isset($horarios_cita->id_operacion) ? $horarios_cita->id_operacion : '',
        'id_tecnico' => isset($horarios_cita->id_tecnico) ? $horarios_cita->id_tecnico : '',
        'estatus_detenido' => isset($datos_estatus->estatus_detenido) ? $datos_estatus->estatus_detenido : 0,
        'motivo_detencion' => $this->input->post('motivo_detencion'),
        'minutos_transcurridos' => dateDiffMinutes($inicio, date('Y-m-d H:i:s'))
      );
      $this->db->insert('transiciones_estatus', $datos_transiciones);
    }

    //Verificar si el estatus antes del cambio era un estatus de detención para tomar los mínutos
    $estatus_old_detenido = $this->getStatusId($this->input->post('id_status_actual'));
    if (isset($estatus_old_detenido->estatus_detenido)) {
      $estatus_old_detenido = $estatus_old_detenido->estatus_detenido;
    } else {
      $estatus_old_detenido = 0;
    }
    if ($estatus_old_detenido == 1) {
      $operaciones_detenidas = array(
        'id_operacion' => $this->input->post('id_operacion'),
        'id_status' => $this->input->post('id_status_actual'),
        'tiempo_detencion' => $datos_transiciones['minutos_transcurridos'],
        'created_at' => date('Y-m-d H:i:s'),
        'motivo_detencion' => $this->input->post('motivo_detencion'),
      );
      $this->db->insert('operaciones_detenidas', $operaciones_detenidas);
    }
  }
  //Obtener la última transacción
  public function getLastTransition($id_cita = '', $tipo = '', $id_operacion = '')
  {
    if ($tipo != '') {
      $this->db->where('tipo', $tipo);
    }
    if ($id_operacion != '') {
      $this->db->where('id_operacion', $id_operacion);
    }
    $q = $this->db->where('id_cita', $id_cita)->order_by('id', 'desc')->get('transiciones_estatus');
    if ($q->num_rows() == 1) {
      return $q->row()->fin;
    } else if ($q->num_rows() > 1) {
      //Cuando trae dos registros por que ya hay del técnico
      if ($id_operacion != '') {
        $this->db->where('id_operacion', $id_operacion);
      }
      $q2 = $this->db->where('id_cita', $id_cita)->where('tipo', 2)->limit(1)->order_by('id', 'desc')->get('transiciones_estatus');
      if ($q2->num_rows() == 1) {
        return $q2->row()->fin;
      } else {
        return '';
      }
    } else {
      //Ver si hay un registro
      if ($id_operacion != '') {
        $this->db->where('id_operacion', $id_operacion);
      }
      $q = $this->db->where('id_cita', $id_cita)->order_by('id', 'asc')->get('transiciones_estatus');
      if ($q->num_rows() == 1) {
        return $q->row()->fin;
      }
      return '';
    }
  }
  public function getFechaCreacion($id_cita = '')
  {
    $q = $this->db->where('id_cita', $id_cita)->select('fecha_creacion_all')->get('citas');
    if ($q->num_rows() == 1) {
      return $q->row()->fecha_creacion_all;
    } else {
      return '';
    }
  }
  //Obtiene el estatus anterior de las transiciones por cita
  public function getLastStatusTransiciones($id_cita = '', $id_operacion = '')
  {
    $q = $this->db->where('id_cita', $id_cita)->select('status_nuevo')->where('id_operacion', $id_operacion)->order_by('id', 'desc')->limit(1)->get('transiciones_estatus');
    if ($q->num_rows() == 1) {
      return $q->row()->status_nuevo;
    } else {
      $q = $this->db->where('id_cita', $id_cita)->select('status_nuevo')->where('general', '1')->order_by('id', 'desc')->limit(1)->get('transiciones_estatus');
      if ($q->num_rows() == 1) {
        return $q->row()->status_nuevo;
      } else {
        return '';
      }
    }
  }
  //Estatus Técnicos
  public function getStatusTecnicoTransiciones($idstatus = '')
  {
    $q = $this->db->where('id', $idstatus)->select('status')->get('cat_status_citas_tecnicos');
    if ($q->num_rows() == 1) {
      return $q->row()->status;
    } else {
      return '';
    }
  }
  //Obtiene nombre técnico
  public function getTecnico($nombre = '', $password = '')
  {
    $q = $this->db->where('nombre', $nombre)->where('password', md5(sha1($password)))->select('nombre')->get('tecnicos');
    if ($q->num_rows() == 1) {
      return $q->row()->nombre;
    } else {
      return $nombre . ' ' . $password;
    }
  }
  //Obtener los datos de los horarios de la cita
  public function getDataHorarioCita()
  {
    //Puede obtenerla de tecnicos_citas o tecnicos_citas historial
    $q = $this->db->where('id_cita', $this->input->post('id_cita_save'))->where('id_operacion', $_POST['id_operacion'])->limit(1)->get($this->input->post('tabla_operacion'));
    if ($q->num_rows() == 1) {
      return $q->row();
    } else {
      return '';
    }
  }
  //Funcion para saber si existe una orden
  public function existeOrden($id_cita = '')
  {
    $q = $this->db->where('id_cita', $id_cita)->get('ordenservicio');
    if ($q->num_rows() == 1) {
      return true;
    } else {
      return false;
    }
  }
  //Obtener las operaciones sin un técnico
  public function getOperacionesSinTecnico($id_cita = '')
  {
    return $this->db->select('ao.*')
      ->join('ordenservicio o', 'o.id_cita = c.id_cita')
      ->join('articulos_orden ao', 'ao.idorden = o.id')
      ->where('c.id_cita', $id_cita)
      ->where('ao.id_tecnico is null')
      ->where('ao.cancelado', 0)
      ->where('no_planificado', 0)
      ->get('citas c')
      ->result();
  }
  //Obtener las operaciones sin un técnico
  public function getOperacionesByCita($id_cita = '', $no_planificado = false)
  {
    if ($no_planificado) {
      $this->db->where('no_planificado', 0);
    }
    return $this->db->select('ao.*')
      ->join('ordenservicio o', 'o.id_cita = c.id_cita')
      ->join('articulos_orden ao', 'ao.idorden = o.id')
      ->where('c.id_cita', $id_cita)
      ->where('ao.cancelado', 0)
      ->get('citas c')
      ->result();
  }
  //Actualizar operaciones en base a id_cita y al estatus al que se debe actualizar
  public function updateStatusOperaciones($id_cita = '', $id_status = '')
  {
    $idorden = $this->getIdOrdenByCita($id_cita);
    $this->db
      ->where('idorden', $idorden)
      ->set('id_status', $id_status)
      ->update('articulos_orden');
  }
  //obtiene el estatus actual de la tabla de citas
  public function getStatusOrden($idorden = '', $id_operacion = '')
  {
    $q = $this->db->where('idorden', $idorden)->where('id', $id_operacion)->select('id_status')->get('articulos_orden');
    if ($q->num_rows() == 1) {
      return $q->row()->id_status;
    } else {
      return 0;
    }
  }
  //Funcion para saber si existe una orden
  public function getOperacionById($id_operacion = '', $campo = '')
  {
    $q = $this->db->where('id', $id_operacion)->get('articulos_orden');
    if ($q->num_rows() == 1) {
      return $q->row()->$campo;
    } else {
      return '';
    }
  }
  //Funcion para saber el total de operaciones por orden
  public function countOperaciones($idorden = '', $id_status = '')
  {
    if ($id_status != '') {
      $this->db->where('id_status', $id_status);
    }
    $operaciones = $this->db->where('idorden', $idorden)->where('no_planificado', 0)->where('cancelado', 0)->select('count(id) as total')->get('articulos_orden');
    if ($operaciones->num_rows() == 1) {
      return $operaciones->row()->total;
    } else {
      return 0;
    }
  }
  //Funcion para saber si ya están en estatus 3 todas las operaciones
  public function validarTodasOperacionesTerminadas($idorden = '')
  {
    $cantidad_operaciones = $this->countOperaciones($idorden);
    $cantidad_operaciones_terminadas = $this->countOperaciones($idorden, 3);

    if ($cantidad_operaciones == $cantidad_operaciones_terminadas) {
      return true;
    } else {
      return false;
    }
  }
  //Funcion para saber si ya están terminadas todas las operaciones de una orden
  public function compararTodasOperacionesTerminadas($idorden = '')
  {
    $cantidad_operaciones = $this->countOperaciones($idorden);
    $cantidad_operaciones_terminadas = $this->countOperaciones($idorden, 23);
    if ($cantidad_operaciones == $cantidad_operaciones_terminadas) {
      return true;
    } else {
      return false;
    }
  }
  //Funcion para saber si ya fueron asignadas todas las operaciones
  public function validarTodasOperacionesAsignadas($idorden = '')
  {
    $cantidad_operaciones = $this->countOperaciones($idorden);
    $cantidad_operaciones_asignadas = $this->countOperacionesAsignadas($idorden, 3);
    if ($cantidad_operaciones == $cantidad_operaciones_asignadas) {
      return true;
    } else {
      return false;
    }
  }
  //Funcion para saber el total de operaciones por orden
  public function countOperacionesAsignadas($idorden = '', $id_status = '')
  {

    $operaciones = $this->db->where('id_tecnico is not null')->where('idorden', $idorden)->where('no_planificado', 0)->where('cancelado', 0)->select('count(id) as total')->get('articulos_orden');
    if ($operaciones->num_rows() == 1) {
      return $operaciones->row()->total;
    } else {
      return 0;
    }
  }
  //Funcion para saber si una operación es la última de la línea, es decir si hay 3 y dos ya están terminadas,
  //esa operación detonaría el terminado
  public function validarUltimaOperacion($idorden = '')
  {
    $cantidad_operaciones = $this->countOperaciones($idorden); //3
    $cantidad_operaciones_terminadas = $this->countOperaciones($idorden, 23); //"
    if ($cantidad_operaciones - 1 == $cantidad_operaciones_terminadas) {
      return true;
    } else {
      return false;
    }
  }
  //Saber si es un estatus de la cita o de la operacion
  public function getTipoStatus($idstatus = '')
  {
    $q = $this->db->select('estatus_general')->where('id', $idstatus)->get('cat_status_citas_tecnicos');
    if ($q->num_rows() == 1) {
      return $q->row()->estatus_general;
    } else {
      return 1;
    }
  }
  public function getIdOrdenByCita($id_cita = '')
  {
    $q = $this->db->where('id_cita', $id_cita)->limit(1)->select('id')->from('ordenservicio')->get();
    if ($q->num_rows() == 1) {
      return $q->row()->id;
    } else {
      return '';
    }
  }
  //Saber si es un estatus de la cita o de la operacion
  public function getOperacionName($idoperacion = '')
  {
    $q = $this->db->select('descripcion')->where('id', $idoperacion)->get('articulos_orden');
    if ($q->num_rows() == 1) {
      return $q->row()->descripcion;
    } else {
      return 1;
    }
  }
  //Que no pueda poner otro trabajando si ya existe uno
  public function ValidarUnTrabajoTecnicoOperacion($id_tecnico = 0, $id_operacion = '')
  {
    if ($id_tecnico == '') {
      $id_tecnico = 0;
    }
    $query_tecnicos_citas = " 
    select c.id_cita,a.id_status, a.id as idoperacion, a.descripcion as operacion, c.id_tecnico
    from articulos_orden a
    join ordenservicio o on a.idorden = o.id
    join tecnicos_citas c on a.id = c.id_operacion
    where a.id_tecnico = " . $id_tecnico . "
    and c.id_tecnico = " . $id_tecnico . "
    and a.cancelado = 0
    and c.historial = 0
    and a.no_planificado = 0
    and a.id_status not in (3,4,5,6,7,1,11,12,9,13,14,15,16,17,18,19,20,21,22,23) 
    and c.id_cita > " . CONST_NO_CITA_NEW_UPDT . "
    and a.id !=" . $id_operacion;

    $query_tecnicos_citas_historial = " 
    select c.id_cita,a.id_status, a.id as idoperacion, a.descripcion as operacion, c.id_tecnico
    from articulos_orden a
    join ordenservicio o on a.idorden = o.id
    join tecnicos_citas_historial c on a.id = c.id_operacion
    where a.id_tecnico = " . $id_tecnico . "
    and c.id_tecnico = " . $id_tecnico . "
    and a.cancelado = 0
    and c.historial = 0
    and a.no_planificado = 0
    and carryover = 0
    and a.id_status not in (3,4,5,6,7,1,11,12,9,13,14,15,16,17,18,19,20,21,22,23) 
    and c.id_cita > " . CONST_NO_CITA_NEW_UPDT . "
    and a.id !=" . $id_operacion;

    $datos_tecnicos_citas = $this->db->query($query_tecnicos_citas)->result();
    $datos_tecnicos_citas_historial = $this->db->query($query_tecnicos_citas_historial)->result();
    if (count($datos_tecnicos_citas) > 0 || count($datos_tecnicos_citas_historial) > 0) {
      return false;
    } else {
      return true;
    }
  }
  //Validar si una unidad tiene diagnóstico
  public function existeDiagnostico($id_cita = '')
  {
    $q = $this->db->where('noServicio', $id_cita)->get('diagnostico');
    if ($q->num_rows() > 0) {
      return true;
    } else {
      return false;
    }
  }
  //Validar si pasó la unidad por trabajando
  public function unidadTrabajando($idcita = '', $id_operacion = '')
  {
    $query = "select * from transiciones_estatus where id_operacion = " . $id_operacion . " and id_cita = " . $idcita . " and (status_nuevo = 'trabajando' or status_anterior = 'trabajando')";
    $result = $this->db->query($query)->result();
    if (count($result) > 0) {
      return true;
    } else {
      return false;
    }
  }
  //Obtener información del estatus
  //Validar si pasó la unidad por trabajando
  public function getStatusId($id = '')
  {
    $q = $this->db->where('id', $id)->get('cat_status_citas_tecnicos');
    return $q->row();
  }
  //Obtiene el Id de todas las operaciones de las transiciones
  public function getIdTransicionByCita($id_cita = '')
  {
    return $this->db->select('id_operacion')
      ->where('id_cita', $id_cita)
      ->where('id_operacion !=', 0)
      ->group_by('id_operacion')
      ->get('transiciones_estatus')
      ->result();
  }
  //NUEVA VERSION
  public function getTransiciones($id_cita = '', $id_operacion = '')
  {
    return $this->db->where('id_cita', $id_cita)->where('id_operacion', $id_operacion)->where('mostrar_linea', 1)->get('transiciones_estatus')->result();
  }
  //function que obtiene el técnico de la cita
  public function getTecnicoByOperacion($id_cita = '', $id_operacion = '')
  {
    $q = $this->db->where('id_cita', $id_cita)->where('id_operacion', $id_operacion)->select('id_tecnico')->get('transiciones_estatus');
    if ($q->num_rows() == 1) {
      return $q->row()->id_tecnico;
    } else {
      return 0;
    }
  }
  public function getNameTecnico($id = 0)
  {
    $q = $this->db->where('id', $id)->select('nombre')->from('tecnicos')->get();
    if ($q->num_rows() == 1) {
      return $q->row()->nombre;
    } else {
      return '';
    }
  }
  //tiempo de la cita
  public function getTimeCita($id_cita = '')
  {
    $q = $this->db->where('id_cita', $id_cita)->get('tecnicos_citas_historial');
    if ($q->num_rows() == 1) {
      //NUEVA VERSION
      $fecha1 = new DateTime($q->row()->fecha . ' ' . $q->row()->hora_inicio_dia); //fecha inicial
      $fecha2 = new DateTime($q->row()->fecha_fin . ' ' . $q->row()->hora_fin); //fecha de cierre
      //var_dump($fecha1,$fecha2);
      $intervalo = $fecha1->diff($fecha2);
      //var_dump($intervalo->format('%d día(s) %H hora(s) %i minuto(s)'));die();
      return $intervalo->format('%d día(s) %H hora(s) %i minuto(s)');
    } else {
      return '';
    }
  }
  public function getIdHorarioActual($id_cita = '')
  {
    $q = $this->db->where('id_cita', $id_cita)->where('activo', 1)->select('id_horario')->get('citas');
    if ($q->num_rows() == 1) {
      return $q->row()->id_horario;
    } else {
      return '';
    }
  }
  //Obtiene el asesor de la cita
  public function getIdOperador($idhorario = '')
  {
    $q = $this->db->where('id', $idhorario)->select('id_operador')->get('aux');
    if ($q->num_rows() == 1) {
      return $q->row()->id_operador;
    } else {
      return '';
    }
  }
  //Obtiene usuario de plani4
  public function getNameAsesor($iduser = '')
  {
    $q = $this->db->where('id', $iduser)->select('nombre')->get('operadores');
    if ($q->num_rows() == 1) {
      return $q->row()->nombre;
    } else {
      return '';
    }
  }
  public function getHora($id_horario = '')
  {
    $q = $this->db->where('id', $id_horario)->select('hora')->get('aux');
    if ($q->num_rows() == 1) {
      return $q->row()->hora;
    } else {
      return '';
    }
  }
  //Obtiene datos de la cita de la tabla tecnicos_ cita
  public function getDataTecnicosCitas($id_cita = '', $id_operacion = '')
  {
    if ($id_operacion != '') {
      $this->db->where('id_operacion', $id_operacion);
    }
    return $this->db->where('id_cita', $id_cita)->get('tecnicos_citas_historial')->result();
  }
  public function hora_promesa($id_cita = '')
  {
    $q = $this->db->where('id_cita', $id_cita)->select('fecha_entrega,hora_entrega')->from('ordenservicio')->get();
    if ($q->num_rows() == 1) {
      return date_eng2esp_1($q->row()->fecha_entrega) . ' ' . $q->row()->hora_entrega;
    } else {
      return '';
    }
  }
  public function toHours($min, $type = '')
  { //obtener segundos
    $sec = $min * 60;
    //dias es la division de n segs entre 86400 segundos que representa un dia
    $dias = floor($sec / 86400);
    //mod_hora es el sobrante, en horas, de la division de días;
    $mod_hora = $sec % 86400;
    //hora es la division entre el sobrante de horas y 3600 segundos que representa una hora;
    $horas = floor($mod_hora / 3600);
    //mod_minuto es el sobrante, en minutos, de la division de horas;
    $mod_minuto = $mod_hora % 3600;
    //minuto es la division entre el sobrante y 60 segundos que representa un minuto;
    $minutos = floor($mod_minuto / 60);
    if ($horas <= 0) {
      $text = $minutos . ' min';
    } elseif ($dias <= 0) {
      if ($type == 'round')
      //nos apoyamos de la variable type para especificar si se muestra solo las horas
      {
        $text = $horas . ' hrs';
      } else {
        $text = $horas . " hrs " . $minutos;
      }
    } else {
      //nos apoyamos de la variable type para especificar si se muestra solo los dias
      if ($type == 'round') {
        $text = $dias . ' dias';
      } else {
        $text = $dias . " dias " . $horas . " hrs " . $minutos . " min";
      }
    }
    return $text;
  }
  //Obtiene el asesor de la cita
  public function getIdStatusByNombre($status = '')
  {
    $q = $this->db->where('status', $status)->select('id')->get('cat_status_citas_tecnicos');
    if ($q->num_rows() == 1) {
      return $q->row()->id;
    } else {
      return '';
    }
  }
  //Función para regresar si una operación es carryover o no
  public function isCarryover($idoperacion = '')
  {
    $q = $this->db->where('id', $idoperacion)->where('id_tecnico is not null')->limit(1)->get('articulos_orden');
    if ($q->num_rows() == 1) {
      return 1;
    } else {
      return 0;
    }
  }
  //Validar si ya fue asignado un técnico a una operacion
  //Saber si es un estatus de la cita o de la operacion
  public function validarTecnicoOperacion($idoperacion = '')
  {
    $q = $this->db->select('id_tecnico')->where('id', $idoperacion)->get('articulos_orden');
    if ($q->num_rows() == 1) {
      if ($q->row()->id_tecnico != '') {
        return true;
      }
    }
    return false;
  }
  //obtener el motivo de detención 
  public function getMotivoDetencion($status = '', $id_operacion = '')
  {
    $q = $this->db->where('status_nuevo', $status)->where('id_operacion', $id_operacion)->select('motivo_detencion')->limit(1)->order_by('id', 'desc')->get('transiciones_estatus');
    if ($q->num_rows() == 1) {
      return $q->row()->motivo_detencion;
    } else {
      return '';
    }
  }
  //Estatus actual de la operación
  public function getStatusActualOp($id_operacion = '')
  {
    return $this->principal->getGeneric('id', $id_operacion, 'articulos_orden', 'id_status');
  }
  public function esEstatusPermitidoCarryOver($id_operacion = '')
  {
    $id_status_actual_op = $this->getStatusActualOp($id_operacion);
    if ($id_status_actual_op != '') {
      return $this->principal->getGeneric('id', $id_status_actual_op, 'cat_status_citas_tecnicos', 'estatus_permitido_carryover');
    } else {
      return 1;
    }
  }
  //Estatus actual de la operación
  public function getCarryOverReservacion($idcarryover = '')
  {
    return $this->principal->getGeneric('id', $idcarryover, 'tecnicos_citas_historial', 'carryover_reservacion');
  }
  //Validar que no existan operaciones extras aprobadas por el cliente sin asignar
  public function validarExisteOperacionesExtras($id_cita = '')
  {
    $data = $this->db->where('id_cita', $id_cita)->where('autoriza_cliente', 1)->where('activo', 1)->where('id_operacion is null')->where('pza_garantia', 0)->get('presupuesto_multipunto')->result();
    if (count($data) > 0) {
      return true;
    }
    return false;
  }
  //validar que todas las operaciones de una orden ya fueron asignadas
  public function OperacionesAsignadas($idorden = '')
  {
    $operaciones = $this->db->where('idorden', $idorden)->where('cancelado', 0)->where('no_planificado', 0)->where('id_tecnico is null')->get('articulos_orden')->result();
    if (count($operaciones) > 0) {
      return false;
    } else {
      return true;
    }
  }
  public function getCitaAsignadaId($id_tecnico = '', $id_operacion = '', $dia_completo = '', $tabla = 'tecnicos_citas')
  {
    if ($dia_completo != '') {
      $this->db->where('dia_completo', $dia_completo);
    }
    return $this->db->where('id_operacion', $id_operacion)->where('id_tecnico', $id_tecnico)->get($tabla)->result();
  }
  public function getCitaAsignadaId2($id_tecnico = '', $id_operacion = '', $dia_completo = '', $tabla = 'tecnicos_citas')
  {

    if ($dia_completo == 0) {
      $this->db->where('dia_completo', $dia_completo);
    }
    return $this->db->where('id_operacion', $id_operacion)->where('id_tecnico', $id_tecnico)->get($tabla)->result();
  }
  //saber si en la cita hay días completos
  public function getDiaCompleto($id_operacion = '', $tabla = 'tecnicos_citas')
  {
    $q = $this->db->where('id_operacion', $id_operacion)->where('dia_completo', 1)->select('dia_completo')->get($tabla);
    if ($q->num_rows() == 1) {
      return 1;
    } else {
      return 0;
    }
  }
  //Obtiene la fecha de tecnicos citas
  public function getFechaTecnicosCita($id_operacion = '', $tabla = '')
  {
    $q = $this->db->where('id_operacion', $id_operacion)->select('fecha')->get($tabla);
    if ($q->num_rows() == 1) {
      return date_eng2esp_1($q->row()->fecha);
    } else {
      return '';
    }
  }
  public function existeCotizacion($id_cita = '')
  {
    $q = $this->db->where('id_cita', $id_cita)
      ->select('acepta_cliente')
      ->limit(1)
      ->get('presupuesto_registro');
    if ($q->num_rows() == 0) {
      return false;
    } else {
      if ($q->row()->acepta_cliente == '') {
        return true;
      } else {
        return false;
      }
    }
  }
  //Estatus actual de la operación
  public function getSituacionOrden($id_cita = '')
  {
    return $this->principal->getGeneric('id_cita', $id_cita, 'ordenservicio', 'id_situacion_intelisis');
  }
  //Saber si se puede terminar en días previos
  public function TerminarEstatusDiasPrevios($id_status = '')
  {
    return $this->principal->getGeneric('id', $id_status, 'cat_status_citas_tecnicos', 'terminar_dias_previos');
  }
  public function cotizacionRechazada($id_cita = '')
  {
    $q = $this->db->where('id_cita', $id_cita)
      ->select('acepta_cliente')
      ->limit(1)
      ->get('presupuesto_registro');
    if ($q->num_rows() == 0) {
      return false;
    } else {
      if ($q->row()->acepta_cliente == 'No') {
        return true;
      } else {
        return false;
      }
    }
  }
  //Saber si una operación ya pasó por trabajando
  public function OperacionTrabajando($id_operacion = '')
  {
    $q = $this->db->where('id_operacion', $id_operacion)
      ->where('status_nuevo', 'trabajando')
      ->limit(1)
      ->get('transiciones_estatus');
    if ($q->num_rows() == 1) {
      return true;
    } else {
      return false;
    }
  }
}
