<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Apintegracion extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('citas/m_catalogos', 'mcat');
        $this->load->model('principal');
        $this->load->helper('general');
        $this->load->model('m_integracion', 'mi');
        if (date('H:i') >= '23:30' || date('H:i') <= '07:30') {
            exit();
        }
        //exit();
    }
    //Crear tabla RO_Data_Sample
    public function createRoDataSample()
    {
        //$this->db->where('c.id_cita',43938);

        $informacion_citas = $this->mi->getCitaOrdenFord();
        $array_ordenes = array(-1); //Este array lo creo para después crear el dataService con base a esas ordenes
        //Actualizar las ordenes sin id_horario a cita_previa = 0
        $this->db->where('id_horario is null')->where('cita_previa', 1)->where('id_cita >=', CONST_NO_CITA_NEW_UPDT)->set('cita_previa', 0)->update('citas');
        foreach ($informacion_citas as $i => $value) {
            $array_ordenes[] = $value->idorden;
            $check_in_cita = $this->mi->getCheckIn($value->id_cita);
            if ($check_in_cita == '') {
                $check_in_cita = $value->fecha_recepcion . ' ' . $value->hora_recepcion;
            }
            //Fecha de la cita
            $fecha_cita = $this->mi->getFechaCita($value->id_horario);
            if ($fecha_cita == '') {
                $fecha_cita = $value->fecha_recepcion . ' ' . $value->hora_recepcion;
            }
            //Sin cita
            if (!$value->cita_previa) {
                $Appointment_ID = '';
                $Date_Appointment_Initiated = null;
                $Appointment_Date = null;
                $Appointment_Type = '';
            } else {
                $Appointment_ID = $value->id_cita;
                $Date_Appointment_Initiated = $value->fecha_creacion_all;
                $Appointment_Date = $fecha_cita;
                $Appointment_Type = $this->mi->getTipoCita($value->id_opcion_cita);
            }

            //Obtener inicio y fin del diagostico y orden

            $data_orden = $this->principal->getGeneric('id_cita', $value->id_cita, 'ordenservicio');
            $inicio_orden = $data_orden->fecha_recepcion_orden . ' ' . $data_orden->hora_recepcion_orden;
            $fin_orden = $data_orden->created_at;


            $data_diagnostico = $this->principal->getGeneric('noServicio', $value->id_cita, 'diagnostico');
            if($data_diagnostico){
                $inicio_formulario_diagnostico = $data_diagnostico->inicio_formulario;
                $fin_formulario_diagnostico = $data_diagnostico->fechaRegistro;
            }else{
                $inicio_formulario_diagnostico = '';
                $fin_formulario_diagnostico = '';
            }

            $RO_Open_Date  = $value->fecha_recepcion . ' ' . $value->hora_recepcion;
            $RO_Closed_Date  = $value->cierre_orden;

            if (str_replace(' ', '', $inicio_orden) != '' && $inicio_formulario_diagnostico != '') {
                //Tomar de inicio el primero que se abrio entre la orden o el diagnóstico
                $RO_Open_Date  = $inicio_formulario_diagnostico;
                if ($inicio_orden <= $inicio_formulario_diagnostico) {
                    $RO_Open_Date  = $inicio_orden;
                }
            }
            if($RO_Open_Date < $check_in_cita){
                $RO_Open_Date  = $fin_orden;
            }
            $array_cita =  array(
                'id_cita' => $value->id_cita,
                'BID' => CONST_BID,
                'RO_Number' => $value->id_cita,
                'RO_Open_Date' => $RO_Open_Date,
                'RO_Closed_Date' => $RO_Closed_Date,
                'Odometer_Reading' => $value->vehiculo_kilometraje,
                'Odometer_Indicator' => 'K',
                'VIN' => $value->vehiculo_numero_serie,
                'Vehicle_Model' => $value->vehiculo_modelo,
                'Vehicle_Year' => $value->vehiculo_anio,
                'Service_Advisor_STARS_ID' => $this->mi->getIdStarAsesorNombre($value->asesor),
                'Vehicle_Check_In_Date' => $check_in_cita,
                'Vehicle_Check_Out_Date' => $value->fecha_entrega_unidad,
                'Promised_Date' => $value->fecha_entrega . ' ' . $value->hora_entrega,
                'RO_Invoice_Date' => $value->fecha_factura,
                'Signed_Documents' => $this->mi->getFirmaClienteOP2($value->id_cita),
                'Job_Card_Created_Date' => $this->mi->JobCardCreatedDate($value->id_cita),
                'Planned_Job_Start' => $this->mi->Planned_Job_Start($value->id_cita),
                'Planned_Job_Finish' => $this->mi->Planned_Job_Finish($value->id_cita),
                'Actual_Job_Start' => $this->mi->Actual_Job_Start($value->id_cita),
                'Actual_Job_Finish' => $this->mi->Actual_Job_Finish($value->id_cita),
                'Job_Completion_Date' => $value->JobCompletionDate,
                'Appointment_ID' => $Appointment_ID,
                'Date_Appointment_Initiated' => $Date_Appointment_Initiated,
                'Appointment_Date' => $Appointment_Date,
                'Appointment_Type' => $Appointment_Type, //Falta ver catálogo
            );

            if ($this->mi->existeRO_Data_Sample($value->id_cita)) {
                $array_cita['updated_at'] = date('Y-m-d H:i:s');
                $this->db->where('id_cita', $value->id_cita)->update('RO_Data_Sample', $array_cita);
            } else {
                $array_cita['created_at'] = date('Y-m-d H:i:s');
                $this->db->insert('RO_Data_Sample', $array_cita);
            }
            //Si la situación es cerrada se debe actualizar enviado ford a 1
            if ($value->unidad_entregada || $value->cancelada == 1) {
                $this->db->where('id_cita', $value->id_cita)->set('generado_ford', 1)->update('ordenservicio');
            }
        }

        //Crear los servicios 
        $this->createDataServices($array_ordenes);
    }
    //Crear tabla RO_Data_Services
    public function createDataServices($array_ordenes = array())
    {
        //->where('date(created_at)',date('Y-m-d'))
        //->where('generado_ford',0)
        //$this->db->where_in('tipo',array(4,5));
        $articulos_orden = $this->db->where_in('idorden', $array_ordenes, false)->where('id_status !=', 4)->order_by('idorden')->get('articulos_orden')->result();
        $id_orden = 0;
        $secuencia = '';
        $id_orden_anterior = '';
        $id_cita_anterior = '';
        $RO_Number_Service_Anterior = '';
        $secuencia_anterior = 0;
        $contador = 0;
        $array_articulos_orden = array(-1);
        $array_citas = array(-1);
        $array_operaciones = array(); //Este array lo creo para después crear el RO_Data_Parts con base a esos articulos
        foreach ($articulos_orden as $a => $articulo) {
            $id_cita = $this->mi->getIdCitaByOrden($articulo->idorden);

            $array_citas[] = $id_cita;
            $array_articulos_orden[] = $articulo->id;
            $id_orden = $articulo->idorden;
            if ($id_orden == $articulo->idorden) {
                if ($id_orden_anterior == $id_orden) {
                    $secuencia++;
                } else {
                    $secuencia = 1;
                }
            }

            $RO_Number_Service = $this->principal->getGeneric('id', $articulo->idorden, 'ordenservicio', 'id_cita');
            if ($id_cita_anterior != $id_cita) {
                $this->insertOpLavado($id_cita_anterior, $secuencia_anterior, $RO_Number_Service_Anterior);
            }

            if ($articulo->no_planificado == 1) {
                $line_status = 'N';
            } else {
                if ($articulo->cancelado) {
                    $line_status = 'C';
                } else {
                    $line_status = $this->mi->Line_Status($articulo->id);
                }
            }

            if ($line_status != 'N') {
                $Planned_Start = $this->mi->PlannedStart($articulo->id, $articulo->principal);
                $Planned_Finish = $this->mi->PlannedFinish($articulo->id);
                $Actual_Start = $this->mi->ActualJob($articulo->id);
                $Actual_Finish = $this->mi->FinishJob($articulo->id);
                $Billed_Hours = $this->mi->getBilledHour($articulo->id, $articulo->principal);

                $Technician = $this->mi->getIdStarTecnicoByOperacion($articulo->id);
            } else {
                $Planned_Start = null;
                $Planned_Finish = null;
                $Actual_Start = null;
                $Actual_Finish = null;
                $Billed_Hours = null;
                $Pause_Duration = null;
                $Technician = null;
            }

            $Pause_Reason = null;

            if ($line_status == 'D') {
                $Pause_Reason =  $articulo->motivo_detencion;
            } else {
                $Pause_Reason = $this->mi->getRazonDetencion($articulo->id);
            }

            $Pause_Duration = $this->mi->getSumaTotalOperacion($articulo->id);

            if ($line_status == 'D') {
                $Pause_Duration = dateDiffMinutes($articulo->fecha_detencion , date('Y-m-d H:i:s')) + $Pause_Duration;
            }
            //Si el tipo es 4(Correctivo) o 3 Genérico se tomará el campo grupo
            if ($articulo->tipo == 5) {
                $Labor_Opcode = $articulo->descripcion;
            } else {
                $Labor_Opcode = $articulo->grupo;
            }


            $array_info_insert = array(
                'BID' => CONST_BID,
                'RO_Number' => $RO_Number_Service, //$id_cita,
                'idarticulo' => $articulo->id,
                'id_cita' => $id_cita,
                'Sequence_Number' => $secuencia,
                'RO_Type_Code' => $this->mi->RO_Type_Code($articulo->idorden),
                'Servicing_Department_Code' => $this->mi->getCodigoDepartamento($id_cita),
                'Labor_Opcode' => $Labor_Opcode,
                'Planned_Start' => $Planned_Start,
                'Planned_Finish' => $Planned_Finish,
                'Actual_Start' => $Actual_Start,
                'Actual_Finish' => $Actual_Finish,
                'Pause_Duration' => $Pause_Duration,
                'Pause_Reason' => $Pause_Reason,
                'Billed_Hours' => $Billed_Hours,
                'Line_Status' => $line_status,
                'Technician' => $Technician,
                'cotizacion_extra' => $articulo->cotizacion_extra
            );

            if ($this->mi->existeRO_Data_Services($articulo->id)) {
                $array_info_insert['updated_at'] = date('Y-m-d H:i:s');
                $this->db->where('idarticulo', $articulo->id)->update('RO_Data_Services', $array_info_insert);
            } else {
                $array_info_insert['created_at'] = date('Y-m-d H:i:s');

                $this->db->insert('RO_Data_Services', $array_info_insert);
            }
            $id_orden_anterior = $articulo->idorden;

            $id_cita_anterior = $id_cita;
            $RO_Number_Service_Anterior = $RO_Number_Service;
            $secuencia_anterior = $secuencia;
        }
        $this->createDataParts($array_articulos_orden, $array_citas);
    }
    //Crear tabla RO_Data_Parts
    public function createDataParts($array_articulos_orden = array(), $array_citas = array())
    {

        $partes = $this->db->where_in('idarticulo', $array_articulos_orden, false)->get('RO_Data_Services')->result();

        //debug_var($partes);die();


        foreach ($partes as $a => $parte) {
            //Si no es una cotización extra buscar en prepiking
            if (!$parte->cotizacion_extra) {
                $parts = [];
                $lista_partes = $this->db->where('id_operacion', $parte->idarticulo)->get('prepiking_extra_cita')->result();
                foreach ($lista_partes as $l => $part) {
                    $parts[$part->codigo] = $part;
                }
                //debug_var($parts);
                foreach ($parts as $l => $lista) {
                    $RO_Data_Parts = array(
                        'BID' => CONST_BID,
                        'RO_Number' => $parte->RO_Number,
                        'Sequence_Number' => $parte->Sequence_Number, //$secuencia_orden,//$parte->id_operacion,
                        'Part_Prefix' => '',
                        'Part_Base' => $lista->codigo,
                        'Part_Suffix' => '',
                        'Part_Quantity' => $lista->cantidad,
                        'Part_Description' => $lista->descripcion,
                        'idoperacion' => $lista->id_operacion,
                        'id_registro' => $lista->id,
                        'cotizacion_extra' => 0
                    );

                    if ($this->mi->existeRO_Data_Parts($parte->idarticulo, $lista->id, 0)) {
                        $RO_Data_Parts['updated_at'] = date('Y-m-d H:i:s');
                        $this->db->where('idoperacion', $parte->idarticulo)->where('id_registro', $lista->id)->where('cotizacion_extra', 0)->update('RO_Data_Parts', $RO_Data_Parts);
                    } else {
                        $RO_Data_Parts['created_at'] = date('Y-m-d H:i:s');
                        $this->db->insert('RO_Data_Parts', $RO_Data_Parts);
                    }
                }
            } else {
                $lista_partes = $this->db->where('id_operacion', $parte->idarticulo)->get('presupuesto_multipunto')->result();
                $parts = [];
                foreach ($lista_partes as $l => $part) {
                    $parts[$part->codigo] = $part;
                }
                foreach ($parts as $l => $lista) {
                    $RO_Data_Parts = array(
                        'BID' => CONST_BID,
                        'RO_Number' => $parte->RO_Number,
                        'Sequence_Number' => $parte->Sequence_Number, //$secuencia_orden,//$parte->id_operacion,
                        'Part_Prefix' => '',
                        'Part_Base' => $lista->num_pieza,
                        'Part_Suffix' => '',
                        'Part_Quantity' => $lista->cantidad,
                        'Part_Description' => $lista->descripcion,
                        'idoperacion' => $lista->id_operacion,
                        'id_registro' => $lista->id,
                        'cotizacion_extra' => 1
                    );
                    if ($this->mi->existeRO_Data_Parts($parte->idarticulo, $lista->id, 1)) {
                        $RO_Data_Parts['updated_at'] = date('Y-m-d H:i:s');
                        $this->db->where('idoperacion', $parte->idarticulo)->where('id_registro', $lista->id)->where('cotizacion_extra', 1)->update('RO_Data_Parts', $RO_Data_Parts);
                    } else {
                        $RO_Data_Parts['created_at'] = date('Y-m-d H:i:s');
                        $this->db->insert('RO_Data_Parts', $RO_Data_Parts);
                    }
                }
            }
        }
    }
    public function insertOpLavado($id_cita = '', $secuencia = 0, $RO_Number_Service = '')
    {

        $q = $this->db->where('l.id_cita', $id_cita)->join('citas c', 'l.id_cita=c.id_cita')->select('c.vehiculo_modelo,c.no_aplica_lavado,l.*')->get('lavado l');


        $Billed_Hours_Wash = 0;
        if ($q->num_rows() == 1) {
            if ($q->row()->no_aplica_lavado == 0) {
                $info = $q->row();
                $Actual_Start_Wash = $this->mi->Actual_Start_Wash($id_cita);
                $Actual_Finish_Wash = $this->mi->Actual_Finish_Wash($id_cita);

                if ($Actual_Start_Wash != '' && $Actual_Finish_Wash != '') {

                    $fecha1 = new DateTime($Actual_Start_Wash);
                    $fecha2 = new DateTime($Actual_Finish_Wash);
                    $intervalo = $fecha1->diff($fecha2);
                    //var_dump($intervalo);
                    $Billed_Hours_Wash = $Billed_Hours_Wash + $this->mi->getHour((int)$intervalo->format('%m'), (int)$intervalo->format('%d'), (int)$intervalo->format('%H'), (int)$intervalo->format('%i'), 1);
                    //echo $Billed_Hours_Wash.'<br>';

                } else {
                    $Billed_Hours_Wash = '';
                }


                $tiempo_lavado = $this->mi->getTiempoLavadoModelo($info->vehiculo_modelo);

                $time = strtotime($info->horario . ' ' . $info->hora);
                $time = $time + ($tiempo_lavado * 60);
                $fecha_proximo_terminar = date("Y-m-d H:i:s", $time);


                if ($Actual_Finish_Wash != '') {
                    $Line_Status_Wash = 'F';
                } else if (date('Y-m-d H:i:s') > $fecha_proximo_terminar) {
                    $Line_Status_Wash = 'R';
                } else {
                    $Line_Status_Wash = 'T';
                }


                $array_info_insert_lavado = array(
                    'BID' => CONST_BID,
                    'RO_Number' => $RO_Number_Service, //$id_cita,
                    'idarticulo' => '',
                    'id_cita' => $id_cita,
                    'Sequence_Number' => (int)$secuencia + 1,
                    'RO_Type_Code' => 'C',
                    'Servicing_Department_Code' => 'S',
                    'Labor_Opcode' => 'LAVADO',
                    'Planned_Start' => $info->horario . ' ' . $info->hora,
                    'Planned_Finish' => $fecha_proximo_terminar,
                    'Actual_Start' => $Actual_Start_Wash,
                    'Actual_Finish' => $Actual_Finish_Wash,
                    'Billed_Hours' => $Billed_Hours_Wash,
                    'Line_Status' => $Line_Status_Wash,
                    'Technician' => $this->mi->getIdStarLavador($info->id_lavador),
                    'lavado' => 1,
                );
                if ($this->mi->existeRO_Data_Services_Lavado($RO_Number_Service)) {
                    $array_info_insert_lavado['updated_at'] = date('Y-m-d H:i:s');
                    $this->db->where('RO_Number', $RO_Number_Service)->where('lavado', 1)->update('RO_Data_Services', $array_info_insert_lavado);
                } else {
                    $array_info_insert_lavado['created_at'] = date('Y-m-d H:i:s');
                    $this->db->insert('RO_Data_Services', $array_info_insert_lavado);
                }
            } // No aplica lavado
        }
    }
    public function prueba_archivo()
    {
        $numero_envio_ford = $this->mi->versionArchivoFORD();
        try {
            $renglon = "";
            $bid = CONST_BID;
            $nombreExt = $bid . "_RODataSample_" . date("Ymd") . "_" . "00000" . ".csv";
            //$urlNombre = "assets/ordenesFord/".$nombreExt;
            $urlNombre = CONST_URL_SAVE_FILE . date('Ymd');

            if (!file_exists($urlNombre)) {
                mkdir($urlNombre, 0777, true);
            }

            $urlNombre = $urlNombre . '/' . $nombreExt;

            // Abrir el archivo, creándolo si no existe
            $archivo = fopen($urlNombre, "wb");

            if ($archivo == false) {
                echo "Error al crear el archivo: " . $nombreExt;
            } else {
                fwrite($archivo, 'HDR|RODataSample|' . date("Ymd"));
                $renglon .= 'PRUEBA' . "|";
            }

            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($archivo);

            //Cerramos el archivo
            fclose($archivo);

            nl2br(file_get_contents($urlNombre));
        } catch (\Exception $e) {
            echo "No se pudo generar el archivo " . $nombreExt;
            echo "\n";
        }
        //Si se creo el archivo, imprimimos la liga
        if (file_exists($urlNombre)) {
            //echo "Archivo correspondiente a la tabla RO Data Sample : <br>";
            $this->subirOxlo($urlNombre, $nombreExt);
            //echo "<a href='".base_url()."".$urlNombre."'>VER ARCHIVO</a><br><br>";
        }
    }
    public function subirOxlo($urlNombre = '', $nombre_archivo = '')
    {

        $ch = curl_init();
        $fp = fopen($urlNombre, 'r');
        $localfile = $fp;
        curl_setopt($ch, CURLOPT_URL,  "ftp://187.162.83.235:21/OutBox/" . $nombre_archivo);
        curl_setopt($ch, CURLOPT_UPLOAD, 1);
        curl_setopt($ch, CURLOPT_INFILE, $fp);
        curl_setopt($ch, CURLOPT_USERPWD, "tableros:2A8;tt49");
        curl_setopt($ch, CURLOPT_INFILESIZE, filesize($urlNombre));
        curl_exec($ch);
        $error_no = curl_errno($ch);
        curl_close($ch);
        if ($error_no == 0) {
            $error = 'File uploaded succesfully.';
        } else {
            $error = 'File upload error.';
        }
        echo '<br>';
        echo $error;
    }
}
