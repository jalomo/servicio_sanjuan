<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generar_archivoFord extends CI_Controller {

    var $folder_save = CONST_URL_SAVE_FILE;

    //var $folder_save = 'assets/ordenesFord/';
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_integracion','mi');
        //$this->load->model('mConsultas', 'mi', TRUE);
        date_default_timezone_set(CONST_ZONA_HORARIA);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        if(date('H:i') >= '23:30' || date('H:i')<='07:30'){
            exit();
        }
        //exit();
    }

    public function index()
    {
          $date = new DateTime('2000-01-01');
          echo $date->format('Y-m-d H:i:s');
    }

    //Recibimos el id Cita para recuperar la informacion de la orden
    public function RO_Data_Sample($x = '')
    {
        //Recibimos los datos
        $numero_envio_ford = $this->mi->versionArchivoFORD();
        try {
            $fecha_hora_extension = date("Ymd")."_".date('His').".csv";
            $renglon = "";
            $ro_orden = [];
            //Para almacenar el numero de orden
            $ro_number = [];
            $bid = CONST_BID;
            $nombreExt = $bid."_RODataSample_".$fecha_hora_extension;

            
            $urlNombre = $this->folder_save.date('Ymd');

            if (!file_exists($urlNombre)) {
                mkdir($urlNombre, 0777, true);
            } 
            $urlNombre = $urlNombre.'/'.$nombreExt;

           
            // Abrir el archivo, creándolo si no existe
            $archivo = fopen($urlNombre, "wb");

            if( $archivo == false ) {
                echo "Error al crear el archivo: ".$nombreExt;
            }else {
                //Cargamos el contenido del archivo
                $ordenes_t1 = $this->mi->datos_RO_Data_Sample();
                //debug_var($ordenes_t1);die();
                fwrite($archivo, 'HDR|RODataSample|'.date("Ymd").'|'.count($ordenes_t1).'|'.$numero_envio_ford.' '."\n");

                foreach ($ordenes_t1 as $row) {
                    
                    $renglon .= $bid."|";

                    $renglon .= $row->RO_Number."|";
                    $ro_number[] = $row->RO_Number;
                    $fecha_a1 = new DateTime($row->RO_Open_Date);
                    $renglon .= $fecha_a1->format('Y/m/d H:i:s')."|";

                    //Verificamos que el campo este afectado
                    if ($row->RO_Closed_Date != "0000-00-00 00:00:00" && $row->RO_Closed_Date != null) {
                        $fecha_a2 = new DateTime($row->RO_Closed_Date);
                        $renglon .= $fecha_a2->format('Y/m/d H:i:s')."|";
                    }else{
                        $renglon .= "|";
                    }
                    
                    $renglon .= $row->Odometer_Reading."|";
                    $renglon .= $row->Odometer_Indicator."|";
                    $renglon .= $row->VIN."|";
                    $modelo = str_replace(',',' ', $row->Vehicle_Model);
                    $renglon .= $modelo."|";
                    $renglon .= $row->Vehicle_Year."|";
                    $renglon .= $row->Service_Advisor_STARS_ID."|";

                    //Verificamos que el campo este afectado
                    if ($row->Vehicle_Check_In_Date != "0000-00-00 00:00:00" && $row->Vehicle_Check_In_Date!=null) {
                        $fecha_a3 = new DateTime($row->Vehicle_Check_In_Date);
                        $renglon .= $fecha_a3->format('Y/m/d H:i:s')."|";
                    }else{
                        $renglon .= "|";
                    }

                    //Verificamos que el campo este afectado
                    if ($row->Vehicle_Check_Out_Date != "0000-00-00 00:00:00" && $row->Vehicle_Check_Out_Date !=null) {
                        $fecha_a4 = new DateTime($row->Vehicle_Check_Out_Date);
                        $renglon .= $fecha_a4->format('Y/m/d H:i:s')."|";
                    }else{
                        $renglon .= "|";
                    }

                    //Verificamos que el campo este afectado
                    if ($row->Promised_Date != "0000-00-00 00:00:00" && $row->Promised_Date !=null) {
                        $fecha_a5 = new DateTime($row->Promised_Date);
                        $renglon .= $fecha_a5->format('Y/m/d H:i:s')."|";
                    }else{
                        $renglon .= "|";
                    }

                    //Verificamos que el campo este afectado
                    if ($row->RO_Invoice_Date != "0000-00-00 00:00:00" && $row->RO_Invoice_Date !=null) {
                        $fecha_a6 = new DateTime($row->RO_Invoice_Date);
                        $renglon .= $fecha_a6->format('Y/m/d H:i:s')."|";
                    }else{
                        $renglon .= "|";
                    }

                    //Verificamos que el campo este afectado
                    if ($row->Signed_Documents != "0000-00-00 00:00:00" && $row->Signed_Documents!=null) {
                        $fecha_a7 = new DateTime($row->Signed_Documents);
                        $renglon .= $fecha_a7->format('Y/m/d H:i:s')."|";
                    }else{
                        $renglon .= "|";
                    }

                    //Verificamos que el campo este afectado
                    if ($row->Job_Card_Created_Date != "0000-00-00 00:00:00" && $row->Job_Card_Created_Date!=null) {
                        $fecha_a8 = new DateTime($row->Job_Card_Created_Date);
                        $renglon .= $fecha_a8->format('Y/m/d H:i:s')."|";
                    }else{
                        $renglon .= "|";
                    }

                    //Verificamos que el campo este afectado
                    if ($row->Planned_Job_Start != "0000-00-00 00:00:00" && $row->Planned_Job_Start !=null) {
                        $fecha_a9 = new DateTime($row->Planned_Job_Start);
                        $renglon .= $fecha_a9->format('Y/m/d H:i:s')."|";
                    }else{
                        $renglon .= "|";
                    }

                    //Verificamos que el campo este afectado
                    if ($row->Planned_Job_Finish != "0000-00-00 00:00:00" && $row->Planned_Job_Finish !=null) {
                        $fecha_a10 = new DateTime($row->Planned_Job_Finish);
                        $renglon .= $fecha_a10->format('Y/m/d H:i:s')."|";
                    }else{
                        $renglon .= "|";
                    }

                    //Verificamos que el campo este afectado
                    if ($row->Actual_Job_Start != "0000-00-00 00:00:00" && $row->Actual_Job_Start !=null) {
                        $fecha_a11 = new DateTime($row->Actual_Job_Start);
                        $renglon .= $fecha_a11->format('Y/m/d H:i:s')."|";
                    }else{
                        $renglon .= "|";
                    }

                    //Verificamos que el campo este afectado
                    if ($row->Actual_Job_Finish != "0000-00-00 00:00:00" && $row->Actual_Job_Finish !=null) {
                        $fecha_a12 = new DateTime($row->Actual_Job_Finish);
                        $renglon .= $fecha_a12->format('Y/m/d H:i:s')."|";
                    }else{
                        $renglon .= "|";
                    }

                    //Verificamos que el campo este afectado
                    if ($row->Job_Completion_Date != "0000-00-00 00:00:00" && $row->Job_Completion_Date !=null) {
                        $fecha_a13 = new DateTime($row->Job_Completion_Date);
                        $renglon .= $fecha_a13->format('Y/m/d H:i:s')."|";
                    }else{
                        $renglon .= "|";
                    }

                    $renglon .= $row->Appointment_ID."|";

                    //Verificamos que el campo este afectado
                    if ($row->Date_Appointment_Initiated != "0000-00-00 00:00:00" && $row->Date_Appointment_Initiated !=null) {
                        $fecha_a14 = new DateTime($row->Date_Appointment_Initiated);
                        $renglon .= $fecha_a14->format('Y/m/d H:i:s')."|";
                    }else{
                        $renglon .= "|";
                    }

                    //Verificamos que el campo este afectado
                    if ($row->Appointment_Date != "0000-00-00 00:00:00" && $row->Appointment_Date !=null) {
                        $fecha_a15 = new DateTime($row->Appointment_Date);
                        $renglon .= $fecha_a15->format('Y/m/d H:i:s')."|";
                    }else{
                        $renglon .= "|";
                    }

                    $renglon .= $row->Appointment_Type;

                    //Guardamos el numero de orden
                    $ro_orden[] = $row->id;

                    fwrite($archivo, $renglon."\n");
                    $renglon = "";
                }
                fwrite($archivo, 'TLR'."\n");
            }

            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($archivo);

            //Cerramos el archivo
            fclose($archivo);

            nl2br(file_get_contents($urlNombre));
            
        } catch (\Exception $e) {
            echo "No se pudo generar el archivo ".$nombreExt;
            echo "\n";
        }
        //Si se creo el archivo, imprimimos la liga
        if(file_exists($urlNombre)){
           //echo "Archivo correspondiente a la tabla RO Data Sample : <br>";
            $this->subirOxlo($urlNombre,$nombreExt);
           //echo "<a href='".base_url()."".$urlNombre."'>VER ARCHIVO</a><br><br>";
        }

        //Mandamos a crear el siguiente archivo
        $this->RO_Data_Services($ro_orden,$bid,$ro_number,$numero_envio_ford,$fecha_hora_extension);
    }

    //Resibimos el id Cita para recuperar la informacion de la orden
    public function RO_Data_Services($ro_orden = NULL,$bid = '',$ro_number = array(),$numero_envio_ford=1,$fecha_hora_extension='')
    {
        //debug_var($ro_number);die();

        if(count($ro_number)==0){
            echo 'No hay registros para generar el archivo RO_Data_Services'; exit();
        }
        
        try {
            $renglon = "";

            //Verificamos que se haya cargado los numero de orden
            if (count($ro_number)>0) {
                //Para almacenar el numero de operacion
                $id_articulo = [];
                $nombreExt = $bid."_RODataServ_".$fecha_hora_extension;
                $urlNombre = $this->folder_save.date('Ymd');
                if (!file_exists($urlNombre)) {
                    mkdir($urlNombre, 0777, true);
                } 
                $urlNombre = $urlNombre.'/'.$nombreExt;

                // Abrir el archivo, creándolo si no existe
                $archivo = fopen($urlNombre, "wb");

                if( $archivo == false ) {
                    echo "Error al crear el archivo: ".$nombreExt;
                }else {
                    //CORREGIR 
                    $cantidad_RO_Data_Services = $this->db->where_in('RO_Number',$ro_number)->select('count(id_ds) as total')->get('RO_Data_Services')->row()->total;
                    
                    fwrite($archivo, 'HDR|RODataService|'.date("Ymd").'|'.$cantidad_RO_Data_Services.'|'.$numero_envio_ford.' '."\n");
                    for ($i=0; $i < count($ro_number) ; $i++) { 
                        $this->db->order_by('RO_Number');
                        $this->db->order_by('Sequence_Number');
                        $ordenes_t2 = $this->mi->get_result("RO_Number",$ro_number[$i],"RO_Data_Services");
                        foreach ($ordenes_t2 as $row){
                            $id_articulo[] = $row->idarticulo;

                            $renglon .= $bid."|";
                            $renglon .= $row->RO_Number."|";
                            $renglon .= $row->Sequence_Number."|";
                            $renglon .= $row->RO_Type_Code."|";
                            $renglon .= $row->Servicing_Department_Code."|";
                            $renglon .= $row->Labor_Opcode."|";

                            //Verificamos que el campo este afectado
                            if ($row->Planned_Start != "0000-00-00 00:00:00" && $row->Planned_Start !=null) {
                                $fecha_b1 = new DateTime($row->Planned_Start);
                                $renglon .= $fecha_b1->format('Y/m/d H:i:s')."|";
                            }else{
                                $renglon .= "|";
                            }

                            //Verificamos que el campo este afectado
                            if ($row->Planned_Finish != "0000-00-00 00:00:00" && $row->Planned_Finish !=null) {
                                $fecha_b2 = new DateTime($row->Planned_Finish);
                                $renglon .= $fecha_b2->format('Y/m/d H:i:s')."|";
                            }else{
                                $renglon .= "|";
                            }

                            //Verificamos que el campo este afectado
                            if ($row->Actual_Start != "0000-00-00 00:00:00" && $row->Actual_Start !=null) {
                                $fecha_b3 = new DateTime($row->Actual_Start);
                                $renglon .= $fecha_b3->format('Y/m/d H:i:s')."|";
                            }else{
                                $renglon .= "|";
                            }

                            //Verificamos que el campo este afectado
                            if ($row->Actual_Finish != "0000-00-00 00:00:00" && $row->Actual_Finish !=null) {
                                $fecha_b4 = new DateTime($row->Actual_Finish);
                                $renglon .= $fecha_b4->format('Y/m/d H:i:s')."|";
                            }else{
                                $renglon .= "|";
                            }

                            $renglon .= $row->Pause_Duration."|";
                            $renglon .= $row->Pause_Reason."|";
                            $renglon .= $row->Billed_Hours."|";
                            $renglon .= $row->Line_Status."|";
                            $renglon .= $row->Technician;

                            //Guardamos el renglon
                            fwrite($archivo, $renglon."\n");
                            //Reiniciamos el renglon
                            $renglon = "";
                        }
                        
                    }
                    fwrite($archivo, 'TLR'."\n");
                        
                }

                // Fuerza a que se escriban los datos pendientes en el buffer:
                fflush($archivo);

                //Cerramos el archivo
                fclose($archivo);

                nl2br(file_get_contents($urlNombre));
            }else{
                echo "No se cargaron los 'RO_Number' ";
            }
            // $contenedor["respuesta"] = "OK";
        } catch (\Exception $e) {
            echo "No se pudo generar el archivo ".$nombreExt;
            echo "\n";
        }

        if(file_exists($urlNombre)){
           //echo "Archivo correspondiente a la tabla RO Data Services : <br>";
           //echo "<a href='".base_url()."".$urlNombre."'>VER ARCHIVO</a><br><br>";
            $this->subirOxlo($urlNombre,$nombreExt);
        }
        //Mandamos a crear el siguiente archivo
        $this->RO_Data_Parts($ro_orden,$bid,$id_articulo,$numero_envio_ford,$fecha_hora_extension);
    }

    //Resibimos el id Cita para recuperar la informacion de la orden
    public function RO_Data_Parts($ro_orden = NULL,$bid = '',$id_articulo = NULL,$numero_envio_ford=1,$fecha_hora_extension='')
    {
        try {
            if(count($id_articulo)==0){
                echo 'No hay registros para generar el archivo RO_Data_Parts'; exit();
            }
            $renglon = "";

            //Verificamos que se hayan cargado los articulos
            if (count($id_articulo)>0) {
                $nombreExt = $bid."_RODataPart_".$fecha_hora_extension;
                
                $urlNombre = $this->folder_save.date('Ymd');
                if (!file_exists($urlNombre)) {
                    mkdir($urlNombre, 0777, true);
                } 
                $urlNombre = $urlNombre.'/'.$nombreExt;
                // Abrir el archivo, creándolo si no existe
                $archivo = fopen($urlNombre, "wb");

                if( $archivo == false ) {
                    echo "Error al crear el archivo: ".$nombreExt;
                }else {
                     $cantidad_RO_Data_Parts = $this->db->where_in('idoperacion',$id_articulo)->select('count(id) as total')->get('RO_Data_Parts')->row()->total;
                     fwrite($archivo, 'HDR|RO_Data_Parts|'.date("Ymd").'|'.$cantidad_RO_Data_Parts.'|'.$numero_envio_ford.' '."\n");
                    for ($i=0; $i < count($id_articulo); $i++) { 
                        $ordenes_t3 = $this->mi->get_result("idoperacion",$id_articulo[$i],"RO_Data_Parts");
                        //Recuperamos la informacion de la consulta
                        foreach ($ordenes_t3 as $row) {
                            $renglon .= $bid."|";
                            $renglon .= $row->RO_Number."|";
                            $renglon .= $row->Sequence_Number."|";
                            $renglon .= $row->Part_Prefix."|";
                            $renglon .= $row->Part_Base."|";
                            $renglon .= $row->Part_Suffix."|";
                            $renglon .= $row->Part_Quantity."|";
                            $renglon .= $row->Part_Description;

                            //Guardamos el renglon
                            fwrite($archivo, $renglon."\n");
                            //Reiniciamos el renglon
                            $renglon = "";
                        }
                    }
                    fwrite($archivo, 'TLR'."\n");
                }
                // Fuerza a que se escriban los datos pendientes en el buffer:
                fflush($archivo);

                //Cerramos el archivo
                fclose($archivo);

                nl2br(file_get_contents($urlNombre));
            } else {
                echo "No se cargaron los 'id_articulos' ";
            }
            
        } catch (\Exception $e) {
            echo "No se pudo generar el archivo ".$nombreExt;
            echo "\n";
        }

        //Si se creo el archivo, imprimimos la liga
        if(file_exists($urlNombre)){
           //echo "Archivo correspondiente a la tabla RO Data Parts : <br>";
           //echo "<a href='".base_url()."".$urlNombre."'>VER ARCHIVO</a><br><br>";
           $this->subirOxlo($urlNombre,$nombreExt);
        }else{
            echo "No se creo el archivo";
        }

        //Cambiamos el estatus de las ordenes
        for ($i=0; $i < count($ro_orden) ; $i++) { 
            $actualizacion = array(
                "orden_cerrada" => 2
            );

            //$actualizar = $this->mi->update_table_row('layout_dashboard',$actualizacion,'id',$ro_orden[$i]);
        }
        //Actualizar el envío de FORD
        $this->mi->updateNumEnvio();
    }


    //Quitar acentos
    public function quitar_tildes($cadena = "")
    {
        $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
        $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
        $texto = str_replace($no_permitidas, $permitidas ,$cadena);
        return $texto;
    }
    public function subirOxlo($ruta='',$nombre_archivo=''){
      $ch = curl_init();
      $fp = fopen($ruta, 'r');
      $localfile = $fp;
      curl_setopt($ch, CURLOPT_URL,  CONST_FTP_OXOLO.$nombre_archivo);
      curl_setopt($ch, CURLOPT_UPLOAD, 1);
      curl_setopt($ch, CURLOPT_INFILE, $fp);
      curl_setopt($ch, CURLOPT_USERPWD, CONST_USERPWD_OXLO);
      curl_setopt($ch, CURLOPT_INFILESIZE, filesize($ruta));
      curl_exec ($ch);
      $error_no = curl_errno($ch);
      curl_close ($ch);
        if ($error_no == 0) {
          $error = 'File uploaded succesfully.';
          
        } else {
            $error = 'File upload error.';
            $datos_error = array('ruta' => $ruta, 
                                 'nombre_archivo' => $nombre_archivo,
                                 'created_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('error_archivos_ford',$datos_error);
        }
        //echo $error;
    }
}
