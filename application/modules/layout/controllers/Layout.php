<?php
//https://techarise.com/import-excel-file-mysql-codeigniter/
defined('BASEPATH') OR exit('No direct script access allowed');

class Layout extends MX_Controller {
    public function __construct()
    {
    parent::__construct();
    $this->load->model('citas/m_catalogos','mcat');
    $this->load->model('principal');
    $this->load->helper('general');
    $this->load->model('m_layout','ml');
    }
    //Función para validar si ya se asignó una operación de preventivo
    public function validarPreventivo($idorden=0){
        if($idorden!=0){
            $q = $this->db->where('idorden',$idorden)->where('tipo',5)->where('cancelado',0)->get('articulos_orden');
        }else{
            $q = $this->db->where('temporal',$this->input->get('temporal'))->where('tipo',5)->where('cancelado',0)->get('articulos_orden');
        }
        if($q->num_rows()>0){
            echo 1;
        }else{
            echo 0;
        }
    }
    public function lista_operaciones(){
        $data['id_cita'] = $this->input->post('id_cita');
        $data['operaciones'] = $this->ml->getOperacionesByCita($this->input->post('id_cita'));
        $this->blade->render('lista_operaciones',$data);
    }
    public function getOperacionesByCita(){
        $operaciones = $this->ml->getOperacionesByCita($this->input->post('id_cita'));
        echo json_encode(array('exito'=>true,'cantidad'=>count($operaciones),'operaciones'=>$operaciones));
    }
    //Funcion para saber si se refiere a un estatus detenido o no
    public function getStatusDetenido(){
        $estatus = $this->ml->getStatusId($this->input->post('id_status'));
        echo $estatus->estatus_detenido;
    }
    public function getTransicionesOperacion(){
        
        $id_cita = $this->input->post('id_cita');
        $id_operacion = $this->input->post('id_operacion');
        $data['transiciones'] = $this->ml->getTransiciones($id_cita,$id_operacion);
        $array_diferencias = array();
        $array_diferencias_asesores = array();
        foreach ($data['transiciones'] as $key => $value) {
          $array_diferencias[$key] = $value;
          $diferenca_minutos = dateDiffMinutes($value->inicio,$value->fin);
    
          $array_diferencias[$key]->tiempo_transcurrido = $diferenca_minutos;
    
          $array_diferencias[$key]->minutos_pasados = $diferenca_minutos ;
    
        }
    
        //FIN ASESORES
        $data['diferencias'] = $array_diferencias;
        
         //Timpo de la cita
        $data['tecnico_asignado'] = $this->ml->getNameTecnico($this->ml->getTecnicoByOperacion($id_cita,$id_operacion));
    
        $data['tiempo_cita'] = $this->ml->getTimeCita($id_cita);
        
        //obtengo el horario actual para obtener el operador
        $horario_actual = $this->ml->getIdHorarioActual($id_cita);
        //obtengo el id del asesor
        $asesor_asignado = $this->ml->getIdOperador($horario_actual);
        $data['asesor_asignado'] = $this->ml->getNameAsesor($asesor_asignado);
    
        $data['hora_cita'] = $this->ml->getHora($horario_actual);
        $data['tecnicos_citas'] = $this->ml->getDataTecnicosCitas($id_cita,$id_operacion);
        //debug_var( $data['tecnicos_citas']);die();
        $data['fecha_promesa'] = $this->ml->hora_promesa($id_cita);
        $data['id_cita'] = $id_cita;
        $data['id_operacion'] = $id_operacion;
        echo $this->blade->render('layout/linea_tiempo_operacion',$data,true); exit();
      }
    //Validar si una operacion es carryover
    public function validarCarryover(){
        $q = $this->db->where('id',$this->input->post('idop'))->limit(1)->where('id_tecnico is not null')->get('articulos_orden')->result();
        if(count($q)>0){
            if($q[0]->id_status == 23||$q[0]->id_status==3){
                echo 2;
            }else{
                echo 1;
            }
        }else{
            echo 0;
        }
        exit();
    }
    //Cambiar operacion no aplica
    public function operacion_no_aplica(){
        $existe_tecnico = $this->ml->validarTecnicoOperacion($_POST['idop']);
        if($existe_tecnico){
            echo -1;exit();
        }else{
            $this->db->where('id',$_POST['idop'])->set('no_planificado',1)->set('id_tecnico',null)->set('id_status',null)->update('articulos_orden');
            echo 1;exit();
        }
        
    }
    public function getCodigoGenerico(){
        $info_codigo = $this->principal->getGeneric('codigo_ford',$_POST['codigo'],'codigos_gen');
        echo json_encode(array('exito'=>true,'data'=>$info_codigo));
        exit();
    }
    //Funcion para guardar las partes de preventivo
  public function savePartsPreventivo(){
    $query = "SELECT p.parte, d.descripcion, p.cantidad, pp.paquete FROM preventivo p JOIN preventivo_descripcion d ON p.id_descripcion = d.id JOIN preventivo_paquetes pp ON pp.id = p.id_paquete WHERE p.id_tipo = '125' AND codigo_ereact = 'SERV120' AND anio = '2020' AND parte != 'N/A' AND idmodelo = '25' AND p.id_motor = '24' AND p.parte != 'Mano de Obra'";

    $data = $this->db->query($query)->result();
    $tipo_aceite = 'Aceite Sintético';

    if($tipo_aceite=='Aceite Semi Sintético'){
        $removeTipo = 'Aceite Sintético';
    }else{
        $removeTipo = 'Aceite Semi Sintético';
    }
    $arr_parts = array();
    foreach ($data as $key => $value) {
     if($value->paquete!=$removeTipo){
        $arr_parts[$value->descripcion] = $value;
     }
     
    }
  }
  public function asignarOperacionPlaneacion(){
    if($this->input->post()){
        $this->form_validation->set_rules('id_op', 'operación', 'trim|required');
        if ($this->form_validation->run()){

            $this->db->where('id',$_POST['idcarryover_planeacion'])->set('id_operacion',$_POST['id_op'])->update('tecnicos_citas_historial');
            $info_cita = $this->principal->getGeneric('id',$_POST['idcarryover_planeacion'],'tecnicos_citas_historial');

            //Actualizar la operación
            $this->db->where('id',$_POST['id_op'])->set('id_tecnico',$info_cita->id_tecnico)->set('id_status',1)->update('articulos_orden');
            echo 1;exit();
        }else{
            $errors = array(
                'id_op' => form_error('id_op'),
            );
            echo json_encode($errors); exit();
        }
    }
    $idorden = $this->ml->getIdOrdenByCita($this->input->get('id_cita'));
    $operaciones = $this->db->where('idorden',$idorden)->where('no_planificado',0)->where('cancelado',0)->where('id_tecnico is null')->get('articulos_orden')->result();
    $data['cantidad_operaciones'] = count($operaciones);
    $data['drop_operaciones'] = form_dropdown('id_op',array_combos($operaciones,'id','descripcion',TRUE),'','class="form-control" id="id_op"');
    $data['idcarryover'] = $this->input->get('idcarryover');

    $this->blade->render('operacion_planeacion',$data);
    }
    
}
