<form id="frm-asignar-op">
	<input type="hidden" id="idcarryover_planeacion" name="idcarryover_planeacion" value="{{$idcarryover}}">
	@if($cantidad_operaciones >0)
	<div class="row">
		<div class="col-sm-12">
			<label>Selecciona una operación</label>
			{{$drop_operaciones}}
			<span class="error error_id_op"></span>
		</div>
	</div>
	@else
	<h3 class="text-center">No se puede realizar está acción por que no se ha generado una orden</h3>
	@endif
</form>