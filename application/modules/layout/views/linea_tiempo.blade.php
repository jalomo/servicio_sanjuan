@layout('tema_luna/layout')
<style>
	.detail span{
		font-size: 20px;
	}
</style>
@section('contenido')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<input type="hidden" id="id_cita" value="{{$id_cita}}">

	<ol class="breadcrumb">
	    <li class="breadcrumb-item">
	      <a href="#">Inicio</a>
	    </li>
	    <li class="breadcrumb-item active">Línea de tiempo</li>
  	</ol>
  	<div class="row">
  		<!--<div class="col-sm-4 detail">
  			<span for="">Tiempo de cita : {{$tiempo_cita}} </span> 
  		</div>-->
  		<div class="col-sm-4 detail">
  			<span for="">Técnico : {{$tecnico_asignado}} </span> 
  		</div>
  		<div class="col-sm-4 detail">
  			<span for="">Asesor : {{$asesor_asignado}} </span> 
  		</div>
  		<div class="col-sm-4 detail">
  			<span for="">Hora de cita : {{$hora_cita}} </span> 
  		</div>
  	</div>
  	<br>
<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-general-tab" data-toggle="tab" href="#nav-general" role="tab" aria-controls="nav-general" aria-selected="true">General</a>
	@foreach($lista_operaciones as $l =>$operaciones)
    <a class="nav-item nav-link js_linea" id="nav-operacion-{{$operaciones->id}}" data-toggle="tab" href="#nav-{{$operaciones->id}}" role="tab" aria-controls="nav-{{$operaciones->id}}" aria-selected="false" data-id_operacion="{{$operaciones->id}}">{{$operaciones->descripcion}}</a>
	@endforeach
  </div>
</nav>

<div class="tab-content" id="nav-tabContent">
	<div class="tab-pane fade show active" id="nav-general" role="tabpanel" aria-labelledby="nav-general-tab">
		<BR></BR>
		<div class="row">
			<div class="col-sm-4">
				<?php $tiempo_cita = 0 ?>
				@foreach($tecnicos_citas as $t => $value)
				<?php $tiempo_cita = $this->mc->diferencia_horas_laborales($value->fecha.' '.$value->hora_inicio_dia,$value->fecha_fin.' '.$value->hora_fin); ?>

					Desde {{date_eng2esp_1($value->fecha).' '.$value->hora_inicio_dia}} Hasta {{date_eng2esp_1($value->fecha_fin).' '.$value->hora_fin}} <br>
				@endforeach
			</div>
			<div class="col-sm-4">
				<label for="">Tiempo de cita: {{$tiempo_cita}} minutos </label>
			</div>
			<div class="col-sm-4">
				<label for="">Fecha promesa: {{$fecha_promesa}} </label>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-12">
				<table class="table table-bordered table-striped table-striped">
					<thead>
						<tr>
							<th>Usuario</th>
							<th>De</th>
							<th>A</th>
							<th>Fecha estatus anterior</th>
							<th>Fecha estatus nuevo</th>
							<th>Tipo</th>
							<th>Tiempo (minutos)</th>

						</tr>
					</thead>
					<tbody>
						<?php $suma_minutos = 0 ?>
						@foreach($diferencias as $d => $value)
							<tr>

								<td>{{$value->usuario}}</td>
								<td>
									@if($value->tipo==2 && $value->status_anterior=='')
										Planeado
									@else
									{{$value->status_anterior}}
									@endif
								</td>
								<td>{{$value->status_nuevo}}</td>
								<td>{{$value->inicio}}</td>
								<td>{{$value->fin}}</td>
								<td>{{($value->tipo==1)?'Asesor':'Técnico'}}</td>
								<?php $suma_minutos = $suma_minutos + $value->minutos_pasados ?>
								<td>{{$value->minutos_pasados}}</td>
							</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr >
							<td colspan="6">
							<td>
								Tiempo de proceso: {{$this->mc->toHours($suma_minutos)}}
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-12">
				<h2 class="text-center">Tiempo del proceso Asesores / Técnicos</h2>
				<div id="container" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
			</div>
		</div>
	</div> <!-- FIN DIV GENERAL -->
	@foreach($lista_operaciones as $l =>$operaciones)
	<div class="tab-pane fade show" id="nav-{{$operaciones->id}}" role="tabpanel" aria-labelledby="nav-operacion-{{$operaciones->id}}">
	
		<div id="div-info-{{$operaciones->id}}">
		
		</div>
	</div>
	@endforeach
</div>
	  

@endsection

@section('scripts')
<script>
	var site_url = "{{site_url()}}"
	var array_diferencias = [];
	var lista_operaciones_cargadas = [];
	@foreach($diferencias as $d => $value)
		var array_diferencias_temp = [];
		var tipo = "{{($value->tipo==1)?'(A)':'(T)'}}";
		@if($value->status_anterior!='')
			array_diferencias_temp.push("{{$value->status_anterior}}",parseInt("{{$value->minutos_pasados}}"));
		@else
			@if($value->tipo==1)
			array_diferencias_temp.push("check asesor",parseInt("{{$value->minutos_pasados}}"));
			@else
				@if($value->status_anterior=='')
					array_diferencias_temp.push("Planeado",parseInt("{{$value->minutos_pasados}}"));
				@endif
			@endif
		@endif
		array_diferencias.push(array_diferencias_temp);
	@endforeach
	

	Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Minutos'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Minutos: <b>{point.y:.1f}</b>'
    },
    series: [{
        name: 'Population',
        data: array_diferencias,
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});

	$(".js_linea").on('click',function(){
    var url =site_url+"/layout/getTransicionesOperacion";
	var id_operacion = $(this).data('id_operacion')
		//if(!lista_operaciones_cargadas.includes( id_operacion )){
			ajaxLoad(url,{"id_cita":$("#id_cita").val(),"id_operacion":id_operacion},"div-info-"+$(this).data('id_operacion'),"POST",function(){
				lista_operaciones_cargadas.push(id_operacion); //No vuelva a cargar si ya cargó
			});
		//}
	});
</script>

@endsection