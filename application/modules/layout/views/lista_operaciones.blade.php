<table class="table table-hover table-striped">
    <thead>
        <tr>
            <th>
                Operación
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach($operaciones as $p => $operacion)
            <tr>
                <td>
                    <a href="{{site_url('citas/linea_tiempo/'.$id_cita.'/'.$operacion->id)}}">{{$operacion->descripcion}}</a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>