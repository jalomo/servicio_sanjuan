<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Api extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('m_api', 'ma');
    $this->load->model('citas/m_catalogos', 'mcat');
    $this->load->model('citas/m_citas', 'mc');
    $this->load->model('dms/m_dms', 'm_dms');
    $this->load->model('proceso/m_proceso', 'mp');
    $this->load->model('dmspaquetes/M_dmspaquetes', 'mpaq');
    $this->load->model('principal');
    $this->load->helper(array('correo', 'notificaciones', 'correos_usuario'));
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  public function catalogos()
  {
    /*
    1) cat_transporte
    2) cat_anios
    3) cat_marca
    4) cat_modelo
    5) preventivo_modelos
    6) drop_id_prepiking
    7) operadores (ASESORES)
    8) cat_servicios
    9) cat_colores
    */
    $data = $this->mcat->get($_POST['catalogo']);
    echo json_encode(array('exito' => true, 'data' => $data));
  }
  //Guardar la cita
  public function save()
  {
    $fecha_separada = explode('-', $_POST['fecha']);
    $fecha_anio = isset($fecha_separada[0]) ? $fecha_separada[0] : null;
    $fecha_mes = isset($fecha_separada[1]) ? $fecha_separada[1] : null;
    $fecha_dia = isset($fecha_separada[2]) ? $fecha_separada[2] : null;
    $id_horario_save = isset($_POST['horario']) ? $_POST['horario'] : null;

    //Hacer validación de que no esté ocupado
    if ($id_horario_save != null && $_POST['id'] == 0) {
      $qhorario = $this->db->select('ocupado')->from('aux')->where('id', $id_horario_save)->get();
      if ($qhorario->num_rows() == 1) {
        if ($qhorario->row()->ocupado) {
          echo json_encode(array('exito' => false, 'message' => 'El horario del asesor ya fue ocupado'));
          die();
        }
      }
    }
    //Validar horario del técnico
    if ($_POST['id'] == 0) {
      $qhorario = $this->db->where('id_tecnico', $_POST['id_tecnico'])
        ->where('hora_inicio', $_POST['horario_tecnico'])
        ->where('fecha', $_POST['fecha'])
        ->get('tecnicos_citas');
      if ($qhorario->num_rows() == 1) {
        echo json_encode(array('exito' => false, 'message' => 'El horario del técnico ya fue ocupado'));
        die();
      }
    }
    $datos = array(
      'transporte' => isset($_POST['transporte']) ? $_POST['transporte'] : null,
      'email' => strtolower($_POST['email']),
      'vehiculo_anio' => $_POST['anio'],
      'vehiculo_modelo' => $_POST['modelo'],
      'vehiculo_placas' => $_POST['placas'],
      'vehiculo_numero_serie' => $_POST['numero_serie'],
      'comentarios_servicio' => $_POST['comentarios_servicio'],
      'asesor' => $_POST['asesor'],
      'id_asesor' => isset($_POST['asesor']) ? $this->mc->getIdAsesor($_POST['asesor']) : '',
      'fecha_anio' => $fecha_anio,
      'fecha_mes' => $fecha_mes,
      'fecha_dia' => $fecha_dia,
      'datos_email' => strtolower($_POST['email']),
      'datos_nombres' => $_POST['nombre'],
      'datos_apellido_paterno' => $_POST['apellido_paterno'],
      'datos_apellido_materno' => $_POST['apellido_materno'],
      'datos_telefono' => $_POST['telefono'],
      'id_prepiking' => isset($_POST['id_prepiking']) ? $_POST['id_prepiking'] : '',
      'id_modelo_prepiking' => isset($_POST['id_modelo_prepiking']) ? $_POST['id_modelo_prepiking'] : '',
      'prepiking' => isset($_POST['id_prepiking']) ? $_POST['id_prepiking'] : '',
      'fecha_creacion' => date('Y-m-d'),
      'status' => 0,
      'servicio' => 'MANTENIMIENTO POR KILOMETRAJE',
      'id_horario' => $id_horario_save,
      'id_opcion_cita' => 1,
      'id_color' => $_POST['id_color'],
      'fecha' => $fecha_anio . '-' . $fecha_mes . '-' . $fecha_dia,
      'id_status_color' => 6,
      'fecha_hora' => $fecha_anio . '-' . $fecha_mes . '-' . $fecha_dia . ' ' . $this->mc->getHora(isset($_POST['horario']) ? $_POST['horario'] : null),
      'origen' => 8,
      'reagendada' => 0,
      'historial' => 0,
      'cancelada' => 0,
      'vehiculo_kilometraje' => isset($_POST['kilometraje']) ? $_POST['kilometraje'] : null,
      'cita_previa' => 1,
      'xehos' =>  0
    );

    if ($_POST['id'] == 0) {
      $datos['id_tecnico'] = $this->input->post('id_tecnico');
      $datos['fecha_creacion_all'] = date('Y-m-d H:i:s');
      $datos['id_usuario'] = 100;
      $datos['id_status'] = 1;
      $this->db->insert('citas', $datos);
      $id_cita = $this->db->insert_id();
      $horarios_tecnicos = array(
        'fecha' => $this->input->post('fecha'),
        'hora_inicio' => $this->input->post('horario_tecnico'),
        'hora_fin' => date('H:i:s', strtotime('+1 hour', strtotime($this->input->post('horario_tecnico')))),
        'id_cita' => $id_cita,
        'id_tecnico' => $this->input->post('id_tecnico'),
        'dia_completo' => 0,
        'fecha_fin' => $this->input->post('fecha'),
        'hora_inicio_dia' => $this->input->post('horario_tecnico')
      );
      $this->db->insert('tecnicos_citas', $horarios_tecnicos);
      $this->db->where('id', $_POST['horario'])->set('ocupado', 1)->update('aux');
      echo json_encode(array('exito' => true, 'data' => $datos, 'message' => 'Registro guardado correctamente'));
    } else {
      $this->db->where('id', $_POST['id'])->update('citas', $datos);
      echo json_encode(array('exito' => true, 'data' => $datos, 'message' => 'Registro actualizado correctamente'));
    }
  }
  //Obtener datos por placa
  public function getByPlaca()
  {
    $q = $this->db->where('vehiculo_placas', $this->input->post('placas'))->limit(1)->get('citas');
    if ($q->num_rows() == 1) {
      echo json_encode(array('exito' => true, 'data' => $q->row()));
    } else {
      echo json_encode(array('exito' => false));
    }
    exit();
  }
  //Obtener datos por serie
  public function getBySerie()
  {
    $q = $this->db->where('vehiculo_numero_serie', $this->input->post('serie'))->limit(1)->get('citas');
    if ($q->num_rows() == 1) {
      echo json_encode(array('exito' => true, 'data' => $q->row()));
    } else {
      echo json_encode(array('exito' => false));
    }
    exit();
  }
  // Obtener los técnicos
  public function getTecnicos()
  {
    $q = $this->db->where('activo', 1)->where('baja_definitiva', 0)->where('api', 1)->limit(1)->get('tecnicos');
    if ($q->num_rows() == 1) {
      echo json_encode(array('exito' => true, 'data' => $q->row()));
    }
  }
  //Obtener horarios de los técnicos 
  //Parámetros fecha y técnico
  public function getHorariosTecnicos()
  {
    $fecha = $_POST['fecha'];
    $id_tecnico = $_POST['id_tecnico'];
    $horarios = $this->db->select('hora_inicio_dia')->where('id_tecnico', $id_tecnico)->where('fecha', $fecha)->get('tecnicos_citas')->result();
    $tiempo_actual = date('H:i');
    $tiempo = '09:00';
    $horarios_ocupados = [];
    foreach ($horarios as $h => $horario) {
      $horarios_ocupados[] = $horario->hora_inicio_dia;
    }
    while ($tiempo <= '20:00') {
      $NuevoTiempo = strtotime('+1 hour', strtotime($tiempo));
      $tiempo = date('H:i:s', $NuevoTiempo);
      if (!in_array($tiempo, $horarios_ocupados)) {
        if ($fecha == date('Y-m-d')) {
          if ($tiempo >= $tiempo_actual) {
            $array_tiempo[] = $tiempo;
          }
        } else {
          $array_tiempo[] = $tiempo;
        }
      }
    }
    echo json_encode(array('exito' => true, 'data' => $array_tiempo));
    exit();
  }
  //Obtener fechas de los asesores
  //Recibe el nombre del asesor
  public function getfechasAsesores()
  {
    if ($this->input->post()) {
      $id_asesor = $this->mc->getIdAsesor($this->input->post("asesor"));
      $fechas = $this->mc->getFechasAsesor($id_asesor);
      echo json_encode($fechas);
    } else {
      echo 'Nada';
    }
  }
  /*
  Obtener los horarios de un asesor con una fecha
  Parámetros : asesor, id_horario (si se edita), fecha
  */
  public function getHorariosAsesor()
  {
    if ($this->input->post()) {
      $id_asesor = $this->mc->getIdAsesor($this->input->post("asesor"));
      $id_horario = $this->input->post("id_horario");
      $fecha = $this->input->post('fecha');
      //esta validación la puse por que si está editando debe ir por los horarios que no están ocupados y además el que ya tenía registrado
      if ($id_horario == 0) {
        $horarios = $this->mc->getHorariosAsesor($id_asesor, $fecha);
      } else {
        $horarios = $this->mc->getHorariosAsesor_edit($id_asesor, $fecha, $id_horario);
      }
      echo json_encode($horarios);
    } else {
      echo 'Nada';
    }
  }
  //Mis citas
  public function citasUsuario()
  {
    $citas = $this->db->where('datos_email', $_POST['email'])->get('citas')->result();
    echo json_encode(array('exito' => true, 'data' => $citas));
  }
  public function getAllTecnicos()
  {
    $tecnicos = $this->db->where('activo', 1)->where('baja_definitiva', 0)->get('tecnicos')->result();
    echo json_encode(array('exito' => true, 'data' => $tecnicos));
  }
  public function checkIn()
  {
    if (!$this->input->post()) {
      echo json_encode(array('exito' => false, 'message' => 'Es necesario ingresar la cita y horario'));
      exit();
    }
    $id_horario = $this->principal->getGeneric('id_cita', $_POST['id_cita'], 'citas', 'id_horario');
    $this->db->where('id', $id_horario)->set('id_status_cita', $_POST['id_status'])->update('aux');

    //Insertar línea tiempo
    $inicio = $this->mc->getFechaCreacion($_POST['id_cita']);
    $datos_transiciones = array(
      'status_anterior' => $this->mc->getLastStatusTransiciones($_POST['id_cita']),
      'status_nuevo' => 'Llegó',
      'fecha_creacion' => date('Y-m-d H:i:s'),
      'tipo' => 1,
      'inicio' => $inicio,
      'fin' => date('Y-m-d H:i:s'),
      'usuario' => 'Cliente',
      'id_cita' => $_POST['id_cita'],
      'mostrar_linea' => 0,
      'general' => 1,
      'minutos_transcurridos' => dateDiffMinutes($inicio, date('Y-m-d H:i:s'))
    );
    $this->db->insert('transiciones_estatus', $datos_transiciones);
    echo json_encode(array('exito' => true, 'message' => 'Registro actualizado correctamente'));
  }
  public function horaCheckIn()
  {
    $q = $this->db->where('status_nuevo', 'Llegó')->where('id_cita', $_POST['id_cita'])->order_by('id', 'asc')->limit(1)->get('transiciones_estatus');

    if ($q->num_rows() == 1) {
      echo json_encode(array('exito' => true, 'fecha' => $q->row()->fecha_creacion));
      exit();
    }
    echo json_encode(array('exito' => false, 'message' => 'No se ha hecho el check-in'));
  }
  public function getHoraCita()
  {
    $q = $this->db->select('a.fecha,a.hora')
      ->join('citas c', 'c.id_horario=a.id')
      ->where('c.id_cita', $_POST['id_cita'])
      ->limit(1)
      ->get('aux a');
    if ($q->num_rows() == 1) {
      echo json_encode(array('exito' => true, 'fecha' => $q->row()->fecha . ' ' . $q->row()->hora));
      return;
    }
    echo json_encode(array('exito' => false, 'message' => 'Error en la petición'));
  }
  public function enviar_cotizacion_multipunto($id_cita = '')
  {
    $idorden = $this->mc->getIdOrdenByCita($id_cita);
    $correo_consumidor = $this->mc->correo_consumidor($id_cita);
    $correo_compania = $this->mc->correo_compania($id_cita);

    if (!$this->mc->firmoAsesor($id_cita)) {
      echo json_encode(array('exito' => false, 'message' => 'El asesor no ha firmado el presupuesto'));
      exit();
    }
    if ($correo_consumidor != $correo_compania) {
      if ($correo_consumidor != '') {
        correo_presupuesto($correo_consumidor, encrypt($id_cita));
      }
      if ($correo_compania != '') {
        correo_presupuesto($correo_compania, encrypt($id_cita));
      }
    } else {
      correo_presupuesto($correo_consumidor, encrypt($id_cita));
    }

    //Actualizar que ya se envió el sms de esa orden
    $datos_mensaje_enviado = array('tipo_mensaje' => 'email_cotizacion_multipunto', 'idorden' => $idorden, 'enviado' => 1);
    $datos_mensaje_enviado['created_at'] = date('Y-m-d H:i:s');
    $this->db->insert('mensajes_enviados_orden', $datos_mensaje_enviado);
    echo json_encode(array('exito' => true, 'message' => 'El correo se envió correctamente'));
  }
  //obtener información de una operación
  public function getDataOperacion($folio)
  {
    $datos = $this->db->select('a.id,a.descripcion, a.id_status,a.id_tecnico,a.principal,a.folio_asociado,t.inicio,t.fin,t.usuario,t.minutos_transcurridos,t.status_anterior,t.status_nuevo')
      ->join('transiciones_estatus t', 'a.id = t.id_operacion', 'left')
      //->where('folio_asociado',$_POST['folio'])
      ->where('folio_asociado', $folio)
      //->where('mostrar_linea',1)
      ->get('articulos_orden a')
      ->result();
    echo json_encode(array('exito' => true, 'data' => $datos));
  }
  //Datos de cita
  public function getDatosCita()
  {
    if (!isset($_POST['id_cita'])) {
      echo json_encode(array('exito' => false, 'message' => 'Es necesario ingresar el #cita'));
      return;
    }
    $id_cita = $_POST['id_cita'];
    $cita = $this->mc->getCitaId($id_cita);
    echo json_encode(array('exito' => true, 'data' => $cita[0]));
  }
  public function eliminar_cita()
  {
    if (!isset($_POST['id_cita'])) {
      echo json_encode(array('exito' => false, 'message' => 'Es necesario ingresar el #cita'));
      return;
    }
    $id_horario = $this->mc->getIdHorarioActual($this->input->post('id_cita'));
    if ($this->db->where('id_cita', $this->input->post('id_cita'))->delete('citas')) {
      $this->db->where('id', $id_horario)->set('ocupado', 0)->update('aux');
      $this->db->where('id_cita', $this->input->post('id_cita'))->delete('tecnicos_citas');
      $this->db->where('id_cita', $this->input->post('id_cita'))->delete('tecnicos_citas_historial');
      echo json_encode(array('exito' => true, 'message' => 'Cita eliminada correctamente'));
      exit();
    } else {
      echo json_encode(array('exito' => false, 'message' => 'Error al eliminar la cita'));
      exit();
    }
  }
  public function getEvidencia($id_cita = '')
  {
    if ($id_cita == '') {
      echo json_encode(array('exito' => false, 'message' => 'Es necesario ingresar el #cita'));
      return;
    }
    $data['evidencia'] = $this->mp->getEvidencia($id_cita);
    $data['evidencia_multipunto'] = $this->mp->evidencia_multipunto($id_cita);
    echo json_encode(array('exito' => true, 'data' => $data));
  }
  public function datos_orden($id_cita = '')
  {
    if ($id_cita == '') {
      echo json_encode(array('exito' => false, 'message' => 'Es necesario ingresar el #cita'));
      return;
    }
    $data_orden = $this->principal->getGeneric('id_cita', $_POST['id_cita'], 'ordenservicio');
    $this->m_dms->crear_cuenta_dms($id_cita, $data_orden->id_tipo_orden);
  }
  public function addPiezasFromDMS($id_cita = '')
  {
    $data = json_decode(file_get_contents('php://input'), true);
    if ($id_cita == '') {
      echo json_encode(array('exito' => false, 'message' => 'Es necesario ingresar el #cita'));
      return;
    }
    if (!$data) {
      echo json_encode(array('exito' => false, 'message' => 'No hay información para actualizar'));
      return;
    }


    //Validar la orden
    $data_orden = $this->principal->getGeneric('id_cita', $id_cita, 'ordenservicio');
    if ($data_orden->id_situacion_intelisis == 5 || $data_orden->id_situacion_intelisis == 6) {
      echo json_encode(array('exito' => false, 'message' => 'La orden se encuentra cerrada'));
      return;
    }

    $paquete_id = $this->principal->getGeneric('id_cita', $id_cita, 'ordenservicio', 'paquete_id');
    
    if (!$paquete_id) {
      // echo json_encode(array('exito' => false, 'message' => 'No existe el paquete'));
      // return;
    }
    

    foreach ($data as $d => $value) {

      $dataArticulo = $this->getDataArticulo($id_cita);
      $tipo_operacion = '';
      if ($dataArticulo->num_rows() == 1) {
        $tipo_operacion = $dataArticulo->row()->tipo;
        $id_operacion = $dataArticulo->row()->idoperacion;
        $tipo_preventivo = 0;
        if ($dataArticulo->row()->tipo == 5) {
          $tipo_preventivo = 1;
        }
      }
      

      // 
      if (!$value['afecta_paquete'] && !$value['afecta_mano_obra'] && !$value['presupuesto']) {
        //Agregar la operación
        $articulos_orden = [
          "descripcion" => $value['descripcion'],
          "idorden" => $this->mc->getIdOrdenByCita($id_cita),
          "id_status" => '',
          "id_tecnico" => '',
          "principal" => 0,
          "no_planificado" => 1,
          "cancelado" => 0,
          "precio_unitario" => (float)$value['precio'] * 1.16,
          "cantidad" => $value['cantidad'],
          "descuento" => 0,
          "total" => (float)$value['precio'] * 1.16,
          "id_usuario" => '',
          "created_at" => date('Y-m-d H:i:s'),
          "operacion" => null,
          "tipo" => $tipo_operacion,
          "total_horas" => $value['tiempo'],
          "costo_hora" => '',
          "numero_pieza" => $value['codigo'],
          "generado_ford" => 0,
        ];
        $this->db->insert('articulos_orden', $articulos_orden);
        $id_operacion = $this->db->insert_id();
    }
    //Insert prepiking extra cita
    $array_insert = [
      'codigo' => $value['codigo'],
      'descripcion' => $value['descripcion'],
      'id_cita' => $id_cita,
      'created_at' => date('Y-m-d H:i:s'),
      'registro_individual' => 1,
      'precio' => $value['precio'],
      'cantidad' => $value['cantidad'],
      'tiempo' => $value['tiempo'],
      'id_operacion' => $id_operacion,
      'tipo_preventivo' => $tipo_preventivo,
      'fromDms' => 1,
    ];
    $this->db->insert('prepiking_extra_cita', $array_insert);

    

      // Si existe el paquete y se va afectar el paquete
      if ($paquete_id && $value['afecta_paquete']) {
        //Si existe el paquete y la pieza borrarla
        $array_insert_detalle_paquete = [
          'articulo_id' => $this->AddOrUpdateArticulo($value['descripcion']),
          'paquete_id' => $paquete_id,
          'cantidad' => $value['cantidad'],
          'precio_unitario' => 0,
          'total_refaccion' => $value['precio'],
          'tiempo_tabulado' => $value['tiempo'],
          'precio_calculado' => 0,
          'parte' => $value['codigo'],
          'created_at' => date('Y-m-d H:i:s'),
          'activo' => 1,
        ];
        $this->db->insert('pkt_detalle_paquete',$array_insert_detalle_paquete);
        $this->mpaq->PreciosPaquetes($paquete_id);
      }
    }
    $data_return = $this->getRefacciones($id_cita);

    $data_log = [
      "id_cita" => $id_cita,
      "postData" => json_encode($data),
      "responseData" => json_encode($data_return),
      "tipo" => 1,
      "created_at" => date('Y-m-d H:i:s')
    ];
    $this->db->insert('logs_piezas',$data_log);
    echo json_encode(array('exito' => true, 'message' => 'Información actualizada con éxito', 'dataUpdated' => $data_return));
  }
  public function deletePiezasFromDMS($id_cita = '')
  {
    if ($id_cita == '') {
      echo json_encode(array('exito' => false, 'message' => 'Es necesario ingresar el #cita'));
      return;
    }
    //Validar la orden
    $data_orden = $this->principal->getGeneric('id_cita', $id_cita, 'ordenservicio');
    if ($data_orden->id_situacion_intelisis == 5 || $data_orden->id_situacion_intelisis == 6) {
      echo json_encode(array('exito' => false, 'message' => 'La orden se encuentra cerrada'));
      return;
    }
    $data = json_decode(file_get_contents('php://input'), true);

    if (!$data) {
      echo json_encode(array('exito' => false, 'message' => 'No hay información para eliminar'));
      return;
    }

    $old_row = $this->principal->getGeneric('id', $data['id'], 'prepiking_extra_cita');
    if (!$old_row) {
      echo json_encode(array('exito' => false, 'message' => 'No existe la pieza que deseas eliminar'));
      return;
    }
    //Guardar el historial
    $this->db->insert('historial_piezas_eliminadas', [
      'registro_anterior' => json_encode($old_row),
      'id_cita' => $id_cita,
      'created_at' => date('Y-m-d H:i:s')
    ]);
    $this->db->where('id', $data['id'])->delete('prepiking_extra_cita');


    $paquete_id = $this->principal->getGeneric('id_cita', $id_cita, 'ordenservicio', 'paquete_id');
    if ($paquete_id) {
      //Si existe el paquete y la pieza borrarla
      $this->db->where('parte', $old_row->codigo)->where('paquete_id', $paquete_id)->delete('pkt_detalle_paquete');
      $this->mpaq->PreciosPaquetes($paquete_id);
    }

    $data = $this->getRefacciones($id_cita);
    echo json_encode(array('exito' => true, 'message' => 'Información actualizada con éxito', 'dataUpdated' => $data));
  }

  public function getDataArticulo($id_cita = '')
  {
    return $this->db->select('a.id as idoperacion, a.tipo, a.descripcion')
      ->join('articulos_orden a', 'a.idorden = o.id')
      ->where('id_cita', $id_cita)
      ->where('a.principal', 1)
      ->where('a.cancelado', 0)
      ->where('a.no_planificado', 0)
      ->limit(1)
      ->get('ordenservicio o');
  }
  public function updatePaqueteFromDMS($id_cita = '')
  {
    $data = json_decode(file_get_contents('php://input'), true);
    if ($id_cita == '') {
      echo json_encode(array('exito' => false, 'message' => 'Es necesario ingresar el #cita'));
      return;
    }
    if (!$data) {
      echo json_encode(array('exito' => false, 'message' => 'No hay información para actualizar'));
      return;
    }
    $paquete_id = $this->principal->getGeneric('id_cita', $id_cita, 'ordenservicio', 'paquete_id');
    if (!$paquete_id) {
      // echo json_encode(array('exito' => false, 'message' => 'No existe el paquete'));
      // return;
    }
    //Validar la orden
    $data_orden = $this->principal->getGeneric('id_cita', $id_cita, 'ordenservicio');
    if ($data_orden->id_situacion_intelisis == 5 || $data_orden->id_situacion_intelisis == 6) {
      echo json_encode(array('exito' => false, 'message' => 'La orden se encuentra cerrada'));
      return;
    }

    foreach ($data as $d => $value) {
      $parte_anterior = $this->principal->getGeneric('id', $value['id'], 'prepiking_extra_cita', 'codigo');
      //Update prepiking extra cita
      $array_update = [
        'codigo' => $value['codigo'],
        'precio' => $value['precio'],
        'cantidad' => $value['cantidad']
      ];
      $this->db->where('id_cita', $id_cita)->where('id', $value['id'])->update('prepiking_extra_cita', $array_update);
      //Afectar paquete
      if ($value['afecta_paquete']) {
        $dataPaquete = [
          'parte' => $value['codigo'],
          'cantidad' => $value['cantidad'],
          'total_refaccion' => $value['precio'],
        ];
        $this->db->where('paquete_id', $paquete_id)->where('parte', $parte_anterior)->update('pkt_detalle_paquete', $dataPaquete);
        //Actualizar el precio del paquete
        $this->mpaq->PreciosPaquetes($paquete_id);
      }

      if (!$value['afecta_paquete'] && !$value['afecta_mano_obra'] && !$value['presupuesto']) {
        $id_operacion = $this->principal->getGeneric('id', $value['id'], 'prepiking_extra_cita', 'id_operacion');
        //Agregar la operación
        $articulos_orden = [
          // "descripcion" => $value['descripcion'],
          "precio_unitario" => (float)$value['precio'] * 1.16,
          "cantidad" => $value['cantidad'],
          "total" => (float)$value['precio'] * 1.16,
          "created_at" => date('Y-m-d H:i:s'),
          "total_horas" => $value['tiempo'],
          "numero_pieza" => $value['codigo'],
        ];
        //$this->db->where('id',$id_operacion)->update('articulos_orden', $articulos_orden);
    }
      


    }
    $data_return = $this->getRefacciones($id_cita);

    $data_log = [
      "id_cita" => $id_cita,
      "postData" => json_encode($data),
      "responseData" => json_encode($data_return),
      "tipo" => 2,
      "created_at" => date('Y-m-d H:i:s')
    ];
    $this->db->insert('logs_piezas',$data_log);
    
    echo json_encode(array('exito' => true, 'message' => 'Información actualizada con éxito', 'dataUpdated' => $data_return));
  }
  //Obtener información de la cita
  public function get_all_data_cita()
  {
    if (!isset($_POST['id_cita'])) {
      echo json_encode(array('exito' => false, 'message' => 'Es necesario ingresar el #cita'));
      return;
    }
    $data =  $this->db->where('c.id_cita', $_POST['id_cita'])
      ->join('ordenservicio o', 'o.id_cita=c.id_cita')
      ->join('cat_colores cc', 'cc.id = c.id_color')
      ->join('pkt_cat_transmision t', 't.id = o.transmision_id')
      ->join('operadores op', 'op.id = c.id_asesor')
      ->join('pkt_subarticulos sbm', 'sbm.id = c.submodelo_id')
      ->select('c.*,o.*,cc.color,t.transmision,op.clave as clave_asesor,op.idStars as idStars_asesor,sbm.nombre as subarticulo')
      ->get('citas c')
      ->row();
    echo json_encode(array('exito' => true, 'data' => $data));
  }
  public function getInfoRefacciones($id_cita = '')
  {
    $data = $this->getRefacciones($id_cita);
    echo json_encode(array('exito' => true, 'data' => $data));
  }
  public function getRefacciones($id_cita = '')
  {
    return $this->db->select('id,codigo,descripcion,precio,cantidad,tiempo')
      ->where('id_cita', $id_cita)
      ->get('prepiking_extra_cita')
      ->result();
  }
  //Crear catálogo de artículos si no existe la descripción
  public function AddOrUpdateArticulo($descripcion)
  {
    $articulo = $this->principal->getGeneric('descripcion', $descripcion, 'pkt_articulos');
    if ($articulo) {
      return $articulo->id;
    } else {
      //Agregarlo
      $this->db->insert('pkt_articulos', [
        'descripcion' => $descripcion,
        'activo' => 1
      ]);
      return $this->db->insert_id();
    }
  }
  public function guardarLogs($data = [])
	{
		$this->db->insert('logs_pizas', $data);
	}
}
