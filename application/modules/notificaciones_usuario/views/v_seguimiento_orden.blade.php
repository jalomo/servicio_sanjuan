<p>
    Hola <strong>{{ $cita->datos_nombres . ' ' . $cita->datos_apellido_paterno . ' ' . $cita->datos_apellido_materno }}
    </strong> le agradecemos su visita a la agencia y se adjuntan formatos de la Orden de Servicio y Contrato de
    Adhesión de la recepción de su vehículo y evidencias de su inventario</p>
<p
    style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
    <strong>Fecha promesa: </strong> {{ date_eng2esp_1($cita->fecha_entrega) }} a las {{ $cita->hora_entrega }} <br>
    <strong>Folio Orden: </strong> {{ $id_cita }} <br>
    <strong>Servicio</strong> {{ $cita->servicio }} <br>
    <strong>Unidad: </strong> {{ $cita->vehiculo_modelo }} {{ $cita->vehiculo_anio }}, placas:
    {{ $cita->vehiculo_placas }} <br>
    <strong>Asesor: </strong> {{ $cita->asesor }} <br>
    @if($hora_inicio_trabajo=='')
    <strong>Estatus: </strong> Planeado <br>
    @endif
    <a target="_blank" href="{{ base_url('mi-proceso-orden/' . $id_cita_encrypt) }}">Enlace de seguimiento</a><br>
</p>
