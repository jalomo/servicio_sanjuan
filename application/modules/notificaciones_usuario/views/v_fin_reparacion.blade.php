<p>
    Hola <strong>{{ $cita->datos_nombres . ' ' . $cita->datos_apellido_paterno . ' ' . $cita->datos_apellido_materno }}
    </strong> le informamos que todos los trabajos programados en su orden {{$id_cita}} se han concluido
</p>
<p>
    Por favor pasar a la agencia para realizar su correspondiente facturación
</p>
<p
    style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
    <strong>Fecha promesa: </strong> {{ date_eng2esp_1($cita->fecha_entrega) }} a las {{ $cita->hora_entrega }} <br>
    <strong>Folio Orden: </strong> {{ $id_cita }} <br>
    <strong>Unidad: </strong> {{ $cita->vehiculo_modelo }} {{ $cita->vehiculo_anio }}, placas:
    {{ $cita->vehiculo_placas }} <br>
    <strong>Asesor: </strong> {{ $cita->asesor }} <br>
    <strong>Estatus: </strong> Terminado <br>
    <a target="_blank" href="{{ base_url('mi-proceso-orden/' . $id_cita_encrypt) }}">Enlace de seguimiento</a><br>
</p>