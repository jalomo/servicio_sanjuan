@layout('layout_correo')
@section('contenido')
    <tr style="border-collapse:collapse;">
        <td style="Margin:0;padding-top:10px;padding-bottom:20px;padding-left:30px;padding-right:30px;background-position:left top;background-color:#FAFAFA;"
            bgcolor="#fafafa" align="left">
            <table width="100%" cellspacing="0" cellpadding="0"
                style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                <tr style="border-collapse:collapse;">
                    <td width="540" valign="top" align="center" style="padding:0;Margin:0;">
                        <table width="100%" cellspacing="0" cellpadding="0" role="presentation"
                            style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                            <tr style="border-collapse:collapse;">
                                <td align="left" style="padding:0;Margin:0;padding-bottom:10px;">
                                    <h2
                                        style="Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:24px;font-style:normal;font-weight:bold;color:#212121;">
                                        {{ $asunto }}</h2>
                                </td>
                            </tr>
                            <tr style="border-collapse:collapse;">
                                <td align="left" style="padding:0;Margin:0;padding-bottom:5px;">
                                    {{$notificacion}}
                                    @if ($reparacion)
                                        <p>
                                            <strong>
                                                Estimado Cliente, su cita fue agendada para servicio de mantenimiento
                                                limpio, por lo que el tiempo de reparación ya fue asignado.

                                                En caso de requerir alguna otra revisión, favor de comunicarse al
                                                {{CONST_TEL_EMPRESA}} Ext. 411 y 486 para consultar disponibilidad con su número de
                                                folio, con la finalidad de brindarle un mejor servicio.
                                            </strong>
                                        </p>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
@endsection
