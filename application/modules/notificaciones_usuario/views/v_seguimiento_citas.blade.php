<p
    style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
    Hola <strong>{{ $cita->datos_nombres . ' ' . $cita->datos_apellido_paterno . ' ' . $cita->datos_apellido_materno }}
    </strong> agradecemos su preferencia y le confirmamos su cita en :
</p>
<br>
<p
    style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#131313;">
    <strong>Fecha Cita: </strong> {{ date_eng2esp_time($cita->fecha_hora) }} <br>
    <strong>Folio Cita: </strong> {{ $id_cita }} <br>
    <strong>Unidad: </strong> {{ $cita->vehiculo_modelo }} {{ $cita->vehiculo_anio }}, placas:
    {{ $cita->vehiculo_placas }} <br>
    <strong>Servicio</strong> {{ $cita->servicio }} <br>
    <strong>Asesor: </strong> {{ $cita->asesor }} <br>
</p>
<p>
    <a target="_blank" href="{{ base_url('mi-proceso-orden/' . $id_cita_encrypt.'/1') }}">Detalle del Servicio</a><br>
    <a target="_blank" href="{{ base_url('mi-proceso-orden/' . $id_cita_encrypt.'/2') }}">Detalle del Asesor</a><br>
    <a target="_blank" href="{{ base_url('mi-proceso-orden/' . $id_cita_encrypt) }}">Configuración de Notificaciones</a><br>
</p>
<p>
    <strong>Para hacer check-in sólo escanea el QR de la agencia con el siguiente</strong> 
    <a target="_blank"
        href="https://planificadorempresarial.com/qr_sanjuan/?token=1wmI3zvoKCHc89U2kNblF1y5HDRgORy5f1wmI3zvoKCHc89U2kNblF1y5HDRgORy5f&id_cita={{$id_cita_encrypt}}=&sucursal=M2139">enlace</a>
</p>


