<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_notificaciones extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  public function getAllData($id_cita=''){
    return $this->db->where('c.id_cita',$id_cita)
                    ->join('ordenservicio o','o.id_cita = c.id_cita','left')
                    ->join('aux a','a.id = c.id_horario','left')
                    ->join('cat_colores cc','cc.id=c.id_color')
                    ->select('c.*,o.*,o.id as idorden,a.hora as hora_cita,a.fecha as fecha_cita,a.id_status_cita,cc.color')
                    ->get('citas c')
                    ->result();
  }
  //Nombre asesor
  public function asesor_by_nombre($nombre=''){
    $q = $this->db->where('nombre',$nombre)->from('operadores')->get();
    if($q->num_rows()==1){
      return $q->row();
    }else{
      return '';
    }
  }
  //Obtener la información de una operación
  public function getDataOperacion($id_operacion=''){
    return $this->db->select('a.id,a.descripcion,t.nombre as tecnico,cs.status')
                    ->join('cat_status_citas_tecnicos cs','cs.id = a.id_status')
                    ->join('tecnicos t','a.id_tecnico = t.id')
                    ->where('a.id',$id_operacion)
                    ->get('articulos_orden a')
                    ->row();
  }
  //Saber si tiene el permiso para enviar 
  public function PermisoNotificacion($id_cita='',$tipo=''){
    $q = $this->db->where('id_cita',$id_cita)->select($tipo)->get('configuracion_notificaciones');
    if($q->num_rows()==0){
      return true;
    }else{
      if($q->row()->$tipo==1){
        return true;
      }
      return false;
    }
  }
}
