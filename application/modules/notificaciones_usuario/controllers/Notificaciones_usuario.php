<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificaciones_usuario extends MX_Controller {
  public function __construct()
  {
    parent::__construct();
    //$this->load->model('citas/m_citas','mc');
    //$this->load->model('m_notificaciones','mn');
    $this->load->helper(array('notificaciones'));
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  //idoperacion OTQ1MDQzMTQ5OS4zNA
  //id_cita encrypt  MTM4ODMwNzQ1OTIx
  // id_cita 178371
  public function seguimiento_citas($id_cita=''){
    $data = seguimiento_citas($id_cita);
    print_r($data);die();
    // $data['id_cita_encrypt'] = $id_cita;
    // $id_cita = decrypt($id_cita);
    // $data['id_cita'] = $id_cita;
    // $data['cita'] = $this->mn->getAllData($id_cita)[0];
    // $data['asesor'] = $this->mn->asesor_by_nombre($data['cita']->asesor);
    // //Guardar el tipo de notificación que se le mandó al cliente
    // //Parametros : id_cita y el tipo de notificación que se envió
    // $this->guardarNotificacion($id_cita,1);
    // return $this->blade->render('v_seguimiento_citas',$data,TRUE);
    //print_r($cuerpo);
  }
  public function seguimiento_orden($id_cita=''){
    //Envío de notificaciones cada que se edita la orden
    $data = seguimiento_orden($id_cita);
    print_r($data);die();
    // $data['id_cita_encrypt'] = $id_cita;
    // $id_cita = decrypt($id_cita);
    // $data['id_cita'] = $id_cita;
    // $data['cita'] = $this->mn->getAllData($id_cita)[0];
    // $data['asesor'] = $this->mn->asesor_by_nombre($data['cita']->asesor);

    // $this->guardarNotificacion($id_cita,2);
    // return $this->blade->render('v_seguimiento_orden',$data,TRUE);
    //print_r($cuerpo);
  }
  public function cambio_estatus($id_cita='',$id_operacion=''){
    //Envío de notificaciones cada que cambia un estatus
    $data = cambio_estatus($id_cita,$id_operacion);
    print_r($data);die();
    // $data['id_cita_encrypt'] = $id_cita;
    // $data['id_operacion_encrypt'] = $id_operacion;
    // $id_cita = decrypt($id_cita);
    // $id_operacion = decrypt($id_operacion);
    // $data['id_cita'] = $id_cita;
    // $data['id_operacion'] = $id_operacion;
    // $data['cita'] = $this->mn->getAllData($id_cita)[0];
    // $data['asesor'] = $this->mn->asesor_by_nombre($data['cita']->asesor);
    // $data['operacion'] = $this->mn->getDataOperacion($id_operacion);
    // $this->guardarNotificacion($id_cita,3);
    // return $this->blade->render('v_cambio_estatus',$data,TRUE);
    //print_r($cuerpo);
  }
  public function presupuesto($id_cita=''){
    $data = presupuesto($id_cita);
    print_r($data);die();
    // $data['id_cita_encrypt'] = $id_cita;
    // $id_cita = decrypt($id_cita);
    // $data['id_cita'] = $id_cita;
    // $data['cita'] = $this->mn->getAllData($id_cita)[0];
    // $data['asesor'] = $this->mn->asesor_by_nombre($data['cita']->asesor);
    // $this->guardarNotificacion($id_cita,4);
    // return $this->blade->render('v_presupuesto',$data,TRUE);
    //print_r($cuerpo);
  }
  public function fin_reparacion($id_cita=''){
    $data = fin_reparacion($id_cita);
    print_r($data);die();
    // $data['id_cita_encrypt'] = $id_cita;
    // $id_cita = decrypt($id_cita);
    // $data['id_cita'] = $id_cita;
    // $data['cita'] = $this->mn->getAllData($id_cita)[0];
    // $data['asesor'] = $this->mn->asesor_by_nombre($data['cita']->asesor);
    // $this->guardarNotificacion($id_cita,5);
    // return $this->blade->render('v_fin_reparacion',$data,TRUE);
    //print_r($cuerpo);
  }
  public function facturacion($id_cita=''){
    $data = facturacion($id_cita);
    print_r($data);die();
    // $data['id_cita_encrypt'] = $id_cita;
    // $id_cita = decrypt($id_cita);
    // $data['id_cita'] = $id_cita;
    // $data['cita'] = $this->mn->getAllData($id_cita)[0];
    // $data['asesor'] = $this->mn->asesor_by_nombre($data['cita']->asesor);
    // $this->guardarNotificacion($id_cita,6);
    // return $this->blade->render('v_facturacion',$data,TRUE);
    //print_r($cuerpo);
  }
  public function guardarNotificacion($id_cita='',$id_tipo_notificacion){
    $data= [
      'id_cita' => $id_cita,
      'id_tipo_notificacion' => $id_tipo_notificacion,
      'id_usuario' => $this->session->userdata('id_usuario'),
      'created_at' => date('Y-m-d H:i:s')
    ];
    $this->db->insert('notificaciones_usuario_orden',$data,TRUE);
  }
}
