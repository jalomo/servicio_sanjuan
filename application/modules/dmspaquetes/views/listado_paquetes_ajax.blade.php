@layout('tema_luna/layout')
<link href="{{ base_url('css/custom/jquery.fileupload.css') }}" rel="stylesheet">
@section('css_vista')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('contenido')
    <form id="frm" action="" method="POST">
        <div class="row">
            <div class='col-md-3'>
                <label for="">Fecha inicio</label>
                <input type="date" class="form-control" name="finicio" id="finicio" value="">
                <span style="color: red" class="error error_fini"></span>
            </div>
            <div class='col-md-3'>
                <label for="">Fecha Fin</label>
                <input type="date" class="form-control" name="ffin" id="ffin" value="">
                <span style="color: red" class="error error_ffin"></span>
            </div>
            <div class="col-sm-6">
                <label for="">Buscar por campo</label>
                <input class="form-control" type="text" name="buscar_campo" id="buscar_campo">
            </div>
        </div>
        <div class="row pull-right">
            <div class="col-sm-3" style="margin-top:30px;">
                <button type="button" id="buscar" class="btn btn-success">Buscar</button>
            </div>
        </div>
        <br>
        <br>
        <br>
    </form>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-bordered table-striped" id="tabla" class="display" width="100%"
                cellpadding="0">
                <thead>
                    <tr class="tr_principal">
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Artículos</th>
                        <th>Subartículos</th>
                        <th>Descripción</th>
                        <th>Precio</th>
                        <th>Fecha creación</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ base_url() }}js/custom/jquery-ui.min.js"></script>
    <script src="{{ base_url() }}js/custom/jquery.fileupload.js"></script>
    <script>
        var site_url = "{{ site_url() }}";
        var tipo = '';
        var id = '';
        var url = '';
        iniciarTabla();
        $("#buscar").on('click', function() {
            $(".error").empty();
            var finicio = $("#finicio").val();
            var ffin = $("#ffin").val();
            var table = $('#tabla').DataTable();
            table.destroy();
            iniciarTabla();
        })

        function iniciarTabla() {
            var tabla = $("#tabla").dataTable({
                paging: true,
                bFilter: false,
                processing: true,
                responsive: true,
                serverSide: true,
                ajax: {
                    url: site_url + "/dmspaquetes/getDatosPaquetes",
                    type: 'POST',
                    data: function(data) {
                        data.buscar_campo = $("#buscar_campo").val();
                        data.finicio = $("#finicio").val();
                        data.ffin = $("#ffin").val();
                        empieza = data.start;
                        por_pagina = data.length;
                    }
                },
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                        "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                        '<option value="5">5</option>' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="30">30</option>' +
                        '<option value="40">40</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">Todos</option>' +
                        '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "No hay registros para mostrar...",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar"
                },
            });
        }
        $('.date').datetimepicker({
            format: 'DD/MM/YYYY',
            icons: {
                time: "far fa-clock",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            locale: 'es'
        });
    </script>
@endsection
