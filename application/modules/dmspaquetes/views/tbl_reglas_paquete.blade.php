<div id="div-reglas-paquete">
    <table class="table table-border table-striped">
        <thead>
            <tr>
                <th># Regla</th>
                <th>Artículos</th>
                <th>Subartículos</th>
                <th>Modelos</th>
                <th>Cilindros</th>
                <th>Transmisiones</th>
                <th>Motores</th>
                <th>Combustibles</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($reglas_paquete as $d => $regla)
                <tr>
                    <td>{{ $regla->numero_regla }}</td>
                    <td>
                        {{ $this->mp->getText($regla->modelo_id, $modelos, 'modelo') }}
                    </td>
                    <td>
                        {{ $this->mp->getText($regla->subarticulos, $subarticulos, 'nombre') }}
                    </td>
                    <td>
                        {{ $regla->anios }}
                    </td>
                    <td>
                        {{ $regla->cilindros }}
                    </td>
                    <td>
                        {{ $this->mp->getText($regla->transmisiones, $transmisiones, 'transmision') }}
                    </td>
                    <td>
                        {{ $this->mp->getText($regla->motores, $motores, 'motor') }}
                    </td>
                    <td>
                        {{ $this->mp->getText($regla->combustibles, $combustibles, 'combustible') }}
                    </td>
                    <td>
                        <a href="#" data-id="{{ $regla->id }}" data-modeloid="{{ $regla->modelo_id }}"
                            class="fa fa-edit editar_regla" aria-hidden="true" data-toggle="tooltip"
                            data-placement="top" title="Editar"></a>

                        <a href="" data-id="{{ $regla->id }}" class="fa fa-trash js_eliminar_regla"
                            aria-hidden="true" data-toggle="tooltip" data-placement="top"
                            title="Eliminar"></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>