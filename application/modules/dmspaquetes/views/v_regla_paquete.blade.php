<div class="row">
    <div class="col-sm-4">
        <label for="">Subartículo(s)</label>
        {{ $drop_subarticulos }}
        <span class="error error_subarticulos"></span>
    </div>
    <div class="col-sm-4">
        <label for="">Modelo(s)</label>
        {{ $drop_anios }}
        <span class="error error_anio"></span>
    </div>
    <div class="col-sm-4">
        <label for="">Cilindros(s)</label>
        {{ $drop_cilindros }}
        <span class="error error_cilindro_id"></span>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <label for="">Transmisión(es)</label>
        {{ $drop_transmisiones }}
        <span class="error error_transmision_id"></span>
    </div>
    <div class="col-sm-4">
        <label for="">Motor(es)</label>
        {{ $drop_motores }}
        <span class="error error_motor_id"></span>
    </div>
    <div class="col-sm-4">
        <label for="">Tipo de combustible(s)</label>
        {{ $drop_combustibles }}
        <span class="error error_combustible_id"></span>
    </div>
</div>