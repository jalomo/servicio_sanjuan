<form id="frm-detalle-articulos" method="POST">
    <input type="hidden" id="art_id" name="art_id" value="{{ $art_id }}">
    <input type="hidden" id="paquete_id" name="paquete_id" value="{{ $paquete_id }} ">
    <div class="row">
        <div class="col-sm-12">
            <label for="">¿Calcular precio del paquete?</label>
            <input type="checkbox" {{($precio_calculado) ? 'checked':''}} id="precio_calculado" name="precio_calculado" value="{{$precio_calculado}}">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <label for="">Artículo</label>
            {{ $articulo_id }}
            <span class="error error_articulo_id"></span>
        </div>
        <div class="col-sm-6">
            <label for="">Parte</label>
            {{ $parte }}
            <span class="error error_parte"></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <label for="">Cantidad</label>
            {{ $cantidad }}
            <span class="error error_cantidad"></span>
        </div>
        <div class="col-sm-6">
            <label for="">Precio unitario</label>
            {{ $precio_unitario }}
            <span class="error error_precio_unitario"></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <label for="">Total</label>
            {{ $total_refaccion }}
            <span class="error error_total_refaccion"></span>
        </div>
        <div class="col-sm-6">
            <label for="">tiempo tabulado</label>
            {{ $tiempo_tabulado }}
            <span class="error error_tiempo_tabulado"></span>
        </div>
    </div>
</form>
<script>
    // $(".busqueda").select2();
    // $(".busqueda").select2({ width: '100%' });
    $(".calcular_total").on('change', function() {
        if ($("#precio_calculado").prop('checked')) {
            updateTotales();
        }
    })
    $("#precio_calculado").on('click', function() {
        if ($(this).prop('checked')) {
            updateTotales();
            $("#total_refaccion").prop('readonly', true);
        } else {
            $("#total_refaccion").prop('readonly', false);
        }
    })

    function updateTotales() {
        cantidad = ($("#cantidad").val()) ? parseFloat($("#cantidad").val()) : 0;
        precio_unitario = ($("#precio_unitario").val()) ? parseFloat($("#precio_unitario").val()) : 0;
        if ($.isNumeric(cantidad) && $.isNumeric(precio_unitario)) {
            $("#total_refaccion").val(cantidad * precio_unitario);
        }
    }
</script>
