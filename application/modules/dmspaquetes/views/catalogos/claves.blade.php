@layout('tema_luna/layout')
@section('contenido')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#">Inicio</a>
        </li>
        <li class="breadcrumb-item active">Claves</li>
    </ol>
    <div class="row">
        <div class="col-sm-10">
            <h1>Lista de claves</h1>
        </div>
        <div class="col-sm-12">
            <button id="agregar_clave" class="btn btn-success pull-right">Agregar clave</button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div id="div_subarticulo">
                <table class="table table-bordered table-striped" id="tbl_claves" width="100%" cellspacing="0">
                    <thead>
                        <tr class="tr_principal">
                            <th>Código</th>
                            <th>Código MO</th>
                            <th>Tipo de mano de obra</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($claves as $c => $value)
                            <tr>
                                <td>{{ $value->clave }}</td>
                                <td>{{ $value->codigo }}</td>
                                <td>{{ $value->tipo_mo }}</td>
                                <td>{{ $value->descripcion }}</td>
                                <td>
                                    <a href="" data-id="{{ $value->id }}" class="pe pe-7s-note js_editar"
                                        aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar"></a>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var site_url = "{{ site_url() }}";
        inicializar_tabla("#tbl_claves", false);
        $("#agregar_clave").on("click", function() {
            var url = site_url + "/dmspaquetes/agregarClave/0";
            customModal(url, {}, "GET", "md", callbackGuardar, "", "Guardar", "Cancelar", "Nueva clave",
                "modal1");
        });
        $("body").on("click", '.js_editar', function(e) {
            e.preventDefault();
            var id = $(this).data('id')
            var url = site_url + "/dmspaquetes/agregarClave/" + id;
            customModal(url, {}, "GET", "md", callbackGuardar, "", "Guardar", "Cancelar", "Nueva clave",
                "modal1");

        });
        function callbackGuardar() {
            var url = site_url + "/dmspaquetes/agregarClave";
            ajaxJson(url, $("#frm").serialize(), "POST", "", function(result) {
                if (isNaN(result)) {
                    data = JSON.parse(result);
                    //Se recorre el json y se coloca el error en la div correspondiente
                    $.each(data, function(i, item) {
                        $.each(data, function(i, item) {
                            $(".error_" + i).empty();
                            $(".error_" + i).append(item);
                            $(".error_" + i).css("color", "red");
                        });
                    });
                } else {
                    if (result < 0) {
                        ErrorCustom('La clave ya fue registrada, por favor intenta con otro');
                    } else {
                        if (result == 0) {
                            ErrorCustom('No se pudo guardar la clave, por favor intenta de nuevo');
                        } else {
                            ExitoCustom("Guardado correctamente", function() {
                                $(".close").trigger("click");
                                buscar();
                            });
                        }
                    }
                }
            });
        }

        function buscar() {
            var url = site_url + "/dmspaquetes/tabla_claves";
            ajaxLoad(url, {}, "div_subarticulo", "POST", function() {
                inicializar_tabla("#tbl_claves", false);
            });
        }
    </script>
@endsection
