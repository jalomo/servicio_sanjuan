<form action="" method="POST" id="frm">
	<input type="hidden" name="id" id="id" value="{{$input_id}}">
	<input type="hidden" name="tabla" id="tabla" value="{{$tabla}}">
	<input type="hidden" name="campo" id="campo" value="{{$campo}}">
	<input type="hidden" name="nombre" id="nombre" value="{{$nombre}}">
	<div class="row form-group">
		<div class="col-sm-10">
			<label>{{$nombre}}</label>
			{{$input_nombre}}
			<span class="error error_{{$campo}}"></span>
		</div>
	</div>
</form>