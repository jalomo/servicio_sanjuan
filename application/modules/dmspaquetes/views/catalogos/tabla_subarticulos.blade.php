<table class="table table-bordered table-striped" id="tbl_subarticulo" width="100%" cellspacing="0">
    <thead>
        <tr class="tr_principal">
            <th>Subartículo</th>
            <th>Artículo</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($subarticulos as $c => $value)
            <tr>
                <td>{{ $value->nombre }}</td>
                <td>{{ $value->modelo }}</td>
                <td>
                    <a href="" data-id="{{ $value->id }}" class="pe pe-7s-note js_editar" aria-hidden="true"
                        data-toggle="tooltip" data-placement="top" title="Editar"></a>

                </td>
            </tr>
        @endforeach
    </tbody>
</table>
