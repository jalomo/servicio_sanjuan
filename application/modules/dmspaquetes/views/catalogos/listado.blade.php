@layout('tema_luna/layout')
@section('contenido')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#">Inicio</a>
        </li>
        <li class="breadcrumb-item active">{{ $catalogo['nombre'] }}</li>
    </ol>
    <div class="row">
        <div class="col-sm-10">
            <h1>Lista de {{ $catalogo['nombre'] }}</h1>
        </div>
        <div class="col-sm-12">
            <button id="agregar" class="btn btn-success pull-right">Agregar {{ $catalogo['nombre'] }}</button>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div id="div_catalogo">
                <table class="table table-bordered table-striped" id="tbl_catalogo" width="100%" cellspacing="0">
                    <thead>
                        <tr class="tr_principal">
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($listado as $c => $value)
                            <tr>
                                <?php $campo = $catalogo['campo']; ?>
                                <td>{{ $value->$campo }}</td>
                                <td>
                                    <a href="" data-id="{{ $value->id }}" class="pe pe-7s-note js_editar"
                                        aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar"></a>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        let tabla = "{{ $catalogo['tabla'] }}";
        let campo = "{{ $catalogo['campo'] }}";
        let nombre = "{{ $catalogo['nombre'] }}";
        let tipo = "{{ $tipo }}";
        let data = {
            tabla,
            campo,
            nombre
        }
        var site_url = "{{ site_url() }}";
        inicializar_tabla("#tbl_catalogo", false);
        $("#agregar").on("click", function() {
            var url = site_url + "/dmspaquetes/agregar_catalogo/0";
            customModal(url, data, "GET", "md", callbackGuardar, "", "Guardar", "Cancelar",nombre,
                "modal1");
        });
        $("body").on("click", '.js_editar', function(e) {
            e.preventDefault();
            var id = $(this).data('id')
            var url = site_url + "/dmspaquetes/agregar_catalogo/" + id;
            customModal(url, data, "GET", "md", callbackGuardar, "", "Guardar", "Cancelar",nombre,
                "modal1");

        });
        $("body").on("click", '.js_eliminar', function(e) {
            e.preventDefault();
            var id = $(this).data('id')
            ConfirmCustom(`¿Está seguro de eliminar el ${nombre}?`, callbackEliminar(id), "", "Confirmar",
                "Cancelar");

        });


        function callbackGuardar() {
            var url = site_url + "/dmspaquetes/agregar_catalogo";
            ajaxJson(url, $("#frm").serialize(), "POST", "", function(result) {
                if (isNaN(result)) {
                    data = JSON.parse(result);
                    //Se recorre el json y se coloca el error en la div correspondiente
                    $.each(data, function(i, item) {
                        $.each(data, function(i, item) {
                            $(".error_" + i).empty();
                            $(".error_" + i).append(item);
                            $(".error_" + i).css("color", "red");
                        });
                    });
                } else {
                    if (result < 0) {
                        ErrorCustom('El nombre ya fue registrado, por favor intenta con otro');
                    } else {
                        if (result == 0) {
                            ErrorCustom('No se pudo guardar, por favor intenta de nuevo');
                        } else {
                            ExitoCustom("Guardado correctamente", function() {
                                $(".close").trigger("click");
                                buscar();
                            });
                        }
                    }
                }
            });
        }

        function buscar() {
            var url = site_url + "/dmspaquetes/tabla_catalogos";
            ajaxLoad(url, {
                tabla,
                campo,
                nombre,
                tipo,
            }, "div_catalogo", "POST", function() {
                inicializar_tabla("#tbl_catalogo", false);
            });
        }
    </script>
@endsection
