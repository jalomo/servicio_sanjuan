<form action="" method="POST" id="frm">
	<input type="hidden" name="id" id="id" value="{{$input_id}}">
	<div class="row form-group">
		<div class="col-sm-12">
			<label>Clave</label>
			{{$input_clave}}
			<span class="error error_clave"></span>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-12">
			<label>Código</label>
			{{$input_codigo}}
			<span class="error error_codigo"></span>
		</div>
	</div>
    <div class="row form-group">
		<div class="col-sm-12">
			<label>Tipo mano de obra</label>
			{{$input_tipo_mo}}
			<span class="error error_tipo_mo"></span>
		</div>
	</div>
    <div class="row form-group">
		<div class="col-sm-12">
            <label>Descripción</label>
			{{$input_descripcion}}
			<span class="error error_descripcion"></span>
		</div>
	</div>
</form>