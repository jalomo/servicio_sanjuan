<table class="table table-bordered table-striped" id="tbl_claves" width="100%" cellspacing="0">
    <thead>
        <tr class="tr_principal">
            <th>Código</th>
            <th>Código MO</th>
            <th>Tipo de mano de obra</th>
            <th>Descripción</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($claves as $c => $value)
            <tr>
                <td>{{ $value->clave }}</td>
                <td>{{ $value->codigo }}</td>
                <td>{{ $value->tipo_mo }}</td>
                <td>{{ $value->descripcion }}</td>
                <td>
                    <a href="" data-id="{{ $value->id }}" class="pe pe-7s-note js_editar" aria-hidden="true"
                        data-toggle="tooltip" data-placement="top" title="Editar"></a>

                </td>
            </tr>
        @endforeach
    </tbody>
</table>
