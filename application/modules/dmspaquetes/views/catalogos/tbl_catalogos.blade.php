<table class="table table-bordered table-striped" id="tbl_catalogo" width="100%" cellspacing="0">
    <thead>
        <tr class="tr_principal">
            <th>Nombre</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($listado as $c => $value)
            <tr>
                <?php $campo = $catalogo['campo']; ?>
                <td>{{ $value->$campo }}</td>
                <td>
                    <a href="" data-id="{{ $value->id }}" class="pe pe-7s-note js_editar" aria-hidden="true"
                        data-toggle="tooltip" data-placement="top" title="Editar"></a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
