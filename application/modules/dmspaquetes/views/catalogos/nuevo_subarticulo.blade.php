<form action="" method="POST" id="frm">
	<input type="hidden" name="id" id="id" value="{{$input_id}}">
	<div class="row form-group">
		<div class="col-sm-12">
			<label>Subartículo</label>
			{{$input_nombre}}
			<span class="error error_nombre"></span>
		</div>
	</div>
	<div class="row form-group">
		<div class="col-sm-12">
			<label>Artículo</label>
			{{$articulo_id}}
			<span class="error error_articulo_id"></span>
		</div>
	</div>
</form>