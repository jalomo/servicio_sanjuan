<table class="table table-border table-striped">
    <thead>
        <tr>
            <th>#Parte</th>
            <th>Descripción</th>
            <th>Tiempo tabulado</th>
            <th>Cantidad</th>
            <th>Costo precio</th>
            <th>Total</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($detalle_paquete as $d => $detalle)
            <tr>
                <td>{{ $detalle->parte }}</td>
                <td>{{ $detalle->descripcion }}</td>
                <td>
                    {{ $detalle->tiempo_tabulado }}
                    <input type="hidden" class="tiempo_tabulado"
                        value="{{ $detalle->tiempo_tabulado }}">

                </td>
                <td>{{ $detalle->cantidad }}</td>
                <td>
                    $ {{ number_format($detalle->precio_unitario, 2) }}
                    
                </td>
                <td>
                    $ {{number_format($detalle->total_refaccion,2)}}
                    <input type="hidden" class="precio"
                        value="{{ $detalle->total_refaccion }}">
                </td>
                <td>
                    <a href="#" data-id="{{ $detalle->id }}"
                        class="fa fa-edit agregar_articulo" aria-hidden="true"
                        data-toggle="tooltip" data-placement="top" title="Editar"></a>

                    <a href="" data-id="{{ $detalle->id }}"
                        class="fa fa-trash js_eliminar" aria-hidden="true"
                        data-toggle="tooltip" data-placement="top" title="Eliminar"></a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>