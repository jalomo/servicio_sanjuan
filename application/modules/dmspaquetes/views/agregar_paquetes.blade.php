@layout('tema_luna/layout')
<style>
    .dropdown-menu {
        max-height: 500px;
        overflow-y: auto;
        overflow-x: hidden;
    }

</style>
@section('contenido')
    <h2>Datos del paquete</h2>
    <a href="{{site_url('dmspaquetes/listado')}}" class="btn btn-info pull-right">Listado de paquetes</a>
    <br><br>
    <form method="POST" id="frm">
        <input type="hidden" name="id" id="id" value="{{ $id }}">
        <div class="row">
            <div class="col-sm-4">
                <label for="">Nombre</label>
                {{ $nombre }}
                <span class="error error_nombre"></span>
            </div>
            <div class="col-sm-4">
                <label for="">Código</label>
                {{ $drop_claves }}
                <span class="error error_clave_id"></span>
            </div>
            <div class="col-sm-4">
                <label for="">Tipo MO</label>
                {{ $tipo_mo }}
                <span class="error error_tipo_mo"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <label for="">Código MO</label>
                {{ $codigo }}
                <span class="error error_codigo"></span>
            </div>
            <div class="col-sm-4">
                <label for="">Tipo paquete</label>
                {{ $tipo_paquete_id }}
                <span class="error error_tipo_paquete_id"></span>
            </div>
            <div class="col-sm-4">
                <label for="">Tipo precio</label>
                {{ $tipo_precio_id }}
                <span class="error error_tipo_precio_id"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <label for="">Descripción</label>
                {{ $descripcion }}
                <span class="error error_descripcion"></span>
            </div>
            <div class="col-sm-4">
                <label for="">Tiempo facturado</label>
                {{ $tiempo_facturado }}
                <span class="error error_tiempo_facturado"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <label for="">Precio paquete</label>
                {{ $precio }}
                <span class="error error_precio"></span>
            </div>
            @if($id!=0)
                <div class="col-sm-4">
                    <label for="">tiempo tabulado</label>
                    {{ $tiempo_tabulado }}
                    <span class="error error_tiempo_tabulado"></span>
                </div>
                <div class="col-sm-4">
                    <label for="">Total refacciones</label>
                    {{ $total_refacciones }}
                    <span class="error error_total_refacciones"></span>
                </div>
            @endif
        </div>
        @if($id!=0)
        <div class="row">
            <div class="col-sm-4">
                <label for="">Mano de obra</label>
                {{ $mano_obra }}
                <span class="error error_mano_obra"></span>
            </div>
            <div class="col-sm-4">
                <label for="">IVA</label>
                {{ $IVA }}
                <span class="error error_IVA"></span>
            </div>
            <div class="col-sm-4">
                <label for="">TOTAL con IVA</label>
                {{ $precio_iva }}
                <span class="error error_precio_iva"></span>
            </div>
        </div>
        @endif
    </form>
    <div class="row text-right">
        <div class="col-sm-12">
            <button type="button" class="btn btn-success"
                id="guardar">{{ $id == 0 ? 'Guardar paquete' : 'Actualizar paquete' }}</button>
        </div>
    </div>
    @if ($id != 0)
        <hr>
        <h2>Detalles del paquete</h2>
        <div class="row text-right">
            <div class="col-sm-12">
                <button data-id="0" type="button" class="btn btn-info agregar_articulo" id="agregar_articulo"><i
                        class="fa fa-plus"></i>Agregar
                    artículo</button>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-12">
                <div id="div-detalle-paquete">
                    <table class="table table-border table-striped">
                        <thead>
                            <tr>
                                <th>#Parte</th>
                                <th>Descripción</th>
                                <th>Tiempo tabulado</th>
                                <th>Cantidad</th>
                                <th>Costo precio</th>
                                <th>Total</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($detalle_paquete as $d => $detalle)
                                <tr>
                                    <td>{{ $detalle->parte }}</td>
                                    <td>{{ $detalle->descripcion }}</td>
                                    <td>
                                        {{ $detalle->tiempo_tabulado }}
                                        <input type="hidden" class="tiempo_tabulado"
                                            value="{{ $detalle->tiempo_tabulado }}">

                                    </td>
                                    <td>{{ $detalle->cantidad }}</td>
                                    <td>
                                        $ {{ number_format($detalle->precio_unitario, 2) }}

                                    </td>
                                    <td>
                                        $ {{ number_format($detalle->total_refaccion, 2) }}
                                        <input type="hidden" class="precio"
                                            value="{{ $detalle->total_refaccion }}">
                                    </td>
                                    <td>
                                        <a href="#" data-id="{{ $detalle->id }}" class="fa fa-edit agregar_articulo"
                                            aria-hidden="true" data-toggle="tooltip" data-placement="top"
                                            title="Editar"></a>

                                        <a href="" data-id="{{ $detalle->id }}" class="fa fa-trash js_eliminar"
                                            aria-hidden="true" data-toggle="tooltip" data-placement="top"
                                            title="Eliminar"></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <br>
        <hr>
        <h2>Reglas del paquete</h2>
        <form id="frm-reglas" method="POST">
            <input type="hidden" id="regla_id" name="regla_id" value="0">
            <input type="hidden" name="paquete_id" id="paquete_id" value="{{ $id }}">
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Artículo</label>
                    {{ $drop_modelos }}
                    <span class="error error_modelo_id"></span>
                </div>
            </div>
            <div id="div-agregar-editar-reglas">

            </div>
        </form>
        <br>
        <div class="row text-right">
            <div class="col-sm-12">
                <button type="button" class="btn btn-default" id="guardar-reglas">Guardar regla</button>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-12">
                <div id="div-reglas-paquete">
                    <table class="table table-border table-striped">
                        <thead>
                            <tr>
                                <th># Regla</th>
                                <th>Artículos</th>
                                <th>Subartículos</th>
                                <th>Modelos</th>
                                <th>Cilindros</th>
                                <th>Transmisiones</th>
                                <th>Motores</th>
                                <th>Combustibles</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($reglas_paquete as $d => $regla)
                                <tr>
                                    <td>{{ $regla->numero_regla }}</td>
                                    <td>
                                        {{ $this->mp->getText($regla->modelo_id, $modelos, 'modelo') }}
                                    </td>
                                    <td>
                                        {{ $this->mp->getText($regla->subarticulos, $subarticulos, 'nombre') }}
                                    </td>
                                    <td>
                                        {{ $regla->anios }}
                                    </td>
                                    <td>
                                        {{ $regla->cilindros }}
                                    </td>
                                    <td>
                                        {{ $this->mp->getText($regla->transmisiones, $transmisiones, 'transmision') }}
                                    </td>
                                    <td>
                                        {{ $this->mp->getText($regla->motores, $motores, 'motor') }}
                                    </td>
                                    <td>
                                        {{ $this->mp->getText($regla->combustibles, $combustibles, 'combustible') }}
                                    </td>
                                    <td>
                                        <a href="#" data-id="{{ $regla->id }}" data-modeloid="{{ $regla->modelo_id }}"
                                            class="fa fa-edit editar_regla" aria-hidden="true" data-toggle="tooltip"
                                            data-placement="top" title="Editar"></a>

                                        <a href="" data-id="{{ $regla->id }}" class="fa fa-trash js_eliminar_regla"
                                            aria-hidden="true" data-toggle="tooltip" data-placement="top"
                                            title="Eliminar"></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('scripts')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script>
        var site_url = "{{ site_url() }}";
        var id = "{{ $id }}";
        var tabla = '';
        var campo = '';
        let detalle_id = '';
        let regla_id = '';
        var aPos = '';
        
        $(".busqueda").select2();
        $('.multiple').multiselect({
            enableFiltering: true,
            enableFullValueFiltering: true,
            buttonWidth: '100%',
            includeSelectAllOption: true,
            selectAllJustVisible: false,
        });

        $("#guardar").on('click', agregarRegistro);
        $("#guardar-reglas").on('click', agregarRegla);
        $("#clave_id").on('change', getClave);
        $("#modelo_id").on('change', agregarReglasPaquete);
        $("body").on('click', '.agregar_articulo', function(e) {
            e.preventDefault();
            const id = $(this).data('id');
            var url = site_url + "/dmspaquetes/asignarDetallePaquete";
            customModal(url, {
                id,
                paquete_id: $("#id").val(),
            }, "GET", "lg", SaveDetalleArticulo, "", "Guardar", "Cancelar", "Guardar artículo ", "modal1");
        });
        //Eliminar paquete
        $("body").on("click", '.js_eliminar', function(e) {
            e.preventDefault();
            detalle_id = $(this).data('id')
            ConfirmCustom("¿Está seguro de eliminar ?", callbackEliminarDetalle, "", "Confirmar",
                "Cancelar");
        });
        // Eliminar regla paquete 
        $("body").on("click", '.js_eliminar_regla', function(e) {
            e.preventDefault();
            regla_id = $(this).data('id')
            ConfirmCustom("¿Está seguro de eliminar ?", callbackEliminarRegla, "", "Confirmar",
                "Cancelar");
        });
        //Editar regla del paquete
        $("body").on("click", '.editar_regla', function(e) {
            e.preventDefault();
            regla_id = $(this).data('id');
            modeloid = $(this).data('modeloid');
            $("#regla_id").val($(this).data('id'));
            $("#modelo_id").select2('destroy');
            $("#modelo_id").val(modeloid);
            $("#modelo_id").select2();
            agregarReglasPaquete();

        });

        function callbackEliminarDetalle() {
            var url = site_url + "/dmspaquetes/eliminar_detalle_articulo/";
            ajaxJson(url, {
                "detalle_id": detalle_id
            }, "POST", "", function(result) {
                if (result == 0) {
                    ErrorCustom('No se pudo eliminar el artículo, por favor intenta de nuevo');
                } else {
                    ExitoCustom("Artículo eliminado correctamente", function() {
                        buscarDetallePaquete();
                    });
                }
            });
        }

        function callbackEliminarRegla() {
            var url = site_url + "/dmspaquetes/eliminar_regla_paquete/";
            ajaxJson(url, {
                regla_id,
            }, "POST", "", function(result) {
                if (result == 0) {
                    ErrorCustom('No se pudo eliminar la regla, por favor intenta de nuevo');
                } else {
                    ExitoCustom("Regla eliminada correctamente", function() {
                        buscarReglasPaquete();
                    });
                }
            });
        }

        function SaveDetalleArticulo() {
            const url = site_url + "/dmspaquetes/asignarDetallePaquete";
            ajaxJson(url, $("#frm-detalle-articulos").serialize(), "POST", "async", function(result) {
                result = JSON.parse(result);
                if (isNaN(result)) {
                    //Se recorre el json y se coloca el error en la div correspondiente
                    $.each(result, function(i, item) {
                        $(".error_" + i).empty();
                        $(".error_" + i).append(item);
                        $(".error_" + i).css("color", "red");
                    });
                } else {
                    if (result == 0) {
                        ErrorCustom("Error al guardar, por favor intentalo otra vez.");
                    } else if (result == -1) {
                        ErrorCustom("Ya fue asignado el artículo al paquete");
                    } else {
                        ExitoCustom("Paquete agregado correctamente", function() {
                            $(".modal1").modal('hide')
                            buscarDetallePaquete();
                        });
                    }
                }
            });
        }

        function agregarRegistro() {
            var url = site_url + "/dmspaquetes/agregar";
            ajaxJson(url, $("#frm").serialize(), "POST", "async", function(result) {
                result = JSON.parse(result);
                if (isNaN(result)) {
                    //Se recorre el json y se coloca el error en la div correspondiente
                    $.each(result, function(i, item) {
                        $(".error_" + i).empty();
                        $(".error_" + i).append(item);
                        $(".error_" + i).css("color", "red");
                    });
                } else {
                    if (result == 0) {
                        ErrorCustom("Error al guardar, por favor intentalo otra vez.");
                    } else {
                        ExitoCustom("Paquete agregado correctamente", function() {
                            location.href = site_url + '/dmspaquetes/agregar/' + result;
                        });
                    }
                }
            });
        }

        function agregarRegla() {
            var url = site_url + "/dmspaquetes/agregarRegla";
            ajaxJson(url, $("#frm-reglas").serialize(), "POST", "async", function(result) {
                result = JSON.parse(result);
                if (isNaN(result)) {
                    //Se recorre el json y se coloca el error en la div correspondiente
                    $.each(result, function(i, item) {
                        $(".error_" + i).empty();
                        $(".error_" + i).append(item);
                        $(".error_" + i).css("color", "red");
                    });
                } else {
                    if (result == 0) {
                        ErrorCustom("Error al guardar, por favor intentalo otra vez.");
                    } else if (result == -1) {
                        ErrorCustom("Ya fue asignado el artículo en el paquete");
                    } else if (result == -2) {
                        ErrorCustom("Ya fue asignado el artículo y el servicio en otro paquete");
                    } else {
                        ExitoCustom("Regla agregada correctamente", function() {
                            $("#regla_id").val(0);
                            buscarReglasPaquete();
                        });
                    }

                }
            });
        }

        function buscarDetallePaquete() {
            var url = site_url + "/dmspaquetes/getDetallePaquete";
            ajaxLoad(url, {
                "paquete_id": $("#id").val(),
            }, "div-detalle-paquete", "POST", function() {
                actualizarPrecio();
                actualizarTiempoTabulado();
                updateAlltotal();
            });
        }

        function buscarReglasPaquete() {
            var url = site_url + "/dmspaquetes/getReglasPaquete";
            ajaxLoad(url, {
                "paquete_id": $("#id").val(),
            }, "div-reglas-paquete", "POST", function() {
                $("#div-agregar-editar-reglas").empty();
                $("#modelo_id").select2('destroy');
                $("#modelo_id").val('');
                $("#modelo_id").select2();
            });
        }

        function agregarReglasPaquete() {
            if ($("#modelo_id").val() == '' && $("#regla_id").val() == 0) {
                return;
            }
            getFormReglasPaquetes();
        }

        function getFormReglasPaquetes() {
            var url = site_url + "/dmspaquetes/v_regla_paquete";
            ajaxLoad(url, {
                paquete_id: $("#id").val(),
                articulo_id: $("#modelo_id").val(),
                regla_id: $("#regla_id").val(),
            }, "div-agregar-editar-reglas", "POST", function() {
                $('.multiple').multiselect({
                    enableFiltering: true,
                    enableFullValueFiltering: true,
                    buttonWidth: '100%',
                    includeSelectAllOption: true,
                    selectAllJustVisible: false,
                });

            });
        }

        function actualizarPrecio() {
            let suma = 0;
            $(".precio").each((i, item) => {
                suma = suma + parseFloat($(item).val());
            });
            $("#total_refacciones").val(suma.toFixed(2));
        }

        function actualizarTiempoTabulado() {
            let tiempo = 0;
            $(".tiempo_tabulado").each((i, item) => {
                tiempo = tiempo + parseFloat($(item).val());
            });
            $("#tiempo_tabulado").val(tiempo.toFixed(2));
        }

        function getClave() {
            var url = site_url + "/dmspaquetes/getClave";
            let clave_id = $("#clave_id").val();
            if (!clave_id) {
                return;
            }
            ajaxJson(url, {
                clave_id
            }, "POST", "async", function(result) {
                result = JSON.parse(result);
                $("#codigo").val(result.codigo)
                $("#tipo_mo").val(result.tipo_mo)
                $("#descripcion").val(result.descripcion)
            });
        }
        function updateAlltotal(){
            let precio = parseFloat($("#precio").val());
            let total_refacciones = parseFloat($("#total_refacciones").val());
            let mano_obra = parseFloat($("#mano_obra").val());
            let IVA = precio*.16;
            let precio_iva = precio + IVA;
            let mo = precio-total_refacciones;
            $("#mano_obra").val(mo.toFixed(2));
            $("#IVA").val(IVA.toFixed(2));
            $("#precio_iva").val(precio_iva.toFixed(2));
            
        }
        if(id!=0){
            updateAlltotal();
        }
    </script>
@endsection
