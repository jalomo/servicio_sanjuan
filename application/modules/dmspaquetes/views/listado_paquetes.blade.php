@layout('tema_luna/layout')
<style>
    .dropdown-menu {
        max-height: 500px;
        overflow-y: auto;
        overflow-x: hidden;
    }

</style>
@section('contenido')
    <h1>Listado de paquetes</h1>
    <div class="row pull-right">
        <div class="col-sm-12">
            <a href="{{base_url('dmspaquetes/agregar')}}" class="btn btn-success">Agregar paquete</a>
        </div>
    </div>
    <br><br><br>
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-bordered table-striped table-striped" id="tbl_paquetes" width="100%" cellspacing="0">
                <thead>
                    <tr class="tr_principal">
                        <th>ID</th>
                        {{-- <th>Usuario creó</th> --}}
                        <th>Nombre</th>
                        <th>Artículos</th>
                        <th>Subartículos</th>
                        <th>Descripción</th>
                        <th>Precio</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($paquetes as $p => $value)
                        <tr>
                            <td>{{ $value->id }}</td>
                            <td>{{ $value->nombre }}</td>
                            <td>
                                {{$this->mp->getArtFromPackage($value->id)}}
                            </td>
                            <td>    
                                {{ $this->mp->getModelosTextReglasPaquete($value->id)}}
                            </td>
                            <td>{{ $value->descripcion }}</td>
                            <td>{{ $value->precio }}</td>
                            <td>
                                <a class="" href="{{ base_url('dmspaquetes/agregar/' . $value->id) }}"> <i
                                        class="pe pe-7s-note"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        var site_url = "{{ site_url() }}";
        var aPos = '';
        var id = '';
        inicializar_tabla("#tbl_paquetes");
    </script>
@endsection
