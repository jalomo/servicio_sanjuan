<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_dmspaquetes extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
  }
  //obtener todos los paquetes
  public function getAllPack()
  {
    return $this->db->get('pkt_paquetes')->result();
  }
  public function getPackageHistory($total = false)
  {
    $datos = $_POST;
    $pagina = $datos['start'];
    $porpagina = $datos['length'];

    if ($_POST['finicio'] != '') {
      $this->db->where('date(created_at) >=', date2sql($_POST['finicio']));
    }
    if ($_POST['ffin'] != '') {
      $this->db->where('date(created_at) <=', date2sql($_POST['ffin']));
    }
    if ($_POST['buscar_campo'] != '') {
      $busqueda = $_POST['buscar_campo'];
      $this->db->like('nombre', $busqueda);
    }

    if ($total) {
      $this->db->select('count(id) as total');
    } else {
      $this->db->select('*');
      $this->db->limit($porpagina, $pagina);
    }
    $info = $this->db
      ->get('pkt_paquetes');

    if ($total) {
      return $info->row()->total;
    } else {
      return $info->result();
    }
  }
  //guarda los operadores
  public function guardarPaquete()
  {
    $id = $this->input->post('id');
    $datos = array(
      'nombre' => $this->input->post('nombre'),
      'clave_id' => $this->input->post('clave_id'),
      'descripcion' => $this->input->post('descripcion'),
      'tipo_paquete_id' => $this->input->post('tipo_paquete_id'),
      'tipo_precio_id' => $this->input->post('tipo_precio_id'),
      'precio' => $this->input->post('precio'),
      'total_refacciones' => $this->input->post('total_refacciones'),
      'mano_obra' => $this->input->post('mano_obra'),
      'IVA' => $this->input->post('IVA'),
      'precio_iva' => $this->input->post('precio_iva'),
      'tiempo_facturado' => $this->input->post('tiempo_facturado'),
      'tiempo_tabulado' => $this->input->post('tiempo_tabulado'),
      'usuario_id' => $this->session->userdata('id_usuario'),
    );
    if ($id == 0) {
      $datos['created_at'] = date('Y-m-d H:i:s');
      $exito = $this->db->insert('pkt_paquetes', $datos);
      $id = $this->db->insert_id();
    } else {
      $datos['updated_at'] = date('Y-m-d H:i:s');
      $exito = $this->db->where('id', $id)->update('pkt_paquetes', $datos);
    }
    if ($exito) {
      echo $id;
    } else {
      echo 0;
    }
    exit();
  }
  public function guardarDetallePaquete()
  {
    $id = $this->input->post('art_id');
    if (!$this->validarArticuloPaquete($this->input->post('articulo_id'), $this->input->post('paquete_id'), $id)) {
      echo -1;
      exit();
    }
    $datos = array(
      'articulo_id' => $this->input->post('articulo_id'),
      'parte' => $this->input->post('parte'),
      'cantidad' => round($this->input->post('cantidad'), 2),
      'precio_unitario' => round($this->input->post('precio_unitario'), 2),
      'tiempo_tabulado' => round($this->input->post('tiempo_tabulado'), 2),
      'total_refaccion' => round($this->input->post('total_refaccion'), 2),
      'precio_calculado' => (round($this->input->post('precio_calculado'), 2)) ? 1 : 0,
      'paquete_id' => $this->input->post('paquete_id'),
    );
    if ($id == 0) {
      $datos['created_at'] = date('Y-m-d H:i:s');
      $exito = $this->db->insert('pkt_detalle_paquete', $datos);
      $id = $this->db->insert_id();
    } else {
      $datos['updated_at'] = date('Y-m-d H:i:s');
      $exito = $this->db->where('id', $id)->update('pkt_detalle_paquete', $datos);
    }
    if ($exito) {
      echo $id;
    } else {
      echo 0;
    }
    exit();
  }
  public function getPaqueteId($id = 0)
  {
    return $this->db->where('id', $id)->get('pkt_paquetes')->result();
  }
  //Obtener el detalle del paquete
  public function getDetallePaquete($paquete_id = '')
  {
    return $this->db
      ->where('paquete_id', $paquete_id)
      ->where('dp.activo', 1)
      ->select('dp.*,a.descripcion')
      ->join('pkt_articulos a', 'dp.articulo_id = a.id')
      ->get('pkt_detalle_paquete dp')
      ->result();
  }
  //Obtener reglas paquete
  public function getReglasPaquete($paquete_id = '')
  {
    return $this->db->where('paquete_id', $paquete_id)
      ->order_by('numero_regla', 'asc')
      ->get('pkt_reglas_paquetes')
      ->result();
  }
  //Guardar reglas
  public function guardarReglas()
  {
    $regla_id = $_POST['regla_id'];
    $paquete_id = $_POST['paquete_id'];
    if (!$this->validarReglaPaquete($_POST['modelo_id'], $paquete_id, $regla_id)) {
      echo -1;
      exit();
    }

    $clave_id = $this->principal->getGeneric('id', $paquete_id, 'pkt_paquetes', 'clave_id');
    if (
        !$this->validarReglaOtrosPaquetes(
          $_POST['modelo_id'], $paquete_id, 
          $clave_id, isset($_POST['subarticulos']) ? implode(",", $_POST['subarticulos']) : '',
          isset($_POST['combustible_id']) ? implode(",", $_POST['combustible_id']) : '',
          isset($_POST['anios']) ? implode(",", $_POST['anios']) : ''
          ) 
    
    ) {
      echo -2;
      exit();
    }


    $data = [
      'paquete_id' => $_POST['paquete_id'],
      'modelo_id' => $_POST['modelo_id'],
      'anios' => implode(",", $_POST['anio']),
      'cilindros' => implode(",", $_POST['cilindro_id']),
      'transmisiones' => implode(",", $_POST['transmision_id']),
      'motores' => implode(",", $_POST['motor_id']),
      'combustibles' => implode(",", $_POST['combustible_id']),
      'subarticulos' => isset($_POST['subarticulos']) ? implode(",", $_POST['subarticulos']) : '',
      'usuario_id' => $this->session->userdata('id_usuario'),
    ];
    if ($regla_id == 0) {
      //Está guardando
      $numero_regla = $this->getLastRule($paquete_id);
      $data['numero_regla'] = $numero_regla;
      $data['created_at'] = date('Y-m-d H:i:s');
      $this->db->insert('pkt_reglas_paquetes', $data);
      echo $this->db->insert_id();
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $this->db->where('id', $regla_id)->update('pkt_reglas_paquetes', $data);
      echo $regla_id;
    }
    exit();
  }
  //Obtener el # de la última regla por paquete
  public function getLastRule($paquete_id = '')
  {
    $query = $this->db->where('paquete_id', $paquete_id)
      ->select('max(numero_regla) as regla')
      ->get('pkt_reglas_paquetes');
    if ($query->num_rows() == 1) {
      return (int)($query->row()->regla) + 1;
    } else {
      return 1;
    }
  }
  //Actualizar campos calculados del paquete sobre el detalle
  public function updateDataPaquete($paquete_id = '')
  {
    $detalles = $this->db->where('paquete_id', $paquete_id)->get('pkt_detalle_paquete')->result();
    $data_paquete = $this->principal->getGeneric('id', $paquete_id, 'pkt_paquetes');
    $suma_tiempo_tabulado = 0;
    $suma_precio_unitario = 0;
    foreach ($detalles as $d => $detalle) {
      $suma_tiempo_tabulado = $suma_tiempo_tabulado + (float)$detalle->tiempo_tabulado;
      $suma_precio_unitario = $suma_precio_unitario + (float)$detalle->total_refaccion;
    }
    $mano_obra = (float)$data_paquete->precio - (float)$suma_precio_unitario;
    $IVA = (float)$data_paquete->precio * .16;
    $precio_iva = (float)$data_paquete->precio + (float)$IVA;
    $this->db->where('id', $paquete_id)
      ->set('tiempo_tabulado', round($suma_tiempo_tabulado, 2))
      ->set('mano_obra', round($mano_obra, 2))
      ->set('IVA', round($IVA, 2))
      ->set('precio_iva', round($precio_iva, 2))
      ->set('total_refacciones', round($suma_precio_unitario, 2))
      ->update('pkt_paquetes');
  }
  //Actualizar totales del paquete
  public function updateTotalPaquete($paquete_id = '')
  {
    $q = $this->db->where('paquete_id', $paquete_id)->get('pkt_paquetes');
    if ($q->num_rows() == 1) {
      $precio = (float)$q->row()->precio;
      $total_refacciones = (float)$q->row()->total_refacciones;
      $mano_obra = (float)$q->row()->mano_obra;
      $IVA = $precio * .16;
      $precio_iva = $precio + $IVA;
      $data_update = [
        'precio' => round($precio, 2),
        'total_refacciones' => round($total_refacciones, 2),
        'mano_obra' => round($mano_obra, 2),
        'IVA' => round($IVA, 2),
        'precio_iva' => round($precio_iva, 2),
      ];
      $this->db->where('paquete_id', $paquete_id)->update('pkt_paquetes', $data_update);
    }
  }
  //Validar si ya existe el artículo en el 
  public function validarArticuloPaquete($articulo_id = '', $paquete_id = '', $detalle_id = '')
  {
    $data = $this->db
      ->where('articulo_id', $articulo_id)
      ->where('paquete_id', $paquete_id)
      ->where('id !=', $detalle_id)
      ->get('pkt_detalle_paquete')
      ->result();
    if (count($data) > 0) {
      return false;
    }
    return true;
  }
  //Validar si ya existe el artículo en alguna regla en el mismo paquete
  public function validarReglaPaquete($modelo_id = '', $paquete_id = '', $regla_id = '')
  {
    $data = $this->db
      ->where('modelo_id', $modelo_id)
      ->where('paquete_id', $paquete_id)
      ->where('id !=', $regla_id)
      ->get('pkt_reglas_paquetes')
      ->result();
    if (count($data) > 0) {
      return false;
    }
    return true;
  }
  //Validar si ya existe el artículo en otros paquetes
  public function validarReglaOtrosPaquetes($modelo_id = '', $paquete_id = '', $clave_id = '', $subarticulos = '',$combustible = '',$anios = '')
  {
   
    $data = $this->db
      ->where('r.modelo_id', $modelo_id)
      ->where('p.clave_id', $clave_id)
      ->where('paquete_id !=', $paquete_id)
      ->join('pkt_reglas_paquetes r', 'p.id = r.paquete_id')
      ->get('pkt_paquetes p')
      ->result();

    if (count($data) > 0) {
      if ($subarticulos == '' && $combustible == '' && $anios == '') {
        return false;
      } else {
        // Recorrer para revisar si existen los subarticulos en otro paquete
        foreach ($data as $d => $regla) {
          $array_subarticulos = explode(',', $regla->subarticulos);
          $array_combustibles = explode(',', $regla->combustibles);
          $array_anios = explode(',', $regla->anios);
          $existSubarticulos = count(array_intersect(explode(',', $subarticulos), $array_subarticulos));
          $existCombustibles = count(array_intersect(explode(',', $combustible), $array_combustibles));
          $existAnios = count(array_intersect(explode(',', $anios), $array_anios));
          if ($existSubarticulos > 0 && $existCombustibles > 0 && $existAnios > 0) {
            return false;
          }
        }
      }
    }
    return true;
  }
  //obtener los subartículos
  public function getSubarticulos($subarticulo_id = '')
  {
    $subarticulos = $this->db->where('id', $subarticulo_id)->get('pkt_subarticulos')->result();
    $cadena = '';
    foreach ($subarticulos as $key => $subarticulo) {
      $cadena = $cadena . $subarticulo->nombre . ',';
    }
    $cadena = substr($cadena, 0, -1);
    return $cadena;
  }
  public function getText($cadena, $array, $campo = '')
  {
    //parámetros la cadena de datos separada por comas
    // array donde va tomar el texto
    $datos = explode(',', $cadena);
    $cadena_return = '';
    foreach ($datos as $dato) {
      foreach ($array as $key => $value) {
        //Revisar si existe la propiedad en el array y regresar el texto
        if ($value->id == $dato) {
          $cadena_return = $cadena_return . $value->$campo . ',';
          break;
        }
      }
    }
    return substr($cadena_return, 0, -1);
  }
  // Obtener a que paquete pertenece lo seleccionado de la operación
  public function getPaqueteByData()
  {
    $clave_id = $_POST['clave_id'];
    $modelo_id = $_POST['modelo_id'];
    $anio = $_POST['anio'];
    $cilindros = $_POST['cilindros'];
    $transmision = $_POST['transmision'];
    $motor = $_POST['motor'];
    $combustible = $_POST['combustible'];
    $submodelo_id = $_POST['submodelo_id'];

   
    $reglas_paquetes = $this->db->where('r.modelo_id', $modelo_id)
      ->where('r.activo', 1)
      ->where('p.activo', 1)
      ->where('p.clave_id', $clave_id)
      ->join('pkt_paquetes p', 'r.paquete_id = p.id')
      ->select('r.*')
      ->get('pkt_reglas_paquetes r')
      ->result();
    $paquete_id = 0;
    if (count($reglas_paquetes) > 0) {
      foreach ($reglas_paquetes as $r => $regla) {
        $array_anios = explode(',', $regla->anios);
        $array_cilindros = explode(',', $regla->cilindros);
        $array_transmisiones = explode(',', $regla->transmisiones);
        $array_motores = explode(',', $regla->motores);
        $array_combustibles = explode(',', $regla->combustibles);
        $array_submodelos = explode(',', $regla->subarticulos);
        // Validar si coincide en todos los campos
        if (
          is_numeric(array_search($anio, $array_anios)) &&
          // is_numeric(array_search($cilindros, $array_cilindros)) &&
          // is_numeric(array_search($transmision, $array_transmisiones)) &&
          is_numeric(array_search($motor, $array_motores)) 
          // is_numeric(array_search($combustible, $array_combustibles))
        ) {
         
          if ($submodelo_id > 0) {
            if (is_numeric(array_search($submodelo_id, $array_submodelos))) {
              $paquete_id = $regla->paquete_id;
            }
          } else {
            $paquete_id = $regla->paquete_id;
          }
          break;
        }
      }
      return $paquete_id;
    } else {
      return $paquete_id;
    }
  }
  // Get details from package
  public function getArtFromPackage($paquete_id = '')
  {
    $articulos =  $this->db->where('r.paquete_id', $paquete_id)
      ->join('cat_modelo m', 'r.modelo_id = m.id')
      ->select('m.modelo')
      ->get('pkt_reglas_paquetes r')
      ->result();

    $cadena = '';
    foreach ($articulos as $key => $articulo) {
      $cadena = $cadena . $articulo->modelo . ',';
    }
    $cadena = substr($cadena, 0, -1);
    return $cadena;
  }
  public function getModelosTextReglasPaquete($paquete_id)
  {
    $reglas = $this->getReglasPaquete($paquete_id);
    if (count($reglas) == 0) {
      return '';
    }
    $cadena_reglas = '';
    foreach ($reglas as $r => $regla) {
      $cadena_reglas = $cadena_reglas . $regla->subarticulos . ',';
    }
    $cadena_reglas = substr($cadena_reglas, 0, -1);
    $subarticulos = $this->db->get('pkt_subarticulos')->result();
    return $this->mp->getText($cadena_reglas, $subarticulos, 'nombre');
  }
  //Catálogos
  public function guardarCatalogo()
  {
    $id = $this->input->post('id');
    $tabla = $_POST['tabla'];
    $campo = $_POST['campo'];
    $datos = array(
      $campo => $this->input->post($campo)
    );
    $q = $this->db->where($campo, $datos[$campo])->where('id !=', $id)->get($tabla);
    if ($q->num_rows() == 1) {
      echo -1;
      exit();
    }
    if ($id == 0) {
      $exito = $this->db->insert($tabla, $datos);
    } else {
      $exito = $this->db->where('id', $id)->update($tabla, $datos);
    }
    if ($exito) {
      echo 1;
    } else {
      echo 0;
    }
    exit();
  }
  public function guardarSubarticulo()
  {
    $id = $this->input->post('id');
    $datos = array(
      'nombre' => $this->input->post('nombre'),
      'articulo_id' => $this->input->post('articulo_id'),
    );
    $q = $this->db->where('nombre', $datos['nombre'])->where('id !=', $id)->get('pkt_subarticulos');
    if ($q->num_rows() == 1) {
      echo -1;
      exit();
    }
    if ($id == 0) {
      $exito = $this->db->insert('pkt_subarticulos', $datos);
    } else {
      $exito = $this->db->where('id', $id)->update('pkt_subarticulos', $datos);
    }
    if ($exito) {
      echo 1;
    } else {
      echo 0;
    }
    exit();
  }
  public function guardarClave()
  {
    $id = $this->input->post('id');
    $datos = array(
      'clave' => $this->input->post('clave'),
      'codigo' => $this->input->post('codigo'),
      'tipo_mo' => $this->input->post('tipo_mo'),
      'descripcion' => $this->input->post('descripcion'),
    );
    $q = $this->db->where('clave', $datos['clave'])->where('id !=', $id)->get('pkt_claves');
    if ($q->num_rows() == 1) {
      echo -1;
      exit();
    }
    if ($id == 0) {
      $exito = $this->db->insert('pkt_claves', $datos);
    } else {
      $exito = $this->db->where('id', $id)->update('pkt_claves', $datos);
    }
    if ($exito) {
      echo 1;
    } else {
      echo 0;
    }
    exit();
  }
  //Actualizar el precio y mano de obra del paquete
  // parámetro opcional paquete_id
  public function PreciosPaquetes($paquete_id=''){
    if($paquete_id != ''){
      $this->db->where('id',$paquete_id);
    }
    $paquetes = $this->db->get('pkt_paquetes')->result();
    foreach($paquetes as $p => $paquete){
      $precio = (float)$paquete->precio;
      //Obtener el total de las refacciones
      $total_refacciones = $this->updateManoObra($paquete->id);
      // $total_refacciones = (float)$paquete->total_refacciones;
      $IVA = (float)$precio * .16;
      $precio_iva = (float)$precio + (float)$IVA;
      $mano_obra  = (float)$precio - (float)$total_refacciones;

      $datos_update = [
        'IVA' => $IVA,
        'precio_iva' => $precio_iva,
        'mano_obra' => $mano_obra
      ];

      $array_compare_mo = [];
      if($mano_obra != $paquete->mano_obra){
        $array_compare_mo[] = $paquete->id;
      }

      $array_compare_precio_iva = [];
      if($precio_iva != $paquete->precio_iva){
        $array_compare_precio_iva[] = $paquete->id;
      }
      $this->db->where('id',$paquete->id)->update('pkt_paquetes',$datos_update);
    }
  }
  //Actualizar solo mano de obra
  public function updateManoObra($paquete_id){
    $total = 0;
    $detalle = $this->db->where('paquete_id',$paquete_id)->select('IFNULL(SUM(total_refaccion),0) as total_refaccion')->get('pkt_detalle_paquete')->row();
    $total = (float)$total + (float)$detalle->total_refaccion; 
    $this->db->where('id',$paquete_id)->set('total_refacciones',$total)->update('pkt_paquetes');
    return (float)$total;
  }
}
