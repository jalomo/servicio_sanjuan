<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dmspaquetes extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_dmspaquetes', 'mp');
    $this->load->model('principal');
    $this->load->model('citas/m_catalogos', 'mcat');
  }
  // public function listado()
  // {
  //   $this->blade->render('listado_paquetes_ajax');
  // }
  public function listado()
  {
    $data['paquetes'] = $this->mp->getAllPack();
    $this->blade->render('listado_paquetes', $data);
  }
  public function getDatosPaquetes()
  {

    $datos = $_POST;
    $mismo = $datos['draw'];
    $pagina = $datos['start'];
    $porpagina = $datos['length'];
    $total = $this->mp->getPackageHistory(true);
    $info = $this->mp->getPackageHistory(false);
    $data = array();
    foreach ($info as $key => $value) {
      $dat = array();

      $dat[] = $value->id;
      $dat[] = $value->nombre;
      $dat[] = $this->mp->getArtFromPackage($value->id);
      $dat[] = $this->mp->getModelosTextReglasPaquete($value->id);
      $dat[] = $value->descripcion;
      $dat[] = number_format($value->precio, 2);
      $dat[] = date_eng2esp($value->created_at);
      $acciones = '';
      $acciones .= '<a class="" href="' . base_url('dmspaquetes/agregar/' . $value->id) . '"> <i class="pe pe-7s-note"></i></a>';
      $dat[] = $acciones;
      $data[] = $dat;
    } //foreach

    $retornar = array(
      'draw' => intval($mismo),
      'recordsTotal' => intval($total),
      'recordsFiltered' => intval($total),
      'data' => $data
    );
    $this->output->set_content_type('application/json')->set_output(json_encode($retornar));
  }
  public function agregar($id = 0)
  {
    if ($this->input->is_ajax_request() && $this->session->userdata('id_usuario') == '') {
      $this->output->set_status_header('409');
      exit();
    } else if ($this->session->userdata('id_usuario') == '') {
      redirect('login');
    }

    if ($this->input->post()) {
      $this->form_validation->set_rules('nombre', 'nombre', 'trim|required');
      $this->form_validation->set_rules('clave_id', 'clave planta', 'trim|required');
      $this->form_validation->set_rules('descripcion', 'descripción', 'trim|required');
      $this->form_validation->set_rules('tipo_paquete_id', 'tipo paquete', 'trim|required');
      $this->form_validation->set_rules('tipo_precio_id', 'tipo precio', 'trim|required');
      $this->form_validation->set_rules('precio', 'precio', 'trim|required|numeric');
      $this->form_validation->set_rules('total_refacciones', 'total refacciones', 'trim|numeric');
      $this->form_validation->set_rules('mano_obra', 'mano de obra', 'trim|numeric');
      $this->form_validation->set_rules('IVA', 'IVA', 'trim|numeric');
      $this->form_validation->set_rules('precio_iva', 'precio con IVA', 'trim|numeric');
      $this->form_validation->set_rules('tiempo_facturado', 'tiempo facturado', 'trim|required|numeric');
      $this->form_validation->set_rules('tiempo_tabulado', 'tiempo tabulado', 'trim|numeric');

      if ($this->form_validation->run()) {
        $this->mp->guardarPaquete();
      } else {
        $errors = array(
          'nombre' => form_error('nombre'),
          'clave_id' => form_error('clave_id'),
          'descripcion' => form_error('descripcion'),
          'tipo_paquete_id' => form_error('tipo_paquete_id'),
          'tipo_precio_id' => form_error('tipo_precio_id'),
          'precio' => form_error('precio'),
          'total_refacciones' => form_error('total_refacciones'),
          'mano_obra' => form_error('mano_obra'),
          'IVA' => form_error('IVA'),
          'precio_iva' => form_error('precio_iva'),
          'tiempo_facturado' => form_error('tiempo_facturado'),
          'tiempo_tabulado' => form_error('tiempo_tabulado'),
        );
        echo json_encode($errors);
        exit();
      }
    }

    if ($id == 0) {
      $info = new Stdclass();
      $info_claves = new Stdclass();
      $detalle_paquete = [];
      $reglas_paquete = [];
    } else {
      $info = $this->mp->getPaqueteId($id);
      $info = $info[0];
      $info_claves = $this->mcat->get('pkt_claves', '', '', array('id' => $info->clave_id));
      $info_claves = $info_claves[0];
      $detalle_paquete = $this->mp->getDetallePaquete($id);
      $reglas_paquete = $this->mp->getReglasPaquete($id);
    }

    $data['nombre'] = form_input('nombre', set_value('nombre', exist_obj($info, 'nombre')), 'class="form-control" id="nombre"');

    $claves = $this->db->order_by('LENGTH(clave)', 'asc')->get('pkt_claves')->result();
    $data['drop_claves'] = form_dropdown('clave_id', array_combos($claves, 'id', 'clave', TRUE, FALSE, TRUE), set_value('clave_id', exist_obj($info, 'clave_id')), 'class="form-control busqueda" id="clave_id"');

    $data['codigo'] = form_input('codigo', set_value('codigo', exist_obj($info_claves, 'codigo')), 'class="form-control" readonly id="codigo"');

    $data['tipo_mo'] = form_input('tipo_mo', set_value('tipo_mo', exist_obj($info_claves, 'tipo_mo')), 'class="form-control numeric" readonly id="tipo_mo"');

    $data['descripcion'] = form_textarea('descripcion', set_value('descripcion', exist_obj($info, 'descripcion')), 'class="form-control" id="descripcion"');

    $data['tipo_paquete_id'] = form_dropdown('tipo_paquete_id', array_combos($this->mcat->get('pkt_cat_tipo_paquetes', 'tipo_paquete'), 'id', 'tipo_paquete', TRUE), set_value('tipo_paquete_id', exist_obj($info, 'tipo_paquete_id')), 'class="form-control" id="tipo_paquete_id"');

    $data['tipo_precio_id'] = form_dropdown('tipo_precio_id', array_combos($this->mcat->get('pkt_cat_tipo_precio', 'tipo_precio'), 'id', 'tipo_precio', TRUE), set_value('tipo_precio_id', exist_obj($info, 'tipo_precio_id')), 'class="form-control" id="tipo_precio_id"');

    $data['precio'] = form_input('precio', set_value('precio', exist_obj($info, 'precio')), 'class="form-control numeric" id="precio"');

    $val_total_refacciones = set_value('total_refacciones', exist_obj($info, 'total_refacciones'));
    $data['total_refacciones'] = form_input('total_refacciones', $val_total_refacciones ? $val_total_refacciones : 0, 'class="form-control numeric" readonly id="total_refacciones"');

    $val_mano_obra = set_value('mano_obra', exist_obj($info, 'mano_obra'));
    $data['mano_obra'] = form_input('mano_obra', $val_mano_obra ? $val_mano_obra : 0, 'class="form-control numeric" readonly id="mano_obra"');

    $val_IVA = set_value('IVA', exist_obj($info, 'IVA'));
    $data['IVA'] = form_input('IVA', $val_IVA ? $val_IVA : 0, 'class="form-control numeric" readonly id="IVA"');

    $val_precio_iva = set_value('precio_iva', exist_obj($info, 'precio_iva'));
    $data['precio_iva'] = form_input('precio_iva', $val_precio_iva ? $val_precio_iva : 0, 'class="form-control numeric" readonly id="precio_iva"');

    $data['tiempo_facturado'] = form_input('tiempo_facturado', set_value('tiempo_facturado', exist_obj($info, 'tiempo_facturado')), 'class="form-control numeric" id="tiempo_facturado"');

    $data['tiempo_tabulado'] = form_input('tiempo_tabulado', set_value('tiempo_tabulado', exist_obj($info, 'tiempo_tabulado')), 'class="form-control numeric" id="tiempo_tabulado" readonly');

    //Reglas de paquetes
    $modelos = $this->mcat->get('cat_modelo', 'modelo');
    $data['drop_modelos'] = form_dropdown('modelo_id', array_combos($modelos, 'id', 'modelo', TRUE, FALSE, TRUE), '', 'class="form-control busqueda" id="modelo_id"');
    $data['drop_anios'] = form_dropdown('anio[]', array_combos($this->mcat->get('cat_anios', 'anio', 'desc'), 'anio', 'anio', TRUE, FALSE, FALSE), "", 'class="form-control multiple" multiple="multiple" id="anio"');

    $data['drop_cilindros'] = form_dropdown('cilindro_id[]', array_combos($this->mcat->get('pkt_cat_cilindros', 'cilindros'), 'cilindros', 'cilindros', TRUE, FALSE, FALSE), "", 'class="form-control multiple" multiple="multiple" id="cilindro_id"');

    $transmisiones = $this->mcat->get('pkt_cat_transmision', 'transmision');
    $data['drop_transmisiones'] = form_dropdown('transmision_id[]', array_combos($transmisiones, 'id', 'transmision', TRUE, FALSE, FALSE), "", 'class="form-control multiple" multiple="multiple" id="transmision_id"');

    $motores = $this->mcat->get('pkt_cat_motor', 'motor');
    $data['drop_motores'] = form_dropdown('motor_id[]', array_combos($motores, 'id', 'motor', TRUE, FALSE, FALSE), "", 'class="form-control multiple" multiple="multiple" id="motor_id"');

    $combustibles = $this->mcat->get('pkt_cat_combustibles', 'combustible');
    $data['drop_combustibles'] = form_dropdown('combustible_id[]', array_combos($combustibles, 'id', 'combustible', TRUE, FALSE, FALSE), "", 'class="form-control multiple" multiple="multiple" id="combustible_id"');


    $subarticulos = $this->mcat->get('pkt_subarticulos', 'nombre');
    $data['id'] = $id;
    $data['detalle_paquete'] = $detalle_paquete;
    $data['reglas_paquete'] = $reglas_paquete;
    $data['modelos'] = $modelos;
    $data['subarticulos'] = $subarticulos;
    $data['transmisiones'] = $transmisiones;
    $data['combustibles'] = $combustibles;
    $data['motores'] = $motores;
    $this->blade->render('agregar_paquetes', $data);
  }
  //Obtener tabla detalle paquete 
  public function getDetallePaquete()
  {
    $paquete_id = $_POST['paquete_id'];
    $data['detalle_paquete'] = $this->mp->getDetallePaquete($paquete_id);
    //Actualizar paquetes
    $this->mp->updateDataPaquete($paquete_id);
    $this->blade->render('tbl_detalle_paquete', $data);
  }
  //Obtener reglas paquete 
  public function getReglasPaquete()
  {
    $subarticulos = $this->mcat->get('pkt_subarticulos', 'nombre');
    $paquete_id = $_POST['paquete_id'];
    $data['modelos'] = $this->mcat->get('cat_modelo', 'modelo');
    $data['subarticulos'] = $subarticulos;
    $data['transmisiones'] = $this->mcat->get('pkt_cat_transmision', 'transmision');
    $data['combustibles'] = $this->mcat->get('pkt_cat_combustibles', 'combustible');
    $data['motores'] = $this->mcat->get('pkt_cat_motor', 'motor');
    $data['reglas_paquete'] = $this->mp->getReglasPaquete($paquete_id);
    $this->blade->render('tbl_reglas_paquete', $data);
  }
  public function asignarDetallePaquete()
  {

    if ($this->input->post()) {
      $this->form_validation->set_rules('cantidad', 'cantidad', 'trim|required|numeric');
      $this->form_validation->set_rules('parte', 'parte', 'trim');
      $this->form_validation->set_rules('precio_unitario', 'precio unitario', 'trim|required|numeric');
      $this->form_validation->set_rules('total_refaccion', 'total refacción', 'trim|required|numeric');
      $this->form_validation->set_rules('tiempo_tabulado', 'precio tabulado', 'trim|required');

      if ($this->form_validation->run()) {
        $this->mp->guardarDetallePaquete();
      } else {
        $errors = array(
          'cantidad' => form_error('cantidad'),
          'parte' => form_error('parte'),
          'precio_unitario' => form_error('precio_unitario'),
          'total_refaccion' => form_error('total_refaccion'),
          'tiempo_tabulado' => form_error('tiempo_tabulado'),
        );
        echo json_encode($errors);
        exit();
      }
    }
    $id = $_GET['id'];
    $paquete_id = $_GET['paquete_id'];
    if ($id == 0) {
      $info = new Stdclass();
      $precio_calculado = 0;
    } else {
      $info = $this->principal->getGeneric('id', $id, 'pkt_detalle_paquete');
      $precio_calculado = $info->precio_calculado;
    }
    $art_return = [];
    $articulos = $this->mcat->get('pkt_articulos', 'descripcion', 'asc', ['activo' => 1]);

    $data['articulo_id'] = form_dropdown('articulo_id', array_combos($articulos, 'id', 'descripcion', TRUE), set_value('articulo_id', exist_obj($info, 'articulo_id')), 'class="form-control busqueda" id="articulo_id"');
    $val_cantidad = set_value('cantidad', exist_obj($info, 'cantidad'));
    $data['cantidad'] = form_input('cantidad', $val_cantidad ? $val_cantidad : 1, 'class="form-control numeric calcular_total" id="cantidad"');

    $data['parte'] = form_input('parte', set_value('parte', exist_obj($info, 'parte')), 'class="form-control numeric calcular_total" id="parte"');

    $data['precio_unitario'] = form_input('precio_unitario', set_value('precio_unitario', exist_obj($info, 'precio_unitario')), 'class="form-control numeric calcular_total" id="precio_unitario"');

    $data['total_refaccion'] = form_input('total_refaccion', set_value('total_refaccion', exist_obj($info, 'total_refaccion')), 'class="form-control numeric" id="total_refaccion"');

    $data['tiempo_tabulado'] = form_input('tiempo_tabulado', set_value('tiempo_tabulado', exist_obj($info, 'tiempo_tabulado')), 'class="form-control numeric" id="tiempo_tabulado"');

    $data['art_id'] = $id;
    $data['paquete_id'] = $paquete_id;
    $data['precio_calculado'] = $precio_calculado;
    $this->blade->render('agregar_detalle_articulos', $data);
  }
  //Agregar regla
  public function agregarRegla()
  {
    if ($this->input->post()) {
      $this->form_validation->set_rules('modelo_id[]', 'artículo(s)', 'trim|required');
      $this->form_validation->set_rules('subarticulos[]', 'subartículo(s)', 'trim');
      $this->form_validation->set_rules('anio[]', 'modelo(s)', 'trim|required');
      $this->form_validation->set_rules('cilindro_id[]', 'cilindro(s)', 'trim|required');
      $this->form_validation->set_rules('transmision_id[]', 'transmision(es)', 'trim|required');
      $this->form_validation->set_rules('motor_id[]', 'motor(es)', 'trim|required');
      $this->form_validation->set_rules('combustible_id[]', 'tipo combustible(es)', 'trim|required');
      if ($this->form_validation->run()) {
        $this->mp->guardarReglas();
      } else {
        $errors = array(
          'modelo_id' => form_error('modelo_id[]'),
          'subarticulos' => form_error('subarticulos[]'),
          'anio' => form_error('anio[]'),
          'cilindro_id' => form_error('cilindro_id[]'),
          'transmision_id' => form_error('transmision_id[]'),
          'motor_id' => form_error('motor_id[]'),
          'combustible_id' => form_error('combustible_id[]'),
        );
        echo json_encode($errors);
        exit();
      }
    }
  }
  //Eliminar detalle
  public function eliminar_detalle_articulo()
  {
    $detalle_id = $_POST['detalle_id'];
    $this->db->where('id', $detalle_id)->delete('pkt_detalle_paquete');
    echo 1;
    exit();
  }
  //Eliminar regla
  public function eliminar_regla_paquete()
  {
    $regla_id = $_POST['regla_id'];
    $this->db->where('id', $regla_id)->delete('pkt_reglas_paquetes');
    echo 1;
    exit();
  }
  public function getClave()
  {
    $clave_id = $_POST['clave_id'];
    $clave = $this->db->where('id', $clave_id)->get('pkt_claves')->row();
    echo json_encode($clave);
  }
  //Obtener la vista del paquete cuando haga el onchange del artículo
  public function v_regla_paquete()
  {
    //Subartículos
    $regla_id = $_POST['regla_id']; // 0 es nueva 1 editando
    $default_subarticulos = [];
    $default_anios = [];
    $default_cilindros = [];
    $default_transmisiones = [];
    $default_motores = [];
    $default_combustibles = [];
    if ($regla_id != 0) {
      $reglas_paquete = $this->principal->getGeneric('id', $regla_id, 'pkt_reglas_paquetes');
      $default_subarticulos = explode(',', $reglas_paquete->subarticulos);
      $default_anios = explode(',', $reglas_paquete->anios);
      $default_cilindros = explode(',', $reglas_paquete->cilindros);
      $default_transmisiones = explode(',', $reglas_paquete->transmisiones);
      $default_motores = explode(',', $reglas_paquete->motores);
      $default_combustibles = explode(',', $reglas_paquete->combustibles);
    }

    $subarticulos = $this->mcat->get('pkt_subarticulos', 'nombre', '', ['articulo_id' => $_POST['articulo_id']]);
    $data['drop_subarticulos'] = form_dropdown('subarticulos[]', array_combos($subarticulos, 'id', 'nombre', TRUE, FALSE, FALSE), $default_subarticulos, 'class="form-control multiple" multiple="multiple" id="subarticulos"');

    $data['drop_anios'] = form_dropdown('anio[]', array_combos($this->mcat->get('cat_anios', 'anio', 'desc'), 'anio', 'anio', TRUE, FALSE, FALSE), $default_anios, 'class="form-control multiple" multiple="multiple" id="anio"');
    $data['drop_cilindros'] = form_dropdown('cilindro_id[]', array_combos($this->mcat->get('pkt_cat_cilindros', 'cilindros'), 'cilindros', 'cilindros', TRUE, FALSE, FALSE), $default_cilindros, 'class="form-control multiple" multiple="multiple" id="cilindro_id"');
    $transmisiones = $this->mcat->get('pkt_cat_transmision', 'transmision');
    $data['drop_transmisiones'] = form_dropdown('transmision_id[]', array_combos($transmisiones, 'id', 'transmision', TRUE, FALSE, FALSE), $default_transmisiones, 'class="form-control multiple" multiple="multiple" id="transmision_id"');
    $motores = $this->mcat->get('pkt_cat_motor', 'motor');
    $data['drop_motores'] = form_dropdown('motor_id[]', array_combos($motores, 'id', 'motor', TRUE, FALSE, FALSE), $default_motores, 'class="form-control multiple" multiple="multiple" id="motor_id"');
    $combustibles = $this->mcat->get('pkt_cat_combustibles', 'combustible');
    $data['drop_combustibles'] = form_dropdown('combustible_id[]', array_combos($combustibles, 'id', 'combustible', TRUE, FALSE, FALSE), $default_combustibles, 'class="form-control multiple" multiple="multiple" id="combustible_id"');
    $this->blade->render('v_regla_paquete', $data);
  }
  public function getPaqueteByData()
  {
    $paquete_id = $this->mp->getPaqueteByData();
    if (!$paquete_id) {
      echo json_encode(array('paquete_id' => $paquete_id, 'data_paquete' => []));
    } else {
      $data_paquete = $this->principal->getGeneric('id', $paquete_id, 'pkt_paquetes');
      echo json_encode(array('paquete_id' => $paquete_id, 'data_paquete' => $data_paquete));
    }
  }
  //CATÁLOGOS
  public function catalogos($tipo = 0)
  {
    $catalogos = [
      1 => [
        'tabla' => 'pkt_articulos',
        'campo' => 'descripcion',
        'nombre' => 'Artículos',
      ],
      2 => [
        'tabla' => 'pkt_cat_cilindros',
        'campo' => 'cilindros',
        'nombre' => 'Cilindros',
      ],
      3 => [
        'tabla' => 'pkt_cat_combustibles',
        'campo' => 'combustible',
        'nombre' => 'Combustibles',
      ],
      4 => [
        'tabla' => 'pkt_cat_motor',
        'campo' => 'motor',
        'nombre' => 'Motores',
      ],
      5 => [
        'tabla' => 'pkt_cat_tipo_paquetes',
        'campo' => 'tipo_paquete',
        'nombre' => 'Tipo de paquetes',
      ],
      6 => [
        'tabla' => 'pkt_cat_tipo_precio',
        'campo' => 'tipo_precio',
        'nombre' => 'Tipo de precios',
      ],
      7 => [
        'tabla' => 'pkt_cat_transmision',
        'campo' => 'transmision',
        'nombre' => 'Transmisiones',
      ],
    ];
    $data['error_catalogo'] = false;
    if (!isset($catalogos[$tipo])) {
      return '<h1>El catálogo no existe</h1>';
    }
    $datos['catalogo'] = $catalogos[$tipo];
    $datos['tipo'] = $tipo;
    $datos['listado'] = $this->mcat->get($datos['catalogo']['tabla'], $datos['catalogo']['campo']);
    $this->blade->set_data($datos)->render('catalogos/listado');
  }
  public function tabla_catalogos()
  {
    $catalogos = [
      1 => [
        'tabla' => 'pkt_articulos',
        'campo' => 'descripcion',
        'nombre' => 'Artículos',
      ],
      2 => [
        'tabla' => 'pkt_cat_cilindros',
        'campo' => 'cilindros',
        'nombre' => 'Cilindros',
      ],
      3 => [
        'tabla' => 'pkt_cat_combustibles',
        'campo' => 'combustible',
        'nombre' => 'Combustibles',
      ],
      4 => [
        'tabla' => 'pkt_cat_motor',
        'campo' => 'motor',
        'nombre' => 'Motores',
      ],
      5 => [
        'tabla' => 'pkt_cat_tipo_paquetes',
        'campo' => 'tipo_paquete',
        'nombre' => 'Tipo de paquetes',
      ],
      6 => [
        'tabla' => 'pkt_cat_tipo_precio',
        'campo' => 'tipo_precio',
        'nombre' => 'Tipo de precios',
      ],
      7 => [
        'tabla' => 'pkt_cat_transmision',
        'campo' => 'transmision',
        'nombre' => 'Transmisiones',
      ],
    ];

    $datos['tabla'] = $_POST['tabla'];
    $datos['campo'] = $_POST['campo'];
    $datos['nombre'] = $_POST['nombre'];
    $datos['listado'] = $this->mcat->get($_POST['tabla'], $_POST['campo']);
    $datos['catalogo'] = $catalogos[$_POST['tipo']];
    $this->blade->set_data($datos)->render('catalogos/tbl_catalogos');
  }
  function agregar_catalogo($id = 0)
  {
    if ($this->input->is_ajax_request() && $this->session->userdata('id_usuario') == '') {
      $this->output->set_status_header('409');
      exit();
    } else if ($this->session->userdata('id_usuario') == '') {
      redirect('login');
    }

    if ($this->input->post()) {
      $this->form_validation->set_rules($_POST['campo'], $_POST['campo'], 'trim|required');
      if ($this->form_validation->run()) {
        $this->mp->guardarCatalogo();
      } else {
        $errors = array(
          $_POST['campo'] => form_error($_POST['campo']),
        );
        echo json_encode($errors);
        exit();
      }
    }
    $tabla = $_GET['tabla'];
    $campo = $_GET['campo'];
    $nombre = $_GET['nombre'];

    if ($id == 0) {
      $info = new Stdclass();
    } else {
      $info = $this->db->where('id', $id)->get($tabla)->result();
      $info = $info[0];
    }
    $data['input_id'] = $id;
    $data['tabla'] = $tabla;
    $data['campo'] = $campo;
    $data['nombre'] = $nombre;

    $data['input_nombre'] = form_input($campo, set_value($campo, exist_obj($info, $campo)), 'class="form-control" rows="5" id="' . $campo . '" ');
    $this->blade->render('catalogos/nuevo_registro', $data);
  }
  // Submodelos
  public function subarticulos()
  {
    $datos['subarticulos'] = $this->db->select('s.*,m.modelo')
      ->join('cat_modelo m', 'm.id=s.articulo_id')
      ->get('pkt_subarticulos s')
      ->result();
    $this->blade->set_data($datos)->render('catalogos/subarticulos');
  }
  function agregarSubarticulo($id = 0)
  {
    if ($this->input->is_ajax_request() && $this->session->userdata('id_usuario') == '') {
      $this->output->set_status_header('409');
      exit();
    } else if ($this->session->userdata('id_usuario') == '') {
      redirect('login');
    }

    if ($this->input->post()) {
      $this->form_validation->set_rules('nombre', 'subartículo', 'trim|required');
      if ($this->form_validation->run()) {
        $this->mp->guardarSubarticulo();
      } else {
        $errors = array(
          'nombre' => form_error('nombre'),
        );
        echo json_encode($errors);
        exit();
      }
    }
    if ($id == 0) {
      $info = new Stdclass();
    } else {
      $info = $info = $this->db->where('id', $id)->get('pkt_subarticulos')->result();
      $info = $info[0];
    }
    $data['input_id'] = $id;
    $data['input_nombre'] = form_input('nombre', set_value('nombre', exist_obj($info, 'nombre')), 'class="form-control" rows="5" id="nombre" ');
    $data['articulo_id'] = form_dropdown('articulo_id', array_combos($this->mcat->get('cat_modelo', 'modelo'), 'id', 'modelo', TRUE), set_value('articulo_id', exist_obj($info, 'articulo_id')), 'class="form-control" id="articulo_id"');
    $this->blade->render('catalogos/nuevo_subarticulo', $data);
  }
  public function tabla_subarticulos()
  {
    $datos['subarticulos'] = $this->db->select('s.*,m.modelo')
      ->join('cat_modelo m', 'm.id=s.articulo_id')
      ->get('pkt_subarticulos s')
      ->result();
    $this->blade->set_data($datos)->render('catalogos/tabla_subarticulos');
  }
  // Claves
  public function catalogo_claves()
  {
    $datos['claves'] = $this->mcat->get('pkt_claves', 'clave');
    $this->blade->set_data($datos)->render('catalogos/claves');
  }
  function agregarClave($id = 0)
  {
    if ($this->input->is_ajax_request() && $this->session->userdata('id_usuario') == '') {
      $this->output->set_status_header('409');
      exit();
    } else if ($this->session->userdata('id_usuario') == '') {
      redirect('login');
    }

    if ($this->input->post()) {
      $this->form_validation->set_rules('clave', 'clave', 'trim|required');
      $this->form_validation->set_rules('codigo', 'código', 'trim|required');
      $this->form_validation->set_rules('tipo_mo', 'tipo mano de obra', 'trim|required');
      $this->form_validation->set_rules('descripcion', 'descripción', 'trim|required');
      if ($this->form_validation->run()) {
        $this->mp->guardarClave();
      } else {
        $errors = array(
          'clave' => form_error('clave'),
          'codigo' => form_error('codigo'),
          'tipo_mo' => form_error('tipo_mo'),
          'descripcion' => form_error('descripcion'),
        );
        echo json_encode($errors);
        exit();
      }
    }
    if ($id == 0) {
      $info = new Stdclass();
    } else {
      $info = $info = $this->db->where('id', $id)->get('pkt_claves')->result();
      $info = $info[0];
    }
    $data['input_id'] = $id;
    $data['input_clave'] = form_input('clave', set_value('clave', exist_obj($info, 'clave')), 'class="form-control" rows="5" id="clave" ');
    $data['input_codigo'] = form_input('codigo', set_value('codigo', exist_obj($info, 'codigo')), 'class="form-control" rows="5" id="codigo" ');
    $data['input_tipo_mo'] = form_input('tipo_mo', set_value('tipo_mo', exist_obj($info, 'tipo_mo')), 'class="form-control" rows="5" id="tipo_mo" ');
    $data['input_descripcion'] = form_textarea('descripcion', set_value('descripcion', exist_obj($info, 'descripcion')), 'class="form-control" id="descripcion"');
    $this->blade->render('catalogos/nueva_clave', $data);
  }
  public function tabla_claves()
  {
    $datos['claves'] = $this->mcat->get('pkt_claves', 'clave');
    $this->blade->set_data($datos)->render('catalogos/tbl_claves');
  }
  public function adminUpdatePreciosPaquetes(){
    $this->mp->PreciosPaquetes();
  }
}
