<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_admin extends CI_Model{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set(CONST_ZONA_HORARIA);
    }
    public function getIdTecnicoByOperacion($idoperacion=''){
	  $q= $this->db->where('id',$idoperacion)->get('articulos_orden');
      if($q->num_rows()==1){
        return $q->row()->id_tecnico;
      }else{
      	return '';
      }
    }
    public function insertTransicion($estatus_general=0,$id_tecnico=''){
     
      //Verificar si no existe la transaccion traerme la de creación de la cita
      $inicio = $this->ml->getLastTransition($this->input->post('id_cita_save'),'',$this->input->post('id_operacion'));
      if($inicio==''){
        $inicio = $this->ml->getFechaCreacion($this->input->post('id_cita_save'));
      }
      $idorden = $this->mc->getIdOrdenByCita($this->input->post('id_cita_save'));
            
      //NUEVA VERSION
      //Obtener datos del estatus
      $id_status_anterior = $this->ml->getIdStatusByNombre($this->ml->getLastStatusTransiciones($this->input->post('id_cita_save'),$this->input->post('id_operacion')));
      

      $datos_estatus = $this->ml->getStatusId($id_status_anterior);
      
      //Si el estatus == 3 y ya se terminaron todas las operaciones guardar en la línea de tiempo
      if($this->ml->validarTodasOperacionesTerminadas($idorden)){
        $this->ma->insertTransicion($estatus_general);
      }
      if($this->input->post('id_status')==3||$this->input->post('id_status')==5){
        //Seleccionar las operaciones para terminarlas
        $resto_operaciones = $this->db->where('idorden',$idorden)->where('no_planificado',0)->get('articulos_orden')->result();
        foreach($resto_operaciones as $r => $val){
          $datos_transiciones = array('status_anterior' => $this->ml->getLastStatusTransiciones($this->input->post('id_cita_save'),$val->id),
                                  'status_nuevo'=> $this->ml->getStatusTecnicoTransiciones($this->input->post('id_status')),
                                  'fecha_creacion'=>date('Y-m-d H:i:s'),
                                  'tipo'=>2, //técnico,
                                  'inicio'=>$inicio,
                                  'fin'=>date('Y-m-d H:i:s'),
                                  'usuario'=>'ADMINISTRADOR',
                                  'id_cita'=>$this->input->post('id_cita_save'),
                                  'general'=>0,
                                  'id_operacion'=> $val->id,
                                  'id_tecnico'=> $val->id_tecnico,
                                  'minutos_transcurridos'=>dateDiffMinutes($inicio , date('Y-m-d H:i:s')),
                                  'motivo'=>$this->input->post('comentarios'),
          );
          $this->db->insert('transiciones_estatus',$datos_transiciones);
          $this->db->where('id',$val->id)->set('id_status',3)->update('articulos_orden');
        }
      
      }else{
        $datos_transiciones = array('status_anterior' => $this->ml->getLastStatusTransiciones($this->input->post('id_cita_save'),$this->input->post('id_operacion')),
                                  'status_nuevo'=> $this->ml->getStatusTecnicoTransiciones($this->input->post('id_status')),
                                  'fecha_creacion'=>date('Y-m-d H:i:s'),
                                  'tipo'=>2, //técnico,
                                  'inicio'=>$inicio,
                                  'fin'=>date('Y-m-d H:i:s'),
                                  'usuario'=>'ADMINISTRADOR',
                                  'id_cita'=>$this->input->post('id_cita_save'),
                                  'general'=>$estatus_general,
                                  'id_operacion'=> $this->input->post('id_operacion'),
                                  'id_tecnico'=> $id_tecnico,
                                  'estatus_detenido'=>isset($datos_estatus->estatus_detenido)?$datos_estatus->estatus_detenido:0,
                                  'motivo_detencion'=>$this->input->post('motivo_detencion'),
                                  'minutos_transcurridos'=>dateDiffMinutes($inicio , date('Y-m-d H:i:s')),
                                  'motivo'=>$this->input->post('comentarios'),
        );
        $this->db->insert('transiciones_estatus',$datos_transiciones);
      }
      //Verificar si el estatus antes del cambio era un estatus de detención para tomar los mínutos
      $estatus_old_detenido = $this->getStatusId($this->input->post('id_status_actual'))->estatus_detenido;
      if($estatus_old_detenido==1){
        $operaciones_detenidas = array('id_operacion' =>$this->input->post('id_operacion'),
                                       'id_status' =>$this->input->post('id_status_actual'),
                                       'tiempo_detencion' =>$datos_transiciones['minutos_transcurridos'],
                                       'created_at' => date('Y-m-d H:i:s'),
                                       'motivo_detencion'=>$this->input->post('motivo_detencion'),
        );
        $this->db->insert('operaciones_detenidas',$operaciones_detenidas);
      }
  }
  public function getStatusId($id=''){
    $q = $this->db->where('id',$id)->get('cat_status_citas_tecnicos');
    return $q->row();
  }
  public function insertTransicionDetenido($id_tecnico = '', $id_operacion)
  {

    //Verificar si no existe la transaccion traerme la de creación de la cita
    $inicio = $this->ml->getLastTransition($this->input->post('id_cita'), '', $id_operacion);
    //Obtener datos del estatus
    $datos_transiciones = array(
      'status_anterior' => $this->ml->getLastStatusTransiciones($this->input->post('id_cita'), $id_operacion),
      'status_nuevo' => 'Detenida por otros',
      'fecha_creacion' => date('Y-m-d H:i:s'),
      'tipo' => 2, //técnico,
      'inicio' => $inicio,
      'fin' => date('Y-m-d H:i:s'),
      'usuario' => 'ADMINISTRADOR',
      'id_cita' => $this->input->post('id_cita'),
      'general' => 0,
      'id_operacion' => $id_operacion,
      'id_tecnico' => $id_tecnico,
      'estatus_detenido' => 1,
      'motivo_detencion' => 'O',
      'minutos_transcurridos' => dateDiffMinutes($inicio, date('Y-m-d H:i:s')),
      'motivo' => $this->input->post('comentarios'),
    );
    $this->db->insert('transiciones_estatus', $datos_transiciones);
      $operaciones_detenidas = array(
        'id_operacion' => $id_operacion,
        'id_status' => 16,
        'tiempo_detencion' => $datos_transiciones['minutos_transcurridos'],
        'created_at' => date('Y-m-d H:i:s'),
        'motivo_detencion' => 'O',
      );
      $this->db->insert('operaciones_detenidas', $operaciones_detenidas);
  }
}


 