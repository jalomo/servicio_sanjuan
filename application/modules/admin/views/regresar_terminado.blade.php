@layout('tema_luna/layout')
<style type="text/css">
    /** NUEVA VERSIÓN */
    .esconder_div {
        display: none;
    }

    .mostrar_div {
        visibility: visible;
    }

</style>
@section('contenido')
    <h1>Regresar terminado</h1>
    <br>
    <form action="" method="POST" id="frm">
        <div class="row">
            <div class="col-sm-3">
                <label for="">Cita</label>
                {{ $input_id_cita }}
                <div class="error error_id_cita_save"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
                <label for="">Comentario</label>
                {{ $input_comentarios }}
                <div class="error error_comentarios"></div>
            </div>
        </div>
        <br>
        <div class="row text-right">
            <div class="col-sm-8">
                <button type="button" id="guardar" class="btn btn-success">Guardar</button>
            </div>
        </div>
        <br>
    </form>

@endsection
@section('scripts')

    <script>
        var site_url = "{{ site_url() }}";
        var id_cita = '';
        var accion = '';
        $("body").on("click", '#guardar', CambiarStatus);
        function CambiarStatus() {
            var url = site_url + "/admin/saveRegresoLavado";
            if ($("#id_cita_save").val() == '' || $("#comentarios").val() == '') {
                ErrorCustom(
                    "Todos los datos son requeridos");
                return;
            }
            ajaxJson(url, {
                id_cita: $("#id_cita_save").val(),
                id_status_actual: 3,
            }, "POST", "async", function(result) {
                if (result == -1) {
                    ErrorCustom("La cita no existe");
                } else if (result == -2) {
                    ErrorCustom(
                        "Esta cita no se puede regresar de terminado por que tiene más de una operación");
                } else if (result == -3) {
                    ErrorCustom(
                        "La unidad ya fue entregada");
                } else if (result == -4) {
                    ErrorCustom(
                        "Sólo se puede regresar si la unidad ya se terminó");
                } else {
                    ExitoCustom("Registro actualizado correctamente", function() {
                        location.reload();
                    });
                }
            });
        }

        function CambiarStatusBK() {
            var url = site_url + "/admin/saveRegresoLavado";
            if ($("#id_status").val() == $("#id_status_actual").val()) {
                ErrorCustom("El estatus que desea cambiar debe ser diferente al que está actualmente");
            } else {
                if ($("#id_status_actual").val() == 1 && ($("#id_status").val() != 2 && $("#id_status").val() != 4) && $(
                        "#id_status").val() != 5) {
                    ErrorCustom("Solamente puedes pasar un trabajo pendiente a trabajando o retrasado");
                } else {
                    //NUEVA VERSION
                    var motivo = $("#motivo_detencion").val();
                    var estatus_detenido = $("#estatus_detenido").val();

                    ajaxJson(url, $("#frm").serialize(), "POST", "async", function(result) {
                        if (isNaN(result)) {
                            data = JSON.parse(result);
                            //Se recorre el json y se coloca el error en la div correspondiente
                            $.each(data, function(i, item) {
                                $.each(data, function(i, item) {
                                    $(".error_" + i).empty();
                                    $(".error_" + i).append(item);
                                    $(".error_" + i).css("color", "red");
                                });
                            });
                        } else {
                            if (result == 0) {
                                ErrorCustom('Error al cambiar el estatus, por favor intenta de nuevo.');
                            } else if (result == -2) {
                                ErrorCustom(
                                    'Es necesario que el asesor determine que el cliente no llegó antes de cambiar el estatus.'
                                );
                            } else if (result == -3) {
                                ErrorCustom(
                                    'La unidad no puede ser cambiada al estatus de terminado por que no hay una hoja multipunto creada para este servicio'
                                );
                            } else if (result == -4) {
                                ErrorCustom('Ya fue asignado otro trabajo');
                            } else if (result == -5) {
                                ErrorCustom(
                                    'La unidad no puede ser cambiada al estatus de terminado por qué no ha pasado por el estatus de trabajando'
                                );
                            } else if (result == -6) {
                                ErrorCustom(
                                    'La unidad no puede ser terminada por que todas sus operaciones aún no han sido terminadas'
                                );
                            } else {
                                ExitoCustom("Estatus cambiado con éxito", function() {
                                    location.reload();
                                });
                            }
                        }
                    });
                }
            }
        }
    </script>
@endsection
