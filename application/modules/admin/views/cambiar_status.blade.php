@layout('tema_luna/layout')
<style type="text/css">
  /** NUEVA VERSIÓN */
  .esconder_div{
    display: none;
  }
  .mostrar_div{
    visibility: visible;
  }
</style>
@section('contenido')
<h1>Cambiar estatus de una cita</h1>
<br>
	<form action="" method="POST" id="frm">
		<input type="hidden" name="id_status_actual" id="id_status_actual" value="">
		<input type="hidden" name="es_estatus_detenido" id="es_estatus_detenido" value="">
		<div class="row">
			<div class="col-sm-3">
				<label for="">Cita</label>
				{{$input_id_cita}}
				<div class="error error_id_cita_save"></div>
			</div>
			<div class="col-sm-3">
				<label for="">Operación</label> <br>
				{{$drop_operaciones}}
				<div class="error error_id_operacion"></div>
			</div>
			<div class="col-sm-3">
				<div id="div_estatus">
					<label for="">Estatus</label> <br>
					{{$drop_status}}
					<div class="error error_id_status"></div>
				</div>
			</div>
			<div id="div_motivo" class="col-sm-3">
				<label for="">Motivo detención:</label>
				<select type="text" name="motivo_detencion" id="motivo_detencion" class="form-control">
					<option selected value="">Selecciona una opción</option>
					<option value = 'R'> Falta de Refacciones </option>
					<option value = 'A'> Autorización del Cliente </option>
					<option value = 'G'> Detenido por Gerencia </option>
					<option value = 'S'> Subcontratados </option>
					<option value = 'C'> Control Calidad </option>
					<option value = 'O'> Otro</option>
				</select>
				<span class="error error_motivo_detencion"></span>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-8">
				<label for="">Comentario</label>
				{{$input_comentarios}}
				<div class="error error_comentarios"></div>
			</div>
		</div>
		<br>
		<div class="row text-right">
			<div class="col-sm-8">
				<button type="button" id="guardar" class="btn btn-success">Actualizar</button>
			</div>
		</div>
		<br>
	</form>
	
@endsection
@section('scripts')

<script>
	var site_url = "{{site_url()}}";
	var id_cita = '';
	var accion = '';

	$("#id_cita_save").on("change",function(){
    var url=site_url+"/admin/getOperaciones";
		$("#id_operacion").empty();
		$("#id_operacion").append("<option value=''>-- Selecciona --</option>");
		id_cita=$(this).val();
		if(id_cita!=''){
			ajaxJson(url,{"id_cita":id_cita},"POST","",function(result){
				if(result.length!=0){
					$("#id_operacion").empty();
					
					result=JSON.parse(result);
					$("#id_operacion").append("<option value=''>-- Selecciona --</option>");
					$.each(result,function(i,item){
						
						$("#id_operacion").append("<option value= '"+result[i].id+"'>"+result[i].descripcion+"</option>");
					});
				}else{
					$("#id_operacion").empty();
					$("#id_operacion").append("<option value=''>No se encontraron datos</option>");
					$("#id_status").empty();
					$("#id_status").append("<option value=''>No se encontraron datos</option>");
				}
			});
		}else{
			$("#id_operacion").empty();
			$("#id_operacion").append("<option value=''>-- Selecciona --</option>");
			$("#id_operacion").attr("disabled",true);
		}
	});	
	$("#id_operacion").on('change',function(){
		var id_cita = $("#id_cita_save").val();
		var id_operacion = $("#id_operacion").val();
		if(id_cita=='' && id_operacion==''){
			ErrorCustom("Es necesario asignar el # de cita y la operación");
		}else{
			var url =site_url+"/admin/getDropEstatus";
          	ajaxLoad(url,{"id_cita":id_cita,"id_operacion":id_operacion},"div_estatus","POST",function(){
          		$("#id_status_actual").val($("#id_status").val());
          		
          		cambioStatus();
          		if($("#id_status").val()==23 || $("#id_status").val()==3){
					$("#id_status option:not(:selected)").attr("disabled", true);
				}

    		});
		}
		
	});
	$("body").on('change','#id_status',function(){
		cambioStatus();
	});
	function cambioStatus(){
		$("#motivo_detencion").val('');
		ajaxJson(site_url+"/layout/getStatusDetenido",{"id_status":$("#id_status").val()},"POST","",function(result){
			if(result==1){
				$("#div_motivo").addClass('mostrar_div');
				$("#div_motivo").removeClass('esconder_div');
				$("#motivo_detencion").val('');
				$("#motivo_detencion option").attr("disabled", false);
				$("#es_estatus_detenido").val(1);
			}else{
				$("#div_motivo").removeClass('mostrar_div');
				$("#div_motivo").addClass('esconder_div');
				$("#es_estatus_detenido").val(0);
			}
			$("#estatus_detenido").val(result);
		});
		$("#motivo_detencion").val($("#motivo").val());
		
	}
	$("body").on("click",'#guardar',CambiarStatus);
	function validarCita(){
      var url =site_url+"/admin/cambiar_status_cita_tecnico";
      ajaxJson(url,$("#frm").serialize(),"POST","async",function(result){
        $(".error").empty();
        if(isNaN(result)){
        	result = JSON.parse( result );
          //Se recorre el json y se coloca el error en la div correspondiente
             $.each(result, function(i, item) {
                  $(".error_"+i).empty();
                  $(".error_"+i).append(item);
                  $(".error_"+i).css("color","red");
              });
        }else{
        	if(result==-1){
        		ErrorCustom("La cita no existe");
        	}else{
        		ExitoCustom("Registro actualizado correctamente",function(){
        			location.reload();
        		});
        	}
        }
      });
    }
    function CambiarStatus(){
			var url =site_url+"/admin/cambiar_status_cita_tecnico";
			if($("#id_status").val()== $("#id_status_actual").val()){
				ErrorCustom("El estatus que desea cambiar debe ser diferente al que está actualmente");
			}else{
				if($("#id_status_actual").val()==1 && ($("#id_status").val()!=2 && $("#id_status").val()!=4) &&  $("#id_status").val()!=5){
					ErrorCustom("Solamente puedes pasar un trabajo pendiente a trabajando o retrasado");
				}else{
					//NUEVA VERSION
					var motivo = $("#motivo_detencion").val();
					var estatus_detenido = $("#estatus_detenido").val();
					
					ajaxJson(url,$("#frm").serialize(),"POST","async",function(result){
						if(isNaN(result)){
							data = JSON.parse( result );
							//Se recorre el json y se coloca el error en la div correspondiente
							$.each(data, function(i, item) {
								 $.each(data, function(i, item) {
				                    $(".error_"+i).empty();
				                    $(".error_"+i).append(item);
				                    $(".error_"+i).css("color","red");
				                });
							});
						}else{
							if(result ==0){
								ErrorCustom('Error al cambiar el estatus, por favor intenta de nuevo.');
							}else if(result==-2){
								ErrorCustom('Es necesario que el asesor determine que el cliente no llegó antes de cambiar el estatus.');
							}else if(result==-3){
								ErrorCustom('La unidad no puede ser cambiada al estatus de terminado por que no hay una hoja multipunto creada para este servicio');
							}else if(result==-4){
								ErrorCustom('Ya fue asignado otro trabajo');
							}else if(result==-5){
								ErrorCustom('La unidad no puede ser cambiada al estatus de terminado por qué no ha pasado por el estatus de trabajando');
							}else if(result==-6){
								ErrorCustom('La unidad no puede ser terminada por que todas sus operaciones aún no han sido terminadas');
							}
							else{
								ExitoCustom("Estatus cambiado con éxito",function(){
									location.reload();
								});
							}
						}
					});
				}
			}
		}

</script>
@endsection