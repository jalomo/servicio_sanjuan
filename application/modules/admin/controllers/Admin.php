<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('citas/m_catalogos','mcat');
    $this->load->model('citas/m_citas','mc');
    $this->load->model('m_admin','ma');
    $this->load->model('layout/m_layout','ml'); //NUEVA VERSION
    $this->load->helper(array('general'));
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  public function bloquearDia(){
    ini_set('max_execution_time', 30000);
    $this->db->where('id_tecnico',9);
    $horarios = $this->db->where('fecha >','2019-08-06')->get('horarios_tecnicos')->result();
      foreach ($horarios as $key => $value) {
        $fecha = $value->fecha;
        $dia = date('l', strtotime($fecha));
        if($dia=='Friday'){
          $this->db->where('id',$value->id)->set('activo',0)->update('horarios_tecnicos');
        }
      }
  }  
  public function CambiarStatus(){

    // $lista_status = $this->mcat->get('cat_status_citas_tecnicos','status','',array('activo'=>1));
    $lista_status = array();
    $data['drop_status'] = form_dropdown('id_status',array_combos($lista_status,'id','status',TRUE),'','class="form-control" id="id_status" ');
    $data['input_comentarios'] = form_textarea('comentarios',"",'class="form-control" rows="5" id="comentarios" ');
    $data['input_id_cita'] = form_input('id_cita_save','','class="form-control" id="id_cita_save" ');
    $data['drop_operaciones'] = form_dropdown('id_operacion',array_combos(array(),'','',TRUE),'','class="form-control" id="id_operacion"');
    $this->blade->render('cambiar_status',$data);
  }
  public function cambiar_status_cita_tecnico(){
      $multipunto_generada = $this->mc->GeneroMultipunto($this->input->post('id_cita_save'));
      if($this->input->post()){

        $this->form_validation->set_rules('id_status', 'estatus', 'trim|required');
        $this->form_validation->set_rules('comentarios', 'comentarios', 'trim|required');
        $this->form_validation->set_rules('id_cita_save', '# Cita', 'trim|required');
        $this->form_validation->set_rules('id_operacion', 'operación', 'trim|required');

        if($this->input->post('es_estatus_detenido')==1){
          $this->form_validation->set_rules('motivo_detencion', 'motivo detención', 'trim|required');
        }

       if ($this->form_validation->run()){
          $idhorario = $this->mc->getIdHorarioActual($this->input->post('id_cita_save'));
          $id_status_cita = $this->mc->getStatusCita($idhorario);
          $origen_cita = $this->mc->getOrigenCita($this->input->post('id_cita_save'));
          $id_status_anterior = $this->mc->getStatusTecnico($this->input->post('id_cita_save')); 
          if($this->input->post('id_status')==5 && $id_status_cita !=4){
            //id_status_cita es igual a 4 cuando el cliente no llego
            //$this->input->post('id_status') es igual a 5 cuando el cliente no llegó
            if($origen_cita!=2){ //cuando es 2 es que no ocupa decir el asesor que no llegó.
              echo -2;exit();
            }
          }

          //No puede poner otro trabajando si ya existe uno
          $idtecnico = $this->ma->getIdTecnicoByOperacion($this->input->post('id_operacion'));
          if($idtecnico!=''){
              if($this->input->post('id_status')==2){
                //NUEVA VERSION
              if(!$this->ml->ValidarUnTrabajoTecnicoOperacion($idtecnico,$this->input->post('id_operacion'))){
                echo -4;exit();
              }
            }
          }

          //Validar que haya pasado por lo menos por trabajando si la quiere terminar
          if(($this->input->post('id_status')==3||$this->input->post('id_status')==23) && !$this->ml->unidadTrabajando($this->input->post('id_cita_save'),$this->input->post('id_operacion'))){
            echo -5;exit();
          }

          $idorden = $this->mc->getIdOrdenByCita($this->input->post('id_cita_save'));
          if($this->input->post('id_status')==3){
            //Validar que si se quiere terminar, todo esté terminado
            $cantidad_operaciones = $this->ml->countOperaciones($idorden);
            $operaciones_terminadas = $this->ml->validarUltimaOperacion($idorden); //Función para saber si la operación actual puede detonar el terminar

              if(!$operaciones_terminadas){
               echo -6;exit();
              }
          }

          //NUEVA VERSION
          $estatus_general = $this->ml->getTipoStatus($this->input->post('id_status'));
          $this->mc->cambiar_status_cita_tecnico($idhorario,$estatus_general);
           //GUARDAR TRANSICIONES DE LOS ESTATUS DE ASESOR

          $status_actual = $this->input->post('status');
          
          //NUEVA VERSION
          $this->ma->insertTransicion($estatus_general,$idtecnico);

          //CUANDO ES LAVADO
          if($this->input->post('id_status')==3){
            $datos_cita = $this->mc->getDatosCita($this->input->post('id_cita_save'));
            
            $fecha = date('Y-m-d');
            $hora = date('H:i');
            $id_cita = $this->input->post('id_cita_save');
            $anio =  $datos_cita->vehiculo_anio;
            $modelo =  $datos_cita->vehiculo_modelo;
            $placas =  $datos_cita->vehiculo_placas;
            $hora_promesa = $this->mc->only_hora_promesa($this->input->post('id_cita_save'));
            $comentario = $this->input->post('comentarios');
            
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,CONST_LAVADO);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,
                        "fecha=".$fecha."&hora=".$hora."&id_cita=".$id_cita.""."&anio=".$anio.""."&modelo=".$modelo.""."&placas=".$placas.""."&hora_promesa=".$hora_promesa.""."&comentario=".$comentario."");
            // Receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $server_output = curl_exec($ch);
            curl_close ($ch);
            $mensaje_sms = "FORD PLASENCIA!, UNIDAD TERMINADA, #cita: ".$id_cita." el día ".date('Y-m-d H:i:s');

          //Enviar mensaje al asesor
          //$this->sms_general('3316723096',$mensaje_sms);//$celular_sms

            $this->db->where('id_cita',$id_cita)->set('id_status',3)->update('citas');
            //Acutalizar que terminaron las operaciones
           $this->db->where('id_cita',$id_cita)->set('JobCompletionDate',date('Y-m-d H:i:s'))->update('ordenservicio');

          }

          //Enviar mensajes en unidad de prueba
          if($this->input->post('id_status')==12){
            $datos_cita = $this->mc->getDatosCita($this->input->post('id_cita_save'));
            $fecha = $datos_cita->fecha;
            $hora = date('H:i');
            $id_cita = $this->input->post('id_cita_save');
            $anio =  $datos_cita->vehiculo_anio;
            $modelo =  $datos_cita->vehiculo_modelo;
            $placas =  $datos_cita->vehiculo_placas;

          }

          $this->db->where('id_cita',$this->input->post('id_cita_save'))->set('id_status_anterior',$id_status_anterior)->update('citas'); 
          //NUEVA VERSION
          $this->db->where('id',$this->input->post('id_operacion'))->set('id_status',$this->input->post('id_status'))->update('articulos_orden');
          
          if($this->input->post('id_status')!=5){
            $this->db->where('id_cita',$this->input->post('id_cita_save'))->set('id_status_anterior',$id_status_anterior)->update('citas'); 
            //NUEVA VERSION
            $this->db->where('id',$this->input->post('id_operacion'))->set('id_status',$this->input->post('id_status'))->update('articulos_orden');
            
            //Si es estatus detención actualizar en la tabla de ordenes el motivo
            if($_POST['es_estatus_detenido']){
              $this->db->where('id',$_POST['id_operacion'])->set('motivo_detencion',$_POST['motivo_detencion'])->update('articulos_orden');
            }else{
               $this->db->where('id',$_POST['id_operacion'])->set('motivo_detencion',null)->update('articulos_orden');
            }

            $this->db->where('id_cita',$this->input->post('id_cita_save'))->set('id_status_anterior',$id_status_anterior)->set('id_status',$_POST['id_status'])->update('citas'); 

          }
          echo 1;die();

         
          }else{
             $errors = array(
                'id_status' => form_error('id_status'),
                'comentarios' => form_error('comentarios'),
                'id_cita_save' => form_error('id_cita_save'),
                'id_operacion' => form_error('id_operacion'),
                'motivo_detencion' => form_error('motivo_detencion'),
             );
            echo json_encode($errors); exit();
          }
      }
  }
  public function getOperaciones(){
    if($this->input->post()){
      $id_cita = $this->input->post('id_cita');
      $idorden = $this->mc->getIdOrdenByCita($id_cita);
      $operaciones = $this->db->where('idorden',$idorden)->where('no_planificado',0)->where('id_tecnico is not null')->where('cancelado',0)->get('articulos_orden')->result();;
      echo json_encode($operaciones);
    }else{
    echo '';
    }
  }
  public function getDropEstatus(){
    $id_cita = $this->input->post('id_cita');
    $idorden = $this->mc->getIdOrdenByCita($id_cita);
    $id_status_cita = $this->ml->getStatusOrden($idorden,$this->input->post('id_operacion')); //2y6

    $cantidad_operaciones = $this->ml->countOperaciones($idorden);
    //var_dump($cantidad_operaciones);die();
    if($cantidad_operaciones==1){
      $this->db->where_not_in('id',23); //No salga operación terminada
    }else{
      $operaciones_terminadas = $this->ml->validarUltimaOperacion($idorden); //Función para saber si la operación actual puede detonar el terminar
       //var_dump($operaciones_terminadas);die();
      if(!$operaciones_terminadas){
        //$this->db->where_not_in('id',3); //No salga para terminar
      }else{
        $this->db->where_not_in('id',23); //No salga para terminar operación, si no terminar todo
      }
    }
    $nombre_estatus = $this->mc->getStatusTecnicoTransiciones($id_status_cita);
    
    //var_dump($nombre_estatus);die();
    
    $data['motivo_detencion'] = $this->ml->getMotivoDetencion($nombre_estatus,$this->input->post('id_operacion'));
    $lista_status = $this->mcat->get('cat_status_citas_tecnicos','status','',array('activo'=>1));    
    $data['drop_status'] = form_dropdown('id_status',array_combos($lista_status,'id','status',TRUE),$id_status_cita,'class="form-control" id="id_status"');    
    $this->blade->render('drop_estatus',$data);
  }
  public function regresar_terminado()
  {

    $data['input_comentarios'] = form_textarea('comentarios', "", 'class="form-control" rows="5" id="comentarios" ');
    $data['input_id_cita'] = form_input('id_cita_save', '', 'class="form-control" id="id_cita_save" ');
    $this->blade->render('regresar_terminado', $data);
  }
  public function saveRegresoLavado()
  {
    $id_cita = $_POST['id_cita'];
    $datos_cita = $this->mc->getDatosCita($id_cita);
    if($datos_cita->unidad_entregada){
      echo -3;exit();
    }
    if($datos_cita->id_status==1){
      echo -4;exit();
    }
    //34995
    $orden_operaciones = $this->db->where('o.id_cita', $id_cita)
      ->join('articulos_orden a', 'a.idorden=o.id')
      ->where('a.cancelado', 0)
      ->where('a.no_planificado', 0)
      ->where('a.id_status !=', null)
      ->where('a.id_tecnico !=', null)
      ->select('a.id, a.idorden,o.id_cita,a.id_tecnico')
      ->get('ordenservicio o')
      ->result();
    if (count($orden_operaciones) == 0) {
      echo -1;
      exit();
    }else if(count($orden_operaciones) > 1){
      echo -2;
      exit();
    }else{
      // actualizar la cita
      $orden_operaciones = $orden_operaciones[0];
      $this->db->where('id_cita',$id_cita)->set('id_status',1)->set('fecha_termino_lavado',null)->update('citas');
      //Borrar de lavado
      $this->db->where('id_cita',$id_cita)->delete('lavado');
      //Actualizar artículos orden

      $this->db->where('id',$orden_operaciones->id)->set('id_status',16)->update('articulos_orden');
      //BORRAR LAS TRANSICIONES
        //Obtener el id de la transición
        $transicion = $this->db->select('*')
                               ->where('id_cita',$id_cita)
                               ->where('status_nuevo','terminado')
                               ->limit(1)
                               ->order_by('id','desc')
                               ->get('transiciones_estatus')
                               ->row();
        //Se borran las que son de esa cita y mayores a la que seleccionó
        $this->db->where('id >',$transicion->id)
                  ->where('id_cita',$id_cita)
                  ->delete('transiciones_estatus');
        //Borrar la transición donde se terminó
        $this->db->where('id',$transicion->id)
                  ->delete('transiciones_estatus');
      //Insertar operación detenida
      $this->ma->insertTransicionDetenido($orden_operaciones->id_tecnico,$orden_operaciones->id);
      $this->db->where('id', $orden_operaciones->id)->set('motivo_detencion', 'O')->set('fecha_detencion',date('Y-m-d H:i:s'))->update('articulos_orden');
      echo 1;exit();

    }
  }
}
