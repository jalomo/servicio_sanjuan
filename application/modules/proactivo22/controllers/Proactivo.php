<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proactivo extends MX_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_proactivo','mp');
    $this->load->model('citas/m_catalogos','mcat');
    $this->load->helper(array('general','dompdf','correo'));
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
//MAGIC PARA GUARDAR COMENTARIOS
  public function comentarios_reagendar_MAGIC(){
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }
       if($this->input->post()){
       $this->form_validation->set_rules('comentario', 'comentario', 'trim');
       $this->form_validation->set_rules('cronoFecha', 'fecha', 'trim|required');

       
       if ($this->form_validation->run()){
          $this->mp->saveComentario();
          exit();
          }else{
             $errors = array(
                'comentario' => form_error('comentario'),
                'cronoFecha' => form_error('cronoFecha'),
             );
            echo json_encode($errors); exit();
          }
      }
      $data['id_cita'] = $this->input->get('id_cita');
      $data['fecha'] = date2sql($this->input->get('fecha'));
      $data['cliente'] = $this->input->get('cliente');
      $data['input_comentario'] = form_textarea('comentario',"",'class="form-control" id="comentario" rows="2"');

    $this->blade->render('comentarios',$data);
  }
  public function correo(){

    $cuerpo = $this->blade->render('correo_intentos',array(),true);
    //print_r($cuerpo);die();
    enviar_correo(strtolower("albertopitava@gmail.com"),"Notificación Contacto Proactivo!",$cuerpo,array());

  }
  public function correo_cp(){

    $cuerpo = $this->blade->render('correo_contacto_proactivo',array(),true);
    enviar_correo(strtolower("albertopitava@gmail.com"),"Carta previa Contacto Proactivo!",$cuerpo,array());
  }

  public function historial_proactivo(){

    $data['proactivo'] = $this->db ->select('c.id_cita as id,c.vehiculo_placas as placas,c.datos_nombres as nombre,c.datos_apellido_paterno as ap,c.datos_apellido_materno as am,c.datos_telefono,c.datos_email as correo,c.vehiculo_numero_serie as serie,p.fecha_contacto,a.adminNombre,p.fecha_contacto,pm.intentos')->join('citas c','p.idcita = c.id_cita')->join(CONST_BASE_PRINCIPAL.'admin a','p.id_usuario=a.adminId')->join('proactivo_citas pm','pm.idcita=c.id_cita')->get('proactivo_historial p')->result();
     $this->blade->render('historial_proactivo',$data);
  }
  public function buscar_historial_proactivo(){
    $fecha_inicio = date2sql($this->input->post('finicio'));
    $fecha_fin = date2sql($this->input->post('ffin'));
    $this->db->where('date(created_at) >=',$fecha_inicio);
    $this->db->where('date(created_at) <=',$fecha_fin);
    $data['proactivo'] = $this->db ->select('c.id_cita as id,c.vehiculo_placas as placas,c.datos_nombres as nombre,c.datos_apellido_paterno as ap,c.datos_apellido_materno as am,c.datos_telefono,c.datos_email as correo,c.vehiculo_numero_serie as serie,p.fecha_contacto,a.adminNombre,p.fecha_contacto,pm.intentos')->join('citas c','p.idcita = c.id_cita')->join(CONST_BASE_PRINCIPAL.'admin a','p.id_usuario=a.adminId')->join('proactivo_citas pm','pm.idcita=c.id_cita')->get('proactivo_historial p')->result();
    $this->blade->set_data($data)->render('tabla_historial');
  }
  public function generar_registros_proactivo($busqueda=0)
  {
    $month =$this->input->post('month');
    $year = $this->input->post('year');
    //Fin
    $data_registros = $this->mp->getData6Month($month,0,$year);
    $registros = array();
    foreach ($data_registros as $key => $value) {
      $registros[$value->vehiculo_numero_serie] = $value;
    }
    $total_semana = round((count($registros)/4));
    $total_dia = round($total_semana/5);
    $fecha_empiezo = $year.'-'.$month.'-01';
    $contador = 1;
    $datos_proactivo = array();
    foreach ($registros as $key => $value) {

      $dia = date("D", strtotime($fecha_empiezo)); 
      //Si el día es sábado agregar un día, si es domingo 2
      if($dia=='Sat'){
        $newdate = strtotime ( '+2 day' , strtotime ( $fecha_empiezo) ) ;
        $fecha_empiezo = date ( 'Y-m-d' , $newdate );
      }

      if($dia=='Sun'){
        $newdate = strtotime ( '+1 day' , strtotime ( $fecha_empiezo) ) ;
        $fecha_empiezo = date ( 'Y-m-d' , $newdate );
      }

      if($contador<=$total_dia){
        $fecha_programacion = $fecha_empiezo;
        $datos = array('idcita' => $value->id_cita, 
                     'intentos' => 0,
                     'fecha_programacion' => $fecha_empiezo,

        );
      }else{
        $datos = array('idcita' => $value->id_cita, 
                     'intentos' => 0,
                     'fecha_programacion' => $fecha_empiezo,

        );
        $newdate = strtotime ( '+1 day' , strtotime ( $fecha_empiezo) ) ;
        $fecha_empiezo = date ( 'Y-m-d' , $newdate );
        $contador = 1;
      }

      $contador ++;
      

      

      $this->db->insert('proactivo_citas',$datos);

    }
    $datos_proactivo = array('mes' =>$month, 
                            'anio' =>$year, 
                            'idusuario'=>$this->session->userdata('id_usuario'),
                            'created_at'=>date('Y-m-d H:i:s')
    );
    //Insertar que ya se hizo el del mes
    $this->db->insert('proactivo_mes',$datos_proactivo);
    echo 1;exit();
    
  }

  public function index($busqueda=0){
    //Si búsqueda es 1 regresar la tabla
    if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
    {
      $this->output->set_status_header('409');
      exit();
    }else if($this->session->userdata('id_usuario')==''){
    redirect('login');
    }

    if($this->input->post('fecha')==''){
      $date = date('Y-m-d');
      $newdate = strtotime ( '-6 month' , strtotime ( $date ) ) ;
      $month = date ( 'm' , $newdate );
      $mes_completo = date ( 'M' , $newdate );
      $datos['fecha_actual'] = date ( 'Y-m-d' , $newdate );
    }else{
      $date = date2sql($this->input->post('fecha'));
      $date = strtotime($date);
      $month = date ( 'm' , $date );
      $mes_completo = date ( 'M' , $date );
      $datos['fecha_actual'] = date ( 'Y-m-d' , $date );
    }


    $datos['registros'] =$this->mp->getDataByDate($datos['fecha_actual']);
    if(!$busqueda){
      $this->blade->set_data($datos)->render('lista_proactivos');
    }else{
      $this->blade->set_data($datos)->render('tabla_proactivo');
    }
  }
  public function generar_mes(){
    $date = date('Y-m-d');
    $newdate = strtotime ( '-6 month' , strtotime ( $date ) ) ;
    $data['month'] = date ( 'm' , $newdate );
    $data['year'] = date ( 'Y' , $newdate );
    $data['nameMes'] = $this->mp->getNameMes($data['month']);
    $data['validar'] = $this->mp->validaContactByMesYear($data['month'],$data['year']);
    //var_dump($month,$year);die();
    $data['registros'] = $this->mp->getProactivoMes();
    $this->blade->set_data($data)->render('proactivo_por_mes');
  }
  
  




  
}
