<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<style>
		.justify{
			text-align: justify;
		}
		table{
			margin: 0px 400px 0px;
			font-size: 20px;
		}
		@media screen and (max-width: 992px) {
		  	table{
				margin: 0px 50px 0px !important;
		    }
		}

		/*On screens that are 600px or less, set the background color to olive */
		@media screen and (max-width: 600px) {
			table{
				margin: 0px 0px 0px !important;
		    }
		}
	</style>
</head>
<body>
	<table>
		<tbody>
			<tr>
				<td><img style="display: block; margin-right: auto;" tabindex="0" src="{{CONST_LOGO_EMPRESA}}" alt="" width="400" height="120" /></td>
			</tr>
			<tr class="text-center">
				<td>
					<p>
						<strong>Estimad@ Cliente:</strong>
					</p>
					<p class="justify">
						{{CONST_AGENCIA}}, S.A. DE C.V trabaja día a día para brindarle un mejor servicio a clientes distinguidos como usted.
					</p>
					<p class="justify">
						Por lo que es necesario recordarle que su vehículo <strong>FORD</strong>, se aproxima a realizarle el <strong>SERVICIO DE MANTENIMIENTO</strong>, el cual le permitirá disfrutar de mayor seguridad en el correcto funcionamiento y confiabilidad de su vehículo.
					</p>
					<p class="justify">
						Por tal motivo queremos ofrecerle y brindarle atención personalizada cumpliendo y dando seguimiento al servicio requerido por parte de nuestra agencia.
					</p>
					<p class="justify">
						Si usted nos permite un representante de FORD se comunicara para agendarle una cita en nuestras instalaciones.
					</p>
					<br>
					<p>
						
						
						<strong>Atención a Clientes</strong><br>
						<a href="mailto:mylsaqro@fordmylsaqueretaro.mx ">mylsaqro@fordmylsaqueretaro.mx </a><br>
						<br>

						
						<span>Tels.: {{CONST_TEL_EMPRESA}}</span><br>
						<span>Lunes a Viernes de 7:00AM a 6:00PM</span><br>
						<span>Sábado 7:00AM a 1:00 PM</span>
					</p>
					<p>
						<strong>
							"Porque eres parte de nuestra familia FORD mereces un servicio EXCELENTE"
						</strong>
						<br>
						<br>
						<br>
						<span style="font-size: 10px;">
							En caso de que al recibir este aviso ya hubiera Usted efectuado el servicio a que nos referimos, favor de hacer caso omiso.
						</span>
					</p>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>