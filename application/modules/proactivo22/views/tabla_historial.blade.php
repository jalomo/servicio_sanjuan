<table id="tbl_proactivo" class="table table-bordered table-striped" cellpadding="0" width="100%">
	<thead>
		<tr class="tr_principal">
		<th>Acciones</th>
		<th>Usuario</th>
		<th>Intentos</th>
		<th>Placas</th>
		<th>Cliente</th>
		<th>Teléfono</th>
		<th>Fecha de contacto</th>
		<th>Serie</th>
	</tr>
	</thead>
	<tbody>
		@foreach($proactivo as $p => $registro)
			<tr>
				<td>
			        <a href="" data-id="{{$registro->id}}" class="pe-7s-info
pe pe-7s-info js_historial" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Historial comentarios"></a>
				</td>
				<td>{{$registro->adminNombre}}</td>
				<td>{{$registro->intentos}}</td>
				<td>{{$registro->placas}}</td>
				<td>{{$registro->nombre}}</td>
				<td>{{$registro->datos_telefono}}</td>
				<td>{{date_eng2esp_1($registro->fecha_contacto)}}</td>
				<td>{{$registro->serie}}</td>
			</tr>
		@endforeach
	</tbody>
</table>