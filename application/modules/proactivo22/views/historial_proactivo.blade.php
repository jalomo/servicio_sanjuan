@layout('tema_luna/layout')
@section('contenido')
	<h1 class="text-center">Historial contacto proactivo</h1>
	<form action="" id="frm" method="POST">
		<div class="row">
            <div class='col-md-3'>
              <label for="">Fecha inicio</label>
               <input class="form-control" type="date" name="finicio" id="finicio" value="">
                <span style="color: red" class="error error_fini"></span>
            </div>
            <div class='col-md-3'>
              <label for="">Fecha Fin</label>
              	<input class="form-control" type="date" name="ffin" id="ffin" value="">
                <span style="color: red" class="error error_ffin"></span>
            </div>
            <div class="col-md-2 col-sm-2" style="margin-top:30px;">
              <button type="button" id="buscar" name="buscar" class="btn btn-info">Buscar</button>
            </div>
      </div>
	</form>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<div id="tabla-items">
				<table id="tbl_proactivo" class="table table-bordered table-striped" cellpadding="0" width="100%">
					<thead>
						<tr class="tr_principal">
						<th>Acciones</th>
						<th>Usuario</th>
						<th>Intentos</th>
						<th>Placas</th>
						<th>Cliente</th>
						<th>Teléfono</th>
						<th>Fecha de contacto</th>
						<th>Serie</th>
					</tr>
					</thead>
					<tbody>
						@foreach($proactivo as $p => $registro)
							<tr>
								<td>
							        <a href="" data-id="{{$registro->id}}" class="pe-7s-info
pe pe-7s-info js_historial" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Historial comentarios"></a>
								</td>
								<td>{{$registro->adminNombre}}</td>
								<td>{{$registro->intentos}}</td>
								<td>{{$registro->placas}}</td>
								<td>{{$registro->nombre}}</td>
								<td>{{$registro->datos_telefono}}</td>
								<td>{{date_eng2esp_1($registro->fecha_contacto)}}</td>
								<td>{{$registro->serie}}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
@section('scripts')

<script>
	var site_url = "{{site_url()}}";
	inicializar_tabla_local()
	var cliente = '';
	$('.date').datetimepicker({
        format: 'DD/MM/YYYY',
        icons: {
            time: "far fa-clock",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
        locale: 'es'
    });
    $("#datetimepicker1").on("dp.change", function (e) {
      $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
  });
  $("#datetimepicker2").on("dp.change", function (e) {
    $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
  });
    
	
    $("body").on("click",'.js_historial',function(e){
    e.preventDefault();
    id_cita = $(this).data('id');
       var url =site_url+"/citas/historial_comentarios_reagendar_magic/0";
       customModal(url,{"id_cita":id_cita},"GET","md","","","","Cerrar","Historial de comentarios","modal1");
    });

  $("body").on('click','#buscar',function(){
  	var finicio = $("#finicio").val();
  	var ffin = $("#ffin").val();
  	
  		$(".error").empty();
		if(finicio=='' && ffin==''){
			$(".error_fini").empty().append('El campo es requerido');
			$(".error_ffin").empty().append('El campo es requerido');
		}else if(finicio=='' && ffin!=''){
			$(".error_fini").empty().append('El campo es requerido');
		}else if(finicio!='' && ffin==''){
			$(".error_ffin").empty().append('El campo es requerido');
		}else{
			buscarInformacion();
		}
  		
  });
  function buscarInformacion(){
    var url =site_url+"/proactivo/buscar_historial_proactivo";
  	ajaxLoad(url,$("#frm").serialize(),"tabla-items","POST",function(){
    	inicializar_tabla_local();
    });
  } 
  function inicializar_tabla_local(){
		 $('#tbl_proactivo').DataTable( {
	        "oLanguage": {
            "oPaginate": {
                "sPrevious": "Anterior",
                "sNext": "Siguiente",
                "sLast": "Última",
                "sFirst": "Primera"
            },
	            "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
	            '<option value="5">5</option>' +
	            '<option value="10">10</option>' +
	            '<option value="20">20</option>' +
	            '<option value="30">30</option>' +
	            '<option value="40">40</option>' +
	            '<option value="50">50</option>' +
	            '<option value="-1">Todos</option>' +
	            '</select> registros',
	            "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
	            "sInfoFiltered": " - filtrados de _MAX_ registros",
	            "sInfoEmpty": "No hay resultados de búsqueda",
	            "sZeroRecords": "No hay registros para mostrar...",
	            "sProcessing": "Espere, por favor...",
	            "sSearch": "Buscar:"
	        },
		    "scrollX": true
	    });
	}
</script>
@endsection