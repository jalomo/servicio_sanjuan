<table id="tbl_proactivo" class="table table-bordered table-striped" cellpadding="0" width="100%">
    <thead>
        <tr class="tr_principal">
            <th>Acciones</th>
            <th>ID</th>
            <th>Intentos</th>
            <th>Kilometraje</th>
            <th>Placas</th>
            <th>Cliente</th>
            <th>Email</th>
            <th>Teléfono</th>
            <th>Fecha</th>
            <th>Serie</th>
            <th>Modelo</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($registros as $r => $registro)
            <tr>
                <td>
                    <a href="citas/agendar_cita/0/0/0/{{ $registro->id }}" class="" aria-hidden="true"
                        data-toggle="tooltip" data-placement="top" title="Agendar cita">
                        <i class="pe pe-7s-plus"></i>
                    </a>

                    <a href="" data-id="{{ $registro->id }}" class="js_comentarios" aria-hidden="true"
                        data-toggle="tooltip" data-placement="top" title="Comentarios"
                        data-fecha="{{ $registro->fecha_programacion }}" data-cliente="{{ $registro->nombre }}">
                        <i class="pe-7s-comment"></i>
                    </a>

                    <a href="" data-id="{{ $registro->id }}" class="js_historial" aria-hidden="true"
                        data-toggle="tooltip" data-placement="top" title="Historial comentarios">
                        <i class="pe-7s-info
pe pe-7s-info"></i>
                    </a>
                </td>
                <td>{{ $registro->id }}</td>
                <td>{{ $registro->intentos }}</td>
                <td>{{ $registro->vehiculo_kilometraje }}</td>
                <td>{{ $registro->placas }}</td>
                <td>{{ $registro->nombre }}</td>
                <td>{{ $registro->correo }}</td>
                <td>{{($registro->telefono_movil)?$registro->telefono_movil:$registro->otro_telefono}}</td>
                <td>{{ $registro->fecha_programacion }}</td>
                <td>{{ $registro->serie }}</td>
                <td>{{ $registro->vehiculo_modelo }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
