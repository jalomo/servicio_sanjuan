<form action="" id="frm_comentarios">
	<input type="hidden" name="id_cita" id="id_cita" value="{{$id_cita}}">
	<input type="hidden" name="fecha" id="fecha" value="{{$fecha}}">
	<input type="hidden" name="cliente" id="fecha" value="{{$cliente}}">
	<!--<div class="row">
		<div class="col-sm-12">
			<label for="">No cumple con el kilometraje</label>
			<input type="checkbox" name="nokm" id="nokm">
		</div>
	</div>-->
	<div class="row">
		<div class="col-sm-6">
			<label for="">Fecha inicio</label>
			<input type="date" class="form-control" value="{{$fecha}}" name="cronoFecha" id="cronoFecha">
            <span class="error error_cronoFecha"></span>
		</div>
		<div class="col-lg-6">
			<label for="">Hora</label>
			<input type="time" class="form-control" value="" name="cronoHora">
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<label>Comentario</label>
			{{$input_comentario}}
			<span class="error error_comentario"></span>
		</div>
	</div>
</form>
<script>
	$('.clockpicker').clockpicker();
	//var fecha_actual = "<?php echo date('Y-m-d') ?>";
	var fecha_actual = "{{$fecha}}";
	$('#datetimepicker2').datetimepicker({
		//minDate: fecha_actual,
        format: 'DD/MM/YYYY',
        icons: {
            time: "far fa-clock",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
        daysOfWeekDisabled: [0],
        locale: 'es'
    });
    $("#cronoFecha").val("");
    $("#cronoFecha").val("{{date_eng2esp_1($fecha)}}");
</script>