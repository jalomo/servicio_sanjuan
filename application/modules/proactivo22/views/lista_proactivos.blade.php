@layout('tema_luna/layout')
@section('contenido')
<h1 class="text-center">Proactivo</h1>
	<div class="row">
		<div class='col-sm-3'>
        	<label for="">Selecciona la fecha</label>
            <input id="fecha" name="fecha" type='date' class="form-control" value="{{$fecha_actual}}" />
            <span class="error_fecha"></span>
        </div>
        <div class="col-sm-2">
        	<br>
        	<button id="buscar" style="margin-top: 8px;" class="btn btn-success">Buscar</button>
        </div>
	</div>
	<div class="row">
		<div class="col-sm-3 ">
        	<button id="enviar-correo" style="margin-top: 8px;" class="btn btn-success">Enviar correo</button>
        </div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div id="tabla-items">
				<!--<h1 class="text-right">Mes: {{$mes}}, semana: {{$semana}}, día: {{$dia}}</h1>-->
				<table id="tbl_proactivo" class="table table-bordered table-striped" cellpadding="0" width="100%">
					<thead>
						<tr class="tr_principal">
							<th>Acciones</th>
              <th>ID</th>
							<th>Intentos</th>
							<th>Kilometraje</th>
							<th>Placas</th>
							<th>Cliente</th>
              <th>Email</th>
							<th>Teléfono</th>
							<th>Fecha</th>
							<th>Serie</th>
              <th>Modelo</th>
						</tr>
					</thead>
					<tbody>
							@foreach($registros as $r => $registro)
								<tr>
									<td>
                    <a href="citas/agendar_cita/0/0/0/{{$registro->id}}" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Agendar cita">
                      <i class="pe pe-7s-plus"></i>
                    </a>
            
                    <a href="" data-id="{{$registro->id}}" class="js_comentarios" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Comentarios" data-fecha="{{$registro->fecha_programacion}}" data-cliente="{{$registro->nombre}}">
                      <i class="pe-7s-comment"></i>
                    </a>
            
                    <a href="" data-id="{{$registro->id}}" class="js_historial" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Historial comentarios">
                      <i class="pe-7s-info
            pe pe-7s-info"></i>
                    </a>
                  </td>
                  <td>{{$registro->id}}</td>
									<td>{{$registro->intentos}}</td>
									<td>{{$registro->vehiculo_kilometraje}}</td>
									<td>{{$registro->placas}}</td>
									<td>{{$registro->nombre}}</td>
                  <td>{{$registro->correo}}</td>
									<td>{{($registro->telefono_movil)?$registro->telefono_movil:$registro->otro_telefono}}</td>
									<td>{{$registro->fecha_programacion}}</td>
									<td>{{$registro->serie}}</td>
                  <td>{{$registro->vehiculo_modelo}}</td>
								</tr>
							@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
@section('scripts')

<script>
	var site_url = "{{site_url()}}";
	var fecha_actual = "{{$fecha_actual}}";
	var cliente = '';
	$('#datetimepicker1').datetimepicker({
        format: 'DD/MM/YYYY',
        icons: {
            time: "far fa-clock",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
        daysOfWeekDisabled: [0],
        locale: 'es'
    });
    $("#fecha").val("{{date_eng2esp_1($fecha_actual)}}");
	inicializar_tabla_local();
	$("body").on("click",'.js_comentarios',function(e){
    e.preventDefault();
       id_cita = $(this).data('id');
       cliente = $(this).data('cliente');
       var url =site_url+"/proactivo/comentarios_reagendar_MAGIC/0";
       //$(this).data('fecha')
       customModal(url,{"id_cita":id_cita,'fecha':$("#fecha").val(),"cliente":cliente},"GET","md",ingresarComentario,"","Guardar","Cancelar","Ingresar comentario","modal1");
    });
    $("body").on("click",'.js_historial',function(e){
    e.preventDefault();
    id_cita = $(this).data('id');
       var url =site_url+"/citas/historial_comentarios_reagendar_magic/0";
       customModal(url,{"id_cita":id_cita},"GET","md","","","","Cerrar","Historial de comentarios","modal1");
    });

    //Creamos el modal para el pdf de la informacion del cliente
    $("body").on("click",'.js_info',function(e){
        e.preventDefault();
        id_cita = $(this).data('id');
        var url = site_url+"/citas/info_cliente_busqueda_shara/";
        customModal(url,{"id_cita":id_cita},"POST","lg","","","","Salir","Ficha Cliente","modal1");
    });
    //Creamos el modal para el pdf de la informacion del cliente
    $("body").on("click",'.js_edit',function(e){
        e.preventDefault();
        idmagic = $(this).data('id');
        var url = site_url+"/proactivo/updatePhones/";
         customModal(url,{"idmagic":idmagic},"GET","md",updatePhones,"","Guardar","Cancelar","Ingresar comentario","modal1");
    });

    function updatePhones(){
    var url =site_url+"/proactivo/updatePhones";
    ajaxJson(url,$("#frm_phones").serialize(),"POST","async",function(result){
      if(isNaN(result)){
        data = JSON.parse( result );
        //Se recorre el json y se coloca el error en la div correspondiente
        $.each(data, function(i, item) {
           $.each(data, function(i, item) {
                      $(".error_"+i).empty();
                      $(".error_"+i).append(item);
                      $(".error_"+i).css("color","red");
                  });
        });
      }else{
        if(result <0){
          ErrorCustom('No se pudo actualizar la información, por favor intenta de nuevo');
        }else{
          ExitoCustom("información actualizada correctamente",function(){
            location.reload();
          })
        }
      }
    });
  }

    //Editar teléfonos
    

  function ingresarComentario(){
    var url =site_url+"/proactivo/comentarios_reagendar_MAGIC";
    ajaxJson(url,$("#frm_comentarios").serialize(),"POST","async",function(result){
      if(isNaN(result)){
        data = JSON.parse( result );
        //Se recorre el json y se coloca el error en la div correspondiente
        $.each(data, function(i, item) {
           $.each(data, function(i, item) {
                      $(".error_"+i).empty();
                      $(".error_"+i).append(item);
                      $(".error_"+i).css("color","red");
                  });
        });
      }else{
        if(result <0){
          ErrorCustom('No se pudo guardar el comentario, por favor intenta de nuevo');
        }else{
          $(".close").trigger('click');
           ExitoCustom("Comentario guardado con éxito",function(){
           	buscarInformacion();
           });

        }
      }
    });
  }
  $("body").on('click','#buscar',function(){
  		buscarInformacion();
  });
  $("body").on('click','#enviar-correo',function(){
  		var url =site_url+"/proactivo/correo_cp";
		ajaxJson(url,{},"POST","async",function(result){
    		ExitoCustom("Correo enviado correctamente");
    	});
  });
  function buscarInformacion(){
    var url =site_url+"/proactivo/index/1";
  	ajaxLoad(url,{"fecha":$("#fecha").val()},"tabla-items","POST",function(){
    	inicializar_tabla_local();
    });
  } 
  function inicializar_tabla_local(){
		 $('#tbl_proactivo').DataTable( {
	        "oLanguage": {
            "oPaginate": {
                "sPrevious": "Anterior",
                "sNext": "Siguiente",
                "sLast": "Última",
                "sFirst": "Primera"
            },
	            "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
	            '<option value="5">5</option>' +
	            '<option value="10">10</option>' +
	            '<option value="20">20</option>' +
	            '<option value="30">30</option>' +
	            '<option value="40">40</option>' +
	            '<option value="50">50</option>' +
	            '<option value="-1">Todos</option>' +
	            '</select> registros',
	            "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
	            "sInfoFiltered": " - filtrados de _MAX_ registros",
	            "sInfoEmpty": "No hay resultados de búsqueda",
	            "sZeroRecords": "No hay registros para mostrar...",
	            "sProcessing": "Espere, por favor...",
	            "sSearch": "Buscar:"
	        },
		    "scrollX": true
	    });
	}
  $("body").on("click",'.js_actualizar_asc',function(e){
    e.preventDefault();
    campo = $(this).data('campo');
    id_input = $(this).data('idcampo');
    serie = $(this).data('serie');
    id = $(this).data('id');
    valor_input = $("#"+id_input).val();

    if(valor_input==''){
      ErrorCustom("Es necesario ingresar el campo");
    }else{
      ConfirmCustom("¿Está seguro de actualizar el registro?", cambiarASC,"", "Confirmar", "Cancelar");

    }

  });
function cambiarASC(){
    var url =site_url+"/proactivo/cambiarASC_Magic";
    
    ajaxJson(url,{"campo":campo,"valor":valor_input,"id":id},"POST","",function(result){
        if(result ==0){
          ErrorCustom('No se pudo actualizar el registro, por favor intenta de nuevo');
        }else{
          
          ExitoCustom("Registro actualizado con éxito",function(){
            $("#"+id_input).parent("td").empty().append(valor_input);
          });
          
        }
    });

  }
</script>
@endsection