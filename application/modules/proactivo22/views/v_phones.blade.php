<form action="" id="frm_phones">
	<input type="hidden" name="idmagic" id="idmagic" value="{{$idmagic}}">
	<div class="row">
		<div class="col-sm-6">
			<label>Teléfono de casa</label>
			{{$input_telefono_casa}}
			<span class="error error_telefono_casa"></span>
		</div>
		<div class="col-sm-6">
			<label>Teléfono oficina</label>
			{{$input_telefono_oficina}}
			<span class="error error_telefono_oficina"></span>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<label>Año</label>
			{{$input_anio}}
			<span class="error error_anio"></span>
		</div>
	</div>
</form>