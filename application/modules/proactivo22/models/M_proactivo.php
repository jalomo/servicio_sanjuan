<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_proactivo extends CI_Model{
  public function __construct(){
    parent::__construct();
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  //Obtener solo los registros de hace 6 meses 
  public function getData6Month($month='',$reprogramado=0,$year=0){
  	return $this->db->where('MONTH(fecha_creacion)',$month)->where('YEAR(fecha_creacion)',$year)->order_by('fecha_creacion','asc')->get('citas')->result();
  }
  //guarda los comentariosa
  public function saveComentario(){
    
       $id= $this->input->post('id_cita');
       $datos = array('comentario' => $this->input->post('comentario'),
                      'id_cita' => $id,
                      'fecha_creacion' =>date('Y-m-d H:i:s'),
                      'id_usuario'=>$this->session->userdata('id_usuario'),
                      'fecha_notificacion' =>date2sql($this->input->post('cronoFecha')).' '.$this->input->post('cronoHora'),
                      'proactivo'=>1
        );
        $this->db->insert('historial_comentarios_cp',$datos);

        //Insertar notificación
        if($this->input->post('comentario')!=''){

          if($this->input->post('cronoFecha')!='' && $this->input->post('cronoFecha')!='cronoHora'){
            $date = date2sql($this->input->post('cronoFecha'));
            $newdate = strtotime ( '+6 month' , strtotime ( $date ) ) ;
            $date = date ( 'Y-m-d' , $newdate );

            $notific['texto']= 'Cliente: '.$this->input->post('cliente').' '.$this->input->post('comentario');
            $notific['id_user'] = $this->session->userdata('id_usuario');
            $notific['estado'] = 1;
            $notific['tipo_notificacion'] = 2;
            $notific['fecha_hora'] = $date.' '.$this->input->post('cronoHora');
          //$notific['url'] = base_url().'index.php/bodyshop/ver_proyecto/'.$this->input->post('idp');
            $notific['estadoWeb'] = 1;
            $this->db->insert(CONST_BASE_PRINCIPAL.'noti_user',$notific);
          }
        }
        
        //Actualizar la variable reprogramado a 1 para saber que lo reprogramaron para otra  fecha

        $this->db->set('fecha_programacion',date2sql($this->input->post('cronoFecha')))->where('idcita',$id)->update('proactivo_citas');
        //Actualizar intentos

        if($this->input->post('comentario')!=''){
          $this->UpdateIntentos($id);
          $array_historial_km = array('idcita'=>$id,
                                             'created_at'=>date('Y-m-d H:i:s'),
                                             'id_usuario' => $this->session->userdata('id_usuario'),
                                             'comentario'=>$this->input->post('comentario'),
                                             'fecha_contacto'=>date2sql($this->input->post('fecha')),
          );
          $this->db->insert('proactivo_historial',$array_historial_km);
        }
        if($this->input->post('no_contactar')){
          //Actualizar automáticamente 6 meses después
          $date = $this->input->post('fecha');
          $newdate = strtotime ( '+6 month' , strtotime ( $date ) ) ;
          $date = date ( 'Y-m-d' , $newdate );
          $dia = date("D", strtotime($date)); 
          //Si el día es sábado agregar un día, si es domingo 2

          if($dia=='Sat'){
            $newdate = strtotime ( '+2 day' , strtotime ( $date ) ) ;
            $date = date ( 'Y-m-d' , $newdate );
          }

          if($dia=='Sun'){
            $newdate = strtotime ( '+1 day' , strtotime ( $date ) ) ;
            $date = date ( 'Y-m-d' , $newdate );
          }
          $this->db->where('idcita',$id)->set('noContactar',1)->set('fecha_programacion',$date)->set('intentos',0)->update('proactivo_citas');
        }

        echo 1;exit();
    }
    public function getComentariosReagendar_magic($id_cita=''){
      return $this->db->where('id_cita',$id_cita)->get('historial_comentarios_cp')->result();

    }

    //Actualizar los intentos
    public function UpdateIntentos($id=''){
	    $q = $this->db->where('idcita',$id)->select('intentos')->from('proactivo_citas')->get();
	    if($q->num_rows()==1){
	      $intentos =  $q->row()->intentos;
	      $this->db->where('idcita',$id)->set('intentos',$intentos+1)->update('proactivo_citas');
	      //Enviar correo ya cuando son mas de 5 intentos
	      if(($intentos+1)>=3){
          //Guardar en el historial > 5 intentos
          $array_historial_intentos = array('idcita'=>$id,
                                             'created_at'=>date('Y-m-d H:i:s'),
                                             'id_usuario' => $this->session->userdata('id_usuario'),
          );
          $this->db->insert('proactivo_historial_5intentos',$array_historial_intentos);
	      	$datos['informacion'] = $this->getDataCita($id);
	      	$cuerpo = $this->blade->render('correo_intentos',$datos,true);
        	enviar_correo(strtolower($datos['informacion']->datos_email),"¡Notificación Contacto Proactivo!",$cuerpo,array());
	      }
	    }
    }
    public function getDataCita($id=''){
    	return $this->db->where('id_cita',$id)->get('citas')->row();
    }

    public function getDataByDate($date=''){
    return $this->db->where('fecha_programacion',$date)
                  ->select('pm.idcita as id,pm.intentos,pm.fecha_programacion,c.vehiculo_placas as placas,c.datos_nombres as nombre,c.datos_apellido_paterno as ap,c.datos_apellido_materno as am,c.datos_telefono,c.datos_email as correo,c.vehiculo_numero_serie as serie,c.vehiculo_modelo,o.vehiculo_kilometraje,o.telefono_movil,o.otro_telefono')
                  ->join('citas c','pm.idcita = c.id_cita')
                  ->join('ordenservicio o','o.id_cita=c.id_cita','left')
                  ->where('intentos <',3)
                  ->get('proactivo_citas pm')
                  ->result();
    }
    //Obtener lo que ya se ha generado para hacer el proactivo
    function getProactivoMes(){
      return $this->db->get('proactivo_mes')->result();
    }

    //Validar si ya está generado el contacto por mes
    function validaContactByMesYear($mes='',$anio=''){
      $q = $this->db->where('mes',$mes)->where('anio',$anio)->get('proactivo_mes');
      if ($q->num_rows()==1) {
        return false;
      }else{
        return true;
      }
    }
    public function getNameMes($mes=''){
    switch ($mes) {
      case '01':
        return 'Enero';
      break;
      case '02':
        return 'Febrero';
      break;
      case '03':
        return 'Marzo';
      break;
      case '04':
        return 'Abril';
      break;
      case '05':
        return 'Mayo';
      break;
      case '06':
        return 'Junio';
      break;
      case '07':
        return 'Julio';
      break;
      case '08':
        return 'Agosto';
      break;
      case '09':
        return 'Septiembre';
      break;
      case '10':
        return 'Octubre';
      break;
      case '11':
        return 'Noviembre';
      break;
      case '12':
        return 'Diciembre';
      break;
      default:
        return '';
        break;
    }
  }
  
  
}
