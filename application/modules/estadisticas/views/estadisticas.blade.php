@layout('tema_luna/layout')
@section('contenido')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<form action="" id="frm">
	<div class="row">
        <div class="col-md-3 col-sm-6">
            <label for="">Asesor</label>
            {{$drop_asesores}}
        </div>
		<div class='col-md-3 col-sm-6'>
            <label for="">Fecha inicio</label>
            <input type="date" class="form-control" name="finicio" id="finicio" value="">
            <span style="color: red" class="error error_fini"></span>
        </div>
        <div class='col-md-3 col-sm-6'>
            <label for="">Fecha Fin</label>
            <input type="date" class="form-control" name="ffin" id="ffin" value="">
            <span style="color: red" class="error error_ffin"></span>
        </div>
        <div class="col-md-3 col-sm-6">
            <button type="button" style="margin-top: 30px" id="buscar" class="btn btn-success">Buscar</button>
        </div>
	</div>
</form>
<div class="row">
	<div class="col-sm-12">
		<h2 class="text-center"></h2>
		<div id="container" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
	</div>
</div>
@endsection

@section('scripts')
<script>
	 $("#datetimepicker1").on("dp.change", function (e) {
      $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
    });
    $("#datetimepicker2").on("dp.change", function (e) {
        $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
    });
	var site_url = "{{site_url()}}";
	$('.date').datetimepicker({
      format: 'DD/MM/YYYY',
      icons: {
          time: "far fa-clock",
          date: "fa fa-calendar",
          up: "fa fa-arrow-up",
          down: "fa fa-arrow-down"
      },
       locale: 'es'
    });


	// Create the chart
var chart = Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Estadísticas citas'
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.0f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
    },

    series: [
        {
            name: "#Citas",
            colorByPoint: true,
            data: [
                {
                    name: "Citas Generadas",
                    y: {{$total_citas}},
                    drilldown: "Citas generadas"
                },
                {
                    name: "Citas efectivas",
                    y: {{$total_citas_efectivas}},
                    drilldown: "Citas efectivas"
                },
                {
                    name: "# de no show",
                    y: {{$total_citas_no_efectivas}},
                    drilldown: "# de no show"
                },
                {
                    name: "# de no show recuperados",
                    y: {{$total_citas_reagendadas}},
                    drilldown: "# de no show recuperados"
                },
                
            ]
        }
    ]
});
$('#buscar').on('click', function() {
    if($("#finicio").val()=='' || $("#finicio").val()==''){
        ErrorCustom("Es necesario ingresar ambas fechas");
    }else{
        var url =site_url+"/estadisticas";
        ajaxJson(url,$("#frm").serialize(),"POST","sync",function(result){
            result = JSON.parse( result );
            actualizarChart(result.total_citas,
                                result.total_citas_efectivas,
                                result.total_citas_no_efectivas,
                                result.total_citas_reagendadas
                );
            
        });
        
    }
    
    
});
function actualizarChart(total_citas=0,total_citas_efectivas=0,total_citas_no_efectivas=0,total_citas_reagendadas=0){
    console.log(total_citas,total_citas_efectivas,total_citas_no_efectivas,total_citas_reagendadas);
    var series= [
                        {
                            name: "Citas Generadas",
                            y: parseInt(total_citas),
                            drilldown: "Citas generadas"
                        },
                        {
                            name: "Citas efectivas",
                            y: parseInt(total_citas_efectivas),
                            drilldown: "Citas efectivas"
                        },
                        {
                            name: "# de no show",
                            y: parseInt(total_citas_no_efectivas),
                            drilldown: "# de no show"
                        },
                        {
                            name: "# de no show recuperados",
                            y: parseInt(total_citas_reagendadas),
                            drilldown: "# de no show recuperados"
                        },
                        
                    ]                   
    chart.update({
            series: [
                {
                    name: "#Citas",
                    colorByPoint: true,
                    data: series
                }
            ]
    })
    chart.redraw()
    
}
</script>
@endsection