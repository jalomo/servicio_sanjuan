<?php
//https://techarise.com/import-excel-file-mysql-codeigniter/
defined('BASEPATH') OR exit('No direct script access allowed');


class Estadisticas extends MX_Controller {
public function __construct()
{
  parent::__construct();
  $this->load->helper(array('general'));
  $this->load->model('citas/m_catalogos','mcat');
  $this->load->model('m_estadisticas','me');
}
  public function index() {
    if($this->input->post()){
      echo json_encode(array(
            'total_citas'=>$this->me->getCitasGeneradas(),
            'total_citas_efectivas'=>$this->me->getCitasEfectivasNoEfectivas(true),
            'total_citas_no_efectivas'=>$this->me->getCitasEfectivasNoEfectivas(false),
            'total_citas_reagendadas'=>$this->me->getCitasReagendadas()
      ));
      exit();
    }
    $data['drop_asesores']=form_dropdown('id_asesor',array_combos($this->mcat->get('operadores'),'nombre','nombre',true),"",'class="form-control busqueda" id="id_asesor" ');
    $data['total_citas'] = $this->me->getCitasGeneradas();
    $data['total_citas_efectivas'] = $this->me->getCitasEfectivasNoEfectivas(true);
    $data['total_citas_no_efectivas'] = $this->me->getCitasEfectivasNoEfectivas(false);
    $data['total_citas_reagendadas'] = $this->me->getCitasReagendadas();
    $this->blade->set_data($data)->render('estadisticas');
  }
  
}
