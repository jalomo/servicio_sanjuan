<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_estadisticas extends CI_Model{
	private $_batchImport;
	public function __construct(){
		parent::__construct();
	}
  public function getCitasGeneradas(){
    if($this->input->post()){
      $this->db->where('fecha >=',date2sql($_POST['finicio']));
      $this->db->where('fecha <=',date2sql($_POST['ffin']));
      if($_POST['id_asesor']!=''){
        $this->db->where('asesor',$_POST['id_asesor']);
      }
    }

    return $this->db->select('count(id_cita)  as total_citas')->get('citas ')->row()->total_citas;
  }
  public function getCitasEfectivasNoEfectivas($mostrarEfectivas=true){
    if($mostrarEfectivas){
      $this->db->where_in('a.id_status_cita',array(2,3));
    }else{
      $this->db->where_in('a.id_status_cita',array(4));
    }
    if($this->input->post()){
      $this->db->where('c.fecha >=',date2sql($_POST['finicio']));
      $this->db->where('c.fecha <=',date2sql($_POST['ffin']));
      if($_POST['id_asesor']!=''){
        $this->db->where('c.asesor',$_POST['id_asesor']);
      }
    }
    
    return $this->db->select('count(c.id_cita) as total_citas_efectivas')
                  ->join('aux a','c.id_horario = a.id')
                  ->get('citas c')
                  ->row()
                  ->total_citas_efectivas;
  }
  public function getCitasReagendadas(){
    if($this->input->post()){
      $this->db->where('c.fecha >=',date2sql($_POST['finicio']));
      $this->db->where('c.fecha <=',date2sql($_POST['ffin']));
      if($_POST['id_asesor']!=''){
        $this->db->where('c.asesor',$_POST['id_asesor']);
      }
    }

    return $this->db->select('count(c.id_cita) as total_reagendadas')
                  ->where('c.reagendada',1)
                  ->get('citas c')
                  ->row()
                  ->total_reagendadas;
    }
  
} 