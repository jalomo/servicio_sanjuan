<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dms extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('citas/m_catalogos', 'mcat');
    $this->load->model('m_dms');
    $this->load->library('table');
    $this->load->library('curl');
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  //Obtiene los datos de la api de intelisis con base al #cliente
  public function datosClienteAPIByNombre()
  {
    if (function_exists("set_time_limit") == TRUE and @ini_get("safe_mode") == 0) {
      @set_time_limit(1000000);
    }
    //Ismodal es para mostrar el modal en caso de que sean varios clientes
    $ismodal = $this->input->post('ismodal');

    if (!$ismodal) {
      $datos_clientes = $this->getClientesNum($this->input->post('nombre'));
      if (is_array($datos_clientes)) {
        if (count($datos_clientes) == 1) {
          echo json_encode(array('cantidad' => 1, 'datos' => $datos_clientes));
        } else {
          echo json_encode(array('cantidad' => count($datos_clientes), 'datos' => array()));
        }
      } else {
        echo json_encode(array('cantidad' => 0, 'datos' => array()));
      }
    } else {
      $data['datos_clientes'] = $this->getClientesNum($this->input->post('nombre'));
      $this->blade->render('modal_clientes_nombre', $data);
    }
  }
  //Obtiene los datos de la api de intelisis con base al #cliente
  public function datosByCliente()
  {
    if (function_exists("set_time_limit") == TRUE and @ini_get("safe_mode") == 0) {
      @set_time_limit(100000);
    }
    $datos_clientes = $this->getClientesNum($this->input->post('cliente'));

    $indice = 0;
    if (is_array($datos_clientes)) {
      if (count($datos_clientes) == 1) {
        echo json_encode(array('cantidad' => 1, 'datos' => $datos_clientes[0]));
      } else {
        foreach ($datos_clientes as $key => $value) {
          if ($value['cliente'] === $this->input->post('cliente')) {
            $indice = $key;
            break;
          }
        }
        echo json_encode(array('cantidad' => 1, 'datos' => $datos_clientes[$indice]));
      }
    } else {
      echo json_encode(array('cantidad' => 0, 'datos' => array()));
    }
  }
  //Obtiene los datos de la api de intelisis con base al serie o placas
  public function datosClienteBySeriePlacas()
  {
    if (function_exists("set_time_limit") == TRUE and @ini_get("safe_mode") == 0) {
      @set_time_limit(1000000);
    }
    $datos_clientes = $this->getClienteBySerie($_POST['tipo'], $_POST['data']);
    if (is_array($datos_clientes)) {
      if (count($datos_clientes) == 1) {
        echo json_encode(array('cantidad' => 1, 'datos' => $datos_clientes[0]));
      } else {
        echo json_encode(array('cantidad' => count($datos_clientes), 'datos' => array()));
      }
    } else {
      echo json_encode(array('cantidad' => 0, 'datos' => array()));
    }
  }
  public function getInfoByUnidad()
  {
    echo json_encode(array('cantidad' => 1, 'datos' => $this->curl->getinfoGet(CONST_PREPEDIDOS_BY_ID . '/' . $_POST['id'][0])));;
  }
  //Mostrar la lista de vehículos
  public function buscar_vehiculos()
  {
    $data['datos_clientes'] = $this->getClientesNum($this->input->post('cliente'));
    $this->blade->render('lista_vehiculos', $data);
  }
  //Obtener clientes de intelisis por nombre
  public function getClienteBySerie($tipo = 'placas', $data = '')
  {
    //$tipo='placas/vin' para hacer la búsqueda por placas o # serie
    return $this->curl->getInfoPOST(CONST_BUSCAR_CLIENTE_SERIE, array($tipo => $data));
  }
  //Obtener clientes de intelisis con el #cliente
  public function getClientesNum($cliente = '')
  {
    return $this->curl->getInfoPOST(CONST_BUSCAR_CLIENTE_NOMBRE, array('nombre' => $cliente));
  }
  //Inventario
  public function inventario()
  {
    $this->blade->render('inventario');
  }
  public function buscar_inventario()
  {
    $url = CONST_DMS_INVENTARIO . '?' . $_POST['tipo_busqueda'] . '=' . $_POST['busqueda_inventario'];
    $data['inventario'] = $this->curl->getinfoGet($url);
    $this->blade->render('busqueda_inventario', $data);
  }
  public function info_cliente_dms()
  {
    $data['id_cita'] = $this->input->post('id_cita');
    $this->blade->render('info_cliente_busqueda', $data);
  }
  public function ordenes_serie()
  {
    //Creamos contenedor
    $contenedor = [];
    //Evitamos posibles fallos al momento de ejecutar las consultas

    try {

      $serie = $_POST["serie"];
      //Consultamos los presupuestos nuevos
      //Recuperamos el numero de orden asociados a la serie
      $precarga_presupuesto = $this->m_dms->presupuesto_por_serie($serie);
      foreach ($precarga_presupuesto as $row) {
        $contenedor[] = array(
          'Contenido' => $this->recuperar_refacciones_presupuesto($row->id_cita)
        );
      }
    } catch (Exception $e) {
    }
    $data['tabla_refacciones'] = $this->m_dms->getTablaRefacciones($contenedor);
    $this->blade->render('tabla_refacciones', $data);
  }

  //Recuperamos los items del presupuesto
  public function recuperar_refacciones_presupuesto($id_cita = '')
  {
    $contenido = [];
    //Recuperamos las refacciones
    $query = $this->mcat->get_result_field("id_cita", $id_cita, "identificador", "M", "presupuesto_multipunto");
    foreach ($query as $row) {
      if (($row->activo == "1") && ($row->autoriza_cliente == "0") && ($row->solicita_psni == "0") && ($row->solicita_operacion == "0")) {
        $contenido[] = $row;
      }
    }

    return $contenido;
  }
  //Guardar un nuevo cliente
  public function newCustomer()
  {
    $datos_cliente = [
      'nombre' => 'Luis Alberto',
      'apellido_paterno' => 'Pita',
      'apellido_materno' => 'Vázquez',
      'rfc' => 'PIVL911222V71'
    ];
    $data['info'] = ($this->curl->curlPost('api/clientes/crear-regresar',$datos_cliente ));
    debug_var($data);
  }
}
