<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_dms extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function dms_cerrar_orden($numero_orden=''){
		return $this->curl->getInfoPOST(CONST_DMS_CERRAR_ORDEN,array('numero_orden'=>$numero_orden));
	}
	public function presupuesto_por_serieBK()
	{
		
		$sql = "SELECT prer.id_cita FROM citas AS c ".
		"INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
		//Presupuesto Multipunto (nuevas cotizaciones)
		"LEFT JOIN presupuesto_registro AS prer ON prer.id_cita = c.id_cita ".
		"WHERE prer.serie = ".$_POST['serie']." AND prer.acepta_cliente = 'No' OR 'Val'".
		"ORDER BY o.id DESC ".
		"LIMIT 200";
		
		// $sql = "SELECT id_cita FROM presupuesto_registro
		// WHERE serie = ".$_POST['serie']." AND (acepta_cliente = 'No' OR acepta_cliente = 'Val')
		// ORDER BY id DESC LIMIT 200";


		$query = $this->db->query($sql);
		//lquery();die();
		$data = $query->result();
		return $data;
	}
	public function presupuesto_por_serie()
	{
		$sql = "SELECT prer.id_cita FROM citas AS c ".
		"INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
		//Presupuesto Multipunto (nuevas cotizaciones)
		"LEFT JOIN presupuesto_registro AS prer ON prer.id_cita = c.id_cita ".
		"WHERE prer.serie = ? AND prer.acepta_cliente = ? OR ?".
		"ORDER BY o.id DESC ".
		"LIMIT 200";
		$query = $this->db->query($sql,array($_POST['serie'],'No','Val'));
		$data = $query->result();
		return $data;
	}
	public function getTablaRefacciones($datos=array()){
		$tmpl = array (
			'table_open' => '<table border="0" id="tbl" cellspacing="0" class="table table-bordered table-striped table-hover">');
		$this->table->set_template($tmpl);
		$this->table->set_heading('Autorizar','Cantidad','Descripción','#Pieza','Precio unitario','Horas MO','Costo MO','Total');
		if(count($datos)>0){
			foreach($datos[0] as $d => $contenido){
				foreach($contenido as $r => $result){
					$row=array();
					$row[] = '<input name="refacciones['.$result->id.']" class="js_autorizar_presupuesto" type="checkbox" value="'.$result->id.'"></input>';
					$row[]=$result->cantidad;
					$row[]=$result->descripcion;
					$row[]=$result->num_pieza;
					$row[]=$result->costo_pieza;
					$row[]=$result->horas_mo;
					$row[]=$result->costo_mo;
					$row[]=round($result->total_refacion,2);
					$this->table->add_row($row);        
				}

			}
			return $this->table->generate();
		}else{
			return null;
		}
		
	}
	public function updateRefaccionesAutorizadas($datos=[]){
		foreach ($datos as $key => $refaccion) {
			$info_update = array('solicita_operacion' => 1,
								'fecha_operacion'=> date('Y-m-d H:i:s'),
								'fecha_actualiza'=> date('Y-m-d H:i:s')
							);
			$this->db->where('id',$key)->update('presupuesto_multipunto',$info_update);
		}
	}
	public function newCustomer($datos_cliente = array())
	{
		$data['info'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/clientes/crear-regresar', $datos_cliente));
		return $data['info']->id;
	}
	public function crear_cuenta_dms($id_cita, $id_tipo_orden)
	{
		switch ($id_tipo_orden) {
			case 3:
				$this->crear_cuenta_hojalateria($id_cita);
				break;
			case 2:
				$this->crear_cuenta_garantia($id_cita);
				break;
			case 1:
				$this->crear_cuenta_servicios($id_cita);
				break;

			default:
				return '';
				break;
		}
	}
	public function crear_cuenta_hojalateria($id_cita = '')
	{
		$cliente_id = $this->principal->getGeneric('id_cita', $id_cita, 'ordenservicio', 'cliente_id_dms');
		//Obtener todos los costos
		$costos = procesarResponseApiJsonToArray($this->curl->curlPost(CONST_GET_COSTOS_ORDEN, ['id_cita' => $id_cita], true));
		$datos_poliza = [
			'total_refacciones' => ($costos->total_refacciones != '') ? $costos->total_refacciones : 0,
			'total_mano_obra' => ($costos->total_mano_obra != '') ? $costos->total_mano_obra : 0,
			'total_iva_facturado' => ($costos->total_iva_facturado != '') ? $costos->total_iva_facturado : 0,
			'total_costo_refaccion' => ($costos->total_costo_refacion != '') ? $costos->total_costo_refacion : 0,
			// 'total_descuento_refacciones' => ($costos->total_descuento_refacciones != '') ? $costos->total_descuento_refacciones : 0,
			// 'total_descuento_mano_obra' => ($costos->total_descuento_mano_obra != '') ? $costos->total_descuento_mano_obra : 0,
			'total_cliente_servicios' => ($costos->total_cliente_servicios != '') ? $costos->total_cliente_servicios : 0,
			'total_inventario_proceso' => ($costos->total_inventario_proceso != '') ? $costos->total_inventario_proceso : 0,
		];

		$venta_total = (float)$datos_poliza['total_cliente_servicios'] ;
		$datos_cuenta = [
			'cliente_id' => $cliente_id,
			'concepto' => 'Hojalatería',
			'numero_orden' => $id_cita,
			'estatus_cuenta_id' => 1,
			'fecha' => date('Y-m-d'),
			'plazo_credito_id' => 14,
			'tipo_forma_pago_id' => 1,
			'tipo_pago_id' => 1,
			'tipo_proceso' => 9,
			'venta_total' => $venta_total, //Cambia
		];
		$response = procesarResponseApiJsonToArray($this->curl->curlPost('api/cuentas-por-cobrar/set-complemento-cxc', $datos_cuenta));
		//Actualizar los campos en la orden
		$this->db->where('id_cita',$id_cita)->set('folio_id_dms',$response->folio_id)->set('id_dms',$response->id)->update('ordenservicio');
		$cuenta_id = $response->id;
		$datos_poliza['cuenta_por_cobrar_id'] = $cuenta_id;

		$this->crear_poliza($datos_poliza, 'api/asientos/poliza-hojalateria');
	}
	public function crear_cuenta_garantia($id_cita = '')
	{
		$cliente_id = $this->principal->getGeneric('id_cita', $id_cita, 'ordenservicio', 'cliente_id_dms');
		//Obtener todos los costos
		$costos = procesarResponseApiJsonToArray($this->curl->curlPost(CONST_GET_COSTOS_ORDEN, ['id_cita' => $id_cita], true));
		$datos_poliza = [
			'total_refacciones' => ($costos->total_refacciones != '') ? $costos->total_refacciones : 0,
			'total_mano_obra' => ($costos->total_mano_obra != '') ? $costos->total_mano_obra : 0,
			'total_iva_facturado' => ($costos->total_iva_facturado != '') ? $costos->total_iva_facturado : 0,
			'total_costo_refaccion' => ($costos->total_costo_refacion != '') ? $costos->total_costo_refacion : 0,
			'total_inventario_proceso' => ($costos->total_inventario_proceso != '') ? $costos->total_inventario_proceso : 0,
			
		];

		$datos_poliza['total_garantias_por_cobrar'] = (float)$datos_poliza['total_refacciones'] + (float)$datos_poliza['total_mano_obra'];



		$venta_total = (float)$datos_poliza['total_refacciones'] + (float)$datos_poliza['total_mano_obra'] + (float)$datos_poliza['total_garantias_por_cobrar'] + (float)$datos_poliza['total_inventario_proceso'] + (float)$datos_poliza['total_iva_facturado'] + (float)$datos_poliza['total_costo_refaccion'];
		$datos_cuenta = [
			'cliente_id' => $cliente_id,
			'concepto' => 'Garantía',
			'numero_orden' => $id_cita,
			'estatus_cuenta_id' => 1,
			'fecha' => date('Y-m-d'),
			'plazo_credito_id' => 14,
			'tipo_forma_pago_id' => 1,
			'tipo_pago_id' => 1,
			'tipo_proceso' => 10,
			'venta_total' => $venta_total, 
		];
		$response = procesarResponseApiJsonToArray($this->curl->curlPost('api/cuentas-por-cobrar/set-complemento-cxc', $datos_cuenta));
		//Actualizar los campos en la orden
		$this->db->where('id_cita',$id_cita)->set('folio_id_dms',$response->folio_id)->set('id_dms',$response->id)->update('ordenservicio');
		$cuenta_id = $response->id;
		$datos_poliza['cuenta_por_cobrar_id'] = $cuenta_id;

		$this->crear_poliza($datos_poliza, 'api/asientos/poliza-garantias');
	}
	public function crear_cuenta_servicios($id_cita = '')
	{
		$cliente_id = $this->principal->getGeneric('id_cita', $id_cita, 'ordenservicio', 'cliente_id_dms');
		//Obtener todos los costos
		$costos = procesarResponseApiJsonToArray($this->curl->curlPost(CONST_GET_COSTOS_ORDEN, ['id_cita' => $id_cita], true));
		//echo '<pre>';
		// print_r($costos);die();
		$datos_poliza = [
			'total_refacciones' => ($costos->total_refacciones != '') ? $costos->total_refacciones : 0,
			'total_mano_obra' => ($costos->total_mano_obra != '') ? $costos->total_mano_obra : 0,
			'total_iva_facturado' => ($costos->total_iva_facturado != '') ? $costos->total_iva_facturado : 0,
			'total_costo_refaccion' => ($costos->total_costo_refacion != '') ? $costos->total_costo_refacion : 0,
			// 'total_descuento_refacciones' => ($costos->total_descuento_refacciones != '') ? $costos->total_descuento_refacciones : 0,
			// 'total_descuento_mano_obra' => ($costos->total_descuento_mano_obra != '') ? $costos->total_descuento_mano_obra : 0,
			'total_cliente_servicios' => ($costos->total_cliente_servicios != '') ? $costos->total_cliente_servicios : 0,
			'total_inventario_proceso' => ($costos->total_inventario_proceso != '') ? $costos->total_inventario_proceso : 0,
		];


		// debug_var($datos_poliza);
		$venta_total = (float)$datos_poliza['total_cliente_servicios'] ;
		// echo $venta_total;die();
		$datos_cuenta = [
			'cliente_id' => $cliente_id,
			'concepto' => 'Servicios',
			'numero_orden' => $id_cita,
			'estatus_cuenta_id' => 1,
			'fecha' => date('Y-m-d'),
			'plazo_credito_id' => 14,
			'tipo_forma_pago_id' => 1,
			'tipo_pago_id' => 1,
			'tipo_proceso' => 8,
			'venta_total' => $venta_total, //Cambia
		];
		$response = procesarResponseApiJsonToArray($this->curl->curlPost('api/cuentas-por-cobrar/set-complemento-cxc', $datos_cuenta));
		//Actualizar los campos en la orden
		$this->db->where('id_cita',$id_cita)->set('folio_id_dms',$response->folio_id)->set('id_dms',$response->id)->update('ordenservicio');
		$cuenta_id = $response->id;
		$datos_poliza['cuenta_por_cobrar_id'] = $cuenta_id;

		$this->crear_poliza($datos_poliza, 'api/asientos/poliza-servicio');
	}
	public function crear_poliza($datos_poliza, $url)
	{
		$response = procesarResponseApiJsonToArray($this->curl->curlPost($url, $datos_poliza));
		return $response;
	}

} 