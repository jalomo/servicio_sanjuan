<table class="table table-striped table-responsive" cellpadding="0" width="100">
    <thead>
        <tr>
            <th>Identificación</th>
            <th>Clave Unidad</th>
            <th>Clave servicio</th>
            <th>Descripción</th>
            <th>Precio</th>
            <th>Cantidad</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($inventario))
            @foreach($inventario as $i => $data)
                <tr>
                    <td>{{$data->no_identificacion}}</td>
                    <td>{{$data->clave_unidad}}</td>
                    <td>{{$data->clave_prod_serv}}</td>
                    <td>{{$data->descripcion}}</td>
                    <td>{{$data->precio_factura}}</td>
                    <td>{{$data->cantidad_actual}}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="6" class="text-center"> No se encontraron registros ...</td>
            </tr>
        @endif
    </tbody>
</table>    