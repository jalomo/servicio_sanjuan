<div style="overflow-y: scroll;height: 400px;" class="row">
	<div class="col-sm-12">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Acciones</th>
					<th>Cliente</th>
					<th>Nombre</th>
					<th>Correo electrónico</th>
				</tr>
			</thead>
			<tbody>
				@if(count($datos_clientes)>0)
				@foreach($datos_clientes as $c =>$cliente)
					<tr>	
						<td>
							<a class="seleccionar_cliente" href="#" data-cliente="{{$cliente['numero_cliente']}}">Seleccionar</a>
						</td>
						<?php $nombre_cliente = $cliente['nombre'].' '.$cliente['apellido_paterno'].' '.$cliente['apellido_materno'] ?>
						<td>{{$cliente['numero_cliente']}}</td>
						<td>{{$nombre_cliente}}</td>
						<td>{{$cliente['correo_electronico']}}</td>
					</tr>
				@endforeach
				@else
					<tr class="text-center">
						<td colspan="4">No hay registros para mostrar...</td>
					</tr>
				@endif
				
			</tbody>
		</table>
	</div>
</div>