<table class="table table-bordered table-striped" cellspacing="0" width="100%">
	<thead>
		<tr>
			<td>Acciones</td>
			<td>Serie</td>
			<td>Placas</td>
			<td>Marca</td>
			<td>Color</td>
		</tr>
	</thead>
	<tbody>

		@if(count($datos_clientes)>0)
			@foreach($datos_clientes as $data => $info)
				@if(isset($info['vehiculo']))
					@foreach($info['vehiculo'] as $i => $vehiculo)
					<tr>
						<td><a href="#" class="buscar_serie" data-serie="{{$vehiculo['vin']}}">Seleccionar</a></td>
						<td>{{$vehiculo['vin']}}</td>
						<td>{{$vehiculo['placas']}}</td>
						<td>{{$vehiculo['marca']}}</td>
						<td>{{$vehiculo['color']}}</td>
					</tr>
					@endforeach
				@else
					<tr>
						<td class="text-center">
							No hay registros para mostrar...
						</td>
					</tr>
				@endif
				
			@endforeach
		@else
			<tr>
				<td class="text-center">
					No hay registros para mostrar...
				</td>
			</tr>
		@endif
	</tbody>
</table>