<form id="frm-busqueda-inventario">
<div class="row">
    <div class="col-sm-2">
        <label>Buscar por:</label> 
    </div>
    <div class="col-sm-2">
        <input type="radio" id="busqueda_desc" name="tipo_busqueda" value="descripcion" checked="true"><label for="busqueda_desc" style="margin-left: 10px">Descripción</label>
    </div>
    <div class="col-sm-4">
       <input type="radio" id="busqueda_id" name="tipo_busqueda" value="no_identificacion"><label for="busqueda_id" style="margin-left: 10px"># Identificación</label>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <input type="text" name="busqueda_inventario" id="busqueda_inventario" class="form-control">
        <span class="error error_busqueda_inventario"> </span>
    </div>
     <div class="col-md-2">
      <button type="button" id="btn-buscar-inventario" class="btn btn-success">Buscar</button>
    </div>
</div>
</form>
<div class="row">
    <div class="col-sm-12">
        <div id="tabla-busqueda"></div>
    </div>
</div>