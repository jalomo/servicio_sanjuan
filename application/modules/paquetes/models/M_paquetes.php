<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_paquetes extends CI_Model{
  public function __construct(){
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
  }
  public function getManteminientoPreventivo(){
    $this->db->where('idmodelo',$this->input->post('modelo'));
    $this->db->where('anio',$this->input->post('anio'));
    $this->db->where('id_tipo',$this->input->post('tipo_preventivo'));

    $this->db->where('codigo_ereact',$this->input->post('codigo_mo'));
    $this->db->where('id_motor',$this->input->post('motor_preventivo'));

    $this->db->where('codigo',$this->input->post('codigo'));
    $this->db->where('id_descripcion',$this->input->post('descripcion'));
    $this->db->where('id_catalogo',$this->input->post('catalogo_preventivo'));
    return $this->db->get('v_mantenimiento_preventivo')->row();
  }
  //guarda los operadores
  public function guardarPreventivo(){
     $id= $this->input->post('id');
     $datos = array('codigo' => $this->input->post('codigo'),
                    'excel_ford'=>0,
                    'id_usuario'=> $this->session->userdata('id_usuario'),
                    'idmodelo' => $this->input->post('idmodelo'),
                    'id_tipo' => $this->input->post('id_tipo'),
                    'anio' => $this->input->post('anio'),
                    'id_motor' => $this->input->post('id_motor'),
                    'id_descripcion' => $this->input->post('id_descripcion'),
                    'parte' => $this->input->post('parte'),
                    'cantidad' => $this->input->post('cantidad'),
                    'tipomo' => $this->input->post('tipomo'),
                    'codigo_ereact' => $this->input->post('codigo_ereact'),
                    'descripcion' => $this->input->post('descripcion'),
                    'tiempo' => $this->input->post('tiempo'),
                    'precio' => $this->input->post('precio'),
                    'costo' => $this->input->post('costo'),
                    'id_catalogo' => $this->input->post('id_catalogo'),
      );
      if($id==0){
          $exito = $this->db->insert('preventivo',$datos);
      }else{
          $exito = $this->db->where('id',$id)->update('preventivo',$datos);
      }
        if($exito){
          echo 1;die();
        } else{
          echo 0; die();
        }
  }
  //Obtener la clave del modelo y servicio
  public function getClavePaquete($idservicio='',$idmodelo=''){
    $q = $this->db->where('idservicio',$idservicio)->where('idmodelo',$idmodelo)->get('paquetes');
    if($q->num_rows()==1){
      return $q->row()->clave;
    }else{
      return '';
    }
  }
  //Obtener la clave del modelo y servicio
  public function getPrecioPaqueteByClave($clave=''){
    $q = $this->db->where('clave',$clave)->get('precios_paquetes');
    if($q->num_rows()==1){
      return $q->row()->total;
    }else{
      return '';
    }
  }
  
  
}
