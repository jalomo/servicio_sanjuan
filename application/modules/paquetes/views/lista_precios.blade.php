@layout('tema_luna/layout')
@section('contenido')
<h2>Lista de claves y precios</h2>
<a href="{{site_url('paquetes/agregar_precios_claves')}}" class="btn btn-info pull-right">Agregar registro</a>
<br>
<br>
<div class="row">
	<div class="col-sm-12">
		<table id="tbl_precios" class="table table-hover table-striped" style="width: 100%">
			<thead>
				<tr class="tr_principal">
					<th>Clave</th>
					<th>Descripción</th>
					<th>$ Mano de obra</th>
					<th>$ Refacciones</th>
					<th>$ Total</th>
				</tr>
			</thead>
			<tbody>
				@foreach($precios as $p => $precio)
					<tr>
						<td>{{$precio->clave}}</td>
						<td>{{$precio->descripcion}}</td>
						<td>{{$precio->mo}}</td>
						<td>{{$precio->ref}}</td>
						<td>{{$precio->total}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('scripts')
<script>
	inicializar_tabla("#tbl_precios");
</script>
@endsection