<form action="" id="frm-paquetes">
<input type="hidden" id="idorden" name="idorden" value="{{$idorden}}">
<input type="hidden" id="tipo" name="tipo" value="{{$tipo}}">
<input type="hidden" id="temporal_save" name="temporal_save" value="{{$temporal_save}}">
<div class="row">
	<div class="col-sm-6">
        <label for=""></label>
        <div style="margin-top: 10px" class="alert alert-info alert-dismissible fade show" role="alert">
        <strong>Precios SIN IVA</strong>.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
  	</div>
</div>
<div class="row">
	<div class="col-sm-3">
		<label for="">Modelo</label>
		{{$drop_idmodelo}}
		<span class="error error_idmodelo"></span>
	</div>
	<div class="col-sm-3">
		<label for="">Clave</label>
		{{$drop_clave}}
		<span class="error error_clave"></span>
	</div>
	<div class="col-sm-3">
		<label for="">Servicio</label>
		{{$drop_idservicio}}
		<span class="error error_idservicio"></span>
	</div>
</div>
<br>
<div id="div_descripciones">
	<span></span>
	<input type="hidden" id="descripcion_productos" name="descripcion_productos">
</div>
<div id="div_descripciones_totales">
	<span></span>
</div>

<span id="noregistros" style="color: red;text-align: right;"></span>
<hr>
<br>
<div class="row">
	<div class="col-sm-2">
		<label for="">Cantidad</label>
		{{$input_cantidad}}
		<span class="error error_cantidad_item"></span>
	</div>
	<div class="col-sm-4">
		<label for="">Descripción</label>
		{{$input_descripcion}}
		<span class="error error_descripcion_item"></span>
	</div>
	<div class="col-sm-2">
		<label for="">Precio</label>
		{{$input_precio}}
		<span class="error error_precio_item"></span>
	</div>
	<div class="col-sm-2">
		<label for="">Total horas</label>
		{{$input_total_horas}}
		<span class="error error_total_horas"></span>
	</div>
	<div class="col-sm-2">
		<label for="">Costo hora</label>
		{{$input_costo_hora}}
		<span class="error error_costo_hora"></span>
	</div>
</div>
<div class="row ">
	<div class="col-sm-8"></div>
	<div class="col-sm-2">
		<label for="">Descuento(%)</label>
		{{$input_descuento}}
		<span class="error error_descuento_item"></span>
	</div>
	<div class="col-sm-2">
		<label for="">Total</label>
		{{$input_total}}
		<span class="error error_total_item"></span>
	</div>
</div>
</form>

<script>
	$(document).ready(function() { 
	$(".contar_item").on('change',function(){
		$.each($(".contar_item"), function(i, item) {
			if(isNaN($(item).val()) || $(item).val()==''){
				$(item).val('0');
			}
        });
        var costo_hora = parseFloat($("#costo_hora").val());
        var total_horas = parseFloat($("#total_horas").val());
        var costo_total_horas = parseFloat(costo_hora)*parseFloat(total_horas);
		var precio_item = parseFloat($("#precio_item").val());
        var cantidad_item = parseInt($("#cantidad_item").val());
        var descuento_item = parseFloat($("#descuento_item").val())/100;

        if(descuento_item>0){
        	var total = precio_item*cantidad_item;
        	var total = total-(total*descuento_item)
        }else{
        	var total = precio_item*cantidad_item;
        }
        total = total + costo_total_horas;
        var total_item = $("#total_item").val(total);

	});
	$("#idservicio").on("change",function(){
    	$("#descripcion_item").val($(this).val())
	});	
	$("#idmodelo").on("change",function(){
    	//getDescripcion(false)
    	getClaves();
	});	
	$("#clave").on("change",function(){
    	getDescripcion(true)
	});	
	function getDescripcion(mostrarMensaje=false){
		var url=site_url+"/paquetes/getDescripcionPaquete";
		var idservicio = $("#idservicio").val();
		var idmodelo = $("#idmodelo").val();
		var clave = $("#clave").val();
		//if(idservicio!='' && idmodelo !=''){
			ajaxJson(url,{"idservicio":idservicio,"idmodelo":idmodelo,"clave":clave},"POST","",function(result){
				if(result.length!=0){
					result=JSON.parse(result);
					var cadena = '';
					$.each(result,function(i,item){
						cadena = cadena+item.descripcion+',';
					});
					cadena = cadena.substring(0,cadena.length-1);
					$("#div_descripciones span").empty().append(cadena);
					$("#descripcion_productos").empty().val(cadena);

				}else{
					$("#div_descripciones span").empty();
					$("#descripcion_productos").empty();
				}
				getPrecio();
			});
		//}else{
			//if(mostrarMensaje){
				//ErrorCustom("Es necesario seleccionar todos los filtros");
			//}
			
		//}
	}	
	function getClaves(){
		var url=site_url+"/paquetes/getClaves";
		var idmodelo = $("#idmodelo").val();
		if(idmodelo !=''){
			ajaxJson(url,{"idmodelo":idmodelo},"POST","",function(result){
				if(result.length!=0){
					$("#clave").empty();
					$("#clave").removeAttr("disabled");
					result=JSON.parse(result);
					$("#clave").append("<option value=''>-- Selecciona --</option>");
					$.each(result,function(i,item){
						$("#clave").append("<option value= '"+item.clave+"'>"+item.clave+"</option>");
					});
				}else{
					$("#clave").empty();
					$("#clave").append("<option value='0'>No se encontraron datos</option>");
				}
			});
		}
	}	
	//Obtiene los servicios
	function getServicios(){
		var url=site_url+"/paquetes/getServicios";
		var idmodelo = $("#idmodelo").val();
		var clave = $("#clave").val();
		if(idmodelo !=''){
			ajaxJson(url,{"idmodelo":idmodelo,"clave":clave},"POST","",function(result){
				if(result.length!=0){
					$("#idservicio").empty();
					$("#idservicio").removeAttr("disabled");
					result=JSON.parse(result);
					$("#idservicio").append("<option value=''>-- Selecciona --</option>");
					$.each(result,function(i,item){
						$("#idservicio").append("<option value= '"+item.servicio+"'>"+item.servicio+"</option>");
					});
				}else{
					$("#idservicio").empty();
					$("#idservicio").append("<option value='0'>No se encontraron datos</option>");
				}
			});
		}
	}	
	function getPrecio(){
		var url=site_url+"/paquetes/getPreciosPorPaquete";
		var clave = $("#clave").val();
		if(clave !=''){
			ajaxJson(url,{"clave":clave},"POST","async",function(result){
				$("#precio_item").val(result);
				$("#cantidad_item").trigger('change');
			});
			getServicios();
		}else{
			
		}
	}

	
//
	// $(".busqueda").select2();
 //    $(".busqueda").select2({ width: 'resolve' }); 
});
</script>