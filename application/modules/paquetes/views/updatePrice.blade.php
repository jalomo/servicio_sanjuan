@layout('tema_luna/layout')
@section('contenido')
<h3 class="text-center">Actualizar Precios Paquetes</h3>
<div class="row">
  <div class="col-sm-6">
    <label for="">Clave</label>
    {{$drop_clave}}
    <div class="error error_clave"></div>
  </div>
  <div class="col-sm-6">
    <label for="">Precio</label>
    <input type="text" name="precio" id="precio" class="form-control">
    <div class="error error_precio"></div>
  </div>
</div>
<br>
<div class="row text-right">
  <div class="col-sm-12">
    <button id="guardar" name="guardar" class="btn btn-success">Guardar</button>
  </div>
</div>
@endsection

@section('scripts')
<script>
  var site_url = "{{site_url()}}";
  $("#clave").on('change',getPrecio);
  $("#guardar").on('click',updatePrice);
  function getPrecio(){
    var url=site_url+"/paquetes/getPreciosPorPaquete";
    var clave = $("#clave").val();
    if(clave !=''){
      ajaxJson(url,{"clave":clave},"POST","async",function(result){
        $("#precio").val(result);
      });
    }else{
      ErrorCustom('Es necesario seleccionar la clave');
    }
  }
  function updatePrice(){
     var url = site_url+'/paquetes/updPrice';
          ajaxJson(url,{"clave":$("#clave").val(),"precio":$("#precio").val()},"POST","async",function(result){
          if(isNaN(result)){
                  data = JSON.parse( result );
                  //Se recorre el json y se coloca el error en la div correspondiente
                  $.each(data, function(i, item) {
                      $(".error_"+i).empty();
                      $(".error_"+i).append(item);
                      $(".error_"+i).css("color","red");
                  });
          }else{
              if(result>0){
                ExitoCustom("Precio actualizado correctamente",function(){
                  location.reload();
                });
              }else{
                ErrorCustom('No se pudo actualizar el precio, intenta otra vez.');
              }
          }
        });
     }
</script>
@endsection