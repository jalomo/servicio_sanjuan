@layout('tema_luna/layout')
@section('contenido')
<form method="POST" id="frm">
<div class="row">
	<div class="col-sm-3">
		<label for="">Clave</label>
		{{$input_clave}}
		<span class="error error_clave"></span>
	</div>
	<div class="col-sm-3">
		<label for="">Precio mano de obra</label>
		{{$input_mo}}
		<span class="error error_mo"></span>
	</div>
	<div class="col-sm-3">
		<label for="">Precio refacciones</label>
		{{$input_ref}}
		<span class="error error_ref"></span>
	</div>
	<div class="col-sm-3">
		<label for="">Total</label>
		{{$input_total}}
		<span class="error error_total"></span>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-12">
		<label for="">Descripción</label>
		{{$input_descripcion}}
		<span class="error error_descripcion"></span>
	</div>
</div>
<br>
<div class="row text-right">
	<div class="col-sm-12">
		<button type="button" class="btn btn-success" id="guardar">Guardar</button>
	</div>
</div>
</form>
@endsection




@section('scripts')
<script>
var site_url = "{{site_url()}}";
var tabla = '';
var campo = '';
var idcampo = '';
var aPos= '';
$("#guardar").on('click',agregarRegistro);
function agregarRegistro(){

	var url =site_url+"/paquetes/agregar_precios_claves";
	  ajaxJson(url,$("#frm").serialize(),"POST","async",function(result){
	    result = JSON.parse( result );
	    if(isNaN(result)){
	      //Se recorre el json y se coloca el error en la div correspondiente
	         $.each(result, function(i, item) {
	              $(".error_"+i).empty();
	              $(".error_"+i).append(item);
	              $(".error_"+i).css("color","red");
	          });
	    }else{
	    	if(result == 0){
	    		ErrorCustom("Error al guardar, por favor intentalo otra vez.");
	    	}else if(result==-1){
	    		ErrorCustom("La clave ya fue registrada");
	    	}else{
	         	ExitoCustom("Registro agregado correctamente",function(){
	         		location.href = site_url+'/paquetes/lista_precios';
	          });
	        }
	    }
	});
}
</script>
@endsection