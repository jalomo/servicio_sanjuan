@layout('tema_luna/layout')
@section('contenido')
<h2>Lista de descripción de paquetes</h2>
<a href="{{site_url('paquetes/agregar_paquete')}}" class="btn btn-info pull-right">Agregar registro</a>
<br>
<br>
<div class="row">
	<div class="col-sm-12">
		<table id="tbl_paquetes" class="table table-hover table-striped" style="width: 100%">
			<thead>
				<tr class="tr_principal">
					<th>Clave</th>
					<th>Tipo</th>
					<th>Descripción</th>
					<th>$ Total</th>
				</tr>
			</thead>
			<tbody>
				@foreach($precios as $p => $precio)
					<tr>
						<td>{{$precio->clave}}</td>
						<td>{{$precio->tipo}}</td>
						<td>{{$precio->descripcion}}</td>
						<td>{{$precio->total}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('scripts')
<script>
	inicializar_tabla("#tbl_paquetes");
</script>
@endsection