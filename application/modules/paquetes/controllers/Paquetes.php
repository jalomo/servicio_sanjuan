<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paquetes extends MX_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_paquetes','mp');
    $this->load->model('citas/m_catalogos','mcat');
    $this->load->helper(array('general','dompdf','correo'));
    date_default_timezone_set('America/Mexico_City');
  }
  public function modal_paquetes(){
      if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
      {
        $this->output->set_status_header('409');
        exit();
      }else if($this->session->userdata('id_usuario')==''){
        redirect('login');
      }

      $idorden = $this->input->get('idorden');

      if($this->input->post()){
          //print_r($_POST);die();
          $this->form_validation->set_rules('descripcion_item', 'descripción', 'trim|required');
          $this->form_validation->set_rules('precio_item', 'precio', 'trim|numeric|required');
          $this->form_validation->set_rules('cantidad_item', 'cantidad', 'trim|numeric|required');
          $this->form_validation->set_rules('descuento_item', 'descuento', 'trim|numeric');
          $this->form_validation->set_rules('total_horas', 'total horas', 'trim|required|numeric');
          $this->form_validation->set_rules('costo_hora', 'costo hora', 'trim|numeric');

          $this->form_validation->set_rules('idservicio', 'servicio', 'trim|required');
          $this->form_validation->set_rules('idmodelo', 'idmodelo', 'trim|required');
          $this->form_validation->set_rules('clave', 'clave', 'trim|required');
          if ($this->form_validation->run()){
            $datos_preventivo = $this->mp->getManteminientoPreventivo();
            $datos_item_orden = array('descripcion' => $this->input->post('descripcion_item'),
                                      'idorden' => $this->input->post('idorden'),
                                      'clave' => $this->input->post('clave'),
                                      'precio_unitario' => $this->input->post('precio_item'),
                                      'cantidad' => $this->input->post('cantidad_item'),
                                      'descuento' => $this->input->post('descuento_item'),
                                      'total' => $this->input->post('total_item'),
                                      'id_usuario' => $this->session->userdata('id_usuario'),
                                      'created_at' => date('Y-m-d H:i:s'),
                                      'temporal' => $this->input->post('temporal_save'),
                                      'tipo'=>$this->input->post('tipo'), //particular (no es de grupos)
                                      'total_horas' => $this->input->post('total_horas'),
                                      'costo_hora' => $this->input->post('costo_hora'),
                                      'grupo'=>$this->input->post('codigo'),
                                      'descripcion_productos' => isset($_POST['descripcion_productos'])?$_POST['descripcion_productos']:''

            );
            if($this->input->post('iditem')==0){
              $exito = $this->db->insert('articulos_orden',$datos_item_orden);
              $idarticulo = $this->db->insert_id();
            }else{
              $exito = $this->db->where('id',$this->input->post('iditem'))->update('articulos_orden',$datos_item_orden);
               $idarticulo = $this->input->post('iditem');
            }
            if($exito){
              echo 1;
            }else{
              echo 0;
            }
            exit();

          }else{
            $errors = array(
              'descripcion_item' => form_error('descripcion_item'),
              'precio_item' => form_error('precio_item'),
              'cantidad_item' => form_error('cantidad_item'),
              'descuento_item' => form_error('descuento_item'),
              'costo_hora' => form_error('costo_hora'),
              'total_horas' => form_error('total_horas'),
              'idservicio' => form_error('idservicio'),
              'idmodelo' => form_error('idmodelo'),
              'clave' => form_error('clave'),

            );
            echo json_encode($errors); exit();
          }
        }

        $info=new Stdclass();
        $valor_cantidad = 1;

    //$data['drop_idservicio'] = form_dropdown('idservicio',array_combos($this->mcat->get('catalogos_ereact','servicio'),'id','servicio',TRUE),set_value('idservicio',exist_obj($info,'idservicio')),'class="form-control" id="idservicio" ');

    $data['drop_idservicio'] = form_dropdown('idservicio',array(),'','class="form-control" id="idservicio" ');

     $data['drop_idmodelo'] = form_dropdown('idmodelo',array_combos($this->mcat->get('modelos_ford','modelo'),'id','modelo',TRUE),set_value('idmodelo',exist_obj($info,'idmodelo')),'class="form-control" id="idmodelo" ');

    $data['input_descripcion'] = form_input('descripcion_item',set_value('descripcion',exist_obj($info,'descripcion')),'class="form-control" id="descripcion_item" readonly ');

    $data['input_precio'] = form_input('precio_item',set_value('precio',exist_obj($info,'precio')),'class="form-control contar_item cantidad" id="precio_item" ');

    $data['input_cantidad'] = form_input('cantidad_item',set_value('cantidad',$valor_cantidad),'class="form-control contar_item cantidad" id="cantidad_item" ');

    $data['input_descuento'] = form_input('descuento_item',set_value('descuento',exist_obj($info,'descuento')),'class="form-control contar_item cantidad" id="descuento_item" maxlength="2" ');

     $data['input_total_horas'] = form_input('total_horas',0,'class="form-control contar_item" id="total_horas" maxlength="6" ');

    $data['input_total'] = form_input('total_item',set_value('total',exist_obj($info,'total')),'class="form-control cantidad contar_item" id="total_item" readonly ');

    $data['input_costo_hora'] = form_input('costo_hora',550,'class="form-control contar_item" id="costo_hora" readonly ');

    $precios_paquetes = array();

     $data['drop_clave'] = form_dropdown('clave',array_combos(array(),'clave','clave',TRUE),'','class="form-control" id="clave" ');


   

    $data['idorden'] = $idorden;
    $data['temporal_save'] = $this->input->get('temporal');
    $data['tipo'] = 5; //preventivo
    $this->blade->render('modal_paquetes',$data);
  }

  public function getDescripcionPaquete(){
    if($this->input->post()){
      //$clave = $this->mp->getClavePaquete($this->input->post('idservicio'),$this->input->post('idmodelo'));
      $clave = $this->input->post('clave');
      if($clave!=''){
        $descripciones = $this->db->where('clave',$clave)->get('descripcion_paquetes')->result();
      }else{
        $descripciones = array();
      }
      echo json_encode($descripciones);
      exit();
    }else{
    echo 'Nada';
    }
  }

  public function getClaves(){
    //$this->db->where('idmodelo',$_POST['idmodelo']);
     $precios_paquetes = $this->db->select('distinct(clave) as clave')->get('paquetes')->result();
     echo json_encode($precios_paquetes);
  }
  public function getServicios(){

     $servicios = $this->db
                          //->where('idmodelo',$_POST['idmodelo'])
                          //->where('clave',$_POST['clave'])
                          ->join('catalogos_ereact c','p.idservicio = c.id')
                          ->select('c.servicio')
                          ->get('paquetes p')->result();
     echo json_encode($servicios);
  }


  
 
  public function getPreciosPorPaquete(){
    //$clave = $this->mp->getClavePaquete($this->input->post('idservicio'),$this->input->post('idmodelo'));
    $clave = $this->input->post('clave');
    if($clave!=''){
      echo $this->mp->getPrecioPaqueteByClave($clave);
      exit();
    }else{
      echo ''; exit();
    }
  }
  public function updPrice(){
    if($this->input->post()){
          $this->form_validation->set_rules('precio', 'precio', 'trim|numeric|required');
          $this->form_validation->set_rules('clave', 'clave', 'trim|required');
          if ($this->form_validation->run()){
            $exito = $this->db->where('clave',$this->input->post('clave'))->set('total',$this->input->post('precio'))->update('precios_paquetes');
            if($exito){
              echo 1;
            }else{
              echo 0;
            }
            exit();

          }else{
            $errors = array(
              'precio' => form_error('precio'),
              'clave' => form_error('clave')

            );
            echo json_encode($errors); exit();
          }
        }
      $precios_paquetes = $this->db->select('distinct(clave) as clave')->get('precios_paquetes')->result();
     $data['drop_clave'] = form_dropdown('clave',array_combos($precios_paquetes,'clave','clave',TRUE),'','class="form-control" id="clave" ');
    $this->blade->render('updatePrice',$data);
  }
  public function agregar_paquete(){
    if($this->input->post()){
          $this->form_validation->set_rules('idservicio', 'servicio', 'trim|required');
          $this->form_validation->set_rules('idmodelo', 'modelo', 'trim|required');
          $this->form_validation->set_rules('clave', 'clave', 'trim|required');
          $this->form_validation->set_rules('tipo', 'tipo', 'trim|required');
          $this->form_validation->set_rules('total', ' ', 'trim|numeric|required');
          $this->form_validation->set_rules('descripcion', 'descripción', 'trim|required');
          if ($this->form_validation->run()){
            //Validar que no exista la clave
            $datos_descripcion = array(
                           'clave' => $this->input->post('clave'),
                           'tipo' => $this->input->post('tipo'), 
                           'descripcion' => $this->input->post('descripcion'), 
                           'total' => $this->input->post('total'),
                           'id_usuario'=>$this->session->userdata('id_usuario'),
                           'manual'=>1,
            );
            $this->db->insert('descripcion_paquetes',$datos_descripcion);
            $datos_paquetes = array('idservicio' => $this->input->post('idservicio'), 
                           'idmodelo' => $this->input->post('idmodelo'), 
                           'clave' => $this->input->post('clave'),
                           'id_usuario'=>$this->session->userdata('id_usuario'),
                           'manual'=>1,
            );
            $exito = $this->db->insert('paquetes',$datos_paquetes);
            if($exito){
              echo 1;
            }else{
              echo 0;
            }
            exit();

          }else{
            $errors = array(
              'idservicio' => form_error('idservicio'),
              'idmodelo' => form_error('idmodelo'),
              'clave' => form_error('clave'),
              'tipo' => form_error('tipo'),
              'total' => form_error('total'),
              'descripcion' => form_error('descripcion'),

            );
            echo json_encode($errors); exit();
          }
        }
    $data['drop_idservicio'] = form_dropdown('idservicio',array_combos($this->mcat->get('catalogos_ereact','servicio'),'id','servicio',TRUE),'','class="form-control" id="idservicio" ');
    $data['drop_idmodelo'] = form_dropdown('idmodelo',array_combos($this->mcat->get('modelos_ford','modelo'),'id','modelo',TRUE),'','class="form-control" id="idmodelo" ');
    $precios_paquetes = $this->db->select('distinct(clave) as clave')->get('precios_paquetes')->result();
    $data['drop_clave'] = form_dropdown('clave',array_combos($precios_paquetes,'clave','clave',TRUE),'','class="form-control busqueda" id="clave" ');
    $opciones_tipo = array('' =>'-- Selecciona --','C'=>'C','R'=>'R');
    $data['drop_tipo'] = form_dropdown('tipo',$opciones_tipo,'','class="form-control" id="tipo" ');
    $data['input_total'] = form_input('total','','class="form-control" id="total" ');
    $data['input_descripcion'] = form_input('descripcion','','class="form-control" id="descripcion" ');

    $this->blade->render('agregar_paquetes',$data);
  }
  function agregar_registro_catalogo($id=0)
  {
      if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
      {
        $this->output->set_status_header('409');
        exit();
      }else if($this->session->userdata('id_usuario')==''){
      redirect('login');
      }
      //print_r($_POST);die();
      if($this->input->post()){
        $this->form_validation->set_rules('valor', ' ', 'trim|required');
       if ($this->form_validation->run()){
          //VALIDAR QUE NO EXISTA EL REGISTRO

        $q= $this->db->where($this->input->post('campo'),$this->input->post('valor'))->get($this->input->post('tabla'));
        if($q->num_rows()==1){
          echo json_encode(array('exito'=>true,'mensaje'=>'Ya se encuentra el registro')); exit();
        }

        $this->db->set($this->input->post('campo'),$this->input->post('valor'))->set('id_usuario',$this->session->userdata('id_usuario'))->insert($this->input->post('tabla'));
        echo json_encode(array('exito'=>true,'id'=>$this->db->insert_id(),'valor'=>$this->input->post('valor'),'mensaje'=>'')); exit();
        }else{
          $errors = array(
                'valor' => form_error('valor'),
          );
          echo json_encode(array('exito'=>false,'errors'=>$errors)); exit();
        }
      }

      $info=new Stdclass();

      $data['input_valor'] = form_input('valor',set_value('valor',exist_obj($info,'valor')),'class="form-control" id="valor" ');
      $this->blade->render('agregar_registro',$data);
  }
  public function agregar_precios_claves(){
    if($this->input->post()){
          $this->form_validation->set_rules('mo', ' ', 'trim|numeric|required');
          $this->form_validation->set_rules('ref', ' ', 'trim|numeric|required');
          $this->form_validation->set_rules('total', ' ', 'trim|numeric|required');
          $this->form_validation->set_rules('clave', 'clave', 'trim|required');
          $this->form_validation->set_rules('descripcion', 'descripción', 'trim|required');
          if ($this->form_validation->run()){
            //Validar que no exista la clave
            $q = $this->db->where('clave',$this->input->post('clave'))->get('precios_paquetes')->result();
            if(count($q)>0){
              echo -1; exit();
            }
            $datos = array('mo' => $this->input->post('mo'), 
                           'ref' => $this->input->post('ref'), 
                           'total' => $this->input->post('total'),
                           'clave' => $this->input->post('clave'), 
                           'descripcion' => $this->input->post('descripcion'), 
                           'id_usuario'=>$this->session->userdata('id_usuario'),
                           'manual'=>1,
            );
            $exito = $this->db->insert('precios_paquetes',$datos);
            if($exito){
              echo 1;
            }else{
              echo 0;
            }
            exit();

          }else{
            $errors = array(
              'total' => form_error('total'),
              'clave' => form_error('clave'),
              'mo' => form_error('mo'),
              'ref' => form_error('ref'),
              'descripcion' => form_error('descripcion'),

            );
            echo json_encode($errors); exit();
          }
        }
      
    $data['input_clave'] = form_input('clave','','class="form-control" id="clave" maxlength="3" ');
    $data['input_mo'] = form_input('mo','','class="form-control" id="mo" ');
    $data['input_ref'] = form_input('ref','','class="form-control" id="ref" ');
    $data['input_total'] = form_input('total','','class="form-control" id="total" ');
    $data['input_descripcion'] = form_input('descripcion','','class="form-control" id="descripcion" ');

    $this->blade->render('agregar_precios_paquetes',$data);
  }
  public function lista_precios(){
    $data['precios'] = $this->db->where('manual',1)->get('precios_paquetes')->result();
    $this->blade->render('lista_precios',$data);
  }
  public function lista_paquetes(){
    $data['precios'] = $this->db->where('manual',1)->get('descripcion_paquetes')->result();
    $this->blade->render('lista_paquetes',$data);
  }

  
}
