@layout('tema_luna/layout')
@section('contenido')
<h2>Agregar paquetes preventivos</h2>
<form id="frm-preventivo">
	<input type="hidden" id="id" name="id" value="{{$id}}">
	<div class="row">
		<div class="col-sm-4">
			<label>Código</label>
			{{$input_codigo}}
			<div class="error error_codigo"></div>
		</div>
		<div class="col-sm-4">
			<label>Modelo</label>
			{{$drop_modelo}}
			<div class="error error_idmodelo"></div>
		</div>
		<div class="col-sm-4">
			<label>Paquete</label>
			{{$drop_paquete}}
			<div class="error error_id_paquete"></div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-4">
			<label>Tipo</label>
			{{$drop_tipo}}
			<div class="error error_id_tipo"></div>
		</div>
		<div class="col-sm-4">
			<label>Año</label>
			{{$drop_anio}}
			<div class="error error_anio"></div>
		</div>
		<div class="col-sm-4">
			<label>Motor</label>
			{{$drop_motor}}
			<div class="error error_id_motor"></div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-4">
			<label>Descripción</label>
			{{$drop_descripcion}}
			<div class="error error_id_descripcion"></div>
		</div>
		<div class="col-sm-4">
			<label># Parte</label>
			{{$input_parte}}
			<div class="error error_parte"></div>
		</div>
		<div class="col-sm-4">
			<label># Parte sin espacios</label>
			{{$input_parte_sin_espacios}}
			<div class="error error_parte_sin_espacios"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4">
			<label>Cantidad</label>
			{{$input_cantidad}}
			<div class="error error_cantidad"></div>
		</div>
		<div class="col-sm-4">
			<label>Código ereact</label>
			{{$input_codigo_ereact}}
			<div class="error error_codigo_ereact"></div>
		</div>
		<div class="col-sm-4">
			<label>Código operación</label>
			{{$input_codigo_operacion}}
			<div class="error error_codigo_operacion"></div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-4">
			<label>Descripción</label>
			{{$input_descripcion}}
			<div class="error error_descripcion"></div>
		</div>
		<div class="col-sm-4">
			<label>Tiempo</label>
			{{$input_tiempo}}
			<div class="error error_tiempo"></div>
		</div>
		<div class="col-sm-4">
			<label>Precio</label>
			{{$input_precio}}
			<div class="error error_precio"></div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-4">
			<label>Precio distribuidor</label>
			{{$input_precio_distribuidor}}
			<div class="error error_distribuidor"></div>
		</div>
		<div class="col-sm-4">
			<label>Costo</label>
			{{$input_costo}}
			<div class="error error_costo"></div>
		</div>
	</div>

</form>
<br>
<div class="row pull-right">
	<div class="col-sm-12 ">
    	{{$btn_guardar}}
	</div>
</div>
<br><br>
@endsection

@section('scripts')
<script>
	$(".busqueda").select2();
	var site_url = "{{site_url()}}";
	$("#guardar").on('click',function(){
		var url = site_url+'/preventivo/agregar_preventivo/0';
		ajaxJson(url,$("#frm-preventivo").serialize(),"POST","",function(result){
			if(isNaN(result)){
				data = JSON.parse( result );
				$.each(data, function(i, item) {
					$(".error_"+i).empty();
					$(".error_"+i).append(item);
					$(".error_"+i).css("color","red");
				});
			}else{
				if(result==1){
					ExitoCustom("Registro agregado correctamente",function(){
						window.location.href = site_url+'/preventivo';
					});
				}else{
					ErrorCustom("Error al guardar el registro, por favor intenta otra vez");
				}
			}
		});
	});
</script>
@endsection