<style type="text/css">
    .mostrar {
        visibility: visible;
    }

    .ocultar {
        display: none;
    }

</style>
<form action="" id="frm-preventivo">
    <input type="hidden" id="idorden" name="idorden" value="{{ $idorden }}">
    <input type="hidden" id="id_cita_preventivo" name="id_cita_preventivo" value="{{ $id_cita }}"> <!-- NV-->
    <input type="hidden" id="tipo" name="tipo" value="{{ $tipo }}">
    <input type="hidden" id="temporal_save" name="temporal_save" value="{{ $temporal_save }}">
    <input type="hidden" id="idoperacion_save" name="idoperacion_save" value="{{ $idoperacion_save }}">
    <!-- Paquetes -->
    <input type="hidden" id="pkt_cilindros" name="pkt_cilindros" value="{{ $cilindro_text }}">
    <input type="hidden" id="pkt_transmision_id" name="pkt_transmision_id" value="{{ $pkt_transmision_id }}">
    <input type="hidden" id="pkt_combustible_id" name="pkt_combustible_id" value="{{ $pkt_combustible_id }}">
    <input type="hidden" id="pkt_motor_id" name="pkt_motor_id" value="{{ $pkt_motor_id }}">
    <input type="hidden" id="pkt_submodelo_id" name="pkt_submodelo_id" value="{{ $pkt_submodelo_id }}">
    <input type="hidden" id="pkt_anio" name="pkt_anio" value="{{ $vehiculo_anio_text }}">
    <input type="hidden" id="pkt_vehiculo_id" name="pkt_vehiculo_id" value="{{ $vehiculo_id }}">
    <input type="hidden" id="codigo" name="codigo" value="{{$codigo}}">


    <div id="div-alert" class="alert alert-warning ocultar" role="alert">
        No existe un paquete asociado
    </div>
    <div class="row">
        <div class="col-sm-3">
            <label for="">Código MO</label>
            {{ $drop_codigo_mo }}
            <span class="error error_codigo_mo"></span>
        </div>
        <div class="col-sm-3">
            <label for="">Año</label>
            <input class="form-control" type="text" value="{{ $vehiculo_anio_text }}" readonly>
        </div>
        <div class="col-sm-3">
            <label for="">Artículo</label>
            <input class="form-control" type="text" value="{{ $vehiculo_modelo_text }}" readonly>
        </div>
        <div class="col-sm-3">
            <label for="">Subartículo</label>
            <input class="form-control" type="text" value="{{ $submodelo_text }}" readonly>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <label for="">Motor</label>
            <input class="form-control" type="text" value="{{ $motor_text }}" readonly>
        </div>
        <div class="col-sm-3">
            <label for="">Cilindros</label>
            <input class="form-control" type="text" value="{{ $cilindro_text }}" readonly>
        </div>
        <div class="col-sm-3">
            <label for="">Transmisión</label>
            <input class="form-control" type="text" value="{{ $transmision_text }}" readonly>
        </div>
        <div class="col-sm-3">
            <label for="">Combustible</label>
            <input class="form-control" type="text" value="{{ $combustible_text }}" readonly>
        </div>
    </div>
    <br>
    <div id="div_descripciones">
        <span></span>
    </div>
    <div id="div_descripciones_totales">
        <span></span>
    </div>
    <span id="noregistros" style="color: red;text-align: right;"></span>
    <hr>
    <br>
    <div class="row">
        <div class="col-sm-2">
            <label for="">Cantidad</label>
            {{ $input_cantidad }}
            <span class="error error_cantidad_item"></span>
        </div>
        <div class="col-sm-4">
            <label for="">Descripción</label>
            {{ $input_descripcion }}
            <span class="error error_descripcion_item"></span>
        </div>
        <div class="col-sm-2">
            <label for="">Precio</label>
            {{ $input_precio }}
            <span class="error error_precio_item"></span>
        </div>
        <div class="col-sm-2">
            <label for="">Total horas</label>
            {{ $input_total_horas }}
            <span class="error error_total_horas"></span>
        </div>
        <div class="col-sm-2">
            <label for="">Costo hora</label>
            {{ $input_costo_hora }}
            <span class="error error_costo_hora"></span>
        </div>
    </div>
    <div class="row ">
        <div class="col-sm-6"></div>
        <div class="col-sm-2">
            <label for="">IVA</label>
            <input type="text" class="form-control" id="iva" name="iva" readonly>
            <span class="error error_descuento_item"></span>
        </div>
        <div class="col-sm-2">
            <label for="">Descuento(%)</label>
            {{ $input_descuento }}
            <span class="error error_descuento_item"></span>
        </div>
        <div class="col-sm-2">
            <label for="">Total</label>
            {{ $input_total }}
            <span class="error error_total_item"></span>
        </div>
    </div>
</form>

<script>
    $(document).ready(function() {
        let idoperacion_save = "{{$idoperacion_save}}";
        let clave_id = "{{$clave_id}}";
        $(".contar_item").on('change', function() {
            var onChange = $(this).prop('name');
            $.each($(".contar_item"), function(i, item) {
                if (isNaN($(item).val()) || $(item).val() == '') {
                    if ($(item).prop('name') == 'total_horas') {
                        $(item).val('0');
                    } else {
                        $(item).val('0');
                    }
                }
            });
            var total_horas = parseFloat($("#total_horas").val());
            var precio_item = parseFloat($("#precio_item").val());
            if (onChange == 'precio_item') {
                if (precio_item > 0) {
                    $("#total_horas").val(0)
                }
            }
            if (onChange == 'total_horas') {
                if (total_horas > 0) {
                    $("#precio_item").val(0)
                }
            }
            var costo_hora = parseFloat($("#costo_hora").val());
            var total_horas = parseFloat($("#total_horas").val());
            var costo_total_horas = parseFloat(costo_hora) * parseFloat(total_horas);
            var precio_item = parseFloat($("#precio_item").val());
            var cantidad_item = parseInt($("#cantidad_item").val());
            var descuento_item = parseFloat($("#descuento_item").val()) / 100;

            if (descuento_item > 0) {
                var total = precio_item * cantidad_item;
                var total = total - (total * descuento_item)
            } else {
                var total = precio_item * cantidad_item;
            }
            total = total + costo_total_horas;
            var total_item = $("#total_item").val(total.toFixed(2));

        });
        const getDescripcionPreventivo = () => {
            var url = site_url + "/preventivo/getDescripcionPreventivo";
            let codigo_mo = $("#codigo_mo").val();
            if (!codigo_mo) {
                $("#codigo").val('');
                return;
            }
            ajaxJson(url, {
                codigo_mo
            }, "POST", "", function(result) {
                result = JSON.parse(result);
                if (result.length != 0) {
                    $("#codigo").val(result.clave);
                    $("#descripcion_item").val(result.codigo);
                    $("#div_descripciones span").empty().append(result.descripcion);
                    //Obtener información del paquete
                    getDataPaquete();
                } else {
                    $("#codigo").val('');
                    $("#div_descripciones span").empty();
                }
            });
        }
        const getDataPaquete = () => {
            
            var url = site_url + "/dmspaquetes/getPaqueteByData";
            let data = {
                clave_id: $("#codigo_mo").val(),
                modelo_id: $("#pkt_vehiculo_id").val(),
                submodelo_id: $("#submodelo_id").val(),
                anio: $("#pkt_anio").val(),
                cilindros: $("#pkt_cilindros").val(),
                transmision: $("#pkt_transmision_id").val(),
                motor: $("#pkt_motor_id").val(),
                combustible: $("#pkt_combustible_id").val(),
            }
            ajaxJson(url, data, "POST", "", function(result) {
                result = JSON.parse(result);
                if (!result.paquete_id) {
                    $("#precio_item").val('');
                    $("#total_horas").val('');
                    $("#total_item").val('');
                    $("#paquete_id").val('');
                    $("#div-alert").addClass('ocultar');
                    $("#div-alert").removeClass('ocultar');

                } else {
                    $("#iva").val(result.data_paquete.IVA);
                    $("#precio_item").val(result.data_paquete.precio);
                    $("#total_horas").val(result.data_paquete.tiempo_tabulado);
                    $("#total_item").val(result.data_paquete.precio_iva);
                    $("#paquete_id").val(result.data_paquete.id);
                    $("#div-alert").removeClass('ocultar');
                    $("#div-alert").addClass('ocultar');
                }
            });
        }


        $("#codigo_mo").on('change', getDescripcionPreventivo);
        
        if(clave_id != ''){
            getDescripcionPreventivo();
        }
        
        // $(".busqueda").select2();
        //    $(".busqueda").select2({ width: 'resolve' }); 
    });
</script>
