<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Preventivo extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_preventivo', 'mp');
    $this->load->model('citas/m_catalogos', 'mcat');
    $this->load->model('principal');
    $this->load->helper(array('general', 'dompdf', 'correo'));
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  public function modal_preventivo()
  {

    if ($this->input->is_ajax_request() && $this->session->userdata('id_usuario') == '') {
      $this->output->set_status_header('409');
      exit();
    } else if ($this->session->userdata('id_usuario') == '') {
      redirect('login');
    }
    $idorden = $this->input->get('idorden');

    if ($this->input->post()) {
      //NUEVA VERSIÓN
      $this->form_validation->set_rules('codigo_mo', 'código mo', 'trim|required');
      $this->form_validation->set_rules('codigo', 'código', 'trim|required');
      $this->form_validation->set_rules('descripcion_item', 'descripción', 'trim|required');
      $this->form_validation->set_rules('precio_item', 'precio', 'trim|numeric|required');
      $this->form_validation->set_rules('cantidad_item', 'cantidad', 'trim|numeric|required');
      $this->form_validation->set_rules('descuento_item', 'descuento', 'trim|numeric');
      $this->form_validation->set_rules('total_horas', 'total horas', 'trim|numeric');
      $this->form_validation->set_rules('costo_hora', 'costo hora', 'trim|numeric');


      if ($this->form_validation->run()) {
        $datos_item_orden = array(
          'descripcion' => $this->input->post('descripcion_item'),
          'idorden' => $this->input->post('idorden'),
          'precio_unitario' => $this->input->post('precio_item'),
          'cantidad' => $this->input->post('cantidad_item'),
          'descuento' => $this->input->post('descuento_item'),
          'total' => $this->input->post('total_item'),
          'id_usuario' => $this->session->userdata('id_usuario'),
          'created_at' => date('Y-m-d H:i:s'),
          'temporal' => $this->input->post('temporal_save'),
          'tipo' => $this->input->post('tipo'), //particular (no es de grupos)
          'total_horas' => $this->input->post('total_horas'),
          'costo_hora' => $this->input->post('costo_hora'),
          'grupo' => $this->input->post('codigo'),

        );
        $datos_detalle_paquete = [
          'clave_id' => $_POST['codigo_mo'],
          'modelo_id' => $_POST['pkt_vehiculo_id'],
          'anio' => $_POST['pkt_anio'],
          'cilindro' => $_POST['pkt_cilindros'],
          'transmision' => $_POST['pkt_transmision_id'],
          'motor' => $_POST['pkt_motor_id'],
          'combustible' => $_POST['pkt_combustible_id'],
          'submodelo_id' => $_POST['pkt_submodelo_id'],
          'temporal' => $_POST['temporal_save'],
          'idorden' => $_POST['idorden'],
          'idoperacion' => $_POST['idoperacion_save'],
          'precio_item' => $_POST['precio_item'],
          'costo_hora' => $_POST['costo_hora'],
          'iva' => $_POST['iva'],
          'descuento_item' => $_POST['descuento_item'],
          'total_item' => $_POST['total_item'],
          'id_usuario' => $this->session->userdata('id_usuario')
        ];
        
        if ($this->input->post('idoperacion_save') == 0) {

          $datos_detalle_paquete['created_at'] = date('Y-m-d H:i:s');
          $exito = $this->db->insert('articulos_orden', $datos_item_orden);
          $id_operacion = $this->db->insert_id();
          //Revisar que no se haya insertado con el temporal
          
          $datos_paquete = $this->principal->getGeneric('temporal',$_POST['temporal_save'],'pkt_detalle_paquete_operacion');
          if($datos_paquete){
            $this->db->where('temporal',$_POST['temporal_save'])->update('pkt_detalle_paquete_operacion',$datos_detalle_paquete);
          }else{
            $this->db->insert('pkt_detalle_paquete_operacion',$datos_detalle_paquete);
          }
          $this->db->where('temporal',$_POST['temporal_save'])->set('idoperacion',$id_operacion)->update('pkt_detalle_paquete_operacion');
        } else {
          $datos_detalle_paquete['updated_at'] = date('Y-m-d H:i:s');
          $datos_item_orden['id_usuario_edito'] = $this->session->userdata('id_usuario');
          $exito = $this->db->where('id', $this->input->post('idoperacion_save'))->update('articulos_orden', $datos_item_orden);
          $idarticulo = $this->input->post('idoperacion_save');
          $this->db->where('idoperacion',$_POST['idoperacion_save'])->update('pkt_detalle_paquete_operacion',$datos_detalle_paquete);
        }

        //   if($this->input->post('idoperacion_save')==0){
        //     $array_preventivo = array(
        //                               'idarticulo' =>$idarticulo,
        //                               'codigo_mo' => $_POST['codigo_mo'],
        //                               'anio' => $_POST['anio'],
        //                               'modelo' => $_POST['modelo'],
        //                               'motor' => $_POST['motor_preventivo'],
        //                               'codigo' => $_POST['codigo'],
        //                               'id_tipo' => isset($_POST['tipo_preventivo'])?$_POST['tipo_preventivo']:null,
        //                               'created_at' => date('Y-m-d H:i:s'),
        //                               'tipo_aceite' => isset($_POST['tipo_aceite'])?$_POST['tipo_aceite']:null,
        //      );
        //     $this->db->insert('preventivo_detalle',$array_preventivo);
        //  }

        if ($exito) {
          echo 1;
        } else {
          echo 0;
        }
        exit();
      } else {
        $errors = array(
          'descripcion_item' => form_error('descripcion_item'),
          'precio_item' => form_error('precio_item'),
          'cantidad_item' => form_error('cantidad_item'),
          'descuento_item' => form_error('descuento_item'),
          'costo_hora' => form_error('costo_hora'),
          'codigo_mo' => form_error('codigo_mo'),
          'codigo' => form_error('codigo'),
          'tipo_preventivo' => form_error('tipo_preventivo'),
          'tipo_aceite' => form_error('tipo_aceite'),

        );
        echo json_encode($errors);
        exit();
      }
    }


    $id = $_GET['idoperacion'];
    if ($id == 0) {
      $info = new Stdclass();
      $valor_cantidad = 1;
      $clave_id = $_GET['id_servicio'];
      $codigo = '';
    } else {
      $info = $this->mp->getOperacionId($id);
      $valor_cantidad = $info->cantidad;
      $clave_id = $this->principal->getGeneric('codigo', $info->descripcion, 'pkt_claves', 'id');
      $codigo = $this->principal->getGeneric('codigo', $info->descripcion, 'pkt_claves', 'clave');
    }
    //codigo mo
    $codigo_mo = $this->db->select('id,codigo')->get('pkt_claves')->result();

    $data['drop_codigo_mo'] = form_dropdown('codigo_mo', array_combos($codigo_mo, 'id', 'codigo', TRUE), $clave_id, 'class="form-control busqueda" id="codigo_mo" ');

    //Costos
    $data['input_descripcion'] = form_input('descripcion_item', set_value('descripcion', exist_obj($info, 'descripcion')), 'class="form-control" id="descripcion_item" readonly ');

    $data['input_precio'] = form_input('precio_item', set_value('precio', exist_obj($info, 'precio_unitario')), 'class="form-control contar_item cantidad" id="precio_item" readonly');

    $data['input_cantidad'] = form_input('cantidad_item', set_value('cantidad', $valor_cantidad), 'class="form-control contar_item cantidad" id="cantidad_item" readonly ');

    $data['input_descuento'] = form_input('descuento_item', set_value('descuento', exist_obj($info, 'descuento')), 'class="form-control contar_item cantidad" id="descuento_item" maxlength="2" ');

    $data['input_total_horas'] = form_input('total_horas', set_value('total_horas', exist_obj($info, 'total_horas')), 'class="form-control contar_item" id="total_horas" maxlength="6" readonly ');

    $data['input_total'] = form_input('total_item', set_value('total', exist_obj($info, 'total')), 'class="form-control cantidad contar_item" id="total_item" readonly ');

    $data['input_costo_hora'] = form_input('costo_hora', CONST_COSTO_MANO_OBRA, 'class="form-control contar_item" id="costo_hora" readonly ');

    $data['idorden'] = $idorden;
    $data['temporal_save'] = $this->input->get('temporal');
    $data['id_cita'] = $this->input->get('id_cita'); //NV

    $data['tipo'] = 5; //preventivo
    $data['idoperacion_save'] = $_GET['idoperacion'];

    //Datos de la orden

    $data['cilindro_text'] = $_GET['cilindro_text'];
    $data['pkt_cilindro_id'] = $_GET['cilindro_id'];

    $data['transmision_text'] = $_GET['transmision_text'];
    $data['pkt_transmision_id'] = $_GET['transmision_id'];

    $data['combustible_text'] = $_GET['combustible_text'];
    $data['pkt_combustible_id'] = $_GET['combustible_id'];

    $data['motor_text'] = $_GET['motor_text'];
    $data['pkt_motor_id'] = $_GET['motor_id'];

    $data['vehiculo_modelo_text'] = $_GET['vehiculo_modelo'];
    $data['vehiculo_id'] = $this->mp->getVehiculoNombre($_GET['vehiculo_modelo']);

    $data['vehiculo_anio_text'] = $_GET['vehiculo_anio'];

    $data['submodelo_text'] = $_GET['submodelo_text'];
    $data['pkt_submodelo_id'] = $_GET['submodelo_id'];
    
    
    $data['clave_id'] = $clave_id;
    $data['codigo'] = $codigo;


    $this->blade->render('modal_preventivo', $data);
  }
  public function getDescripcionPreventivo()
  {
    if ($this->input->post()) {
      $descripciones = $this->db->where('id', $this->input->post('codigo_mo'))->get('pkt_claves')->row();
      echo json_encode($descripciones);
    } else {
      echo 'Nada';
    }
  }
  public function getModeloByNombre()
  {
    $q = $this->db->where('modelo', $_POST['modelo'])->get('cat_modelo');
    if ($q->num_rows() == 1) {
      echo json_encode(array('exito' => true, 'data' => $q->row()));
    } else {
      echo json_encode(array('exito' => false));
    }
  }
}
