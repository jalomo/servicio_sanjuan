<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_preventivo extends CI_Model{
  public function __construct(){
    parent::__construct();
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  public function getManteminientoPreventivo(){
    $this->db->where('idmodelo',$this->input->post('modelo'));
    $this->db->where('anio',$this->input->post('anio'));
    $this->db->where('id_tipo',$this->input->post('tipo_preventivo'));

    $this->db->where('codigo_ereact',$this->input->post('codigo_mo'));
    $this->db->where('id_motor',$this->input->post('motor_preventivo'));

    $this->db->where('codigo',$this->input->post('codigo'));
    $this->db->where('id_descripcion',$this->input->post('descripcion'));
    $this->db->where('id_catalogo',$this->input->post('catalogo_preventivo'));
    return $this->db->get('v_mantenimiento_preventivo')->row();
  }
  //obtiene los registros de preventivo que se dieron de alta manualmente
  public function getDataPreventivo(){
    return $this->db->where('id_usuario !=',null)->get('v_mantenimiento_preventivo')->result();
  }
  //obtener el operador en base a su id
  public function getPreventivoId($id=0){
    $q = $this->db->where('id',$id)->where('excel_ford',0)->get('preventivo');
    //Validar solo de los registros que agregan 
    if($q->num_rows()==1){
      return $q->row();
    }else{
      return false;
    }
  }
  //obtener el operador en base a su id
  public function getOperacionId($id=0){
    $q = $this->db->where('id',$id)->get('articulos_orden');
    if($q->num_rows()==1){
      return $q->row();
    }else{
      return false;
    }
  }
  //guarda los operadores
  public function guardarPreventivo(){
     $id= $this->input->post('id');
     $datos = array('codigo' => $this->input->post('codigo'),
                    'excel_ford'=>0,
                    'id_usuario'=> $this->session->userdata('id_usuario'),
                    'idmodelo' => $this->input->post('idmodelo'),
                    'id_tipo' => $this->input->post('id_tipo'),
                    'id_paquete' => $this->input->post('id_paquete'),
                    'anio' => $this->input->post('anio'),
                    'id_motor' => $this->input->post('id_motor'),
                    'id_descripcion' => $this->input->post('id_descripcion'),
                    'parte' => $this->input->post('parte'),
                    'parte_sin_espacios' => $this->input->post('parte_sin_espacios'),
                    'cantidad' => $this->input->post('cantidad'),
                    'codigo_ereact' => $this->input->post('codigo_ereact'),
                    'codigo_operacion' => $this->input->post('codigo_operacion'),
                    'descripcion' => $this->input->post('descripcion'),
                    'tiempo' => $this->input->post('tiempo'),
                    'precio' => $this->input->post('precio'),
                    'precio_distribuidor' => $this->input->post('precio_distribuidor'),
                    'costo' => $this->input->post('costo'),
      );
      if($id==0){
          $exito = $this->db->insert('preventivo',$datos);
      }else{
          $exito = $this->db->where('id',$id)->update('preventivo',$datos);
      }
        if($exito){
          echo 1;die();
        } else{
          echo 0; die();
        }
  }
  //Obtener el vehículo por nombre
  public function getVehiculoNombre($vehiculo=''){
      $q = $this->db->where('modelo',$vehiculo)->limit(1)->get('cat_modelo');
      if($q->num_rows()==1){
        return $q->row()->id;
      }else{
        return '';
      }
  }
  
}
