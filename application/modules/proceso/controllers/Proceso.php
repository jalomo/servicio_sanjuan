<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Proceso extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('citas/m_catalogos', 'mcat');
    $this->load->model('citas/m_citas', 'mc');
    $this->load->model('m_proceso', 'mp');
    $this->load->model('principal');
    $this->load->helper(array('general', 'notificaciones'));
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  public function prueba()
  {
    echo encrypt(43650);
    //MTM4ODI5OTY3NTk1
  }
  public function orden($id = '', $showDetails = 0)
  {
    //$showDetails si es 1 abre el detalle del servicio, si es 2 abre el detalle del asesor en el acordion
    $id = decrypt($id);
    $data_orden = $this->principal->getGeneric('id_cita', $id, 'citas');
    if (!$data_orden) {
      $this->v_404();
      return;
    }
    $data['sinmenu'] = true;
    $data['showDetails'] = $showDetails;
    $data['id_cita'] = $id;
    $data['cita'] = $this->mp->getAllData($id)[0];
    $data['asesor'] = $this->mp->asesor_by_nombre($data['cita']->asesor);
    $data['operaciones'] = $this->mp->getOperaciones($id);
    $data['hora_inicio_trabajo'] = $this->mc->getFechaTransicion('trabajando', $id);
    $data['drop_operaciones'] = form_dropdown('id_operacion', array_combos($data['operaciones'], 'id', 'descripcion', TRUE), '', 'class="form-control" id="id_operacion" ');
    $data['tecnicos_asignados'] = $this->mp->getTecnicosAsignados($id);
    $data['presupuesto'] = $this->mp->dataPresupuesto($id);
    $data['estatus_presupuesto'] = $this->mp->status_presupuesto($id);
    $data['estatus_refacciones_ventanilla'] = $this->mp->status_refacciones_ventanilla($id);
    $data['voz_cliente'] = $this->mp->audio_evi($id);
    $data['evidencia'] = $this->mp->getEvidencia($id);
    $data['evidencia_multipunto'] = $this->mp->evidencia_multipunto($id);
    $data['notificaciones'] = $this->mp->getNotificaciones($id);
    $data['firmo_asesor'] = $this->mc->firmoAsesor($id);
    $data['existe_carta_renuncia'] = $this->mp->existe_carta_renuncia($id);
    $data['existe_formato_voz_cliente'] = $this->mp->existe_formato_voz_cliente($id);
    $data['cat_estatus_refacciones_ventanilla'] = [
      '' => '',
      '0' => 'SIN SOLICITAR',
      '1' => 'SOLICITADAS',
      '2' => 'RECIBIDAS',
      '3' => 'ENTREGADAS'
    ];


    $data['cat_estatus_presupuesto'] = [
      '' => '',
      'Si' => 'CONFIRMADA',
      'Val' => 'DETALLADA',
      'No' => 'RECHAZADA',
    ];
    //obtengo el id del asesor
    $this->blade->render('proceso', $data);
  }
  public function v_404()
  {
    $data['sinmenu'] = true;
    $this->blade->render('v_404', $data);
  }
  public function getLineaTiempoOperacion()
  {
    $motivos_detencion = [
      'R' => 'Falta de Refacciones',
      'A' => 'Autorización del Cliente',
      'G' => 'Detenido por Gerencia',
      'S' => 'Subcontratados',
      'C' => 'Control Calidad',
      'O' => 'Otro',
    ];

    $data['motivos'] = $motivos_detencion;
    $data['linea_tiempo'] = $this->mp->lineaTiempoOperacion($_POST['id_cita'], $_POST['id_operacion']);
    $data['cita_previa'] = $this->principal->getGeneric('id_cita',$_POST['id_cita'],'citas','cita_previa');
    $this->blade->render('v_operaciones', $data);
  }
  public function mis_notificaciones()
  {
    $data['notificaciones'] = $this->mp->getNotificaciones($_POST['id_cita']);
    $data['id_cita_not'] = $_POST['id_cita'];
    $this->blade->render('v_notificaciones_user', $data);
  }
  public function buscar_grafica_linea_tiempo()
  {
    $data['linea_tiempo'] = $this->mp->lineaTiempoOperacion($_POST['id_cita'], $_POST['id_operacion']);
    $data['cita_previa'] = $this->principal->getGeneric('id_cita',$_POST['id_cita'],'citas','cita_previa');
    $this->blade->render('grafica_linea_tiempo', $data);
  }
  public function reproducir_voz_cliente()
  {
    $data['url_voz'] = $_POST['url_voz'];
    $this->blade->render('v_voz_cliente', $data);
  }
  public function reproducir_evidencia()
  {
    $data['url_evidencia'] = $_POST['url_evidencia'];
    $data['tipo'] = $_POST['tipo'];
    $this->blade->render('v_reproducir_evidencia', $data);
  }
  public function configuraciones()
  {
    $data['notificaciones'] = $this->mp->getConfiguraciones($_POST['id_cita']);
    $data['existe'] = (count($data['notificaciones']) > 0) ? true : false;
    $data['id_cita_config'] = $_POST['id_cita'];
    $data['correo'] = $this->principal->getGeneric('id_cita',$_POST['id_cita'],'citas','datos_email');
    $data['telefono'] = $this->principal->getGeneric('id_cita',$_POST['id_cita'],'ordenservicio','telefono_movil');
    $data['telefono2'] = $this->principal->getGeneric('id_cita',$_POST['id_cita'],'citas','datos_telefono');
    $this->blade->render('v_config_user', $data);
  }
  public function guardarConf()
  {

    $this->form_validation->set_rules('telefono', 'teléfono', 'trim|required|numeric|exact_length[10]');
    $this->form_validation->set_rules('correo', 'correo', 'trim|valid_email|required');

    if ($this->form_validation->run()) {
      $data = [
        'id_cita' => $_POST['id_cita_config'],
        'citas' => isset($_POST['citas']) ? 1 : 0,
        'recepcion' => isset($_POST['recepcion']) ? 1 : 0,
        'inicio_reparacion' => isset($_POST['inicio_reparacion']) ? 1 : 0,
        'cambio_status' => isset($_POST['cambio_status']) ? 1 : 0,
        'cierre_orden' => isset($_POST['cierre_orden']) ? 1 : 0,
        'presupuesto' => isset($_POST['presupuesto']) ? 1 : 0,
        'facturacion' => isset($_POST['facturacion']) ? 1 : 0,
      ];
      //print_r($_POST);die();
      if ($_POST['existe']) {
        $data['updated_at'] = date('Y-m-d H:i:s');
        //Existe y hay que actualizar las configuraciones
        $this->db->where('id_cita', $data['id_cita'])->update('configuracion_notificaciones', $data);
      } else {
        $data['created_at'] = date('Y-m-d H:i:s');
        $this->db->insert('configuracion_notificaciones', $data);
      }
     //Actualizar teléfono
     $this->db->where('id_cita',$data['id_cita'])->set('datos_email',$_POST['correo'])->set('email',$_POST['correo'])->set('datos_telefono',$_POST['telefono'])->update('citas');
     $this->db->where('id_cita',$data['id_cita'])->set('telefono_movil',$_POST['telefono'])->update('ordenservicio');
      echo 1;
      exit();
    } else {
      $errors = array(
        'telefono' => form_error('telefono'),
        'correo' => form_error('correo'),
      );
      echo json_encode($errors);
      exit();
    }
  }
}
