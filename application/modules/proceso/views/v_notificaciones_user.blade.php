<div style="height: 500px; overflow-y: scroll;">
    @foreach ($notificaciones as $n => $notificacion)
        <div id="accordion" style="padding-bottom:10px">
            <div class="card">
                <div class="card-header" id="noti{{ $notificacion->id }}" style="background:#2E8EEC;">
                    <button style="color:#fff;" class="btn btn-link" data-toggle="collapse"
                        data-target="#collapseNoti{{ $notificacion->id }}" aria-expanded="true"
                        aria-controls="collapseNoti{{ $notificacion->id }}">
                        <div class="row">
                            <div class="col-sm-12 col-md-6" style="text-align: left">
                                <img id="noti-logo-ford" src="{{base_url('img/ford-logo.png')}}">
                                <img id="noti-logo-angencia" src="{{ base_url('img/white-logo.png') }}">
                                <i style="color: #fff" class="fa fa-chevron-right" aria-hidden="true"></i>
                            </div>
                            <div id="date-noti" class="col-sm-12 col-md-6">
                                <span>{{ date_eng2esp_time($notificacion->fecha_creacion) }}</span>
                            </div>
                        </div>
                    </button>
                </div>
                <div id="collapseNoti{{ $notificacion->id }}" class="collapse"
                    aria-labelledby=" noti{{ $notificacion->id }}" data-parent="#accordion">
                    <div class="card-body">
                        <h3 class="text-center">{{ $notificacion->titulo }}</h3>
                        <?php switch ($notificacion->controlador) {
                        case 'seguimiento_citas':
                        echo seguimiento_citas(encrypt($notificacion->id_cita), true);
                        break;

                        case 'seguimiento_orden':
                        echo seguimiento_orden(encrypt($notificacion->id_cita), true);
                        break;

                        case 'cambio_estatus':
                        echo cambio_estatus(encrypt($notificacion->id_cita), encrypt($notificacion->id_operacion),
                        $notificacion->estatusCambio, true);
                        break;

                        case 'presupuesto':
                        echo presupuesto(encrypt($notificacion->id_cita), true);
                        break;

                        case 'fin_reparacion':
                        echo fin_reparacion(encrypt($notificacion->id_cita), true);
                        break;

                        case 'facturacion':
                        echo facturacion(encrypt($notificacion->id_cita), true);
                        break;
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
