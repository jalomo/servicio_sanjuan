<div class="row">
    <div class="col-sm-12">
        <figure class="highcharts-figure">
            <div id="container"></div>
        </figure>
    </div>
</div>
<script>
    var series = [];
    var background = [];
    var radius = 100;
    var innerRadius = 0;
    var status = '';
    var color = [];
    @foreach($linea_tiempo as $s => $data)
    innerRadius = radius-8;
    status = "{{$data->status_anterior}}"

    status_nuevo = "{{$data->status_nuevo}}"
    cita_previa = "{{$cita_previa}}"
    if(status || cita_previa){
        series.push({
            name:(status)?status:'Cita',
            data: [{
                color: Highcharts.getOptions().colors["{{$s}}"],
                radius: radius+'%',
                innerRadius: innerRadius+'%',
                y: parseFloat("{{$data->minutos_transcurridos}}")
            }]
        })
        background.push(
            {
                outerRadius: radius+'%',
                innerRadius: innerRadius+'%',
                backgroundColor: Highcharts.color(Highcharts.getOptions().colors["{{$s}}"])
                    .setOpacity(0.3)
                    .get(),
                borderWidth: 0
            }
        )
        radius = radius-8;
    }
    @endforeach
    Highcharts.chart('container', {

        chart: {
            type: 'solidgauge',
            height: '110%',
        },

        title: {
            text: 'Tiempos por estatus en minutos',
            style: {
                fontSize: '24px'
            }
        },

        tooltip: {
            borderWidth: 0,
            backgroundColor: 'none',
            shadow: false,
            style: {
                fontSize: '16px'
            },
            valueSuffix: 'min',
            pointFormat: '{series.name}<br><span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}</span>',
            positioner: function(labelWidth) {
                return {
                    x: (this.chart.chartWidth - labelWidth) / 2,
                    y: (this.chart.plotHeight / 2) + 15
                };
            }
        },

        pane: {
            startAngle: 0,
            endAngle: 360,
            background:background
        },

        yAxis: {
            min: 0,
            max: 100,
            lineWidth: 0,
            tickPositions: []
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    enabled: false
                },
                linecap: 'round',
                stickyTracking: false,
                rounded: true
            }
        },

        series: series
    });

</script>
