<style>
    thead tr th {
        text-align: center
    }

    tbody tr td {
        text-align: center
    }

</style>
<div id="accordion">
    <div class="card">
        <div class="card-header" id="detail">
            <h5 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse"
                    data-target="#collapsedetail" aria-expanded="true"
                    aria-controls="collapsedetail">
                    Detalles del Proceso <i style="color: #808080" class="fa fa-chevron-right"
                        aria-hidden="true"></i>
                </button>
            </h5>
        </div>
        <div id="collapsedetail" class="collapse"
            aria-labelledby=" detail" data-parent="#accordion">
            <div class="card-body">
                <table class="table table-striped table-responsive">
                    <thead>
                        <tr style="background: #162ED3;color:white"> 
                            <th>Estatus anterior</th>
                            <th>Estatus actual</th>
                            <th>Inicio</th>
                            <th>Fin</th>
                            <th>Minutos Transcurridos</th>
                            <th>Motivo detención</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($linea_tiempo) > 0)
                            @foreach ($linea_tiempo as $l => $linea)
                                @if ($linea->status_anterior != ''||!$cita_previa)
                                    <tr>
                                        <td>{{ ($linea->status_anterior)?$linea->status_anterior:'Recepción' }}</td>
                                        <td>{{ $linea->status_nuevo }}</td>
                                        <td>{{ date_eng2esp_time($linea->inicio) }}</td>
                                        <td>{{ date_eng2esp_time($linea->fin) }}</td>
                                        <td>{{ $linea->minutos_transcurridos }}</td>
                                        <td>
                                            <?php echo $linea->motivo_detencion ?
                                            $motivos[$linea->motivo_detencion] : '-'; ?>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">
                                    Aún no se inicia el proceso de la operación
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
