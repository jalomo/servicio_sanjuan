<style>
    @import url("https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800&display=swap");
    
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box
    }

    body,
    input,
    textarea {
        font-family: "Poppins", sans-serif
    }
    .white{
        color : "#fff" !important
    }

    ::placeholder {
        color: #fff;
        opacity: 1
    }

    .form {
        width: 100%;
        max-width: 820px;
        background-color: #fff;
        border-radius: 4px;
        box-shadow: 0 0 20px 1px rgba(0, 0, 0, 0.1);
        z-index: 1000;
        overflow: hidden;
        display: grid;
        grid-template-columns: repeat(2, 1fr)
    }

    .contact-info-form {
        
        position: relative,
    }

    .circle {
        border-radius: 50%;
        background: linear-gradient(135deg, transparent 20%, #0000CC);
        position: absolute
    }

    .circle.one {
        width: 130px;
        height: 130px;
        top: 130px;
        right: -40px
    }

    .circle.two {
        width: 80px;
        height: 80px;
        top: 10px;
        right: 30px
    }

    .contact-info-form:before {
        content: "";
        position: absolute;
        width: 26px;
        height: 26px;
        background-color: #fff;
        transform: rotate(45deg);
        bottom: 66px;
        left: -13px
    }

    form {
        padding: 2.3rem 2.2rem;
        z-index: 10;
        overflow: hidden;
        position: relative
    }

    .title {
        color: #fff;
        font-weight: 500;
        font-size: 1.5rem;
        line-height: 1;
        margin-bottom: 0.7rem
    }
    .input {
        width: 100%;
        outline: none;
        border: 2px solid #fafafa;
        background: none;
        padding: 0.6rem 1.2rem;
        color: #fff;
        font-weight: 500;
        font-size: 0.95rem;
        letter-spacing: 0.5px;
        border-radius: 4px;
        transition: 0.3s
    }

    textarea.input {
        padding: 0.8rem 1.2rem;
        min-height: 150px;
        border-radius: 4px;
        resize: none;
        overflow-y: auto
    }
    .contact-info {
        padding: 2.3rem 2.2rem;
        position: relative
    }

    .contact-info .title {
        color: #0000CC
    }

    .text {
        color: #333;
        margin: 1.5rem 0 2rem 0
    }

    .icon {
        width: 28px;
        margin-right: 0.7rem
    }

    .contact-info:before {
        content: "";
        position: absolute;
        width: 110px;
        height: 100px;
        border: 22px solid #0000CC;
        border-radius: 50%;
        bottom: -77px;
        right: 50px;
        opacity: 0.3
    }

    .big-circle {
        position: absolute;
        width: 500px;
        height: 500px;
        border-radius: 50%;
        background: linear-gradient(to bottom, #0000CC);
        bottom: 50%;
        right: 50%;
        transform: translate(-40%, 38%)
    }

    .big-circle:after {
        content: "";
        position: absolute;
        width: 360px;
        height: 360px;
        background-color: #fafafa;
        border-radius: 50%;
        top: calc(50% - 180px);
        left: calc(50% - 180px)
    }

    .square {
        position: absolute;
        height: 400px;
        top: 50%;
        left: 50%;
        transform: translate(181%, 11%);
        opacity: 0.2
    }

    @media (max-width: 850px) {
        .form {
            grid-template-columns: 1fr
        }

        .contact-info:before {
            bottom: initial;
            top: -75px;
            right: 65px;
            transform: scale(0.95)
        }

        .contact-info-form:before {
            top: -13px;
            left: initial;
            right: 70px
        }

        .square {
            transform: translate(140%, 43%);
            height: 350px
        }

        .big-circle {
            bottom: 75%;
            transform: scale(0.9) translate(-40%, 30%);
            right: 50%
        }

        .text {
            margin: 1rem 0 1.5rem 0
        }
    }

    @media (max-width: 480px) {
        .contact-info:before {
            display: none
        }

        .square,
        .big-circle {
            display: none
        }

        form,
        .contact-info {
            padding: 1.7rem 1.6rem
        }
        .title {
            font-size: 1.15rem
        }
        .icon {
            width: 23px
        }

        .input {
            padding: 0.45rem 1.2rem
        }

        .btn {
            padding: 0.45rem 1.2rem
        }
    }

</style>
<form id="frm-conf" action="">
    <div class="form">
        <input type="hidden" id="id_cita_config" name="id_cita_config" value="{{ $id_cita_config }}">
        <input type="hidden" id="existe" name="existe" value="{{ $existe }}">
        <div class="contact-info">
            <h3 class="title">Confirmación de datos</h3>
            <div class="info">
                <div class="row">
                    <div class="col-sm-12">
                        <label for="">Correo electrónico</label>
                        <input class="form-control" type="correo" name="correo" id="correo" value="{{$correo}}">
                        <span class="error error_correo"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="">Celular</label>
                        <input class="form-control" maxlength="10" type="text" name="telefono" id="telefono" value="{{($telefono)?$telefono:$telefono2}}">
                        <span class="error error_telefono"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-info-form white"> <!--<span class="circle one"></span> <span class="circle two"></span>-->
            <br>
            <h5 style="color:#0000CC">Configuración de envío de notificaciones</h5>
            
            
            <input type="checkbox" name="recepcion"
                {{ !isset($notificaciones) || $notificaciones->recepcion ? 'checked' : '' }}>
                <span style="color:black !important; font-size:14px">
                Notificación y Seguimiento en Línea de Recepción <br><br>
                </span>
                <input type="checkbox" name="cambio_status"
                {{ !isset($notificaciones) || $notificaciones->cambio_status ? 'checked' : '' }}>
                <span style="color:black !important; font-size:14px">Notificación y Seguimiento en Línea de Reparación</span><br>
                <input type="checkbox" name="presupuesto"
                {{ !isset($notificaciones) || $notificaciones->presupuesto ? 'checked' : '' }}>
                <span style="color:black !important; font-size:14px">Notificación y Seguimiento en Línea de Presupuesto <br></span><br>
                <input type="checkbox" name="cierre_orden"
                {{ !isset($notificaciones) || $notificaciones->cierre_orden ? 'checked' : '' }}><span style="color:black !important; font-size:14px">Notificación y Seguimiento en Línea de Fin de Reparación <br></span><br>
                <input type="checkbox" name="facturacion"
                {{ !isset($notificaciones) || $notificaciones->facturacion ? 'checked' : '' }}>
                <span style="color:black !important; font-size:14px">Notificación y Seguimiento en Línea de Facturación <br></span>
        </div>
    </div>
</form>
