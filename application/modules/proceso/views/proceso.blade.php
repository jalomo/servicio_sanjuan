{{-- https://bbbootstrap.com/users/luis-alberto-pita-vazquez-29759/forksnippets/social-profile-container-63944396 --}}
{{-- https://bbbootstrap.com/users/luis-alberto-pita-vazquez-29759/forksnippets/multi-step-form-wizard-30467045 --}}
@layout('layout')
@section('contenido')
<link rel="stylesheet" href="css/custom/proceso.css">

@endsection
@section('scripts')
    <!-- MultiStep Form -->
    <input type="hidden" id="id_cita" name="id_cita" value="{{$id_cita}}">
    <div class="container-fluid" id="grad1">
        <div class="row justify-content-center mt-0">
            <div class="col-11 col-sm-9 col-md-7 col-lg-11 text-center p-0 mt-3 mb-2">
                <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                    <div class="row">
                        <div class="col-sm-6 text-left">
                            <i id="configuracion" style="cursor: pointer;font-size:35px;margin-left:20px" title="Mis configuraciones" class="fa fa-cog"></i>
                        </div>
                        <div class="col-sm-6">
                            <span id="notification" class="fa-stack fa-3x pull-right" style="margin-right:30px;cursor: pointer;" data-count="{{count($notificaciones)}}">
                                <i title="Mis notificaciones" class="fa fa-bell fa-stack-1x fa-inverse pull-right"></i>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 logo_ford">
                            <img style="margin-left: 20px;max-height:100px;max-width:200px" src="{{base_url('img/logo.jpeg')}}">
                        </div>
                        <div class="col-sm-6 logo_agencia">
                            <img style="margin-right: 20px;max-height:100px;max-width:200px" src="{{base_url('img/fordround.png')}}">
                        </div>
                    </div>
                    <h2 class="proceso"><strong>Proceso de mi orden</strong></h2>
                    <div class="row">
                        <div class="col-sm-4">
                            <strong>Distribuidora:</strong> <span>{{CONST_AGENCIA}}</span>
                        </div>
                        <div class="col-sm-4">
                            <strong>Dirección:</strong> <span>{{CONST_DIRECCION}}</span>
                        </div>
                        <div class="col-sm-4">
                            <strong>Teléfono:</strong> <span><a href="tel:{{CONST_TEL_HREF}}">{{CONST_TEL_EMPRESA}}</a></span>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 mx-0">
                            <form id="msform">
                                <!-- progressbar -->
                                <?php
                                    $active_recepcion = '';
                                    if($cita->cita_previa){
                                        if($cita->id_status_cita==2||$cita->id_status_cita==3 ||$cita->id_status_cita==5){
                                            $active_recepcion = 'active';
                                        }
                                    }else{
                                        $active_recepcion = 'active real-active';
                                    }
                                ?>
                                <ul id="progressbar">
                                    <li class="select_tab {{($cita->cita_previa)?'active real-active':''}}" id="cita"><strong>CITA</strong></li>
                                    <li class="select_tab {{$active_recepcion}}" id="recepcion"><strong>RECEPCIÓN</strong></li>
                                    <li class="select_tab {{($hora_inicio_trabajo)?'active':''}}" id="reparacion_inicial"><strong>REPARACIÓN</strong></li>
                                    <li class="select_tab {{($presupuesto&&$firmo_asesor)?'active':''}}" id="presupuesto"><strong>PRESUPUESTO ADICIONAL</strong></li>
                                    <li class="select_tab {{($cita->JobCompletionDate)?'active':''}}" id="reparacion"><strong>REPARACIÓN TERMINADA</strong></li>
                                    <li class="select_tab {{($cita->factura_pdf)?'active':''}}" id="facturacion"><strong>FACTURACIÓN</strong></li>
                                </ul> <!-- fieldsets -->
                                <fieldset id="fieldset_cita" class="{{(!$cita->cita_previa)?'ocultar':''}}">
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h2 class="text-center" style="color: black">Cita</h2>
                                                <strong>Fecha Cita:</strong>
                                                <span>{{ date_eng2esp_1($cita->fecha_cita) . ' a las ' . $cita->hora_cita }}</span><br>
                                                <strong>Folio Cita: </strong> <span>{{ $cita->id_cita }}</span> <br>
                                                <strong>Unidad: </strong>
                                                <span>{{ $cita->vehiculo_modelo . ' ' . $cita->vehiculo_anio . ' placas: ' . $cita->vehiculo_placas . ', color: ' . $cita->color . ', VIN: ' . $cita->vehiculo_numero_serie }}</span><br>
                                                <strong>Servicio: </strong> <span>{{ $cita->servicio }}</span> <br>
                                                <strong>Asesor: </strong> <span>{{ $cita->asesor }}</span> <br>                                                
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset id="fieldset_recepcion" class="{{(!$cita->cita_previa)?'mostrar':''}}">
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h2 class="text-center" style="color: black">Recepción</h2>
                                                <strong>Fecha Ingreso a la Distribuidora:</strong>
                                                <span>{{ date_eng2esp_time($cita->created_at)}}</span><br>
                                                <strong>Folio Orden de Servicio: </strong> <span>{{ $cita->id_cita }}</span> <br>
                                                <strong>Unidad: </strong>
                                                <span>{{ $cita->vehiculo_modelo . ' ' . $cita->vehiculo_anio . ' placas: ' . $cita->vehiculo_placas . ', color: ' . $cita->color . ', VIN: ' . $cita->vehiculo_numero_serie }}</span><br>
                                                <strong>Servicio: </strong> <span>{{ $cita->servicio }}</span> <br>
                                                <strong>Asesor: </strong> <span>{{ $cita->asesor }}</span> <br>
                                                <strong>Si desea contactar con nosotros vía teléfono: </strong><a href="tel:{{CONST_TEL_HREF}}">{{CONST_TEL_EMPRESA}}</a><br>
                                                <strong>Si desea contactar al asesor vía Chat: </strong><a target="_blank" href="https://api.whatsapp.com/send?phone=+52{{$asesor->telefono}}&text=%20FORD%20MYLSA%20SJR,%20Necesito%20contactar%20a%20mi%20asesor%20{{$cita->asesor}}%20sobre%20mi%20Orden%20No.%20{{$cita->id_cita}}">{{$asesor->telefono}}</a> <br>
                                                <strong>Consulta expediente digital: </strong> <a target="_blank" href="{{CONST_URL_CONEXION.'OrdenServicio_PDF/'.encrypt($cita->id_cita)}}">Orden</a> <br>
                                            </div>
                                        </div>
                                    </div> 
                                </fieldset>
                                <fieldset id="fieldset_reparacion_inicial">
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h2 class="text-center" style="color: black">Reparación</h2>
                                                <strong>Fecha Ingreso al taller:</strong>
                                                <span>{{ date_eng2esp_time($hora_inicio_trabajo)}}</span><br>
                                                <strong>Folio Orden de Servicio: </strong> <span>{{ $cita->id_cita }}</span> <br>
                                                <strong>Unidad: </strong>
                                                <span>{{ $cita->vehiculo_modelo . ' ' . $cita->vehiculo_anio . ' placas: ' . $cita->vehiculo_placas . ', color: ' . $cita->color . ', VIN: ' . $cita->vehiculo_numero_serie }}</span><br>
                                                <strong>Servicio: </strong> <span>{{ $cita->servicio }}</span> <br>
                                                <strong>Asesor: </strong> <span>{{ $cita->asesor }}</span> <br>
                                                <strong>Si desea contactar con nosotros vía teléfono: </strong><a href="tel:{{CONST_TEL_HREF}}">{{CONST_TEL_EMPRESA}}</a><br>
                                                <strong>Si desea contactar al asesor vía Chat: </strong><a target="_blank" href="https://api.whatsapp.com/send?phone=+52{{$asesor->telefono}}&text=%20FORD%20MYLSA%20SJR,%20Necesito%20contactar%20a%20mi%20asesor%20{{$cita->asesor}}%20sobre%20mi%20Orden%20No.%20{{$cita->id_cita}}">{{$asesor->telefono}}</a> <br>
                                                <strong>Consulta expediente digital: </strong> <a target="_blank" href="{{CONST_URL_CONEXION.'OrdenServicio_PDF/'.$cita->id_cita}}">Orden</a> <br>
                                            </div>
                                        </div>
                                        <br>
                                       <strong>**Selecciona una operación para visualizar la línea del tiempo</strong>
                                        <!--<h3 class="text-center" style="color: black">Línea de tiempo de operaciones</h3>-->
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label for="">Operaciones planeadas</label>
                                                {{$drop_operaciones}}
                                            </div>
                                        </div>
                                       <div class="row">
                                           <div class="col-sm-12">
                                                <div id="div_grafica_linea"></div>
                                           </div>
                                       </div>
                                        <!--tabla de las operaciones -->
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <br>
                                                <div id="div_operaciones">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset id="fieldset_presupuesto">
                                    <div class="form-card">
                                        @if($presupuesto&&$firmo_asesor)
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <strong>Estatus proceso: </strong> <span> {{($cat_estatus_presupuesto[$estatus_presupuesto])?$cat_estatus_presupuesto[$estatus_presupuesto]:''}}</span> <br>
                                                <strong>Estatus actual ventanilla: </strong> <span> {{($cat_estatus_refacciones_ventanilla[$estatus_refacciones_ventanilla]?$cat_estatus_refacciones_ventanilla[$estatus_refacciones_ventanilla]:'')}}</span> <br>
                                            </div>
                                        </div>
                                        <div class="row">
                                            
                                            <iframe src="{{CONST_URL_CONEXION.'Presupuesto_Cliente/'.encrypt($cita->id_cita)}}" width="100%" height="100%" style="display:block;height: 600px;width:100%;border:none !important;">
                                                <p>Your browser does not support iframes.</p>
                                              </iframe>
                                        </div>
                                        @endif
                                    </div>
                                </fieldset>
                                <fieldset id="fieldset_reparacion">
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h2 class="text-center" style="color: black">Reparación terminada</h2>
                                                <strong>Último Estatus:</strong>
                                                <span>Trabajos terminados en Taller</span><br>
                                                <strong>Vehículo disponible para entrega a la fecha/hora: </strong> <span>{{date_eng2esp_1($cita->fecha_entrega)}} a las {{$cita->hora_entrega}}</span> <br>
                                                <strong>Técnicos asignados en el proceso de reparación: </strong>
                                                <span>{{$tecnicos_asignados}}</span><br>
                                                <strong>Comentarios Técnico: </strong> <span>-</span> <br>
                                                <strong>Si desea contactar con nosotros vía teléfono: </strong><a href="tel:{{CONST_TEL_HREF}}">{{CONST_TEL_EMPRESA}}</a><br>
                                                <strong>Si desea contactar al asesor vía Chat: </strong><a target="_blank" href="https://api.whatsapp.com/send?phone=+52{{$asesor->telefono}}&text=%20FORD%20MYLSA%20SJR,%20Necesito%20contactar%20a%20mi%20asesor%20{{$cita->asesor}}%20sobre%20mi%20Orden%20No.%20{{$cita->id_cita}}">{{$asesor->telefono}}</a> <br>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset id="fieldset_facturacion">
                                    <div class="form-card">
                                        <h2 class="text-center" style="color: black">Facturación</h2>
                                        <div class="row">
                                            <div class="col-sm-6 col-md-3">
                                                <div class="form-group">
                                                    <a target="_blank" href="{{base_url('/assets/facturas/'.$cita->factura_pdf)}}">
                                                        <label for="">Descargar factura formato pdf</label> <br>
                                                        <img class="icons" src="img/icons/pdf.png" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-3">
                                                <div class="form-group">
                                                    <a target="_blank" href="{{base_url('/assets/facturas/'.$cita->factura_xml)}}">
                                                        <label for="">Descargar factura formato xml</label> <br>
                                                        <img class="icons" src="img/icons/xml.png" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="accordion">
                                @if($cita->idorden)
                                    <div class="card">
                                        <div class="card-header" id="headingDetalleServicio">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link" data-toggle="collapse"
                                                    data-target="#collapseAsesorServicio" aria-expanded="true"
                                                    aria-controls="collapseAsesorServicio">
                                                    Detalles del Servicio <i style="color: #808080" class="fa fa-chevron-right"
                                                        aria-hidden="true"></i>
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="collapseAsesorServicio" class="collapse {{($showDetails==1)?'show':''}}""
                                            aria-labelledby="headingDetalleServicio" data-parent="#accordion">
                                            <div class="card-body">
                                                <h3 id="div_documentacion" class="text-center">Documentación</h3>
                                                <div class="row">
                                                    <div class="col-sm-6 col-md-3">
                                                        <div class="form-group">
                                                            <a target="_blank" href="{{CONST_URL_CONEXION.'OrdenServicio_PDF/'.$cita->id_cita}}">
                                                                <label for="">Ver Orden de servicio</label> <br>
                                                                <img class="icons" src="img/icons/orden-servicio.png" alt="">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3">
                                                        <div class="form-group">
                                                            <a target="_blank" href="{{CONST_URL_CONEXION.'multipunto/Multipunto/setDataPDF/'.encrypt($cita->id_cita)}}">
                                                                <label for="">Ver hoja multipunto</label> <br>
                                                                <img class="icons" src="img/icons/multipunto.png" alt="">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @if($existe_formato_voz_cliente)
                                                        <div class="col-sm-6 col-md-3">
                                                            <div class="form-group">
                                                                <a target="_blank" href="{{CONST_URL_CONEXION.'VCliente_PDF/'.encrypt($cita->id_cita)}}">
                                                                    <label for="">Voz cliente</label> <br>
                                                                    <img class="icons" src="img/icons/voz-cliente.png" alt="">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    @if($existe_carta_renuncia)
                                                        <div class="col-sm-6 col-md-3">
                                                            <div class="form-group">
                                                                <a target="_blank" href="{{CONST_URL_CONEXION.'CartaRenuncia_PDF/'.encrypt($cita->id_cita)}}">
                                                                    <label for="">Carta de renuncia a beneficios</label> <br>
                                                                    <img class="icons" src="img/icons/carta-renuncia.png" alt="">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="container">
                                                    <h3 class="text-center">Lista de operaciones</h3>
                                                    <table class="table table-hover table-stripped table-responsive">
                                                        <thead>
                                                            <tr style="background: #162ED3;color:white">
                                                                <th class="text-center">Operación</th>
                                                                <th class="text-center">Técnico</th>
                                                                <th class="text-center">Estatus</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($operaciones as $o => $operacion)
                                                                <tr>
                                                                    <td>{{$operacion->descripcion}}</td>
                                                                    <td>{{$operacion->tecnico}}</td>
                                                                    <td>{{$operacion->estatus_operacion}}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="card">
                                    <div class="card-header" id="headingDetalleServicio">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse"
                                                data-target="#DetalleServicio" aria-expanded="false"
                                                aria-controls="DetalleServicio">
                                                Detalles del Asesor de Servicio <i style="color: #808080"
                                                    class="fa fa-chevron-right" aria-hidden="true"></i>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="DetalleServicio" class="collapse {{($showDetails==2)?'show':''}}" aria-labelledby="headingDetalleServicio"
                                        data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="page-content page-container" id="page-content">
                                                    <div id="div_asesor_servicio" class="row d-flex justify-content-center">
                                                        <div class="col-xl-6 col-md-12">
                                                            <div class="card user-card-full">
                                                                <div class="row m-l-0 m-r-0">
                                                                    <div class="col-sm-4 bg-c-lite-green user-profile">
                                                                        <div class="card-block text-center text-white">
                                                                            <div class="m-b-25"> <img
                                                                                    src="https://img.icons8.com/bubbles/100/000000/user.png"
                                                                                    class="img-radius"
                                                                                    alt="User-Profile-Image"> </div>
                                                                            <h6 class="f-w-600">{{$cita->asesor}}</h6>
                                                                            <p>Asesor de Servicio</p> <i
                                                                                class=" mdi mdi-square-edit-outline feather icon-edit m-t-10 f-16"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-8">
                                                                        <div class="card-block">
                                                                            <h6 class="m-b-20 p-b-5 b-b-default f-w-600">
                                                                                Información del Asesor de Servicio</h6>
                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <p class="m-b-10 f-w-600">Email <i class="fa fa-envelope-o"></i></p>
                                                                                    <h6 class="text-muted f-w-400">
                                                                                        
                                                                                        <a target="_blank" href = "mailto:{{$asesor->correo}}?subject=Quiero contactar a mi asesor de servicio {{$cita->asesor}} sobre mi orden # {{$id_cita}} &body=Cliente: {{$cita->datos_nombres}} {{$cita->datos_apellido_paterno}} {{$cita->datos_apellido_materno}} #Orden:{{$id_cita}}, Datos de la unidad: {{ $cita->vehiculo_modelo . ' ' . $cita->vehiculo_anio . ' placas: ' . $cita->vehiculo_placas . ', color: ' . $cita->color . ', VIN: ' . $cita->vehiculo_numero_serie }}    ">{{$asesor->correo}}</a>

                                                                                        <br></h6>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <p class="m-b-10 f-w-600">Teléfono <i class="fa fa-phone"></i></p>
                                                                                    <h6 class="text-muted f-w-400">
                                                                                        <a href="tel:{{$asesor->telefono}}">{{$asesor->telefono}}</a>
                                                                                    </h6>
                                                                                </div>
                                                                            </div>
                                                                            <br>
                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <p class="m-b-10 f-w-600">WhatsApp <i class="fa fa-whatsapp"></i></p>
                                                                                    <h6 class="text-muted f-w-400">
                                                                                        <a target="_blank" href="https://api.whatsapp.com/send?phone=+52{{$asesor->telefono}}&text=%20FORD%20MYLSA%20SJR,%20Necesito%20contactar%20a%20mi%20asesor%20{{$cita->asesor}}%20sobre%20mi%20Orden%20No.%20{{$cita->id_cita}}">mandar mensaje</a>
                                                                                    </h6>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <p class="m-b-10 f-w-600">Horario de atención<i class="fa fa-clock"></i></p>
                                                                                    <h6 class="text-muted f-w-400">
                                                                                        {{$asesor->horario}}
                                                                                    </h6>
                                                                                </div>
                                                                            </div>
                                                                            <ul
                                                                                class="social-link list-unstyled m-t-40 m-b-10">
                                                                                <li><a href="#!" data-toggle="tooltip"
                                                                                        data-placement="bottom" title=""
                                                                                        data-original-title="facebook"
                                                                                        data-abc="true"><i
                                                                                            class="mdi mdi-facebook feather icon-facebook facebook"
                                                                                            aria-hidden="true"></i></a></li>
                                                                                <li><a href="#!" data-toggle="tooltip"
                                                                                        data-placement="bottom" title=""
                                                                                        data-original-title="twitter"
                                                                                        data-abc="true"><i
                                                                                            class="mdi mdi-twitter feather icon-twitter twitter"
                                                                                            aria-hidden="true"></i></a></li>
                                                                                <li><a href="#!" data-toggle="tooltip"
                                                                                        data-placement="bottom" title=""
                                                                                        data-original-title="instagram"
                                                                                        data-abc="true"><i
                                                                                            class="mdi mdi-instagram feather icon-instagram instagram"
                                                                                            aria-hidden="true"></i></a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingEvidencia">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse"
                                                data-target="#Evidencia" aria-expanded="false"
                                                aria-controls="Evidencia">
                                                Evidencia del proceso <i style="color: #808080"
                                                    class="fa fa-chevron-right" aria-hidden="true"></i>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="Evidencia" class="collapse" aria-labelledby="headingEvidencia"
                                        data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="page-content page-container" id="page-content">
                                                @if($voz_cliente)
                                                <div class="row pull-center">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <a id="reproducir_voz_cliente" data-url="{{$voz_cliente}}" href="#">
                                                                <img class="icons" src="img/icons/reproductor.png" alt=""><br>
                                                                <label for="">Reproducir voz cliente</label> <br>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                @endif
                                                @if(count($evidencia)>0)
                                                    <h3 class="text-center">Imágenes</h3>
                                                    <div class="row">
                                                        @foreach($evidencia as $e => $imagen)
                                                            @if($imagen->tipo==2)
                                                                <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        <a class="visualizar_evidencia" data-tipo="{{$imagen->tipo}}"  data-url="{{$imagen->video}}" href="#">
                                                                            <img width="60%" class="icons" src="img/icons/image.png" alt=""><br>
                                                                            <label for="">visualizar imagen</label> <br>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                    <hr>
                                                    <h3 class="text-center">Videos</h3>
                                                    <div class="row">
                                                        @foreach($evidencia as $e => $video)
                                                            @if($video->tipo==1)    
                                                                <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        <a class="visualizar_evidencia" data-tipo="{{$video->tipo}}"  data-url="{{$video->video}}" href="#">
                                                                            <img width="60%" class="icons" src="img/icons/reproductor-de-video.png" alt=""><br>
                                                                            <label for="">Reproducir video</label> <br>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                @endif
                                                @if(count($evidencia_multipunto)>0)
                                                <hr>
                                                    <h3 class="text-center">Evidencia multipunto</h3>
                                                    <div class="row">
                                                        @foreach($evidencia_multipunto as $e => $row)
                                                            <?php $info_evidencia = explode("|",$row->archivo);  ?> 
                                                            @foreach($info_evidencia as $d => $rep)
                                                                @if($rep!='')
                                                                <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        <a target="_blank" href="{{CONST_URL_CONEXION.$rep}}">
                                                                            <img width="60%" class="icons" src="img/icons/visualizar-informacion.png" alt=""><br>
                                                                            <label for="">Visualizar</label> <br>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script>
        var site_url = "{{ site_url() }}";
        var id_cita = '';
        var accion = '';
        let currentTab = '';
        $(document).ready(function() {

            var current_fs, next_fs, previous_fs; //fieldsets
            var opacity;
            var showDetails = "{{$showDetails}}";
            // if(showDetails==1){
            //     let div_documentacion = $("#div_documentacion");
            //     var targetOffset = $(div_documentacion).offset().top;
            //     $('html,body').animate({scrollTop: targetOffset}, 1000);
            // }else{
            //     let div_asesor_servicio = $("#div_asesor_servicio");
            //     var targetOffset = $(div_asesor_servicio).offset().top;
            //     $('html,body').animate({scrollTop: targetOffset}, 1000);
            // }
            $(".next").click(function() {

                current_fs = $(this).parent();
                next_fs = $(this).parent().next();

                console.log(current_fs)
                console.log(next_fs)

                //Add Class Active
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                //show the next fieldset
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function(now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        next_fs.css({
                            'opacity': opacity
                        });
                    },
                    duration: 600
                });
            });

            $(".previous").click(function() {

                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();
                //Remove class active
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                //show the previous fieldset
                previous_fs.show();

                //hide the current fieldset with style
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function(now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        previous_fs.css({
                            'opacity': opacity
                        });
                    },
                    duration: 600
                });
            });
            $(".select_tab").on('click',function(){
                if($(this).hasClass('active')){
                    $.each( $(".select_tab"), function( i, val ) {
                        $(val).removeClass('real-active');
                    });
                    $(this).addClass('real-active');
                    selectTab($(this).prop('id'));
                }
                
            })
            $("body").on('change','#id_operacion',function(){
                let id_operacion = $(this).val();
                if(id_operacion){
                    getGrafica(id_operacion);
                }
            });
            $("body").on('click','#notification',function(){
                let id_cita = $("#id_cita").val();
                var url =site_url+"/proceso/mis_notificaciones";
                customModal(url,{id_cita:$("#id_cita").val()},"POST","lg","","","","Cerrar","Mis notificaciones","modal1");
            })
            $("body").on('click','#configuracion',function(){
                let id_cita = $("#id_cita").val();
                var url =site_url+"/proceso/configuraciones";
                customModal(url,{id_cita:$("#id_cita").val()},"POST","md",saveConf,"","Guardar","Cerrar","Mis configuraciones","modalConfig");
            })
            $("body").on('click','#reproducir_voz_cliente',function(e){
                e.preventDefault();
                let url_voz = $(this).data('url');
                var url =site_url+"/proceso/reproducir_voz_cliente";
                customModal(url,{url_voz},"POST","lg","","","","Cerrar","Voz cliente","modalVoz");
            });
            $("body").on('click','.visualizar_evidencia',function(e){
                e.preventDefault();
                let tipo = $(this).data('tipo');
                let url_evidencia = $(this).data('url');
                var url =site_url+"/proceso/reproducir_evidencia";
                customModal(url,{tipo,url_evidencia},"POST","md","","","","Cerrar","Evidencia","modalEvidencia");
            });
        });
       
        function selectTab (tab) {
                currentTab = $("#"+tab);
                current_fs = $("#"+tab).addClass("active");
                next_fs = $("#fieldset_"+tab);
                $.each( $("fieldset"), function( i, val ) {
                   $(val).hide();
                });
                $("#fieldset_recepcion").removeClass('mostrar');
                //show the next fieldset
                next_fs.show();
        }
        function saveConf(){
            ajaxJson(site_url+"/proceso/guardarConf",$("#frm-conf").serialize(),"POST","",function(result){
                if(isNaN(result)){
                        data = JSON.parse( result );
                        //Se recorre el json y se coloca el error en la div correspondiente
                        $.each(data, function(i, item) {
                            $(".error_"+i).empty();
                            $(".error_"+i).append(item);
                            $(".error_"+i).css("color","red");
                        });
                }else{
                    if(result==1){
                        ExitoCustom('Configuraciones actualizadas correctamente',function(){
                            $('.close').trigger('click')
                        });
                    }else{
                        ErrorCustom('Error al actualizar las configuraciones por favor intenta otra vez');
                    }
                }
            });  
        }
        function buscarOperaciones(id_operacion){
            var url =site_url+"/proceso/getLineaTiempoOperacion";
                ajaxLoad(url,{id_operacion,id_cita:$("#id_cita").val()},"div_operaciones","POST",function(){
                    
            });
        } 
        function getGrafica(id_operacion){
            var url = site_url + "/proceso/buscar_grafica_linea_tiempo";
                ajaxLoad(url, {id_operacion,id_cita:$("#id_cita").val()}, "div_grafica_linea", "POST", function() {
                    buscarOperaciones(id_operacion)
            });
        }   
    </script>
@endsection
