<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_proceso extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  public function getAllData($id_cita = '')
  {
    return $this->db->where('c.id_cita', $id_cita)
      ->join('ordenservicio o', 'o.id_cita = c.id_cita', 'left')
      ->join('aux a', 'a.id = c.id_horario', 'left')
      ->join('cat_colores cc', 'cc.id=c.id_color')
      ->select('c.*,o.*,o.id as idorden,c.id_cita,a.hora as hora_cita,a.fecha as fecha_cita,a.id_status_cita,cc.color')
      ->get('citas c')
      ->result();
  }
  //Nombre asesor
  public function asesor_by_nombre($nombre = '')
  {
    $q = $this->db->where('nombre', $nombre)->from('operadores')->get();
    if ($q->num_rows() == 1) {
      return $q->row();
    } else {
      return '';
    }
  }
  //Operaciones de una orden
  public function getOperaciones($id_cita = '')
  {
    return $this->db->select('a.id,a.idorden,a.descripcion,t.nombre as tecnico,cs.status as estatus_operacion')
      ->join('articulos_orden a', 'o.id = a.idorden')
      ->join('tecnicos t', 't.id = a.id_tecnico', 'left')
      ->join('cat_status_citas_tecnicos cs', 'cs.id = a.id_status', 'left')
      ->where('o.id_cita', $id_cita)
      ->where('cancelado', 0)
      ->where('no_planificado', 0)
      ->get('ordenservicio o')
      ->result();
  }
  //Obtener la línea de tiempo de una operacion
  public function lineaTiempoOperacion($id_cita = 0, $id_operacion = '')
  {
    $query = "select * from transiciones_estatus
    where (id_operacion = " . $id_operacion . " or id_operacion is null and id_cita = " . $id_cita . ")
    or (id_cita = " . $id_cita . " and id_operacion = " . $id_operacion . ") or (id_cita = " . $id_cita . " and id_operacion = 0)";
    return $this->db->query($query)->result();
  }
  public function getTecnicosAsignados($id_cita = '')
  {
    $array_tecnicos = [];
    $tecnicos = $this->db->where('id_cita', $id_cita)
      ->join('tecnicos t', 't.id=tc.id_tecnico')
      ->select('distinct(t.nombre) as tecnico')
      ->get('tecnicos_citas tc')
      ->result();

    $tecnicos_historial = $this->db->where('id_cita', $id_cita)
      ->join('tecnicos t', 't.id=tc.id_tecnico')
      ->select('distinct(t.nombre) as tecnico')
      ->get('tecnicos_citas_historial tc')
      ->result();

    $data = array_merge($tecnicos, $tecnicos_historial);
    foreach ($data as $d => $tecnico) {
      $array_tecnicos[$tecnico->tecnico] = $tecnico->tecnico;
    }

    if (count($array_tecnicos) > 0) {
      return implode(', ', $array_tecnicos);
    } else {
      return '';
    }
  }
  public function dataPresupuesto($id_cita = '')
  {
    return $this->principal->getGeneric('id_cita', $id_cita, 'presupuesto_registro');
  }
  public function status_presupuesto($id_cita = '')
  {
    $q = $this->db->where('id_cita', $id_cita)->select('acepta_cliente')->order_by('id', 'DESC')->limit(1)->get('presupuesto_registro');
    if ($q->num_rows() == 1) {
      $respuesta = $q->row()->acepta_cliente;
    } else {
      $respuesta = '0';
    }
    return $respuesta;
  }
  public function status_refacciones_ventanilla($id_cita = '')
  {
    $q = $this->db->where('id_cita', $id_cita)->select('estado_refacciones')->order_by('id', 'DESC')->limit(1)->get('presupuesto_registro');
    if ($q->num_rows() == 1) {
      $respuesta = $q->row()->estado_refacciones;
    } else {
      $respuesta = '0';
    }
    return $respuesta;
  }
  public function getNotificaciones($id_cita = '')
  {
    return $this->db->select('n.id,c.controlador,n.id_operacion,n.estatusCambio,n.id_cita,n.created_at as fecha_creacion,c.titulo')
      ->join('cat_notificaciones_usuario c', 'c.id=n.id_tipo_notificacion')
      ->where('id_cita', $id_cita)
      ->order_by('n.created_at', 'desc')
      ->get('notificaciones_usuario_orden n')
      ->result();
  }
  //Obtener la voz cliente
  public function audio_evi($id_cita = '')
  {
    $q = $this->db->where('id_cita', $id_cita)->select('evidencia_voz_cliente')->limit(1)->get('documentacion_ordenes');
    if ($q->num_rows() == 1 && $q->row()->evidencia_voz_cliente != '') {
      $retorno = CONST_URL_CONEXION . $q->row()->evidencia_voz_cliente;
    } else {
      $retorno = '';
    }
    return $retorno;
  }
  public function existe_carta_renuncia($id_cita = '')
  {
    $q = $this->db->where('id_cita', $id_cita)->select('firma_cliente')->limit(1)->get('carta_renuncia');
    if ($q->num_rows() == 1 && $q->row()->firma_cliente != '') {
      return true;
    } 
    return false;
  }
  public function existe_formato_voz_cliente($id_cita = '')
  {
    $q = $this->db->where('id_cita', $id_cita)->select('voz_cliente')->limit(1)->get('documentacion_ordenes');
    if ($q->num_rows() == 1 && $q->row()->voz_cliente ==1) {
      return true;
    } 
    return false;
  }
  //Obtener la evidencia de citas 
  public function getEvidencia($id_cita = '')
  {
    return $this->db->where('id_cita', $id_cita)->get('evidencia')->result();
  }
  //Obtener la evidencia de la multipunto
  public function evidencia_multipunto($id_cita = 0)
  {
    $query = $this->db->where("mg.orden", $id_cita)
      ->join('multipunto_general_3 AS mg3', 'mg3.idOrden = mg.id')
      ->select('mg3.comentario,mg3.archivo')
      ->limit(1)
      ->get('multipunto_general AS mg')
      ->result();
    return $query;
  }
  public function getConfiguraciones($id_cita=''){
    return $this->db->where('id_cita', $id_cita)->get('configuracion_notificaciones')->row();
  }
}
