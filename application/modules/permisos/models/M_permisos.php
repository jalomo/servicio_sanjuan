<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_permisos extends CI_Model{
  public $dias_transcurridos = 0;
  public $horas_transcurridos = 0;
  public $minutos_transcurridos = 0;
  public function __construct(){
    parent::__construct();
    date_default_timezone_set('America/Mazatlan');
  }

  public function getUser(){
    $this->db->where('status !=',4);
    $this->db->where('adminStatus !=',1);
    $this->db->where('status !=',1);
    return $this->db->get(CONST_BASE_PRINCIPAL.'admin')->result();
  }
  public function tienePermisoAccion($id_usuario='',$accion=''){
    $q = $this->db->where('id_usuario',$id_usuario)->where('accion',$accion)->get('permisos_acciones_usuarios');
    if($q->num_rows()==1){
      return true;
    }else{
      return false;
    }
  }
  public function tienePermisoModulo($id_usuario='',$controlador='',$accion=''){
    if($accion!=''){
      $this->db->where('accion',$accion);
    }
    $q = $this->db->where('id_usuario',$id_usuario)->where('controlador',$controlador)->get('permisos_modulos');
    if($q->num_rows()==1){
      return true;
    }else{
      return false;
    }
  }
  
}
