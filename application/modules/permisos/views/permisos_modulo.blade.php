@layout('tema_luna/layout')
<style>

</style>
@section('contenido')

<form action="" id="frm">
  <div class="container1">
    <h2>Permisos de usuarios por módulos</h2>

    <table class="table table-fixed">
      <thead>
        <tr>
          <th>Usuario</th>
          <th>Reagendar cita</th>
          <th>Contacto proactivo</th>
          <th>Importar formatos</th>
        </tr>
      </thead>
      <tbody>
        @foreach($usuarios as $u => $usuario)
        <tr>
          <td>{{$usuario->adminId.'-'.$usuario->adminNombre}}</td>
          <td>
            @if($this->mp->tienePermisoModulo($usuario->adminId,'citas','reagendar'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="citas[{{$usuario->adminId}}]" value="reagendar">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="citas[{{$usuario->adminId}}]" value="reagendar">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoModulo($usuario->adminId,'contacto_proactivo',''))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="contacto_proactivo[{{$usuario->adminId}}]" value="">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="contacto_proactivo[{{$usuario->adminId}}]" value="">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoModulo($usuario->adminId,'importar_excel',''))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="importar_excel[{{$usuario->adminId}}]" value="">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="importar_excel[{{$usuario->adminId}}]" value="">
            @endif
          </td>
          
        </tr>
        @endforeach
      </tbody>
    </table>
  </form>
  <div class="row pull-right">
    <div class="col-sm-12">
      <button type="button" class="btn btn-success" id="guardar_permisos">Guardar Permisos</button>
    </div>
  </div>
  <br>
  <br>
</div>
@endsection

@section('scripts')
<script>
  var site_url = "{{site_url()}}";
  $("#guardar_permisos").on('click',function(){
    var url =site_url+"/permisos/updatePermisosModulo/0";
    ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
      if(result){
        ExitoCustom("Permisos actualizados correctamente",function(){
          window.location.reload();
        });
      }else{
        ErrorCustom("Error al actualizar los permisos",function(){
          window.location.reload();
        });
      }
    });
  });
</script>
@endsection
