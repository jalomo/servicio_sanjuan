@layout('tema_luna/layout')
<style>
</style>
@section('contenido')

<form action="" id="frm">
  <div class="container1">
    <h2>Permisos de usuarios por acción</h2>

    <table class="table table-fixed table-hover">
      <thead>
        <tr>
          <th>Usuario</th>
          <th>Agendar cita</th>
          <th>Editar cita</th>
          <th>Eliminar cita</th>
          <th>Cancelar cita</th>
          <th>Editar orden</th>
          <th>Editar precio operacion</th>
          <th>Editar hora operación</th>
          <th>Eliminar operación</th>
          <th>Asignar op. No aplica</th>
          <th>Anexar factura orden</th>
          <th>Carryover reservación</th>
          <th>Cancelar orden</th>
        </tr>
      </thead>
      <tbody>
        @foreach($usuarios as $u => $usuario)
        <tr>
          <td>{{$usuario->adminId.'-'.$usuario->adminNombre}}</td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'agendar_cita'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="agendar_cita[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="agendar_cita[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'editar_cita'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="editar_cita[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="editar_cita[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'eliminar_cita'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="eliminar_cita[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="eliminar_cita[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'cancelar_cita'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="cancelar_cita[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="cancelar_cita[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'editar_orden'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="editar_orden[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="editar_orden[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'editar_operacion'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="editar_operacion[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="editar_operacion[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'editar_hora_operacion'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="editar_hora_operacion[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="editar_hora_operacion[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'eliminar_operacion'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="eliminar_operacion[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="eliminar_operacion[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'no_aplica_asignacion_operacion'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="no_aplica_asignacion_operacion[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="no_aplica_asignacion_operacion[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'Anexar_factura'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="Anexar_factura[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="Anexar_factura[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'Carryover_reservacion'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="Carryover_reservacion[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="Carryover_reservacion[{{$usuario->adminId}}]">
            @endif
          </td>
          <td>
            @if($this->mp->tienePermisoAccion($usuario->adminId,'cancelar_orden'))
            <input type="checkbox" checked="" class="check_{{$usuario->adminId}}" name="cancelar_orden[{{$usuario->adminId}}]">
            @else
            <input type="checkbox" class="check_{{$usuario->adminId}}" name="cancelar_orden[{{$usuario->adminId}}]">
            @endif
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </form>
  <div class="row pull-right">
    <div class="col-sm-12">
      <button type="button" class="btn btn-success" id="guardar_permisos">Guardar Permisos</button>
    </div>
  </div>
  <br>
  <br>
</div>
@endsection

@section('scripts')
<script>
  var site_url = "{{site_url()}}";
  $("#guardar_permisos").on('click',function(){
    var url =site_url+"/permisos/updatePermisos/0";
    ajaxJson(url,$("#frm").serialize(),"POST","",function(result){
      if(result){
        ExitoCustom("Permisos actualizados correctamente",function(){
          window.location.reload();
        });
      }else{
        ErrorCustom("Error al actualizar los permisos",function(){
          window.location.reload();
        });
      }
    });
  });
</script>
@endsection
