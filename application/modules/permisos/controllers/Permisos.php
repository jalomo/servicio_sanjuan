<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class Permisos extends MX_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_permisos','mp');
    $this->load->model('citas/m_catalogos','mcat');
    $this->load->helper(array('general','dompdf','correo'));
    $this->load->library('table');
    date_default_timezone_set('America/Mazatlan');

    if(!PermisoModulo()){
       redirect(site_url('citas/ver_citas'));
    }
  }
  public function permisos_accion(){
    $data['usuarios'] = $this->mp->getUser();
    $this->blade->set_data($data)->render('permisos_accion');
  }
  public function permisos_modulo(){
    $data['usuarios'] = $this->mp->getUser();
    $this->blade->set_data($data)->render('permisos_modulo');
  }

  
  public function updatePermisos(){
    $this->db->where('1','1')->delete('permisos_acciones_usuarios');
    foreach ($_POST as $p => $info) {
      foreach ($info as $d => $dato) {
        $info_permisos = array('id_usuario' => $d, 'accion'=> $p);
        $this->db->insert('permisos_acciones_usuarios',$info_permisos);
      }
    }
    echo 1;exit();
  }
  public function updatePermisosModulo(){
    $this->db->where('1','1')->delete('permisos_modulos');
    foreach ($_POST as $p => $info) {
      foreach ($info as $d => $dato) {
        $info_permisos = array('id_usuario' => $d,'controlador'=> $p,'accion'=> $dato);
        $this->db->insert('permisos_modulos',$info_permisos);
      }
    }
    echo 1;exit();
  }
 
  
}
