<?php
//https://techarise.com/import-excel-file-mysql-codeigniter/
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . "/third_party/PHPExcel.php";
class Excel extends MX_Controller {
public function __construct()
{
  parent::__construct();
  $this->load->model('citas/m_catalogos','mcat');
  $this->load->model('m_excel','mexcel');
  date_default_timezone_set('America/Mexico_City');
  $this->load->library('excel');
  $this->load->library('upload');
}
public function index() {
      $data['page'] = 'import';
      $data['title'] = 'Import XLSX | TechArise';
      $this->blade->render('index', $data);
  }
  // import excel data
  public function save() {
      if ($this->input->post('importfile')) {
          $path = './upload_excel/';
          $fetchData = array();
          $random = $this->random(10);
          $config['upload_path'] = $path;
          $config['allowed_types'] = 'xlsx|xls';
          $config['remove_spaces'] = TRUE;
          $this->upload->initialize($config);
          $this->load->library('upload', $config);
          if (!$this->upload->do_upload('userfile')) {
              $error = array('error' => $this->upload->display_errors());
              $path_delete = '';
          } else {
              $data = array('upload_data' => $this->upload->data());
              $path_delete = $data['upload_data']['file_path'].$data['upload_data']['file_name'];
          }
          
          if (!empty($data['upload_data']['file_name'])) {
              $import_xls_file = $data['upload_data']['file_name'];
          } else {
              $import_xls_file = 0;
          }
          $inputFileName = $path . $import_xls_file;
          try {
              $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
              $objReader = PHPExcel_IOFactory::createReader($inputFileType);
              $objPHPExcel = $objReader->load($inputFileName);
          } catch (Exception $e) {
              die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                      . '": ' . $e->getMessage());
          }
          $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
          
          $arrayCount = count($allDataInSheet);
          $flag = 0;
          $createArray = array('vin', 'primer_nombre', 'segundo_nombre', 'apellidos','email','telefono','direccion','direccion2','direccion3','direccion4','ciudad','provincia','cp','estado','asc1');

          $makeArray = array('vin' => 'vin', 'primer_nombre' => 'primer_nombre', 'segundo_nombre' => 'segundo_nombre', 'apellidos' => 'apellidos', 'email' => 'email', 'telefono' => 'telefono', 'direccion' => 'direccion', 'direccion2' => 'direccion2', 'direccion3' => 'direccion3', 'direccion4' => 'direccion4', 'ciudad' => 'ciudad', 'provincia' => 'provincia', 'cp' => 'cp', 'estado' => 'estado', 'asc1' => 'asc1');
          $SheetDataKey = array();
          foreach ($allDataInSheet as $dataInSheet) {
              foreach ($dataInSheet as $key => $value) {
                  if (in_array(trim($value), $createArray)) {
                      $value = preg_replace('/\s+/', '', $value);
                      $SheetDataKey[trim($value)] = $key;
                  } else {
                      
                  }
              }
          }
          $data = array_diff_key($makeArray, $SheetDataKey);
         
          if (empty($data)) {
              $flag = 1;
          }
          //$this->db->delete('asc_campanias');
          if ($flag == 1) {
              for ($i = 2; $i <= $arrayCount; $i++) {
                  
                  $createArray = array('vin', 'primer_nombre', 'segundo_nombre', 'apellidos','direccion','direccion2','direccion3','direccion4','ciudad','provincia','cp','estado','asc1');

                  $vin = $SheetDataKey['vin'];
                  $primer_nombre = $SheetDataKey['primer_nombre'];
                  $segundo_nombre = $SheetDataKey['segundo_nombre'];
                  $apellidos = $SheetDataKey['apellidos'];
                  $email = $SheetDataKey['email'];
                  $telefono = $SheetDataKey['telefono'];
                  $direccion = $SheetDataKey['direccion'];
                  $direccion2 = $SheetDataKey['direccion2'];
                  $direccion3 = $SheetDataKey['direccion3'];
                  $direccion4 = $SheetDataKey['direccion4'];
                  $ciudad = $SheetDataKey['ciudad'];
                  $provincia = $SheetDataKey['provincia'];
                  $cp = $SheetDataKey['cp'];
                  $estado = $SheetDataKey['estado'];
                  $asc1 = $SheetDataKey['asc1'];

                  $vin = filter_var(trim($allDataInSheet[$i][$vin]), FILTER_SANITIZE_STRING);
                  $primer_nombre = filter_var(trim($allDataInSheet[$i][$primer_nombre]), FILTER_SANITIZE_STRING);
                  $segundo_nombre = filter_var(trim($allDataInSheet[$i][$segundo_nombre]), FILTER_SANITIZE_STRING);
                  $apellidos = filter_var(trim($allDataInSheet[$i][$apellidos]), FILTER_SANITIZE_STRING);
                  $email = filter_var(trim($allDataInSheet[$i][$email]), FILTER_SANITIZE_EMAIL);
                  $telefono = filter_var(trim($allDataInSheet[$i][$telefono]), FILTER_SANITIZE_STRING);
                  $direccion = filter_var(trim($allDataInSheet[$i][$direccion]), FILTER_SANITIZE_STRING);
                  $direccion2 = filter_var(trim($allDataInSheet[$i][$direccion2]), FILTER_SANITIZE_STRING);
                  $direccion3 = filter_var(trim($allDataInSheet[$i][$direccion3]), FILTER_SANITIZE_STRING);
                  $direccion4 = filter_var(trim($allDataInSheet[$i][$direccion4]), FILTER_SANITIZE_STRING);
                  $ciudad = filter_var(trim($allDataInSheet[$i][$ciudad]), FILTER_SANITIZE_STRING);
                  $provincia = filter_var(trim($allDataInSheet[$i][$provincia]), FILTER_SANITIZE_STRING);
                  $cp = filter_var(trim($allDataInSheet[$i][$cp]), FILTER_SANITIZE_STRING);
                  $estado = filter_var(trim($allDataInSheet[$i][$estado]), FILTER_SANITIZE_STRING);
                  $asc1 = filter_var(trim($allDataInSheet[$i][$asc1]), FILTER_SANITIZE_STRING);
                  
                if(!$this->mexcel->validarCampania($vin,$asc1) && $vin!='' && $asc1!=''){
                  $fetchData[] = array('vin' => $vin, 'primer_nombre' => $primer_nombre, 'segundo_nombre' => $segundo_nombre, 'apellidos' => $apellidos,'email' => $email,'telefono' => $telefono, 'direccion' => $direccion, 'direccion2' => $direccion2, 'direccion3' => $direccion3, 'direccion4' => $direccion4, 'ciudad' => $ciudad, 'provincia' => $provincia, 'cp' => $cp, 'estado' => $estado, 'asc1' => $asc1,'random'=>$random);
                }
              }              
              $data['vinList'] = $fetchData;
              $this->mexcel->setBatchImport($fetchData);
              $this->mexcel->importData($path_delete,$random);
          } else {
              echo "Please import correct file";
          }
      }
      $this->blade->render('display', $data);
  }
  public function exportar() {
        $data['export_list'] = $this->mexcel->exportList();
        $this->blade->render('export', $data);
  }
  // create xlsx
  public function generateXls() {
    // create file name
    $fileName = 'orden_servicio-'.time().'.xlsx';  
    // load excel library
    $this->load->library('excel');
    $listInfo = $this->mexcel->exportarOrden();
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0);
    // set Header
    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Fecha Ingreso');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Fecha y hora promesa');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Fecha entrega unidad');
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Vehiculo');
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Asesor');  
    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Cliente');  
    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Placas');  
    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Tecnico');  
    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Estatus');
    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Tipo orden');       
    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Situación');       
    // set Row
    $rowCount = 2;
    foreach ($listInfo as $list) {
      $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount,  $list->identificador.'-'.$list->id_cita);
      $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, date_eng2esp_time($list->created_at));
      $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $list->fecha_entrega.' '.$list->hora_entrega);
      $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $list->fecha_entrega_unidad);
      $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $list->vehiculo_modelo);
      $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $list->asesor);
      $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $list->datos_nombres.' '.$list->datos_apellido_paterno.' '.$list->datos_apellido_materno);
      $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $list->vehiculo_placas);
      $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $list->tecnico);
      $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $list->status_tecnico);
      $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $list->estatus_cita);
      $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $list->situacion_intelisis);
      $rowCount++;
    }
    $filename = "orden_servicio_". date("Y-m-d-H-i-s").".csv";
    header('Content-Type: application/vnd.ms-excel'); 
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0'); 
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
    $objWriter->save('php://output'); 

}
    public function prueba(){
      $this->mexcel->deleteDataASC();die();
    }
    function random($num){
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        for ($i = 0; $i < $num; $i++) {
             $string .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $string;
  }
    public function unidades_nuevas() {
    $data['page'] = 'import';
    $data['title'] = 'Import XLSX | TechArise';
    $this->blade->render('unidades_nuevas', $data);
  }
  // import excel data
  public function save_unidades_nuevas() {
      if ($this->input->post('importfile')) {
          $path = './upload_excel/';
          $random = $this->random(10);
          $fetchData = array();
          $config['upload_path'] = $path;
          $config['allowed_types'] = 'xlsx|xls|jpg|png';
          $config['remove_spaces'] = TRUE;
          $this->upload->initialize($config);
          $this->load->library('upload', $config);
          if (!$this->upload->do_upload('userfile')) {
              $error = array('error' => $this->upload->display_errors());
              $path_delete = '';
          } else {
              $data = array('upload_data' => $this->upload->data());
              $path_delete = $data['upload_data']['file_path'].$data['upload_data']['file_name'];
          }
          
          if (!empty($data['upload_data']['file_name'])) {
              $import_xls_file = $data['upload_data']['file_name'];
          } else {
              $import_xls_file = 0;
          }
          $inputFileName = $path . $import_xls_file;
          try {
              $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
              $objReader = PHPExcel_IOFactory::createReader($inputFileType);
              $objPHPExcel = $objReader->load($inputFileName);
          } catch (Exception $e) {
              die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                      . '": ' . $e->getMessage());
          }
          $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
          
          $arrayCount = count($allDataInSheet);
          $flag = 0;
          $createArray = array('serie', 'fechafac', 'cliente', 'contacto','direccion','telefono','telefono2','telefono3','correo','vendedor','tipovta','economico','descripcion','rfc','poblacion','placas');
          
          $makeArray = array('serie'=>'serie', 'fechafac'=>'fechafac', 'cliente'=>'cliente', 'contacto'=>'contacto','direccion'=>'direccion','telefono'=>'telefono','telefono2'=>'telefono2','telefono3'=>'telefono3','correo'=>'correo','vendedor'=>'vendedor','tipovta'=>'tipovta','economico'=>'economico','descripcion'=>'descripcion','rfc'=>'rfc','poblacion'=>'poblacion','placas'=>'placas');

          $SheetDataKey = array();
          foreach ($allDataInSheet as $dataInSheet) {
              foreach ($dataInSheet as $key => $value) {
                  if (in_array(trim($value), $createArray)) {
                      $value = preg_replace('/\s+/', '', $value);
                      $SheetDataKey[trim($value)] = $key;
                  } else {
                      
                  }
              }
          }
          $data = array_diff_key($makeArray, $SheetDataKey);
         
          if (empty($data)) {
              $flag = 1;
          }
          $flag = 1;
          //$this->db->delete('asc_campanias');
          if ($flag == 1) {
              for ($i = 2; $i <= $arrayCount; $i++) {
                  
                  $serie = $SheetDataKey['serie'];
                  $fechafac = $SheetDataKey['fechafac'];
                  $cliente = $SheetDataKey['cliente'];
                  $contacto = $SheetDataKey['contacto'];
                  $direccion = $SheetDataKey['direccion'];
                  $telefono = $SheetDataKey['telefono'];
                  $telefono2 = $SheetDataKey['telefono2'];
                  $telefono3 = $SheetDataKey['telefono3'];
                  $correo = $SheetDataKey['correo'];
                  $vendedor = $SheetDataKey['vendedor'];
                  $tipovta = $SheetDataKey['tipovta'];
                  $economico = $SheetDataKey['economico'];
                  $descripcion = $SheetDataKey['descripcion'];
                  $rfc = $SheetDataKey['rfc'];
                  $poblacion = $SheetDataKey['poblacion'];
                  $placas = $SheetDataKey['placas'];
                  

                  $serie = filter_var(trim($allDataInSheet[$i][$serie]), FILTER_SANITIZE_STRING);
                  $fechafac = filter_var(trim($allDataInSheet[$i][$fechafac]), FILTER_SANITIZE_STRING);
                  $cliente = filter_var(trim($allDataInSheet[$i][$cliente]), FILTER_SANITIZE_STRING);
                  $contacto = filter_var(trim($allDataInSheet[$i][$contacto]), FILTER_SANITIZE_STRING);
                  $direccion = filter_var(trim($allDataInSheet[$i][$direccion]), FILTER_SANITIZE_STRING);
                  $telefono = filter_var(trim($allDataInSheet[$i][$telefono]), FILTER_SANITIZE_STRING);
                  $telefono2 = filter_var(trim($allDataInSheet[$i][$telefono2]), FILTER_SANITIZE_STRING);
                  $telefono3 = filter_var(trim($allDataInSheet[$i][$telefono3]), FILTER_SANITIZE_STRING);
                  $correo = filter_var(trim($allDataInSheet[$i][$correo]), FILTER_SANITIZE_STRING);
                  $vendedor = filter_var(trim($allDataInSheet[$i][$vendedor]), FILTER_SANITIZE_STRING);
                  $tipovta = filter_var(trim($allDataInSheet[$i][$tipovta]), FILTER_SANITIZE_STRING);
                  $economico = filter_var(trim($allDataInSheet[$i][$economico]), FILTER_SANITIZE_STRING);
                  $descripcion = filter_var(trim($allDataInSheet[$i][$descripcion]), FILTER_SANITIZE_STRING);
                  $rfc = filter_var(trim($allDataInSheet[$i][$rfc]), FILTER_SANITIZE_STRING);
                  $poblacion = filter_var(trim($allDataInSheet[$i][$poblacion]), FILTER_SANITIZE_STRING);
                  $placas = filter_var(trim($allDataInSheet[$i][$placas]), FILTER_SANITIZE_STRING);
                  
                  $fetchData[] = array('serie'=>$serie, 'fechafac'=>$fechafac, 'cliente'=>$cliente, 'contacto'=>$contacto,'direccion'=>$direccion,'telefono'=>$telefono,'telefono2'=>$telefono2,'telefono3'=>$telefono3,'correo'=>$correo,'vendedor'=>$vendedor,'tipovta'=>$tipovta,'economico'=>$economico,'descripcion'=>$descripcion,'rfc'=>$rfc,'poblacion'=>$poblacion,'placas'=>$placas,'random'=>$random);
              }             
              $data['unidades'] = $fetchData;
              $this->mexcel->setBatchImport($fetchData);
              $this->mexcel->importDataUnidades($path_delete);
          } else {
              echo "Please import correct file";
          }
      }
      $this->blade->render('display_unidades', $data);
  }
}
