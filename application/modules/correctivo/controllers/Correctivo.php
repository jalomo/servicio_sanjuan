<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Correctivo extends MX_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_correctivo','mc');
    $this->load->model('citas/m_catalogos','mcat');
    $this->load->helper(array('general','dompdf','correo'));
    $this->load->library('table');
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  //modal correctivo
  //Cuando funciona con base a un código
  public function modal_correctivo(){
      if($this->input->is_ajax_request() && $this->session->userdata('id_usuario')=='')
      {
        $this->output->set_status_header('409');
        exit();
      }else if($this->session->userdata('id_usuario')==''){
        redirect('login');
      }

      $idorden = $this->input->get('idorden');

      if($this->input->post()){
         if($this->input->post('idoperacion_save')==0){
            $this->form_validation->set_rules('categoria', 'categoría', 'trim|required');
            $this->form_validation->set_rules('tipo_operacion', 'tipo de operación', 'trim|required');
            $this->form_validation->set_rules('operacion', 'operación', 'trim|required');
            $this->form_validation->set_rules('no_operacion', 'operación', 'trim');
            $this->form_validation->set_rules('modelo', 'modelo', 'trim|required');
            $this->form_validation->set_rules('anio', 'año', 'trim|required');
         }
          $this->form_validation->set_rules('descripcion_item', 'descripción', 'trim|required');
          $this->form_validation->set_rules('precio_item', 'precio', 'trim|numeric|required');
          $this->form_validation->set_rules('cantidad_item', 'cantidad', 'trim|numeric|required');
          $this->form_validation->set_rules('descuento_item', 'descuento', 'trim|numeric');
          $this->form_validation->set_rules('total_horas', 'total horas', 'trim|numeric');
          $this->form_validation->set_rules('costo_hora', 'costo hora', 'trim|numeric');

         
          if ($this->form_validation->run()){

            $datos_mantenimiento = $this->mc->getManteminientoCorrectivo();
            $datos_item_orden = array('descripcion' => $this->input->post('descripcion_item'),
                                      'idorden' => $this->input->post('idorden'),
                                      'precio_unitario' => $this->input->post('precio_item'),
                                      'cantidad' => $this->input->post('cantidad_item'),
                                      'descuento' => $this->input->post('descuento_item'),
                                      'total' => $this->input->post('total_item'),
                                      'id_usuario' => $this->session->userdata('id_usuario'),
                                      'created_at' => date('Y-m-d H:i:s'),
                                      'temporal' => $this->input->post('temporal_save'),
                                      'tipo'=>$this->input->post('tipo'), //particular (no es de grupos)
                                      'total_horas' => $this->input->post('total_horas'),
                                      'costo_hora' => $this->input->post('costo_hora'),

            );
            if($this->input->post('idoperacion_save')==0){
                $datos_item_orden['grupo'] = $this->input->post('grupo_operacion');
                $datos_item_orden['tipo_operacion'] = $this->input->post('tipo_operacion_save');
                $datos_item_orden['info'] = 'modelo: '.$_POST['modelo'].', año: '.$_POST['anio'].',tipo operación: '.$_POST['tipo_operacion'].', categoría: '.$_POST['categoria'].', operación: '.$_POST['operacion'];

              if(isset($_POST['relacionar_op'])){
                $datos_item_orden['cotizacion_extra'] = 1;
              }
              $exito = $this->db->insert('articulos_orden',$datos_item_orden);
              $idarticulo = $this->db->insert_id();
              //Si se van a relacionar las operaciones
              if(isset($_POST['relacionar_op'])){
               
                //Asignar la operación a la mano de obra
                if(isset($_POST['opext'])){
                  foreach ($_POST['opext'] as $o => $op) {
                    $this->db->where('id',$o)->set('id_operacion',$idarticulo)->update('presupuesto_multipunto');
                  }
                }
              }
            }else{
              $datos_item_orden['id_usuario_edito'] = $this->session->userdata('id_usuario');
              $exito = $this->db->where('id',$this->input->post('idoperacion_save'))->update('articulos_orden',$datos_item_orden);
              $idarticulo = $this->input->post('idoperacion_save');

              //Si id_status_op != 1 y != null o != '' quiere decir que todavia puede editar de lo contrario es que ya se hizo un cambio de estatus
              if($_POST['id_status_op']==''||$_POST['id_status_op']==null||$_POST['id_status_op']==1){
                 $this->db->where('id_cita',$_POST['id_cita'])->set('id_operacion',null)->where('id_operacion',$idarticulo)->update('presupuesto_multipunto');
                if(isset($_POST['opext'])){
                  foreach ($_POST['opext'] as $o => $op) {
                    $this->db->where('id',$o)->set('id_operacion',$idarticulo)->update('presupuesto_multipunto');
                  }
                }
              }
            }

            if($exito){
              echo 1;
            }else{
              echo 0;
            }
            exit();

          }else{
            $errors = array(
              'descripcion_item' => form_error('descripcion_item'),
              'precio_item' => form_error('precio_item'),
              'cantidad_item' => form_error('cantidad_item'),
              'descuento_item' => form_error('descuento_item'),
              'costo_hora' => form_error('costo_hora'),
              'categoria' => form_error('categoria'),
              'operacion' => form_error('operacion'),
              'no_operacion' => form_error('no_operacion'),
              'tipo_operacion' => form_error('tipo_operacion'),
              'modelo' => form_error('modelo'),
              'anio' => form_error('anio'),

            );
            echo json_encode($errors); exit();
          }
        }

      $id = $_GET['idoperacion'];
      if($id==0){
        $info=new Stdclass();
        $valor_cantidad = 1;
        $data['cotizacion_extra'] = 0;  // Si es 1 se hizo con la relación a las partes
        $data['id_status'] = null;
      }else{
        $info= $this->mc->getOperacionId($id);
        $valor_cantidad = $info->cantidad;
        $data['cotizacion_extra'] = $info->cotizacion_extra;  // Si es 1 se hizo con la relación a las partes
        $data['id_status'] = $info->id_status;
      }
    $array_seleccion = array(''=>'-- Selecciona --');

    $data['drop_categorias'] = form_dropdown('categoria',$array_seleccion,"",'class="form-control" id="correctivo_categoria" ');

    $data['drop_operaciones'] = form_dropdown('operacion',$array_seleccion,"",'class="form-control" id="operacion" ');

    //array_combos($this->mcat->get('correctivo_tipo_operacion','tipo_operacion'),'id','tipo_operacion',TRUE)
    $data['drop_tipo_operacion'] = form_dropdown('tipo_operacion',$array_seleccion,"",'class="form-control" id="tipo_operacion" ');

    $data['drop_no_operacion'] = form_dropdown('no_operacion',$array_seleccion,"",'class="form-control" id="no_operacion" ');

    $data['drop_modelo'] = form_dropdown('modelo',array_combos($this->mcat->get('correctivo_cat_modelo','modelo'),'id','modelo',TRUE),"",'class="form-control" id="modelo" ');

    //$anios = $this->db->limit(20)->order_by('anio','desc')->get('cat_anios')->result();
    $data['drop_cat_anios'] = form_dropdown('anio',$array_seleccion,"",'class="form-control" id="anio" ');


   $data['input_descripcion'] = form_input('descripcion_item',set_value('descripcion_item',exist_obj($info,'descripcion')),'class="form-control" id="descripcion_item" ');

    $data['input_precio'] = form_input('precio_item',set_value('precio',exist_obj($info,'precio_unitario')),'class="form-control contar_item cantidad" id="precio_item" ');

    $data['input_cantidad'] = form_input('cantidad_item',set_value('cantidad',$valor_cantidad),'class="form-control contar_item cantidad" id="cantidad_item" ');

    $data['input_descuento'] = form_input('descuento_item',set_value('descuento',exist_obj($info,'descuento')),'class="form-control contar_item cantidad" id="descuento_item" maxlength="2" ');

     $data['input_total_horas'] = form_input('total_horas',set_value('total_horas',exist_obj($info,'total_horas')),'class="form-control contar_item" id="total_horas" maxlength="6" ');

    $data['input_total'] = form_input('total_item',set_value('total',exist_obj($info,'total')),'class="form-control cantidad contar_item" id="total_item" readonly ');

    $data['input_costo_hora'] = form_input('costo_hora',CONST_COSTO_MANO_OBRA,'class="form-control contar_item" id="costo_hora" ');
    
  
    $data['idorden'] = $idorden;
    $data['id_cita'] = $this->mc->getIdCitaByOrden($idorden);
    $data['operaciones_extras'] = $this->mc->operacionesExtras($data['id_cita'],$_GET['idoperacion']);
    
    $data['temporal_save'] = $this->input->get('temporal');
    $data['idoperacion_save'] = $_GET['idoperacion'];
    $data['tipo'] = 4; //corectivo
    $this->blade->render('modal_correctivo',$data);
  }
  public function getAllCorrectivo(){
    if($this->input->post()){
      $this->db->where('idmodelo',$this->input->post('modelo'));
      $this->db->where('anio',$this->input->post('anio'));
      $this->db->where('idoperacion',$this->input->post('operacion'));
      $this->db->where('idcategoria',$this->input->post('categoria'));
      $this->db->where('id_tipo_operacion',$this->input->post('tipo_operacion'));
      $informacion_correctivo = $this->db->limit(1)->get('v_mantenimiento_correctivo')->result();
      if(count($informacion_correctivo)>0){
        $registros = true;
        $informacion_correctivo = $informacion_correctivo[0];
      }else{
        $registros = false;
        $informacion_correctivo = array();
      }
      echo json_encode(array('registros'=>$registros,'informacion_correctivo'=>$informacion_correctivo));
    }else{
    echo 'Nada';
    }
  }
public function getAniosCorrectivo(){
  if($this->input->post()){
      $anios = $this->db->select('distinct(anio)')->order_by('anio','desc')->where('idmodelo',$this->input->post('modelo'))->get('correctivo')->result();
      echo json_encode($anios);
    }else{
    echo 'Nada';
    }
}

public function getTipoOperacionCorrectivo(){
  if($this->input->post()){
      $tipo_operacion = $this->db->select('distinct(cto.id), cto.tipo_operacion')->join('correctivo_tipo_operacion cto','c.id_tipo_operacion = cto.id')->where('c.anio',$this->input->post('anio'))->where('idmodelo',$this->input->post('modelo'))->get('correctivo c')->result();
      echo json_encode($tipo_operacion);
    }else{
    echo 'Nada';
    }
}
public function getCategoriasCorrectivo(){
  if($this->input->post()){
      $categorias = $this->db->select('distinct(cc.id), cc.categoria')->join('correctivo_categoria cc','c.idcategoria = cc.id')->where('c.anio',$this->input->post('anio'))->where('idmodelo',$this->input->post('modelo'))->where('c.id_tipo_operacion',$this->input->post('tipo_operacion'))->get('correctivo c')->result();
      echo json_encode($categorias);
    }else{
    echo 'Nada';
    }
}
public function getOperacionCorrectivo(){
    if($this->input->post()){
      $operaciones = $this->db->select('distinct(co.id), co.operacion')->join('correctivo_operaciones co','c.idoperacion = co.id')->where('c.idcategoria',$this->input->post('categoria'))->where('anio',$this->input->post('anio'))->where('idmodelo',$this->input->post('modelo'))->where('c.id_tipo_operacion',$this->input->post('tipo_operacion'))->get('correctivo c')->result();
      echo json_encode($operaciones);
    }else{
    echo 'Nada';
    }
  }
public function getNoOperacionCorrectivo(){
    if($this->input->post()){
      $operaciones = $this->db->select('distinct(co.id), co.operacion')->join('correctivo_no_operacion co','c.id_no_operacion = co.id')->where('c.idcategoria',$this->input->post('categoria'))->where('c.idsubcategoria',$this->input->post('subcategoria'))->where('anio',$this->input->post('anio'))->where('idmodelo',$this->input->post('modelo'))->where('idoperacion',$this->input->post('operacion'))->where('id_mano_obra',$this->input->post('mano_obra'))->get('correctivo c')->result();

      echo json_encode($operaciones);
    }else{
    echo 'Nada';
    }
  }
public function getSubcategoriasCorrectivo(){
    if($this->input->post()){
      $subcategorias = $this->db->select('cs.id,cs.subcategoria')->where('cs.idcategoria',$this->input->post('idcategoria'))->join('correctivo c','cs.id=c.idsubcategoria')->where('c.anio',$this->input->post('anio'))->where('idmodelo',$this->input->post('modelo'))->get('correctivo_subcategorias cs')->result();
      echo json_encode($subcategorias);
    }else{
    echo 'Nada';
    }
}
public function getModeloByNombre(){
    $q = $this->db->where('modelo',$_POST['modelo'])->get('cat_modelo');
    if($q->num_rows()==1){
      echo json_encode(array('exito'=>true,'data'=>$q->row()));
    }else{
      echo json_encode(array('exito'=>false));
    }
}

 
  
}
