<style type="text/css">
	.operaciones label{
		margin-right: 10px;
	}
	.operaciones{
		margin-right: 5px;
	}
	.tbl-operaciones th{
		text-align: center;
	}
	.tbl-operaciones th input{
		margin-bottom: 5px;
	}
	.mostrar{
		visibility: visible;
	}
	.ocultar{
		display: none;
	}
</style>
<form action="" id="frm-correctivo">
<input type="hidden" id="idorden" name="idorden" value="{{$idorden}}">
<input type="hidden" id="id_cita" name="id_cita" value="{{$id_cita}}">
<input type="hidden" id="tipo" name="tipo" value="{{$tipo}}">
<input type="hidden" id="grupo_operacion" name="grupo_operacion" value="">
<input type="hidden" id="temporal_save" name="temporal_save" value="{{$temporal_save}}">
<input type="hidden" id="idoperacion_save" name="idoperacion_save" value="{{$idoperacion_save}}">
<input type="hidden" id="id_status_op" name="id_status_op" value="{{$id_status}}">
<input type="hidden" id="tipo_operacion_save" name="tipo_operacion_save" value="">
@if(($cotizacion_extra||$idoperacion_save==0))
	<?php $disabled = 'disabled'; ?>
	@if($id_status==null || $id_status==1)
		<?php $disabled = ''; ?>
	@endif
@endif
<?php $disabled_o_e = ''; ?>
@if($idoperacion_save!=0)
	<?php $disabled_o_e = 'disabled'; ?>
@endif
@if($idorden!=0 && ($cotizacion_extra||!$idoperacion_save))
<label for="relacionar_op">¿Relacionar mano de obra con operaciones adicionales?</label> <input type="checkbox" name="relacionar_op" id="relacionar_op" {{$disabled}} {{$disabled_o_e}} {{($cotizacion_extra==1)?'checked':''}}>
@endif

@if($idoperacion_save!=0)
<div class="row">
	<div class="col-sm-9 {{($cotizacion_extra==1)?'mostrar':'ocultar'}} " id="div_operaciones">
		<label class="text-center" for="">Operaciones extras</label> <br>
		<table class="table tbl-operaciones">
			<tr>
			@if(count($operaciones_extras)>0)
				@foreach($operaciones_extras as $o => $value)
					<th>
						<?php $check_op = ''; ?>
					@if($value->id_operacion==''||$value->id_operacion==0)
						<?php $check_op = ''; ?>
					@else
						<?php $check_op = 'checked'; ?>
					@endif
					<input class="operaciones" type="checkbox" id="opext-{{$value->id}}" name="opext[{{$value->id}}]" {{$check_op}} {{$disabled}} ><label for="opext-{{$value->id}}">{{$o+1}}){{$value->descripcion}}</label> 
					</th>
				@endforeach
			@else
				<th>No existen operaciones extras</th>
			@endif
			</tr>
		</table>
	</div>
</div>
@endif
@if($idoperacion_save==0)
<div class="row">
	<div class="col-sm-12 ocultar" id="div_operaciones">
		<label class="text-center" for="">Operaciones extras</label> <br>
		<table class="table tbl-operaciones">
			<tr>
			@if(count($operaciones_extras)>0)
				@foreach($operaciones_extras as $o => $value)
					<th>
						<input class="operaciones" type="checkbox" id="opext-{{$value->id}}" name="opext[{{$value->id}}]"><label for="opext-{{$value->id}}">{{$o+1}}){{$value->descripcion}}</label> 
					</th>
				@endforeach
			@else
				<th>No existen operaciones extras</th>
			@endif
			</tr>
		</table>
	</div>
</div>
<div class="row">
	<div class="col-sm-3">
		<label for="">Modelo</label>
		{{$drop_modelo}}
		<span class="error error_modelo"></span>
	</div>
	<div class="col-sm-3">
		<label for="">Año</label>
		{{$drop_cat_anios}}
		<span class="error error_anio"></span>
	</div>
	
	<div class="col-sm-3">
		<label for="">Tipo de operación</label>
		{{$drop_tipo_operacion}}
		<span class="error error_tipo_operacion"></span>
	</div>
	<div class="col-sm-3">
		<label for="">Categoría</label>
		{{$drop_categorias}}
		<span class="error error_categoria"></span>
	</div>
</div>
<div class="row">
	<div class="col-sm-3">
		<label for="">Operación</label>
		{{$drop_operaciones}}
		<span class="error error_operacion"></span>
	</div>
</div>
<span id="noregistros" style="color: red;text-align: right;"></span>
<br>
<div id="div_descripciones_totales">
	<span></span>
</div>
<hr>

@else
	<h3 class="text-center">Actualización de precio</h3>
@endif
<br>
<div class="row">
	<div class="col-sm-2">
		<label for="">Cantidad</label>
		{{$input_cantidad}}
		<span class="error error_cantidad_item"></span>
	</div>
	<div class="col-sm-4">
		<label for="">Descripción</label>
		{{$input_descripcion}}
		<span class="error error_descripcion_item"></span>
	</div>
	<div class="col-sm-2">
		<label for="">Precio</label>
		{{$input_precio}}
		<span class="error error_precio_item"></span>
	</div>
	<div class="col-sm-2">
		<label for="">Total horas</label>
		{{$input_total_horas}}
		<span class="error error_total_horas"></span>
	</div>
	<div class="col-sm-2">
		<label for="">Costo hora</label>
		{{$input_costo_hora}}
		<span class="error error_costo_hora"></span>
	</div>
</div>
<div class="row ">
	<div class="col-sm-8"></div>
	<div class="col-sm-2">
		<label for="">Descuento(%)</label>
		{{$input_descuento}}
		<span class="error error_descuento_item"></span>
	</div>
	<div class="col-sm-2">
		<label for="">Total</label>
		{{$input_total}}
		<span class="error error_total_item"></span>
	</div>
</div>
</form>

<script>
	$(".busqueda_all").select2();
	$(".busqueda_all").select2({ width: 'resolve' });
	$(".contar_item").on('change',function(){
		var onChange = $(this).prop('name');
		$.each($(".contar_item"), function(i, item) {
			if(isNaN($(item).val()) || $(item).val()==''){
				if($(item).prop('name')=='total_horas'){
					$(item).val('0');
				}else{
					$(item).val('0');
				}
			}
        });
     	var total_horas = parseFloat($("#total_horas").val());
     	var precio_item = parseFloat($("#precio_item").val());
     	if(onChange=='precio_item'){
			if(precio_item>0){
			  $("#total_horas").val(0)
			}
			
		}
		if(onChange=='total_horas'){
			if(total_horas>0){
				$("#precio_item").val(0)
			}
			
		}
		var total_horas = parseFloat($("#total_horas").val());
		var precio_item = parseFloat($("#precio_item").val());
        var costo_hora = parseFloat($("#costo_hora").val());
        var costo_total_horas = parseFloat(costo_hora)*parseFloat(total_horas);
        var cantidad_item = parseInt($("#cantidad_item").val());
        var descuento_item = parseFloat($("#descuento_item").val())/100;
        if(descuento_item>0){
        	var total = precio_item*cantidad_item;
        	var total = total-(total*descuento_item)
        }else{
        	var total = precio_item*cantidad_item;
        }
        total = total + costo_total_horas;
        var total_item = $("#total_item").val(total.toFixed(2));

	});
	$("#relacionar_op").on('click',function(){
		if($(this).prop('checked')){
			$("#div_operaciones").removeClass('ocultar');
			$("#div_operaciones").addClass('mostrar');
		}else{
			$("#div_operaciones").addClass('ocultar');
			$("#div_operaciones").removeClass('mostrar');
		}
	})
	loadModel();
	//Obtiene Años
	$("#modelo").on("change",function(){
    var url=site_url+"/correctivo/getAniosCorrectivo";
		$("#anio").empty();
		$("#anio").append("<option value=''>-- Selecciona --</option>");
		$("#anio").attr("disabled",true);
		$("#tipo_operacion").empty();
		$("#tipo_operacion").append("<option value='0'>-- Selecciona --</option>");
	    $("#correctivo_categoria").empty();
		$("#correctivo_categoria").append("<option value='0'>-- Selecciona --</option>");
		$("#operacion").empty();
		$("#operacion").append("<option value='0'>-- Selecciona --</option>");
		var modelo = $("#modelo").val();
		if(modelo!=''){
			ajaxJson(url,{"modelo":modelo},"POST","",function(result){
				if(result.length!=0){
					$("#anio").empty();
					$("#anio").removeAttr("disabled");
					result=JSON.parse(result);
					$("#anio").append("<option value=''>-- Selecciona --</option>");
					$.each(result,function(i,item){
						$("#anio").append("<option value= '"+item.anio+"'>"+item.anio+"</option>");
					});
					//Seleccionar el año
					$("#anio").val($("#vehiculo_anio").val());
					if($("#anio").val()!=''){
						$("#anio").trigger('change');
					}
				}else{
					$("#anio").empty();
					$("#anio").append("<option value='0'>No se encontraron datos</option>");
				}
			});
		}else{
			$(".error_categoria").append('El campo modelo es requerido');
			$(".error_modelo").css('color','red');
		}
	});	
	$("#anio").on("change",function(){
    var url=site_url+"/correctivo/getTipoOperacionCorrectivo";
		$("#tipo_operacion").empty();
		$("#tipo_operacion").append("<option value=''>-- Selecciona --</option>");
		$("#tipo_operacion").attr("disabled",true);


	    $("#correctivo_categoria").empty();
		$("#correctivo_categoria").append("<option value='0'>-- Selecciona --</option>");
		$("#operacion").empty();
		$("#operacion").append("<option value='0'>-- Selecciona --</option>");

		var modelo = $("#modelo").val();
		var anio = $("#anio").val();
		if(modelo!='' && anio!=''){
			ajaxJson(url,{"modelo":modelo,"anio":anio},"POST","async",function(result){
				if(result.length!=0){
					$("#tipo_operacion").empty();
					$("#tipo_operacion").removeAttr("disabled");
					result=JSON.parse(result);
					$("#tipo_operacion").append("<option value=''>-- Selecciona --</option>");
					$.each(result,function(i,item){
						$("#tipo_operacion").append("<option value= '"+item.id+"'>"+item.tipo_operacion+"</option>");
					});
				}else{
					$("#tipo_operacion").empty();
					$("#tipo_operacion").append("<option value='0'>No se encontraron datos</option>");
				}
			});
		}else{
			ErrorCustom("Es necesario seleccionar los filtros previos");
		}
	});	
	$("#tipo_operacion").on("change",function(){
		$("#tipo_operacion_save").val($("#tipo_operacion option:selected" ).text());
    var url=site_url+"/correctivo/getCategoriasCorrectivo";
		$("#correctivo_categoria").empty();
		$("#correctivo_categoria").append("<option value=''>-- Selecciona --</option>");
		$("#correctivo_categoria").attr("disabled",true);

		$("#operacion").empty();
		$("#operacion").append("<option value='0'>-- Selecciona --</option>");
		var modelo = $("#modelo").val();
		var anio = $("#anio").val();
		var tipo_operacion = $("#tipo_operacion").val();
		if(modelo!='' && anio!='' && tipo_operacion!=''){
			ajaxJson(url,{"modelo":modelo,"anio":anio,"tipo_operacion":tipo_operacion},"POST","",function(result){
				if(result.length!=0){
					$("#correctivo_categoria").empty();
					$("#correctivo_categoria").removeAttr("disabled");
					result=JSON.parse(result);
					$("#correctivo_categoria").append("<option value=''>-- Selecciona --</option>");
					$.each(result,function(i,item){
						$("#correctivo_categoria").append("<option value= '"+item.id+"'>"+item.categoria+"</option>");
					});
				}else{
					$("#correctivo_categoria").empty();
					$("#correctivo_categoria").append("<option value='0'>No se encontraron datos</option>");
				}
			});
		}else{
			ErrorCustom("Es necesario seleccionar los filtros previos");
		}
	});	
	//Obtiene categorías
	
	//$("#no_operacion").on("change",function(){
	$("#operacion").on("change",function(){
    var url=site_url+"/correctivo/getAllCorrectivo";
		
		var modelo = $("#modelo").val();
		var anio = $("#anio").val();
		var correctivo_categoria = $("#correctivo_categoria").val();
		var tipo_operacion = $("#tipo_operacion").val();
		var operacion = $("#operacion").val();
		var no_operacion = $("#no_operacion").val();

		if(modelo !='' && correctivo_categoria !='' && anio !='' && tipo_operacion !='' && operacion !=''){
			ajaxJson(url,{"modelo":modelo,"anio":anio,"operacion":operacion,"categoria":correctivo_categoria,"tipo_operacion":tipo_operacion,"no_operacion":no_operacion},"POST","",function(result){
				result=JSON.parse(result);
				$("#noregistros").text('');
				if(result.registros==1){
					$("#div_descripciones_totales span").empty().append('Descripción: '+result.informacion_correctivo.comentarios);
					$("#grupo_operacion").val(result.informacion_correctivo.codigo_operacion_ereact)

				}else{
					// $("#noregistros").text('* no se encontraron coincidencias');
					// $("#precio_item").val('');
					// $("#cantidad_item").val('');
					// $("#precio_item").val('');
					$("#noregistros").text('* no se encontraron coincidencias');
					$("#div_descripciones_totales span").empty()
					//$("#descripcion_item").val('');
				}
			});
		}else{
			ErrorCustom("Es necesario aplicar todos los filtros");
		}
	});	
	$("#correctivo_categoria").on("change",function(){
    var url=site_url+"/correctivo/getOperacionCorrectivo";
		$("#operacion").empty();
		$("#operacion").append("<option value=''>-- Selecciona --</option>");
		$("#operacion").attr("disabled",true);
		var modelo = $("#modelo").val();
		var anio = $("#anio").val();
		var correctivo_categoria = $("#correctivo_categoria").val();
		var tipo_operacion = $("#tipo_operacion").val();
		if(modelo !='' && correctivo_categoria !='' && anio !='' && correctivo_categoria!='' && tipo_operacion!=''){
			ajaxJson(url,{"modelo":modelo,"anio":anio,"categoria":correctivo_categoria,"tipo_operacion":tipo_operacion},"POST","",function(result){
				if(result.length!=0){
					$("#operacion").empty();
					$("#operacion").removeAttr("disabled");
					result=JSON.parse(result);
					$("#operacion").append("<option value=''>-- Selecciona --</option>");
					$.each(result,function(i,item){
						$("#operacion").append("<option value= '"+item.id+"'>"+item.operacion+"</option>");
					});
				}else{
					$("#operacion").empty();
					$("#operacion").append("<option value='0'>No se encontraron datos</option>");
				}
			});
		}else{
			ErrorCustom("Es necesario aplicar todos los filtros previos");
		}
	});	

	function loadModel(){
		ajaxJson(site_url+"/correctivo/getModeloByNombre",{"modelo":$("#vehiculo_modelo").val()},"POST","async",function(result){
			result=JSON.parse(result);
				if(result.exito){
					$("#modelo").val(result.data.modelo_correctivo_id);
					$("#modelo").trigger('change');
				}
			});
	}


	
</script>