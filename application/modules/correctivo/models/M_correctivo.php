<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_correctivo extends CI_Model{
  public function __construct(){
    parent::__construct();
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  //Datos del mantenimiento correctivo
  public function getManteminientoCorrectivo(){
    $this->db->where('idmodelo',$this->input->post('modelo'));
    $this->db->where('anio',$this->input->post('anio'));
    $this->db->where('idoperacion',$this->input->post('operacion'));
    $this->db->where('idcategoria',$this->input->post('categoria'));
    $this->db->where('id_no_operacion',$this->input->post('no_operacion'));
    $this->db->where('id_tipo_operacion',$this->input->post('id_tipo_operacion'));
    return $this->db->get('v_mantenimiento_correctivo')->row();
  }
   //obtener el operador en base a su id
  public function getOperacionId($id=0){
    $q = $this->db->where('id',$id)->get('articulos_orden');
    if($q->num_rows()==1){
      return $q->row();
    }else{
      return false;
    }
  }
   //FIN PROACTIVO ITELISIS
  public function getIdCitaByOrden($idorden=''){
    $q = $this->db->where('id',$idorden)->select('id_cita')->from('ordenservicio')->get();
    if($q->num_rows()==1){
      return $q->row()->id_cita;
    }else{
      return '';
    }
  }
  //Obtener operaciones extras
  public function operacionesExtras($id_cita='',$id_operacion=0){
    //Si viene la operación es que están editando y deben salir todas de lo contrario sólo deben salir las NO asignadas
    if($id_operacion==0){
      $this->db->where('id_operacion',0);
    }else{
      $this->db->where('id_operacion',$id_operacion);
      $this->db->or_where('id_operacion',0);
    }
    return $this->db->where('id_cita',$id_cita)->where('autoriza_cliente',1)->where('pza_garantia',0)->where('activo',1)->get('presupuesto_multipunto')->result();
  }
  
}
