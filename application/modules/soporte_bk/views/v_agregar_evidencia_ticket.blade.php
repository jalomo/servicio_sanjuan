<form id="frm">
    <input type="hidden" name="ticket" id="ticket" value="{{ $ticket }}">
    <div class="row">
        <div class="col-sm-4">
            <label for="">Evidencia</label>
            <input type="file" class="form-control-file" id="evidencia" name="evidencia">
            <div id="evidencia_error" class="invalid-feedback"></div>
        </div>
    </div>
</form>