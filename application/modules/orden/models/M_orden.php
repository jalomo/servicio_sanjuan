<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_orden extends CI_Model{
  public function __construct(){
    parent::__construct();
    date_default_timezone_set(CONST_ZONA_HORARIA);
  }
  public function getOrderHistory($total=false,$buscar_id=0){
    $datos = $_POST;
    $pagina = $datos['start'];
    $porpagina = $datos['length'];

    if($_POST['id_asesor']!=''){
      $this->db->where('c.asesor',$_POST['id_asesor']);
    }
    
    if($_POST['id_status_search']!=''){
      $this->db->where('o.id_status_intelisis',$_POST['id_status_search']);
    }
    if($_POST['id_situacion_intelisis_search']!=''){
      $this->db->where_in('o.id_situacion_intelisis',implode(',',$this->input->post('id_situacion_intelisis_search')),FALSE);
    }

    if($_POST['cita_previa']!=''){
      $this->db->where('c.cita_previa',$_POST['cita_previa']);
    }
    if($this->input->post('id_status_color')){
      $this->db->where_in('c.id_status_color',implode(',',$this->input->post('id_status_color')),FALSE);
    } 
    if($this->input->post('id_status_tecnico')){
      $this->db->where_in('c.id_status',implode(',',$this->input->post('id_status_tecnico')),FALSE);
    }
    $this->db->join('citas c','c.id_cita=o.id_cita');

    if($_POST['filtro_cierre_orden']){
      if($_POST['finicio']!=''){
        $this->db->where('date(o.cierre_orden) >=',date2sql($_POST['finicio']));
      }
      if($_POST['ffin']!=''){
        $this->db->where('date(o.cierre_orden) <=',date2sql($_POST['ffin']));
      }
    }else{
      if($_POST['finicio']!='' || $_POST['ffin']!=''){
        $this->db->join('tecnicos_citas tc','c.id_cita=tc.id_cita');
        $this->db->where('tc.dia_completo',0);
      }
  
      if($_POST['finicio']!=''){
        $this->db->where('tc.fecha >=',date2sql($_POST['finicio']));
      }
      if($_POST['ffin']!=''){
        $this->db->where('tc.fecha <=',date2sql($_POST['ffin']));
      }
    }
    

    //$this->db->where('id_status_intelisis !=',4);
    if($buscar_id){
      if($_POST['buscar_campo']!=''){
         $this->db->where('o.id_cita',$_POST['buscar_campo']);
      }
    }else{
        if($_POST['buscar_campo']!=''){
          $busqueda = $_POST['buscar_campo'];
          $this->db->or_like('o.id_cita',$busqueda);
          $this->db->or_like('c.orden_magic',$busqueda);
          $this->db->or_like('c.vehiculo_numero_serie',$busqueda);
          $this->db->or_like('c.vehiculo_placas',$busqueda);
          $this->db->or_like('c.datos_nombres',$busqueda);
          $this->db->or_like('c.datos_apellido_paterno',$busqueda);
          $this->db->or_like('c.datos_apellido_materno',$busqueda);
        }
    }
    
    if($total){
      $this->db->select('count(o.id) as total');
    }else{
      $this->db->select('o.*,c.*, csio.status as estatus_intelisis, csit.situacion as situacion_intelisis, cso.status as estatus_orden,cso.id as id_estatus_orden,cto.identificador,csc.status as status_tecnico,e.nombre as estatus_cita');
      $this->db->limit($porpagina,$pagina);
    }
    $info= $this->db->join('cat_status_intelisis_orden csio','o.id_status_intelisis = csio.id','left')
                              ->join('cat_situacion_intelisis csit','o.id_situacion_intelisis = csit.id','left')
                              ->join('cat_status_orden cso','cso.id = o.id_status','left')
                              ->join('cat_tipo_orden cto','o.id_tipo_orden=cto.id','left')
                              ->join('cat_status_citas_tecnicos csc','c.id_status=csc.id')
                              ->join('estatus e','c.id_status_color=e.id')
                              ->order_by('o.id','desc')
                              ->where('o.cancelada',0)
                              ->get('ordenservicio o');
                              
    if($total){
      return $info->row()->total;
    }else{
      return $info->result();
    }                         
  }
  public function getMessageSend(){
    $datos = $this->db->get('mensajes_enviados_orden')->result();
    $regreso = array();
    foreach ($datos as $key => $dato) {
        //$keyname = $dato->idorden.'-'.$dato->tipo_mensaje.'-'.$dato->enviado;
        // $regreso->$keyname = $dato->enviado;
      $regreso[] = $dato->idorden.'-'.$dato->tipo_mensaje.'-'.$dato->enviado;
    }
    return $regreso;
  }
  public function cotizacion_multipunto_status($idorden=''){
      $q = $this->db->select('aceptoTermino')->where('idOrden',$idorden)->get('cotizacion_multipunto');
      if($q->num_rows()==1){
        return $q->row()->aceptoTermino;
      }else{
        return '';
      }
  }
  public function saveComentario_ordenServicio(){
     $id= $this->input->post('id');
     $datos = array('comentario' => $this->input->post('comentario'),
                    'idorden' => $this->input->post('idorden'),
                    'fecha_creacion' =>date('Y-m-d H:i:s'),
                    'id_usuario'=>$this->session->userdata('id_usuario'),
      );
        $this->db->insert('historial_comentarios_orden_servicio',$datos);

      echo 1;exit();
  }
  public function getComentarios_orden_servicio($idorden=''){
      return $this->db->where('idorden',$idorden)->get('historial_comentarios_orden_servicio')->result();
  }
  public function getDataOrdenId($id=''){
    return $this->db->where('id',$id)->get('ordenservicio')->row();
  }
  public function upload($myfile='',$formato=''){
        $result = array();
        if(isset($_FILES[$myfile])){
            //var_dump($formato);die();

            ini_set('max_file_uploads', '600');
            ini_set('post_max_size', '50M');
            ini_set('upload_max_filesize', '50M');

            $config['upload_path'] = './assets/facturas/';
            $config['allowed_types'] = $formato;
            //var_dump($config['allowed_types']);die();
            $config['max_size'] = '21200';
            $config['encrypt_name']  = true;
             
            $this->load->library('Upload2', $config);
            
            if ( ! $this->upload2->do_upload('file')){
                $result[0] = array('isok'=>false,'client_name'=>'','error' => $this->upload2->display_errors());
            }else{
                $ret = array();
                $contadorFILES=0;
                $dataTMPARR = $this->upload2->data();   
                foreach($dataTMPARR as $indice => $dataTMP){

                     
                    $result[$contadorFILES]['isok'] = $dataTMP['isok'];
                    if($dataTMP['isok']){                                              
                        $result[$contadorFILES]['client_name'] = $dataTMP['client_name'];
                        $result[$contadorFILES]['file_name'] = $dataTMP['file_name']; 
                        $result[$contadorFILES]['path'] = $dataTMP['full_path'];                       
                    }else{

                        $result[$contadorFILES]['client_name'] = $dataTMP['file_name'];
                        $result[$contadorFILES]['error'] = $this->upload2->display_errors($indice);
                    }
                    $contadorFILES++;
                } 
            }
        }
        return $result;

  }
  function validarLogin(){
    if($this->input->post('usuario')==CONST_USER_FACTURACION && $this->input->post('password') ==CONST_PASS_FACTURACION){
      echo 1;
    }else{
      echo 0;
    }
    exit();
  }

 
}
