<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Orden extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('citas/m_catalogos', 'mcat');
    $this->load->model('orden/m_orden', 'mo');
    $this->load->model('citas/m_citas', 'mc');
    $this->load->helper(array('correo', 'notificaciones', 'correos_usuario'));
    $this->load->model('layout/m_layout', 'ml'); //NUEVA VERSION
    $this->load->model('principal');
    date_default_timezone_set(CONST_ZONA_HORARIA);
  } //
  public function historial_orden_servicio()
  {
    if ($this->input->is_ajax_request() && $this->session->userdata('id_usuario') == '') {
      $this->output->set_status_header('409');
      exit();
    } else if ($this->session->userdata('id_usuario') == '') {
      redirect('login');
    }

    $data['drop_asesores'] = form_dropdown('id_asesor', array_combos($this->mcat->get('operadores'), 'nombre', 'nombre', true), "", 'class="form-control busqueda" id="id_asesor" ');

    $data['drop_estatus'] = form_dropdown('id_status_search', array_combos($this->mcat->get('cat_status_intelisis_orden'), 'id', 'status', true), "", 'class="form-control busqueda" id="id_status_search" ');

    $data['drop_situaciones'] = form_dropdown('id_situacion_intelisis_search[]', array_combos($this->mcat->get('cat_situacion_intelisis'), 'id', 'situacion'), "", 'class="form-control multiple" multiple="multiple" id="id_situacion_intelisis_search" ');

    $data['drop_estatus_tecnicos'] = form_dropdown('id_status_tecnico[]', array_combos($this->mcat->get('cat_status_citas_tecnicos', 'status'), 'id', 'status', TRUE, FALSE, FALSE), "", 'class="form-control multiple" multiple="multiple" id="id_status_tecnico"');

    $data['drop_estatus_cita'] = form_dropdown('id_status_color[]', array_combos($this->mcat->get('estatus', 'nombre'), 'id', 'nombre'), "", 'class="form-control multiple" multiple="multiple" id="id_status_color"');
    $cita_previa = array('' => '-- Selecciona --', '0' => 'No', '1' => 'Si');
    $data['drop_tipo_cita'] = form_dropdown('cita_previa', $cita_previa, "", 'class="form-control" id="cita_previa"');
    $this->blade->render('historial_orden_servicio_ajax', $data);
  }
  public function getDatosOrdenAjax()
  {
    $mensajes_enviados = $this->mo->getMessageSend();

    $datos = $_POST;
    $mismo = $datos['draw'];
    $pagina = $datos['start'];
    $porpagina = $datos['length'];
    $total = $this->mo->getOrderHistory(true, $_POST['buscar_id']);
    $info = $this->mo->getOrderHistory(false, $_POST['buscar_id']);
    $data = array();
    foreach ($info as $key => $value) {
      $dat = array();
      if ($this->ml->validarTodasOperacionesAsignadas($value->id)) {
        $clase_operaciones = 'bg-verde';
      } else {
        $clase_operaciones = 'bg-red';
      }
      $operaciones_extras = $this->principal->operacionesExtrasByCita($value->id_cita);

      if (count($operaciones_extras) > 0) {
        $clase_operaciones = 'bg-red';
      }

      $folio_asociado = '';
      if ($value->id_tipo_orden == 2 && $value->folio_asociado != '') {
        $folio_asociado = '-' . $value->folio_asociado;
      }

      $dat[] = '<span class="' . $clase_operaciones . '">' . $value->identificador . '-' . $value->id_cita . $folio_asociado . '</span>';
      $dat[] = $value->vehiculo_placas;
      $dat[] = $value->vehiculo_numero_serie;
      $dat[] = date_eng2esp_1($value->fecha_recepcion);
      $dat[] = ($value->fecha_entrega . ' ' . $value->hora_entrega);
      $dat[] = ($value->fecha_entrega_unidad);
      $dat[] = ($value->estatus_cita);
      $dat[] = $value->datos_nombres . ' ' . $value->datos_apellido_paterno . ' ' . $value->datos_apellido_materno;
      $dat[] = $value->status_tecnico;
      if ($value->cita_previa == 1) {
        $dat[] = 'Si';
      } else {
        if ($value->id_status == 9 || $value->id_status == 11) {
          $clase = ($value->unidad_entregada == 1) ? 'cverde' : 'cgris';
          $dat[] = '<a href="" class="js_cambiar_status ' . $clase . '"  aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Unidad entregada" data-status="5" data-unidadentregada="' . $value->unidad_entregada . '" data-id_cita="' . $value->id_cita . '"><i class="pe pe-7s-car"></i></a> <br>No';
        } else {
          $dat[] = 'No';
        }
      }
      //ORDEN PARTE 1
      $orden_parte_1 = '';
      if (!$value->unidad_entregada && $value->id_status_intelisis != 4 && $value->id_situacion_intelisis != 6 && PermisoAccion('editar_orden')) {
        $orden_parte_1 .= '<a href="' . base_url("citas/cita_servicio/" . $value->id) . '" data-id="' . $value->id . '" class="" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar"><i class="pe pe-7s-note"></i></a>';
      }
      if ($value->id_tipo_orden == 1 && $value->id_situacion_intelisis != 5 && $value->id_situacion_intelisis != 6 && $value->id_status_intelisis == 2) {
        $orden_parte_1 .= '<a href="#" data-id="' . $value->id_cita . '" class=" js_asociar_orden" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Asociar orden"><i class="pe pe-7s-left-arrow"></i></a>';
      }

      if ($value->status_gerente == 1) {
        $color_gerente_co = '#28a745';
        $clase_gerente_co = '';
      } elseif ($value->status_gerente == 2) {
        $color_gerente_co = '#dc3545';
        $clase_gerente_co = '';
      } else {
        $color_gerente_co = '#000';
        $clase_gerente_co = 'solicitar_gerencia';
      }

      $orden_parte_1 .= '<a  data-id="' . $value->id . '" data-id_cita="' . $value->id_cita . '" data-orden_magic="' . $value->orden_magic . '" class=" ' . $clase_gerente_co . '" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Solicitar a gerencia C.O." style="color: ' . $color_gerente_co . ' !important;cursor: pointer;"><i class="pe pe-7s-check"></i></a>';
      $nombre_cliente = $value->datos_nombres . ' ' . $value->datos_apellido_paterno . ' ' . $value->datos_apellido_materno;
      $orden_parte_1 .= '<a target="_blank" href="https://api.whatsapp.com/send?phone=+52' . $value->telefono_movil . '&text=¡Estimado%20cliente!%20' . $nombre_cliente . '%20Bienvenido%20a%20su%20distribuidor%20FORD%20MYLSA%20QUERÉTARO%20Orden%20No.%20' . $value->id_cita . '"><i class="fa fa-whatsapp"></i></a>';
      if ($value->id_situacion_intelisis == 5) {
        $orden_parte_1 .= '<a href="' . base_url('citas/exportar_remision/' . $value->id) . '" data-id="' . $value->id . '" class="" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Exportar remisión"><i class="pe pe-7s-file"></i></a>';
      }
      $orden_parte_1 .= '<a href="' . base_url('citas/cotizacion_individual/' . $value->id) . '" data-id="' . $value->id . '" class="" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" target="_blank" title="Generar cotización"><i class="pe pe-7s-shopbag"></i></a>';

      if (PermisoAccion('cancelar_orden', true)) {
        $orden_parte_1 .= '<a href="" data-id="' . $value->id . '" data-id_cita="' . $value->id_cita . '" class=" js_cancelar" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Cancelar orden"><i class="pe pe-7s-close-circle"></i></a>';
      }

      if (in_array($value->id . '-email-1', $mensajes_enviados)) {
        if ($value->cotizacion_confirmada == 1) {
          $color_cotizacion = '#28a745';
        } elseif ($value->cotizacion_confirmada == 2) {
          $color_cotizacion = '#dc3545';
        } else {
          $color_cotizacion = '#31B9CE';
        }
      } else {
        $color_cotizacion = '#000';
      }

      $orden_parte_1 .= '<a href="#" data-id="' . $value->id . '" class=" enviar_cotizacion" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Enviar cotización" style="color: ' . $color_cotizacion . ' !important" target="_blank"><i class="pe pe-7s-mail"></i></a>';

      if (in_array($value->id . '-sms-1', $mensajes_enviados)) {
        $color_sms = '#31B9CE';
      } else {
        $color_sms = '#000';
      }

      $orden_parte_1 .= '<a href="#" data-id="' . $value->id . '" class=" sms_cotizacion" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Enviar mensaje" target="_blank" style="color: ' . $color_sms . ' !important"><i class="pe pe-7s-phone"></i></a>';

      //FIN ORDEN 1

      //ORDEN PARTE 1
      $orden_parte_2 = '';
      $orden_parte_2 .= '<a target="_blank" href="' . CONST_URL_CONEXION . 'OrdenServicio_Edita/' . encrypt($value->id_cita) . '" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar"><i class="pe pe-7s-note"></i></a>';

      $orden_parte_2 .= '<a target="_blank" href="' . CONST_URL_CONEXION . 'OrdenServicio_PDF/' . encrypt($value->id_cita) . '" class=""  aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Exportar orden"><i class="pe pe-7s-file"></i></a>';


      if (in_array($value->id . '-sms_orden2-1', $mensajes_enviados)) {
        $color_sms_orden2 = '#31B9CE';
      } else {
        $color_sms_orden2 = '#000';
      }

      $orden_parte_2 .= '<a href="#" data-id="' . $value->id_cita . '" data-url="' . CONST_URL_CONEXION . 'OrdenServicio_PDF/' . encrypt($value->id_cita) . '" data-idorden="' . $value->id . '" class="enviar_mensaje" aria-hidden="true" data-toggle="tooltip" data-placement="top" style="color: ' . $color_sms_orden2 . ' !important" title="Enviar mensaje" data-tipo="orden" data-tipomensaje="sms_orden2"><i class="pe pe-7s-phone"></i></a>';

      if (in_array($value->id . '-email_orden2-1', $mensajes_enviados)) {
        $color_email_orden2 = '#31B9CE';
      } else {
        $color_email_orden2 = '#000';
      }

      $orden_parte_2 .= '<a href="#" data-id="' . $value->id_cita . '" data-url="' . CONST_URL_CONEXION . 'OrdenServicio_PDF/' . encrypt($value->id_cita) . '" data-idorden="' . $value->id . '" class="enviar_correo" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Enviar correo" data-tipo="orden" data-tipomensaje="email_orden2" style="color: ' . $color_email_orden2 . ' !important"><i class="pe pe-7s-mail"></i></a>';

      $orden_parte_2 .= '<a href="#" class="js_upload" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Subir archivo" data-id="' . $value->id_cita . '" data-tipo="orden"><i class="pe-7s-upload"></i></a>';
      //FIN ORDEN 2

      //MULTIPUNTO
      $multipunto = '';
      $multipunto .= '<a target="_blank" href="' . CONST_URL_CONEXION . 'Multipunto_Edita/' . encrypt($value->id_cita) . '" data-id="' . $value->id_cita . '" class="" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Editar multipunto"><i class="pe pe-7s-note"></i></a>';

      $multipunto .= '<a target="_blank" href="' . CONST_URL_CONEXION . 'multipunto/Multipunto/setDataPDF/' . encrypt($value->id_cita) . '" class=""  aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Exportar multipunto"><i class="pe pe-7s-file"></i></a>';

      if (in_array($value->id . '-sms_multipunto-1', $mensajes_enviados)) {
        $color_sms_multipunto = '#31B9CE';
      } else {
        $color_sms_multipunto = '#000';
      }

      $multipunto .= '<a href="#" data-id="' . $value->id_cita . '" data-url="' . CONST_URL_CONEXION . 'multipunto/Multipunto/setDataPDF/' . encrypt($value->id_cita) . '" data-idorden="' . $value->id . '" class="enviar_mensaje" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Enviar mensaje" data-tipo="multipunto" data-tipomensaje="sms_multipunto" style="color: ' . $color_sms_multipunto . ' !important"><i class="pe pe-7s-phone"></i></a>';

      if (in_array($value->id . '-email_multipunto-1', $mensajes_enviados)) {
        $color_email_multipunto = '#31B9CE';
      } else {
        $color_email_multipunto = '#000';
      }

      $multipunto .= '<a href="#" data-id="' . $value->id_cita . '" data-url="' . CONST_URL_CONEXION . 'multipunto/Multipunto/setDataPDF/' . encrypt($value->id_cita) . '" data-idorden="' . $value->id . '" class="enviar_correo" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Enviar correo" data-tipo="hoja multipunto" data-tipomensaje="email_multipunto" style="color: ' . $color_email_multipunto . ' !important"><i class="pe pe-7s-mail"></i></a>';

      //FIN MULTIPUNTO

      //INICIO COTIZACIÓN MULTIPUNTO
      $cotizacion_multipunto = '';
      $cotizacion_multipunto .= '<a href="' . CONST_URL_CONEXION . 'Multipunto_Cotizacion/' . encrypt($value->id_cita) . '" data-id="' . $value->id . '" class="" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" target="_blank" title="Generar cotización"><i class="pe pe-7s-shopbag"></i></a>';

      $status_multipunto = $this->mo->cotizacion_multipunto_status($value->id_cita);


      if (in_array($value->id . '-email_cotizacion_multipunto-1', $mensajes_enviados)) {
        if ($status_multipunto == 'Si') {
          $color_multipunto = '#28a745';
        } elseif ($status_multipunto == 'No') {
          $color_multipunto = '#dc3545';
        } else {
          $color_multipunto = '#31B9CE';
        }
      } else {
        $color_multipunto = '#000';
      }
      $cotizacion_multipunto .= '<a href="#" data-id="' . $value->id . '" class="enviar_cotizacion_multipunto" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Enviar cotización" target="_blank" data-url="' . CONST_URL_CONEXION . 'Cotizacion_Cliente/' . encrypt($value->id_cita) . '" style="color: ' . $color_multipunto . ' !important"><i class="pe pe-7s-mail"></i></a>';

      if (in_array($value->id . '-sms_cotizacion_multipunto-1', $mensajes_enviados)) {
        $color_sms_multipunto = '#31B9CE';
      } else {
        $color_sms_multipunto = '#000';
      }


      $cotizacion_multipunto .= '<a href="#" data-id="' . $value->id . '" class="sms_cotizacion_multipunto" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Enviar mensaje" target="_blank" data-url="' . CONST_URL_CONEXION . 'Cotizacion_Cliente/' . encrypt($value->id_cita) . '" style="color: ' . $color_sms_multipunto . ' !important"><i class="pe pe-7s-phone"></i></a>';
      //FIN COTIZACION MULTIPUNTO

      //INICIO VOZ CLIENTE
      $voz_cliente = '';
      $voz_cliente .= '<a target="_blank" href="' . CONST_URL_CONEXION . 'VCliente_Alta/0" data-id="' . encrypt($value->id_cita) . '" class="" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Voz cliente"><i class="pe-7s-pen"></i></a>';
      $voz_cliente .= '<a target="_blank" href="#" data-id="' . $value->id_cita . '" class="js_voz" data-valor="1" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Voz cliente"><i class="pe-7s-micro"></i></a>';

      $voz_cliente .= '<a target="_blank" href="' . CONST_URL_CONEXION . 'VCliente_PDF/' . encrypt($value->id_cita) . '" class=""  aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Exportar voz cliente"><i class="pe-7s-file"></i></a>';
      //FIN VOZ CLIENTE

      //AUDITORIA CALIDAD
      $auditoria_calidad = '';

      $auditoria_calidad .= '<a target="_blank" href="' . CONST_URL_CONEXION . 'Auditoria_Calidad_PDF/' . encrypt($value->id_cita) . '" class=""  aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Exportar"><i class="pe-7s-file"></i></a>';
      //FIN AUDITORIA CALIDAD

      //Estatus intelisis
      $estatus_intelisis = '';

      $estatus_intelisis .= '<span class="intelisis_' . $value->id . '">' . $value->estatus_intelisis . '</span>';
      $estatus_intelisis .= '<a id="status_intelisis_' . $value->id . '" href="#" class=" js_cambiar_status_intelisis"  aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Cambiar estatus" data-id="' . $value->id . '" data-id_status="' . $value->id_status_intelisis . '"><i class="pe pe-7s-note"></i></a>';

      //fin estatus intelisis

      //Inicio situacion intelisis
      $situacion_intelisis = '';

      $situacion_intelisis .= '<span class="situacion_' . $value->id . '">
                  ' . $value->situacion_intelisis . '</span> <br>';
      $situacion_intelisis .= '<a id="situacion_intelisis_' . $value->id . '" href="#" class=" js_cambiar_situacion_orden"  aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Cambiar situación" data-id="' . $value->id . '" data-id_situacion="' . $value->id_situacion_intelisis . '"><i class="pe pe-7s-note"></i></a>';
      //fin situación intelisis

      $acciones = '';
      $acciones .= '<a href="" data-id="' . $value->id . '" class="js_comentarios" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Comentarios"><i class="pe-7s-comment"></i></a>';

      $acciones .= '<a href="" data-id="' . $value->id . '" class=" js_historial" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Historial comentarios"><i class="pe-7s-info
pe pe-7s-info"></i></a>';

      if (PermisoAccion('anexar_factura')) {
        if ($value->factura_pdf != '') {
          $color_factura = '#31B9CE';
        } else {
          $color_factura = '#000';
        }
        $acciones .= '<a href="" data-id="' . $value->id . '" class="js_factura" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Anexar factura" style="color: ' . $color_factura . ' !important"><i class="pe-7s-upload"></i></a>';
      }
      $acciones .= '<a target="_blank" href="' . base_url('mi-proceso-orden/' . encrypt($value->id_cita)) . '" data-id="' . $value->id . '" class="" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Proceso orden"><i class="pe-7s-note2"></i></a>';

      if ($value->folio_asociado == '' && $value->id_tipo_orden == 2) {
        $acciones .= '<a target="_blank" href="#" data-id="' . $value->id . '" data-id_cita="' . $value->id_cita . '" class="js_asociar_folio" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Asociar OR. A folio de garantía"><i class="pe-7s-back-2"></i></a>';
      }

      $dat[] = $orden_parte_1;
      $dat[] = $orden_parte_2;
      $dat[] = $multipunto;
      $dat[] = $cotizacion_multipunto;
      $dat[] = $voz_cliente;
      $dat[] = $auditoria_calidad;
      $dat[] = $estatus_intelisis;
      $dat[] = $situacion_intelisis;
      $dat[] = $acciones;

      $data[] = $dat;
    } //foreach

    $retornar = array(
      'draw' => intval($mismo),
      'recordsTotal' => intval($total),
      'recordsFiltered' => intval($total),
      'data' => $data
    );
    $this->output->set_content_type('application/json')->set_output(json_encode($retornar));
  }
  public function historial_comentarios_orden_servicio()
  {
    if ($this->input->is_ajax_request() && $this->session->userdata('id_usuario') == '') {
      $this->output->set_status_header('409');
      exit();
    } else if ($this->session->userdata('id_usuario') == '') {
      redirect('login');
    }
    $data['comentarios'] = $this->mo->getComentarios_orden_servicio($this->input->get('idorden'));
    $data['tipo_reagendar_proact'] = $this->input->get('tipo_reagendar_proact');
    $this->blade->render('orden/historial_comentarios', $data);
  }
  // GUARDAR COMENTARIOS ORDEN DE SERVICIO
  public function comentarios_orden_servicio()
  {
    if ($this->input->is_ajax_request() && $this->session->userdata('id_usuario') == '') {
      $this->output->set_status_header('409');
      exit();
    } else if ($this->session->userdata('id_usuario') == '') {
      redirect('login');
    }
    if ($this->input->post()) {
      $this->form_validation->set_rules('comentario', 'comentario', 'trim|required');
      if ($this->form_validation->run()) {
        $this->mo->saveComentario_ordenServicio();
        exit();
      } else {
        $errors = array(
          'comentario' => form_error('comentario'),
        );
        echo json_encode($errors);
        exit();
      }
    }
    $data['idorden'] = $this->input->get('idorden');
    $data['input_comentario'] = form_textarea('comentario', "", 'class="form-control" id="comentario" rows="2"');

    $this->blade->render('citas/comentarios_orden_servicio', $data);
  }
  public function escuchar_voz()
  {
    $q = $this->db->where('idPivote', $this->input->post('id'))->select('evidencia')->get('voz_cliente');
    if ($q->num_rows() == 1) {
      $data['url'] = CONST_URL_CONEXION . $q->row()->evidencia;
    } else {
      $data['url'] = '';
    }
    //echo $data['url'];die();
    $this->blade->render('vozcliente', $data);
  }
  public function upload_file_shara()
  {
    $data['id'] = $this->input->post('id');
    $this->blade->render('upload_file_shara', $data);
  }
  public function notificacion_gerencia()
  {
    $celular = $this->mc->telefono_asesor($this->input->post('idorden'));
    $mensaje = "Solicitar C.O. de la cita #" . $this->input->post('id_cita'); //mensaje de la notificación
    if ($this->input->post('orden_magic') != '') {
      $mensaje = $mensaje . ' ,orden magic: ' . $this->input->post('orden_magic');
    }
    $sucursal = CONST_BID; //sucursal
    $id_cita = $this->input->post('id_cita'); // ID de la cita
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion_gerencia");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt(
      $ch,
      CURLOPT_POSTFIELDS,
      "celular=" . $celular . "&mensaje=" . $mensaje . "&sucursal=" . $sucursal . "&id_cita=" . $id_cita . ""
    );
    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);
    //var_dump($server_output );
    curl_close($ch);

    echo 1;
    exit();
  }
  //Estatus intelisis
  public function cambiar_status_intelisis()
  {
    if ($this->input->is_ajax_request() && $this->session->userdata('id_usuario') == '') {
      $this->output->set_status_header('409');
      exit();
    } else if ($this->session->userdata('id_usuario') == '') {
      redirect('login');
    }
    if ($this->input->post()) {
      $this->form_validation->set_rules('id_status_intelisis', 'estatus', 'trim|required');
      if ($this->form_validation->run()) {

        $this->db->where('id', $this->input->post('id_orden_save'))->set('id_status_intelisis', $this->input->post('id_status_intelisis'))->update('ordenservicio');

        $datos_historial = array(
          'estatus_anterior' => $this->mc->getStatusIntelisis($this->input->post('id_status_actual')),
          'estatus_nuevo' => $this->mc->getStatusIntelisis($this->input->post('id_status_intelisis')),
          'usuario' => $this->mc->getNameUser($this->session->userdata('id_usuario')),
          'created_at' => date('Y-m-d H:i:s'),
          'idorden' => $this->input->post('id_orden_save')
        );
        $idorden = $this->input->post('id_orden_save');
        $this->db->insert('historial_cambio_status_intelisis', $datos_historial);
        $this->db->where('id', $idorden)->set('id_status_intelisis', 3)->update('ordenservicio');

        //Si está cancelando, cancelar las operaciones
        if ($_POST['id_status_intelisis'] == 4) {
          $this->db->where('idorden', $_POST['id_orden_save'])->set('cancelado', 1)->update('articulos_orden');
        }
        echo 1;
        exit();
      } else {
        $errors = array(
          'id_status_intelisis' => form_error('id_status_intelisis'),
        );
        echo json_encode(array('exito' => false, 'errors' => $errors));
        exit();
      }
    }

    $info = new Stdclass();

    $data['drop_id_status_intelisis'] = form_dropdown('id_status_intelisis', array_combos($this->mcat->get('cat_status_intelisis_orden', 'status'), 'id', 'status', TRUE), $this->input->get('id_status'), 'class="form-control" id="id_status_intelisis" ');

    $data['id_status_actual'] = $this->input->get('id_status');
    $data['id_orden_save'] = $this->input->get('id');

    $this->blade->render('cambiar_status_intelisis', $data);
  }
  public function entregarUnidad()
  {
    $id_cita = $this->input->post('id_cita');
    //Firma cliente
    if (!$this->mc->firma_cliente($id_cita)) {
      echo -1;
      exit();
    }
    $situacion_actual = $this->ml->getSituacionOrden($id_cita);
    if ($situacion_actual != 6) {
      echo -2;
      exit();
    }
    $this->db->where('id_cita', $id_cita)->set('unidad_entregada', 1)->update('citas');
    $this->db->where('id_cita', $id_cita)->set('fecha_termino_lavado', date('Y-m-d H:i:s'))->set('fecha_entrega_unidad', date('Y-m-d H:i:s'))->update('citas');

    //CUANDO LA UNIDAD ES ENTREGADA TAMBIÉN GUARDAR LA SITUACION DE ORDEN CERRADA / ENTREGADA Y EL HISTORIAL
    $idorden = $this->mc->getIdOrdenByCita($id_cita);
    $id_situacion_anterior = $this->mc->getSituacionIntelisis($this->mc->getSituacionOrden($idorden));

    $this->db->where('id', $idorden)->set('id_situacion_intelisis', 5)->update('ordenservicio');
    $datos_historial = array(
      'situacion_anterior' => $id_situacion_anterior,
      'situacion_nueva' => $this->mc->getSituacionIntelisis(5),
      'usuario' => $this->mc->getNameUser($this->session->userdata('id_usuario')),
      'created_at' => date('Y-m-d H:i:s'),
      'idorden' => $idorden
    );
    $this->db->insert('historial_cambio_situacion_intelisis', $datos_historial);
    $this->db->where('id', $idorden)->set('id_status_intelisis', 3)->update('ordenservicio');
    echo 1;
    die();
  }
  public function modal_factura()
  {
    $data['idorden'] = $this->input->post('id');
    $info = $this->mo->getDataOrdenId($this->input->post('id'));

    $data['factura_pdf'] = $info->factura_pdf;
    $data['path_factura_pdf'] = $info->path_factura_pdf;
    $data['factura_xml'] = $info->factura_xml;
    $data['path_factura_xml'] = $info->path_factura_xml;
    //print_r($data);die();
    $this->blade->render('factura', $data);
  }
  public function upload($id = '', $formato = '')
  {
    $result = $this->mo->upload('file', $formato);
    echo json_encode($result);
  }
  public function upload_delete_pdf()
  {
    $directorio = $this->input->post('path_factura_pdf');
    if (file_exists($directorio)) {
      unlink($directorio);
      $result = array('isok' => true);
    } else {
      $result = array('isok' => true);
    }

    $datos = array(
      'archivo' => $_POST['factura_pdf'],
      'path' => $_POST['path_factura_pdf'],
      'created_at' => date('Y-m-d H:i:s'),
      'id_usuario' => $this->session->userdata('id_usuario'),
      'tipo' => 1
    );
    $this->db->insert('historial_facturas_eliminadas', $datos);
    echo json_encode($result);
  }
  public function upload_delete_xml()
  {
    $directorio = $this->input->post('path_factura_xml');
    if (file_exists($directorio)) {
      unlink($directorio);
      $result = array('isok' => true);
    } else {
      $result = array('isok' => true);
    }
    //$this->db->where('path_factura_xml',$directorio)->set('factura_xml','')->set('path_factura_xml','')->update('ordenservicio');
    $datos = array(
      'archivo' => $_POST['factura_xml'],
      'path' => $_POST['path_factura_xml'],
      'created_at' => date('Y-m-d H:i:s'),
      'id_usuario' => $this->session->userdata('id_usuario'),
      'tipo' => 2
    );
    $this->db->insert('historial_facturas_eliminadas', $datos);
    echo json_encode($result);
  }
  function guardarFactura()
  {
    $this->form_validation->set_rules('factura_pdf', ' ', 'trim|required');
    $this->form_validation->set_rules('factura_xml', ' ', 'trim|required');
    //print_r($_POST);die();
    if ($this->form_validation->run()) {
      $facturas = array(
        'factura_pdf' => $_POST['factura_pdf'],
        'path_factura_pdf' => $_POST['path_factura_pdf'],
        'factura_xml' => $_POST['factura_xml'],
        'path_factura_xml' => $_POST['path_factura_xml'],
      );
      $this->db->where('id', $_POST['ordenid'])->update('ordenservicio', $facturas);
      $this->updateFechaFactura($_POST['ordenid'], $_POST['factura_xml']);
      echo 1;
      exit();
    } else {
      $errors = array(
        'factura_pdf' => form_error('factura_pdf'),
        'factura_xml' => form_error('factura_xml'),
      );
      echo json_encode($errors);
      exit();
    }
  }
  public function login_eliminar_facturas()
  {
    if ($this->input->post()) {
      $this->form_validation->set_rules('usuario', 'usuario', 'trim|required');
      $this->form_validation->set_rules('password', 'contraseña', 'trim|required');

      if ($this->form_validation->run()) {
        $this->mo->validarLogin();
      } else {
        $errors = array(
          'usuario' => form_error('usuario'),
          'password' => form_error('password'),
        );
        echo json_encode($errors);
        exit();
      }
    }
    $data['input_usuario'] = form_input('usuario', $this->input->get('usuario_tecnico'), 'class="form-control" rows="5" id="usuario" ');

    $data['input_password'] = form_password('password', "", 'class="form-control" rows="5" id="password" ');

    $this->blade->render('login_facturas', $data);
  }
  public function updateFechaFactura($orden = '', $archivo = '')
  {
    $sucursal = CONST_BID;
    $url = CONST_URL_FACTURA . $archivo;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, CONST_URL_READ_XML);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt(
      $ch,
      CURLOPT_POSTFIELDS,
      "url=" . $url . "&orden=" . $orden . "&sucursal=" . $sucursal . ""
    );
    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);
    //var_dump($server_output );
    curl_close($ch);

    $this->enviarFacturasCliente($orden);
  }
  public function enviarFacturasCliente($id_orden = '')
  {
    $datos_orden = $this->mo->getDataOrdenId($id_orden);
    $id_cita = $datos_orden->id_cita;

    $correo_consumidor = $this->mc->correo_consumidor($id_cita);
    $correo_compania = $this->mc->correo_compania($id_cita);
    $factura_pdf = base_url() . 'assets/facturas/' . $datos_orden->factura_pdf;
    $factura_xml = base_url() . 'assets/facturas/' . $datos_orden->factura_xml;

    $array_facturas = array();

    $array_facturas[] = $factura_pdf;
    $array_facturas[] = $factura_xml;
    //$array_facturas[] = CONST_URL_BASE . 'multipunto/Multipunto/setDataPDF/' . encrypt($id_cita);
    if ($correo_consumidor != $correo_compania) {
      if ($correo_consumidor != '') {
        correo_facturacion($correo_consumidor, encrypt($id_cita), $array_facturas);
      }
      if ($correo_compania != '') {
        correo_facturacion($correo_compania, encrypt($id_cita), $array_facturas);
      }
    } else {
      correo_facturacion($correo_consumidor, encrypt($id_cita), $array_facturas);
    }
  }
  public function cancelar_orden()
  {

    //Obtener estatus actual de la cita y validar que no esté terminado
    // $estatus_actual_cita = $this->db->where('id_cita',$this->input->post('id_cita'))->select('id_status')->get('citas')->row()->id_status;
    // if($estatus_actual_cita==3||$estatus_actual_cita==8||$estatus_actual_cita==9||$estatus_actual_cita==10||$estatus_actual_cita==11){
    //   echo -1;exit();
    // }
    //Cancelar cita
    $this->db->where('id_cita', $this->input->post('id_cita'))->set('cancelada', 1)->update('citas');
    //Cancelar orden
    $this->db->where('id', $this->input->post('id'))->set('cancelada', 1)->update('ordenservicio');

    //tecnicos_citas/tecnicos_citas_historial
    $this->db->where('id_cita', $this->input->post('id_cita'))->set('historial', 1)->update('tecnicos_citas');
    $this->db->where('id_cita', $this->input->post('id_cita'))->set('historial', 1)->update('tecnicos_citas_historial');

    $this->db->where('id_cita', $this->input->post('id'))->set('historial', 1)->update('citas');

    //cancelar operaciones
    $this->db->where('idorden', $this->input->post('id'))->set('cancelado', 1)->update('articulos_orden');
    //Cambiar estatus intelisis a cancelado 
    $this->db->where('id', $this->input->post('id'))->set('id_status_intelisis', 4)->update('ordenservicio');

    eliminar_orden_dms($this->input->post('id_cita'));

    //Guardar el historial 
    $historial_cancelaciones = array(
      'idorden' => $_POST['id'],
      'idusuario' => $this->session->userdata('id_usuario'),
      'created_at' => date('Y-m-d H:i:s')
    );
    $this->db->insert('historial_ordenes_canceladas', $historial_cancelaciones);
    echo 1;
    die();
  }
  public function sendMessages()
  {
    $celular = "3328351119"; //celular 
    $mensaje = "MENSAJE DE prueba123"; //mensaje 160 caracteres
    $sucursal = "M123"; // sucursal

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_mensaje");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt(
      $ch,
      CURLOPT_POSTFIELDS,
      "celular=" . $celular . "&mensaje=" . $mensaje . "&sucursal=" . $sucursal . ""
    );
    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec($ch);
    curl_close($ch);
    var_dump($server_output);
  }
  public function asociar_orden_folio()
  {
    $data['folios'] = $this->curl->getinfoGet(CONST_FOLIOS_GARANTIAS)->data;
    $this->blade->render('asociar_folio_orden', $data);
  }
  public function ingresarFolio()
  {
    $this->db->where('id_cita', $_POST['id_cita'])->set('folio_asociado', $_POST['folio'])->update('citas');
    $this->db->where('id_cita', $_POST['orden_asociada'])->set('folio_asociado', $_POST['folio'])->update('citas');
    echo 1;
    exit();
  }
  public function asociar_operacion_folio()
  {
    
    $data['folios'] = $this->getFoliosOperacionesAPI($_POST['id_cita']);
    $this->blade->render('asociar_folio_operacion', $data);
  }
  public function ingresarFolioOperacion()
  {
    $this->db->where('id', $_POST['idop'])->set('folio_asociado', $_POST['folio'])->update('articulos_orden');
    echo 1;
    exit();
  }
  public function getFoliosOperacionesAPI($no_orden='')
  {
    // $no_orden='43772';
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://sohexdms.com.mx/panel_garantias/index.php/garantias/F1863/partes_operaciones_byorden',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'no_orden='.$no_orden,
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/x-www-form-urlencoded',
        'Cookie: ci_session=6sctvp55kpl7rvnir0uhr12dj8iv6hde'
      ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    return json_decode($response);
  }
  public function modificaciones($id_cita=''){
    $historial = $this->db->where('id_cita',$id_cita)->select('c.*,ad.adminNombre')->join(CONST_BASE_PRINCIPAL.'admin ad','c.id_usuario = ad.adminId','left')->order_by('c.created_at','desc')->get('campos_editados_orden c')->result();
    $data['id_cita'] = $id_cita;

    $datos = [];
    foreach($historial as $h => $hist){
      $cambios = json_decode($hist->cambio);
      $temp = '';
      foreach($cambios as $c => $cambio){
        $temp.= "<strong>$c:</strong> DE $cambio->campo_anterior A $cambio->campo_nuevo <br>";
      }
      $datos[$h]['cambio'] = $temp;
      $datos[$h]['created_at'] = $hist->created_at;
      $datos[$h]['adminNombre'] = $hist->adminNombre;
    }
    $data['historial'] = $datos;
    $this->blade->render('v_modificaciones_orden',$data);
  }
}
