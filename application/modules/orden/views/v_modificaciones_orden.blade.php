@layout('tema_luna/layout')
@section('contenido')
		
	<h2>Modificaciones orden #{{$id_cita}}</h2>
	<br>
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Usuario</th>
						<th>Fecha</th>
						<th>Cambio</th>
					</tr>
				</thead>
				<tbody>
					@foreach($historial as $h =>$value)
						<tr>
							<td>{{$value['adminNombre']}}</td>
							<td>{{date_eng2esp_time($value['created_at'])}}</td>
							<td>{{$value['cambio']}}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection
