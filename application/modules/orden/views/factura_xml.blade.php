<div id="divfileupload_xml" style="{{(($xml != '') ? 'display:none;' : '')}}">
	<span class="btn btn-success fileinput-button btn-xs" title="Agregar factura xml">
	    <i class="pe pe-7s-plus"></i>
	    <span>Agregar factura xml</span>
	    <!-- The file input field used as target for the file upload widget -->
	    <input id="fileupload_xml" type="file" name="file[]" data-url="{{ site_url('orden/upload/').$id }}/xml" multiple class="fileupload_xml btn-success">
	</span>

	<div id="progress_xml" class="progress_xml" style="display:none;">
	    <div class="progress-bar_xml progress-bar-azul"></div>
	</div>
</div>

<div id="files_xml" class="files_xml">
<?php if($xml != ''){ ?>
	
	<?php echo anchor(base_url('assets/facturas/'.$xml),'<i class="fa fa-save-file"></i> Descargar','title="'.$xml.'" class="btn btn-info btn-xs" target="_blank"') ?>
	<?php echo anchor('orden/upload_delete/'.$xml,'<i class="pe pe-7s-trash"></i>','data-id="" data-xml="'.$xml.'" title="Eliminar'.$xml.'" class="btn btn-xs eliminar_xml text-centernteright" target="_blank"') ?>

<?php } ?>
</div>


