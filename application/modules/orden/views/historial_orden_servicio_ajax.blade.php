@layout('tema_luna/layout')
<style>
    .multiselect-container {
        width: 100% !important;
    }

    .cgris {
        color: #000 !important;
    }

    .cverde {
        color: #1F7D31 !important;
        background-color: transparent
    }

    .bg-verde {
        background-color: #c3e6cb !important;
    }

    .bg-red {
        background-color: #f8d7da !important;
    }

    td a {
        margin-right: 1px;
        font-size: 20px !important;
    }

</style>
<link href="{{ base_url('css/custom/jquery.fileupload.css') }}" rel="stylesheet">
@section('css_vista')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('contenido')
    <div class="row">
        <div class="col-lg-2 col-sm-3 col-md-6">
            <div class="alert alert-primary" role="alert" style="background-color:#28a745;">
                <span style="color: white; ">Aceptada</span>
            </div>
        </div>
        <div class="col-lg-2 col-sm-3 col-md-6">
            <div class="alert alert-primary" role="alert" style="background-color:#dc3545;">
                <span style="color: white; ">Rechazada</span>
            </div>
        </div>
        <div class="col-lg-2 col-sm-3 col-md-6">
            <div class="alert alert-primary" role="alert" style="background-color:#31B9CE;">
                <span style="color: white; ">Enviada</span>
            </div>
        </div>
        <div class="col-lg-2 col-sm-3 col-md-6">
            <div class="alert alert-primary" role="alert" style="background-color:white; border-color: black">
                <span style="color: black; ">Sin enviar</span>
            </div>
        </div>
    </div>
    <br>
    <form id="frm" action="" method="POST">
        <div class="row">
            <div class="col-sm-3">
                <label for="">Asesor</label>
                {{ $drop_asesores }}
            </div>
            <div class="col-sm-3">
                <label for="">Estatus</label>
                {{ $drop_estatus }}
            </div>
            <div class="col-sm-3">
                <label for="">Situación</label>
                {{ $drop_situaciones }}
            </div>
            <div class="col-sm-3">
                <label for="">Cita previa</label>
                {{ $drop_tipo_cita }}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                <label for="">Estatus técnico</label>
                {{ $drop_estatus_tecnicos }}
            </div>
            <div class='col-md-3'>
                <label for="">Fecha inicio</label>
                <input type="date" class="form-control" name="finicio" id="finicio" value="">
                <span style="color: red" class="error error_fini"></span>
            </div>
            <div class='col-md-3'>
                <label for="">Fecha Fin</label>
                <input type="date" class="form-control" name="ffin" id="ffin" value="">
                <span style="color: red" class="error error_ffin"></span>
            </div>
            <div class="col-sm-3">
                <label for="">Buscar por campo</label>
                <input class="form-control" type="text" name="buscar_campo" id="buscar_campo">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <label for="">Filtro tipo de orden</label>   
                {{ $drop_estatus_cita }}
            </div>
            <div class="col-sm-2">
                <label for="">Filtro cierre orden</label> <br> 
                <input type="checkbox" name="filtro_cierre_orden" id="filtro_cierre_orden">
            </div>
            <div class="col-sm-4" style="margin-top:30px;">
                <button type="button" id="exportarExcel" data-action="excel/generateXls"
                    class="btn btn-info js_exportar"><i style="color: white;"
                        class="fa fa-file-excel-o"></i>Exportar EXCEL</button>
                <button style="margin-right: 10px" type="button" id="exportarPDF" data-action="citas/exportar_orden_pdf"
                        class="btn btn-primary js_exportar"><i style="color: white;"
                            class="fa fa-file-pdf-o"></i>Exportar PDF</button>
            </div>
            <div class="col-sm-3 pull-right" style="margin-top:30px; text-align:right">
                <button type="button" id="buscar" class="btn btn-success">Buscar</button>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-lg-3 col-sm-4">
                <div class="alert alert-success bg-verde" role="alert">
                    Operaciones asignadas
                </div>
            </div>
            <div class="col-lg-3 col-sm-4">
                <div class="alert alert-danger bg-red" role="alert">
                    Operaciones no asignadas
                </div>
            </div>
        </div>
    </form>
    <br>
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-bordered table-striped" id="tabla" class="display" width="100%" cellpadding="0">
                <thead>
                    <tr class="tr_principal">
                        <th># Orden</th>
                        <th>Placas</th>
                        <th># Serie</th>
                        <th>Fecha Recepción</th>
                        <th>Fecha promesa entrega</th>
                        <th>Fecha entrega unidad</th>
                        <th>Tipo orden</th>
                        <th>Cliente</th>
                        <th>Estatus cita</th>
                        <th>Cita previa</th>
                        <th>Orden Parte I</th>
                        <th>Orden Parte II</th>
                        <th>Multipunto</th>
                        <th>Cotización multipunto</th>
                        <th>Voz cliente</th>
                        <th>Auditoría de calidad</th>
                        <th>Estatus</th>
                        <th>Situación</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ base_url() }}js/custom/jquery-ui.min.js"></script>
    <script src="{{ base_url() }}js/custom/jquery.fileupload.js"></script>
    <script>
        var site_url = "{{ site_url() }}";
        var idorden = '';
        var url_archivo = '';
        var tipo = '';
        var tipomensaje = '';
        var id = '';
        var id_cita = '';
        var orden_magic = '';
        var url = '';
        var id_cita = '';
        var idorden = '';
        $('.multiple').multiselect({
            enableFiltering: true,
            enableFullValueFiltering: true,
            buttonWidth: '100%',
            includeSelectAllOption: true,
            selectAllJustVisible: false,
            buttonClass: 'btn btn-info',
        });
        $(".js_exportar").on('click', function() {
            var action = $(this).data('action');
            $('#frm').attr('action', action);
            $('#frm').submit();
        });

        iniciarTabla();
        $("#buscar").on('click', function() {
            $(".error").empty();
            var finicio = $("#finicio").val();
            var ffin = $("#ffin").val();
            var table = $('#tabla').DataTable();
            table.destroy();
            iniciarTabla();

        })

        function iniciarTabla() {
            var tabla = $("#tabla").dataTable({
                paging: true,
                bFilter: false,
                processing: true,
                responsive: true,
                serverSide: true,
                ajax: {
                    url: site_url + "/orden/getDatosOrdenAjax",
                    type: 'POST',
                    data: function(data) {
                        buscar_id = 0;
                        if ($("#buscar_id").prop('checked')) {
                            buscar_id = 1;
                        }
                        data.buscar_campo = $("#buscar_campo").val();
                        data.buscar_id = buscar_id;
                        data.finicio = $("#finicio").val();
                        data.ffin = $("#ffin").val();
                        data.id_asesor = $("#id_asesor").val();
                        data.id_status_search = $("#id_status_search").val();
                        data.id_situacion_intelisis_search = $("#id_situacion_intelisis_search").val();
                        data.cita_previa = $("#cita_previa").val();
                        data.id_status_tecnico = $("#id_status_tecnico").val();
                        data.id_status_color = $("#id_status_color").val();
                        data.filtro_cierre_orden = ($("#filtro_cierre_orden").prop('checked') ? 1 : 0);
                        empieza = data.start;
                        por_pagina = data.length;
                    }
                },
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Siguiente",
                        "sLast": "Última",
                        "sFirst": "Primera"
                    },
                    "sLengthMenu": '<div id="combo_datatable">Mostrar <select>' +
                        '<option value="5">5</option>' +
                        '<option value="10">10</option>' +
                        '<option value="20">20</option>' +
                        '<option value="30">30</option>' +
                        '<option value="40">40</option>' +
                        '<option value="50">50</option>' +
                        '<option value="-1">Todos</option>' +
                        '</select> registros',
                    "sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)",
                    "sInfoFiltered": " - filtrados de _MAX_ registros",
                    "sInfoEmpty": "No hay resultados de búsqueda",
                    "sZeroRecords": "No hay registros para mostrar...",
                    "sProcessing": "Espere, por favor...",
                    "sSearch": "Buscar"
                },
            });
        }
        $("#datetimepicker1").on("dp.change", function(e) {
            //$('#datetimepicker2').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker2").on("dp.change", function(e) {
            //$('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
        });
        $('.date').datetimepicker({
            format: 'DD/MM/YYYY',
            icons: {
                time: "far fa-clock",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            locale: 'es'
        });
        $("body").on("click", '.js_voz', function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var url = site_url + "/orden/escuchar_voz/0";
            customModal(url, {
                "id": id
            }, "POST", "md", "", "", "", "Salir", "Voz cliente", "modal1");
        });
        $("body").on('click', '.js_upload', function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var url = site_url + "/orden/upload_file_shara/0";
            customModal(url, {
                "id": id
            }, "POST", "md", "", "", "", "Salir", "Subir archivo", "modalUpload");

        })
        $("body").on('click', '.solicitar_gerencia', function(e) {
            e.preventDefault();
            id_cita = $(this).data('id_cita');
            idorden = $(this).data('id');
            orden_magic = $(this).data('orden_magic');

            ConfirmCustom("¿Está seguro de solicitar a gerencia C.O?", solicitarGerencia, "", "Confirmar",
                "Cancelar");

        })

        function solicitarGerencia() {
            ajaxJson(site_url + "/orden/notificacion_gerencia", {
                "id_cita": id_cita,
                "orden_magic": orden_magic,
                "idorden": idorden
            }, "POST", "async", function(result) {
                if (result == 1) {
                    ExitoCustom("Registro actualizado correctamente", function() {
                        location.reload();
                    });
                } else {
                    ErrorCustom("Error, por favor intente de nuevo");
                }
            });
        }
        //Estatus intelisis
        $("body").on("click", '.js_cambiar_status_intelisis', function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var id_status = $(this).data('id_status');
            var url = site_url + "/orden/cambiar_status_intelisis/0";
            customModal(url, {
                    "id": id,
                    "id_status": id_status
                }, "GET", "md", cambiar_status_intelisis, "", "Guardar", "Cancelar", "Cambiar estatus",
                "modalStatusIntelisis");

        });
        // $('body').on('click','#buscar',function(e){
        //     e.preventDefault();         
        //     var url =site_url+"/citas/buscar_orden_servicio/0";
        //     ajaxLoad(url,$("#frm").serialize(),"div_tabla","POST",function(){
        //         iniciarTabla("#tbl");
        //     }); 
        // });
        $('body').on('click', '#exportar1', function(e) {
            $("#frm").submit();
        });

        function cambiar_status_intelisis() {
            id_status_actual = $("#id_status_actual").val();
            id_nuevo_status = $("#id_status_intelisis").val();
            id_orden = $("#id_orden_save").val();
            status_text = $("#id_status_intelisis option:selected").text();
            //NUEVA VERSIÓN
            if (id_nuevo_status == 4) {
                ConfirmCustom("¿Está seguro de cancelar NO PODRÁS DESHACER ESTA ACCIÓN ?", confirmarCambio, "", "Confirmar",
                    "Cancelar");
            } else {
                cambiarStatusIntelisis()
            }
        }

        function confirmarCambio() {
            cambiarStatusIntelisis()
        }

        function cambiarStatusIntelisis() {
            if (id_status_actual == id_nuevo_status) {
                ErrorCustom("Es necesario exista un cambio de estatus");
            } else {
                ajaxJson(site_url + "/orden/cambiar_status_intelisis", $("#frm_status_intelisis").serialize(), "POST", "",
                    function(result) {
                        if (result == 1) {
                            ExitoCustom("Estatus actualizado correctamente", function() {
                                $(".intelisis_" + id_orden).text(status_text);
                                $("#status_intelisis_" + id_orden).data('id_status', id_nuevo_status);
                                $(".close").trigger('click');
                            });
                        } else {
                            ErrorCustom("Error al enviar el mensaje");
                        }
                    });
            }
        }

        //Situación intelisis
        $("body").on("click", '.js_cambiar_situacion_orden', function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var id_situacion = $(this).data('id_situacion');
            var url = site_url + "/citas/cambiar_situacion_orden/0";
            customModal(url, {
                    "id": id,
                    "id_situacion": id_situacion
                }, "GET", "md", cambiar_situacion_orden, "", "Guardar", "Cancelar", "Cambiar situación",
                "modalSituacionIntelisis");

        });

        function cambiar_situacion_orden() {
            var id_situacion_actual = $("#id_situacion_actual").val();
            var id_nueva_situacion = $("#id_situacion_intelisis").val();
            var id_orden = $("#id_orden_save").val();
            var situacion_text = $("#id_situacion_intelisis option:selected").text();
            if (id_situacion_actual == id_nueva_situacion) {
                ErrorCustom("Es necesario exista un cambio de situación");
            } else {
                ajaxJson(site_url + "/citas/cambiar_situacion_orden", $("#frm_situacion_intelisis").serialize(), "POST", "",
                    function(result) {
                        result = JSON.parse(result);
                        if(result.exito){
                            ExitoCustom(result.message, function() {
                                    $(".situacion_" + id_orden).text(situacion_text);
                                    $("#situacion_intelisis_" + id_orden).data('id_situacion',
                                        id_nueva_situacion);
                                    $(".close").trigger('click');
                            });
                        }else{
                            if(result.validations){
                                $.each(JSON.parse(result.validations), function(i, item) {
                                    $(".error_" + i).empty();
                                    $(".error_" + i).append(item);
                                    $(".error_" + i).css("color", "red");
                                });
                            }else{
                                ErrorCustom(result.message);
                            }   
                        }
                        
                    });
            }
        }

        //Estatus orden
        $("body").on("click", '.js_cambiar_status_orden_intelisis', function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var id_status = $(this).data('id_status');
            var url = site_url + "/citas/cambiar_status_orden/0";
            customModal(url, {
                    "id": id,
                    "id_status": id_status
                }, "GET", "md", cambiar_status_orden, "", "Guardar", "Cancelar", "Cambiar estatus",
                "modalStatusIntelisis");

        });

        function cambiar_status_orden() {
            var id_status_actual = $("#id_status_actual").val();
            var id_nuevo_status = $("#id_status_orden").val();
            var id_orden = $("#id_orden_save").val();
            var status_text = $("#id_status_orden option:selected").text();
            if (id_status_actual == id_nuevo_status) {
                ErrorCustom("Es necesario exista un cambio de estatus");
            } else {
                ajaxJson(site_url + "/citas/cambiar_status_orden", $("#frm_status_orden_intelisis").serialize(), "POST", "",
                    function(result) {
                        if (result == 1) {
                            ExitoCustom("Estatus actualizado correctamente", function() {
                                $(".status_orden_" + id_orden).text(status_text);
                                $("#status_orden_intelisis_" + id_orden).data('id_status', id_nuevo_status);
                                $(".close").trigger('click');
                            });
                        } else {
                            ErrorCustom("Error al enviar el mensaje");
                        }
                    });
            }
        }


        $("body").on("click", '.enviar_mensaje', function(e) {
            e.preventDefault();
            url_archivo = $(this).data('url');
            idorden = $(this).data('idorden');
            tipo = $(this).data('tipo');
            //Saber que tipo de mensaje es
            tipomensaje = $(this).data('tipomensaje');
            ConfirmCustom("¿Está seguro de enviar el mensaje?", enviarMensaje, "", "Confirmar", "Cancelar");
            //ErrorCustom("Servicio temporalmente suspendido");
        });

        function enviarMensaje() {
            ajaxJson(site_url + "/citas/sms_multipunto_orden", {
                "url_archivo": url_archivo,
                "idorden": idorden,
                "tipo": tipo,
                "tipomensaje": tipomensaje
            }, "POST", "async", function(result) {
                if (result == 1) {
                    ExitoCustom("Mensaje enviado correctamente", function() {
                        location.reload();
                    });
                } else if (result == -3) {
                    ErrorCustom("Debe pasar por lo menos un lapso de 30 minutos para poder enviar el mensaje");
                } else {
                    ErrorCustom("Error al enviar el mensaje");
                }
            });
        }

        $("body").on("click", '.enviar_correo', function(e) {
            e.preventDefault();
            url_archivo = $(this).data('url');
            idorden = $(this).data('idorden');
            tipo = $(this).data('tipo');
            tipomensaje = $(this).data('tipomensaje');
            ConfirmCustom("¿Está seguro de enviar el correo?", enviarCorreo, "", "Confirmar", "Cancelar");
        });

        function enviarCorreo() {
            ajaxJson(site_url + "/citas/enviar_correo_orden_multipunto", {
                "url_archivo": url_archivo,
                "idorden": idorden,
                "tipo": tipo,
                "tipomensaje": tipomensaje
            }, "POST", "async", function(result) {
                if (result == 1) {
                    ExitoCustom("Mensaje enviado correctamente", function() {
                        location.reload();
                    });
                } else {
                    ErrorCustom("Error al enviar el mensaje");
                }
            });
        }

        $("body").on("click", '.enviar_cotizacion', function(e) {
            e.preventDefault();
            idorden = $(this).data('id');
            ConfirmCustom("¿Está seguro de enviar la cotización?", enviarCotizacion, "", "Confirmar", "Cancelar");

        });

        function enviarCotizacion() {
            ajaxJson(site_url + "/citas/enviar_cotizacion", {
                "idorden": idorden
            }, "POST", "async", function(result) {
                if (result == 1) {
                    ExitoCustom("Mensaje enviado correctamente", function() {
                        location.reload();
                    });
                } else if (result == -2) {
                    ErrorCustom("No se ha generado una cotización");
                } else if (result == -3) {
                    ErrorCustom("Debe pasar por lo menos un lapso de 30 minutos para poder enviar el mensaje");
                } else {
                    ErrorCustom("Error al enviar el mensaje");
                }
            });
        }

        $("body").on("click", '.sms_cotizacion', function(e) {
            e.preventDefault();
            idorden = $(this).data('id');
            ConfirmCustom("¿Está seguro de enviar el mensaje?", mensajeCotizacion, "", "Confirmar", "Cancelar");
        });

        function mensajeCotizacion() {
            ajaxJson(site_url + "/citas/sms", {
                "idorden": idorden
            }, "POST", "async", function(result) {
                if (result == 1) {
                    ExitoCustom("Mensaje enviado correctamente", function() {
                        location.reload();
                    });
                } else if (result == -2) {
                    ErrorCustom("No se ha generado una cotización");
                } else if (result == -3) {
                    ErrorCustom("Debe pasar por lo menos un lapso de 30 minutos para poder enviar el mensaje");
                } else {
                    ErrorCustom("Error al enviar el mensaje");
                }
            });
        }
        //Multipunto
        $("body").on("click", '.enviar_cotizacion_multipunto', function(e) {
            e.preventDefault();
            idorden = $(this).data('id');
            url = $(this).data('url');
            ConfirmCustom("¿Está seguro de enviar el correo?", correoCotizacion, "", "Confirmar", "Cancelar");
        });

        function correoCotizacion() {
            ajaxJson(site_url + "/citas/enviar_cotizacion_multipunto", {
                "idorden": idorden,
                "url": url
            }, "POST", "async", function(result) {
                if (result == 1) {
                    ExitoCustom("Mensaje enviado correctamente", function() {
                        location.reload();
                    });
                } else if (result == -1) {
                    ErrorCustom("El presupuesto no ha sido firmado por el asesor");
                } else {
                    ErrorCustom("Error al enviar el mensaje");
                }
            });
        }
        $("body").on("click", '.sms_cotizacion_multipunto', function(e) {
            e.preventDefault();
            idorden = $(this).data('id');
            url = $(this).data('url');
            ConfirmCustom("¿Está seguro de enviar el mensaje?", smsCotizacion, "", "Confirmar", "Cancelar");
        });

        function smsCotizacion() {
            ajaxJson(site_url + "/citas/sms_multipunto", {
                "idorden": idorden,
                "url": url
            }, "POST", "async", function(result) {
                if (result == 1) {
                    ExitoCustom("Mensaje enviado correctamente", function() {
                        location.reload();
                    });
                } else if (result == -3) {
                    ErrorCustom("Debe pasar por lo menos un lapso de 30 minutos para poder enviar el mensaje");
                } else {
                    ErrorCustom("Error al enviar el mensaje");
                }
            });
        }

        $("body").on("click", '.js_comentarios', function(e) {
            e.preventDefault();
            idorden = $(this).data('id');
            var url = site_url + "/orden/comentarios_orden_servicio/0";
            customModal(url, {
                "idorden": idorden
            }, "GET", "md", ingresarComentario, "", "Guardar", "Cancelar", "Ingresar comentario", "modal1");
        });
        $("body").on("click", '.js_historial', function(e) {
            e.preventDefault();
            idorden = $(this).data('id');
            var url = site_url + "/orden/historial_comentarios_orden_servicio/0";
            customModal(url, {
                "idorden": idorden
            }, "GET", "md", "", "", "", "Cerrar", "Historial de comentarios", "modal1");
        });

        function ingresarComentario() {
            var url = site_url + "/orden/comentarios_orden_servicio";
            ajaxJson(url, $("#frm_comentarios").serialize(), "POST", "", function(result) {
                if (isNaN(result)) {
                    data = JSON.parse(result);
                    //Se recorre el json y se coloca el error en la div correspondiente
                    $.each(data, function(i, item) {
                        $.each(data, function(i, item) {
                            $(".error_" + i).empty();
                            $(".error_" + i).append(item);
                            $(".error_" + i).css("color", "red");
                        });
                    });
                } else {
                    if (result < 0) {
                        ErrorCustom('No se pudo guardar el comentario, por favor intenta de nuevo');
                    } else {
                        $(".close").trigger('click');
                        ExitoCustom("Comentario guardado con éxito");

                    }
                }
            });
        }
        //ASOCIAR ORDEN
        $("body").on('click', '.js_asociar_orden', function(e) {
            e.preventDefault();
            id_cita = $(this).data('id');
            ConfirmCustom("¿Está seguro de asociar la orden?", asociarOrden, "async", "Confirmar", "Cancelar");
        });

        function asociarOrden(){
            var url =site_url+"/citas/asociarOrden";
            ajaxJson(url,{"idcita":id_cita},"POST","",function(result){
                if(result == -1){
                ErrorCustom('Ya fue asociada una orden de garantía a este # de orden');
                }else{
                ExitoCustom("Orden asociada correctamente, #"+result,function(){
                    location.reload();
                });
                }
            });
        }
        $("body").on('click', '.js_cambiar_status', function(e) {
            e.preventDefault();
            id_cita = $(this).data('id_cita')
            var unidad_entregada = $(this).data('unidadentregada');
            if (unidad_entregada) {
                ErrorCustom("La unidad ya fue entregada");
            } else {
                ConfirmCustom("¿Está seguro de poner el estatus en unidad entregada?", entregarUnidad, "",
                    "Confirmar", "Cancelar");
            }


        });

        function entregarUnidad() {
            var url = site_url + "/orden/entregarUnidad/";
            ajaxJson(url, {
                "id_cita": id_cita
            }, "POST", "", function(result) {
                if (result == 0) {
                    ErrorCustom('No se pudo cambiar el estatus, por favor intenta de nuevo');
                } else if (result == -1) {
                    ErrorCustom("No se puede entregar la unidad por que no existe la firma del cliente");
                } else if (result == -2) { //
                    ErrorCustom("Solamente puedes entregar una unidad si ya está cerrada la orden");
                } else {

                    ExitoCustom("Estatus cambiado correctamente", function() {
                        location.reload();
                    });
                }
            });
        }
        //Factura
        //NUEVA VERSIÓN
        $("body").on("click", '.js_factura', function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var url = site_url + "/orden/modal_factura/0";
            customModal(url, {
                "id": id
            }, "POST", "md", guardarFactura, "", "Guardar", "Salir", "Anexar factura", "modalFactura");
        });

        function guardarFactura() {
            ajaxJson(site_url + "/orden/guardarFactura", $("#frm-factura").serialize(), "POST", true, function(result) {
                if (isNaN(result)) {
                    data = JSON.parse(result);
                    //Se recorre el json y se coloca el error en la div correspondiente
                    $.each(data, function(i, item) {
                        $(".error_" + i).empty();
                        $(".error_" + i).append(item);
                        $(".error_" + i).css("color", "red");
                    });
                } else {
                    if (result) {
                        ExitoCustom("Archivos guardados correctamente", function() {
                            $(".close").trigger('click');
                        });

                    } else {
                        ErrorCustom('Error al guardar por favor intenta otra vez');
                    }
                }
            });
        }

        $("body").on("click", '.eliminar_pdf', function(e) {
            e.preventDefault();
            archivo_eliminar = $("#path_factura_pdf").val();
            tipo_factura = 'pdf';
            var url = site_url + "/orden/login_eliminar_facturas/0";
            customModal(url, {}, "GET", "md", ValidarLogin, "", "Ingresar", "Cancelar", "Eliminar factura",
                "modalLogin");
        });
        $("body").on("click", '.eliminar_xml', function(e) {
            e.preventDefault();
            tipo_factura = 'xml';
            archivo_eliminar = $("#path_factura_xml").val();
            var url = site_url + "/orden/login_eliminar_facturas/0";
            customModal(url, {}, "GET", "md", ValidarLogin, "", "Ingresar", "Cancelar", "Eliminar factura",
                "modalLogin");
        });

        function ValidarLogin() {
            var url = site_url + "/orden/login_eliminar_facturas";
            ajaxJson(url, {
                "usuario": $("#usuario").val(),
                "password": $("#password").val()
            }, "POST", "", function(result) {
                if (isNaN(result)) {
                    data = JSON.parse(result);
                    //Se recorre el json y se coloca el error en la div correspondiente
                    $.each(data, function(i, item) {
                        $(".error_" + i).empty();
                        $(".error_" + i).append(item);
                        $(".error_" + i).css("color", "red");
                    });
                } else {
                    if (result == 0) {
                        ErrorCustom('Usuario o contraseña inválidos.');
                        $(".deshabilitados").prop('readonly', true);
                    } else {
                        if (tipo_factura == 'pdf') {
                            $('.modalLogin').modal('toggle');

                            callbackEliminarPDF();
                        } else {
                            $('.modalLogin').modal('toggle');

                            callbackEliminarXML();
                        }

                    }
                }
            });
        }

        function callbackEliminarPDF() {
            ajaxJson(site_url + "/orden/upload_delete_pdf", {
                "path_factura_pdf": $("#path_factura_pdf").val(),
                "factura_pdf": $("#factura_pdf").val()
            }, "POST", true, function(j) {
                var j = $.parseJSON(j);
                if (j.isok) {
                    ExitoCustom("Archivo eliminado correctamente");
                    $("#files_pdf").html("");
                    $("#divfileupload_pdf").show();
                    $("#factura_pdf").val('');
                    $("#path_factura_pdf").val('');
                }
            });
        }

        function callbackEliminarXML() {
            ajaxJson(site_url + "/orden/upload_delete_xml", {
                "path_factura_xml": $("#path_factura_xml").val(),
                "factura_xml": $("#factura_xml").val()
            }, "POST", true, function(j) {
                var j = $.parseJSON(j);
                if (j.isok) {
                    ExitoCustom("Archivo eliminado correctamente");
                    $("#files_xml").html("");
                    $("#divfileupload_xml").show();
                    $("#factura_xml").val('');
                    $("#path_factura_xml").val('');
                }
            });
        }

        //Cancelar cita
        $("body").on("click", '.js_cancelar', function(e) {
            e.preventDefault();
            idorden = $(this).data('id');
            id_cita = $(this).data('id_cita');
            ConfirmCustom("¿Está seguro de cancelar la orden?", callbackCancelarOrden, "", "Confirmar", "Cancelar");
        });

        function callbackCancelarOrden() {
            var url = site_url + "/orden/cancelar_orden/";
            ajaxJson(url, {
                "id": idorden,
                "id_cita": id_cita
            }, "POST", "", function(result) {
                if (result == 0) {
                    ErrorCustom('La orden no se pudo cancelar, intenta otra vez');
                } else if (result == -1) {
                    ErrorCustom('La orden no se pudo cancelar, ya fue terminada');
                } else {
                    ExitoCustom("Orden cancelada correctamente", function() {
                        window.location.href = site_url + "/orden/historial_orden_servicio";
                    });
                }
            });
        }
        //Estatus orden
        $("body").on("click", '.js_asociar_folio', function(e) {
            e.preventDefault();
            id = $(this).data('id');
            id_cita = $(this).data('id_cita');
            var url = site_url + "/orden/asociar_orden_folio/0";
            customModal(url, {
                    "id": id,
                    "id_cita": id_cita
                }, "GET", "md", asociarOrdenFolio, "", "Guardar", "Cancelar",
                "Asociar folio a orden de garantía", "modalAsociarOrden");

        });

        function asociarOrdenFolio() {
            var url = site_url + "/orden/ingresarFolio/";
            if ($("#folio_asociar").val() != '') {
                const arr_folio = $("#folio_asociar").val().split('-');
                ajaxJson(url, {
                    "folio": arr_folio[0],
                    "orden_asociada": arr_folio[1],
                    "id_cita": id_cita
                }, "POST", "", function(result) {
                    if (result == 0) {
                        ErrorCustom('Error al asignar el folio, intenta otra vez');
                    } else {
                        ExitoCustom('Folio asignado correctamente', function() {
                            $("#buscar").trigger('click');
                            $(".close").trigger('click');
                        });
                    }
                });
            }else{
                $(".error_folio_asociar").empty().append('El folio es requerido');
            }
        }

    </script>
@endsection
