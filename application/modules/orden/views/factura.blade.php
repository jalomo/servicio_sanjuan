

<form id="frm-factura">
	<input type="hidden" name="ordenid" id="ordenid" value="{{$idorden}}">
	<input type="hidden" name="factura_pdf" id="factura_pdf" value="{{$factura_pdf}}">
	<input type="hidden" name="path_factura_pdf" id="path_factura_pdf" value="{{$path_factura_pdf}}">
	<input type="hidden" name="factura_xml" id="factura_xml" value="{{$factura_xml}}">
	<input type="hidden" name="path_factura_xml" id="path_factura_xml" value="{{$path_factura_xml}}">

	<div class="row">
		<div class="col-sm-6">
			<label>Factura formato pdf</label>
			 {{ $this->blade->render('orden/factura_pdf',array('pdf'=>$factura_pdf,'id'=>$idorden),true)}}
			<span class="error">** Sólo se permiten archivos PDF</span>
	 		<span class="error error_factura_pdf"></span>
		</div>
		<div class="col-sm-6">
			<label>Factura formato xml</label>
			 {{ $this->blade->render('orden/factura_xml',array('xml'=>$factura_xml,'id'=>$idorden),true)}}
			<span class="error">** Sólo se permiten archivos XML</span>
	 		<span class="error error_factura_xml"></span>
		</div>
	</div>
</form>


<script type="text/javascript">
	
	$(".fileupload_pdf").each(function(){
		$(this).fileupload({        
	        dataType: 'json',
	        done: function (e, data) {
	        	
	            $.each(data.result, function (index, file) {
	            	if(file.isok){
	            		$("#factura_pdf").val(file.file_name);
	            		$("#path_factura_pdf").val(file.path);
	            		var html = "<a href='{{ base_url()}}assets/facturas/"+file.file_name+"' target='_blank' title='"+file.client_name+"' class='btn btn-info btn-xs'><i class='fa fa-save'></i> Descargar</a>";
	            		html = html + "<a href='{{ base_url()}}orden/upload_delete_pdf/"+file.file_name+"' data-id='"+file.file_name+"' 'title='Eliminar "+file.client_name+"' class='btn btn-xs eliminar_pdf text-right'><i class='pe pe-7s-trash'></i></a>";
	
	            		$("#files_pdf").html(html);							            		
	            		$("#divfileupload_pdf").hide();
	            	}else{
	            		ErrorCustom("Error al procesar el documento:<br/>"+file.error,"warning");
	            	}	                
	            });
	        },
		
	 		start: function (e, data) {
	 		  $('#progress_pdf').show();
	          $('#progress_pdf .progress-bar_pdf').html('0%');
	          $('#progress_pdf .progress-bar_pdf').css(
	              'width',
	              '0%'
	          );
	        },
	        progressall: function (e, data) {
	          var progress = parseInt(data.loaded / data.total * 100, 10);
	          $('#progress_pdf .progress-bar_pdf').html(progress + '%');
	          $('#progress_pdf .progress-bar_pdf').css(
	              'width',
	              progress + '%'
	          );
	          setTimeout(function(){ $('#progress_pdf').hide(); }, 1000);
	          
	        },
	        progress: function (e, data) {
	            var progress = parseInt(data.loaded / data.total * 100, 10);
	            $('#progress_pdf .progress-bar_pdf').html(progress + '%');
	            $('#progress_pdf .progress-bar_pdf').css(
	                'width',
	                progress + '%'
	            );
	        }
	    }).prop('disabled', !$.support.fileInput)
	        .parent().addClass($.support.fileInput ? undefined : 'disabled');

	}); //fileupload pdf
	$(".fileupload_xml").each(function(){
		$(this).fileupload({        
	        dataType: 'json',
	        done: function (e, data) {
	            $.each(data.result, function (index, file) {
	            	if(file.isok){
	            		$("#factura_xml").val(file.file_name);
	            		$("#path_factura_xml").val(file.path);
	            		var html = "<a href='{{ base_url()}}assets/facturas/"+file.file_name+"' target='_blank' title='"+file.client_name+"' class='btn btn-info btn-xs'><i class='fa fa-save'></i> Descargar</a>";
	            		html = html + "<a href='{{ base_url()}}orden/upload_delete_xml/"+file.file_name+"' data-id='"+file.file_name+"' 'title='Eliminar "+file.client_name+"' class='btn btn-xs eliminar_xml text-right'><i class='pe pe-7s-trash'></i></a>";
	
	            		$("#files_xml").html(html);							            		
	            		$("#divfileupload_xml").hide();
	            	}else{
	            		ErrorCustom("Error al procesar el documento:<br/>"+file.error,"warning");
	            	}	                
	            });
	        },
		
	 		start: function (e, data) {
	 		  $('#progress_xml').show();
	          $('#progress_xml .progress-bar_xml').html('0%');
	          $('#progress_xml .progress-bar_xml').css(
	              'width',
	              '0%'
	          );
	        },
	        progressall: function (e, data) {
	          var progress = parseInt(data.loaded / data.total * 100, 10);
	          $('#progress_xml .progress-bar_xml').html(progress + '%');
	          $('#progress_xml .progress-bar_xml').css(
	              'width',
	              progress + '%'
	          );
	          setTimeout(function(){ $('#progress_xml').hide(); }, 1000);
	          
	        },
	        progress: function (e, data) {
	            var progress = parseInt(data.loaded / data.total * 100, 10);
	            $('#progress_xml .progress-bar_xml').html(progress + '%');
	            $('#progress_xml .progress-bar_xml').css(
	                'width',
	                progress + '%'
	            );
	        }
	    }).prop('disabled', !$.support.fileInput)
	        .parent().addClass($.support.fileInput ? undefined : 'disabled');

	}); //fileupload xml
	
</script>


