<div class="row">
    <div class="col-sm-12">
        <label for="">Selecciona folio</label>
        <select class="form-control" name="folio_asociar" id="folio_asociar">
            <option value="">-- Selecciona un folio</option>
            @foreach ($folios as $f => $folio)
                <option value={{$folio->folio}}-{{$folio->num_orden_garantia}}>{{$folio->folio}}-{{$folio->num_orden_garantia}}</option>
            @endforeach
        </select>
        <div class="error error_folio_asociar"></div>
    </div>
</div>

<script>
    $(".busqueda").select2();
    $(".busqueda").select2({ width: 'resolve' });      
</script>
