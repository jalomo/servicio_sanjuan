<div id="divfileupload_pdf" style="{{(($pdf != '') ? 'display:none;' : '')}}">
	<span class="btn btn-success fileinput-button btn-xs" title="Agregar factura pdf">
	    <i class="pe pe-7s-plus"></i>
	    <span>Agregar factura pdf</span>
	    <!-- The file input field used as target for the file upload widget -->
	    <input id="fileupload_pdf" type="file" name="file[]" data-url="{{ site_url('orden/upload/').$id }}/pdf" multiple class="fileupload_pdf btn-success">
	</span>

	<div id="progress_pdf" class="progress_pdf" style="display:none;">
	    <div class="progress-bar_pdf progress-bar-azul"></div>
	</div>
</div>

<div id="files_pdf" class="files_pdf">
<?php if($pdf != ''){ ?>
	
	<?php echo anchor(base_url('assets/facturas/'.$pdf),'<i class="fa fa-save-file"></i> Descargar','title="'.$pdf.'" class="btn btn-info btn-xs" target="_blank"') ?>
	<?php echo anchor('orden/upload_delete/'.$pdf,'<i class="pe pe-7s-trash"></i>','data-id="" data-pdf="'.$pdf.'" title="Eliminar'.$pdf.'" class="btn btn-xs eliminar_pdf text-centernteright" target="_blank"') ?>

<?php } ?>
</div>


