
<div class="row">
    <div class="col-sm-12">
        <label for="">Selecciona folio</label>
        <select class="form-control" name="operacion_folio" id="operacion_folio">
            <option value="">-- Selecciona un folio</option>
            @foreach ($folios->data as $f => $folio)
            <?php
            ?>
                <option value={{ $folio->id_parte_operacion }}>{{ $folio->id_parte_operacion }}-{{ $folio->queja_cliente }}</option>
            @endforeach
        </select>
        <div class="error error_operacion_folio"></div>
    </div>
</div>

<script>
    $(".busqueda").select2();
    $(".busqueda").select2({
        width: 'resolve'
    });

</script>
