$("#inventario").on("click", function (e) {
	e.preventDefault();
	var url = site_url + "/dms/inventario/";
	customModal(
		url,
		{},
		"GET",
		"lg",
		"",
		"",
		"",
		"Cerrar",
		"Inventario",
		"modalInventario"
	);
});
$("body").on("click", "#btn-buscar-inventario", () => {
	if ($("#busqueda_inventario").val() != "") {
		var url = site_url + "/dms/buscar_inventario";
		ajaxLoad(
			url,
			$("#frm-busqueda-inventario").serialize(),
			"tabla-busqueda",
			"POST",
			function () {}
		);
	} else {
		$(".error_busqueda_inventario")
			.empty()
			.append("Es necesario ingresar la búsqueda")
			.css("color", "red");
	}
});
$("body").on("click", ".buscar_serie", function (e) {
	e.preventDefault();
	getDataDMS("placas", $(this).data("serie"));
	$(".modalVeh").modal("hide");
});
$("body").on("change", "#numero_cliente", function () {
	if ($(this).val() != "") {
		getDatosCliente($(this).val());
	}
});
$("body").on("click", ".seleccionar_cliente", function (e) {
	e.preventDefault();
	getDatosCliente($(this).data("cliente"));
	$(".close").trigger("click");
});
$("#clientebuscar").on("click", function () {
	var busqueda = $("#clientesearch").val();
	if (busqueda != "") {
		if (busqueda.length < 4) {
			ErrorCustom("Es necesario ingresar por lo menos 4 caracteres");
		} else {
			datosClienteAPIByNombre(busqueda);
		}
	} else {
		ErrorCustom("Es necesario ingresar el nombre del cliente");
	}
});
function getDataDMS(tipo = "", data = "") {
	$("#vehiculo_nuevo").val(1);
	var url = site_url + "/dms/datosClienteBySeriePlacas";
	ajaxJson(url, { tipo: tipo, data: data }, "POST", "", function (result) {
		result = JSON.parse(result);
		if (result.cantidad == 1) {
			$("#numero_cliente").val(result.datos.numero_cliente);
			$("#datos_nombres").val(result.datos.nombre);
			$("#datos_apellido_paterno").val(result.datos.apellido_paterno);
			$("#datos_apellido_materno").val(result.datos.apellido_materno);
			$("#vehiculo_placas").val(result.datos.placas);
			$("#email").val(result.datos.correo_electronico);
			$("#datos_telefono").val(result.datos.telefono);
			$("#vehiculo_numero_serie").val(result.datos.vin);
			$("#vehiculo_anio").val(result.datos.anio);
			$("#transmision").val(result.datos.transmision);
			$("#motor").val(result.datos.motor);
			$("#id_color").val(result.datos.color_id);
			$("#cilindros").val(result.datos.numero_cilindros);
			$("#nuevocliente").prop("checked", false);
			$("#vehiculo_nuevo").val(0);
			//Filtrar el vehículo por el vin
			const vehiculo = result.datos.vehiculo.filter((vehiculo)=>parseFloat(vehiculo.vin)==data)
			$("#vehiculo_modelo").val(vehiculo[0].modelo_id);
			$("#id_color").val(vehiculo[0].color_id);
			$("#vehiculo_numero_serie").val(vehiculo[0].vin);
			$("#vehiculo_placas").val(vehiculo[0].placas);
			$("#vehiculo_anio").val(vehiculo[0].anio);
			$("#motor").val(vehiculo[0].motor);
			$("#transmision").val(vehiculo[0].transmision);
			$("#cilindros").val(vehiculo[0].numero_cilindros);
			$("#vehiculo_nuevo").val(0);
			getPresupuestos();
		}else{
			getDataOrdenesOld(data)
		}
	});
}
function getDatosCliente(busqueda) {
	var url = site_url + "/dms/datosByCliente";
	ajaxJson(
		url,
		{ cliente: busqueda },
		"POST",
		"async",
		function (result) {
			result = JSON.parse(result);
			if (result.cantidad == 1) {
				llenarDatosCliente(result, busqueda);
			} else {
				ErrorCustom("No se encontraron coincidencias");
			}
		},
		true,
		"Buscando información..."
	);
}
function llenarDatosCliente(result, busqueda) {
	console.log(result,busqueda);
	if (typeof result.datos[0] === 'undefined') {
		result = result.datos
	} else {
    result = result.datos[0]
	}
	$("#numero_cliente").val(result.numero_cliente);
	$("#datos_nombres").val(result.nombre);
	$("#datos_apellido_paterno").val(result.apellido_paterno);
	$("#datos_apellido_materno").val(result.apellido_materno);
	$("#estado").val(result.Estado);
	$("#cp").val(result.CP);
	$("#rfc").val(result.rfc);
	$("#email").val(result.correo_electronico);
	$("#nombre_compania").val(result.nombre_empresa);
	$("#calle").val(result.direccion);
	$("#colonia").val(result.colonia);
	$("#municipio").val(result.municipio);
	$("#estado").val(result.estado);
	$("#cp").val(result.codigo_postal);
	$("#telefono_movil").val(result.telefono);
	$("#otro_telefono").val(result.telefono_secundario);
	$("#nombre_contacto_compania").val(result.contactos.contacto_nombre);
	$("#ap_contacto").val(result.contactos.contacto_apellido_paterno);
	$("#am_contacto").val(result.contactos.contacto_apellido_materno);
	$("#cliente_nuevo").val(0);
	if (result.vehiculo.length > 0) {
		if (result.vehiculo.length == 1) {
			$("#vehiculo_modelo").val(result.vehiculo[0].modelo_id);
			$("#id_color").val(result.vehiculo[0].color_id);
			$("#vehiculo_numero_serie").val(result.vehiculo[0].vin);
			$("#vehiculo_placas").val(result.vehiculo[0].placas);
			$("#vehiculo_anio").val(result.vehiculo[0].anio);
			$("#motor").val(result.vehiculo[0].motor);
			$("#transmision").val(result.vehiculo[0].transmision);
			$("#cilindros").val(result.vehiculo[0].numero_cilindros);
			$("#vehiculo_nuevo").val(0);
		} else {
			var url = site_url + "/dms/buscar_vehiculos/0";
			customModal(
				url,
				{ cliente: busqueda },
				"POST",
				"lg",
				"",
				"",
				"",
				"Cancelar",
				"Seleccionar vehículo",
				"modalVeh"
			);
		}
	}
}
function datosClienteAPIByNombre(nombre) {
	var url = site_url + "/dms/datosClienteAPIByNombre";
	ajaxJson(
		url,
		{ nombre: nombre, ismodal: 0 },
		"POST",
		"async",
		function (result) {
			result = JSON.parse(result);
			if (result.cantidad == 1) {
				llenarDatosCliente(result, nombre);
			} else if (result.cantidad == 0) {
				// ErrorCustom("No se encontraron coincidencias");
				datosCliente(nombre)
			} else {
				var url = site_url + "/dms/datosClienteAPIByNombre/0";
				customModal(
					url,
					{ nombre: $("#clientesearch").val(), ismodal: 1 },
					"POST",
					"lg",
					"",
					"",
					"",
					"Cancelar",
					"",
					"modalClientesNombre"
				);
			}
		},
		true,
		"Buscando información..."
	);
}
function datosCliente(nombre) {
	var url = site_url + "/citas/getDataByNombre";
	ajaxJson(
		url,
		{ nombre: nombre, modal: 0 },
		"POST",
		"async",
		function (result) {
			result = JSON.parse(result);
			if (result.cantidad == 1) {
				$(".busqueda").select2('destroy');
				llenarClienteServicio(result.datos[0])
	            $(".busqueda").select2();
			} else if (result.cantidad == 0) {
				ErrorCustom("No se encontraron coincidencias");
			} else {
				var url = site_url + "/citas/getDataByNombre/0";
				customModal(
					url,
					{ nombre: $("#clientesearch").val(), modal: 1 },
					"POST",
					"lg",
					"",
					"",
					"",
					"Cancelar",
					"",
					"modalClientesNombre"
				);
			}
		},
		true,
		"Buscando información..."
	);
}
function llenarClienteServicio(data){
	$("#datos_nombres").val(data.datos_nombres);
	$("#datos_apellido_paterno").val(data.datos_apellido_paterno);
	$("#datos_apellido_materno").val(data.datos_apellido_materno);
	$("#email").val(data.email);
	$("#datos_telefono").val(data.datos_telefono);
	$("#vehiculo_numero_serie").val(data.vehiculo_numero_serie);
	$("#vehiculo_placas").val(data.vehiculo_placas);
	$("#id_color").val(data.id_color);
	$("#vehiculo_anio").val(data.vehiculo_anio);
	$("#vehiculo_modelo").val(data.vehiculo_modelo);
	$("#vehiculo_kilometraje").val(data.vehiculo_kilometraje);

	//ORDEN
	$("#nombre_compania").val(data.nombre_compania);
	$("#nombre_contacto_compania").val(data.nombre_contacto_compania);
	$("#ap_contacto").val(data.ap_contacto);
	$("#am_contacto").val(data.am_contacto);
	$("#rfc").val(data.rfc);
	$("#correo_compania").val(data.correo_compania);
	$("#calle").val(data.calle);
	$("#noexterior").val(data.noexterior);
	$("#nointerior").val(data.nointerior);
	$("#colonia").val(data.colonia);
	$("#municipio").val(data.municipio);
	$("#cp").val(data.cp);
	$("#estado").val(data.estado);
	$("#telefono_movil").val(data.telefono_movil);
	$("#otro_telefono").val(data.otro_telefono);
	$("#motor").val(data.motor);
	$("#cilindros").val(data.cilindros);
	$("#transmision").val(data.transmision);
	$("#numero_cliente").val(data.numero_cliente);
	getPresupuestos();
}
$("body").on("click", ".seleccionar_cliente_servicios", function (e) {
	e.preventDefault();
	let id_cita = $(this).data("id_cita");
	var url = site_url + "/citas/getFullDataCita";
	ajaxJson(
		url,
		{ id_cita},
		"POST",
		"async",
		function (result) {
			result = JSON.parse(result);
			if (result.cantidad == 1) {
				$(".busqueda").select2('destroy');
				llenarClienteServicio(result.datos)
	            $(".busqueda").select2();
	            $(".close").trigger("click");
			} else if (result.cantidad == 0) {
				ErrorCustom("No se encontraron coincidencias");
			} 
		},
		true,
		"Buscando información..."
	);
});
