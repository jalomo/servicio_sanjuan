$("body").on('click','.js_add_campania',function(e){
	e.preventDefault();
	var url =site_url+"/citas/addCampania/0";
	customModal(url,{"serie":$("#vehiculo_numero_serie").val()},"GET","md",SaveCampania,"","Guardar","Cancelar","Guardar campaña","modalCampania");
});
$('#hora_inicio').on('change',function(){
	validar_fecha_inicio();
});
$('.hora_fin').on('change',validar_fin);
$("#dia_completo").on('click',function(){
	if($(this).prop('checked')){
		$("#div_completo").show('slow');
		$("#div_incompleto").hide('slow');
	}else{
		$("#div_completo").hide('slow');
		$("#div_incompleto").show('slow');
	}
});
$(".js_view_refa").on('click',function(e){
	e.preventDefault();
	var url =site_url+"/citas/viewRefa/0";
	customModal(url,{},"GET","lg","","","","Cerrar","Refacciones ","modalRefacciones");
});
$("#div_completo").hide();
var fecha_actual = "<?php echo date('Y-m-d') ?>";
$("#datetimepicker1").on("dp.change", function (e) {
	$('#datetimepicker2').data("DateTimePicker").minDate(e.date);
});
$("#datetimepicker2").on("dp.change", function (e) {
	var new_date = moment(e.date, "DD-MM-YYYY").add(1, 'days');
	$('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
	$('#datetimepicker3').data("DateTimePicker").minDate(new_date);
});
$("body").on('click','.js_ver_citas',function(e){
	e.preventDefault();
	var id_tecnico = $("#tecnicos").val();
	if(id_tecnico==''){
		ErrorCustom("Es necesario asignar al técnico");
	}else{
		var url =site_url+"/citas/modal_ver_citas_asignadas/0";
		customModal(url,{"id_tecnico":id_tecnico,'fecha':$("#fecha").val()},"POST","md","","","","Cerrar","Lista de citas asignadas","modal6");
	}
});
$("body").on('click','.js_ver_citas_dias',function(e){
	e.preventDefault();
	var id_tecnico = $("#tecnicos_dias").val();
	if(id_tecnico==''){
		ErrorCustom("Es necesario asignar al técnico");
	}else{
		var fecha_split = $("#fecha").val().split('/');
		fecha = fecha_split['2']+'-'+fecha_split['1']+'-'+fecha_split['0'];
		var url =site_url+"/citas/modal_ver_citas_asignadas/0";
		customModal(url,{"id_tecnico":id_tecnico,'fecha':fecha},"POST","md","","","","Cerrar","Lista de citas asignadas","modal6");
	}
});
function validar_fin(){	
	var hora_fin = $("#hora_fin").val();
	var hora_inicio = $("#hora_inicio").val();

	if(hora_fin < hora_inicio){
		$("#hora_fin").val(hora_inicio);
	}
}
// function getPresupuestos(){
// 	var url=site_url+"/dms/ordenes_serie";
// 	ajaxLoad(url,{"serie":$("#vehiculo_numero_serie").val()},'div_refacciones',"POST","async",function(result){
// 	});
// }
$("#btn-no-aut").on('click',function(){
    var url =site_url+"/citas/ViewPresupuestos/0";
         customModal(url,{"serie":$("#vehiculo_numero_serie").val(),"mostrar_tabla":1},"POST","lg","","","","Cancelar","Presupuestos NO autorizados","modalNoa");
  });
  function getPresupuestos(){
    var url=site_url+"/citas/ViewPresupuestos";
      ajaxJson(url,{"serie":$("#vehiculo_numero_serie").val(),"mostrar_tabla":0},"POST","async",function(result){
        if(result==1){
          $("#btn-no-aut").removeClass('invisible');
        }else{
          $("#btn-no-aut").addClass('invisible');
        }
      });
  }
function ValidarCampania(){
	ajaxJson(site_url+'/citas/validarCampania',{"serie":$("#vehiculo_numero_serie").val()},"POST","",function(result){
		result=JSON.parse(result);
		if(result.registros){
			$("#lbl-campania").empty().append('La unidad cuenta con campaña(s) ('+result.datos+') <a class="js_add_campania" href="#">Agregar campaña</a>');
			$("#color-2").prop('checked',true)
			$("#color-1").prop('disabled',true)
			$("#color-2").prop('disabled',false)
			$("#color-3").prop('disabled',false)

		}else{
			$("#lbl-campania").empty().append('La unidad NO cuenta con campaña <a class="js_add_campania" href="#">Agregar campaña</a>');
			$("#color-1").prop('disabled',false)
			$("#color-2").prop('disabled',true)
			$("#color-3").prop('disabled',true)
			$("#color-1").prop('checked',true)
		}
	});
}
function SaveCampania(){
	var url = site_url+'/citas/addCampania';
	ajaxJson(url,$("#frm-campania").serialize(),"POST","async",function(result){
		if(isNaN(result)){
			data = JSON.parse( result );
			$.each(data, function(i, item) {
				$(".error_"+i).empty();
				$(".error_"+i).append(item);
				$(".error_"+i).css("color","red");
			});
		}else{
			if(result==1){
				ExitoCustom("Guardado correctamente",function(){
					$(".modalCampania").modal('hide');
					ValidarCampania();
				});
			}else{
				ErrorCustom('No se pudo guardar, intenta otra vez.');
			}
		}
	});
}
function validarHorasIguales(){
	return true;
    if(!$("#dia_completo").prop('checked')){
      if($("#hora_inicio").val()==$("#hora_fin").val() && $("#hora_inicio").val()!='' && $("#hora_fin").val()!='' ){
        return false;
      }
    }
    return true;
  }
function llenarCamposNoVacios(campo='',valor=''){
	if(valor!=''){
		$("#"+campo).val(valor);
	}
}
function validar_fecha_inicio(){
	var dt = new Date();
	if(parseInt(dt.getMinutes())<10){
		var time = dt.getHours() + ":0" + dt.getMinutes();
	}else{
		var time = dt.getHours() + ":" + dt.getMinutes();
	}
	var hora_inicio = $("#hora_inicio").val();
	var hora_fin = $("#hora_inicio").val();
	if(time.length==4){
		time = '0'+time;
	}
	var fecha_asesor = $("#fecha").val();
	if(fecha_asesor){
		if(fecha_asesor===fecha_comparar){
			if(hora_inicio < time){
				$("#hora_inicio").val(time);
			}
			if(hora_fin<hora_inicio){
				$("#hora_fin").val(hora_inicio)
			}     
		}
	}else{
		if(hora_inicio < time){
			$("#hora_inicio").val(time);
		}
		if(hora_fin<hora_inicio){
			$("#hora_fin").val(hora_inicio)
		}  
	}
}
function buscar_serie(){
	var url=site_url+"/citas/getSerie";
	ajaxJson(url,{"placas":$("#vehiculo_placas").val()},"POST","",function(result){
		if(result){
			$("#vehiculo_numero_serie").val(result);
			$("#vehiculo_numero_serie").trigger('change');
		}else{
			getDataOrdenesOld($("#vehiculo_placas").val())
		}
	});
}
function verificarFormatoHoras(){
	var contador = true;
	if($("#dia_completo").prop('checked')){
		$("#hora_inicio").removeClass('check_hora');
		$("#hora_fin").removeClass('check_hora');
		$("#hora_comienzo").addClass('check_hora');
		$("#hora_fin_extra").addClass('check_hora');
	}else{
		$("#hora_inicio").addClass('check_hora');
		$("#hora_fin").addClass('check_hora');
		$("#hora_comienzo").removeClass('check_hora');
		$("#hora_fin_extra").removeClass('check_hora');
	}
	$.each( $(".check_hora"), function( key, value ) {
		var valor = '';
		valor = $(value).val();
		var id = $(value).attr('id');
		if(valor !='' && valor != 'undefined'){
			var array_valor = valor.split(':');
			valor = array_valor[1];
			if(valor==00 || valor==20 || valor==40){
				$(".error_"+id).empty();
			}else{
				$(".error_"+id).empty();
				$(".error_"+id).append('El formato de la hora debe ser lapso de 20 minutos');
				$(".error_"+id).css("color","red");
				contador = false;
			}
		}
	});
	return contador ;
}
function getDataOrdenesOld(busqueda) {
	var url = site_url + "/citas/datosOrdenesOld";
	ajaxJson(url, {busqueda }, "POST", "", function (result) {
		result = JSON.parse(result);
		if (result.datos) {
			
			$("#datos_nombres").val(result.datos.cnombre);
			$("#datos_apellido_paterno").val(result.datos.cam);
			$("#datos_apellido_materno").val(result.datos.cap);
			$("#vehiculo_placas").val(result.datos.placas);
			$("#email").val(result.datos.correo);
			$("#datos_telefono").val(result.datos.celular);
			$("#vehiculo_numero_serie").val(result.datos.serie);
			$("#vehiculo_anio").val(result.datos.modelo);
			$("#transmision").val(result.datos.transmision);
			$("#motor").val(result.datos.motor);
			$("#cilindros").val(result.datos.cilindros);
			$("#rfc").val(result.datos.rfc);

			$("#nombre_contacto_compania").val(result.datos.cnombre);
			$("#ap_contacto").val(result.datos.cam);
			$("#am_contacto").val(result.datos.cap);
			$("#noexterior").val(result.datos.numero);
			$("#colonia").val(result.datos.colonia);
			$("#municipio").val(result.datos.poblacion);
			$("#estado").val(result.datos.estado);
			$("#cp").val(result.datos.cp);
			$("#telefono_movil").val(result.datos.celular);
			$("#otro_telefono").val(result.datos.tel);
			$("#vehiculo_kilometraje").val(result.datos.kilometraje);
			$("#calle").val(result.datos.direccion);
			$("#nombre_compania").val(result.datos.contacto);
			
		}
	});
}